﻿using HuangLiCollege.Common;
using HuangLiCollege.Domain.Entities.School;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace HuangLiCollege.Domain.Services.School
{
    public interface ISchoolService : IService
    {

        #region IComBaseService
        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
       SchoolEntity Get(object id);

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(SchoolEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(SchoolEntity entity);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(SchoolEntity entity);

        /// <summary>
        /// 根据查询条件批量删除
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void Delete(Expression<Func<SchoolEntity, bool>> predicate);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<SchoolEntity> Table();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <returns>IQueryable类型的实体集合</returns>
        IQueryable<SchoolEntity> Fetch(Expression<Func<SchoolEntity, bool>> predicate, Action<Orderable<SchoolEntity>> order);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="pageIndex">第几页</param>
        PageList<SchoolEntity> Gets(Expression<Func<SchoolEntity, bool>> predicate, Action<Orderable<SchoolEntity>> order, int pageSize, int pageIndex);
        #endregion

    }

}
