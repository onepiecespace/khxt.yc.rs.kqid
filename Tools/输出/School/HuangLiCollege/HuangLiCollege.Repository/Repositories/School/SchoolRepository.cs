﻿using Dapper;
using HuangLiCollege.Domain.Entities.School;
using HuangLiCollege.Domain.Repositories.School;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace HuangLiCollege.Repository.School
{
    public class SchoolRepository : DapperRepositoryBase<SchoolEntity, long>, ISchoolRepository
    {
        public SchoolRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        } 
    }
 

}
