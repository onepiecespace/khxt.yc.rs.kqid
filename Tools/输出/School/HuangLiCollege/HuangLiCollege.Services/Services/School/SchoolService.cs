﻿using HuangLiCollege.Common;
using HuangLiCollege.Domain.Entities.School;
using HuangLiCollege.Domain.Repositories.School;
using HuangLiCollege.Domain.Services;
using HuangLiCollege.Domain.Services.School;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace HuangLiCollege.Services.School
{
    public class SchoolService : ServiceBase, ISchoolService
    {
        ISchoolRepository _repo;

        public SchoolService(ISchoolRepository repo)
        {
            _repo = repo;
        }

        #region IComBaseService
        public IQueryable<SchoolEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable< SchoolEntity>>(() =>
                {
                    return _repo.GetList().AsQueryable();
                });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create( SchoolEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }



        public void Delete( SchoolEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(entity);
            });
        }

        public void Delete(Expression<Func< SchoolEntity, bool>> predicate)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(predicate);
            });
        }



        public IQueryable< SchoolEntity> Fetch(Expression<Func< SchoolEntity, bool>> predicate, Action<Orderable< SchoolEntity>> order)
        {
            var body = UnitOfWorkService.Execute<IQueryable< SchoolEntity>>(() =>
            {
                if (order == null)
                {
                    return Table().Where(predicate);
                }
                var deferrable = new Orderable< SchoolEntity>(Table().Where(predicate));
                order(deferrable);
                return deferrable.Queryable;

            });
            return body;

        }

        public  SchoolEntity Get(object id)
        {
            var body = UnitOfWorkService.Execute< SchoolEntity>(() =>
            {
				 long.TryParse(id.ToString(), out long nid);
                 return _repo.Get(nid);

            });
            return body;

        }


        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList< SchoolEntity> Gets(Expression<Func< SchoolEntity, bool>> predicate, Action<Orderable< SchoolEntity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList< SchoolEntity>>(() =>
            {
                if (order == null)
                {
                    return new PageList< SchoolEntity>(Table().Where(predicate), pageIndex, pageSize);
                }
                var deferrable = new Orderable< SchoolEntity>(Table().Where(predicate));
                order(deferrable);
                var ts = deferrable.Queryable;
                var totalCount = ts.Count();
                var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

                return new PageList< SchoolEntity>(pagingTs, pageIndex, pageSize, totalCount);

            });
            return body;
        }

        public void Update( SchoolEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }
        #endregion
    }









}
