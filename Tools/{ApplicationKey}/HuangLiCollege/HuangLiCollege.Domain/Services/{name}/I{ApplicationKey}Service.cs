﻿using HuangLiCollege.Common;
using HuangLiCollege.Domain.Entities.{name};
using System;
using System.Linq;
using System.Linq.Expressions;

namespace HuangLiCollege.Domain.Services.{name}
{
    public interface I{ApplicationKey}Service : IService
    {

        #region IComBaseService
        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
       {ApplicationKey}Entity Get(object id);

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create({ApplicationKey}Entity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update({ApplicationKey}Entity entity);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete({ApplicationKey}Entity entity);

        /// <summary>
        /// 根据查询条件批量删除
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void Delete(Expression<Func<{ApplicationKey}Entity, bool>> predicate);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<{ApplicationKey}Entity> Table();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <returns>IQueryable类型的实体集合</returns>
        IQueryable<{ApplicationKey}Entity> Fetch(Expression<Func<{ApplicationKey}Entity, bool>> predicate, Action<Orderable<{ApplicationKey}Entity>> order);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="pageIndex">第几页</param>
        PageList<{ApplicationKey}Entity> Gets(Expression<Func<{ApplicationKey}Entity, bool>> predicate, Action<Orderable<{ApplicationKey}Entity>> order, int pageSize, int pageIndex);
        #endregion

    }

}
