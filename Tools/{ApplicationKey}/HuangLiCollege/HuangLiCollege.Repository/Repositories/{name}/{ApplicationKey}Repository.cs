﻿using Dapper;
using HuangLiCollege.Domain.Entities.{name};
using HuangLiCollege.Domain.Repositories.{name};
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace HuangLiCollege.Repository.{name}
{
    public class {ApplicationKey}Repository : DapperRepositoryBase<{ApplicationKey}Entity, long>, I{ApplicationKey}Repository
    {
        public {ApplicationKey}Repository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        } 
    }
 

}
