﻿using HuangLiCollege.Common;
using HuangLiCollege.Domain;
using HuangLiCollege.Domain.Entities.{name};
using HuangLiCollege.Domain.Repositories.{name};
using HuangLiCollege.Domain.Services;
using HuangLiCollege.Domain.Services.{name};
using System;
using System.Linq;
using System.Linq.Expressions;

namespace HuangLiCollege.Services.{name}
{
    public class {ApplicationKey}Service : ServiceBase, I{ApplicationKey}Service
    {
        I{ApplicationKey}Repository _repo;

        public {ApplicationKey}Service(I{ApplicationKey}Repository repo)
        {
            _repo = repo;
        }

        #region IComBaseService
        public IQueryable<{ApplicationKey}Entity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable< {ApplicationKey}Entity>>(() =>
                {
                    return _repo.GetList().AsQueryable();
                });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create( {ApplicationKey}Entity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }



        public void Delete( {ApplicationKey}Entity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(entity);
            });
        }

        public void Delete(Expression<Func< {ApplicationKey}Entity, bool>> predicate)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(predicate);
            });
        }



        public IQueryable< {ApplicationKey}Entity> Fetch(Expression<Func< {ApplicationKey}Entity, bool>> predicate, Action<Orderable< {ApplicationKey}Entity>> order)
        {
            var body = UnitOfWorkService.Execute<IQueryable< {ApplicationKey}Entity>>(() =>
            {
                if (order == null)
                {
                    return Table().Where(predicate);
                }
                var deferrable = new Orderable< {ApplicationKey}Entity>(Table().Where(predicate));
                order(deferrable);
                return deferrable.Queryable;

            });
            return body;

        }

        public  {ApplicationKey}Entity Get(object id)
        {
            var body = UnitOfWorkService.Execute< {ApplicationKey}Entity>(() =>
            {
				 long.TryParse(id.ToString(), out long nid);
                 return _repo.Get(nid);

            });
            return body;

        }


        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList< {ApplicationKey}Entity> Gets(Expression<Func< {ApplicationKey}Entity, bool>> predicate, Action<Orderable< {ApplicationKey}Entity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList< {ApplicationKey}Entity>>(() =>
            {
                if (order == null)
                {
                    return new PageList< {ApplicationKey}Entity>(Table().Where(predicate), pageIndex, pageSize);
                }
                var deferrable = new Orderable< {ApplicationKey}Entity>(Table().Where(predicate));
                order(deferrable);
                var ts = deferrable.Queryable;
                var totalCount = ts.Count();
                var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

                return new PageList< {ApplicationKey}Entity>(pagingTs, pageIndex, pageSize, totalCount);

            });
            return body;
        }

        public void Update( {ApplicationKey}Entity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }
        #endregion
    }









}
