﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Payment.ApplePay
{
    /// <summary>
    /// 苹果支付
    /// </summary>
    public class AppleIapOptions
    {
        /// <summary>
        /// SandBox验证地址
        /// </summary>
        public string SandBoxVerifyReceiptUrl { get; set; } = "https://sandbox.itunes.apple.com/verifyReceipt";
        /// <summary>
        /// 正式验证地址
        /// </summary>
        public string VerifyReceiptUrl { get; set; } = "https://buy.itunes.apple.com/verifyReceipt";
        /// <summary>
        /// 共享密钥
        /// </summary>
        public string ShareKey { get; set; } = "6b16e4bf3e284f9f8d5e8ea9e36f830e";
    }
}
