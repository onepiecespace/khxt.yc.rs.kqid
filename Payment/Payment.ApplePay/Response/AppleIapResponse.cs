﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Payment.ApplePay.Response
{
    /// <summary>
    /// 验证购买信息
    /// https://developer.apple.com/library/archive/releasenotes/General/ValidateAppStoreReceipt/Chapters/ReceiptFields.html#//apple_ref/doc/uid/TP40010573-CH106-SW1
    /// </summary>
    public class AppleIapResponse
    {
        /// <summary>
        /// 状态码 
        /// </summary>
        [JsonProperty("status")]
        public int Status { get; set; }
        /// <summary>
        /// 有关收据中找到的密钥的信息
        /// </summary>
        [JsonProperty("receipt")]
        public Receipt Receipt { get; set; }
        /// <summary>
        /// 仅针对包含自动续订的收据返回
        /// </summary>
        [JsonProperty("latest_receipt_info")]
        public List<LatestReceiptInfoItem> LatestReceiptInfo { get; set; }
        /// <summary>
        /// 仅针对包含自动续订的收据返回
        /// </summary>
        [JsonProperty("latest_receipt")]
        public string LatestReceipt { get; set; }
        /// <summary>
        /// 仅针对包含自动续订的收据返回
        /// </summary>
        [JsonProperty("pending_renewal_info")]
        public List<PendingRenewalInfoItem> PendingRenewalInfo { get; set; }

        /// <summary>
        /// 环境
        /// </summary>
        [JsonProperty("environment")]
        public string Environment { get; set; }
    }

    /// <summary>
    /// 验证购买信息
    /// </summary>
    public class Receipt
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("receipt_type")]
        public string ReceiptType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("adam_id")]
        public long AdamId { get; set; }
        /// <summary>
        /// 应用程式编号
        /// </summary>
        [JsonProperty("app_item_id")]
        public long AppItemId { get; set; }
        /// <summary>
        /// 捆绑包标识符
        /// </summary>
        [JsonProperty("bundle_id")]
        public string BundleId { get; set; }
        /// <summary>
        /// 应用程式版本
        /// </summary>
        [JsonProperty("application_version")]
        public string ApplicationVersion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("download_id")]
        public long DownloadId { get; set; }
        /// <summary>
        /// 外部版本标识符
        /// </summary>
        [JsonProperty("version_external_identifier")]
        public long VersionExternalIdentifier { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("receipt_creation_date")]
        public string ReceiptCreationDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("receipt_creation_date_ms")]
        public string ReceiptCreationDateMs { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("receipt_creation_date_pst")]
        public string ReceiptCreationDatePst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("request_date")]
        public string RequestDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("request_date_ms")]
        public string RequestDateMs { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("request_date_pst")]
        public string RequestDatePst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_purchase_date")]
        public string OriginalPurchaseDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_purchase_date_ms")]
        public string OriginalPurchaseDateMs { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_purchase_date_pst")] 
        public string OriginalPurchaseDatePst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_application_version")]
        public string OriginalApplicationVersion { get; set; }
        /// <summary>
        /// 应用内购买的收据
        /// </summary>
        [JsonProperty("in_app")]
        public List<InApp> InApp { get; set; }
    }

    /// <summary>
    /// 应用内购买的收据
    /// </summary>
    public class InApp
    {
        /// <summary>
        /// 购买商品的数量
        /// </summary>
        [JsonProperty("quantity")]
        public string Quantity { get; set; }
        /// <summary>
        /// 交易的标识
        /// </summary>
        [JsonProperty("transaction_id")]
        public string TransactionId { get; set; }
        /// <summary>
        /// 商品的标识 
        /// </summary>
        [JsonProperty("product_id")]
        public string ProductId { get; set; }
        /// <summary>
        /// 原始交易ID
        /// </summary>
        [JsonProperty("original_transaction_id")]
        public string OriginalTransactionId { get; set; }
        /// <summary>
        /// 购买时间
        /// </summary>
        [JsonProperty("purchase_date")]
        public string PurchaseDate { get; set; }
        /// <summary>
        /// 购买时间毫秒
        /// </summary>
        [JsonProperty("purchase_date_ms")]
        public string PurchaseDateMs { get; set; }
        /// <summary>
        /// 太平洋标准时间
        /// </summary>
        [JsonProperty("purchase_date_pst")]
        public string PurchaseDatePst { get; set; }
        /// <summary>
        /// 原始购买时间
        /// </summary>
        [JsonProperty("original_purchase_date")]
        public string OriginalPurchaseDate { get; set; }
        /// <summary>
        /// 订阅到期日期
        /// </summary>
        [JsonProperty("expires_date")]
        public string ExpiresDate { get; set; }
        /// <summary>
        /// 毫秒
        /// </summary>
        [JsonProperty("original_purchase_date_ms")]
        public string OriginalPurchaseDateMs { get; set; }
        /// <summary>
        /// 订阅到期意向 “ 1”-客户取消订阅。
        /// </summary>
        [JsonProperty("expiration_intent")]
        public string ExpirationIntent { get; set; }
        /// <summary>
        /// 订阅重试标志
        /// </summary>
        [JsonProperty("is_in_billing_retry_period")]
        public string IsInBillingRetryPeriod { get; set; }


        /// <summary>
        /// 购买时间,太平洋标准时间
        /// </summary>
        [JsonProperty("original_purchase_date_pst")]
        public string OriginalPurchaseDatePst { get; set; }
        /// <summary>
        /// 订阅试用期
        /// </summary>
        [JsonProperty("is_trial_period")]
        public string IsTrialPeriod { get; set; }

        /// <summary>
        /// 订阅介绍价格期
        /// </summary>
        [JsonProperty("is_in_intro_offer_period")]
        public string IsInIntroOfferPeriod { get; set; }

        /// <summary>
        /// 取消日期
        /// </summary>
        [JsonProperty("cancel_date")]
        public string CancelDate { get; set; }

        /// <summary>
        /// 取消原因
        /// </summary>
        [JsonProperty("cancellation_reason")]
        public string CancellationReason { get; set; }

        /// <summary>
        /// 应用程式编号
        /// </summary>
        [JsonProperty("app_item_id")]
        public string AppItemId { get; set; }

        /// <summary>
        /// 外部版本标识符
        /// </summary>
        [JsonProperty("version_external_identifier")]
        public string VersionExternalIdentifier { get; set; }

        /// <summary>
        /// 网络订单订单项ID
        /// </summary>
        [JsonProperty("web_order_line_item_id")]
        public string WebOrderLineItemId { get; set; }

        /// <summary>
        /// 订阅自动续订状态
        /// </summary>
        [JsonProperty("auto_renew_status")]
        public string AutoRenewStatus { get; set; }

        /// <summary>
        /// 订阅自动续订首选项
        /// </summary>
        [JsonProperty("auto_renew_product_id")]
        public string AutoRenewProductId { get; set; }

        /// <summary>
        /// 订阅价格同意状态
        /// </summary>
        [JsonProperty("price_consent_status")]
        public string PriceConsentStatus { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class LatestReceiptInfoItem
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("quantity")]
        public string Quantity { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("product_id")]
        public string ProductId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("transaction_id")]
        public string TransactionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_transaction_id")]
        public string OriginalTransactionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("purchase_date")]
        public string PurchaseDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("purchase_date_ms")]
        public string PurchaseDateMs { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("purchase_date_pst")]
        public string PurchaseDatePst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_purchase_date")]
        public string OriginalPurchaseDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_purchase_date_ms")]
        public string OriginalPurchaseDateMs { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_purchase_date_pst")]
        public string OriginalPurchaseDatePst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("is_trial_period")]
        public string IsTrialPeriod { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PendingRenewalInfoItem
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("auto_renew_product_id")]
        public string AutoRenewProductId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_transaction_id")]
        public string OriginalTransactionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("product_id")]
        public string ProductId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("auto_renew_status")]
        public string AutoRenewStatus { get; set; }
    }

}
