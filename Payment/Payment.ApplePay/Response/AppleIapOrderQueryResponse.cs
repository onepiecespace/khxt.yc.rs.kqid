﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Payment.ApplePay.Response
{
    /// <summary>
    /// 查询订单支付情况
    /// </summary>
    public class AppleIapOrderQueryResponse
    {
        /// <summary>
        /// 苹果交易号
        /// </summary>
        public string PayNo { get; set; }

        /// <summary>
        /// 支付情况状态码 -1  获取苹果内购凭证异常 0 获取苹果内购凭证失败 1 获取苹果内购凭证成功 -2 未找到对应交易订单信息
        /// </summary>
        public string Code { get; set; } = "0";
        /// <summary>
        /// 是否沙盒支付
        /// </summary>
        public bool IsSandBox { get; set; } = false;
        /// <summary>
        /// 捆绑包标识符
        /// </summary>
        public string BundleId { get; set; }
        /// <summary>
        /// 应用程式版本
        /// </summary>
        public string ApplicationVersion { get; set; }
        /// <summary>
        /// 交易信息
        /// </summary>
        public string Transaction { get; set; }
    }
}
