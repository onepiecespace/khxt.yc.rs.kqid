﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Payment.ApplePay
{
    /// <summary>
    /// 
    /// </summary>
    public interface IApplePayNotifyClient
    {
        Task<T> ExecuteAsync<T>(HttpRequest request);
    }
}
