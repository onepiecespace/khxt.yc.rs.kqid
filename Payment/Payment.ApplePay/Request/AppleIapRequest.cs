﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Payment.ApplePay.Request
{
    /// <summary>
    /// 苹果支付验证数据
    /// </summary>
    [Serializable]
    public class AppleIapRequest
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonRequired]
        [JsonProperty("receipt-data")]
        public string ReceiptData { get; set; }
        /// <summary>
        /// 仅用于自动续订，获取方法见共享密钥附录
        /// </summary>
        [JsonProperty("password")]
        public string Password { get; set; }
        /// <summary>
        /// 仅用于自动续订或非续订订阅的iOS 7样式的应用收据
        /// </summary>
        [JsonProperty("exclude-old-transactions")]
        public bool ExcludeOldTransactions { get; set; }

        public override string ToString()
        {
            JsonSerializerSettings jsonSerializer = new JsonSerializerSettings
            {
                DefaultValueHandling = DefaultValueHandling.Ignore
            };
            return JsonConvert.SerializeObject(this, Formatting.Indented, jsonSerializer);
        }
    }
}
