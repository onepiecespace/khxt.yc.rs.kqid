﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Payment.ApplePay.Request;
using Payment.ApplePay.Response;

namespace Payment.ApplePay
{
    /// <summary>
    /// 苹果支付
    /// </summary>
    public interface IAppleIapSdkRequest
    {
        /// <summary>
        /// 普通支付
        /// </summary>
        /// <param name="applePayRequest"></param>
        /// <param name="options"></param>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        Task<AppleIapOrderQueryResponse> AppleIapVerify(AppleIapRequest applePayRequest, AppleIapOptions options, string transactionId);
        /// <summary>
        /// 自动续费
        /// </summary>
        /// <param name="applePayRequest"></param>
        /// <param name="options"></param>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        Task<AppleIapOrderQueryResponse> AppleIapRenewVerify(AppleIapRequest applePayRequest, AppleIapOptions options, string transactionId);

    }
}
