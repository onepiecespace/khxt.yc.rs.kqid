﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Payment.ApplePay.Request;
using Payment.ApplePay.Response;

namespace Payment.ApplePay
{
    /// <summary>
    /// 
    /// </summary>
    public class AppleIap : IAppleIapSdkRequest
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IHttpClientFactory _httpClientFactory;

        public AppleIap(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applePayRequest"></param>
        /// <param name="options"></param>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public async Task<AppleIapOrderQueryResponse> AppleIapVerify(AppleIapRequest applePayRequest, AppleIapOptions options, string transactionId)
        {
            try
            {
                var client = _httpClientFactory.CreateClient(nameof(AppleIapVerify));
                var stringContent = new StringContent(applePayRequest.ToString());
                var body = await client.PostAsync(options.VerifyReceiptUrl, stringContent);
                if (body.StatusCode != HttpStatusCode.OK)
                    throw new Exception("获取苹果内购凭证异常,普通支付", new Exception($"访问苹果服务器，返回错误状码{body.StatusCode}"));
                var resString = await body.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(resString))
                {
                    throw new Exception("获取苹果内购凭证异常,普通支付", new Exception("访问苹果服务器，返回空"));
                }
                var iapRes = JsonConvert.DeserializeObject<AppleIapResponse>(resString);
                if (iapRes.Status == 0)
                {
                    var inApps = iapRes.Receipt.InApp.Where(t => t.TransactionId.Equals(transactionId));
                    var enumerable = inApps.ToList();
                    return enumerable.Any()
                        ? new AppleIapOrderQueryResponse()
                        {
                            Code = "1",
                            PayNo = transactionId,
                            BundleId = iapRes.Receipt.BundleId,
                            ApplicationVersion = iapRes.Receipt.ApplicationVersion,
                            Transaction = JsonConvert.SerializeObject(iapRes.Receipt)
                        }
                        : new AppleIapOrderQueryResponse() { Code = "-2" };
                }
                if (iapRes.Status == 21007)
                {
                    var sandBody = await client.PostAsync(options.SandBoxVerifyReceiptUrl, stringContent);
                    if (sandBody.StatusCode != HttpStatusCode.OK)
                        throw new Exception("获取苹果内购凭证异常,普通支付", new Exception($"访问苹果沙盒服务器,返回错误状码{sandBody.StatusCode}"));
                    var resSandString = await sandBody.Content.ReadAsStringAsync();
                    if (string.IsNullOrEmpty(resString))
                    {
                        throw new Exception("获取苹果内购凭证异常,普通支付", new Exception("访问苹果沙盒服务器，返回空"));
                    }
                    var iapSandRes = JsonConvert.DeserializeObject<AppleIapResponse>(resSandString);
                    if (iapSandRes.Status == 0)
                    {
                        var inApps = iapSandRes.Receipt.InApp.Where(t => t.TransactionId.Equals(transactionId));
                        return inApps.Any()
                            ? new AppleIapOrderQueryResponse()
                            {
                                Code = "1",
                                IsSandBox = true,
                                PayNo = transactionId,
                                BundleId = iapSandRes.Receipt.BundleId,
                                ApplicationVersion = iapSandRes.Receipt.ApplicationVersion,
                                Transaction = JsonConvert.SerializeObject(iapSandRes.Receipt)
                            }
                            : new AppleIapOrderQueryResponse() { Code = "-2" };
                    }
                    else
                        return new AppleIapOrderQueryResponse() { Code = iapSandRes.Status.ToString() };
                }
                return new AppleIapOrderQueryResponse() { Code = iapRes.Status.ToString() };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw new Exception("获取苹果内购凭证异常,普通支付", ex);
            }
            finally
            {

            }
        }

        public async Task<AppleIapOrderQueryResponse> AppleIapRenewVerify(AppleIapRequest applePayRequest, AppleIapOptions options, string transactionId)
        {
            try
            {
                var client = _httpClientFactory.CreateClient(nameof(AppleIapVerify));
                var stringContent = new StringContent(applePayRequest.ToString());
                var body = await client.PostAsync(options.VerifyReceiptUrl, stringContent);
                if (body.StatusCode != HttpStatusCode.OK)
                    throw new Exception("获取苹果内购凭证异常,自动续费", new Exception($"访问苹果服务器，返回错误状码{body.StatusCode}"));
                var resString = await body.Content.ReadAsStringAsync();

                if (string.IsNullOrEmpty(resString))
                {
                    throw new Exception("获取苹果内购凭证异常,自动续费", new Exception("访问苹果服务器，返回空"));
                }

                var iapRes = JsonConvert.DeserializeObject<AppleIapResponse>(resString);
                if (iapRes.Status == 0)
                {
                    var inApps = iapRes.Receipt.InApp.Where(t => t.TransactionId.Equals(transactionId));
                    return inApps.Any()
                        ? new AppleIapOrderQueryResponse()
                        {
                            Code = "1",
                            PayNo = transactionId,
                            BundleId = iapRes.Receipt.BundleId,
                            ApplicationVersion = iapRes.Receipt.ApplicationVersion,
                            Transaction = JsonConvert.SerializeObject(iapRes.Receipt)
                        }
                        : new AppleIapOrderQueryResponse() { Code = "-2" };
                }
                if (iapRes.Status == 21007)
                {
                    var sandBody = await client.PostAsync(options.SandBoxVerifyReceiptUrl, stringContent);
                    if (sandBody.StatusCode != HttpStatusCode.OK)
                        throw new Exception("获取苹果内购凭证异常,自动续费", new Exception($"访问苹果沙盒服务器，返回错误状码{sandBody.StatusCode}"));
                    var resSandString = await sandBody.Content.ReadAsStringAsync();
                    if (string.IsNullOrEmpty(resSandString))
                    {
                        throw new Exception("获取苹果内购凭证异常,自动续费", new Exception("访问苹果沙盒服务器，返回空"));
                    }
                    var iapSandRes = JsonConvert.DeserializeObject<AppleIapResponse>(resSandString);
                    if (iapSandRes.Status == 0)
                    {
                        var inApps = iapSandRes.LatestReceiptInfo.Where(t => t.TransactionId.Equals(transactionId));
                        if (inApps.Any())
                        {
                            return new AppleIapOrderQueryResponse()
                            {
                                Code = "1",
                                IsSandBox = true,
                                PayNo = transactionId,
                                BundleId = iapSandRes.Receipt.BundleId,
                                ApplicationVersion = iapSandRes.Receipt.ApplicationVersion,
                                Transaction = JsonConvert.SerializeObject(iapSandRes.Receipt)
                            };
                        }
                        else
                        {
                            return new AppleIapOrderQueryResponse() { Code = "-2" };
                        }
                    }
                    else
                        return new AppleIapOrderQueryResponse() { Code = iapSandRes.Status.ToString() };
                }
                return new AppleIapOrderQueryResponse() { Code = iapRes.Status.ToString() };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw new Exception("获取苹果内购凭证异常,自动续费", ex);
            }
        }
    }
}
