﻿using System;

namespace Oss.Ali
{
    /// <summary>
    /// OSS 配置
    /// </summary>
    public class OssOptions
    {
        /// <summary>
        /// 访问域名 
        /// </summary>
        public string EndPoint { get; set; }
        /// <summary>
        /// Bucket
        /// </summary>
        public string Bucket { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AccessKeyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AccessKeySecret { get; set; }
        /// <summary>
        /// 存储空间名称
        /// </summary>
        public string BucketName { get; set; }
    }
}
