﻿using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http.Features;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aliyun.OSS;
using Aliyun.OSS.Common;
using Microsoft.Extensions.Options;

namespace Oss.Ali
{
    public class OssService : IOssService
    {
        private readonly string _bucketName;
        private readonly OssClient _client;

        public OssService(OssOptions optionsAccessor)
        {
            var endpoint = optionsAccessor.EndPoint;
            var accessKeyId = optionsAccessor.AccessKeyId;
            var accessKeySecret = optionsAccessor.AccessKeySecret;
            _bucketName = optionsAccessor.BucketName;
            _client = new OssClient(endpoint, accessKeyId, accessKeySecret);
        }
        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="folderPath"></param>
        public void CreateEmptyFolder(string folderPath)
        {
            try
            {
                // put object with zero bytes stream.
                using (var memStream = new MemoryStream())
                {
                    _client.PutObject(_bucketName, folderPath, memStream);
                    Console.WriteLine("创建文件夹:{0} 成功", folderPath);
                }
            }
            catch (OssException ex)
            {
                Console.WriteLine("创建文件夹失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("上传文件失败: {0}", ex.Message);
                throw;
            }
        }


        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="folderPath">image/1.png 或者 1.png</param>
        /// <param name="filePath">完整文件路径</param>
        public void PutObjectFromFile(string folderPath, string filePath)
        {
            try
            {
                _client.PutObject(_bucketName, folderPath, filePath);
                Console.WriteLine("上传文件:{0} 成功", folderPath);
            }
            catch (OssException ex)
            {
                Console.WriteLine("上传文件失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("上传文件失败: {0}", ex.Message);
                throw;
            }

        }
        /// <summary>
        /// 上传文件 流方式
        /// </summary>
        /// <param name="folderPath">image/1.png 或者 1.png</param>
        /// <param name="stream">文件流</param>
        public void PutObjectFromFile(string folderPath, Stream stream)
        {
            try
            {
                _client.PutObject(_bucketName, folderPath, stream);
                Console.WriteLine("上传文件:{0} 成功", folderPath);
            }
            catch (OssException ex)
            {
                Console.WriteLine("上传文件失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("上传文件失败: {0}", ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromPath"></param>
        /// <param name="toPath"></param>
        public void MoveFileAndDel(string fromPath, string toPath)
        {
            try
            {
                var metadata = new ObjectMetadata();
                metadata.AddHeader("mk1", "mv1");
                metadata.AddHeader("mk2", "mv2");
                var req = new CopyObjectRequest(_bucketName, fromPath, _bucketName, toPath)
                {
                    NewObjectMetadata = metadata
                };
                _client.CopyObject(req);
                DeleteFile(fromPath);
                Console.WriteLine("移动文件成功");
            }
            catch (OssException ex)
            {
                Console.WriteLine("移动文件失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("移动文件失败，错误信息: {0}", ex.Message);
                throw;
            }
        }

        public void DeleteFile(string fileName)
        {
            try
            {
                _client.DeleteObject(_bucketName, fileName);
                Console.WriteLine("删除文件成功");
            }
            catch (OssException ex)
            {
                Console.WriteLine("删除文件失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("删除文件失败，错误信息: {0}", ex.Message);
                throw;
            }
        }

        public bool FileExist(string filePath)
        {
            try
            {
                return _client.DoesObjectExist(_bucketName, filePath);
            }
            catch (OssException ex)
            {
                Console.WriteLine("获取文件是否存在失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("获取文件是否存在失败，错误信息: {0}", ex.Message);
                throw;
            }
        }

        public OssObject GetObject(string filePath)
        {
            try
            {
                return _client.GetObject(_bucketName, filePath);
            }
            catch (OssException ex)
            {
                Console.WriteLine("获取文件信息失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("获取文件信息失败，错误信息: {0}", ex.Message);
                throw;
            }
        }
    }
}
