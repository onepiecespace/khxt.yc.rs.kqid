﻿using System;
using Oss.Ali;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static void AddAliOss(this IServiceCollection services)
        {
            services.AddAliOss(null);
        }
        public static void AddAliOss(this IServiceCollection services, Action<OssOptions> setupAction)
        {
            services.AddSingleton<IOssService, OssService>();

            if (setupAction != null)
            {
                services.Configure(setupAction);
            }
        }
    }
}
