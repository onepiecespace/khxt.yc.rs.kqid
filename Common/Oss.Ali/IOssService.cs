﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Aliyun.OSS;

namespace Oss.Ali
{
    public interface IOssService
    {
        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="folderPath"></param>
        void CreateEmptyFolder(string folderPath);
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="folderPath">image/1.png 或者 1.png</param>
        /// <param name="filePath">完整文件路径</param>
        void PutObjectFromFile(string folderPath, string filePath);
        /// <summary>
        /// 上传文件 流方式
        /// </summary>
        /// <param name="folderPath">image/1.png 或者 1.png</param>
        /// <param name="stream">文件流</param>
        void PutObjectFromFile(string folderPath, Stream stream);
        /// <summary>
        /// 移动文件并删除
        /// </summary>
        /// <param name="fromPath"></param>
        /// <param name="toPath"></param>
        void MoveFileAndDel(string fromPath, string toPath);
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="fileName"></param>
        void DeleteFile(string fileName);
        /// <summary>
        /// 获取文件是否存在
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        bool FileExist(string filePath);
        /// <summary>
        /// 获取文件信息
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        OssObject GetObject(string filePath);

    }
}
