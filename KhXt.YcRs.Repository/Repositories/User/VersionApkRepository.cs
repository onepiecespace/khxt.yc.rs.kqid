﻿using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Repositories.User;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.User
{
    public class VersionApkRepository : DapperRepositoryBase<VersionApkEntity, long>, IVersionApkRepository
    {
        public VersionApkRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }







}
