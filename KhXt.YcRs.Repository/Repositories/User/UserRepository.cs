﻿using Dapper;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Repositories.User;
using Hx.Dapper.Repository;
using Hx.Domain.Services;
using Hx.Domain.Uow;
using System.Collections.Generic;
using System.Linq;

namespace KhXt.YcRs.Repository.User
{
    public class UserRepository : DapperRepositoryBase<UserEntity, long>, IUserRepository
    {
        public UserRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
        //public UserEntity GetByPhone(string  phone)
        //{
        //    var sql = $"select * from a_User where phone='{phone}' ";
        //    var body = Connection.QueryFirstOrDefault<UserEntity>(sql, null, ActiveTransaction);
        //    return body;
        //}

        public List<UserInfoOfMsg> GetListOfMsg(string datetime)
        {

            var sql = "SELECT distinct us.Id,us.username,us.phone,co.CourseName from a_Course_Buy_Collect as byc LEFT JOIN a_User as us on byc.userId=us.Id LEFT JOIN a_Course as co on byc.CourseId=co.Id  left JOIN  a_Course_Child ch on co.id=ch.ParentId  where  byc.IsBuy=1 and byc.ExpirationDate>'" + datetime + "'and ch.ChildStartTime = '" + datetime + "'";
            var rlt = Connection.Query<UserInfoOfMsg>(sql);
            return rlt.ToList();
        }

    }
}
