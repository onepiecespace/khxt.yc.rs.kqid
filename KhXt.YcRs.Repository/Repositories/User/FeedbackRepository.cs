﻿using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Repositories.User;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.User
{
    /// <summary>
    ///  Feedback  FeedbackEntity FeedbackRepository
    /// </summary>
    public class FeedbackRepository : DapperRepositoryBase<FeedbackEntity, long>, IFeedbackRepository
    {
        public FeedbackRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }
}
