﻿using Dapper;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Repositories.User;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.User
{
    public class UserThreePartyRepository : DapperRepositoryBase<UserThreePartyEntity, long>, IUserThreePartyRepository
    {
        public UserThreePartyRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
