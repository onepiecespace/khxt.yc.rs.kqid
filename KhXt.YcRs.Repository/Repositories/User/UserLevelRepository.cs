﻿using Dapper;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Repositories.User;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;
using System.Threading.Tasks;

namespace KhXt.YcRs.Repository.User
{
    public class UserLevelRepository : DapperRepositoryBase<UserLeveEntity, long>, IUserLevelRepository
    {
        public UserLevelRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
        //public UserEntity GetByPhone(string  phone)
        //{
        //    var sql = $"select * from a_User where phone='{phone}' ";
        //    var body = Connection.QueryFirstOrDefault<UserEntity>(sql, null, ActiveTransaction);
        //    return body;
        //}
    }
}
