﻿using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Repositories.User;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.User
{
    public class UserValueLogRepository : DapperRepositoryBase<UserValueLogEntity, long>, IUserValueLogRepository
    {
        public UserValueLogRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
        //public UserEntity GetByPhone(string  phone)
        //{
        //    var sql = $"select * from a_User where phone='{phone}' ";
        //    var body = Connection.QueryFirstOrDefault<UserEntity>(sql, null, ActiveTransaction);
        //    return body;
        //}
    }
}
