﻿using Dapper;
using KhXt.YcRs.Domain.Entities;
using KhXt.YcRs.Domain.Repositories;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;
using Hx.MongoDb.Repository;
using Hx.MongoDb.Uow;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Repositories
{
    public class TestRepository : DapperRepositoryBase<TestEntity, long>, ITestRepository
    {
        public TestRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }

        public async Task<string> GetBody(long id)
        {
            var sql = $"select body from a_test where id={id}";
            var body = Connection.ExecuteScalar<string>(sql, null, ActiveTransaction);
            return await Task.FromResult(body);
        }
    }

    public class MongoTestRepository : MongoDbRepositoryBase<PersonEntity>, IMongoTestRepository
    {
        public MongoTestRepository(IMongoDbProvider databaseProvider) : base(databaseProvider)
        {
        }
    }
}
