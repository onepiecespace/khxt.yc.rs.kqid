﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Wallet;
using KhXt.YcRs.Domain.Repositories.Wallet;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Wallet
{
    public class WalletRepository : DapperRepositoryBase<WalletEntity, long>, IWalletRepository
    {
        public WalletRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
