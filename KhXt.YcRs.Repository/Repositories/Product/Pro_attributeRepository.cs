﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Repositories.Product;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Product
{
    public class Pro_attributeRepository : DapperRepositoryBase<Pro_attributeEntity, long>, IPro_attributeRepository
    {
        public Pro_attributeRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
