﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Repositories.Product;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Product
{
    public class Pro_listRepository : DapperRepositoryBase<Pro_listEntity, long>, IPro_listRepository
    {
        public Pro_listRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
