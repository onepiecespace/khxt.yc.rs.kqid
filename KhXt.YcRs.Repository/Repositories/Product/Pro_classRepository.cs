﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Repositories.Product;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Product
{
    public class Pro_classRepository : DapperRepositoryBase<Pro_classEntity, long>, IPro_classRepository
    {
        public Pro_classRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
