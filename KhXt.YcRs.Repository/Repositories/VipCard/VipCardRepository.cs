﻿using KhXt.YcRs.Domain.Entities.VipCard;
using KhXt.YcRs.Domain.Repositories.VipCard;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Repository.VipCard
{
    public class VipCardRepository : DapperRepositoryBase<VipCardEntity, string>, IVipCardRepository
    {
        public VipCardRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {

        }
    }
}
