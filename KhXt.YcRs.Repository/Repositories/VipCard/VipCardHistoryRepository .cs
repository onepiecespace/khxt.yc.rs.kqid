﻿using KhXt.YcRs.Domain.Entities.VipCard;
using KhXt.YcRs.Domain.Repositories.VipCard;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Repository.VipCard
{
    public class VipCardHistoryRepository : DapperRepositoryBase<VipCardHistoryEntity, string>, IVipCardHistoryRepository
    {
        public VipCardHistoryRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }
}
