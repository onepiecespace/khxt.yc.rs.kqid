﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Repositories.Order;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Order
{
    public class OrderShippingRepository : DapperRepositoryBase<OrderShippingEntity, long>, IOrderShippingRepository
    {
        public OrderShippingRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
