﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Repositories.Order;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Order
{
    public class OrderItemRepository : DapperRepositoryBase<OrderItemEntity, long>, IOrderItemRepository
    {
        public OrderItemRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
