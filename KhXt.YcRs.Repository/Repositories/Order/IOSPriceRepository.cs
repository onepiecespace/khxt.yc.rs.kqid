﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Repositories.Order;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Order
{
    public class IOSPriceRepository : DapperRepositoryBase<IOSPriceEntity, long>, IIOSPriceRepository
    {
        public IOSPriceRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
