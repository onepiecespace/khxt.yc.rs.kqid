﻿using KhXt.YcRs.Domain.Entities.Log;
using KhXt.YcRs.Domain.Repositories.Log;
using Hx.MongoDb.Repository;
using Hx.MongoDb.Uow;

namespace KhXt.YcRs.Repository.Log
{
    public class LoginLogRepository : MongoDbRepositoryBase<LoginLogEntity, string>, ILoginLogRepository
    {
        public LoginLogRepository(IMongoDbProvider databaseProvider) : base(databaseProvider)
        {

        }

        public void Test()
        {
            //this.Collection.Aggregate()
        }
    }
}
