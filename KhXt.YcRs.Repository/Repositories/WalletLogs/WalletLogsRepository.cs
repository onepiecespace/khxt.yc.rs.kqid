﻿using Dapper;
using KhXt.YcRs.Domain.Entities.WalletLogs;
using KhXt.YcRs.Domain.Repositories.WalletLogs;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.WalletLogs
{
    public class WalletLogsRepository : DapperRepositoryBase<WalletLogsEntity, long>, IWalletLogsRepository
    {
        public WalletLogsRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
