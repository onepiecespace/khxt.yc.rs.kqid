﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Manager;
using KhXt.YcRs.Domain.Repositories.Manager;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Manager
{
    public class ManagerRepository : DapperRepositoryBase<ManagerEntity, long>, IManagerRepository
    {
        public ManagerRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
