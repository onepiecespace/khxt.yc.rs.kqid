﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Repositories.Course;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Course
{
    public class CourseUserItemRepository : DapperRepositoryBase<CourseUserItemEntity, long>, ICourseUserItemRepository
    {
        public CourseUserItemRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
