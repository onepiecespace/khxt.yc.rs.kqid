﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Repositories.Course;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Course
{
    public class CourseItemRepository : DapperRepositoryBase<CourseItemEntity, long>, ICourseItemRepository
    {
        public CourseItemRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
