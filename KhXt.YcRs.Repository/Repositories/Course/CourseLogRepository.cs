﻿using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.MongoLogEntity;
using KhXt.YcRs.Domain.Repositories.Course;
using Hx.MongoDb.Repository;
using Hx.MongoDb.Uow;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Repository.Course
{
    public class CourseLogRepository : MongoDbRepositoryBase<CourseLogEntity>, ICourseLogRepository
    {
        public CourseLogRepository(IMongoDbProvider databaseProvider) : base(databaseProvider)
        {
        }
    }

    public class OperationLogRepository : MongoDbRepositoryBase<OperationLogEntity, string>, IOperationLogRepository
    {
        public OperationLogRepository(IMongoDbProvider databaseProvider) : base(databaseProvider)
        {
        }
    }
}
