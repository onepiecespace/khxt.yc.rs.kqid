﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Repositories.Course;
using Hx.Dapper.Repository;
using Hx.Domain.Services;
using Hx.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Repository.Course
{
    public class CourseRepository : DapperRepositoryBase<CourseEntity, long>, ICourseRepository
    {
        public CourseRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }

        public IEnumerable<OrderListOfCMS> GetOrderList()
        {
            var sql = "SELECT a.Id,OrderNo,UserId,CourseId,PayPrice,OrderState,Ispay,PayTime,PayType,PayNo,IsSend,SendTime,IsGet,GetTime,a.CreateTime,a.UpdateTime,CourseName,Price,c.UserName from a_Course_Order a left join a_Course b on a.CourseId=b.Id left JOIN a_User c on a.UserId=c.Id";
            var rlt = Connection.Query<OrderListOfCMS>(sql);
            return rlt;
        }
        public IEnumerable<UserliveModel> GetLiveUserList(int CourseId)
        {
            var sql = "SELECT distinct us.Id as userid,us.username,us.phone,us.AccountType FROM a_User as us left join  a_Course_Buy_Collect as byc  on us.Id = byc.UserId WHERE byc.IsBuy = 1 and byc.CourseId =" + CourseId + "";
            var rlt = Connection.Query<UserliveModel>(sql);
            return rlt;
        }

        public IEnumerable<CourseCMS> GetCourseChildList(Expression<Func<CourseCMS, bool>> predicate)
        {
            return Table().Where(predicate);
        }


        public IQueryable<CourseCMS> Table()
        {

            var sql = "select * from a_Course_Child a LEFT JOIN a_Course b on a.ParentId=b.id  left JOIN a_Course_Cateory c on b.Cateorys=c.id";
            var rlt = Connection.Query<CourseCMS>(sql);
            return rlt.AsQueryable();
        }
    }

    public class CourseAppoRepository : DapperRepositoryBase<CourseAppointmentEntity, long>, ICourseAppoRepository
    {
        public CourseAppoRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }
}
