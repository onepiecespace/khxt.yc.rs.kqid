﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Repositories.Vip;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Vip
{
    public class VipPrivilegeRepository : DapperRepositoryBase<VipPrivilegeEntity, long>, IVipPrivilegeRepository
    {
        public VipPrivilegeRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
