﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Repositories.Vip;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Vip
{
    public class UserVipRepository : DapperRepositoryBase<UserVipEntity, long>, IUserVipRepository
    {
        public UserVipRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
