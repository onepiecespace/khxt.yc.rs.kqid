﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Repositories.Vip;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Vip
{
    public class VipInfoRepository : DapperRepositoryBase<VipInfoEntity, long>, IVipInfoRepository
    {
        public VipInfoRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
