﻿using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Repositories.Sys;
using Hx.MongoDb.Repository;
using Hx.MongoDb.Uow;

namespace KhXt.YcRs.Repository.Sys
{
    /// <summary>
    /// 
    /// </summary>
    public class SysLogRepository : MongoDbRepositoryBase<SysLogEntity>, ISysLogRepository
    {
        public SysLogRepository(IMongoDbProvider databaseProvider) : base(databaseProvider)
        {
        }


    }
}
