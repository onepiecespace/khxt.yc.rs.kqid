﻿using Dapper;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Repositories.Sys;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Sys
{
    public class User_ChannelRepository : DapperRepositoryBase<User_ChannelEntity, long>, IUser_ChannelRepository
    {
        public User_ChannelRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }
    }


}
