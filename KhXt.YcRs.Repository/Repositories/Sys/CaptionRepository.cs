﻿using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Repositories.Sys;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Sys
{
    public class CaptionRepository : DapperRepositoryBase<CaptionEntity, long>, ICaptionRepository
    {
        public CaptionRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }


    }

}
