﻿using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Repositories.Sys;
using Hx.Dapper.Repository;
using Hx.Domain.Uow;

namespace KhXt.YcRs.Repository.Sys
{
    public class ContentRepository : DapperRepositoryBase<ContentEntity, long>, IContentRepository
    {
        public ContentRepository(ICurrentUnitOfWorkProvider currentUnitOfWorkProvider) : base(currentUnitOfWorkProvider)
        {
        }


    }

}
