﻿using KhXt.YcRs.Domain;
using Hx.Components;
using Hx.Modules;
using System.Reflection;
using System.Collections.Generic;
using KhXt.YcRs.Domain.TypeHandlers;
using Hx.MQ;
using Microsoft.Extensions.Configuration;
using Payment.ApplePay;
using KhXt.YcRs.Domain.Services.Files;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using Hx.Logging;
using Hx.Serializing;
using Essensoft.AspNetCore.Payment.Alipay;
using Essensoft.AspNetCore.Payment.WeChatPay;
using KhXt.YcRs.Domain.SmsOptions;
using KhXt.YcRs.HttpApi.Test;

namespace KhXt.YcRs.HttpApi
{
    [DependsOn(
        typeof(DomainModule)
        //typeof(MqModule)
     )]
    public class ApiModule : ModuleBase
    {
        public override void PreInitialize()
        {
            Register<ISomeUtil, SomeUtil>(null, LifeStyle.Transient);
            //Register<ITextCompare, TextCompare>(null, LifeStyle.Transient);
            Register<IAppleIapSdkRequest, AppleIap>(null, LifeStyle.Transient);
            Register<IHttpContextAccessor, HttpContextAccessor>(null, LifeStyle.Singleton);
            //Register<IRateLimitConfiguration, RateLimitConfiguration>(null, LifeStyle.Singleton);
            //Register<IIpPolicyStore, MemoryCacheIpPolicyStore>(null, LifeStyle.Singleton);
            //Register<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>(null, LifeStyle.Singleton);
        }

        public override void Initialize()
        {
            var cfg = Resolve<IConfiguration>();
            //var hxsetting = cfg.GetSection("HuanXinSettings").Get<HuanXinSetting>();
            //ObjectContainer.RegisterInstance(hxsetting);
            var aliPayConfig = cfg.GetSection("Alipay").Get<AlipayOptions>();
            ObjectContainer.RegisterInstance(aliPayConfig);

            var wxPayConfig = cfg.GetSection("WeChatPay").Get<WeChatPayOptions>();
            ObjectContainer.RegisterInstance(wxPayConfig);

            var applePayConfig = cfg.GetSection("ApplePay").Get<AppleIapOptions>();
            ObjectContainer.RegisterInstance(applePayConfig);

            var aliOssConfig = cfg.GetSection("AliOssConfig").Get<OssOptions>();
            ObjectContainer.RegisterInstance(aliOssConfig);

            var ShanYanSetting = cfg.GetSection("ShanYanSetting").Get<ShanYanSetting>();
            ObjectContainer.RegisterInstance(ShanYanSetting);

            var JPushSetting = cfg.GetSection("JPushSetting").Get<JPushSetting>();
            ObjectContainer.RegisterInstance(JPushSetting);

            var smsConfig = cfg.GetSection("SmsSettings").Get<SmsSettings>();
            ObjectContainer.RegisterInstance(smsConfig);

            HttpClientHelper.Serializer = Resolve<ISerializer>();
            HttpClientHelper.HttpClientFactory = Resolve<IHttpClientFactory>();
            HttpClientHelper.Logger = Resolve<ILoggerFactory>().Create("httpclient");
        }

  
        public override Assembly[] GetAdditionalAssemblies()
        {
            var assemblies = new List<Assembly>();
            assemblies.Add(typeof(DictionaryTypeHandler).Assembly);
            return assemblies.ToArray();
        }


    }
}
