﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.ApiGroup
{
    /// <summary>
    /// 系统分组枚举值
    /// </summary>
    [Flags]
    public enum ApiGroupNames
    {
        [GroupInfo(Title = "App全部接口模块", Description = "全部模块接口v1", Version = "v1")]
        All,
        [GroupInfo(Title = "登录认证", Description = "登录认证相关接口", Version = "v1")]
        Auth = 1,
        [GroupInfo(Title = "课程模块", Description = "课程相关接口", Version = "v1")]
        Course = 4,
        [GroupInfo(Title = "支付模块", Description = "支付模块相关接口", Version = "v1")]
        Pay = 16,
        [GroupInfo(Title = "VIP卡模块", Description = "VIP卡相关接口", Version = "v1")]
        VIP = 512,
        [GroupInfo(Title = "Test模块", Description = "Test相关接口v1", Version = "v1")]
        Test = 4096,
        [GroupInfo(Title = "订单模块", Description = "订单模块相关接口", Version = "v1")]
        Order = 32768,
        [GroupInfo(Title = "系统管理模块", Description = "系统管理相关接口", Version = "v1")]
        Sys = 262144,
        [GroupInfo(Title = "钱包模块", Description = "钱包模块", Version = "v1")]
        Wallet,
        [GroupInfo(Title = "极光推送模块", Description = "钱包模块", Version = "v1")]
        JPush,
    }
}
