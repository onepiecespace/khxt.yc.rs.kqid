﻿using Hx.Extensions;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.ApiGroup
{
    /// <summary>
    /// 系统分组特性
    /// </summary>
    public class ApiGroupAttribute : Attribute, IApiDescriptionGroupNameProvider
    {
        public ApiGroupAttribute(params ApiGroupNames[] names)
        {
            GroupName = names.Select(p => p.ToString()).JoinAsString(",");
        }
        public string GroupName { get; set; }
    }
}
