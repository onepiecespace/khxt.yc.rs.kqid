﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using KhXt.YcRs.Domain;
using Hx.Extensions;
using Hx.Utilities;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace KhXt.YcRs.HttpApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var parser = CommandLineArgumentParser.Parse(args);
            var p = parser.Get("--urls");
            var urls = p == null ? "http://*:5516" : p.Next;
            CreateWebHostBuilder(args).UseUrls(urls).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) => WebHost
                    .CreateDefaultBuilder(args)
                   .ConfigureLogging(logger =>
                   {
                       logger.AddFilter("System", LogLevel.Warning);
                       logger.AddFilter("Microsoft", LogLevel.Warning);
                       logger.AddLog4Net();
                   })
                    .UseStartup<Startup>();



        internal static class GlobalStatus
        {
            private static long _index;

            public static long NextIndex()
            {
                return Interlocked.Increment(ref _index);
            }

            public static long Index { get; } = _index;
        }
    }


}
