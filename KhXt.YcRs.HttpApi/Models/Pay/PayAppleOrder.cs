﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.Pay
{
    public class In_appItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string quantity { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string product_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string transaction_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string original_transaction_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string purchase_date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string purchase_date_ms { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string purchase_date_pst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string original_purchase_date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string original_purchase_date_ms { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string original_purchase_date_pst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string is_trial_period { get; set; }
    }

    public class Receipt
    {
        /// <summary>
        /// 
        /// </summary>
        public string receipt_type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long adam_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long app_item_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string bundle_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string application_version { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long download_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long version_external_identifier { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string receipt_creation_date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string receipt_creation_date_ms { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string receipt_creation_date_pst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string request_date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string request_date_ms { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string request_date_pst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string original_purchase_date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string original_purchase_date_ms { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string original_purchase_date_pst { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string original_application_version { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<In_appItem> in_app { get; set; }
    }

    public class PayAppleOrder
    {
        /// <summary>
        /// 
        /// </summary>
        public Receipt receipt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string environment { get; set; }
    }
}
