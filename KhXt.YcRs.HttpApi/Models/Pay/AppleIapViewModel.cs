﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.Pay
{
    /// <summary>
    /// 
    /// </summary>
    public class AppleIapViewModel
    {
        /// <summary>
        /// 产品标识符
        /// </summary>
        [Required]
        [Display(Name = "productIdentifier")]
        public string ProductIdentifier { get; set; }
        /// <summary>
        /// 交易状态
        /// </summary>
        [Required]
        [Display(Name = "state")]
        public string State { get; set; }
        /// <summary>
        /// Receipt
        /// </summary>
        [Required]
        [Display(Name = "receipt")]
        public string Receipt { get; set; }
        /// <summary>
        /// 交易标识符
        /// </summary>
        [Required]
        [Display(Name = "transactionIdentifier")]
        public string TransactionIdentifier { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        [Required]
        [Display(Name = "OrderNo")]
        public string OrderNo { get; set; }
        /// <summary>
        /// 业务模块类型定义
        /// </summary>
        [Required]
        [Display(Name = "BusinessType")]
        public int BusinessType { get; set; }
    }
}
