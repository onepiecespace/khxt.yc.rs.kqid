﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.Course
{
    /// <summary>
    /// 
    /// </summary>
    public class CourseUserItemViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int CourseChildId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<CourseUserItemAddViewModel> AddViewModelList { get; set; }
    }
}
