﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Course;

namespace KhXt.YcRs.HttpApi.Models.Course
{
    public class OrderResult : Result<OrderListInfo>
    {

    }

    public class CourseItemResult : Result<CourseItemListInfo>
    {

    }
    public class CourseItemErrResult : Result<CourseItemErrListInfo>
    {

    }
    public class CourseItemDoneResult : Result<CourseItemDoneListInfo>
    {

    }
    public class CourseItemCountResult : Result<CourseItemCountListInfo>
    {

    }

    public class CourseUserItemResultViewModel : Result<CourseUserWorkResult>
    {

    }
}