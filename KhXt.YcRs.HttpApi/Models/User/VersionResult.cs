﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models;

namespace HuangLiCollege.Models
{
    #region 检查更新、清楚用户缓存日志

    public class VersionResult : Result<VersionInfo>
    {

    }
    public class VersionApkResult : Result<VersionApkInfo>
    {

    }
    #endregion
}
