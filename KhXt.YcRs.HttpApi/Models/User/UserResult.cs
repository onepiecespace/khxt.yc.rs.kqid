﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.User;
using System.Collections.Generic;

namespace KhXt.YcRs.HttpApi.Models.User
{
    public class UserResult : Result<UserBaseInfo>
    {

    }
    public class UserLiveResult
    {
        public List<string> phones { get; set; }
        public List<UserliveModel> UserliveList { get; set; }

    }


}
