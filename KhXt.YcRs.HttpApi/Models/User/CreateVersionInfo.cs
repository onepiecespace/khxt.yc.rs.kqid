﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.User
{
    public class CreateVersionInfo
    {
        /// <summary>
        /// 代理商标识
        /// </summary>
        public string Aid { get; set; }
        /// <summary>
        /// 设备类型 0 安卓 1 苹果
        /// </summary>
        public int OsSign { get; set; }
        /// <summary>
        /// 版本号 例如 1.0.0 （ios是数字）
        /// </summary>
        public string VersionCode { get; set; }
        /// <summary>
        /// 当前最新版本号（安卓有效 ios 也可以和VersionCode一样的设置数字）
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 版本描述信息
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 下载地址
        /// </summary>
        public string ApkUrl { get; set; }

        /// <summary>
        /// 最小支持版本
        /// </summary>
        public int MinVersion { get; set; }
        /// <summary>
        /// 是否有更新
        /// </summary>
        public int IsUpdate { get; set; }
        /// <summary>
        /// 是否强制更新
        /// </summary>
        public int ForceUpdate { get; set; }

    }
}
