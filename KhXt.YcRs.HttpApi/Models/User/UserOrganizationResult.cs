﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.User;
using System.Collections.Generic;

namespace KhXt.YcRs.HttpApi.Models.User
{
    public class UserOrgApplyResult
    {
        ///<Summary>
        /// 租户加盟商id
        ///</Summary>
        public long tenantId { get; set; }
        /// <summary>
        /// 返回的二维码调用地址 调用接口/api/qrcode 传入此字段动态获取二维码
        /// </summary>
        //public string qrcodeurl { get; set; }
        public UserOrganizationInfo orginfo { get; set; }
    }

}
