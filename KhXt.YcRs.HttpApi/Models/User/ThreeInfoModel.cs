﻿using KhXt.YcRs.Domain;

namespace HuangLiCollege.Models
{
    public class LoginWith2faViewModel
    {
        /// <summary>
        /// 登录方式 1 phone 2 wx 4 qq 8 guest  bbk = 16, dsl = 32,apple=64
        /// </summary>
        public LoginType LogingType { get; set; }

        public string UnionId { get; set; }

        public string OpenId { get; set; }

        public string NickName { get; set; }

        public string Avatar { get; set; }

        public Sex Sex { get; set; }
    }


}
