﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.User
{
    public class UpdateVersionInfo
    {
        /// <summary>
        /// 版本的主键ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 代理商标识
        /// </summary>
        public string Aid { get; set; }
        /// <summary>
        /// 设备类型 0 安卓 1 苹果
        /// </summary>
        public int OsSign { get; set; }
        /// <summary>
        /// 版本号 例如 1.0.0 （ios是数字）
        /// </summary>
        public string VersionCode { get; set; }
        /// <summary>
        /// 当前最新版本号（安卓有效 ios 也可以和VersionCode一样的设置数字）
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 版本描述信息
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 下载地址 (安卓必填注意不带域名 格式dl/lianggehuangli_1.2.1.3.apk)
        /// </summary>
        public string ApkUrl { get; set; }

        /// <summary>
        /// 最小支持版本（根据当前版本对比MinVersion 判断是否强制个更新）
        /// </summary>
        public int MinVersion { get; set; }
        /// <summary>
        /// 是否有更新 1 是更新 0 否未更新
        /// </summary>
        public int IsUpdate { get; set; }
        /// <summary>
        /// 是否强制更新 
        /// </summary>
        public int ForceUpdate { get; set; }

    }
}
