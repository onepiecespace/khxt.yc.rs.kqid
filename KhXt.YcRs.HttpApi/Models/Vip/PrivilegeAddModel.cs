﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.Vip
{
    /// <summary>
    /// 会员特权添加
    /// </summary>
    public class PrivilegeAddModel
    {
        /// <summary>
        /// Vip
        /// </summary>
        public long VipId { get; set; }

        /// <summary>
        /// 是否可用
        /// </summary>
        public bool IsEnable { get; set; } = true;
        /// <summary>
        /// 特权名称
        /// </summary>
        public string PrivilegeName { get; set; }

        /// <summary>
        /// 特权标题
        /// </summary>
        public string PrivilegeTitle { get; set; }

        /// <summary>
        /// 特权图片
        /// </summary>
        public string PrivilegeImg { get; set; }

        /// <summary>
        /// 链接地址
        /// </summary>
        public string PrivilegeUrl { get; set; }

        /// <summary>
        /// 特权介绍
        /// </summary>
        public string PrivilegeContent { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public long Updater { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
