﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.Vip
{
    public class VipInfoAddModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 原价格
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// 自动续费价格
        /// </summary>
        public float RenewPrice { get; set; }

        /// <summary>
        /// 销售价格
        /// </summary>
        public float SalePrice { get; set; }

        /// <summary>
        /// 有效天数
        /// </summary>
        public int ExpiredDays { get; set; }

        /// <summary>
        /// 特权集合
        /// </summary>
        public string Privileges { get; set; }

        /// <summary>
        /// 优惠券信息
        /// </summary>
        public string Coupons { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public long Updater { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; } = DateTime.Now;
    }
}
