﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.Wallet
{
    /// <summary>
    /// 
    /// </summary>
    public class WalletPay
    {
        /// <summary>
        /// 产品标识符
        /// </summary>
        [Required]
        [Display(Name = "productIdentifier")]
        public string ProductIdentifier { get; set; }
        /// <summary>
        /// 交易状态
        /// </summary>
        [Required]
        [Display(Name = "state")]
        public string State { get; set; }
        /// <summary>
        /// Receipt
        /// </summary>
        [Required]
        [Display(Name = "receipt")]
        public string Receipt { get; set; }
        /// <summary>
        /// 交易标识符
        /// </summary>
        [Required]
        [Display(Name = "transactionIdentifier")]
        public string TransactionIdentifier { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 本次充值金币数
        /// </summary>
        public float ChargeCoin { get; set; }
        /// <summary>
        /// 充值描述
        /// </summary>
        public string Description { get; set; }
    }
}
