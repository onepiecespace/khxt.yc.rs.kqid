﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Product;

namespace KhXt.YcRs.HttpApi.Models.Product
{
    public class ProListResult : Result<ProListInfo>
    {

    }
    public class ProPageResult : Result<ProListPageList>
    {
    }
}