﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.Order
{
    public class MyCouponModel : Result<MyCoupon>
    {
    }
    public class MyCouponModels : Result<MyCouponList>
    {
    }

    public class MyCanUseCouponModels : Result<MyCanUseCouponList>
    {

    }
}
