﻿using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.Order
{
    /// <summary>
    /// 优惠券类别添加
    /// </summary>
    public class CouponInfoAddModel
    {
        /// <summary>
        /// 分类 0 立减券 1 满减券 2 折扣券
        /// </summary>
        public int Category { get; set; }
        /// <summary>
        /// 业务类别
        /// </summary>
        public long BusinessType { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 副标题
        /// </summary>
        public string Subtitle { get; set; }
        /// <summary>
        /// 活动链接
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// 折扣
        /// </summary>
        public float? Discount { get; set; }
        /// <summary>
        /// 限制价格 满多少减 0 无限制
        /// </summary>
        public float LimitPrice { get; set; }

        /// <summary>
        /// 限制类别 0 无限制 1 会员可用  2 仅自己可用 3 仅好友可用
        /// </summary>
        public int LimitCategory { get; set; }
        /// <summary>
        /// 领取次数 0 无限制 
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 发放数量 -1 无限制  
        /// </summary>
        [Required]
        public int IssueCount { get; set; } = -1;

        /// <summary>
        /// 是否已发放
        /// </summary>
        public int Issued { get; set; }

        /// <summary>
        /// 已经发放数量
        /// </summary>
        public long IssuedCount { get; set; }
        /// <summary>
        /// 花费金币 0 不需要
        /// </summary>
        [Required]
        public int SpendCoins { get; set; }
        /// <summary>
        /// 冲突优惠券
        /// </summary>
        public string ConflictIds { get; set; }
        /// <summary>
        /// 是否固定天数
        /// </summary>
        public int IsFixedDay { get; set; }
        /// <summary>
        /// 开始生效时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpiredTime { get; set; }

        /// <summary>
        /// 有效天数 0 无限制
        /// </summary>
        [Required]
        public int ValidDate { get; set; }

        /// <summary>
        /// -1 已作废  0 未启用 1 启用 
        /// </summary>
        public int Status { get; set; } = 1;
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public long Updater { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }


    }
}
