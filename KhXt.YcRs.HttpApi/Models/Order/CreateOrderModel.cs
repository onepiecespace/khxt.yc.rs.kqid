﻿using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Order;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Models.Order
{
    /// <summary>
    /// 提交订单
    /// </summary>
    public class SubmitOrder
    {
        /// <summary>
        /// 订单地址（金币商城实际物品不可为空）
        /// </summary>
        public AddressInfo AddressInfo { get; set; }
        /// <summary>
        /// 订单信息
        /// </summary>
        public OrderInfoModel OrderInfo { get; set; }
        /// <summary>
        /// 订单详情
        /// </summary>
        public OrderItemModel OrderItem { get; set; }
        /// <summary>
        /// 优惠券信息
        /// </summary>
        public List<OrderCouponInfoModel> OrderCouponInfo { get; set; }
    }
}
