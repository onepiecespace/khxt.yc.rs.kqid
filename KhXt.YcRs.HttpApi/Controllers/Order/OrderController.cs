﻿using Castle.Core.Internal;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.User;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Order;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Order
{
    [ApiController]
    [ApiGroup(ApiGroupNames.Order)]
    public class OrderController : BaseApiController
    {
        private readonly IOrderInfoService _orderInfoService;

        public OrderController(IOrderInfoService orderInfoService)
        {
            _orderInfoService = orderInfoService;

        }

        /// <summary>
        /// 创建订单模块（VIP下单，课程下单  金币商城下单 ）
        /// </summary>
        /// <param name="submitOrderInfo"></param>
        /// <returns></returns>
        [Route("api/Order")]
        [HttpPost]
        public async Task<OrderStatusInfo> Post(SubmitOrder submitOrderInfo)
        {
            try
            {
                var orderInfoEntity = submitOrderInfo.OrderInfo.MapTo<OrderInfoEntity>();
                orderInfoEntity.UserId = UserIdValue;
                var orderItemEntity = submitOrderInfo.OrderItem.MapTo<OrderItemEntity>();
                OrderShippingEntity orderShippingEntity = null;
                if (submitOrderInfo.AddressInfo != null)
                    orderShippingEntity = submitOrderInfo.AddressInfo.MapTo<OrderShippingEntity>();
                var orderCouponEntities = submitOrderInfo.OrderCouponInfo.MapTo<List<OrderCouponEntity>>();
                var rlt = await _orderInfoService.CreateOrder(orderInfoEntity, orderItemEntity, orderShippingEntity,
                    orderCouponEntities);
                return new OrderStatusInfo() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"用户：{UserIdValue}，生成订单异常", ex);
                return new OrderStatusInfo() { Code = (int)ResultCode.Exception, Message = "生成订单异常！" };
            }
        }
        /// <summary>
        /// 返回订单总价
        /// </summary>
        /// <param name="price">单价</param>
        /// <param name="count">数量</param>
        /// <param name="userCouponId">优惠券</param>
        /// <returns></returns>
        [Route("api/order/prices")]
        [HttpPost]
        public async Task<Result<string>> Put([FromForm] float price, [FromForm] int count, [FromForm] long userCouponId)
        {
            try
            {
                var rlt = await _orderInfoService.CountPrice(UserIdValue, price, count, userCouponId);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("计算订单价格异常", ex);
                return new Result<string>() { Code = (int)ResultCode.Exception, Message = "计算订单价格异常" };
            }
        }
        #region   同步订单模块  获取订单列表 舍弃

        //[Route("api/TaskOrderSync")]
        //[HttpPost]
        //public async Task<Result> TaskSyncPost(string OrderNo, long UserId, int OrderState, bool isall = false)
        //{
        //    try
        //    {
        //        int suscount = 0;
        //        int errcount = 0;
        //        Expression<Func<CourseOrderEntity, bool>> predicate = null;
        //        Action<Orderable<CourseOrderEntity>> preorder = null;
        //        preorder = order => order.Desc(n => n.Id);
        //        if (isall == false)
        //        {
        //            if (!string.IsNullOrEmpty(OrderNo))
        //            {
        //                predicate = n => n.OrderNo == OrderNo;
        //            }
        //            else
        //            {
        //                if (UserId > 0)
        //                {
        //                    predicate = n => n.UserId == UserId && n.OrderState == OrderState;
        //                }
        //                else
        //                {
        //                    predicate = n => n.OrderState == OrderState;
        //                }
        //            }
        //        }
        //        var orderList = await _courseOrderService.Fetch(predicate, preorder);
        //        var totalCount = orderList.Count();
        //        if (totalCount > 0)
        //        {
        //            foreach (var courseorder in orderList)
        //            {
        //                var orderentity = _orderInfoService.Get(courseorder.OrderNo);
        //                if (orderentity == null)//判断新订单表是否存在记录
        //                {
        //                    #region 读取课程信息

        //                    var lrt = await _courseService.GetAsync(courseorder.CourseId);
        //                    var course = lrt.Data;
        //                    var useAddress = await _addressService.Get(UserId);
        //                    var addInfo = useAddress.Data;
        //                    int ok = addInfo != null && !string.IsNullOrWhiteSpace(addInfo.Phone) ? 1 : 0;
        //                    #region  订单主体
        //                    OrderInfoModel OrderInfo = new OrderInfoModel();
        //                    OrderInfo.ProductId = course.Iosproductid;
        //                    OrderInfo.Payment = courseorder.PayPrice.ToString();
        //                    #region  类型赋值
        //                    switch (courseorder.PayType)
        //                    {
        //                        case 0:
        //                            OrderInfo.PaymentType = 3;
        //                            break;
        //                        case 1:
        //                            OrderInfo.PaymentType = 4;
        //                            break;
        //                        case 2:
        //                            OrderInfo.PaymentType = 0;
        //                            break;
        //                        case 3:
        //                            OrderInfo.PaymentType = 1;
        //                            break;
        //                        case 4:
        //                            OrderInfo.PaymentType = 2;
        //                            break;
        //                        case 5:
        //                            OrderInfo.PaymentType = 5;
        //                            break;
        //                        default:
        //                            break;
        //                    }
        //                    #endregion
        //                    OrderInfo.PostFee = "0";
        //                    OrderInfo.ShippingName = "";
        //                    OrderInfo.ShippingCode = "";
        //                    OrderInfo.IsFrom = 0;
        //                    OrderInfo.BusinessType = (int)BusinessType.Course;
        //                    var orderInfoEntity = OrderInfo.MapTo<OrderInfoEntity>();
        //                    orderInfoEntity.Id = courseorder.OrderNo;
        //                    orderInfoEntity.CreateTime = courseorder.CreateTime;
        //                    orderInfoEntity.UserId = courseorder.UserId;
        //                    #region 赋值状态
        //                    switch (courseorder.OrderState)
        //                    {
        //                        case 0:
        //                            orderInfoEntity.Status = 1;

        //                            break;
        //                        case 1:
        //                            orderInfoEntity.Status = 5;
        //                            orderInfoEntity.PaymentTime = courseorder.PayTime;
        //                            orderInfoEntity.PaymentNo = courseorder.PayNo;
        //                            break;
        //                        case 2:
        //                            orderInfoEntity.Status = 7;
        //                            break;
        //                        default:
        //                            break;
        //                    }
        //                    #endregion
        //                    #endregion
        //                    #region  订单明细
        //                    OrderItemModel OrderItem = new OrderItemModel();
        //                    OrderItem.ItemId = courseorder.CourseId.ToString();
        //                    OrderItem.Num = 1;
        //                    OrderItem.Title = course.CourseName;
        //                    OrderItem.Description = course.Detailed;
        //                    OrderItem.Price = float.Parse(courseorder.PayPrice.ToString());
        //                    OrderItem.TotalFee = float.Parse(courseorder.PayPrice.ToString()); ;
        //                    OrderItem.PicPath = course.CoursePicPhone;

        //                    var orderItemEntity = OrderItem.MapTo<OrderItemEntity>();
        //                    #endregion
        //                    #region  收货地址明细
        //                    AddressInfo addressInfo = new AddressInfo();
        //                    if (addInfo != null)
        //                    {
        //                        addressInfo.Userid = int.Parse(courseorder.UserId.ToString());
        //                        addressInfo.Sex = addInfo.Sex;
        //                        addressInfo.Phone = addInfo.Phone;
        //                        addressInfo.Consignee = addInfo.Consignee;
        //                        addressInfo.Provincial = addInfo.Provincial;
        //                        addressInfo.City = addInfo.City;
        //                        addressInfo.Areas = addInfo.Areas;
        //                        addressInfo.Address = addInfo.Address;
        //                    }
        //                    var orderShippingEntity = addressInfo.MapTo<OrderShippingEntity>();
        //                    #endregion
        //                    #region  使用优惠券明细
        //                    OrderCouponInfoModel OrderCouponInfo = new OrderCouponInfoModel();
        //                    //OrderCouponInfo.ItemId= courseEntity.Id.ToString();
        //                    var orderCouponEntities = OrderCouponInfo.MapTo<List<OrderCouponEntity>>();
        //                    #endregion
        //                    var rlt = await _orderInfoService.CreateOrder(orderInfoEntity, orderItemEntity, orderShippingEntity,
        //                    orderCouponEntities, true);//true同步订单
        //                    if (rlt.Code == (int)ResultCode.Success)
        //                    {
        //                        suscount++;
        //                    }
        //                    else
        //                    {
        //                        errcount++;
        //                    }
        //                    #endregion
        //                }

        //            }
        //        }
        //        string message = "成功:" + suscount + "失败:" + errcount;
        //        return new Result() { Code = (int)ResultCode.Success, Message = message };
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //        Logger.Error($"用户：{OrderNo}，同步订单异常", ex);
        //        return new Result() { Code = (int)ResultCode.Exception, Message = "生成订单异常！" };
        //    }
        //}
        ///// <summary>
        ///// 获取订单列表
        ///// </summary>
        ///// <returns></returns>
        //[Route("api/Orders/{businessType}")]
        //[HttpGet]
        //public async Task<Result<OrderStatusModels>> Gets([FromRoute]int businessType = 0, [FromQuery]int status = 0, [FromQuery] int pageIndex = 1, [FromQuery] int pageSize = 10)
        //{
        //    try
        //    {
        //        var rlt = await _orderInfoService.GetOrders(UserIdValue, businessType, status, pageIndex, pageSize);
        //        return rlt;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //        Logger.Error($"用户：{UserIdValue}，获取订单列表信息异常", ex);
        //        return new Result<OrderStatusModels>() { Code = (int)ResultCode.Exception, Message = "获取订单列表信息异常！" };
        //    }
        //}
        #endregion
        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <returns></returns>
        [Route("api/Orders/{businessType}")]
        [HttpGet]
        public async Task<Result<OldOrderStatusModels>> Gets([FromRoute] int businessType = 0, [FromQuery] int status = 0, [FromQuery] int pageIndex = 1, [FromQuery] int pageSize = 10)
        {
            try
            {
                var rlt = await _orderInfoService.GetOldOrders(UserIdValue, businessType, status, pageIndex, pageSize);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"用户：{UserIdValue}，获取订单列表信息异常", ex);
                return new Result<OldOrderStatusModels>() { Code = (int)ResultCode.Exception, Message = "获取订单列表信息异常！" };
            }
        }
        /// <summary>
        /// 获取订单信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [Route("api/Order/{orderId}")]
        [HttpGet]
        public async Task<OrderStatusInfo> Get([FromRoute] string orderId)
        {
            try
            {
                var rlt = await _orderInfoService.GetOrder(orderId);
                return new OrderStatusInfo() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"用户：{UserIdValue}，获取某个{orderId}订单信息异常", ex);
                return new OrderStatusInfo() { Code = (int)ResultCode.Exception, Message = "获取订单信息异常！" };
            }
        }

        /// <summary>
        /// 获取订单信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns> 5 订单支付成功 </returns>
        [Route("api/Order/Status/{orderId}")]
        [HttpGet]
        public async Task<Result> GetStatus([FromRoute] string orderId)
        {
            try
            {
                var rlt = await _orderInfoService.GetOrderStatus(UserIdValue, orderId);
                return new Result() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"用户：{UserIdValue}，获取某个{orderId}订单状态异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "获取订单状态异常！" };
            }
        }
        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [Route("api/Order/Cancel/{orderId}")]
        [HttpPost]
        public async Task<Result> CancelOrder(string orderId)
        {
            try
            {
                var rlt = await _orderInfoService.CancelOrder(orderId);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"取消订单{orderId}异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "取消订单异常" };
            }
        }

        [Route("api/Order/OrderPay")]
        [HttpPost]
        public async Task<Result> OrderPay([FromForm] PayOrderModel payOrderModel)
        {

            try
            {
                var rlt = await _orderInfoService.OrderPay(payOrderModel);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"订单支付台账写入失败异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "订单支付台账写入失败异常" };
            }
        }
    }
}