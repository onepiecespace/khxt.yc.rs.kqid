﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Services.Order;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Order;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Senparc.NeuChar.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Order
{
    /// <summary>
    /// IOS价格维护
    /// </summary>

    [ApiController]
    [ApiGroup(ApiGroupNames.Order)]
    public class IOSPriceController : BaseApiController
    {
        private readonly IIOSPriceService _iosPriceService;
        public IOSPriceController(IIOSPriceService iosPriceService)
        {
            _iosPriceService = iosPriceService;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet("api/order/iosprices/{productId}")]
        public async Task<Result<float>> Get(string productId)
        {
            try
            {
                var rlt = await _iosPriceService.GetIosPrice(productId);
                return rlt;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Logger.Error("获取IOS价格异常");
                return new Result<float>() { Code = (int)ResultCode.Exception, Message = "获取IOS价格异常" };
            }
        }

        /// <summary>
        /// 获取所有IOS价格
        /// </summary>
        /// <returns></returns>
        [HttpGet("api/order/iosprices")]
        public async Task<Result<Dictionary<string, float>>> Gets()
        {
            try
            {
                var rlt = await _iosPriceService.Gets();
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取IOS价格列表异常");
                return new Result<Dictionary<string, float>>() { Code = (int)ResultCode.Exception, Message = "获取IOS价格列表异常" };
            }

        }
        /// <summary>
        /// 添加IOS价格
        /// </summary>
        /// <param name="priceAddModel">价格实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Order/IOSPrice")]
        public async Task<Result> Post(IOSPriceAddModel priceAddModel)
        {
            try
            {
                var entity = priceAddModel.MapTo<IOSPriceEntity>();
                var rlt = await _iosPriceService.AddIOSPrice(entity);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "添加IOS价格异常" };
            }
        }
    }
}