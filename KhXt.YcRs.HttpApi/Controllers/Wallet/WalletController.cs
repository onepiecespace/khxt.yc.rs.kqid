﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Wallet;
using KhXt.YcRs.Domain.Services.Wallet;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Wallet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Payment.ApplePay;
using Payment.ApplePay.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Wallet
{
    /// <summary>
    /// 钱包管理
    /// </summary>
    [Route("api")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Wallet)]
    public class WalletController : BaseApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IAppleIapSdkRequest _appleIapSdkRequest;
        /// <summary>
        /// 
        /// </summary>
        private readonly IWalletService _walletService;
        /// <summary>
        /// 
        /// </summary>
        private readonly IOptions<AppleIapOptions> _options;
        public WalletController(IAppleIapSdkRequest appleIapSdkRequest,
            IOptions<AppleIapOptions> options,
            IWalletService walletService)
        {
            _appleIapSdkRequest = appleIapSdkRequest;
            _options = options;
            _walletService = walletService;
        }
        /// <summary>
        /// 钱包充值
        /// </summary>
        /// <param name="walletPay"></param>
        /// <returns></returns>
        [Route("wallet/walletcharge")]
        [HttpPost]
        public async Task<WalletChargeViewResult> WalletCharge(WalletPay walletPay)
        {
            try
            {
                var iapReq = new AppleIapRequest
                {
                    ReceiptData = walletPay.Receipt,
                    Password = _options.Value.ShareKey
                };
                Logger.Info("钱包充值开始！");
                Logger.Info("调用苹果内购！");
                Logger.Info(iapReq);
                var rlt = await _appleIapSdkRequest.AppleIapVerify(iapReq, _options.Value, walletPay.TransactionIdentifier);
                Logger.Info("调用苹果内购返回数据！");
                Logger.Info(rlt);
                if (rlt.Code == "1")
                {
                    //调用业务模块
                    if (walletPay.State.Equals("Purchased"))//支付成功 
                    {
                        Logger.Info("支付成功！处理业务");
                        var model = new WalletChargeModel
                        {
                            ChargeCoin = walletPay.ChargeCoin,
                            Description = walletPay.Description,
                            UserId = UserIdValue
                        };
                        var walletRlt = _walletService.WalletCharge(model);
                        if (walletRlt.Code != (int)ResultCode.Success)
                        {
                            return new WalletChargeViewResult() { Code = (int)ResultCode.Fail, Message = $"钱包充值失败！{walletRlt.Message}" };
                        }
                        else
                            return new WalletChargeViewResult() { Data = walletRlt.Data, Message = $"钱包充值成功！" };
                    }
                    else if (walletPay.State.Equals("Restored"))//已经订阅过 
                    {
                        Logger.Info("已经订阅过");
                        return new WalletChargeViewResult() { Code = (int)ResultCode.Fail, Message = "已经订阅过！" };
                    }
                    else //支付失败
                    {
                        Logger.Info("支付失败,调用苹果内购");
                        return new WalletChargeViewResult() { Code = (int)ResultCode.Fail, Message = "钱包充值失败！内购支付失败！" };
                    }
                }
                else
                {
                    Logger.Info("支付失败,调用苹果内购");
                    return new WalletChargeViewResult() { Code = int.Parse(rlt.Code), Message = "钱包充值失败！内购支付失败！" };
                }

            }
            catch (Exception ex)
            {
                Logger.Error("钱包充值", ex);
                Logger.Info("钱包充值异常结束！", ex);
                return new WalletChargeViewResult { Code = (int)ResultCode.Exception, Message = "钱包充值异常" };
            }
        }
        /// <summary>
        /// 获取钱包余额
        /// </summary>
        /// <returns></returns>
        [HttpGet("wallet")]
        public async Task<Result<WalletResult>> WalletBalance()
        {
            try
            {
                var rlt = _walletService.GetWallet(UserIdValue);
                return await Task.Run(() => rlt);
            }
            catch (Exception ex)
            {
                Logger.Error($"用户{UserIdValue}获取钱包余额异常", ex);
                return new WalletViewModel();
            }
        }
        /// <summary>
        /// 获取充值记录
        /// </summary>
        /// <returns></returns>
        [HttpGet("wallet/list")]
        public async Task<Result<WalletLogsResult>> Wallets(int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var rlt = _walletService.GetWallets(UserIdValue, pageIndex, pageSize);
                return await Task.Run(() => rlt);
            }
            catch (Exception ex)
            {
                Logger.Error($"用户{UserIdValue}，获取充值记录异常", ex);
                return new Result<WalletLogsResult> { Code = (int)ResultCode.Exception, Message = "获取充值记录异常" };
            }
        }
    }
}