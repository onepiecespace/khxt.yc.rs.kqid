﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Order;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Sys
{
    public partial class SysController
    {

        /// <summary>
        /// 获取支付台账记录
        /// </summary>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="businessType">0 全部 16 课程  1024 VIP</param>
        /// <param name="payType"></param>
        /// <param name="aid">代理商</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("order/bills")]
        public async Task<Result<SysOrderPayInfoResult>> GetSysOrderPayList(string startTime, string endTime, int businessType, string aid, [Required] int pageIndex, [Required] int pageSize, int payType = -1)
        {
            try
            {
                OrderQueryModel orderQueryModel = new OrderQueryModel();
                orderQueryModel.StartTime = startTime;
                orderQueryModel.EndTime = endTime;
                orderQueryModel.BusinessType = businessType;
                orderQueryModel.PayType = payType;
                orderQueryModel.Aid = aid;
                orderQueryModel.PageIndex = pageIndex;
                orderQueryModel.PageSize = pageSize;
                var rlt = await _sysService.GetSysOrderPayList(orderQueryModel);
                return rlt;
            }
            catch (Exception ex)
            {
                Logger.Error("获取支付台账记录异常", ex);
                return new Result<SysOrderPayInfoResult> { Message = "获取支付台账记录异常", Code = (int)ResultCode.Exception };
            }
        }

        [HttpGet("order/bills/export")]
        public async Task<Result> ExportOrderPay(OrderQueryModel orderQueryModel)
        {
            try
            {
                var rlt = _sysService.ExportOrderPay(orderQueryModel);
                return await Task.Run(() => rlt);
            }
            catch (Exception ex)
            {
                Logger.Error("导出Excel异常", ex);
                return new Result { Message = "导出Excel异常", Code = (int)ResultCode.Exception };
            }


        }
    }
}
