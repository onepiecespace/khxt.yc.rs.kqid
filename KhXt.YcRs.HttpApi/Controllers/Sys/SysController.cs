﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Sys;
using KhXt.YcRs.Domain.Services.Sys;
using KhXt.YcRs.HttpApi.ApiGroup;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Sys
{
    [Route("api/sys")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Sys)]
    public partial class SysController : BaseApiController
    {
        private readonly ISysLogService _sysLogService;
        private readonly ISysService _sysService;
        private ISettingService _settingService;
        public SysController(ISysLogService sysLogService, ISysService sysService, ISettingService settingService)
        {
            _sysLogService = sysLogService;
            _sysService = sysService;
            _settingService = settingService;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="isClean"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result> SyncLogRedis([FromForm] string startTime, [FromForm] bool isClean = false)
        {
            try
            {
                var rlt = await _sysLogService.SyncRedis(startTime, isClean);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("同步服务器Redis记录异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "同步服务器Redis记录异常" };
            }

        }
        /// <summary>
        /// 访问记录统计异常
        /// </summary>
        /// <param name="category"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>

        [HttpGet("statistics/{category}")]
        public async Task<Result<SysLogStatistics>> LogsStatistics([FromRoute] int category, [FromQuery] string startTime = "", [FromQuery] string endTime = "")
        {
            try
            {
                var rlt = await _sysLogService.LogsStatistics(category, startTime, endTime);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("访问记录统计异常", ex);
                return new Result<SysLogStatistics>() { Code = (int)ResultCode.Exception, Message = "访问记录统计异常" };
            }
        }
        /// <summary>
        /// 用户当日统计
        /// </summary>
        /// <param name="today"></param>
        /// <returns></returns>
        [HttpGet("statistics/usershour")]
        public async Task<Result<UserHourChars>> GetHourChar(string today = "")
        {
            try
            {
                var rlt = await _sysService.GetHourChar(today, UserIdValue);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("访问记录统计异常", ex);
                return new Result<UserHourChars>() { Code = (int)ResultCode.Exception, Message = "访问记录统计异常" };
            }
        }
        /// <summary>
        /// 用户最近一周统计
        /// </summary>
        /// <returns></returns>
        [HttpGet("statistics/usersweek")]
        public async Task<Result<UserWeekChars>> GetWeekChar()
        {
            try
            {
                var rlt = await _sysService.GetWeekChar(UserIdValue);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("访问记录统计异常", ex);
                return new Result<UserWeekChars>() { Code = (int)ResultCode.Exception, Message = "访问记录统计异常" };
            }
        }
        /// <summary>
        /// 用户最近一年统计
        /// </summary>
        /// <returns></returns>
        [HttpGet("statistics/usersyear")]
        public async Task<Result<UserYearChars>> GetYearChar()
        {
            try
            {
                var rlt = await _sysService.GetYearChar(UserIdValue);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("访问记录统计异常", ex);
                return new Result<UserYearChars>() { Code = (int)ResultCode.Exception, Message = "访问记录统计异常" };
            }
        }
        /// <summary>
        /// 获取支付统计 
        /// </summary>
        /// <param name="category">0 今天 1 最近一周 2 最近一年</param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet("statistics/paychars")]
        public async Task<Result<PayChars>> GetPayChars(int category, string startTime = "", string endTime = "")
        {
            try
            {
                var rlt = await _sysService.GetPayChars(UserIdValue, category, startTime, endTime);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("支付统计异常", ex);
                return new Result<PayChars>() { Code = (int)ResultCode.Exception, Message = "支付统计异常" };
            }
        }

        /// <summary>
        /// 所有缓存的控制器
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/GetAllServices")]
        [AllowAnonymous]
        private async Task<List<Type>> GetAllServices()
        {

            var lrt = await _sysService.GetAllServices();

            return await Task.FromResult(lrt);
        }

        /// <summary>
        /// 清空此节点缓存并加载
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/ClearAllServiceCache")]
        [AllowAnonymous]
        private async Task<bool> ClearAllServiceCache()
        {

            await _sysService.ClearAllServiceCache();

            return await Task.FromResult(true);
        }
        /// <summary>
        /// 获取服务字典
        /// </summary>
        /// <param name="code">ServiceDictionary 字典编号</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("/api/ServiceDicByCode")]
        public Dictionary<int, Type> GetServiceDictionaryAll(int code = 0)
        {
            Dictionary<int, Type> list = new Dictionary<int, Type>();
            if (code == 0)
            {
                list = Consts.ServiceDictionary;

            }
            else
            {
                if (Consts.ServiceDictionary.TryGetValue(code, out Type serviceType))
                {

                }
                list.Add(code, serviceType);
            }
            return list;
        }
        /// <summary>
        /// 清空指定节点实例下的某个服务的缓存
        /// </summary>
        /// <param name="nodeName"> wapi_226 与 wapi_45 与 hl_wapi_45 与 df_wapi_45</param>
        /// <param name="code">ServiceDictionary 字典编号 ServiceDicByCode 获取编号</param>
        /// <returns></returns>
        [HttpPost("/api/NoticeClearCache")]
        [AllowAnonymous]
        public async Task<bool> NoticeClearCache(string nodeName = "", int code = 0)
        {
            await _sysService.NoticeClearCache(nodeName, code);
            return await Task.FromResult(true);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/nodes/{key}/{type}")]
        public async Task<Dictionary<string, string>> GetNodes([FromRoute] string key, [FromRoute] int type = 0)
        {
            var akey = await Redis.StringGetAsync("akey");
            if (!akey.Equals(Hx.Security.AESCryptogram.Encrypt(key)))
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return null;
            }
            var nodes = type == 0 ? await _sysService.GetAllNodes() : type == 1 ? await _sysService.GetActiveNodes() : await _sysService.GetInActiveNodes();
            var ns = nodes.ToDictionary(n => n.Key, n => DateTimeOffset.FromUnixTimeSeconds(n.Value).ToLocalTime().ToString("yyy-MM-dd HH:mm:ss"));
            return ns;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/hc")]
        public string HealthCheck()
        {
            return "jaden";
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/api/vs/{offset}")]
        public async Task<VisitStat> GetVisitStat([FromRoute] int offset, string path)
        {
            var dt = DateTime.Now.AddDays(offset * -1);
            return await _sysService.GetVisitStat(path, dt);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/api/vss/{offset}")]
        public async Task<List<VisitStat>> GetAllVisitStat(int offset = 0)
        {
            var dt = DateTime.Now.AddDays(offset * -1);
            return await _sysService.GetAllVisitStat(dt);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/api/set/course")]
        public async Task<string> SetHomeCourse(long id, int mode, int sort, int picDirect, string values)
        {
            var vals = values.Split(",").Select(v => Convert.ToInt64(v)).ToArray();
            var rlt = await _settingService.SetHomeCourse(id, mode, sort, picDirect, vals);
            return rlt;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/svrdbg")]
        public async Task<object> ServiceDebug(string key, int serviceCode)
        {
            var pkey = "";
            if (string.IsNullOrEmpty(key))
            {
                pkey = $"ly{DateTime.Now.ToString("HHdd")}";
            }
            else
            {
                pkey = key;
            }
            if (!_sysService.Valid(pkey))
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return "";
            }
            var data = await _sysService.GetServiceDebugInfo(serviceCode);
            return new { node = DomainEnv.NodeName, data };
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/dbg/header")]
        public async Task<IHeaderDictionary> PrintHeader(string key)
        {
            if (!_sysService.Valid(key))
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return null;
            }
            var header = HttpContext.Request.Headers;
            return await Task.FromResult(header);
        }

        [HttpGet("/api/guest")]
        [AllowAnonymous]
        public async Task<Result<int>> GetGuestStatus()
        {
            var rlt = await _sysService.GetGuestStatus();
            return new Result<int> { Data = rlt };
        }

        [HttpPost("/api/guest/{flag}")]
        [AllowAnonymous]
        public async Task<Result<int>> SetGuestStatus([FromRoute] int flag)
        {
            var rlt = await _sysService.SetGuestStatus(flag);
            return new Result<int> { Data = rlt ? 1 : 0 };
        }
    }
}

