﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Services.VipCard;
using KhXt.YcRs.HttpApi.ApiGroup;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Vip
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.VIP)]
    public class VipCardController : BaseApiController
    {
        private readonly IVipCardService _vipcardService;
        public VipCardController(IVipCardService vipcardService)
        {
            _vipcardService = vipcardService;
        }

        /// <summary>
        /// Vip卡片注册
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="cardNo">卡号</param>
        /// <param name="passWord">密码</param>
        /// <returns></returns>
        //1根据卡号查询是否有该卡，2且是否已绑定，并更新数据到数据库
        [HttpPost]
        [AllowAnonymous]
        public async Task<Result> Register([FromForm] string phone, [FromForm] string cardNo, [FromForm] string passWord)
        {

            try
            {
                //if (UserId.IsNotNullOrEmpty())
                //{
                //    userId = Convert.ToInt64(UserId);
                //}
                var resultValues = await _vipcardService.Register(phone, cardNo, passWord);
                Logger.Debug($"resultValues:{ resultValues}");
                return await Task.FromResult(resultValues);
            }
            catch (Exception ex)
            {
                Logger.Error("GetVipCardInfo", ex);
                return new Result(ResultCode.Exception) { Message = ex.Message };
            }
        }


    }
}
