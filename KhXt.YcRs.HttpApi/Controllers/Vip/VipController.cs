﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Services.Vip;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Vip;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Vip
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.VIP)]
    public class VipController : BaseApiController
    {
        private readonly IVipService _vipService;
        public VipController(IVipService vipService)
        {
            _vipService = vipService;
        }
        /// <summary>
        /// 会员中心
        /// </summary>
        /// <param name="size">图片尺寸  1 1x 2 2x 3 3x</param>
        /// <returns></returns>
        [Route("/api/Vip/UserCenter/{size}")]
        [HttpGet]
        public async Task<MyVipInfo> GetUserCenter([FromRoute] int size = 1)
        {
            try
            {
                if (size == 3)
                    size = 1;
                var rlt = await _vipService.GetUserCenter(UserIdValue, size);
                return new MyVipInfo() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取个人会员中心异常", ex);
                return new MyVipInfo() { Code = (int)ResultCode.Exception, Message = "获取个人会员中心异常" };
            }
        }
        /// <summary>
        /// 会员服务
        /// </summary>
        /// <param name="size">图片尺寸  1 1x 2 2x 3 3x</param>
        /// <returns></returns>
        [Route("/api/VipService/{size}")]
        [HttpGet]
        public async Task<VipServiceInfoModel> Get([FromRoute] int size = 1)
        {
            try
            {
                if (size == 3)
                    size = 1;
                var rlt = await _vipService.GetVipServiceInfo(UserIdValue, size);
                return new VipServiceInfoModel() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取个人会员中心服务异常", ex);
                return new VipServiceInfoModel() { Code = (int)ResultCode.Exception, Message = "获取个人会员中心服务异常" };
            }
        }

        /// <summary>
        /// 保存会员信息
        /// </summary>
        /// <param name="addModel">会员信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result> Post(VipInfoAddModel addModel)
        {
            try
            {
                addModel.Updater = UserIdValue;
                var entity = addModel.MapTo<VipInfoEntity>();
                var rlt = await _vipService.AddVipInfo(entity);
                return new Result() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("保存会员卡信息异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "保存会员卡信息异常" };
            }
        }
        /// <summary>
        /// 保存会员服务
        /// </summary>
        /// <param name="addPrivilegeAddModel">会员服务信息</param>
        /// <returns></returns>
        [Route("/api/VipPrivilege")]
        [HttpPost]
        public async Task<Result> PostVipPrivilege(PrivilegeAddModel addPrivilegeAddModel)
        {
            try
            {
                addPrivilegeAddModel.Updater = UserIdValue;
                var entity = addPrivilegeAddModel.MapTo<VipPrivilegeEntity>();
                var rlt = await _vipService.AddVipPrivilegeInfo(entity);
                return new Result() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("保存会员卡特权信息异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "保存会员卡特权信息异常" };
            }
        }

    }
}