﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Sys;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Ad
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AdController : BaseApiController
    {

        IContentService _contentService;

        public AdController(IContentService contentService)
        {
            _contentService = contentService;
        }

        /// <summary>
        /// 新的广告位107后台固定的广告位统一的接口(废除其他广告位) 返回接口字段结构一致 规范数据库存放
        /// </summary>
        /// <param name="tag">广告标记,可以标记位置，home = 1,read = 2, course_top = 4,course = 8,course_bottom = 16,pccourse = 32,gold = 64,jxj=128</param>
        /// <returns></returns>
        [HttpGet("tagtype/{tag}", Name = "GetAdByTag")]
        public async Task<Result<List<AdModel>>> GetAdByTag(TagType tag)
        {
            var tagstr = EnumHelper.GetDescription(tag);
            var rlt = await _contentService.GetAdsByTag(ContentType.newAd, tagstr);
            rlt.Expire = 3 * 60;
            return rlt;
        }

        /// <summary>
        /// 所有广告
        /// 返回Molde说明：
        /// Title:广告标题
        /// Summary:简短描述，也可作为广告副标题
        /// Content:广告内容，h5格式
        /// Image:广告图片
        /// SmallImage:广告缩略图（非必须）
        /// Slide:幻灯内容，可能广告不止一个图片或文件，此处可存H5的幻灯，或其他自定义格式
        /// Tag:广告标记，可以标记广告的类型、位置等信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Result<List<AdModel>>> Get()
        {
            return await _contentService.GetAllAd(ContentType.Ad);
        }


        // GET: api/Ad/tag/game
        /// <summary>
        /// 指定Tag值的所有广告
        /// </summary>
        /// <param name="tag">广告标记,可以标记位置，类型等等，简短字符串，不能有空格等特殊字符</param>
        /// <returns></returns>
        [HttpGet("tag/{tag}", Name = "GetByTag")]
        public async Task<Result<List<AdModel>>> GetByTag(string tag)
        {
            return await _contentService.GetAdsByTag(ContentType.Ad, tag);
        }

        /// <summary>
        /// 获取不同类型下的轮播图
        /// </summary>
        /// <param name="type">类别：1.html外跳，2.课程，3.pk,4.闯关，5.听写，6.任务 7 广告 8朗读 9 Vip 10 Teacher</param>
        /// <returns></returns>
        [HttpGet("type/{type}", Name = "GetList")]
        public async Task<Result<List<AdModel>>> GetList(ContentType type)
        {

            return await _contentService.GetAllAd(type);
        }
        /// <summary>
        /// 获取朗读类型下的轮播图（指定区域）
        /// </summary>
        /// <param name="type">类别：1.html外跳，2.课程，3.pk,4.闯关，5.听写，6.任务 7 广告 8朗读 9 Vip 10 Teacher</param>
        /// <param name="action">区域标识 例如：1 top 2 课程分类 3 底部</param>
        /// <returns></returns>
        [HttpGet("types/{type}", Name = "GetTypeList")]

        public async Task<Result<List<AdModel>>> GetTypeList(ContentType type = ContentType.Course, int action = 1)
        {
            return await _contentService.GetAdsByAction(type, action);
        }
        /// <summary>
        /// 第一个指定Tag值的广告(暂时未用到)
        /// </summary>
        /// <param name="type">类别：1.html外跳，2.课程，3.pk,4.闯关，5.听写，6.任务 7 广告 8朗读9 Vip 10 Teacher </param>
        /// <param name="tag">广告标记,可以标记位置，类型等等，简短字符串，不能有空格等特殊字符</param>
        /// <returns></returns>
        [HttpGet("{type}/{tag}", Name = "GetTypeBytag")]
        public async Task<Result<AdModel>> GetTypeBytag(ContentType type, string tag)
        {
            return await _contentService.GetAdByTag(type, tag);
        }

        // GET: api/Ad/tag1
        /// <summary>
        /// 第一个指定Tag值的广告（暂时未用到）
        /// </summary>
        /// <param name="tag">广告标记,可以标记位置，类型等等，简短字符串，不能有空格等特殊字符</param>
        /// <returns></returns>
        [HttpGet("{tag}", Name = "Get")]
        public async Task<Result<AdModel>> Get(string tag)
        {
            return await _contentService.GetAdByTag(ContentType.Ad, tag);
        }
        /// <summary>
        /// 清空广告位并加载
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/SetAdClearCache")]
        [AllowAnonymous]
        public async Task<bool> SetadClearCache()
        {

            var isok = _contentService.addClearCache();

            return await Task.FromResult(isok);
        }

        //// POST: api/Ad
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/Ad/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
