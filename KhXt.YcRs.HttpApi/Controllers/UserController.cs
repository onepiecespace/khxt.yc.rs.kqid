using HuangLiCollege.Controllers;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.JWT;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Files;
using KhXt.YcRs.Domain.Services.SSO;
using KhXt.YcRs.Domain.Services.Sys;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using HuangLiCollege.Models;
using Hx.Extensions;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models;
using KhXt.YcRs.HttpApi.Models.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace KhXt.YcRs.HttpApi.Controllers
{
    /// <summary>
    /// 用户接口、个人中心(必须传 【token】)
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Auth)]
    public partial class UserController : BaseApiController
    {
        #region 构造函数

        //private readonly int _smsTime = 60 * 3;
        private readonly int _tokenTime = 60 * 24 * 900;
        private readonly string _defaultHeaderUrl = "/image/pidan.png";
        //private readonly IASmsBLL _iaSmsBll;
        private readonly IFilesService _iFilesService;

        private readonly IVersionService _versionService;
        private readonly IFeedbackService _feedbackService;
        private readonly IUserFeedbackService _userFeedbackService;
        private readonly IUserBankService _userBankService;
        private readonly IAddressService _addressService;
        private readonly IUserService _userService;
        private readonly ICourseOrderService _courseOrderService;
        private readonly ICourseService _courseService;
        private readonly ISmsService _smsService;
        private ICourseBuyCollectService _courseBuyService;
        private ShanYanSetting _sysitting;
        private IUserSSOService _ssoService;
        private readonly IConfiguration _configuration;
        public UserController(
            IConfiguration configuration,
            IUserService userService,
            IAddressService addressService,
            IFeedbackService feedbackService,
            IUserFeedbackService userFeedbackService,
            IVersionService versionService,
            IUserBankService userBankService,
            ICourseOrderService courseOrderService,
            ICourseService courseService,
            ISmsService smsService,
            IFilesService iFilesService,
            ICourseBuyCollectService courseBuyService,
            ShanYanSetting sysitting,
            IUserSSOService ssoService)
        {
            _configuration = configuration;
            _userService = userService;
            _addressService = addressService;
            _feedbackService = feedbackService;
            _userFeedbackService = userFeedbackService;
            _versionService = versionService;
            _userBankService = userBankService;
            _courseOrderService = courseOrderService;
            _courseService = courseService;
            _smsService = smsService;
            _iFilesService = iFilesService;
            _courseBuyService = courseBuyService;
            _sysitting = sysitting;
            _ssoService = ssoService;
        }

        #endregion

        #region 测试阿里短信
        [HttpGet]
        [AllowAnonymous]
        public async Task<Result> TestAliSms(string phone)
        {

            var key1 = _configuration.GetValue<string>("SmsSettings:Ali2:Ali_Sms_AccessKeyId", "");
            var key2 = _configuration.GetValue<string>("SmsSettings:Ali:Ali_Sms_AccessKeyId", "");
            var number = phone;
            var aid = "college";
            var bizType = "1";

            var rlt = await _smsService.SendValidCodeV2(number, aid, bizType, "");

            return rlt;

        }
        #endregion

        #region 获取登录手机号/或用户ID获取用户token

        /// <summary>
        /// 获取登录手机号/或用户ID获取用户token  JWT授权(数据将在请求头中进行传输) 直接在下框中输入冒号后的字符串信息，token:token字符串 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<Result> GetToken(string phone)
        {
            var rlt = await _userService.GetToken(phone);
            return rlt;

        }
        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<Result> GetSms(string phone)
        {
            var sms = "";
            Result result = new Result();
            if (phone.Length < 11)
            {
                try
                {
                    long.TryParse(phone, out long idResult);
                    var info = await _userService.Get(idResult);
                    phone = string.IsNullOrWhiteSpace(info.Phone) ? phone : info.Phone;

                }
                catch (Exception ex)
                {
                    result.Code = (int)ResultCode.Fail;
                    Logger.Error(ex);
                }
            }
            sms = await Redis.StringGetAsync("SMS:" + phone);
            if (!string.IsNullOrEmpty(sms))
            {
                result.Code = (int)ResultCode.Success;
                result.Data = $"{sms}";
                result.Message = "获取验证码成功";
            }
            return await Task.FromResult(result);

        }

        /// <summary>
        /// 获取验证码Test(不调用短信接口，内部测试别登录别的用户用)
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<Result> GetSmsTest(string phone)
        {
            var sendRlt = await _smsService.SendValidCodeTest(phone);
            var sms = "";
            Result result = new Result();
            sms = await Redis.StringGetAsync("SMS:" + phone);
            if (!string.IsNullOrEmpty(sms))
            {
                result.Code = (int)ResultCode.Success;
                result.Data = $"{sms}";
                result.Message = "获取验证码成功";
            }
            return await Task.FromResult(result);
        }
        /// <summary>
        /// 获取验证码Test(不调用短信接口，内部测试别登录别的用户用)
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="contentstr"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<Result> SendContentTest(string phone, string contentstr)
        {
            //短信提醒
            var sendRlt = await _smsService.SendContentTest(phone, contentstr);
            var sms = "";
            Result result = new Result();
            sms = await Redis.StringGetAsync("SMS:" + phone);
            if (!string.IsNullOrEmpty(sms))
            {
                result.Code = (int)ResultCode.Success;
                result.Data = $"{sms}";
                result.Message = "获取验证码成功";
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// 【获取用户信息】
        /// 【 username用户名称 userid用户ID sex 0女1男2未知 birthday生日 provincial省city市areas区 grade年级】
        /// 【 address地址 headurl头像 level 等级 phone手机号 wx 微信token qq QQtoken creatime 创建时间 state状态】
        /// 【 inviterid 邀请用户id  score 积分    experience 经验  tenantid 租户  isFirstInputAddress 是否地址一次输入收货地址 1是 0不是 detailAddress 】 
        /// 【 GradeList 年纪列表 】
        /// </summary>
        /// <returns>用户信息</returns>
        [HttpGet()]
        public async Task<UserResult> Get()
        {
            UserResult result = new UserResult();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    //DJD:有问题，不应该返回此错误值 
                    return new UserResult { Code = (int)ResultCode.InvalidPara, Message = ResultCode.InvalidPara.GetDescription() };
                }
                var rlt = await _userService.GetUserInfo(UserIdValue);

                return new UserResult { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                Logger.Error(ex);
            }
            return result;
        }

        [HttpGet]
        public async Task<UserResult> GetInfo()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    return new UserResult { Code = (int)ResultCode.InvalidPara, Message = ResultCode.InvalidPara.GetDescription() };
                }
                var rlt = await _userService.Get(UserIdValue);
                return new UserResult { Data = rlt };
            }
            catch (Exception ex)
            {
                var result = new UserResult
                {
                    Code = (int)ResultCode.Fail
                };
                Logger.Error(ex);
                return result;
            }
        }
        /// <summary>
        /// 加盟商用户开通商学院账户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<Result<long>> ChargeCollege(AUserTobInput model)
        {
            if (model == null)
            {
                return new Result<long>(ResultCode.Fail, "参数错误");
            }
            if (model.Phone.IsNullOrEmpty())
            {
                return new Result<long>(ResultCode.Fail, "无有效手机号");
            }
            var rlt = await _userService.ChargeCollegeAsync(model);
            return rlt;
        }
        #endregion

        #region 获取用户信息、编辑个人头像、获取用户年纪信息

        /// <summary>
        /// 【编辑用户信息】
        /// 【返回值 200 成功】
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="sex">性别 2 表示未知 1表示男 0 表示女</param>
        /// <param name="birthday">生日 如： 2019年09月11日</param>
        /// <param name="provincial">省份  如：山东省</param>
        /// <param name="city">市 如：青岛市</param>
        /// <param name="areas">县区 如:市南区</param> 
        /// <param name="address">详细地址</param>
        /// <param name="grade">年级 如：1 对应的看【获取用户信息 data GetLevelList[]】</param>
        /// <returns>返回结果代码</returns>
        [HttpPost()]
        public async Task<Result> EditUser([FromForm] string username, [FromForm] int sex = 2, [FromForm] string birthday = "", [FromForm] string provincial = "", [FromForm] string city = "", [FromForm] string areas = "", [FromForm] string address = "", [FromForm] int grade = 1)
        {
            Result result = new Result();
            try
            {


                var info = await _userService.Get(UserIdValue);
                var oldUserEntityString = JsonConvert.SerializeObject(info);
                var oldUserEntity = JsonConvert.DeserializeObject<UserBaseInfo>(oldUserEntityString);
                if (!string.IsNullOrEmpty(username))
                {
                    info.Username = username;
                    var resultVCR = VCRApiHelper.ResultTextDefined(username);
                    if (resultVCR.Code == (int)ResultCode.warning)
                    {
                        return new Result() { Code = (int)ResultCode.warning, Message = $"昵称含有敏感字符:" + resultVCR.Message + ", 请更换!" };
                    }
                }
                if (sex != 2)
                {
                    info.Sex = sex;
                }
                if (!string.IsNullOrEmpty(birthday))
                {
                    info.Birthday = birthday;
                }

                if (grade != info.Grade)
                {
                    info.Grade = grade;
                }
                await _userService.Update(info);

                if (!oldUserEntity.Username.Equals(info.Username))//昵称不等
                {
                    oldUserEntity.Username = info.Username;//只修改了昵称
                                                           // if (oldUserEntity.Equals(info))
                                                           //向任务REDIS中写数据 ,最后一个为TRUE,是新手任务，只覆盖数据不做累加
                                                           //await _taskService.SetTaskToRedis(ActionType.Xgnc, 1, long.Parse(UserId), true);
                }

                //向任务REDIS中写数据 
                //await _taskService.SetTaskToRedis(ActionType.Wsgrzl, 1, info.Userid, true);
                result.Message = "更新成功";
                result.Data = "";

            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"更新失败{ex.Message}";
                result.Data = "";
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// 【上传头像】
        /// FromForm{file } 使用post body form-data 形式传递文件流默认第一个
        /// 【返回值 headurl：用户头像地址】
        /// </summary>
        /// <returns>头像地址</returns>
        [HttpPost()]
        [Consumes("application/json", "multipart/form-data")]
        public async Task<Result> SetHeadImg([FromForm] IFormCollection formFiles)
        {
            Result result = new Result();
            try
            {

                var form = formFiles;//定义接收类型的参数
                Hashtable hash = new Hashtable();
                IFormFileCollection cols = Request.Form.Files;
                if (cols == null || cols.Count == 0)
                {
                    result.Code = (int)ResultCode.Fail; ;
                    result.Message = "未获取到文件！";
                    return result;

                }
                var file = cols[0];
                if (file.Length == 0)
                {
                    result.Code = (int)ResultCode.Fail; ;
                    result.Message = "未获取到文件！";
                    return result;
                }



                //把文件读取到字节数组
                Stream filestream = file.OpenReadStream();
                var imgresult = BaiduApiHelper.GetForImgJson(filestream);
                JObject jo = (JObject)imgresult;
                var message = jo["conclusion"].ToString();
                if (message.Equals("正常"))
                {
                    var filePath = _defaultHeaderUrl;
                    var startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1), TimeZoneInfo.Local); // 当地时区
                    var timeStamp = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数 
                    var path = Path.Combine("image", timeStamp + RandomHelper.GenerateRandomCode(5) + file.FileName);


                    var rlt = await _iFilesService.UploadFilePic(file, path);
                    if (rlt.Code == (int)ResultCode.Success)
                    {
                        filePath = $"/{path}";
                        result.Data = rlt.Data;
                    }
                    else
                    {
                        Logger.Error("用户上传头像失败，使用默认头像", new Exception(rlt.Message));
                        result.Data = filePath;
                        result.Message = "用户上传头像失败，使用默认头像";
                    }
                    // var ftp = new FtpFile();
                    //var path = await ftp.UploadAsync(file, UserId);
                    if (!string.IsNullOrWhiteSpace(UserId))
                    {
                        var info = await _userService.GetUser(UserIdValue);
                        if (info != null)
                        {
                            info.Headurl = filePath;
                            await _userService.Update(info);
                            result.Message = "成功上传";
                        }
                    }
                    //向任务REDIS中写数据 
                    //await _taskService.SetTaskToRedis(ActionType.Xgtx, 1, long.Parse(UserId), true);
                    return result;

                }
                else
                {
                    result.Code = (int)ResultCode.warning;
                    result.Message = $"禁用图片:" + message;
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail; ;
                result.Message = ex.Message;
                return result;
            }

        }
        /// <summary>
        /// 【上传头像】
        /// FromForm{file } 使用post body form-data 形式传递文件流默认第一个
        /// 【返回值 headurl：用户头像地址】
        /// </summary>
        /// <returns>头像地址</returns>
        [HttpPost()]
        [Consumes("application/json", "multipart/form-data")]
        [AllowAnonymous]
        public async Task<Result> SetIOSHeadImg([FromForm] IFormCollection formFiles, long UserId)
        {
            Result result = new Result();
            try
            {
                var form = formFiles;//定义接收类型的参数
                Hashtable hash = new Hashtable();
                IFormFileCollection cols = Request.Form.Files;
                if (cols == null || cols.Count == 0)
                {
                    result.Code = (int)ResultCode.Fail; ;
                    result.Message = "未获取到文件！";
                    return result;

                }
                var file = cols[0];
                if (file.Length == 0)
                {
                    result.Code = (int)ResultCode.Fail; ;
                    result.Message = "未获取到文件！";
                    return result;
                }
                //把文件读取到字节数组
                Stream filestream = file.OpenReadStream();
                var imgresult = BaiduApiHelper.GetForImgJson(filestream);
                JObject jo = (JObject)imgresult;
                var message = jo["conclusion"].ToString();
                if (message.Equals("正常"))
                {
                    var filePath = _defaultHeaderUrl;
                    var startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1), TimeZoneInfo.Local); // 当地时区
                    var timeStamp = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数 
                    var path = Path.Combine("image", timeStamp + RandomHelper.GenerateRandomCode(5) + file.FileName);
                    var rlt = await _iFilesService.UploadFilePic(file, path);
                    if (rlt.Code == (int)ResultCode.Success)
                    {
                        filePath = $"/{path}";
                        result.Data = rlt.Data;
                    }
                    else
                    {
                        Logger.Error("用户上传头像失败，使用默认头像", new Exception(rlt.Message));
                        result.Data = filePath;
                        result.Message = "用户上传头像失败，使用默认头像";
                    }

                    //var ftp = new FtpFile();
                    //var path = await ftp.UploadAsync(file, UserId.ToString());
                    result.Message = "成功上传";

                    var info = await _userService.GetUser(UserId);
                    if (info != null)
                    {
                        info.Headurl = filePath;
                        await _userService.Update(info);
                    }


                    //向任务REDIS中写数据 
                    //  await _taskService.SetTaskToRedis(ActionType.Xgtx, 1, long.Parse(UserId.ToString()), true);
                    return result;
                }
                else
                {
                    result.Code = (int)ResultCode.warning;
                    result.Message = $"禁用图片:" + message;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail; ;
                result.Message = ex.Message;
                return result;
            }

        }
        /// <summary>
        /// 【上传图片】，返回上传的数组  FromForm{file } 使用post body form-data 形式传递文件流默认第一个
        /// 【返回值 headurl：用户头像地址】
        /// </summary>
        /// <returns>图片数组</returns>
        [HttpPost()]
        [Consumes("application/json", "multipart/form-data")]
        private async Task<Result> SetImg([FromForm] IFormCollection formFiles)
        {
            Result result = new Result();
            try
            {
                var form = formFiles;//定义接收类型的参数
                Hashtable hash = new Hashtable();
                IFormFileCollection cols = Request.Form.Files;
                if (cols == null || cols.Count == 0)
                {
                    result.Code = (int)ResultCode.Fail; ;
                    result.Message = "未获取到文件！";
                    return result;

                }
                List<string> pathList = new List<string>();
                foreach (var item in cols)
                {
                    var path = await Task<string>.Run(() =>
                    {
                        //  return FtpFile.UploadReturnUrl(item);
                        return "";
                    });
                    pathList.Add(path);
                }
                result.Message = "成功上传";
                result.Data = string.Join(',', pathList.ToArray());
                return result;
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail; ;
                result.Message = ex.Message;
                return result;

            }

        }
        /// <summary>
        ///   获取系统头像列表 【废弃暂时不用】
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>返回头像列表和头像数量</returns>
        [HttpGet()]
        public IActionResult GetHeadImg(int pageIndex = 1, int pageSize = 10)
        {

            ResultAPP result = new ResultAPP();
            result.Code = 200;
            var list = new List<object>();
            for (int i = 0; i < pageSize; i++)
            {
                if (10 < i)
                {
                    list.Add(new { headurl = $" " });
                }
                else
                {
                    list.Add(new { headurl = $"http://file.lianggehuangli.cn/image/logo{i}.jpg" });
                }

            }

            result.Message = new
            {
                count = pageSize,
                data = list
            };
            return Json(result);
        }


        static Dictionary<int, string> _gradeDefine = new Dictionary<int, string>
        {
            //{-1,"全部" },
            {1,"一年级" },
            {2,"二年级" },
            {3,"三年级" },
            {4,"四年级" },
            {5,"五年级" },
            {6,"六年级" }
        };

        static Dictionary<int, string> _gradeTeamDefine = new Dictionary<int, string>
        {
            {11,"一年级上" },
            {12,"一年级下" },
            {21,"二年级上" },
            {22,"二年级下" },
            {31,"三年级上" },
            {32,"三年级下" },
            {41,"四年级上" },
            {42,"四年级下" },
            {51,"五年级上" },
            {52,"五年级下" },
            {61,"六年级上" },
            {62,"六年级下" }
        };

        /// <summary>
        /// 【获取班级、年级信息】 默认获取当前用户的班级信息，和全部班级信息
        /// </summary>
        /// <param name="typeId">默认为1正常年级（没上下册） typeId=2 听写专用年级</param>
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        public async Task<GradeResult> GetGradeList(int typeId = 1)
        {
            GradeResult result = new GradeResult();

            //TIP: 别扭，以后配合app改，直接返回字典就行
            try
            {
                if (typeId == 1)
                {
                    result.Data = _gradeDefine.Select(n => new
                    {
                        KeyData = n.Key,
                        ValueData = n.Value
                    });
                }
                else
                {
                    result.Data = _gradeTeamDefine.Select(n => new
                    {
                        KeyData = n.Key,
                        ValueData = n.Value,
                    });
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"获取失败{ex.Message}";
                Logger.Error(ex);
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// 【设置默认】 
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<GradeResult> SetGradeList()
        {
            GradeResult result = new GradeResult();
            try
            {
                //
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"获取失败{ex.Message}";
                Logger.Error(ex);
            }
            return await Task.FromResult(result);
        }

        #endregion

        #region 编辑用户地址信息  

        /// <summary>
        /// 获取收货地址信息(获取一条数据) 
        /// </summary> 
        /// <returns></returns>
        [HttpGet()]
        public async Task<AddressResult> GetAddress()
        {
            try
            {
                var rlt = await _userService.GetAddress(UserId);
                return new AddressResult { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Logger.Error($"获取用户地址异常,用户Id：{UserId}", ex);
                return new AddressResult() { Code = (int)ResultCode.Exception, Message = $"获取用户地址异常" };
            }

        }
        /// <summary>
        /// 获取收货地址列表信息
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<Result<List<AddressInfo>>> GetAddressInfos()
        {
            try
            {
                var rlt = await _userService.GetAddressInfos(UserIdValue);
                return new Result<List<AddressInfo>> { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Logger.Error($"获取用户地址异常,用户Id：{UserId}", ex);
                return new Result<List<AddressInfo>>() { Code = (int)ResultCode.Exception, Message = $"获取用户地址异常" };
            }

        }



        /// <summary>
        /// 编辑用户地址信息 （存在修改 不存在添加 /GetAddress）
        /// 【返回值 200 成功】
        /// </summary>
        /// <param name="consignee">收货人</param>
        /// <param name="sex">性别 2未知 1男 0 女</param>
        /// <param name="mobile">手机号 </param>
        /// <param name="provincial">省</param>
        /// <param name="city">市 </param>
        /// <param name="areas">区</param>
        /// <param name="adress">详细地址</param>
        /// <param name="isCommonly">是否常用地址 默认1 常用 0 不常用</param>
        /// <returns>返回结果值</returns>
        [HttpPost()]
        public async Task<Result> AddAddress([FromForm] string consignee, [FromForm] int sex, [FromForm] string mobile, [FromForm] string provincial, [FromForm] string city, [FromForm] string areas, [FromForm] string adress, [FromForm] int isCommonly = 1)
        {
            //获取用户 
            Result result = new Result();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "修改失败";
                    return result;
                }
                if (!string.IsNullOrEmpty(consignee))
                {
                    var resultVCR = VCRApiHelper.ResultTextDefined(consignee);
                    if (resultVCR.Code == (int)ResultCode.warning)
                    {
                        return new Result() { Code = (int)ResultCode.warning, Message = $"收货人含有敏感字符:" + resultVCR.Message + ",请更换!" };
                    }
                }
                if (!string.IsNullOrEmpty(adress))
                {
                    var resultVCR = VCRApiHelper.ResultTextDefined(adress);
                    if (resultVCR.Code == (int)ResultCode.warning)
                    {

                        return new Result() { Code = (int)ResultCode.warning, Message = $"收货详细地址含有敏感字符:" + resultVCR.Message + ",请更换!" };
                    }
                }

                var userInfo = await _userService.GetUser(UserIdValue);
                var useAddress = await _addressService.Get(userInfo.Userid);
                var info = useAddress.Data;
                if (info == null)
                {
                    info = new AddressInfo();
                    info.Consignee = consignee;
                    info.Sex = sex;
                    info.Phone = mobile;
                    info.Provincial = provincial;
                    info.City = city;
                    info.Areas = areas;
                    info.Address = adress;
                    info.Userid = (int)userInfo.Userid;
                    info.Iscommonly = isCommonly;
                    _addressService.Add(info);
                    //向任务REDIS中写数据 
                    //  await _taskService.SetTaskToRedis(ActionType.Wsshdz, 1, userInfo.Userid);
                    result.Message = "添加成功";
                }
                else
                {
                    info.Consignee = consignee;
                    info.Sex = sex;
                    info.Phone = mobile;
                    info.Provincial = provincial;
                    info.City = city;
                    info.Areas = areas;
                    info.Address = adress;
                    info.Userid = (int)userInfo.Userid;
                    info.Iscommonly = isCommonly;
                    _addressService.Update(info);
                    //await _taskService.SetTaskToRedis(ActionType.Wsshdz, 1, userInfo.Userid);
                    result.Message = "修改成功";
                }

                #region  最后同步user地址
                if (!string.IsNullOrEmpty(provincial))
                {
                    userInfo.Provincial = provincial;
                }
                if (!string.IsNullOrEmpty(city))
                {
                    userInfo.City = city;
                }
                if (!string.IsNullOrEmpty(areas))
                {
                    userInfo.Areas = areas;
                }
                if (!string.IsNullOrEmpty(adress))
                {
                    userInfo.Address = adress;
                }
                if (userInfo != null)
                {
                    await _userService.Update(userInfo);
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;

        }

        #endregion

        #region 检查更新获取版本号、清楚用户缓存日志  

        /// <summary>
        /// 获取最新版本及其版本更新日志
        /// 【返回值 200 成功】
        /// </summary>
        /// <param name="osSign">系统标识 0= 安卓 or 1= Ios 获取header里 aid 与当前版本version</param>
        /// <returns>返回最新版本信息和版本日志信息</returns>
        [HttpGet()]
        [AllowAnonymous]
        public async Task<VersionResult> GetVersionCode(int osSign = 0)
        {
            VersionResult result = new VersionResult();
            try
            {
                string Aid = GetAid();
                int currentVersion = GetVersion();
                result.Message = "获取版本信息成功";
                var lrt = await _versionService.GetVersionInfo(osSign, Aid, currentVersion);
                result.Data = lrt.Data;
            }
            catch (Exception e)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "获取版本信息失败" + e.Message;
            }
            return result;
        }
        /// <summary>
        /// 指定历史版本号
        /// </summary>
        /// <param name="Version"></param>
        /// <param name="Aid">空从header取</param>
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        public async Task<Result<VersionLog>> GetLogVersionCode(string Version, string Aid)
        {
            Result<VersionLog> result = new Result<VersionLog>();
            try
            {
                if (string.IsNullOrEmpty(Aid))
                {
                    Aid = GetAid();
                }
                int currentVersion = GetVersion();
                var lrt = await _versionService.GetVersionCode(Version, Aid);
                result.Code = (int)ResultCode.Success;
                result.Message = "获取版本信息成功";
                result.Data = lrt.Data;

            }
            catch (Exception e)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "获取版本信息失败" + e.Message;
            }
            return result;
        }

        /// <summary>
        /// 创建版本记录 
        /// </summary>
        /// <param name="createVersion">版本号 例如 1.0.0  版本描述信息  系统标识 0 安卓 1 IOS</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result> CreateVersion(CreateVersionInfo createVersion)
        {
            Result result = new Result();
            try
            {
                var version = createVersion.MapTo<VersionLog>();

                result.Message = "获取版本信息成功";
                var lrt = await _versionService.GetVersionCode(createVersion.VersionCode, createVersion.Aid);
                VersionLog info = lrt.Data;
                if (info != null)
                {
                    version.Id = info.Id;
                    bool flag = _versionService.Update(version);
                    result.Message = flag == true ? "修改版本信息成功！" : "修改版本信息失败！";

                }
                else
                {
                    long flag = (long)_versionService.Add(version);
                    result.Message = flag > 0 ? "新增版本信息成功！" : "新增版本信息失败！";
                }
            }
            catch (Exception e)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "新增或修改版本信息异常";
            }
            return result;
        }
        /// <summary>
        /// 删除接口
        /// </summary>
        /// <param name="Version"></param>
        /// <param name="Aid"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result> DeleteVersion(string Version, string Aid)
        {
            Result result = new Result();
            try
            {
                var lrt = await _versionService.GetVersionCode(Version, Aid);
                VersionLog info = lrt.Data;
                bool flag = _versionService.Delete(info);
                result.Message = flag == true ? "删除版本信息成功！" : "删除版本信息失败！";
            }
            catch (Exception e)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "删除版本信息异常";
            }
            return result;
        }
        /// <summary>
        /// 修改版本记录 
        /// </summary>
        /// <param name="updateVersion">版本号 例如 1.0.0  版本描述信息  系统标识 0 安卓 1 IOS</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result> updateVersion(UpdateVersionInfo updateVersion)
        {
            Result result = new Result();
            try
            {
                var lrt = await _versionService.GetVersionCode(updateVersion.VersionCode, updateVersion.Aid);
                VersionLog info = lrt.Data;
                if (info != null)
                {
                    var version = updateVersion.MapTo<VersionLog>();
                    bool flag = _versionService.Update(version);
                    result.Message = flag == true ? "修改版本信息成功！" : "修改版本信息失败！";
                }
                else
                {
                    result.Message = "未找到此版本";
                }
            }
            catch (Exception e)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "修改版本信息异常";
            }
            return result;
        }


        #endregion

        #region 银行卡信息、余额信息 编辑银行卡信息  
        /// <summary>
        /// 获取账号余额信息和账号银行卡列表信息 
        /// </summary> 
        /// <returns></returns>
        [HttpGet()]
        public async Task<BankResult> GetBank()
        {
            BankResult result = new BankResult();
            try
            {
                var userInfo = await _userService.GetUser(UserIdValue);
                var list = _userBankService.Table().Where(n => n.UserId == userInfo.Userid).ToList();
                var newList = new List<BankInfo>();
                foreach (var itemEntity in list)
                {
                    newList.Add(new BankInfo
                    {
                        UserName = itemEntity.UserName,
                        BankName = itemEntity.BankName,
                        BankCode = itemEntity.BankCode,
                        BankPhone = itemEntity.BankPhone,
                        UserId = itemEntity.UserId,
                        BankAbbrName = itemEntity.BankName.Substring(0, (itemEntity.BankName.IndexOf('|')))
                    });
                }


                result.Data = new BankListInfo()
                {
                    MoneyTotal = userInfo.MoneyTotal,
                    BankList = newList,
                };
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;

                result.Message = $"获取信息失败{ex.Message}";
            }
            return result;
        }

        /// <summary>
        /// 设置账号余额信息和账号银行卡列表信息
        /// </summary>
        /// <param name="userName">用户姓名，</param>
        /// <param name="bankcode">银行卡号 测试账号：6228480402564890018 (随意修改后缀字符) 可以自己百度，需要根据卡号查询银行名称</param>
        /// <param name="bankphone">绑定银行卡的手机号码  默认为空</param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<Result> EditBank([FromForm] string userName, [FromForm] string bankcode, [FromForm] string bankphone)
        {
            Result result = new Result();
            try
            {
                var userInfo = await _userService.GetUser(UserIdValue);
                if (string.IsNullOrWhiteSpace(bankcode))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Data = "";
                    result.Message = $"添加失败，bankcode为空";
                }
                else
                {
                    var info = _userBankService.Table().Where(n => n.UserId == userInfo.Userid)?.FirstOrDefault();
                    if (info == null)
                    {
                        //TODO,需要判断短信验证码是否正确
                        var id = (long)_userBankService.Create(new UserBankEntity()
                        {
                            UserName = userName.Replace(@"""", ""),
                            UserId = userInfo.Userid,
                            BankCode = bankcode.Replace(@"""", ""),
                            BankPhone = bankphone.Replace(@"""", ""),
                            BankName = BankUtil.GetBankName(bankcode.Replace(@"""", "").ToCharArray()),
                            CreateTime = DateTime.Now
                        });

                        if (id > 0)
                        {
                            result.Code = (int)ResultCode.Success;
                            result.Data = id.ToString();
                            result.Message = "添加成功";
                            //向任务REDIS中写数据 
                            //await _taskService.SetTaskToRedis(ActionType.Tjyhk, 1, userInfo.Userid, true);

                        }
                        else
                        {
                            result.Code = (int)ResultCode.Fail;
                            result.Data = id.ToString();
                            result.Message = "添加失败";
                        }

                    }
                    else
                    {
                        //已存在去修改
                        info.UserName = userName.Replace(@"""", "");
                        info.UserId = userInfo.Userid;
                        info.BankCode = bankcode.Replace(@"""", "");
                        info.BankPhone = bankphone.Replace(@"""", "");
                        info.BankName = BankUtil.GetBankName(info.BankCode.ToCharArray());
                        info.CreateTime = DateTime.Now;
                        _userBankService.Update(info);
                        //向任务REDIS中写数据 
                        // await _taskService.SetTaskToRedis(ActionType.Tjyhk, 1, userInfo.Userid, true);
                        result.Message = "修改成功";
                        result.Code = (int)ResultCode.Success;
                        result.Data = "";
                        return result;
                    }

                }

            }
            catch (Exception ex)
            {

                result.Code = (int)ResultCode.Fail;
                result.Message = $"添加失败{ex.Message}";
                Logger.Error(ex);
            }


            return result;

        }

        #endregion

        #region 个人中心 我的课程 获取收藏和已购买列表
        /// <summary>
        /// 【个人中心 我的课程】获取收藏和已购买列表信息 【IsLive 1表示直播 0表示没有直播】  todo
        /// </summary>
        /// <param name="type">type 1已购买 2收藏 </param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IsLive </returns>
        [HttpGet()]
        public async Task<MyCourseResult> GetMyCourseNoCacheList(int type = 1, int pageIndex = 1, int pageSize = 10)
        {

            MyCourseResult result = new MyCourseResult();
            if (string.IsNullOrEmpty(UserId))
            {
                result.Code = (int)ResultCode.Fail;
            }
            else
            {

                var nList = new PageList<MyCourseOrderCollectInfo>();
                var nliveList = new PageList<MyCourseOrderCollectInfo>();
                var list = _userService.GetMyCourseOrderCollectList(type, int.Parse(UserId), pageIndex, pageSize);
                var rltcourselist = await _courseService.GetAll();

                foreach (CourseBuyCollectEntity item in list)
                {
                    MyCourseOrderCollectInfo colitem = new MyCourseOrderCollectInfo();
                    // var lrt = await _courseService.GetAsync(item.CourseId);
                    var courseEntity = rltcourselist.FirstOrDefault(p => p.Id == item.CourseId);
                    string SecretKey = "";
                    bool authen = false;
                    if (courseEntity != null)
                    {
                        var childrlt = await _courseService.GetChildCountAsync(long.Parse(UserId), item.CourseId);
                        var userchildrlt = await _courseService.GetUserChildCountAsync(long.Parse(UserId), item.CourseId);
                        SecretKey = courseEntity.SecretKey;
                        if (!string.IsNullOrEmpty(SecretKey))
                        {
                            authen = true;
                        }
                        colitem.cbid = item.Id;
                        colitem.Id = item.CourseId;
                        colitem.childIds = courseEntity.ChildIds;
                        colitem.Cateorys = courseEntity.Cateorys;
                        colitem.CourseEndTime = courseEntity.ValidEndTime.ToString("yyyy-MM-dd");
                        colitem.CourseStartTime = courseEntity.ValidBeginTime.ToString("yyyy-MM-dd");
                        colitem.DateTime = courseEntity.CreateTime.ToString("yyyy-MM-dd");
                        colitem.CoverImg = courseEntity.CoursePicPhone;
                        colitem.PicPhoneMini = courseEntity.CoursePicPhoneMini;
                        colitem.TeachersUserName = courseEntity.TeachersUserName;
                        colitem.IsLive = courseEntity.IsLive;
                        colitem.LiveStartTime = courseEntity.LiveBeginTime.ToString("yyyy-MM-dd");
                        colitem.LiveEndTime = courseEntity.LiveEndTime.ToString("yyyy-MM-dd");
                        colitem.Title = courseEntity.CourseName;
                        colitem.Detailed = courseEntity.Detailed;
                        colitem.isbuy = item.IsBuy;
                        colitem.IsRecord = courseEntity.IsRecord;
                        if (item.IsBuy == 1)
                        {
                            colitem.PayTime = item.CreateTime.ToString("yyyy-MM-dd"); //item.ExpirationDate.AddYears(-1);
                            colitem.ExpirationDate = item.ExpirationDate.ToString("yyyy-MM-dd");
                            colitem.IsHide = item.IsHide;
                            if (DateTime.Compare(DateTime.Now, item.ExpirationDate) > 0)
                            {
                                colitem.exmsg = "此课程免费期已过，建议购买观看";
                                colitem.Isexpired = true;
                            }
                            else
                            {
                                colitem.exmsg = "";
                                colitem.Isexpired = false;
                            }
                        }
                        else
                        {
                            colitem.IsHide = 0;
                            colitem.exmsg = "";
                            colitem.Isexpired = false;
                        }
                        colitem.authen = authen;
                        colitem.SecretKey = SecretKey;
                        #region 处理进度
                        var childTotal = childrlt.Data.Count;
                        var userchildTotal = userchildrlt.Data.Count;
                        if (childTotal > 0)
                        {
                            colitem.TotalDone = childTotal;
                        }
                        else
                        {
                            colitem.TotalDone = 0;
                        }
                        if (userchildTotal > 0)
                        {
                            colitem.AlreadyDone = userchildTotal;
                        }
                        else
                        {
                            colitem.AlreadyDone = 0;
                        }
                        int m = colitem.AlreadyDone;
                        int n = colitem.TotalDone;
                        float x;
                        x = (float)m / n;
                        colitem.progress = x;
                        #endregion
                        nList.Add(colitem);
                    }
                }
                //nliveList.AddRange(nList);
                result.Data = new MyCourseList()
                {
                    pageIndex = list.PageIndex,
                    pageSize = list.PageSize,
                    totalCount = list.TotalCount,
                    MyCourseOrderCollectList = nList,
                };
            }
            return result;

        }
        /// <summary>
        /// 【个人中心 我的课程】获取收藏和已购买列表信息 【IsLive 1表示直播 0表示没有直播】  todo
        /// </summary>
        /// <param name="type">type 1已购买 2收藏 </param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IsLive </returns>
        [HttpGet()]
        public async Task<MyCourseResult> GetMyCourseList(int type = 1, int pageIndex = 1, int pageSize = 10)
        {

            MyCourseResult result = new MyCourseResult();
            if (string.IsNullOrEmpty(UserId))
            {
                result.Code = (int)ResultCode.Fail;
            }
            else
            {
                //先从推荐缓存里去朗读推荐的课程 再去过滤缓存里的数据 
                Expression<Func<CourseBycollectModel, bool>> predicate = null;
                Action<Orderable<CourseBycollectModel>> preorder = null;
                predicate = n => n.UserId == int.Parse(UserId);

                switch (type)
                {

                    case 1: //已购买
                        preorder = order => order.Desc(n => n.CreateTime);
                        predicate = n => n.UserId == int.Parse(UserId) && n.IsBuy == 1 && n.IsHide == 0;
                        break;
                    case 2:  //收藏
                        preorder = order => order.Desc(n => n.CreateTime);
                        predicate = n => n.UserId == int.Parse(UserId) && n.IsCollect == 1 && n.IsHide == 0;
                        break;
                    default:
                        preorder = order => order.Desc(n => n.CreateTime);
                        predicate = n => n.UserId == int.Parse(UserId) && n.IsHide == 0;
                        break;
                }

                var nList = new PageList<MyCourseOrderCollectInfo>();
                var list = new PageList<CourseBycollectModel>();

                list = await _courseBuyService.GetPageListAsync(predicate, preorder, pageSize, pageIndex);
                var rltcourselist = await _courseService.GetAll();

                foreach (CourseBycollectModel item in list)
                {
                    MyCourseOrderCollectInfo colitem = new MyCourseOrderCollectInfo();
                    // var lrt = await _courseService.GetAsync(item.CourseId);
                    var courseEntity = rltcourselist.FirstOrDefault(p => p.Id == item.CourseId);
                    string SecretKey = "";
                    bool authen = false;
                    if (courseEntity != null)
                    {
                        var childrlt = await _courseService.GetChildCountAsync(long.Parse(UserId), item.CourseId);
                        var userchildrlt = await _courseService.GetUserChildCountAsync(long.Parse(UserId), item.CourseId);
                        SecretKey = courseEntity.SecretKey;
                        if (!string.IsNullOrEmpty(SecretKey))
                        {
                            authen = true;
                        }
                        colitem.cbid = item.Id;
                        colitem.Id = item.CourseId;
                        colitem.childIds = courseEntity.ChildIds;
                        colitem.Cateorys = courseEntity.Cateorys;
                        colitem.CourseEndTime = courseEntity.ValidEndTime.ToString("yyyy-MM-dd");
                        colitem.CourseStartTime = courseEntity.ValidBeginTime.ToString("yyyy-MM-dd");
                        colitem.DateTime = courseEntity.CreateTime.ToString("yyyy-MM-dd");
                        colitem.CoverImg = courseEntity.CoursePicPhone;
                        colitem.PicPhoneMini = courseEntity.CoursePicPhoneMini;
                        colitem.TeachersUserName = courseEntity.TeachersUserName;
                        colitem.IsLive = courseEntity.IsLive;
                        colitem.LiveStartTime = courseEntity.LiveBeginTime.ToString("yyyy-MM-dd");
                        colitem.LiveEndTime = courseEntity.LiveEndTime.ToString("yyyy-MM-dd");
                        colitem.Title = courseEntity.CourseName;
                        colitem.Detailed = courseEntity.Detailed;
                        colitem.isbuy = item.IsBuy;
                        colitem.IsRecord = courseEntity.IsRecord;
                        if (item.IsBuy == 1)
                        {
                            colitem.PayTime = item.CreateTime.ToString("yyyy-MM-dd"); //item.ExpirationDate.AddYears(-1);
                            colitem.ExpirationDate = item.ExpirationDate.ToString("yyyy-MM-dd");
                            colitem.IsHide = item.IsHide;
                            if (DateTime.Compare(DateTime.Now, item.ExpirationDate) > 0)
                            {
                                colitem.exmsg = "此课程免费期已过，建议购买观看";
                                colitem.Isexpired = true;
                            }
                            else
                            {
                                colitem.exmsg = "";
                                colitem.Isexpired = false;
                            }
                        }
                        else
                        {
                            colitem.IsHide = 0;
                            colitem.exmsg = "";
                            colitem.Isexpired = false;
                        }
                        colitem.authen = authen;
                        colitem.SecretKey = SecretKey;
                        #region 处理进度
                        var childTotal = childrlt.Data.Count;
                        var userchildTotal = userchildrlt.Data.Count;
                        if (childTotal > 0)
                        {
                            colitem.TotalDone = childTotal;
                        }
                        else
                        {
                            colitem.TotalDone = 0;
                        }
                        if (userchildTotal > 0)
                        {
                            colitem.AlreadyDone = userchildTotal;
                        }
                        else
                        {
                            colitem.AlreadyDone = 0;
                        }
                        int m = colitem.AlreadyDone;
                        int n = colitem.TotalDone;
                        float x;
                        x = (float)m / n;
                        colitem.progress = x;
                        #endregion
                        nList.Add(colitem);
                    }
                }
                //nliveList.AddRange(nList);
                result.Data = new MyCourseList()
                {
                    pageIndex = list.PageIndex,
                    pageSize = list.PageSize,
                    totalCount = list.TotalCount,
                    MyCourseOrderCollectList = nList,
                };
            }
            return result;

        }

        /// <summary>
        /// 清空购买与收藏的课程并加载
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/SetBuyCollectClearCache")]
        [AllowAnonymous]
        public async Task<bool> SetBuyCollectClearCache()
        {

            var isok = _courseBuyService.BuycollectClearCache();

            return await Task.FromResult(isok);
        }
        /// <summary>
        /// 设置过期不显示课程
        /// </summary>
        /// <param name="ishide"></param>
        /// <param name="cbid"></param>
        /// <returns></returns>
        [HttpPost]
        public Result MyCourseListIshide([FromForm] int ishide, [FromForm] int cbid)
        {
            try
            {
                if (string.IsNullOrEmpty(UserId))
                {
                    return new Result { Code = (int)ResultCode.Fail };
                }
                else
                {
                    var result = _courseBuyService.UpdateIshide(ishide, cbid);
                    return new Result { Code = (int)ResultCode.Success, Data = result.ToString() };
                }

            }
            catch (Exception ex)
            {
                return new Result { Code = (int)ResultCode.Fail, Message = ex.Message };
            }
        }
        /// <summary>
        /// 批量设置我的课程里的过期时间值 
        /// </summary>
        /// <param name="courseid">对一门课修改过期时间</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<Result> MyCourseListSetExpirationDate(int courseid)
        {
            List<MyCourseOrderCollectInfo> bylist = new List<MyCourseOrderCollectInfo>();
            var list = _userService.GetMyCourseOrderCollect(courseid);

            int count = 0;
            foreach (CourseBuyCollectEntity item in list)
            {
                MyCourseOrderCollectInfo colitem = new MyCourseOrderCollectInfo();
                var lrt = await _courseService.GetAsync(item.CourseId);
                var courseEntity = lrt.Data;
                if (courseEntity != null)
                {
                    colitem.cbid = item.Id;
                    colitem.Id = item.CourseId;
                    #region 处理有效期
                    colitem.CourseStartTime = courseEntity.ValidBeginTime.ToString("yyyy-MM-dd");
                    colitem.CourseEndTime = courseEntity.ValidEndTime.ToString("yyyy-MM-dd");
                    colitem.IsLive = courseEntity.IsLive;
                    colitem.LiveEndTime = courseEntity.LiveEndTime.ToString("yyyy-MM-dd");
                    colitem.LiveStartTime = courseEntity.LiveBeginTime.ToString("yyyy-MM-dd");
                    //var ExpirationDate = colitem.CourseEndTime;
                    #endregion
                    bool isok = _courseBuyService.UpdateExpirationDate(courseEntity.ValidEndTime, item.Id);
                    if (isok)
                    {
                        count++;
                    }
                }
            }
            return new Result { Code = (int)ResultCode.Success, Data = "修改有效期成功条数" + count };

        }
        #endregion


        #region  清空用户相关缓存并加载
        /// <summary>
        /// 清空用户缓存并加载
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/SetUserClearCache")]
        [AllowAnonymous]
        public async Task<bool> SetuserClearCache()
        {

            var isok = _userService.userClearCache();

            return await Task.FromResult(isok);
        }
        #endregion

        #region 清空版本并加载
        /// <summary>
        /// 清空版本并加载
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/SetVersionClearCache")]
        [AllowAnonymous]
        public async Task<bool> SetVersionClearCache()
        {

            var isok = _versionService.VersionClearCache();

            return await Task.FromResult(isok);
        }
        #endregion

        #region 意见反馈

        /// <summary>
        /// 获取意见反馈信息
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public FeedbackResult GetFeedback()
        {
            var result = new FeedbackResult();
            try
            {
                var info = _feedbackService.Table().Where(n => n.TenantId == 1).FirstOrDefault();
                result.Data = new FeedbackInfo()
                {
                    Phone = $"热线：{info.Phone}",
                    Email = $"邮箱：{info.Email}"
                };
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                Logger.Error(ex);
            }
            return result;
        }

        #endregion
        #region 用户意见反馈

        /// <summary>
        /// 获取用户反馈信息
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public UserFeedbackResult GetUserFeedback()
        {
            var result = new UserFeedbackResult();
            try
            {
                var info = _userFeedbackService.Table().Where(n => n.UserId == UserIdValue).OrderByDescending(t => t.CreateTime).FirstOrDefault();
                result.Data = new UserFeedbackInfo()
                {
                    Phone = $"电话：{info.Phone}",
                    Content = $"内容：{info.Content}",
                    PicPath = info.PicPath
                };
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                Logger.Error(ex);
            }
            return result;
        }

        #endregion
        /// <summary>
        /// 添加用户问题反馈 picpath："多图片之间用|分割"
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<Result> AddUserFeedback([FromBody] UserFeedbackInfo req)
        {
            //获取用户 
            Result result = new Result();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "用户ID不能为空";
                    return result;
                }
                _userFeedbackService.Create(req);
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;

        }
        /// <summary>
        /// 获取购买当前时间下课程的用户
        /// </summary>
        /// <param name="datetime">时间：2020/03/16 13:50:00</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public Result<List<UserInfoOfMsg>> GetUserInfo(string datetime)
        {
            try
            {
                var rlt = _userService.GetListOfMsg(datetime);
                return new Result<List<UserInfoOfMsg>> { Code = (int)ResultCode.Success, Data = rlt };
            }
            catch (Exception ex)
            {
                Logger.Error("GetUserInfo", ex);
                return new Result<List<UserInfoOfMsg>> { Code = (int)ResultCode.Fail, Message = ex.Message };
            }
        }

    }
}
