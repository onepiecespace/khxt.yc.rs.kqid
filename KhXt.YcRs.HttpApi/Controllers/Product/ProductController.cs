﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Product;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Product;
using KhXt.YcRs.HttpApi.Models.Product;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Product
{
    /// <summary>
    /// 金币商城模块
    /// </summary>
    [Route("api/pro")]
    [ApiController]
    public class ProductController : BaseApiController
    {
        #region ProductController
        private readonly IKVStoreService _kVStoreService;
        private readonly IPro_classService _proclassService;
        private readonly IPro_listService _prolistService;
        private readonly IPro_attributeService _proattributeService;
        public ProductController(IKVStoreService kVStoreService, IPro_classService proclassService, IPro_listService prolistService, IPro_attributeService proattributeService
            )
        {
            _kVStoreService = kVStoreService;
            _proclassService = proclassService;
            _prolistService = prolistService;
            _proattributeService = proattributeService;
        }
        #endregion



        #region 商品分类
        /// <summary>
        /// 获取商品分类数据 （已新增数据）
        /// </summary> 
        /// <param name="sid">默认传0</param>
        /// <returns></returns>
        [HttpGet("cate/{sid}")]
        public async Task<ProCategoryResult> GetCategorySid(int sid = 0)
        {

            try
            {
                var rlt = await _proclassService.GetList(sid);
                if (rlt.Data == null)
                {
                    return new ProCategoryResult() { Code = (int)ResultCode.onlineing, Data = rlt.Data, Message = "暂未上线" };
                }
                else
                {
                    return new ProCategoryResult() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new ProCategoryResult() { Code = (int)ResultCode.Exception, Message = $"获取失败{ex.Message}" };
            }

        }
        #region 获取商品分类
        /// <summary>
        /// 获取商品分类详情
        /// </summary> 
        /// <param name="cid">默认传0</param>
        /// <returns></returns>
        [HttpGet("cateinfo/{cid}")]
        private async Task<Result<ProCategoryInfo>> GetCategoryInfo(int cid)
        {
            try
            {
                var rlt = await _proclassService.Get(cid);
                return new Result<ProCategoryInfo>() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("GetCategoryInfo", ex);
                return new Result<ProCategoryInfo>() { Code = (int)ResultCode.Exception, Message = "读取商品分类详情失败！" };
            }
        }
        #endregion
        #endregion

        #region 商品首页
        /// <summary>
        /// 获取商品不分页加载列表 （已新增数据）
        /// </summary> 
        /// <param name="cid">传商品分类ID 默认0 全部  </param>
        /// <returns></returns>
        [HttpGet("pro/{cid}")]
        public async Task<ProListResult> GetProList(int cid = 0)
        {

            try
            {
                var rlt = await _prolistService.GetList(cid);
                return new ProListResult() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new ProListResult() { Code = (int)ResultCode.Exception, Message = $"获取失败{ex.Message}" };
            }

        }

        /// <summary>
        /// 获取分类首页商品推荐分页加载列表（已新增数据）
        /// </summary>
        /// <param name="cid">cateinfo/{cid}/1获取值  默认是0代表全部</param>
        /// <param name="s_type">产品值属性  0默认全部  1：新品,2：热销，3：推荐</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("propage/{cid}/{s_type}")]
        public async Task<ProPageResult> GetProPageList(int cid = 0, int s_type = 0, int pageIndex = 1, int pageSize = 10)
        {
            ProPageResult result = new ProPageResult();
            try
            {
                var rlt = await _prolistService.ProPageList(cid, s_type, pageIndex, pageSize);
                return new ProPageResult() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new ProPageResult() { Code = (int)ResultCode.Exception, Message = $"获取失败{ex.Message}" };
            }

        }

        #region 获取商品
        /// <summary>
        /// 获取商品详情
        /// </summary> 
        /// <param name="proId">传商品ID</param>
        /// <returns></returns>
        [HttpGet("Proinfo/{proId}")]
        public async Task<Result<ProDetailInfoDto>> GetProInfo(int proId)
        {
            try
            {
                var rlt = await _prolistService.Get(proId);
                return new Result<ProDetailInfoDto>() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("GetProInfo", ex);
                return new Result<ProDetailInfoDto>() { Code = (int)ResultCode.Exception, Message = "读取商品属性详情失败！" };
            }
        }
        #endregion
        #endregion

        #region 商品属性配置
        /// <summary>
        /// 获取商品属性数据 （已新增数据）
        /// </summary> 
        /// <param name="proid">传商品ID</param>
        /// <returns></returns>
        [HttpGet("att/{proid}")]
        private async Task<ProAttributeResult> GetAttributeProid(int proid)
        {

            try
            {
                var rlt = await _proattributeService.GetList(proid);
                return new ProAttributeResult() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new ProAttributeResult() { Code = (int)ResultCode.Exception, Message = $"获取失败{ex.Message}" };
            }

        }
        #region 获取商品属性
        /// <summary>
        /// 获取商品属性详情
        /// </summary> 
        /// <param name="Id">默认传商品属性</param>
        /// <returns></returns>
        [HttpGet("attrinfo/{Id}")]
        private async Task<Result<ProAttributeInfo>> GetAttributeInfo(int Id)
        {
            try
            {
                var rlt = await _proattributeService.Get(Id);
                return new Result<ProAttributeInfo>() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("GetCategoryInfo", ex);
                return new Result<ProAttributeInfo>() { Code = (int)ResultCode.Exception, Message = "读取商品属性详情失败！" };
            }
        }
        #endregion
        #endregion
    }
}