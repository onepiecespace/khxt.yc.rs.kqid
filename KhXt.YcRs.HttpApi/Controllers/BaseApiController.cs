﻿using Castle.Core.Internal;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.JWT;
using KhXt.YcRs.Domain.Util;
using Hx.Components;
using Hx.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Senparc.NeuChar.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers
{
    [ApiController]
    public class BaseApiController : Controller
    {
        private const string HEADER_TENANT_ID = "tid";
        private const string HEADER_AGENT_ID = "aid";
        private const string HEADER_VERSION = "ver";
        private const string HEADER_PLATFORM = "os";
        private const string HEADER_APPCODE = "appcode";
        private const string HEADER_RegistrationId = "registrationId";

        #region 用户id与手机号码
        //ver 数字版本  aid 代理商  tid 租户  os 操作系统类型 1苹果 2 安卓 4 PC
        /// <summary>
        /// 用户id
        /// </summary>
        protected string UserId { get; set; }
        /// <summary>
        /// 版本编译号 18 19 20
        /// </summary>
        protected int ver { get; set; }

        /// <summary>
        /// 版本编译编码 v1.3.6.dev
        /// </summary>
        protected string appcode { get; set; }
        /// <summary>
        /// 代理商 mxh bbk dsl
        /// </summary>
        protected string aid { get; set; }
        /// <summary>
        /// 租户编号 "100001" 默认"0"
        /// </summary>
        protected string tid { get; set; }
        /// <summary>
        /// 操作系统类型 1苹果 2 安卓 4 PC
        /// </summary>
        protected int os { get; set; }
        /// <summary>
        /// 设备极光注入Id
        /// </summary>
        protected string registrationId { get; set; }

        protected long UserIdValue { get; private set; }
        // protected long TenantId { get; private set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        protected string Phone { get; set; }


        private Hx.Redis.RedisHelper _redis;

        protected Hx.Redis.RedisHelper Redis
        {
            get
            {
                if (_redis != null) return _redis;
                _redis = ObjectContainer.Resolve<Hx.Redis.RedisHelper>();
                return _redis;
            }
        }

        private ILogger _logger;

        /// <summary>
        /// 
        /// </summary>
        protected ILogger Logger
        {
            get
            {
                if (_logger != null) return _logger;
                var factory = ObjectContainer.Resolve<ILoggerFactory>();
                _logger = factory.Create();
                return _logger;
            }
        }

        private DomainSetting _domainSetting;
        protected DomainSetting DomainSetting
        {
            get
            {
                if (_domainSetting != null) return DomainSetting;
                _domainSetting = ObjectContainer.Resolve<DomainSetting>();
                return _domainSetting;
            }
        }


        #endregion

        #region 暂存方法
        //        public override async Task OnActionExecutionAsync(ActionExecutingContext context,
        //            ActionExecutionDelegate next)
        //        {
        //            try
        //            {
        //                var dt = DateTime.Now;
        //                //按照小时统计接口访问频率
        //                var keyHh = RedisKeyHelper.CreateSysHhLog(dt.ToString("yyyy-MM-dd"));
        //                await Redis.HashIncrementAsync(keyHh, dt.Hour.ToString(), 1);
        //                var value = "";
        //                if (context.Controller is Controller controller)
        //                {
        //                    value = controller.Request.Path.Value;
        //                    if (value.IsNullOrEmpty())
        //                        value = "";
        //                }
        //                //按照接口统计接口访问频率
        //                var key = RedisKeyHelper.CreateSysLog(dt.ToString("yyyy-MM-dd"));
        //                await Redis.HashIncrementAsync(key, value, 1);
        //                var requestContext = context;
        //                var httpContext = requestContext.HttpContext;
        //                if (requestContext.Filters.Any(f => f is IAllowAnonymousFilter))
        //                {
        //                    return;
        //                }
        //                //检测是否包含'Authorization'请求头
        //                var tokenValue = httpContext.Request.Headers["token"].ToString();
        //                if (tokenValue.IsNullOrEmpty())
        //                {
        //#if DEBUG
        //                    //UserId = "107";
        //                    //Phone = "18210413749";
        //                    httpContext.Request.Headers.Add("token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxODIxMDQxMzc0OSIsImlhdCI6IjE1NzQzMjQ1MjQiLCJuYmYiOiIxNTc0MzI0NTI0IiwiZXhwIjoiMTU4MjI3MzMyNCIsImlzcyI6Ikh1YW5nTGlBUFAuV2ViIiwiYXVkIjoid3IiLCJ1c2VyaWQiOiIxMDcifQ.fHxhIJaRHZY0sxOiK2VWD4NnmuFPSCMSFaz3MXHUZjk");
        //#else
        //                var result = new Result
        //                {
        //                    Code = (int)ResultCode.NonAuthoritativeInformation,
        //                    Message = "非法请求"
        //                };
        //                requestContext.Result = new ObjectResult(result);
        //                base.OnActionExecuting(requestContext);
        //                return;
        //#endif
        //                }
        //                #region  获取header版本号
        //                if (!httpContext.Request.Headers.ContainsKey("version"))
        //                {
        //                    httpContext.Request.Headers.Add("version", "0");
        //                    Version = 0;
        //                }
        //                else
        //                {
        //                    var versionValue = httpContext.Request.Headers["version"];
        //                    Version = int.Parse(versionValue.ToString());
        //                }
        //                #endregion
        //                try
        //                {
        //                    var result = new Result();
        //                    var tm = GetToken(httpContext);
        //                    if (tm == null)
        //                    {
        //                        result.Code = (int)ResultCode.NonAuthoritativeInformation;
        //                        result.Message = "验权错误";
        //                        requestContext.Result = new ObjectResult(result);
        //                        return;
        //                    }

        //                    var token = RedisKeyHelper.CreateTokenKey(tm.Phone);

        //                    if (string.IsNullOrEmpty(token))
        //                    {
        //                        result.Code = (int)ResultCode.NonAuthoritativeInformation;
        //                        result.Message = "登录token已过期，请重新登录";
        //                        requestContext.Result = new ObjectResult(result);
        //                        //requestContext.Result = new ObjectResult(result.Message) { StatusCode = result.Code };
        //                        return;
        //                    }

        //                    UserId = tm.UserId.ToString();
        //                    Phone = tm.Phone;
        //                    UserIdValue = Convert.ToInt64(UserId);

        //                    //this.Request.EnableRewind();
        //                    await next();
        //                }
        //                catch (Exception ex)
        //                {
        //                    Logger.Error($"{DateTime.Now} Authorize wrong", ex);
        //                    var result = new Result
        //                    {
        //                        Code = (int)ResultCode.NonAuthoritativeInformation,
        //                        Message = "验权错误"
        //                    };
        //                    requestContext.Result = new ObjectResult(result);
        //                    return;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Console.WriteLine(ex);
        //                Logger.Error("记录接口请求数量异常");
        //            }
        //            await next();
        //        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestContext"></param>
        public override void OnActionExecuting(ActionExecutingContext requestContext)
        {
            var c = requestContext.ActionDescriptor;
            requestContext.HttpContext.Items.Add("routePath", $"{c.RouteValues["controller"]}/{c.RouteValues["action"]}");
            Task.Run(async () =>
            {
                var dt = DateTime.Now;
                //按照小时统计接口访问频率
                var keyHh = RedisKeyHelper.CreateSysHhLog(dt.ToString("yyyy-MM-dd"));
                await Redis.HashIncrementAsync(keyHh, dt.Hour.ToString(), 1);
                var value = "";
                if (requestContext.Controller is Controller controller)
                {
                    value = controller.Request.Path.Value;
                    if (value.IsNullOrEmpty())
                        value = "";
                }
                //按照接口统计接口访问频率
                var key = RedisKeyHelper.CreateSysLog(dt.ToString("yyyy-MM-dd"));
                await Redis.HashIncrementAsync(key, value, 1);

            });

            HttpContext httpContext = requestContext.HttpContext;

            var endpoint = httpContext.GetEndpoint();
            if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null)
            {
                return;
            }
            //检测是否包含'Authorization'请求头
            var tokenValue = httpContext.Request.Headers["token"].ToString();
            if (tokenValue.IsNullOrEmpty())
            {
#if DEBUG
                //UserId = "107";
                //Phone = "18210413749";
                //httpContext.Request.Headers.Add("token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxODIxMDQxMzc0OSIsImlhdCI6IjE1NzQzMjQ1MjQiLCJuYmYiOiIxNTc0MzI0NTI0IiwiZXhwIjoiMTU4MjI3MzMyNCIsImlzcyI6Ikh1YW5nTGlBUFAuV2ViIiwiYXVkIjoid3IiLCJ1c2VyaWQiOiIxMDcifQ.fHxhIJaRHZY0sxOiK2VWD4NnmuFPSCMSFaz3MXHUZjk");
#else
                var result = new Result
                {
                    Code = (int)ResultCode.NonAuthoritativeInformation,
                    Message = "非法请求"
                };
                requestContext.Result = new ObjectResult(result);
                base.OnActionExecuting(requestContext);
                return;
#endif
            }
            #region  获取header发布编译版本ver
            if (!httpContext.Request.Headers.ContainsKey("ver"))
            {
                httpContext.Request.Headers.Add("ver", "0");
                ver = 0;
            }
            else
            {
                var versionValue = httpContext.Request.Headers["ver"];
                ver = int.Parse(versionValue.ToString());
            }
            #endregion
            #region  获取header 代理商aid号
            if (!httpContext.Request.Headers.ContainsKey("aid"))
            {
                httpContext.Request.Headers.Add("aid", "college");
                aid = "college";
            }
            else
            {
                aid = httpContext.Request.Headers["aid"].ToString();
            }
            #endregion
            #region  获取header 加盟商租户tid号
            if (!httpContext.Request.Headers.ContainsKey("tid"))
            {
                httpContext.Request.Headers.Add("tid", "0");
                tid = "0";
            }
            else
            {
                tid = httpContext.Request.Headers["tid"].ToString();
                if (string.IsNullOrEmpty(tid))
                {
                    tid = "0";
                }
            }
            #endregion
            #region  获取header 操作系统类型 os
            if (!httpContext.Request.Headers.ContainsKey("os"))
            {
                httpContext.Request.Headers.Add("os", "2");
                os = 2;//不传默认安卓
            }
            else
            {
                var osValue = httpContext.Request.Headers["os"];
                os = int.Parse(osValue.ToString());
            }
            #endregion
            #region  获取header 版本编号
            if (!httpContext.Request.Headers.ContainsKey("appcode"))
            {
                httpContext.Request.Headers.Add("appcode", "0");
                appcode = "0";
            }
            else
            {
                appcode = httpContext.Request.Headers["appcode"].ToString();
                if (string.IsNullOrEmpty(appcode))
                {
                    appcode = "0";
                }
            }
            #endregion
            #region  获取header 设备编号
            if (!httpContext.Request.Headers.ContainsKey("registrationId"))
            {
                httpContext.Request.Headers.Add("registrationId", "");
                registrationId = "";
            }
            else
            {
                registrationId = httpContext.Request.Headers["registrationId"].ToString();
                if (string.IsNullOrEmpty(registrationId))
                {
                    registrationId = "";
                }
            }
            #endregion
            try
            {

                var result = new Result();
                var tm = GetToken(httpContext);
                if (tm == null)
                {
                    result.Code = (int)ResultCode.NonAuthoritativeInformation;
                    result.Message = "验权错误";
                    requestContext.Result = new ObjectResult(result);
                    return;
                }
                //查看Redis是否存在匹配项
                var token = Redis.StringGet("token:" + tm.Phone);
                //验证token是否过期
                if (string.IsNullOrEmpty(token))
                {
                    result.Code = (int)ResultCode.NonAuthoritativeInformation;
                    result.Message = "登录token已过期，请重新登录";
                    requestContext.Result = new ObjectResult(result);
                    //requestContext.Result = new ObjectResult(result.Message) { StatusCode = result.Code };
                    return;
                }
                //查看Redis是否存在regId匹配项
                var regIdToken = Redis.StringGet("regid:" + registrationId);
                //验证regId的token是否过期
                if (!string.IsNullOrEmpty(regIdToken))
                {
                    if (token != regIdToken)
                    {
                        result.Code = (int)ResultCode.NonAuthoritativeInformation;
                        result.Message = "检测到设备已在其他设备登陆，请重新验证登录";
                        requestContext.Result = new ObjectResult(result);
                        return;
                    }
                }
                UserId = tm.UserId.ToString();
                Phone = tm.Phone;
                UserIdValue = Convert.ToInt64(tm.UserId);
                //DomainEnv.CommonLogger.Info($"{DateTime.Now} CoreliAuthorize header:{"UserId:" + UserIdValue + "|ver:" + ver + "|aid:" + aid + "|tid:" + tid + "|os:" + os}");
                //TenantId = tm.TenantId;
                //this.Request.EnableRewind();
                base.OnActionExecuting(requestContext);
            }
            catch (Exception ex)
            {
                Logger.Error($"{DateTime.Now} Authorize wrong", ex);
                var result = new Result
                {
                    Code = (int)ResultCode.NonAuthoritativeInformation,
                    Message = "验权错误"
                };
                requestContext.Result = new ObjectResult(result);
                return;
            }
        }


        protected void FillRequestModel(RequestModelBase model)
        {
            if (model == null) return;
            if (Request.Headers.TryGetValue(HEADER_AGENT_ID, out var v))
            {
                model.AgentId = v;
            }
            else
            {
                model.AgentId = "college";
            }

            if (Request.Headers.TryGetValue(HEADER_TENANT_ID, out v))
            {
                model.TenantId = v;
            }

            if (Request.Headers.TryGetValue(HEADER_PLATFORM, out v))
            {
                model.Platform = v;
            }

            if (Request.Headers.TryGetValue(HEADER_VERSION, out v))
            {
                model.Version = v;
            }

            if (Request.Headers.TryGetValue(HEADER_APPCODE, out v))
            {
                model.AppCode = v;
            }
            if (Request.Headers.TryGetValue(HEADER_RegistrationId, out v))
            {
                model.RegistrationId = v;
            }
        }

        protected string GetHeader(string key)
        {
            if (Request.Headers.TryGetValue(key, out var v))
            {
                return v;
            }
            return "";
        }

        protected string GetAppCode()
        {
            return GetHeader(HEADER_APPCODE);
        }


        /// <summary>
        /// 获取token
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        private static TokenModel GetToken(HttpContext httpContext)
        {
            TokenModel tm = new TokenModel();
            var tokenHeader = httpContext.Request.Headers["token"].ToString();
            try
            {
                if (tokenHeader.Length >= 128)
                {
                    tm = JwtHelper.SerializeJWT(tokenHeader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{DateTime.Now} Authorize  wrong:{e.Message}");
                DomainEnv.CommonLogger.Error($"{DateTime.Now} Authorize wrong:{e.Message}");
                return null;
            }

            return tm;
        }


        /// <summary>
        /// 重写Authorize
        /// </summary>
        public class ApiAuthorize : ActionFilterAttribute
        {
            private Hx.Redis.RedisHelper _redis;
            protected Hx.Redis.RedisHelper Redis
            {
                get
                {
                    if (_redis != null) return _redis;
                    _redis = ObjectContainer.Resolve<Hx.Redis.RedisHelper>();
                    return _redis;
                }
            }
            public override void OnActionExecuting(ActionExecutingContext context)
            {
                var httpContext = context.HttpContext;
                if (context.Filters.Any(f => f is IAllowAnonymousFilter))
                {
                    return;
                }

                //检测是否包含'Authorization'请求头
                var key = httpContext.Request.Headers.Keys;
                var result = new Result();
                if (!httpContext.Request.Headers.ContainsKey("token"))
                {
                    result.Code = (int)ResultCode.NonAuthoritativeInformation;
                    result.Message = "非法请求";
                    context.Result = new ObjectResult(result);
                    base.OnActionExecuting(context);
                    return;
                }
                var tm = GetToken(context.HttpContext);
                //查看Redis是否存在匹配项
                var token = Redis.StringGet("token:" + tm.Phone);
                //验证token是否过期

                if (!string.IsNullOrEmpty(token)) return;
                //过期了，返回结果集
                result.Code = (int)ResultCode.NonAuthoritativeInformation;
                result.Message = "登录token已过期，请重新登录";
                context.Result = new ObjectResult((object)result);
                base.OnActionExecuting(context);
            }
        }

        public class ApiException : ExceptionFilterAttribute
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="context"></param>
            public override void OnException(ExceptionContext context)
            {
                DomainEnv.CommonLogger.Error(context.Exception);

            }
        }
    }
}