﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Services.Files;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx.Extensions;
using KhXt.YcRs.HttpApi.Models.Files;
using log4net.Repository.Hierarchy;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Files
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [AllowAnonymous]
    public class FilesController : BaseApiController
    {
        private readonly IFilesService _iFilesService;

        public FilesController(IFilesService iFilesService)
        {
            _iFilesService = iFilesService;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="formFiles"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<FilesResultViewModel> UploadFiles([FromForm] IFormCollection formFiles)
        {
            if (formFiles.Files.Count <= 0)
                return new FilesResultViewModel() { Code = (int)ResultCode.Fail, Message = "上传失败，文件为空" };
            var filePath = "";
            var startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1), TimeZoneInfo.Local); // 当地时区
            foreach (var file in formFiles.Files)
            {
                var timeStamp = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数 
                var path = Path.Combine("uploads", timeStamp + RandomHelper.GenerateRandomCode(5) + file.FileName);
                var rlt = await _iFilesService.UploadFiles(file, path);
                if (rlt.Code == (int)ResultCode.Success)
                {
                    filePath += $"{rlt.Data},";
                }
            }

            if (!filePath.IsNullOrEmpty() && filePath.EndsWith(","))
                filePath = filePath.TrimEnd(',');
            return new FilesResultViewModel() { Data = filePath };
        }

        /// <summary>
        /// 移动服务器文件 指定路径到Oss指定路径
        /// </summary>
        /// <param name="fromPath">服务器文件路径</param>
        /// <param name="toPath">Oss路径</param>
        /// <returns></returns>

        [HttpPost]
        public async Task<Result> MoveFiles(string fromPath, string toPath)
        {
            try
            {
                return await _iFilesService.MoveDirToOss(fromPath, toPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("移动", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "移动服务器文件异常" };
            }
        }

        /// <summary>
        /// 加盟商上传接口
        /// </summary>
        /// <param name="pathstr"></param>
        /// <param name="formFiles"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<FilesResultViewModel> OrgUploadFiles([FromForm] string pathstr, [FromForm] IFormCollection formFiles)
        {
            if (formFiles.Files.Count <= 0)
                return new FilesResultViewModel() { Code = (int)ResultCode.Fail, Message = "上传失败，文件为空" };
            var filePath = "";
            if (string.IsNullOrEmpty(pathstr))
            {
                pathstr = "temp";
            }
            var startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1), TimeZoneInfo.Local); // 当地时区
            var timeStamp = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数 
            foreach (var file in formFiles.Files)
            {
                //把文件读取到字节数组
                //Stream filestream = file.OpenReadStream();
                //var imgresult = BaiduApiHelper.GetForImgJson(filestream);
                //JObject jo = (JObject)imgresult;
                //var message = jo["conclusion"].ToString();
                //if (message.Equals("正常"))
                //{
                var path = Path.Combine(pathstr, timeStamp + RandomHelper.GenerateRandomCode(5) + file.FileName);
                var rlt = await _iFilesService.UploadFiles(file, path);
                if (rlt.Code == (int)ResultCode.Success)
                {
                    filePath += rlt.Data + ",";
                }
                //}
            }
            if (!filePath.IsNullOrEmpty() && filePath.EndsWith(","))
                filePath = filePath.TrimEnd(',');
            return new FilesResultViewModel() { Data = filePath };
        }

        [HttpGet]
        public async Task<FileContentResult> UploadFile([FromQuery] string path)
        {
            if (string.IsNullOrEmpty(path))
                return File(System.Text.Encoding.UTF8.GetBytes("{\"message\":\"这不是一个文件路径！\"}"), "application/json"); ;
            var provider = new FileExtensionContentTypeProvider();
            FileInfo fileInfo = new FileInfo(path);
            var ext = fileInfo.Extension;
            new FileExtensionContentTypeProvider().Mappings.TryGetValue(ext, out var contentType);
            return await Task.FromResult(File(System.IO.File.ReadAllBytes(path), contentType ?? "application/octet-stream",
                fileInfo.Name));
        }

        [HttpPost]
        public async Task<FilesResultViewModel> UploadFileChunk([FromForm] int chunk, [FromForm] int chunks, [FromForm] IFormCollection formFiles)
        {
            var uploadId = Guid.NewGuid().ToString();
            try
            {
                if (formFiles.Files.Count <= 0)
                    return new FilesResultViewModel() { Code = (int)ResultCode.Fail, Message = "上传失败，文件为空" };
                var filePath = "";
                var startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1), TimeZoneInfo.Local); // 当地时区
                var timeStamp = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数 
                foreach (var file in formFiles.Files)
                {
                    var path = timeStamp + RandomHelper.GenerateRandomCode(5) + file.FileName;
                    var rlt = await _iFilesService.UploadFileChunk(uploadId, chunk, chunks, file.OpenReadStream(), path);
                    if (rlt.Code == (int)ResultCode.Success)
                    {
                        filePath += rlt.Data + ",";
                    }
                }
                if (!filePath.IsNullOrEmpty() && filePath.EndsWith(","))
                    filePath = filePath.TrimEnd(',');
                return new FilesResultViewModel() { Data = filePath };
            }
            catch (Exception ex)
            {
                Logger.Error($"分片上传文件异常{uploadId}", ex);
                return new FilesResultViewModel() { Code = (int)ResultCode.Fail, Message = "分片上传文件异常" };
            }

        }

        [HttpPost]
        public async Task<Result> SaveToFile(string uploadId, int chunks, string path)
        {
            try
            {
                var rlt = await _iFilesService.SaveToFile(uploadId, path, chunks);
                return rlt;
            }
            catch (Exception ex)
            {
                Logger.Error($"文件合并异常{uploadId}", ex);
                return new FilesResultViewModel() { Code = (int)ResultCode.Fail, Message = "文件合并异常" };
            }
        }
        [HttpPost]
        public async Task<Result> RemoveChunk(string uploadId, int chunks)
        {
            try
            {
                var rlt = await _iFilesService.RemoveChunk(uploadId, chunks);
                return rlt;
            }
            catch (Exception ex)
            {
                Logger.Error($"文件分片清理异常{uploadId}", ex);
                return new FilesResultViewModel() { Code = (int)ResultCode.Fail, Message = "文件分片清理异常" };
            }
        }
    }
}