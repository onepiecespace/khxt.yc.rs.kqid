﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Log;
using KhXt.YcRs.Domain.Services.Log;
using KhXt.YcRs.Domain.Util;
using Jiguang.JPush;
using Jiguang.JPush.Model;
using KhXt.YcRs.HttpApi.ApiGroup;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers
{
    /// 极光推送控制器
    /// <summary>
    /// </summary>
    [Route("api/jpush")]
    [ApiController]
    [ApiGroup(ApiGroupNames.JPush)]
    // [AllowAnonymous]
    public class JPushController : BaseApiController
    {
        private JPushSetting _sysitting;
        private JPushClient client;
        public JPushController(JPushSetting sysitting)
        {
            _sysitting = sysitting;
            client = new JPushClient(_sysitting.APP_KEY, _sysitting.MASTER_SECRET);
        }
        /// <summary>
        /// 获取设备下的信息
        /// </summary>
        /// <param name="registrationId">设备唯一ID</param>
        [Route("deviceInfo")]
        [HttpPost]
        [AllowAnonymous]
        public Result<object> GetDeviceInfo(string registrationId)
        {
            Result<object> result = new Result<object>();
            try
            {
                var response = client.Device.GetDeviceInfo(registrationId);
                result.Data = JsonConvert.DeserializeObject<JObject>(response.Content);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result.Code = (int)ResultCode.Success;
                    result.Message = "获取设备绑定用户信息成功";
                }
                else
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "获取设备绑定用户信息失败";
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "接口名称：GetDeviceInfo,错误问题：" + ex.Message;
                Logger.Error("GetDeviceInfo", ex);
            }
            return result;
        }
        /// <summary>
        /// app 用户登录后 调用 初始化设备绑定用户关系以及用户默认追加的业务标签tag标记（对接APP）
        /// </summary>
        [Route("executeDevice")]
        [HttpPost]
        public Result<JObject> ExecuteDevice()
        {
            Result<JObject> result = new Result<JObject>();
            try
            {
                var devicePayload = new DevicePayload
                {
                    Alias = UserId,//从header TOKEN获取
                    Mobile = Phone,//header TOKEN解密获取
                    Tags = new Dictionary<string, object>
                    {
                        { "add", new List<string>() { "Dictation", "Course", "Task", "Read", "Lesson", "Recite" } },
                    }
                };
                //第一步检查改设备是否已经绑定用户
                var devresponse = client.Device.GetDeviceInfo(registrationId);
                JObject jo = (JObject)JsonConvert.DeserializeObject(devresponse.Content);
                if (devresponse.StatusCode != HttpStatusCode.OK)//该设备未绑定用户
                {
                    var response = client.Device.UpdateDeviceInfo(registrationId, devicePayload);
                    result.Data = JsonConvert.DeserializeObject<JObject>(response.Content);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //数据库追加设备唯一registrationId与UID绑定关系
                        result.Code = (int)ResultCode.Success;
                        result.Message = "设备绑定用户关联成功";
                    }
                    else
                    {
                        result.Code = (int)ResultCode.Fail;
                        result.Message = "设备绑定用户失败";
                    }
                }
                else
                {
                    var alias = jo["alias"].ToString();
                    var mobile = jo["mobile"].ToString();
                    if (alias == UserId)
                    {
                        result.Code = (int)ResultCode.Success;
                        result.Message = "该设备已绑定用户成功";
                        result.Data = jo;
                    }
                    else//该设备绑定覆盖原先的用户
                    {
                        var response = client.Device.UpdateDeviceInfo(registrationId, devicePayload);
                        result.Data = JsonConvert.DeserializeObject<JObject>(response.Content);
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            //数据库修改设备唯一registrationId与UID绑定关系
                            result.Code = (int)ResultCode.Success;
                            result.Message = "设备绑定用户变动成功";
                        }
                        else
                        {
                            result.Code = (int)ResultCode.Fail;
                            result.Message = "设备绑定用户变动失败";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "接口名称：ExecuteDevice,错误问题：" + ex.Message;
                Logger.Error("ExecuteDevice", ex);
            }
            return result;

        }

        #region 今日消息提醒
        /// <summary>
        /// 今日消息提醒(每一天通知一次)
        /// </summary>
        /// <returns></returns>
        [Route("todayInfo")]
        [HttpPost]
        [AllowAnonymous]
        public Result<object> TodayInfo(string registrationId)
        {
            Result<object> result = new Result<object>();
            try
            {
                #region  获取未开播的课程
                //List<aces_studentEntity> studentList = aces_studentclassBLL.GetTodayClass();
                //if (studentList != null && studentList.Count > 0)
                //{

                //    foreach (aces_studentEntity student in studentList)
                //    {
                //        cAlias.Add(student.studentid);

                //        aces_infomationEntity entity = new aces_infomationEntity();
                //        string myTitle = "新消息！您今日有Tutor辅导课，请准时上课，查看详情  >>";
                //        entity.cometype = 3;
                //        entity.content = myTitle;
                //        entity.comeid = student.studentid;
                //        entity.enable = 1;
                //        entity.from = 1;
                //        entity.fromid = "0";
                //        entity.sendtime = DateTime.Now;
                //        entity.targettype = "scheduleClass";
                //        entity.targetvalue = "";
                //        entity.title = "今日课程提醒";
                //        aces_infomationBLL.SaveEntity(0, entity);

                //    }
                // }

                //List<string> cAlias = new List<string>();
                ////cAlias.Add(UserId);
                //cAlias.Add("107");
                //// cAlias.Add("174");
                //if (cAlias != null && cAlias.Count > 0)
                //{
                //}
                #endregion
                PushPayload pushPayload = new PushPayload()
                {
                    Platform = new List<string> { "android", "ios" },
                    Audience = new Audience
                    {
                        RegistrationId = new List<string> { registrationId },//"13065ffa4ebd0d79288", "120c83f760907ceb143",
                    },
                    Notification = new Notification
                    {
                        Alert = "今日课程提醒",
                        Android = new Android
                        {
                            Alert = "今日课程提醒",
                            Title = "新消息！您今日有Tutor辅导课，请准时上课，查看详情  >>",
                            Extras = new Dictionary<string, object>
                            {
                                ["Target"] = "scheduleClass",
                                ["TargetValue"] = ""
                            }
                        },
                        IOS = new IOS
                        {
                            Alert = "新消息！您今日有Tutor辅导课，请准时上课，查看详情  >>",
                            Badge = "+1",
                            Extras = new Dictionary<string, object>
                            {
                                ["Target"] = "scheduleClass",
                                ["TargetValue"] = ""
                            }

                        }
                    },
                    Message = new Message
                    {
                        Title = "今日课程提醒",
                        Content = "新消息！您今日有Tutor辅导课，请准时上课，查看详情  >>",
                        Extras = new Dictionary<string, object>
                        {
                            ["Target"] = "scheduleClass",
                            ["TargetValue"] = ""
                        }
                    },
                    Options = new Options
                    {
                        IsApnsProduction = _sysitting.isDebug // 设置 iOS 推送生产环境。不设置默认为开发环境。
                    }
                };
                var response = client.SendPush(pushPayload);
                result.Data = JsonConvert.DeserializeObject<JObject>(response.Content);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result.Code = (int)ResultCode.Success;
                    result.Message = "推送消息成功";
                }
                else
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "推送消息失败";
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "请求失败接口名称：TodayInfo,错误问题：" + ex.Message + ",错误堆栈信息：" + ex.StackTrace.ToString();
                Logger.Error("TodayInfo", ex);
            }
            return result;
        }
        #endregion

        #region 定时任务消息提醒
        /// <summary>
        /// 定时任务消息提醒 今日消息提醒(每一天通知一次)
        /// </summary>
        /// <returns></returns>
        [Route("executeSchedule")]
        [HttpPost]
        [AllowAnonymous]
        public Result<object> ExecuteSchedule(string registrationId)
        {
            Result<object> result = new Result<object>();
            try
            {

                PushPayload pushPayload = new PushPayload()
                {
                    Platform = new List<string> { "android", "ios" },
                    Audience = new Audience
                    {
                        RegistrationId = new List<string> { registrationId },//"13065ffa4ebd0d79288", "120c83f760907ceb143",
                    },
                    Notification = new Notification
                    {
                        Alert = "今日课程提醒",
                        Android = new Android
                        {
                            Alert = "今日课程提醒",
                            Title = "新消息！您今日有Tutor辅导课，请准时上课，查看详情  >>",
                            Extras = new Dictionary<string, object>
                            {
                                ["Target"] = "scheduleClass",
                                ["TargetValue"] = ""
                            }
                        },
                        IOS = new IOS
                        {
                            Alert = "新消息！您今日有Tutor辅导课，请准时上课，查看详情  >>",
                            Badge = "+1",
                            Extras = new Dictionary<string, object>
                            {
                                ["Target"] = "scheduleClass",
                                ["TargetValue"] = ""
                            }

                        }
                    },
                    Message = new Message
                    {
                        Title = "今日课程提醒",
                        Content = "新消息！您今日有Tutor辅导课，请准时上课，查看详情  >>",
                        Extras = new Dictionary<string, object>
                        {
                            ["Target"] = "scheduleClass",
                            ["TargetValue"] = ""
                        }
                    },
                    Options = new Options
                    {
                        IsApnsProduction = _sysitting.isDebug // 设置 iOS 推送生产环境。不设置默认为开发环境。
                    }
                };
                var trigger = new Trigger
                {
                    StartDate = "2020-09-16 16:00:00",
                    EndDate = "2020-09-23 16:00:00",
                    TriggerTime = "16:00:00",
                    TimeUnit = "day",
                    Frequency = 1,
                    TimeList = new List<string>
                    {
                        //  "mon", "tue", "wed", "thu", "fri", "sat", "sun"
                    }
                };
                var response = client.Schedule.CreatePeriodicalScheduleTask("task1", pushPayload, trigger);
                result.Data = JsonConvert.DeserializeObject<JObject>(response.Content);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result.Code = (int)ResultCode.Success;
                    result.Message = "定时任务推送消息成功";
                }
                else
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "定时任务推送消息失败";
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "请求失败接口名称：ExecuteSchedule,错误问题：" + ex.Message + ",错误堆栈信息：" + ex.StackTrace.ToString();
                Logger.Error("ExecuteSchedule", ex);
            }
            return result;
        }
        #endregion
        /// <summary>
        /// 送达统计详情（新）
        /// </summary>
        /// <param name="msgIdList"></param>
        /// <returns></returns>
        [Route("receivedDetail")]
        [HttpPost]
        [AllowAnonymous]
        public Result<object> receivedDetail(List<string> msgIdList)
        {
            Result<object> result = new Result<object>();
            try
            {
                var response = client.Report.GetReceivedDetailReport(msgIdList);
                result.Data = JsonConvert.DeserializeObject<JObject>(response.Content);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result.Code = (int)ResultCode.Success;
                    result.Message = "送达统计详情";
                }
                else
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "送达统计详情失败";
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "请求失败接口名称：receivedDetail,错误问题：" + ex.Message + ",错误堆栈信息：" + ex.StackTrace.ToString();
                Logger.Error("receivedDetail", ex);
            }
            return result;
        }
        #region VIP开通后的服务
        #region  针对Alias方式批量单推（VIP专属接口）
        /// <summary>
        /// 针对Alias方式批量单推（VIP专属接口）
        /// </summary>
        /// <param name="registrationId"></param>
        /// <returns></returns>
        [Route("executeBatchPush")]
        [HttpPost]
        [AllowAnonymous]
        public Result ExecuteBatchPush(string registrationId)
        {
            Result result = new Result();
            try
            {
                List<SinglePayload> singlePayloads = new List<SinglePayload>();
                SinglePayload singlePayload = new SinglePayload()
                {
                    Platform = new List<string> { "android", "ios" },
                    Target = registrationId,//regid
                    Notification = new Notification
                    {
                        Alert = "hello jpush",
                        Android = new Android
                        {
                            Alert = "android alert",
                            Title = "title"
                        },
                        IOS = new IOS
                        {
                            Alert = "ios alert",
                            Badge = "+1"
                        }
                    },
                    Message = new Message
                    {
                        Title = "今日课程提醒",
                        Content = "新消息！您今日有Tutor辅导课，请准时上课",
                        Extras = new Dictionary<string, string>
                        {
                            ["Target"] = "scheduleClass",
                            ["TargetValue"] = ""
                        }
                    },
                    Options = new Options
                    {
                        IsApnsProduction = false // 设置 iOS 推送生产环境。不设置默认为开发环境。
                    }
                };
                singlePayloads.Add(singlePayload);
                var response = client.BatchPushByRegid(singlePayloads);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result.Code = (int)ResultCode.Success;
                    result.Message = "针对Alias方式批量单推成功";
                }
                else
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "针对Alias方式批量单推成功失败";
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "接口名称：ExecuteBatchPush,错误问题：" + ex.Message;
                Logger.Error("DayReport", ex);
            }
            return result;
        }
        #endregion
        ///<summary>
        /// 消息统计详情（VIP 专属接口，新）
        /// </summary>
        /// <param name="msgIdList"></param>
        /// <returns></returns>
        [Route("messagesDetail")]
        [HttpPost]
        [AllowAnonymous]
        public Result<object> messagesDetail(List<string> msgIdList)
        {
            Result<object> result = new Result<object>();
            try
            {
                var response = client.Report.GetMessagesDetailReport(msgIdList);
                result.Data = JsonConvert.DeserializeObject<JObject>(response.Content);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result.Code = (int)ResultCode.Success;
                    result.Message = "消息统计详情";
                }
                else
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "消息详情失败";
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "请求失败接口名称：receivedDetail,错误问题：" + ex.Message + ",错误堆栈信息：" + ex.StackTrace.ToString();
                Logger.Error("receivedDetail", ex);
            }
            return result;
        }
        #endregion
    }
}