﻿using HuangLiCollege.Common.Models;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.JWT;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Util;
using HuangLiCollege.Models;
using Hx.Extensions;
using Hx.ObjectMapping;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers
{
    public partial class UserController
    {
        private readonly string _aidKey = "aid";
        private readonly string _aid = "college";

        private readonly string _tidKey = "tid";
        private readonly string _tid = "0";
        private readonly string _versionKey = "ver";
        private readonly int _version = 0;
        private readonly string _regidKey = "registrationId";
        private readonly string _regid = "";
        public UserController()
        {


        }
        #region 短信  手机号登录
        /// <summary>
        /// 【短信】发送短信验证码
        /// 【返回值 code:手机验证码  后台缓存1分钟】
        /// </summary>
        /// <param name="phone">手机号</param>
        /// <returns>手机验证码</returns>
        [HttpGet("{phone}")]
        [AllowAnonymous]
        public async Task<Result> SmsCode(string phone)
        {
            try
            {
                if (phone != "18210413749")
                {
                    var getIp = HuangLiCollege.IpHelper.GetRealIp(this.HttpContext);
                    //Logger.Info($"SmsCode:{phone},来自:{getIp}");
                    //if (!SafeCheck(getIp))
                    //{
                    //    Logger.Info($"SmsCode:{phone},拉黑一天:{getIp}");
                    //    return new Result() { Code = (int)ResultCode.SpeedLimit, Message = "操作太频繁" };
                    //}
                    //Logger.Info($"SafeCheck 验证结束");
                }
                var sendRlt = await _smsService.SendValidCode(phone, GetAid());
                return sendRlt;
            }
            catch (Exception ex)
            {
                Logger.Error($"发送短信失败,{phone}", ex);
                return new Result { Message = "发送短信失败" };
            }

        }
        bool SafeCheck(string ip)
        {
            Logger.Info("验证，Redis 开始");
            var list = Redis.StringGetWithType<List<string>>(RedisKeyHelper.White_List);
            Logger.Info("验证，Redis 结束");
            if (list.Contains(t => t.Equals(ip)))
                return true;
            Logger.Info("验证，写入 Redis 开始");
            Redis.KeyExpire(RedisKeyHelper.SMS_SPEED_LIMIT_IP, TimeSpan.FromDays(1));
            var cv = Redis.HashIncrement(RedisKeyHelper.SMS_SPEED_LIMIT_IP, ip);
            Logger.Info("验证，写入 Redis 结束");
            return cv < 10;
        }
        /// <summary>
        /// 【注册或登陆】
        /// 【返回值 token:返回用户唯一TOKEN值】
        /// </summary>
        /// <param name="phone">手机号</param>
        /// <param name="code">接收到的验证码</param> 
        /// <returns>返回用户唯一TOKEN值</returns>
        [HttpGet("{phone},{code}")]
        [AllowAnonymous]
        public async Task<Result> LoginOrRegister(string phone, string code)
        {
            Result result = new Result();
            //验证手机与验证码是否匹配
            //读取redis 验证码
            string redisNum = Redis.StringGet($"SMS:{phone}");
            if (string.IsNullOrEmpty(redisNum))
            {
                result.Code = (int)ResultCode.ValidCodeExpired;
                result.Message = EnumHelper.GetDescription(ResultCode.ValidCodeExpired);
                result.Data = redisNum;
                return await Task.FromResult(result);
            }

            if (code.Trim() != redisNum.Trim())
            {
                result.Code = (int)ResultCode.ValidCodeError;
                result.Message = EnumHelper.GetDescription(ResultCode.ValidCodeError);
                result.Data = redisNum;
                return await Task.FromResult(result);
            }

            if (result.Code != (int)ResultCode.Success) return await Task.FromResult(result);

            result = await _userService.LoginOrRegister(phone, GetAid(), GetTid(), GetRegid());

            return await Task.FromResult(result);
        }
        /// <summary>
        /// 【注册或登陆】
        /// 【返回值 token:返回用户唯一TOKEN值】
        /// </summary>
        /// <param name="phone">手机号</param>
        /// <param name="code">接收到的验证码</param> 
        /// <returns>返回用户唯一TOKEN值</returns>
        [HttpGet("{phone},{code}")]
        [AllowAnonymous]
        public async Task<Result> SSOLoginOrRegister(string phone, string code)
        {
            Result result = new Result();
            //验证手机与验证码是否匹配
            //读取redis 验证码
            //   string redisNum = Redis.StringGet($"SMS:{phone}");
            //if (string.IsNullOrEmpty(redisNum))
            //{
            //    result.Code = (int)ResultCode.ValidCodeExpired;
            //    result.Message = EnumHelper.GetDescription(ResultCode.ValidCodeExpired);
            //    result.Data = redisNum;
            //    return await Task.FromResult(result);
            //}

            //if (code.Trim() != redisNum.Trim())
            //{
            //    result.Code = (int)ResultCode.ValidCodeError;
            //    result.Message = EnumHelper.GetDescription(ResultCode.ValidCodeError);
            //    result.Data = redisNum;
            //    return await Task.FromResult(result);
            //}

            // if (result.Code != (int)ResultCode.Success) return await Task.FromResult(result);

            result = await _userService.SSOLoginOrRegister(phone, GetAid());

            return await Task.FromResult(result);
        }

        /// <summary>
        /// 对接sso测试使用权限中心获取统一的token
        /// </summary>
        /// <param name="account"></param>
        /// <param name="password">默认123456</param>
        /// <param name="appCode"></param>
        /// <returns></returns>
        [HttpGet("{account},{password},{appCode}")]
        [AllowAnonymous]
        public async Task<Result> AuthValidateOrRegister(string account, string password, string appCode)
        {
            Result result = new Result();
            if (string.IsNullOrEmpty(account))
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = "账号不能为空";
                return await Task.FromResult(result);
            }

            var resultCheck = await _ssoService.ValidateOrRegister(account, password, appCode);

            if (resultCheck.Success == true)
            {
                result.Code = (int)ResultCode.Success;
                result.Message = "获取token成功";
                result.Data = resultCheck.AccessToken;
            }
            else
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = resultCheck.ErrMsg;
            }
            return await Task.FromResult(result);
        }
        /// <summary>
        ///对接sso测试使用 统一授权获取token与详细信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{token}")]
        [AllowAnonymous]
        public async Task<Result<UserSSOOutPutDto>> GetAccessToken(string token)
        {
            Result<UserSSOOutPutDto> result = new Result<UserSSOOutPutDto>();
            if (string.IsNullOrEmpty(token))
            {
                result.Code = (int)ResultCode.NonAuthoritativeInformation;
                result.Message = EnumHelper.GetDescription(ResultCode.NonAuthoritativeInformation);
                return await Task.FromResult(result);
            }

            var resultCheck = await _ssoService.CheckAppToken(token);

            if (resultCheck.Success == true)
            {
                result.Code = (int)ResultCode.Success;
                result.Message = "获取统一授权token成功";
                result.Data = resultCheck.Result;
            }
            else
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = resultCheck.ErrMsg;
                result.Data = resultCheck.Result;
            }
            return await Task.FromResult(result);
        }
        /// <summary>
        /// 闪验后端服务置换手机号接口
        /// </summary>
        /// <param name="token"></param>
        /// <param name="os">  ios = 1, andriod = 2,web = 4,</param>
        /// <returns></returns>
        [HttpGet("{token}")]
        [AllowAnonymous]
        public async Task<Result> mobileQuery(string token, PlatFormType os = PlatFormType.andriod)
        {

            //闪验后端服务置换手机号接口V2请求方法
            string phone = FlashLoginHelper.mobileQuery(token, os, _sysitting);
            Result result = new Result();
            if (phone.IsNullOrEmpty())
            {
                return new Result(ResultCode.Fail, "手机号不能为空");
            }
            else
            {
                result.Code = (int)ResultCode.Success;
                result.Message = "闪验后端服务置换手机号成功";
                result.Data = phone;
                return await Task.FromResult(result);
            }
        }
        /// <summary>
        /// 闪验后端服务本机号校验接口
        /// </summary>
        /// <param name="token"></param>
        /// <param name="phone">待校验的手机号码</param>
        /// <param name="os">  ios = 1, andriod = 2,web = 4,</param>
        /// <returns></returns>
        [HttpGet("{token},{phone}")]
        [AllowAnonymous]
        public async Task<Result> mobilevalidate(string token, string phone, PlatFormType os = PlatFormType.andriod)
        {
            Result result = new Result();
            //闪验后端服务本机号校验接口V2请求方法
            string isVerify = FlashValidateHelper.mobilevalidate(token, phone, os, _sysitting);
            result.Code = (int)ResultCode.Success;
            result.Message = "闪验后端服务本机号校验成功";
            result.Data = isVerify;
            return await Task.FromResult(result);

        }
        /// <summary>
        /// 【一键注册登录登陆】
        /// 【返回值 token:返回用户唯一TOKEN值】
        /// </summary>
        /// <param name="token">运营商app获取的加密token</param>
        /// <param name="os">  ios = 1, andriod = 2,web = 4,</param>
        /// <returns>返回用户唯一TOKEN值</returns>
        [HttpGet("{token}")]
        [AllowAnonymous]
        public async Task<Result> OnekeyLoginOrRegister(string token, PlatFormType os = PlatFormType.andriod)
        {

            //闪验后端服务置换手机号接口V2请求方法
            string phone = FlashLoginHelper.mobileQuery(token, os, _sysitting);

            if (phone.IsNullOrEmpty())
            {
                return new Result(ResultCode.Fail, "手机号不能为空");
            }
            else
            {
                var rlt = await _userService.LoginOrRegister(phone, GetAid(), GetTid(), GetRegid());
                return await Task.FromResult(rlt);
            }
        }
        /// <summary>
        ///【注册或登陆】
        /// 【返回值 token:返回用户唯一TOKEN值】
        /// </summary>
        /// <param name="phone">手机号</param>
        /// <param name="wxToken">用于小程序登录的唯一标识，默认为空</param>        /// 
        /// <returns>返回用户唯一TOKEN值</returns>
        [HttpPost()]
        [AllowAnonymous]
        public async Task<Result> LoginOrRegisterWx([FromForm] string phone, [FromForm] string wxToken = "")
        {

            Result result = new Result();
            if (string.IsNullOrWhiteSpace(wxToken))
            {
                wxToken = Guid.NewGuid().ToString("N");
            }

            //存在该手机号，查询数据
            var token = string.Empty;
            var user = await _userService.GetUserByPhone(phone.Trim());
            if (user == null)
            {
                //注册
                var resolute = await _userService.Create(new UserBaseInfo()
                {
                    Phone = phone,
                    LoginType = 1,
                    Creatime = DateTime.Now,
                    AccountType = (int)AccountType.Normal,
                    Username = "",
                    Sex = 2,
                    Provincial = "",
                    Grade = 1,
                    City = "",
                    Address = "",
                    Areas = "",
                    Headurl = _defaultHeaderUrl,
                    Wx = "",
                    Qq = "",
                    MiniWX = wxToken,
                    Aid = GetAid(),
                    IsFirstInputAddress = 1,

                });

                if (resolute.Code == (int)ResultCode.Success)
                {
                    #region 杜甫标识的用户必须市会员才可以登陆
                    var aid = GetAid();
                    if (aid == "college")
                    {
                        result.Code = (int)ResultCode.warning;
                        result.Message = "此手机号暂无登录权限";
                        Logger.Error($"手机号{phone}登陆失败!,您的账号待审核,暂不可以登陆！");
                        return await Task.FromResult(result);
                    }
                    #endregion
                    token = JwtHelper.IssueJWT(new TokenModel() { Phone = phone, UserId = resolute.Data });
                    Redis.StringSet("token:" + phone, token, TimeSpan.FromSeconds(_tokenTime));
                    result.Data = token;
                    return await Task.FromResult(result);
                }
                else
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = resolute.Message;
                    result.Data = "";
                    Logger.Error($"手机号{phone}注册失败!,可能原因添加到数据库失败！");
                    return await Task.FromResult(result);
                }
            }
            else
            {
                #region 杜甫标识的用户必须市会员才可以登陆
                var aid = GetAid();
                if (aid == "college")
                {
                    if (user.AccountType <= 1 && user.IsMember == 0)//只限制普通用户
                    {
                        result.Code = (int)ResultCode.warning;
                        result.Message = "此手机号暂无登录权限";
                        Logger.Error($"手机号{phone}登陆失败!,您的账号待审核，暂不可以登陆！");
                        return await Task.FromResult(result);
                    }
                }
                #endregion
                //存在该用户，取redis里的token
                token = Redis.StringGet("token:" + user.Phone);

            }

            //如果第一次登陆或者Token 过期 则重新生成Token
            if (string.IsNullOrEmpty(token))
            {
                token = JwtHelper.IssueJWT(new TokenModel() { Phone = phone, UserId = user.Userid });
                Redis.StringSet("token:" + phone, token, TimeSpan.FromSeconds(_tokenTime));
                result.Message = "注册成功";
            }
            else
            {
                result.Message = "登录成功";
            }

            result.Data = token;
            return await Task.FromResult(result);
        }

        /// <summary>
        ///  获取header 加盟商租户tid号
        /// </summary>
        /// <returns></returns>
        private string GetTid()
        {

            var tid = _tid;
            try
            {
                if (!Request.Headers.ContainsKey(_tidKey))
                {
                    Request.Headers.Add("tid", "0");
                    tid = "0";
                }
                else
                {
                    tid = Request.Headers[_tidKey].ToString();
                    if (string.IsNullOrEmpty(tid))
                    {
                        tid = "0";
                    }
                }
            }
            catch
            {
                tid = _tid;
            }
            return tid;
        }

        private string GetAid()
        {
            var aid = _aid;
            try
            {
                if (!Request.Headers.ContainsKey(_aidKey))
                {
                    Request.Headers.Add(_aidKey, "college");
                    aid = "college";
                }
                else
                {
                    aid = Request.Headers[_aidKey].ToString();
                }

            }
            catch
            {
                aid = _aid;
            }
            return aid;
        }

        private int GetVersion()
        {
            var version = _version;
            try
            {
                if (!Request.Headers.ContainsKey(_versionKey))
                {
                    Request.Headers.Add(_versionKey, "0");
                    version = 0;
                }
                else
                {
                    version = int.Parse(Request.Headers[_versionKey].ToString());
                }


            }
            finally
            {
                version = _version;
            }
            return version;
        }
        #endregion
        private string GetRegid()
        {
            var regid = _regid;
            try
            {
                if (!Request.Headers.ContainsKey(_regidKey))
                {
                    Request.Headers.Add(_regidKey, "");
                    regid = "";
                }
                else
                {
                    regid = Request.Headers[_regidKey].ToString();
                }

            }
            catch
            {
                regid = _regid;
            }
            return regid;
        }


        #region 第三方登录

        /// <summary>
        /// success：data返回token
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost()]
        [AllowAnonymous]
        public async Task<Result> TryLoginWith2fa(LoginWith2faViewModel model)
        {
            var rm = model.MapTo<LoginWith2faModel>();
            FillRequestModel(rm);
            var rlt = await _userService.LoginWith2fa(rm);
            return rlt;
        }

        [HttpPost()]
        [AllowAnonymous]
        public async Task<Result> LoginWith2faBind(LoginWith2faBindModel model)
        {
            var rlt = await _userService.LoginWith2faBind(model);
            return rlt;
        }

        /// <summary>
        /// 【第三方登录】【第一步 200 成功返回 KeyData:1表示绑定过手机号的第三方，0表示没有绑定过手机号的第三方 】
        /// 【wx或者QQ threeToken存在】就直接判断用户信息是否存在，存在返回KeyData=1,token(128获取 api/user/get) 
        /// 【wx或者QQ threeToken 不存在】返回 KeyData=0,token(32位) 进入下一步绑定 
        /// </summary>
        /// <param name="type">loginType</param>
        /// <param name="threeToken">第三方登录用户的唯一token【当前唯一 只能且绑定一个用户 绑定过的不可以绑定】</param>
        /// <param name="nickName">昵称</param>
        /// <param name="sex">性别  1男 0女 2未知</param>
        /// <param name="avatar">t头像</param> 
        /// <returns></returns>
        [HttpPost()]
        [AllowAnonymous]
        [Obsolete("使用TryLoginWith2fa替换")]
        public async Task<UserThreeResult> SetThreeParty([FromForm] string type, [FromForm] int sex,
            [FromForm] string threeToken = "",
            [FromForm] string nickName = "", [FromForm] string avatar = "")
        {

            try
            {
                var loginType = (LoginType)Enum.Parse(typeof(LoginType), type, true);
                var model = new ThreeLoginModel
                {
                    OpenId = threeToken,
                    LogingType = loginType,
                    NickName = nickName,
                    Avatar = avatar,
                    Aid = GetAid(),
                    Sex = (Sex)Enum.Parse(typeof(Sex), sex.ToString())
                };

                var srlt = await _userService.ThreeLogin(model);

                if (srlt.Code == (int)ResultCode.Success)
                {
                    return new UserThreeResult { Data = new KeyValue { KeyData = "1", ValueData = srlt.Data } };

                }

                return new UserThreeResult { Data = new KeyValue { KeyData = "0", ValueData = srlt.Data } };
            }
            catch (Exception ex)
            {
                Logger.Error("SetThreeParty", ex);
                return new UserThreeResult { Code = (int)ResultCode.Fail, Message = ex.Message, Data = new KeyValue { KeyData = "0", ValueData = "" } };
            }
            //   return await Task.FromResult();
        }

        /// <summary>
        ///  第三方登录【第二步。根据第一步绑定返回信息token,绑定手机号码 需要判断验证码的】
        /// </summary>
        /// /// <param name="openId"></param>
        /// <param name="phone">手机号码</param>
        /// <param name="code">手机号码验证码</param>
        /// <returns></returns>
        [HttpPost()]
        [AllowAnonymous]
        [Obsolete("升级使用LoginWith2faBind，旧版本淘汰后可删除")]
        public async Task<Result> LoginThreeParty(
            [FromForm] string phone = "",
            [FromForm] string openId = "",
            [FromForm] string code = "")
        {
            try
            {
                var resultB = await _userService.ThreeLoginBind(GetAid(), openId, phone, code);
                Logger.Debug($"openid:{ resultB.Data}");
                return await Task.FromResult(resultB);
            }
            catch (Exception ex)
            {
                Logger.Error("LoginThreeParty", ex);
                return await Task.FromResult(new Result { Code = (int)ResultCode.Fail, Message = ex.Message });
            }
        }

        #endregion

        #region 绑定第三方账号 手机号码、微信、QQ
        /// <summary>
        /// 获取第三方绑定列表信息
        /// </summary>
        /// <returns>{Name：名称,IsBind：是否绑定,Token：对应QQ微信的唯一值}</returns>
        [HttpGet()]
        public async Task<UserThreePartyResult> GetThreeParty()
        {
            UserThreePartyResult result = new UserThreePartyResult();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                }
                else
                {

                    result.Code = (int)ResultCode.Success;
                    var info = await _userService.GetUser(UserIdValue);

                    result.Data = new UserThreePartyList()
                    {
                        GetUserThreeParty = new List<UserThreeParty>()
                        {
                            new UserThreeParty(){
                                Name="WX",
                                IsBind = !string.IsNullOrWhiteSpace(info.Wx) ,
                                Token=info.Wx?.Trim()??""
                            },
                            new UserThreeParty(){
                                Name="QQ",
                                IsBind =!string.IsNullOrWhiteSpace(info.Qq) ,
                                Token=info.Qq?.Trim()??""
                            },
                            new UserThreeParty(){
                                Name="Phone",
                                IsBind =!string.IsNullOrWhiteSpace(info.Phone) ,
                                Token=info.Phone?.Trim()??""
                            }
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                Logger.Error(ex);
            }
            return result;
        }
        /// <summary>
        /// qq绑定
        /// 【返回值 200 成功】
        /// </summary>
        /// <param name="qq">QQ code 授权码</param>
        /// <returns>返回结果值</returns>
        [HttpPost()]
        public async Task<Result> SetQQBind([FromForm] string qq)
        {
            Result result = new Result();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                }
                else
                {
                    var info = await _userService.GetUser(UserIdValue);
                    info.Qq = qq;
                    await _userService.Update(info);
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                Logger.Error(ex);
            }

            return result;
        }
        /// <summary>
        /// 微信绑定
        /// 【返回值 200 成功】
        /// </summary>
        /// <param name="wx">微信号</param>
        /// <returns>返回结果值</returns>
        [HttpPost()]
        public async Task<Result> SetWXBind([FromForm] string wx)
        {
            Result result = new Result();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                }
                else
                {
                    var info = await _userService.GetUser(UserIdValue);
                    info.Wx = wx;
                    await _userService.Update(info);
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// 手机绑定
        /// 【返回值 200 成功】
        /// </summary>
        /// <param name="mobile">新的手机号</param>
        /// <param name="code">新的手机号验证码</param>
        /// <returns>返回结果值</returns>
        [HttpPost()]
        public async Task<Result> SetPhoneBind([FromForm] string mobile, [FromForm] string code)
        {
            //验证手机与验证码是否匹配 //读取redis 验证码
            Result result = new Result();
            try
            {
                //测试时短信验证先注释掉
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    return new Result { Code = (int)ResultCode.Fail, Message = "token无效或已经过期" };
                }
                else
                {
                    string redisNum = Redis.StringGet($"SMS:{mobile}");
                    Logger.Debug($"mobile:{mobile},{code},{redisNum}");
                    if (string.IsNullOrEmpty(redisNum))
                    {
                        return new Result(ResultCode.ValidCodeExpired);
                    }
                    if (code.Trim() != redisNum.Trim())
                    {
                        return new Result { Code = (int)ResultCode.ValidCodeError, Message = "" };
                    };
                }
                #region  测试值是多少
                //////   获取原来的 token 设置过期   获取原来的
                //string token = Redis.StringGet("token:" + Phone);
                //Logger.Info($"用户解绑信息：phone:{UserId},token{token}");
                //Redis.StringSet("token:" + mobile, token);
                //Redis.KeyDelete($"token:{UserId}");
                // string redistoken = Redis.StringGet("token:" + mobile);
                #endregion
                string token = Redis.StringGet("token:" + Phone);
                Logger.Info($"用户解绑信息：phone:{UserId},token{token}");

                ////测试使用
                //var phone = "18513694712";
                //执行更新（新手机绑定）
                var user = await _userService.GetUserByPhone(mobile.Trim());
                if (user != null)
                {
                    return new Result { Code = (int)ResultCode.AccountErr, Message = "该号已绑定另一账号" };
                }
                else
                {
                    result = _userService.PhoneChangeAsync(mobile, code, Phone).Result;
                }
                ////获取新的token
                string redistoken = Redis.StringGet("token:" + mobile);
                Logger.Info($"用户解绑新的信息：phone:{mobile},token{redistoken}");
                return new Result { Data = result.Data };

            }
            catch (Exception ex)
            {
                Logger.Error("SetPhoneBind", ex);
                return new Result { Code = (int)ResultCode.Fail, Message = ex.Message };
            }
        }


        /// <summary>
        /// 获取解绑信息(手机号码、微信、QQ)是否存在          data 1存在0 不存在
        /// </summary>
        /// <param name="code">手机号码</param>
        /// <param name="type">类型 1 手机号码 2 qq 3 微信</param>
        /// <returns>1存在0 不存在</returns>
        [HttpGet()]
        public async Task<Result> GetPhoneExist(string code, int type = 1)
        {
            Result result = new Result();
            try
            {
                var alllist = await _userService.GetAll();
                switch (type)
                {
                    case 1:
                        var info = await _userService.GetUserByPhone(code);
                        result.Data = $"{((info == null) ? "0" : "1")}";
                        result.Message = $"手机号码{((info == null) ? "不存在" : "存在")}";
                        break;
                    case 2:
                        var info2 = alllist.FirstOrDefault(n => n.Qq == code);
                        result.Data = $"{((info2 == null) ? "0" : "1")}";
                        result.Message = $"QQ号码{((info2 == null) ? "不存在" : "存在")}";
                        break;
                    case 3:
                        var info3 = alllist.FirstOrDefault(n => n.Wx == code);
                        result.Data = $"{((info3 == null) ? "0" : "1")}";
                        result.Message = $"微信号码{((info3 == null) ? "不存在" : "存在")}";
                        break;
                    default:
                        result.Code = (int)ResultCode.Fail;
                        result.Message = "类型不存在";
                        break;
                }


            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = ex.Message;
                Logger.Error(ex);
            }

            return result;
        }

        [HttpGet("/api/user/token/{method}")]
        [AllowAnonymous]
        public async Task<string> GetTokenByOpenId(string openId, [FromRoute] string method)
        {
            if (string.IsNullOrEmpty(openId)) return "";
            if (LoginTypeDict.TryGetValue(method.ToLower(), out var loginType))
            {
                return await _userService.GetTokenByOpenId(openId, loginType);
            }
            return "";
        }

        private Dictionary<string, LoginType> LoginTypeDict = new Dictionary<string, LoginType> { { "wx", LoginType.Wx }, { "qq", LoginType.Qq } };

        #endregion

    }
}
