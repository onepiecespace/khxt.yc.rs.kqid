﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Log;
using KhXt.YcRs.Domain.Services.Log;
using KhXt.YcRs.Domain.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Controllers;

namespace KhXt.YcRs.HttpApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Test)]
    public class HlLogController : BaseApiController
    {
        ILogService _logService;
        public HlLogController(ILogService logService)
        {
            _logService = logService;
        }
        /// <summary>
        /// 黄鹂获取用户安装的app版本型号信息
        /// </summary>
        /// <param name="uid">uid</param>
        /// <param name="devType">Other = 0,  IPhone = 1, IPad = 2,APhone = 3,APad = 4,Web = 5, Wap = 6</param>
        /// <param name="Os"></param>
        /// <param name="devName">设备型号名称</param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<Result<string>> Login([FromForm]long uid, [FromForm]DeviceType devType, [FromForm]string Os, [FromForm]string devName)
        {

            Result result = new Result();
            try
            {
                var model = new LoginLogModel
                {
                    UserId = uid,
                    Phone = Phone,
                    DeviceType = devType,
                    DeviceTypeName = EnumHelper.GetDescription<DeviceType>(devType.ToString()),
                    DeviceName = devName,
                    Os = Os,
                    AppCode = appcode,
                    Ver = ver,
                    Aid = aid,
                    Tid = tid,
                    RegistrationId = registrationId
                };
                await _logService.UserLogin(model);

                result.Code = (int)ResultCode.Success;
                result.Data = "1";
                result.Message = "黄鹂记录设备日志成功";
                return await Task.FromResult(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Log.Login", ex);
                result.Code = (int)ResultCode.Fail;
                result.Message = "黄鹂记录设备日志失败";
                return await Task.FromResult(result);
            }


        }
    }
}