using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using HuangLiCollege.Models;
using Hx.Components;
using Hx.Extensions;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace KhXt.YcRs.HttpApi.Controllers
{
    /// <summary>
    /// 版本升级用
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Auth)]
    public class VersionController : BaseApiController
    {

        private IVersionApkService _VersionService;
        public VersionController(IVersionApkService VersionService)
        {
            _VersionService = VersionService;
        }


        /// <summary>
        /// 获取最新版本及其版本在客户端请求参数中添加当前版本号currentVersion传输给后台，由后台根据客户端传过来的当前版本号currentVersion做相应的判断后给出是否强制更新。
        /// 【返回值 200 成功】
        /// </summary>
        /// <param name="osSign">系统标识</param>
        /// <param name="currentVersion">当前版本号</param>
        /// <returns>返回最新版本信息和版本日志信息</returns>
        [HttpGet()]
        [Route("/api/VersionUpgrade")]
        [AllowAnonymous]
        public async Task<VersionApkResult> GetVersionAPK(int osSign, string currentVersion)
        {
            try
            {

                var rlt = await _VersionService.GetVersionInfo(osSign, currentVersion);
                return new VersionApkResult() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new VersionApkResult() { Code = (int)ResultCode.Exception, Message = $"获取版本失败{ex.Message}" };
            }


        }
        /// <summary>
        /// 获取最新版本及其版本在客户端请求参数中添加当前版本号currentVersion传输给后台，由后台根据客户端传过来的当前版本号currentVersion做相应的判断后给出是否强制更新。
        /// 【返回值 200 成功】
        /// </summary>
        /// <param name="osSign">系统标识</param>
        /// <returns>返回最新版本信息和版本日志信息</returns>
        [HttpGet()]
        [Route("/api/Version")]
        [AllowAnonymous]
        public async Task<Result> GetVersion(int osSign)
        {
            try
            {

                var rlt = await _VersionService.GetVersionInfo(osSign);
                return new Result() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = $"获取版本失败{ex.Message}" };
            }


        }

    }

}
