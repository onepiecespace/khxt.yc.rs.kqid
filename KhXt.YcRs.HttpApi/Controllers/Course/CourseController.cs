﻿using HuangLiCollege;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Message;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx.Extensions;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Course;
using KhXt.YcRs.HttpApi.Models.Order;
using KhXt.YcRs.HttpApi.Models.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Senparc.Weixin.TenPay.V3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Result = KhXt.YcRs.Domain.Result;

namespace KhXt.YcRs.HttpApi.Controllers.Course
{
    /// <summary>
    /// 课程API （待修改对接询问）
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Course)]
    public partial class CourseController : BaseApiController
    {
        private readonly string _aidKey = "aid";
        private readonly string _aid = "college";
        #region 构造函数
        //private readonly int _smsTime = 60 * 150;
        private readonly int _tokenTime = 60 * 24 * 900;
        private readonly IUserService _userService;
        private readonly ICourseBuyCollectService _courseBuyCollectService;
        private readonly ICourseCateoryService _courseCateoryService;
        private readonly ICourseChildService _courseChildService;
        private readonly ICourseLogService _courseLogService;
        private readonly ICourseOrderService _courseOrderService;
        private readonly ICourseItemService _courseItemService;
        private readonly IUserCourseChildProgessService _userCourseChildProgessService;
        private readonly IAddressService _addressService;
        private readonly ICourseService _courseService;
        private readonly ICourseUserItemService _courseUserItemService;
        private readonly ICouponInfoService _couponInfoService;
        private readonly IOrderInfoService _orderInfoService;
        private readonly IOrderPayRepository _payRepository;
        public CourseController(IUserService userService,
            ICourseBuyCollectService courseBuyCollectService,
            ICourseCateoryService courseCateoryService,
            ICourseChildService courseChildService,
            ICourseOrderService courseOrderService,
            ICourseItemService courseItemService,
            IUserCourseChildProgessService userCourseChildProgessService,
            ICourseService courseService,
            IAddressService addressService,
            ICourseUserItemService courseUserItemService,
            ICouponInfoService couponInfoService,
            IOrderInfoService orderInfoService,
            ICourseLogService courseLogService,
            IOrderPayRepository payRepository
            )
        {

            _userService = userService;
            _courseBuyCollectService = courseBuyCollectService;
            _courseCateoryService = courseCateoryService;
            _courseChildService = courseChildService;
            _courseOrderService = courseOrderService;
            _courseItemService = courseItemService;
            _userCourseChildProgessService = userCourseChildProgessService;
            _addressService = addressService;
            _courseService = courseService;
            _courseUserItemService = courseUserItemService;
            _couponInfoService = couponInfoService;
            _orderInfoService = orderInfoService;
            _courseLogService = courseLogService;
            _payRepository = payRepository;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        public async Task<Result<List<CourseQueryModel>>> GetAll()
        {
            try
            {
                var list = await _courseService.GetAll();
                return new Result<List<CourseQueryModel>> { Data = list };
            }
            catch (Exception ex)
            {
                Logger.Error("获取所有课程失败", ex);
                return new Result<List<CourseQueryModel>> { Code = (int)ResultCode.Exception, Message = "获取所有课程失败" };
            }
        }
        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        public async Task<Result<List<CoursePcThinModel>>> GetPcWithChildModelsAsync(long userId)
        {
            try
            {
                var list = await _courseService.GetPcWithChildModelsList(userId);

                return new Result<List<CoursePcThinModel>> { Data = list };
            }
            catch (Exception ex)
            {
                Logger.Error("获取所有课程失败", ex);
                return new Result<List<CoursePcThinModel>> { Code = (int)ResultCode.Exception, Message = "获取所有课程失败" };
            }
        }

        #region 【课程详细页面】

        /// <summary>
        /// 【课程详细页面】获取课程详细内容信息
        /// 【CourseName 课程标题，Detailed课程内容描述 DiscountPrice折扣价格 Price 真是价格】
        /// </summary> 
        /// <param name="couerseId">课程具体的Id </param> 
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        public async Task<CouerseDetailedResult> GetDetailed(int couerseId = 0)
        {
            CouerseDetailedResult result = new CouerseDetailedResult();

            try
            {

                var lrt = await _courseService.GetAsync(couerseId);
                var infoEntity = lrt.Data;
                if (infoEntity == null)
                    return new CouerseDetailedResult { Message = "课程不存在！" };
                var couerse = infoEntity.MapTo<CouerseDetailedListInfo>();
                if (!string.IsNullOrEmpty(UserId))
                {
                    var courseBuyCollectEntity = _courseBuyCollectService.Table().FirstOrDefault(n => n.CourseId == couerseId && n.UserId == int.Parse(UserId));
                    couerse.IsCollection = courseBuyCollectEntity == null ? 0 : courseBuyCollectEntity.IsCollect;
                    couerse.IsBuy = courseBuyCollectEntity == null ? 0 : courseBuyCollectEntity.IsBuy;
                    couerse.ExpirationDate = courseBuyCollectEntity.ExpirationDate;
                    couerse.IsHide = courseBuyCollectEntity.IsHide;
                    if (couerse.IsBuy == 1)
                    {
                        couerse.OrderId = courseBuyCollectEntity.CourseOrderId;
                        if (DateTime.Compare(DateTime.Now, courseBuyCollectEntity.ExpirationDate) > 0)
                        {
                            couerse.exmsg = "此课程免费期已过，建议购买观看";
                            couerse.Isexpired = true;
                        }
                        else
                        {
                            couerse.exmsg = "";
                            couerse.Isexpired = false;
                        }
                    }
                }
                else
                {
                    couerse.IsCollection = 0;
                    couerse.IsBuy = 0;
                    couerse.Isexpired = false;
                }
                couerse.CourseId = infoEntity.Id;
                couerse.CoursePic = infoEntity.CoursePicPhoneMini;
                couerse.CoursePicPhone = infoEntity.CoursePicPhone;
                couerse.baseurl = DomainSetting.ResourceUrl;

                string pic = infoEntity.CoursePicContent;
                if (!string.IsNullOrWhiteSpace(pic))
                {
                    var picStrings = pic.Split('|', StringSplitOptions.None);
                    var piclist = picStrings.ToList();
                    List<string> piccontents = new List<string>();
                    var timestr = "?_=" + CommonHelper.GetTimeStamp();
                    foreach (var item in piclist)
                    {
                        piccontents.Add(item + timestr);
                    }
                    couerse.CoursePicContent = piccontents;
                }
                result.Data = couerse;
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }
        /// <summary>
        /// 【课程详细页面】获取课程详细内容信息
        /// 【CourseName 课程标题，Detailed课程内容描述 DiscountPrice折扣价格 Price 真是价格】
        /// </summary> 
        /// <param name="couerseId">课程具体的Id </param> 
        /// <returns></returns>
        [HttpGet()]
        public async Task<CouerseDetailedResult> GetAuthDetailed(int couerseId = 0)
        {
            CouerseDetailedResult result = new CouerseDetailedResult();

            try
            {

                var lrt = await _courseService.GetAsync(couerseId);
                var infoEntity = lrt.Data;
                if (infoEntity == null)
                    return new CouerseDetailedResult { Message = "课程不存在！" };
                var couerse = infoEntity.MapTo<CouerseDetailedListInfo>();
                if (!string.IsNullOrEmpty(UserId))
                {
                    var courseBuyCollectEntity = _courseBuyCollectService.Table().FirstOrDefault(n => n.CourseId == couerseId && n.UserId == int.Parse(UserId));
                    if (courseBuyCollectEntity != null)
                    {
                        couerse.IsCollection = courseBuyCollectEntity == null ? 0 : courseBuyCollectEntity.IsCollect;
                        couerse.IsBuy = courseBuyCollectEntity == null ? 0 : courseBuyCollectEntity.IsBuy;
                        couerse.ExpirationDate = courseBuyCollectEntity.ExpirationDate;
                        couerse.IsHide = courseBuyCollectEntity.IsHide;
                        if (couerse.IsBuy == 1)
                        {
                            couerse.OrderId = courseBuyCollectEntity.CourseOrderId;
                            if (DateTime.Compare(DateTime.Now, courseBuyCollectEntity.ExpirationDate) > 0)
                            {
                                couerse.exmsg = "此课程免费期已过，建议购买观看";
                                couerse.Isexpired = true;
                            }
                            else
                            {
                                couerse.exmsg = "";
                                couerse.Isexpired = false;
                            }
                        }
                    }
                }
                else
                {
                    couerse.IsCollection = 0;
                    couerse.IsBuy = 0;
                    couerse.Isexpired = false;
                }
                couerse.CourseId = infoEntity.Id;
                couerse.CoursePic = infoEntity.CoursePicPhoneMini;
                couerse.CoursePicPhone = infoEntity.CoursePicPhone;
                string pic = infoEntity.CoursePicContent;
                couerse.baseurl = DomainSetting.ResourceUrl;

                if (!string.IsNullOrWhiteSpace(pic))
                {
                    var picStrings = pic.Split('|', StringSplitOptions.None);
                    var piclist = picStrings.ToList();
                    List<string> piccontents = new List<string>();
                    var timestr = "?_=" + CommonHelper.GetTimeStamp();
                    foreach (var item in piclist)
                    {
                        piccontents.Add(item + timestr);
                    }
                    couerse.CoursePicContent = piccontents;
                }
                //课程播放量统计增加
                //double value = 1;
                //var taskkey = RedisKeyHelper.CreateTaskCourseLerunCount();
                //Redis.HashIncrement(taskkey, couerseId.ToString(), value);

                result.Data = couerse;
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }
        /// <summary>
        /// 【课程详细页面】获取课程详细内容信息
        /// 【CourseName 课程标题，Detailed课程内容描述 DiscountPrice折扣价格 Price 真是价格】
        /// </summary> 
        /// <param name="couerseId">课程具体的Id </param> 
        /// <param name="userId">用户ID </param> 
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        public async Task<CouerseDetailedResult> GetH5Detailed(int couerseId = 0, int userId = 0)
        {
            CouerseDetailedResult result = new CouerseDetailedResult();

            try
            {

                var lrt = await _courseService.GetAsync(couerseId);
                var infoEntity = lrt.Data;
                if (infoEntity == null)
                    return new CouerseDetailedResult { Message = "课程不存在！" };
                var couerse = infoEntity.MapTo<CouerseDetailedListInfo>();
                if (userId > 0)
                {
                    var courseBuyCollectEntity = _courseBuyCollectService.Table().FirstOrDefault(n => n.CourseId == couerseId && n.UserId == userId);
                    if (courseBuyCollectEntity != null)
                    {
                        couerse.IsCollection = courseBuyCollectEntity == null ? 0 : courseBuyCollectEntity.IsCollect;
                        couerse.IsBuy = courseBuyCollectEntity == null ? 0 : courseBuyCollectEntity.IsBuy;
                        couerse.ExpirationDate = courseBuyCollectEntity.ExpirationDate;
                        couerse.IsHide = courseBuyCollectEntity.IsHide;
                        if (couerse.IsBuy == 1)
                        {
                            couerse.OrderId = courseBuyCollectEntity.CourseOrderId;
                            if (DateTime.Compare(DateTime.Now, courseBuyCollectEntity.ExpirationDate) > 0)
                            {
                                couerse.exmsg = "此课程免费期已过，建议购买观看";
                                couerse.Isexpired = true;
                            }
                            else
                            {
                                couerse.exmsg = "";
                                couerse.Isexpired = false;
                            }
                        }
                    }
                }
                else
                {
                    couerse.IsCollection = 0;
                    couerse.IsBuy = 0;
                    couerse.Isexpired = false;
                }
                couerse.CourseId = infoEntity.Id;
                couerse.CoursePic = infoEntity.CoursePicPhoneMini;
                couerse.CoursePicPhone = infoEntity.CoursePicPhone;
                string pic = infoEntity.CoursePicContent;
                couerse.baseurl = DomainSetting.ResourceUrl;

                if (!string.IsNullOrWhiteSpace(pic))
                {
                    var picStrings = pic.Split('|', StringSplitOptions.None);
                    var piclist = picStrings.ToList();
                    List<string> piccontents = new List<string>();
                    var timestr = "?_=" + CommonHelper.GetTimeStamp();
                    foreach (var item in piclist)
                    {
                        piccontents.Add(item + timestr);
                    }
                    couerse.CoursePicContent = piccontents;
                }
                //课程播放量统计增加
                double value = 1;
                var taskkey = RedisKeyHelper.CreateTaskCourseLerunCount();
                Redis.HashIncrement(taskkey, couerseId.ToString(), value);
                result.Data = couerse;
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }
        /// <summary>
        /// 【课程详细页面的课程目录】【childType 子课程类型1直播2录播3点播】
        /// </summary>
        /// <param name="pid">父类 课程的ID</param>
        /// <param name="pageSize">一页多少条</param>
        /// <param name="pageIndex">当前第几页</param>
        /// <param name="userId">用户ID</param> 
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        public async Task<CourseChildResult> GetCourseChild(int pid = 0, int pageSize = 40, int pageIndex = 1, int userId = 0)
        {
            CourseChildResult result = new CourseChildResult();
            try
            {
                int IsBuy = 0;
                string OrderId = "";
                string ChildIds = "0";
                string SecretKey = "";
                bool authen = false;
                #region  条件判断
                var lrt = await _courseService.GetAsync(pid);
                var infoEntity = lrt.Data;
                if (infoEntity != null)
                {
                    ChildIds = infoEntity.ChildIds;
                    SecretKey = infoEntity.SecretKey;
                    if (!string.IsNullOrEmpty(SecretKey))
                    {
                        authen = true;
                    }
                }
                if (userId > 0)
                {
                    var courseBuyCollectEntity = _courseBuyCollectService.Table().FirstOrDefault(n => n.CourseId == pid && n.UserId == userId);
                    if (courseBuyCollectEntity != null)
                    {
                        IsBuy = courseBuyCollectEntity == null ? 0 : courseBuyCollectEntity.IsBuy;
                        if (IsBuy == 1)
                        {
                            OrderId = courseBuyCollectEntity.CourseOrderId;
                        }
                    }
                }

                Expression<Func<CourseChildInfo, bool>> predicate = null;
                Action<Orderable<CourseChildInfo>> preorder = null;
                preorder = orderch => orderch.Asc(n => n.ChildSort);
                predicate = n => n.ParentId == pid;
                #endregion
                var pageList = await _courseChildService.GetPageListAsync(predicate, preorder, pageSize, pageIndex);

                pageList.ForEach(n =>
                {
                    n.ChildNameAndTeacher = $"{n.ChildName}|{n.ChildTeacher}";
                    //子课程播放量统计增加
                    var rescount = 0;
                    var taskkey = RedisKeyHelper.CreateTaskCourseChidLerunCount();
                    var enumKey = RedisKeyHelper.CreateChildLerunCountKey(n.ParentId, n.id);
                    var Total = Redis.HashGet(taskkey, enumKey.ToString());
                    if (!string.IsNullOrEmpty(Total))
                    {
                        rescount = Convert.ToInt32(Total);
                    }
                    if (ChildIds.Contains(n.id.ToString()))
                    {
                        n.IsTrialable = 1;
                    }
                    n.ChildLerunCount = rescount;
                });
                //课程播放量统计增加
                double value = 1;
                var taskcoursekey = RedisKeyHelper.CreateTaskCourseLerunCount();
                Redis.HashIncrement(taskcoursekey, pid.ToString(), value);
                result.Data = new CourseChildListInfo()
                {
                    ChildIds = ChildIds,
                    IsBuy = IsBuy,
                    OrderId = OrderId,
                    authen = authen,
                    SecretKey = SecretKey,
                    CourseChildList = pageList,
                };
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }


        /// <summary>
        /// 【课程直播录播单独接口信息】 
        /// </summary>
        /// <param name="type">【type 课程类型1直播2录播3点播】可为空</param>
        /// <param name="courseld">课程Id 可为空</param>
        /// <param name="courseChildId">课程子Id</param>
        /// <returns></returns>
        [HttpGet()]
        public async Task<VideoLiveResult> GetVideoLiveInfo(int type, int courseld, int courseChildId)
        {
            try
            {
                string UserIp = HuangLiCollege.IpHelper.GetRealIp(Request.HttpContext);
                var rlt = await _courseService.GetVideoLiveInfo(UserIdValue, courseChildId, UserIp);
                return new VideoLiveResult() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new VideoLiveResult() { Code = (int)ResultCode.Exception, Message = $"获取失败{ex.Message}" };
            }
        }

        /// <summary>
        /// 查询时间段某个课程人员学习情况
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="channelId"></param>
        /// <param name="ignore"></param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<Result<Dictionary<string, List<CourseLogModel>>>> ExportExcelBaseDay(string startTime, string endTime, string channelId, bool ignore = true)
        {
            try
            {
                var rlt = await _courseLogService.ExportExcelBaseDay(startTime, endTime, channelId, ignore);
                return rlt;
            }
            catch (Exception ex)
            {
                Logger.Error("按天导出 某个课程人员情况 异常", ex);
                return new Result<Dictionary<string, List<CourseLogModel>>> { Code = (int)ResultCode.Exception, Message = "按天导出 某个课程人员情况 异常" };
            }
        }
        /// <summary>
        /// 设置课程收藏
        /// </summary>
        /// <param name="courseId">课程ID</param>
        /// <param name="collect"> 1收藏，0未收藏</param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<VideoLiveResult> SetCourseCollect([FromForm] int courseId, [FromForm] int collect)
        {
            VideoLiveResult result = new VideoLiveResult();
            try
            {
                var info = await _userService.GetUser(UserIdValue);
                if (info == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    return result;
                }

                //存在收藏，不存在修改

                var courseBuyCollectEntity = _courseBuyCollectService.Table().FirstOrDefault(n => n.UserId == info.Userid && n.CourseId == courseId);
                if (courseBuyCollectEntity == null)
                {
                    _courseBuyCollectService.Add(new CourseBuyCollectEntity
                    {
                        UserId = (int)info.Userid,
                        CourseId = courseId,
                        CourseOrderId = "",
                        CreateId = 0,
                        CreateTime = DateTime.Now,
                        IsBuy = 0,
                        IsHide = 0,
                        IsCollect = collect,
                    });
                }
                else
                {
                    courseBuyCollectEntity.IsCollect = collect;
                    _courseBuyCollectService.Update(courseBuyCollectEntity);
                }
                if (collect == 1)
                {
                    //向任务REDIS中写数据 
                    //  await _taskService.SetTaskToRedis(ActionType.Sckc, 1, UserIdValue);
                }
                result.Message = $"成功";
                return result;

            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }

        #endregion

        #region 【获取课程下单信息】
        /// <summary>
        /// 获取课程信息【点击支付，需要调用/api/Pay 提交订单，选择微信支付】
        /// 【HaveAddress 1表示存在 0便是不存】
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <param name="device">ios = 1, andriod = 2,pc=4</param> 
        /// <returns></returns>
        [HttpGet()]
        public async Task<OrderResult> GetOrder(int courseId, PlatFormType device = PlatFormType.ios)
        {
            OrderResult result = new OrderResult();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"用户信息已过期，请重新登录";
                    return result;
                }

                var useInfo = await _userService.GetUser(UserIdValue);
                var useAddress = await _addressService.Get(UserIdValue);
                var addInfo = useAddress.Data;
                int ok = addInfo != null && !string.IsNullOrWhiteSpace(addInfo.Phone) ? 1 : 0;
                var lrt = await _courseService.GetAsync(courseId);
                var courseEntity = lrt.Data;
                if (courseEntity == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"课程不存在{courseId}";
                    return result;
                }

                Result res = new Result();
                courseEntity.ExpressPirce = courseEntity.IsExpress == 0 ? 0M : courseEntity.ExpressPirce;
                string spBillno = string.Format("{0}{1}", EncryptionUtility.OrderCode(), TenPayV3Util.BuildRandomStr(6));

                var payInfo = courseEntity.MapTo<PayOrderInfo>();
                payInfo.CourseId = courseEntity.Id;
                payInfo.IsDiscount = courseEntity.IsDiscount;
                decimal PayPrice = 0;
                int PayType = 0;
                if (device == PlatFormType.ios)
                {
                    PayPrice = courseEntity.IosPayPirce;
                    PayType = 4;
                }
                else
                {
                    PayPrice = courseEntity.DiscountPrice;
                    PayType = 2;

                }
                int UsableCount = 0;
                //如果开启优惠 录播课 可以兑换  收费直播课限制使用兑换券&& courseEntity.IsLive == 0
                if (courseEntity.IsDiscount == 1)
                {
                    #region  获取可用的课程兑换券
                    var rltCoupon = _couponInfoService.GetUsableCount(UserIdValue, (int)CouponStatus.NoUse, (int)BusinessType.Course);

                    if (rltCoupon != null)
                    {
                        if (rltCoupon.Result.Code == 200)
                        {
                            UsableCount = Convert.ToInt32(rltCoupon.Result.Data);
                        }
                    }
                    #endregion
                }
                payInfo.UsableCount = UsableCount;
                payInfo.PreferentialBeginTime = courseEntity.PreferentialBeginTime.ToString("yyyy.MM.dd");
                payInfo.PreferentialEndTime = courseEntity.PreferentialEndTime.ToString("yyyy.MM.dd");
                payInfo.ValidBeginTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.ValidEndTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.OrderNo = spBillno;
                if (courseEntity.DiscountPrice > 0)
                {
                    CourseOrderEntity orderentity = new CourseOrderEntity();
                    var courseOrder = _courseOrderService.Create(new CourseOrderEntity
                    {
                        OrderNo = spBillno,
                        UserId = useInfo.Userid,
                        PayPrice = PayPrice,
                        OrderState = 0,
                        IsPay = 0,
                        PayTime = DateTime.Now,
                        PayType = PayType,
                        PayNo = spBillno,
                        CourseId = courseEntity.Id,
                        IsSend = 0,
                        SendTime = DateTime.Now,
                        IsGet = 0,
                        GetTime = DateTime.Now,
                        PackageNo = "",
                        PackageType = 0,
                        OrderDes = null,
                        AddressId = 0,
                        IsDelete = 0,
                        CreateId = 0,
                        CreateTime = DateTime.Now,
                        UpdateTime = DateTime.Now,
                        TenantId = 0,
                        IsInvoice = 0,
                        InvoiceDes = ""
                    });
                    long.TryParse(courseOrder.ToString(), out long nid);
                    if (nid == 0)
                    {
                        result.Code = (int)ResultCode.Fail;
                        result.Message = $"生成订单失败";
                        return result;
                    }
                    else
                    {
                        #region  订单主体
                        int PaymentType = 0;
                        float TotalFee = 0;
                        if (device == PlatFormType.ios)
                        {
                            TotalFee = float.Parse(courseEntity.IosPayPirce.ToString());
                            PaymentType = 2;
                        }
                        else
                        {
                            TotalFee = float.Parse(courseEntity.DiscountPrice.ToString());
                            PaymentType = 0;

                        }
                        OrderInfoModel OrderInfo = new OrderInfoModel();
                        OrderInfo.ProductId = courseEntity.Iosproductid;
                        OrderInfo.Payment = TotalFee.ToString();
                        OrderInfo.PaymentType = PaymentType;
                        OrderInfo.PostFee = "0";
                        OrderInfo.ShippingName = "";
                        OrderInfo.ShippingCode = "";
                        OrderInfo.IsFrom = 0;
                        OrderInfo.BusinessType = (int)BusinessType.Course;
                        var orderInfoEntity = OrderInfo.MapTo<OrderInfoEntity>();
                        orderInfoEntity.Id = spBillno;
                        orderInfoEntity.UserId = useInfo.Userid;
                        #endregion
                        #region  订单明细
                        OrderItemModel OrderItem = new OrderItemModel();
                        OrderItem.ItemId = courseEntity.Id.ToString();
                        OrderItem.Num = 1;
                        OrderItem.Title = courseEntity.CourseName;
                        OrderItem.Description = courseEntity.TeachersUserName;
                        OrderItem.Price = TotalFee;
                        OrderItem.TotalFee = TotalFee;
                        OrderItem.PicPath = courseEntity.CoursePicPhoneMini;
                        var orderItemEntity = OrderItem.MapTo<OrderItemEntity>();
                        #endregion
                        #region  收货地址明细
                        AddressInfo AddressInfo = new AddressInfo();
                        AddressInfo.Userid = (int)useInfo.Userid;
                        if (addInfo != null)
                        {
                            AddressInfo.Sex = addInfo.Sex;
                            AddressInfo.Phone = addInfo.Phone;
                            AddressInfo.Consignee = addInfo.Consignee;
                            AddressInfo.Provincial = addInfo.Provincial;
                            AddressInfo.City = addInfo.City;
                            AddressInfo.Areas = addInfo.Areas;
                            AddressInfo.Address = addInfo.Address;
                        }
                        var orderShippingEntity = AddressInfo.MapTo<OrderShippingEntity>();
                        #endregion
                        #region  使用优惠券明细
                        OrderCouponInfoModel OrderCouponInfo = new OrderCouponInfoModel();
                        //OrderCouponInfo.ItemId= courseEntity.Id.ToString();
                        var orderCouponEntities = OrderCouponInfo.MapTo<List<OrderCouponEntity>>();
                        #endregion
                        var rlt = await _orderInfoService.CreateOrder(orderInfoEntity, orderItemEntity, orderShippingEntity,
                        orderCouponEntities);
                        // return new OrderStatusInfo() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
                    }
                    //生成购买课程记录 回调修改状态
                    var courseBuyCollectEntity = _courseBuyCollectService.Table()
                        .FirstOrDefault(n => n.CourseId == courseEntity.Id && n.UserId == useInfo.Userid);
                    if (courseBuyCollectEntity == null)
                    {
                        _courseBuyCollectService.Add(new CourseBuyCollectEntity
                        {
                            UserId = (int)useInfo.Userid,
                            CourseId = (int)courseEntity.Id,
                            CourseOrderId = spBillno,
                            CreateId = 0,
                            CreateTime = DateTime.Now,
                            IsBuy = 0,
                            IsHide = 0,
                            IsCollect = 0,
                        });
                    }
                }
                result.Data = new OrderListInfo()
                {
                    HaveAddress = ok,
                    AddressList = addInfo,
                    PayOrderList = payInfo
                };
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;

        }
        #endregion

        #region 【获取课程信息】
        /// <summary>
        /// 获取课程信息【点击支付，需要调用/api/Pay 提交订单，选择微信支付】
        /// 【HaveAddress 1表示存在 0便是不存】
        /// </summary>
        /// <param name="courseId">免费课程Id</param>
        /// <returns></returns>
        [HttpGet()]
        public async Task<OrderResult> GetFreeOrder(int courseId)
        {
            OrderResult result = new OrderResult();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"用户信息已过期，请重新登录";
                    return result;
                }

                var useInfo = await _userService.GetUser(UserIdValue);

                var useAddress = await _addressService.Get(UserIdValue);
                var addInfo = useAddress.Data;
                int ok = addInfo != null && !string.IsNullOrWhiteSpace(addInfo.Phone) ? 1 : 0;
                var lrt = await _courseService.GetAsync(courseId);
                var courseEntity = lrt.Data;
                if (courseEntity == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"课程不存在{courseId}";
                    return result;
                }
                courseEntity.ExpressPirce = courseEntity.IsExpress == 0 ? 0M : courseEntity.ExpressPirce;
                string spBillno = string.Format("{0}{1}", EncryptionUtility.OrderCode(), TenPayV3Util.BuildRandomStr(6));

                var payInfo = courseEntity.MapTo<PayOrderInfo>();
                payInfo.CourseId = courseEntity.Id;
                payInfo.PreferentialBeginTime = courseEntity.PreferentialBeginTime.ToString("yyyy.MM.dd");
                payInfo.PreferentialEndTime = courseEntity.PreferentialEndTime.ToString("yyyy.MM.dd");
                payInfo.ValidBeginTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.ValidEndTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.OrderNo = spBillno;
                if (courseEntity.DiscountPrice == 0 && courseEntity.IosPayPirce == 0)
                {
                    var courseOrder = _courseOrderService.Create(new CourseOrderEntity
                    {
                        OrderNo = spBillno,
                        UserId = useInfo.Userid,
                        PayPrice = 0,
                        OrderState = 1,
                        IsPay = 1,
                        PayTime = DateTime.Now,
                        PayType = 0,
                        PayNo = spBillno,
                        CourseId = courseEntity.Id,
                        IsSend = 0,
                        SendTime = DateTime.Now,
                        IsGet = 0,
                        GetTime = DateTime.Now,
                        PackageNo = "",
                        PackageType = 0,
                        OrderDes = "免费领取课程",
                        AddressId = 0,
                        IsDelete = 0,
                        CreateId = 0,
                        CreateTime = DateTime.Now,
                        UpdateTime = DateTime.Now,
                        TenantId = 0,
                        IsInvoice = 0,
                        InvoiceDes = ""
                    });
                    long.TryParse(courseOrder.ToString(), out long nid);
                    if (nid == 0)
                    {
                        result.Code = (int)ResultCode.Fail;
                        result.Message = $"生成免费订单失败";
                        return result;
                    }
                    else
                    {
                        #region  订单主体
                        float TotalFee = 0;
                        OrderInfoModel OrderInfo = new OrderInfoModel();
                        OrderInfo.ProductId = courseEntity.Iosproductid;
                        OrderInfo.Payment = TotalFee.ToString("F2");
                        OrderInfo.PaymentType = 3;
                        OrderInfo.PostFee = "0";
                        OrderInfo.ShippingName = "";
                        OrderInfo.ShippingCode = "";
                        OrderInfo.IsFrom = 0;
                        OrderInfo.BusinessType = (int)BusinessType.Course;
                        var orderInfoEntity = OrderInfo.MapTo<OrderInfoEntity>();
                        orderInfoEntity.Id = spBillno;
                        orderInfoEntity.UserId = useInfo.Userid;
                        orderInfoEntity.PaymentTime = DateTime.Now;
                        #endregion
                        #region  订单明细
                        OrderItemModel OrderItem = new OrderItemModel();
                        OrderItem.ItemId = courseEntity.Id.ToString();
                        OrderItem.Num = 1;
                        OrderItem.Title = courseEntity.CourseName;
                        OrderItem.Description = courseEntity.TeachersUserName;
                        OrderItem.Price = TotalFee;
                        OrderItem.TotalFee = 0;
                        OrderItem.PicPath = courseEntity.CoursePicPhoneMini;

                        var orderItemEntity = OrderItem.MapTo<OrderItemEntity>();
                        #endregion
                        #region  收货地址明细
                        AddressInfo AddressInfo = new AddressInfo();
                        AddressInfo.Userid = int.Parse(useInfo.Userid.ToString());
                        if (addInfo != null)
                        {
                            AddressInfo.Sex = addInfo.Sex;
                            AddressInfo.Phone = addInfo.Phone;
                            AddressInfo.Consignee = addInfo.Consignee;
                            AddressInfo.Provincial = addInfo.Provincial;
                            AddressInfo.City = addInfo.City;
                            AddressInfo.Areas = addInfo.Areas;
                            AddressInfo.Address = addInfo.Address;
                        }
                        var orderShippingEntity = AddressInfo.MapTo<OrderShippingEntity>();
                        #endregion

                        #region  使用优惠券明细
                        OrderCouponInfoModel OrderCouponInfo = new OrderCouponInfoModel();
                        //OrderCouponInfo.ItemId= courseEntity.Id.ToString();
                        var orderCouponEntities = OrderCouponInfo.MapTo<List<OrderCouponEntity>>();
                        #endregion
                        var rlt = await _orderInfoService.CreateOrder(orderInfoEntity, orderItemEntity, orderShippingEntity,
                        orderCouponEntities);

                        var orderPay = orderInfoEntity.MapTo<OrderPayEntity>();
                        orderPay.UpdateTime = orderInfoEntity.UpdateTime;
                        orderPay.CreateTime = orderInfoEntity.CreateTime;
                        orderPay.Transaction = "免费领取";
                        orderPay.IsTest = false;
                        orderPay.Aid = "college";
                        _orderInfoService.Add(orderPay);
                    }
                    //生成购买课程记录 回调修改状态
                    var courseBuyCollectEntity = _courseBuyCollectService.Table()
                        .FirstOrDefault(n => n.CourseId == courseEntity.Id && n.UserId == useInfo.Userid);
                    if (courseBuyCollectEntity == null)
                    {
                        _courseBuyCollectService.Add(new CourseBuyCollectEntity
                        {
                            UserId = (int)useInfo.Userid,
                            CourseId = (int)courseEntity.Id,
                            CourseOrderId = spBillno,
                            CreateId = 0,
                            CreateTime = DateTime.Now,
                            IsBuy = 1,
                            ExpirationDate = DateTime.Now.AddYears(1),
                            IsHide = 0,
                            IsCollect = 0,
                        });
                    }
                    else
                    {
                        courseBuyCollectEntity.CourseOrderId = spBillno;
                        courseBuyCollectEntity.IsBuy = 1;
                        courseBuyCollectEntity.CreateTime = DateTime.Now;
                        courseBuyCollectEntity.ExpirationDate = DateTime.Now.AddYears(1);
                        courseBuyCollectEntity.IsHide = 0;
                        _courseBuyCollectService.Update(courseBuyCollectEntity);
                    }
                }
                result.Data = new OrderListInfo()
                {
                    HaveAddress = ok,
                    AddressList = addInfo,
                    PayOrderList = payInfo
                };
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;

        }

        /// <summary>
        /// 支付成功，订单详细【PayStatus 订单状态 99全部, 0 进行中(即待支付) 1已完成 2取消交易 3已结算 4申请退款中 5已退款】 TODO:待优化  优化支付逻辑判断
        /// 【PayType 付款方式 1微信 2支付宝 3线下付款 4 已取消】
        /// </summary>
        /// <param name="OrderNo"></param>
        /// <param name="PayStatus">无需传参 可为空</param>
        /// <returns>订单状态 99全部, 0 进行中(即待支付) 1已完成 2取消交易 3已结算 4申请退款中 5已退款</returns>

        [HttpGet()]
        public async Task<OrderDetailedResult> GetOrderDetailed(string OrderNo, int PayStatus = 2)
        {
            OrderDetailedResult result = new OrderDetailedResult();
            try
            {


                var Isexpired = false;
                var exmsg = "";
                var IsHide = 0;
                var orderEntity = _courseOrderService.Table().FirstOrDefault(n => n.OrderNo == OrderNo);

                if (orderEntity == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "订单信息不存在";
                    return result;

                }
                if (orderEntity.CourseId == 0)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "购买课程ID不存在";
                    return result;

                }
                var lrt = await _courseService.GetAsync(orderEntity.CourseId);
                var course = lrt.Data;
                if (course == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "课程信息不存在";
                    return result;

                }

                var courseBuyCollectEntity = _courseBuyCollectService.Table().FirstOrDefault(n => n.CourseId == orderEntity.CourseId && n.UserId == orderEntity.UserId);
                if (courseBuyCollectEntity != null)
                {
                    IsHide = courseBuyCollectEntity.IsHide;
                    if (courseBuyCollectEntity.IsBuy == 1)
                    {
                        if (DateTime.Compare(DateTime.Now, courseBuyCollectEntity.ExpirationDate) > 0)
                        {
                            exmsg = "此课程免费期已过，建议购买观看";
                            Isexpired = true;
                        }
                    }
                    else
                    {
                        exmsg = "";
                        Isexpired = false;
                    }
                }
                else
                {
                    exmsg = "";
                    Isexpired = false;
                }
                result.Data = new OrderDetailedListInfo()
                {
                    PayStatus = orderEntity.IsPay == 1 ? 1 : orderEntity.OrderState,
                    PayStateCaption = convertOrderState(orderEntity.OrderState, orderEntity.CreateTime),
                    CoursePicPhoneMini = course.CoursePicPhoneMini,
                    CourseName = course.CourseName,
                    Detailed = course.Detailed,
                    OrderNo = orderEntity.OrderNo,
                    PayTime = orderEntity.PayTime.ToString19() ?? DateTime.Now.ToString19(),
                    PayType = orderEntity.PayType,
                    payCaption = Enum.GetName(typeof(OrderPayType), orderEntity.PayType),
                    OrderTime = orderEntity.CreateTime.ToString19() ?? DateTime.Now.AddHours(-1).ToString19(),
                    IosPayPirce = course.IosPayPirce,
                    Iosproductid = course.Iosproductid,
                    IosProductidName = course.IosProductidName,
                    Price = course.Price,
                    DiscountPrice = orderEntity.PayPrice,
                    IsExpress = course.IsExpress,
                    ExpressPirce = course.ExpressPirce,
                    IsHide = IsHide,
                    Isexpired = Isexpired,
                    exmsg = exmsg
                };

                if (result.Data.PayStateCaption == "已取消" && (orderEntity.OrderState != 2))
                {

                    //订单超过15分钟未支付时，为已取消状态，需要更新数据库订单状态为2
                    orderEntity.OrderState = 2;
                    _courseOrderService.Update(orderEntity);
                }

            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;

        }

        #endregion

        #region 【新版获取课程信息】
        /// <summary>
        /// 获取使用优惠券后的修改后的订单信息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <param name="orderNo">订单号</param>
        ///   <param name="UserCouponId">用户选择兑换券ID</param>
        /// <returns></returns>
        [HttpGet()]
        public async Task<OrderResult> GetCourseCoupon(int courseId, string orderNo, int UserCouponId)
        {
            OrderResult result = new OrderResult();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"用户信息已过期，请重新登录";
                    return result;
                }
                var isCanRlt = _couponInfoService.GetIsCanUse(UserCouponId, courseId.ToString()).GetAwaiter();
                if (isCanRlt.GetResult().Code != (int)ResultCode.Success)
                {
                    result.Code = (int)ResultCode.warning;
                    result.Message = $"该课程不支持课程兑换券兑换";
                    return result;
                }
                var useInfo = await _userService.GetUser(UserIdValue);

                var useAddress = await _addressService.Get(UserIdValue);
                var addInfo = useAddress.Data;
                int ok = addInfo != null && !string.IsNullOrWhiteSpace(addInfo.Phone) ? 1 : 0;
                var lrt = await _courseService.GetAsync(courseId);
                var courseEntity = lrt.Data;
                if (courseEntity == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"课程不存在{courseId}";
                    return result;
                }
                var orderEntity = _courseOrderService.Table().FirstOrDefault(n => n.OrderNo == orderNo);

                if (orderEntity == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "订单信息不存在";
                    return result;

                }
                courseEntity.ExpressPirce = courseEntity.IsExpress == 0 ? 0M : courseEntity.ExpressPirce;
                int UsableCount = 0;
                //如果开启优惠 直播课限制使用兑换券&& courseEntity.IsLive==0
                if (courseEntity.IsDiscount == 1)
                {
                    #region  获取可用的优惠券
                    var rltCoupon = _couponInfoService.GetUsableCount(UserIdValue, (int)CouponStatus.NoUse, (int)BusinessType.Course);

                    if (rltCoupon != null)
                    {
                        if (rltCoupon.Result.Code == 200)
                        {
                            UsableCount = Convert.ToInt32(rltCoupon.Result.Data);
                        }
                    }
                    #endregion
                }
                var payInfo = courseEntity.MapTo<PayOrderInfo>();
                payInfo.CourseId = courseEntity.Id;
                payInfo.IsDiscount = courseEntity.IsDiscount;
                if (UsableCount >= 1)
                {
                    payInfo.UsableCount = UsableCount - 1;
                }
                else
                {
                    payInfo.UsableCount = 0;
                }

                payInfo.PreferentialBeginTime = courseEntity.PreferentialBeginTime.ToString("yyyy.MM.dd");
                payInfo.PreferentialEndTime = courseEntity.PreferentialEndTime.ToString("yyyy.MM.dd");
                payInfo.ValidBeginTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.ValidEndTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.OrderNo = orderNo;
                if (UserCouponId > 0)//修改价格为0元购
                {
                    orderEntity.PayPrice = 0;
                    orderEntity.OrderDes = "优惠券兑换购买";
                }
                _courseOrderService.Update(orderEntity);
                payInfo.DiscountPrice = orderEntity.PayPrice.ToString("F2");
                payInfo.IosPayPirce = orderEntity.PayPrice.ToString("F2");
                result.Data = new OrderListInfo()
                {
                    HaveAddress = ok,
                    AddressList = addInfo,
                    PayOrderList = payInfo
                };
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;

        }

        /// <summary>
        /// 新版 兑换券零元购
        /// </summary>
        /// <param name="courseId">兑换课程Id</param>
        /// <param name="orderNo">兑换课程订单</param>
        /// <param name="UserCouponId">用户选择兑换券ID</param>
        /// <returns></returns>
        [HttpGet()]
        public async Task<OrderResult> ExchangeOrder(int courseId, string orderNo, int UserCouponId)
        {
            OrderResult result = new OrderResult();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"用户信息已过期，请重新登录";
                    return result;
                }
                var isCanRlt = _couponInfoService.GetIsCanUse(UserCouponId, courseId.ToString()).GetAwaiter();
                if (isCanRlt.GetResult().Code != (int)ResultCode.Success)
                {
                    result.Code = (int)ResultCode.warning;
                    result.Message = $"该课程不支持课程兑换券兑换";
                    return result;
                }
                var useInfo = await _userService.GetUser(UserIdValue);

                var useAddress = await _addressService.Get(UserIdValue);
                var addInfo = useAddress.Data;
                int ok = addInfo != null && !string.IsNullOrWhiteSpace(addInfo.Phone) ? 1 : 0;
                var lrt = await _courseService.GetAsync(courseId);
                var courseEntity = lrt.Data;
                if (courseEntity == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"课程不存在{courseId}";
                    return result;
                }
                var orderEntity = _courseOrderService.Table().FirstOrDefault(n => n.OrderNo == orderNo);

                if (orderEntity == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "订单信息不存在";
                    return result;

                }
                courseEntity.ExpressPirce = courseEntity.IsExpress == 0 ? 0M : courseEntity.ExpressPirce;
                var payInfo = courseEntity.MapTo<PayOrderInfo>();
                payInfo.CourseId = courseEntity.Id;
                payInfo.PreferentialBeginTime = courseEntity.PreferentialBeginTime.ToString("yyyy.MM.dd");
                payInfo.PreferentialEndTime = courseEntity.PreferentialEndTime.ToString("yyyy.MM.dd");
                payInfo.ValidBeginTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.ValidEndTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.OrderNo = orderNo;
                payInfo.DiscountPrice = orderEntity.PayPrice.ToString("F2");
                payInfo.IosPayPirce = orderEntity.PayPrice.ToString("F2");

                orderEntity.IsPay = 1;
                orderEntity.OrderState = 1;
                orderEntity.PayType = 0;
                orderEntity.PayPrice = 0;
                orderEntity.PayTime = DateTime.Now;
                orderEntity.SendTime = DateTime.Now;
                orderEntity.OrderDes = "兑换券" + UserCouponId + "兑换收费课程";
                _courseOrderService.Update(orderEntity);
                var orderInfo = _orderInfoService.Get(orderNo);
                if (orderInfo != null)
                {
                    orderInfo.Status = (int)OrderStatus.Success;
                    orderInfo.PaymentType = 3;
                    orderInfo.Payment = "0.00";
                    orderInfo.PaymentTime = DateTime.Now;
                    _orderInfoService.Update(orderInfo);
                    await _couponInfoService.userCouponHasUsed(useInfo.Userid, UserCouponId);
                }

                //生成购买课程记录 回调修改状态
                var courseBuyCollectEntity = _courseBuyCollectService.Table()
                    .FirstOrDefault(n => n.CourseId == courseEntity.Id && n.UserId == useInfo.Userid);
                if (courseBuyCollectEntity == null)
                {
                    _courseBuyCollectService.Add(new CourseBuyCollectEntity
                    {
                        UserId = (int)useInfo.Userid,
                        CourseId = (int)courseEntity.Id,
                        CourseOrderId = orderNo,
                        CreateId = 0,
                        CreateTime = DateTime.Now,
                        IsBuy = 1,
                        ExpirationDate = DateTime.Now.AddYears(1),
                        IsHide = 0,
                        IsCollect = 0,
                    });
                }
                else
                {
                    courseBuyCollectEntity.CourseOrderId = orderNo;
                    courseBuyCollectEntity.IsBuy = 1;
                    courseBuyCollectEntity.CreateTime = DateTime.Now;
                    courseBuyCollectEntity.ExpirationDate = DateTime.Now.AddYears(1);
                    courseBuyCollectEntity.IsHide = 0;
                    _courseBuyCollectService.Update(courseBuyCollectEntity);
                }

                result.Data = new OrderListInfo()
                {
                    HaveAddress = ok,
                    AddressList = addInfo,
                    PayOrderList = payInfo
                };
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;

        }
        #endregion

        #region 课程课后作业题库

        /// <summary>
        ///  课程播放列表信息 
        /// 【回放就是直播结束时间】
        /// 【即将开始就是直播开始时间提前12小时】
        /// 【子课程类型1直播2录播3点播 4 即将开始】
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet()]
        public async Task<CourseItemResult> GetCourseItemList(long courseId, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                bool authen = false;
                string SecretKey = "";
                bool wxstatus = false;
                string wxqrcode = "";
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    return new CourseItemResult() { Code = (int)ResultCode.Fail, Message = $"用户信息已过期，请重新登录" };
                }
                if (pageSize == 10)
                {
                    pageSize = 100;
                }
                #region 获取主课程信息
                var courrlt = await _courseService.GetAsync(courseId);
                var courseinfo = courrlt.Data;
                if (courseinfo != null)
                {

                    if (!string.IsNullOrEmpty(courseinfo.SecretKey))
                    {
                        authen = true;
                        SecretKey = courseinfo.SecretKey;
                    }
                    else
                    {
                        authen = false;
                        SecretKey = "";
                    }
                    if (courseinfo.WxStatus)
                    {
                        wxqrcode = $"{DomainSetting.ResourceUrl}img/collegecourse/{courseinfo.Id}_wxqr.png";
                        wxstatus = true;
                    }

                    #endregion
                    var rlt = await _courseChildService.GetUserCourseListResult(long.Parse(UserId), courseId, pageIndex, pageSize);
                    PageList<CourseUserListModel> courseItemList = new PageList<CourseUserListModel>();
                    courseItemList = rlt.Data.CourseItemList;
                    var courseItem = new CourseItemListInfo()
                    {
                        authen = authen,
                        SecretKey = SecretKey,
                        wxstatus = wxstatus,
                        wxqrcode = wxqrcode,
                        PageIndex = pageIndex,
                        PageSize = pageSize,
                        CourseItemList = courseItemList,
                        TotalCount = courseItemList.Count
                    };
                    return new CourseItemResult { Data = courseItem };
                }
                else
                {
                    return new CourseItemResult { Code = (int)ResultCode.warning, Message = $"没有找到主课程" };
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return new CourseItemResult() { Code = (int)ResultCode.Exception, Message = $"获取我的课程异常：{ex.Message}" };
            }
        }
        /// <summary>
        /// 获取课程子课程课后作业题库列表【类似错题本    UseTime 每道题的用时 默认0 】【AlreadyDone/TotalDone 6/10 看设计图】 
        /// </summary>
        /// <param name="courseId">父课程</param>
        /// <param name="courseChildId">子课程</param>
        /// <param name="alreadyDone">完成度</param>
        /// <returns></returns>
        [HttpGet()]

        public async Task<CourseItemDoneResult> GetCourseItemBankList(long courseId, long courseChildId, int alreadyDone = 0)
        {
            try
            {
                var result = new CourseItemDoneResult();
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    return new CourseItemDoneResult() { Code = (int)ResultCode.Fail, Message = $"用户信息已过期，请重新登录" };
                }
                var rlt = await _courseUserItemService.GetUserCourseWorkList(long.Parse(UserId), courseId, courseChildId,
                    alreadyDone);
                return new CourseItemDoneResult() { Data = rlt.Data };
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return new CourseItemDoneResult() { Code = (int)ResultCode.Exception, Message = $"获取我的课程作业失败 {ex.Message}" };
            }
        }

        ///// <summary>
        ///// 【课程播放列表题库】 最后统计结果计算 ，在用户答题最后一道错题的时候提交当前错题的，
        ///// 【所有错题的ID、选项、答题时间】 TODO:待优化
        ///// </summary>
        ///// <param name="addViewModel">【ID都是int类型的】使用都竖线斜线逗号分隔，例如：{1|A/B/C|5,1|12/32/15|3}（题ID/用户选择答案选项|答题时间秒,题ID/用户选择答案选项|答题时间秒,）</param> 
        ///// <returns></returns>
        /// <summary>
        /// 我的课程提交
        /// </summary>
        /// <param name="courseChildId">子课程Id</param>
        /// <param name="addViewModelList">添加</param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<CourseUserItemResultViewModel> SaveCourseUserItem(int courseChildId, List<CourseUserItemAddViewModel> addViewModelList)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    return new CourseUserItemResultViewModel() { Code = (int)ResultCode.Fail, Message = $"用户信息已过期，请重新登录" };
                }
                var model = addViewModelList.MapTo<List<CourseUserItemAddModel>>();
                var rlt = await _courseUserItemService.SaveCourseUserItem(int.Parse(UserId), courseChildId, model);
                return new CourseUserItemResultViewModel() { Data = rlt.Data, Code = rlt.Code, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("", ex);
                return new CourseUserItemResultViewModel()
                {
                    Code = (int)ResultCode.Exception,
                    Message = "提交课程作业失败！"
                };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="addViewModel"></param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<CourseUserItemResultViewModel> SaveCourseUserItemIOS(CourseUserItemViewModel addViewModel)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    return new CourseUserItemResultViewModel() { Code = (int)ResultCode.Fail, Message = $"用户信息已过期，请重新登录" };
                }
                var model = addViewModel.MapTo<CourseUserItemViewModel>();
                var listModel = model.AddViewModelList.MapTo<List<CourseUserItemAddModel>>();
                var rlt = await _courseUserItemService.SaveCourseUserItem(int.Parse(UserId), model.CourseChildId, listModel);
                return new CourseUserItemResultViewModel() { Data = rlt.Data, Code = rlt.Code, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("", ex);
                return new CourseUserItemResultViewModel()
                {
                    Code = (int)ResultCode.Exception,
                    Message = "提交课程作业失败！"
                };
            }
        }


        /// <summary>
        /// 设置当前录播的子课程播放当前进度
        /// </summary>
        /// <param name="Info"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result<UserCourseChildInfo>> SetCourseChildWatchTime([FromBody] UserCourseChildInfo Info)
        {
            //获取用户 
            Result<UserCourseChildInfo> result = new Result<UserCourseChildInfo>();
            try
            {
                if (string.IsNullOrWhiteSpace(UserId))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "修改用户ID不能为空";
                    return result;
                }
                var userInfo = await _userService.GetUser(UserIdValue);
                var userchild = await _userCourseChildProgessService.Get(UserIdValue, Info.CourseId, Info.CourseChildId);
                var childinfo = userchild.Data;
                if (childinfo == null)
                {
                    if (Info.LiveNum == 0)
                    {
                        Info.LiveNum = 1;
                    }
                    if (Info.CurrentTime > 0 && Info.DurationTime > 0)
                    {
                        int m = Info.CurrentTime;
                        int n = Info.DurationTime;
                        float x;
                        x = (float)m / n;
                        Info.ChildProgress = x;
                    }
                    else
                    {
                        Info.ChildProgress = 0;
                    }
                    if (Info.ChildProgress >= 0.98)
                    {
                        Info.IsFinish = true;
                    }
                    else
                    {
                        Info.IsFinish = false;
                    }
                    long pId = _userCourseChildProgessService.Add(Info);
                    result.Message = "添加记录成功";
                    Info.Id = pId;
                    result.Data = Info;
                }
                else
                {
                    Info.Id = childinfo.Id;
                    Info.LiveNum = childinfo.LiveNum + 1;
                    if (childinfo.IsFinish == false)
                    {
                        if (Info.CurrentTime > 0 && childinfo.DurationTime > 0)
                        {
                            int m = Info.CurrentTime;
                            int n = childinfo.DurationTime;
                            float x;
                            x = (float)m / n;
                            Info.ChildProgress = x;
                        }
                        else
                        {
                            Info.ChildProgress = 0;
                        }
                        if (Info.ChildProgress >= 0.98)
                        {
                            Info.IsFinish = true;
                        }
                        else
                        {
                            Info.IsFinish = false;
                        }
                    }
                    else
                    {
                        Info.ChildProgress = childinfo.ChildProgress;
                        Info.IsFinish = childinfo.IsFinish;
                    }
                    _userCourseChildProgessService.Update(Info);
                    result.Message = "修改记录成功";
                    result.Data = Info;
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = ex.Message;
                Logger.Error(ex);
            }
            return result;
        }

        private string GetAid()
        {
            var aid = _aid;
            try
            {
                if (!Request.Headers.ContainsKey(_aidKey))
                {
                    Request.Headers.Add(_aidKey, "college");
                    aid = "college";
                }
                else
                {
                    aid = Request.Headers[_aidKey].ToString();
                }

            }
            catch
            {
                aid = _aid;
            }
            return aid;
        }
        #endregion

        #region 清理缓存数据与状态

        /// <summary>
        /// 根据当前时间设置课的子章节的Child的直播状态 子课程类型1直播中2录播（回放）3点播 4 未开始
        /// </summary>
        /// <param name="courseChildId">子课程ID</param>
        /// <param name="ChildType">子课程类型1直播中2录播（回放）3点播 4 未开始</param>
        /// <param name="videoId">videoId</param>
        /// <returns></returns>
        [HttpPost("/api/SetCourseChild")]
        [AllowAnonymous]
        public async Task<bool> SetCourseChildTypeAndvideoId(long courseChildId, int ChildType, string videoId)
        {

            var isok = _courseChildService.UpdateChildType(courseChildId, ChildType, videoId);
            return await Task.FromResult(isok);
        }

        /// <summary>
        /// 获取即将开始的子课程
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/liveChildlist")]
        [AllowAnonymous]
        public async Task<Result<List<CourseisliveChildInfo>>> GetliveChildlist()
        {
            var rlt = await _courseChildService.GetliveChildlist();
            List<CourseisliveChildInfo> childlist = new List<CourseisliveChildInfo>();
            childlist = rlt.Data;
            if (childlist.Count > 0)
            {
                childlist.ForEach(async n =>
                {
                    var lrt = await _courseService.GetAsync(n.ParentId);
                    var info = lrt.Data;
                    {
                        n.CourseName = info.CourseName;
                        n.IsLive = info.IsLive;
                        n.img = info.CoursePicPhoneMini;
                    }

                });
            }
            return new Result<List<CourseisliveChildInfo>>() { Code = (int)ResultCode.Success, Message = "获取即将直播列表成功", Data = childlist };
        }
        /// <summary>
        /// 新增当日即将开启的指定直播课用户列表
        /// </summary>
        /// <param name="CourseId"></param>
        /// <returns></returns>
        [HttpGet("/api/LiveUserList")]
        [AllowAnonymous]
        public Result<UserLiveResult> GetLiveUserList(int CourseId)
        {
            List<UserliveModel> liveUserList = new List<UserliveModel>();
            var UserList = _courseService.GetLiveUserList(CourseId);
            liveUserList = UserList.Where(p => p.AccountType == 1).ToList();
            List<string> phonelist = new List<string>();
            foreach (var item in liveUserList)
            {
                if (!string.IsNullOrEmpty(item.phone))
                {
                    phonelist.Add(item.phone);
                }
            }
            var result = new UserLiveResult()
            {
                phones = phonelist,
                UserliveList = liveUserList
            };
            return new Result<UserLiveResult>() { Code = (int)ResultCode.Success, Message = "获取购买的学员手机列表信息成功", Data = result };
        }

        /// <summary>
        /// 批量检测当前之间最近的直播课开启直播状态
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/TaskSetChildType")]
        [AllowAnonymous]
        public async Task<bool> TaskSetCourseChildType()
        {
            bool isok = false;
            var rlt = await _courseChildService.TaskSetChildType();
            List<int> courselist = new List<int>();
            courselist = rlt.Data;
            int success = 0;
            if (courselist.Count > 0)
            {
                #region 处理主课程开播状态
                foreach (int courseid in courselist)
                {
                    var lrt = await _courseService.GetAsync(courseid);
                    var info = lrt.Data;
                    if (info != null)
                    {
                        var infoEntity = info.MapTo<CourseEntity>();
                        infoEntity.IsLive = 1;
                        var result = _courseService.Update(infoEntity);
                        if (result)
                        {
                            success++;
                        }
                    }

                }
                #endregion
                if (success > 0)
                {
                    isok = true;
                }
            }
            return await Task.FromResult(isok);
        }

        /// <summary>
        /// 打指定的课后作业清除缓存并加载
        /// </summary>
        /// <param name="courseChildId">子课程ID</param>
        /// <param name="IsDelete">0 正常 1删除</param>
        /// <returns></returns>
        [HttpPost("/api/SetCourseItemIsDelete")]
        [AllowAnonymous]
        public Result SetCourseItemIsDelete(long courseChildId, int IsDelete)
        {
            var result = new Result();
            try
            {
                var isok = _courseItemService.BatchUpdatetExcel(courseChildId, IsDelete);
                if (isok)
                {
                    _courseChildService.CourseChildClearCache();
                }
                return new Result() { Code = (int)ResultCode.Success, Message = "修改成功" };

            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"{ex.Message}";
                return result;
            }
        }
        /// <summary>
        /// 清空子课程并加载
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/SetCourseChildClearCache")]
        [AllowAnonymous]
        public async Task<bool> SetCourseChildClearCache()
        {

            var isok = _courseChildService.CourseChildClearCache();

            return await Task.FromResult(isok);
        }
        /// <summary>
        /// 清空课课程并加载
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/SetCourseClearCache")]
        [AllowAnonymous]
        public async Task<bool> SetCourseClearCache()
        {

            var isok = _courseService.CourseClearCache();

            return await Task.FromResult(isok);
        }
        /// <summary>
        /// 清空课后作业题并加载
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/SetCourseItemClearCache")]
        [AllowAnonymous]
        public async Task<bool> SetCourseItemClearCache()
        {

            var isok = _courseItemService.CourseItemClearCache();

            return await Task.FromResult(isok);
        }
        /// <summary>
        /// 清空学生课后作业并加载
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/SetCourseUserItemClearCache")]
        [AllowAnonymous]
        public async Task<bool> SetCourseUserItemClearCache()
        {

            var isok = _courseUserItemService.CourseUserItemClearCache();

            return await Task.FromResult(isok);
        }
        #endregion

        #region  私有方法 等待支付超时
        private string convertOrderState(int orderState, DateTime orderCreateTime)
        {
            string res = "等待支付";
            TimeSpan ts = DateTime.Now - orderCreateTime;
            int spend_time = Convert.ToInt32(ts.TotalMinutes);

            res = Enum.GetName(typeof(OrderStateType), orderState);


            if (orderState == 0)
            {
                if (spend_time >= DomainSetting.UnpayOrderExprie)
                {

                    res = "已取消";

                }

            }

            return res;
        }
        #endregion

        #region  舍弃方法
        /// <summary>
        /// 优惠券状态变化
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="UserCouponId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result> userCouponHasUsed([FromForm] long userId, [FromForm] long UserCouponId)
        {
            var lrt = _couponInfoService.userCouponHasUsed(userId, UserCouponId);
            return new Result() { Code = (int)ResultCode.Success, Data = lrt.Result.Data, Message = "修改成功" };

        }
        #region 课程金币 经验变化
        /// <summary>
        /// 金币值变化
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        private async Task<Result<ValueChange>> AddCoin([FromForm] string userId, [FromForm] int value)
        {
            return new Result<ValueChange> { Data = new ValueChange { ChangedValue = value, CurrentValue = 1000 } };
        }

        /// <summary>
        /// 课程经验值变化
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        private Result<ValueChange> AddExp([FromForm] string userId, [FromForm] int value)
        {
            return new Result<ValueChange> { Data = new ValueChange { ChangedValue = value, CurrentValue = 9999 } };
        }
        #endregion
        #endregion

        #region 生成html 
        /// <summary>
        /// 生成课程html 
        /// </summary>
        /// <param name="courseId">0 全部 ，课程Id</param>
        /// <returns></returns>
        [HttpPost("/api/Course/html/{courseId}")]
        [AllowAnonymous]
        public async Task<Result<Dictionary<long, string>>> GenerateHtml(long courseId)
        {
            try
            {
                var htmlPath = await _courseService.GenerateHtml(courseId);
                return await Task.Run(() => new Result<Dictionary<long, string>> { Data = htmlPath });
            }
            catch (Exception ex)
            {
                Logger.Error($"生成课程{courseId},html失败!", ex);
                return new Result<Dictionary<long, string>> { Code = (int)ResultCode.Exception, Message = "生成html失败" };
            }
        }
        #endregion

        #region 调用保利威视频长度的赋值修改
        [HttpPost("/api/CourseChild/{courseId}")]
        [AllowAnonymous]
        public async Task<Result<Dictionary<string, string>>> GenerateChildTimel(int courseId)
        {
            try
            {

                var htmlPath = await _courseService.GenerateChildTimel(courseId);
                return await Task.Run(() => new Result<Dictionary<string, string>> { Data = htmlPath });

            }
            catch (Exception ex)
            {

                Logger.Error($"获取课程{courseId},视频时长!", ex);
                return new Result<Dictionary<string, string>> { Code = (int)ResultCode.Exception, Message = "获取保利威时长失败" };
            }
        }
        #endregion

        #region 预约听课
        /// <summary>
        /// 预约听课
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phone"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("/api/course/appo")]
        public async Task<Result<bool>> Appointment([FromForm] string name, [FromForm] string phone)
        {

            try
            {
                var result = _courseService.CourseAppointmentAdd(name, phone);
                return await Task.FromResult(new Result<bool> { Data = true });
            }
            catch (Exception ex)
            {
                Logger.Error("Appointment() error");
                return await Task.FromResult(new Result<bool> { Message = ex.ToString(), Data = false });
            }

        }

        /// <summary>
        ///预约课查询
        /// </summary>
        /// <param name="datetime">格式：2020-02-17  or  2020/02/17</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<Result<List<CourseAppointmentModel>>> GetAllOfAppointment([FromForm] string datetime)
        {
            try
            {
                var result = _courseService.SearchAllorTime(datetime);
                return await Task.FromResult(new Result<List<CourseAppointmentModel>> { Data = result.ToList() });
            }
            catch (Exception ex)
            {
                Logger.Error("Appointment() error");
                return await Task.FromResult(new Result<List<CourseAppointmentModel>> { Message = ex.ToString() });
            }
        }
        #endregion

        #region 线下支付
        /// <summary>
        /// 线下支付课程开通
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result> OfflinePay([FromForm] CourseOfflinePay courseOfflinePay)
        {
            try
            {
                var courseId = courseOfflinePay.CourseId;
                var useInfo = await _userService.GetUserByPhone(courseOfflinePay.Phone);
                if (useInfo == null)
                    return new Result() { Code = (int)ResultCode.Fail, Message = "此账户未注册马小哈" };
                courseOfflinePay.UserId = useInfo.Userid;
                var useAddress = await _addressService.Get(useInfo.Userid);
                var addInfo = useAddress.Data;
                int ok = addInfo != null && !string.IsNullOrWhiteSpace(addInfo.Phone) ? 1 : 0;
                var lrt = await _courseService.GetAsync(courseId);
                var courseEntity = lrt.Data;
                if (courseEntity == null)
                {
                    return new Result { Code = (int)ResultCode.Fail, Message = $"课程不存在{courseId}" };
                }
                courseEntity.ExpressPirce = courseEntity.IsExpress == 0 ? 0M : courseEntity.ExpressPirce;
                string spBillno = string.Format("{0}{1}", EncryptionUtility.OrderCode(), TenPayV3Util.BuildRandomStr(6));

                var payInfo = courseEntity.MapTo<PayOrderInfo>();
                payInfo.CourseId = courseEntity.Id;
                payInfo.PreferentialBeginTime = courseEntity.PreferentialBeginTime.ToString("yyyy.MM.dd");
                payInfo.PreferentialEndTime = courseEntity.PreferentialEndTime.ToString("yyyy.MM.dd");
                payInfo.ValidBeginTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.ValidEndTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
                payInfo.OrderNo = spBillno;
                var courseOrder = new CourseOrderEntity
                {
                    OrderNo = spBillno,
                    UserId = courseOfflinePay.UserId,
                    PayPrice = courseOfflinePay.Price,
                    OrderState = 1,
                    IsPay = 1,
                    PayTime = courseOfflinePay.PaymentTime,
                    PayType = 1,
                    PayNo = spBillno,
                    CourseId = courseEntity.Id,
                    IsSend = 0,
                    SendTime = DateTime.Now,
                    IsGet = 0,
                    GetTime = DateTime.Now,
                    PackageNo = "",
                    PackageType = 0,
                    OrderDes = "线下购买课程",
                    AddressId = 0,
                    IsDelete = 0,
                    CreateId = 0,
                    CreateTime = courseOfflinePay.PaymentTime,
                    UpdateTime = DateTime.Now,
                    TenantId = 0,
                    IsInvoice = 0,
                    InvoiceDes = ""
                };
                #region  订单主体
                OrderInfoModel OrderInfo = new OrderInfoModel();
                OrderInfo.ProductId = courseEntity.Iosproductid;
                OrderInfo.Payment = courseOfflinePay.Price.ToString("F2");
                OrderInfo.PaymentType = courseOfflinePay.PaymentType;
                OrderInfo.PostFee = "0";
                OrderInfo.ShippingName = "";
                OrderInfo.ShippingCode = "";
                OrderInfo.IsFrom = 0;
                OrderInfo.BusinessType = (int)BusinessType.Course;
                var orderInfoEntity = OrderInfo.MapTo<OrderInfoEntity>();
                orderInfoEntity.Id = spBillno;
                orderInfoEntity.UserId = courseOfflinePay.UserId;
                orderInfoEntity.PaymentTime = courseOfflinePay.PaymentTime;
                orderInfoEntity.Status = (int)OrderStatus.Success;
                orderInfoEntity.PaymentNo = courseOfflinePay.TransactionNo;
                //orderInfoEntity.EndTime = courseOfflinePay.PaymentTime;
                orderInfoEntity.CreateTime = courseOfflinePay.PaymentTime;
                orderInfoEntity.UpdateTime = DateTime.Now;
                #endregion
                #region  订单明细
                OrderItemModel OrderItem = new OrderItemModel();
                OrderItem.ItemId = courseEntity.Id.ToString();
                OrderItem.Num = 1;
                OrderItem.Title = courseEntity.CourseName;
                OrderItem.Description = courseEntity.Detailed;
                OrderItem.Price = float.Parse(courseOfflinePay.Price.ToString("F2"));
                OrderItem.TotalFee = float.Parse(courseOfflinePay.Price.ToString("F2")); ;
                OrderItem.PicPath = courseEntity.CoursePicPhone;

                var orderItemEntity = OrderItem.MapTo<OrderItemEntity>();
                #endregion
                #region  收货地址明细
                AddressInfo AddressInfo = new AddressInfo();
                AddressInfo.Userid = (int)courseOfflinePay.UserId;
                if (addInfo != null)
                {
                    AddressInfo.Sex = addInfo.Sex;
                    AddressInfo.Phone = addInfo.Phone;
                    AddressInfo.Consignee = addInfo.Consignee;
                    AddressInfo.Provincial = addInfo.Provincial;
                    AddressInfo.City = addInfo.City;
                    AddressInfo.Areas = addInfo.Areas;
                    AddressInfo.Address = addInfo.Address;
                }
                var orderShippingEntity = AddressInfo.MapTo<OrderShippingEntity>();
                #endregion

                #region  使用优惠券明细
                OrderCouponInfoModel OrderCouponInfo = new OrderCouponInfoModel();
                //OrderCouponInfo.ItemId= courseEntity.Id.ToString();
                var orderCouponEntities = OrderCouponInfo.MapTo<List<OrderCouponEntity>>();
                #endregion

                var orderPay = orderInfoEntity.MapTo<OrderPayEntity>();
                orderPay.UpdateTime = orderInfoEntity.UpdateTime;
                orderPay.CreateTime = orderInfoEntity.CreateTime;
                orderPay.Transaction = "线下支付";
                orderPay.IsTest = false;
                orderPay.Aid = "college";
                _orderInfoService.Add(orderPay);
                // _payRepository.InsertAndGetId(orderPay);
                var rlt = await _orderInfoService.CreateOrder(orderInfoEntity, orderItemEntity, orderShippingEntity,
                orderCouponEntities, true);

                //生成购买课程记录 回调修改状态
                var courseBuyCollectEntity = _courseBuyCollectService.Table()
                    .FirstOrDefault(n => n.CourseId == courseEntity.Id && n.UserId == useInfo.Userid);
                if (courseBuyCollectEntity == null)
                {
                    _courseBuyCollectService.Add(new CourseBuyCollectEntity
                    {
                        UserId = (int)useInfo.Userid,
                        CourseId = (int)courseEntity.Id,
                        CourseOrderId = spBillno,
                        CreateId = 0,
                        CreateTime = DateTime.Now,
                        IsBuy = 1,
                        ExpirationDate = DateTime.Now.AddYears(1),
                        IsHide = 0,
                        IsCollect = 0,
                    });
                }
                else
                {
                    courseBuyCollectEntity.CourseOrderId = spBillno;
                    courseBuyCollectEntity.IsBuy = 1;
                    courseBuyCollectEntity.ExpirationDate = DateTime.Now.AddYears(1);
                    courseBuyCollectEntity.IsHide = 0;
                    _courseBuyCollectService.Update(courseBuyCollectEntity);
                }


                return new Result { Message = "线下支付课程开通成功" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("线下支付课程开通异常");
                return new Result { Code = (int)ResultCode.Exception, Message = "线下支付课程开通异常" };
            }
        }
        #endregion

    }
}
