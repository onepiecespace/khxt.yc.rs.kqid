﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Home;
using KhXt.YcRs.Domain.Services.Sys;
using Hx.Components;
using Hx.Extensions;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.Models.Course;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Course
{
    public partial class CourseController
    {
        #region APP首页推荐
        /// <summary>
        /// 【APP首页推荐】 （LiveBeginTime直播开始）(VirtualSales 虚拟销量人在学)（ValidEndTime有效结束时间）
        /// </summary>
        /// <param name="classType">年级对应的ID /api/User/GetGradeList 0全部 </param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="livePageSize"> LiveList 的默认显示条数3 默认从第一页查询条数。</param>
        /// <param name="userId">userId。</param>
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        private async Task<CouerseInfoListResult> GetHomeCouerseList(int classType = 1, int pageIndex = 1, int pageSize = 10, int livePageSize = 3, long userId = 0)
        {
            try
            {

                var rlt = await _courseService.GetIndexListInfo(userId, GetAid(), classType, pageIndex, pageSize, livePageSize);
                return new CouerseInfoListResult() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new CouerseInfoListResult() { Code = (int)ResultCode.Exception, Message = $"获取失败{ex.Message}" };
            }
        }
        #endregion

        #region 课程首页和分类

        /// <summary>
        /// 【首页】获取首页课程分类直播列表信息
        /// </summary>
        /// <param name="classType"> 年级对应的ID /api/User/GetGradeList 0全部</param>
        /// <param name="pageIndex">第一页</param>
        /// <param name="pageSize">默认10条数据</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        private async Task<CouerseIndexListResult> GetIndexList(int classType = 1, int pageIndex = 1, int pageSize = 10, long userId = 0)
        {


            try
            {
                var rlt = await _courseService.GetIndexList(userId, GetAid(), classType, pageIndex, pageSize);
                return new CouerseIndexListResult() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new CouerseIndexListResult() { Code = (int)ResultCode.Exception, Message = $"获取失败{ex.Message}" };
            }

        }

        /// <summary>
        /// 【首页   课程列表】获取具体的课程信息 （LiveBeginTime直播开始）(VirtualSales虚拟销量)（ValidEndTime有效结束时间）
        /// </summary>
        /// <param name="cateoryId">分类id  对应的NavCateoryList 的id 默认0全部分类</param>
        /// <param name="selectType"> 读取二级联动的分类Id 默认0全部二级分类 </param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param> 
        /// <param name="userId">userId</param>
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        public async Task<CouerseInfoListResult> GetCouerseList(int cateoryId = 0, int selectType = 0, int pageIndex = 1, int pageSize = 10, long userId = 0)
        {
            try
            {

                var rlt = await _courseService.GetCouerseList(userId, GetAid(), cateoryId, selectType, pageIndex, pageSize);
                return new CouerseInfoListResult() { Code = rlt.Code, Data = rlt.Data, Message = rlt.Message };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error(ex);
                return new CouerseInfoListResult() { Code = (int)ResultCode.Exception, Message = $"获取失败{ex.Message}" };
            }

        }

        /// <summary>
        /// 【课程列表】 获取课程分类信息
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        //[AllowAnonymous]
        public CateoryListResult GetCateoryOneList()
        {

            CateoryListResult result = new CateoryListResult();
            try
            {
                List<CateoryInfo> OneCatList = new List<CateoryInfo>();
                var rlt = _userService.Get(UserIdValue);
                var userinfo = rlt.Result;
                if (userinfo != null)
                {
                    var topList = _courseCateoryService.Table().Where(n => n.IsShow == 1 && n.ParentId == 0 && n.IsHomeTop == 1).OrderBy(n => n.Sort).ToList();
                    var list = topList.MapTo<List<CateoryInfo>>();
                    foreach (CateoryInfo item in list)
                    {
                        var OneModel = item.MapTo<CateoryInfo>();

                        if (userinfo.AuthCateorys.Contains(item.Id.ToString()) == false)
                        {
                            OneModel.IsNav = 0;//不可以点击提示
                            OneModel.Des = "当前您没有权限查看此菜单";
                        }
                        OneCatList.Add(OneModel);
                    }
                    result.Data = OneCatList;
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }
        /// <summary>
        /// 【课程列表】 获取课程分类二级联动信息
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        //[AllowAnonymous]
        public async Task<CateoryListResult> GetCateoryList()
        {

            CateoryListResult result = new CateoryListResult();
            try
            {
                List<CateoryInfo> OneCatList = new List<CateoryInfo>();
                var userinfo = await _userService.GetUser(UserIdValue);
                if (userinfo != null)
                {
                    var AllList = _courseCateoryService.Table().Where(n => n.IsShow == 1).ToList();
                    var OneList = AllList.Where(n => n.ParentId == 0).OrderBy(n => n.Sort).ToList();

                    foreach (CourseCateoryModel item in OneList)
                    {
                        var OneModel = item.MapTo<CateoryInfo>();
                        if (userinfo.AuthCateorys.Contains(item.Id.ToString()) == false)
                        {
                            OneModel.IsNav = 0;//不可以点击提示
                            OneModel.Des = "当前您没有权限查看此菜单";
                        }

                        List<CateoryInfo> TWOCatList = new List<CateoryInfo>();
                        if (OneModel.IsNav == 1)
                        {
                            var TwoList = AllList.Where(n => n.ParentId == item.Id).OrderBy(n => n.Sort).ToList();
                            TWOCatList = TwoList.MapTo<List<CateoryInfo>>();
                            //根据级别配置过滤分类
                            if (userinfo.Level <= 3)//非会员 内部身份
                            {
                                var settingService = ObjectContainer.Resolve<ISettingService>();
                                var levelsetting = await settingService.GetLevelCourseSetting();
                                List<CateoryInfo> newClist = new List<CateoryInfo>();
                                newClist = LevelCateCourse(TWOCatList, levelsetting, userinfo.Level);
                                TWOCatList = newClist;
                            }
                        }
                        OneModel.Twolist = TWOCatList;
                        OneCatList.Add(OneModel);
                    }
                }

                result.Data = OneCatList;
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }

        #endregion
        #region 私有方法
        private List<CateoryInfo> LevelCateCourse(List<CateoryInfo> list, List<LevelCourseSettingModel> levelsetting, int level)
        {

            #region  对账号指定level级别配置课程
            List<CateoryInfo> newClist = new List<CateoryInfo>();
            int[] cate = null;
            switch (level)
            {
                case 1:
                    {
                        cate = levelsetting.FirstOrDefault(s => s.Id == 1).Cat;
                        break;
                    }
                case 2:
                    {
                        cate = levelsetting.FirstOrDefault(s => s.Id == 2).Cat;
                        break;
                    }
                case 3:
                    {
                        cate = levelsetting.FirstOrDefault(s => s.Id == 3).Cat;
                        break;
                    }
                case 4:
                    {
                        cate = levelsetting.FirstOrDefault(s => s.Id == 4).Cat;
                        break;
                    }
                default:
                    {

                        break;
                    }
            }
            if (cate.Length > 0)
            {

                newClist = list.Where(a => a.Id.IsIn(cate)).ToList();
                //if (newClist.Count > 0)
                //{
                //    list = new List<CateoryInfo>();
                //    list.AddRange(newClist);
                //}
            }
            return newClist;
            #endregion
        }
        #endregion
    }
}
