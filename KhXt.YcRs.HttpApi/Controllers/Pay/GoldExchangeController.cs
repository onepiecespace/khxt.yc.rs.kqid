﻿using Essensoft.AspNetCore.Payment.WeChatPay;
using Essensoft.AspNetCore.Payment.WeChatPay.Request;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Services.Order;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Pay;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Pay
{
    [Route("api/Pay/[controller]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Pay)]
    public class GoldExchangeController : ControllerBase
    {

        private readonly IOrderInfoService _orderInfoService;

        public GoldExchangeController(IOrderInfoService orderInfoService)
        {
            _orderInfoService = orderInfoService;
        }

        /// <summary>
        /// 运费0元 也就是payment=0 运费不等于0  总金额 payment=freight    直接调用兑换接口
        /// </summary>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        [Route("/api/GoldExchange")]
        [HttpPost]
        public async Task<Result<ValueChange>> GoldExchange([FromForm] string orderId)
        {
            try
            {

                var rlt = await _orderInfoService.GoldExchange(PaymentType.GoldExchange, orderId);
                return rlt.Code == (int)ResultCode.Success
                ? new Result<ValueChange>() { Code = rlt.Code, Message = $"{rlt.Message}", Data = rlt.Data }
                : new Result<ValueChange>() { Code = rlt.Code, Message = $"创建金币兑换失败：{rlt.Message}" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new Result<ValueChange>() { Code = (int)ResultCode.Exception };
            }

        }

    }
}