﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Pay;
using KhXt.YcRs.Domain.Services.Wallet;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Pay;
using KhXt.YcRs.HttpApi.Models.Wallet;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Pay
{
    [Route("api")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Pay)]
    public class WalletController : BaseApiController
    {
        private readonly IWalletService _walletService;
        public WalletController(IWalletService walletService)
        {
            _walletService = walletService;
        }
        /// <summary>
        /// 金币支付
        /// </summary>
        /// <param name="orderId">订单Id</param>
        /// <param name="businessType"></param>
        /// <param name="isRenew"></param>
        /// <param name="code">验证</param>
        /// <returns></returns>
        [HttpPost("pay/wallet")]
        public async Task<Result<WalletPayResult>> WalletPay([FromForm] string orderId, [FromForm] int businessType, [FromForm] bool isRenew, [FromForm] string code)
        {
            try
            {
                var rlt = await _walletService.PayAsync(UserIdValue, orderId, businessType, code);
                return rlt;
            }
            catch (Exception ex)
            {
                Logger.Error($"用户：{UserIdValue},钱包支付异常,订单{orderId}", ex);
                return new Result<WalletPayResult> { Code = (int)ResultCode.Exception, Message = "钱包支付异常" };
            }
        }
    }
}