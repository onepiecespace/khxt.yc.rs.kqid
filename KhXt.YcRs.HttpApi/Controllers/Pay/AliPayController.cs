﻿using Essensoft.AspNetCore.Payment.Alipay;
using Essensoft.AspNetCore.Payment.Alipay.Domain;
using Essensoft.AspNetCore.Payment.Alipay.Request;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Pay;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Pay
{
    [Route("api/Pay/AliPay")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Pay)]
    public class AliPayController : BaseApiController
    {
        private readonly IAlipayClient _client;
        private readonly IOptions<AlipayOptions> _optionsAccessor;
        private readonly IOrderInfoService _orderInfoService;

        public AliPayController(IAlipayClient client,
            IOptions<AlipayOptions> optionsAccessor, IOrderInfoService orderInfoService)
        {
            _client = client;
            _optionsAccessor = optionsAccessor;
            _orderInfoService = orderInfoService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId">订单号</param>
        /// <param name="businessType">支付类别</param>
        /// <param name="isRenew">是否自动续期</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result> AppPay([FromForm] string orderId, [FromForm] int businessType,
            [FromForm] bool isRenew)
        {
            try
            {
                var rlt = await _orderInfoService.AliPayOrder(orderId, businessType, isRenew);
                if (rlt.Code != (int)ResultCode.Success)
                    return new Result { Code = rlt.Code, Message = rlt.Message };
                var viewModel = rlt.Data;
                var model = new AlipayTradeAppPayModel
                {
                    OutTradeNo = viewModel.OutTradeNo,
                    Subject = viewModel.Subject,
                    ProductCode = viewModel.ProductCode,
                    TotalAmount = viewModel.TotalAmount,
                    Body = viewModel.Body
                };
                var req = new AlipayTradeAppPayRequest();
                req.SetBizModel(model);
                req.SetNotifyUrl(_optionsAccessor.Value.NotifyUrl);
                var response = await _client.SdkExecuteAsync(req, _optionsAccessor.Value);
                //将response.Body给 ios/android端 由其去调起支付宝APP(https://docs.open.alipay.com/204/105296/ https://docs.open.alipay.com/204/105295/)
                if (response == null)
                {
                    Logger.Error($"支付宝支付失败，参数或者数据有误！订单号:{orderId}");
                    return new Result { Code = (int)ResultCode.Fail, Message = "支付宝支付失败，参数或者数据有误！" };
                }
                else
                {
                    Logger.Info($"支付宝支付成功，订单号：{orderId}");
                    return new Result { Data = response.ResponseBody };
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine($"支付宝支付异常，订单号:{orderId}", ex.Message);
                Logger.Error($"支付宝支付异常，订单号:{orderId}", ex);
                return new Result { Code = (int)ResultCode.Exception, Message = "支付宝支付异常" }; ;
            }

        }
    }
}
