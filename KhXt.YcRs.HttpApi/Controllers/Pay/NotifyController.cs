﻿using Essensoft.AspNetCore.Payment.Alipay;
using Essensoft.AspNetCore.Payment.Alipay.Notify;
using Essensoft.AspNetCore.Payment.UnionPay;
using Essensoft.AspNetCore.Payment.UnionPay.Notify;
using Essensoft.AspNetCore.Payment.WeChatPay;
using Essensoft.AspNetCore.Payment.WeChatPay.Notify;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Services.Order;
using Hx.Components;
using Hx.Logging;
using KhXt.YcRs.HttpApi.ApiGroup;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Pay
{
    #region 支付宝异步通知

    [Route("notify/alipay")]
    public class AlipayNotifyController : Controller
    {
        #region 日志

        private ILogger _logger;

        /// <summary>
        /// 
        /// </summary>
        protected ILogger Logger
        {
            get
            {
                if (_logger != null) return _logger;
                var factory = ObjectContainer.Resolve<ILoggerFactory>();
                _logger = factory.Create();
                return _logger;
            }
        }

        #endregion
        private readonly IAlipayNotifyClient _client;
        private readonly IOptions<AlipayOptions> _optionsAccessor;
        private readonly IOrderInfoService _orderInfoService;
        public AlipayNotifyController(IAlipayNotifyClient client, IOptions<AlipayOptions> optionsAccessor, IOrderInfoService orderInfoService)
        {
            _client = client;
            _optionsAccessor = optionsAccessor;
            _orderInfoService = orderInfoService;
        }

        /// <summary>
        /// 扫码支付异步通知
        /// </summary>
        /// <returns></returns>
        [Route("precreate")]
        [HttpPost]
        public async Task<IActionResult> Precreate()
        {
            try
            {
                var notify = await _client.ExecuteAsync<AlipayTradePrecreateNotify>(Request, _optionsAccessor.Value);
                if ("TRADE_SUCCESS" == notify.TradeStatus)
                {
                    Console.WriteLine("OutTradeNo: " + notify.OutTradeNo);

                    return AlipayNotifyResult.Success;
                }
                return NoContent();
            }
            catch
            {
                return NoContent();
            }
        }

        /// <summary>
        /// APP支付异步通知
        /// </summary>
        /// <returns></returns>
        [Route("apppay")]
        [HttpPost]
        public async Task<IActionResult> AppPay()
        {
            try
            {
                Logger.Info($"支付宝支付结果通知开始");
                var notify = await _client.ExecuteAsync<AlipayTradeAppPayNotify>(Request, _optionsAccessor.Value);
                Logger.Info($"支付宝支付 商户订单号{notify.OutTradeNo}");
                var payOrderModel = new PayOrderModel
                {
                    OrderId = notify.OutTradeNo,
                    PaymentNo = notify.TradeNo,
                    IsAutoRenew = false,
                    Transaction = notify.TradeNo,
                    IsTest = false,
                    PaymentType = (int)PaymentType.AliPay,
                };
                if ("TRADE_SUCCESS" == notify.TradeStatus)
                {
                    Console.WriteLine("OutTradeNo: " + notify.OutTradeNo);
                    Logger.Info(JsonConvert.SerializeObject(notify));
                    Logger.Info(notify.OutTradeNo);
                    var payResult = await _orderInfoService.PayOrder(payOrderModel);
                    Logger.Info($"支付宝支付回调，订单支付完成，订单号{notify.OutTradeNo}");
                    var isSuccess = payResult.Code.Equals((int)ResultCode.Success);
                    payOrderModel.IsSuccessPay = isSuccess;
                    await _orderInfoService.OrderPay(payOrderModel);
                    Logger.Info($"支付宝支付回调，写入台账表成功，订单号{notify.OutTradeNo}");
                    return WeChatPayNotifyResult.Success;
                }
                payOrderModel.IsSuccessPay = false;
                await _orderInfoService.OrderPay(payOrderModel);
                return WeChatPayNotifyResult.Failure;
            }
            catch
            {
                return NoContent();
            }
        }
        #region 朗读作品审核回调接口
        /// <summary>
        /// 朗读作品审核回调接口
        /// </summary>
        /// <returns></returns>
        [Route("AudioNotify")]
        [HttpPost]
        public async Task<IActionResult> AudioNotify()
        {
            try
            {
                var notify = await _client.ExecuteAsync<AlipayTradeAppPayNotify>(Request, _optionsAccessor.Value);
                if ("TRADE_SUCCESS" == notify.TradeStatus)
                {
                    Console.WriteLine("OutTradeNo: " + notify.OutTradeNo);

                    return AlipayNotifyResult.Success;
                }
                return NoContent();
            }
            catch
            {
                return NoContent();
            }
        }

        #endregion
        /// <summary>
        /// 电脑网站支付异步通知
        /// </summary>
        /// <returns></returns>
        [Route("pagepay")]
        [HttpPost]
        public async Task<IActionResult> PagePay()
        {
            try
            {
                var notify = await _client.ExecuteAsync<AlipayTradePagePayNotify>(Request, _optionsAccessor.Value);
                if ("TRADE_SUCCESS" == notify.TradeStatus)
                {
                    Console.WriteLine("OutTradeNo: " + notify.OutTradeNo);

                    return AlipayNotifyResult.Success;
                }
                return NoContent();
            }
            catch
            {
                return NoContent();
            }
        }

        /// <summary>
        /// 手机网站支付异步通知
        /// </summary>
        /// <returns></returns>
        [Route("wappay")]
        [HttpPost]
        public async Task<IActionResult> WapPay()
        {
            try
            {
                var notify = await _client.ExecuteAsync<AlipayTradeWapPayNotify>(Request, _optionsAccessor.Value);
                if ("TRADE_SUCCESS" == notify.TradeStatus)
                {
                    Console.WriteLine("OutTradeNo: " + notify.OutTradeNo);

                    return AlipayNotifyResult.Success;
                }
                return NoContent();
            }
            catch
            {
                return NoContent();
            }
        }
    }

    #endregion

    #region 微信支付异步通知

    [Route("notify/wechatpay")]
    public class WeChatPayNotifyController : Controller
    {
        #region 日志

        private ILogger _logger;

        /// <summary>
        /// 
        /// </summary>
        protected ILogger Logger
        {
            get
            {
                if (_logger != null) return _logger;
                var factory = ObjectContainer.Resolve<ILoggerFactory>();
                _logger = factory.Create();
                return _logger;
            }
        }

        #endregion
        private readonly IWeChatPayNotifyClient _client;
        private readonly IOptions<WeChatPayOptions> _optionsAccessor;
        private readonly IOrderInfoService _orderInfoService;

        public WeChatPayNotifyController(IWeChatPayNotifyClient client, IOptions<WeChatPayOptions> optionsAccessor, IOrderInfoService orderInfoService)
        {
            _client = client;
            _optionsAccessor = optionsAccessor;
            _orderInfoService = orderInfoService;
        }

        /// <summary>
        /// 统一下单支付结果通知
        /// </summary>
        /// <returns></returns>
        [Route("unifiedorder")]
        [HttpPost]
        public async Task<IActionResult> Unifiedorder()
        {
            try
            {
                Logger.Info($"微信支付结果通知开始");
                var notify = await _client.ExecuteAsync<WeChatPayUnifiedOrderNotify>(Request, _optionsAccessor.Value);
                Logger.Info($"微信支付 商户订单号{notify.OutTradeNo}");
                var payOrderModel = new PayOrderModel
                {
                    OrderId = notify.OutTradeNo,
                    PaymentNo = notify.TransactionId,
                    IsAutoRenew = false,
                    Transaction = notify.TransactionId,
                    IsTest = false,
                    PaymentType = (int)PaymentType.WeChatPay,
                };
                if (notify.ReturnCode == "SUCCESS")
                {
                    if (notify.ResultCode == "SUCCESS")
                    {
                        Console.WriteLine("OutTradeNo: " + notify.OutTradeNo);
                        Logger.Info(JsonConvert.SerializeObject(notify));
                        Logger.Info(notify.OutTradeNo);
                        var payResult = await _orderInfoService.PayOrder(payOrderModel);
                        Logger.Info($"微信支付回调，订单支付完成，订单号{notify.OutTradeNo}");
                        var isSuccess = payResult.Code.Equals((int)ResultCode.Success);
                        payOrderModel.IsSuccessPay = isSuccess;
                        await _orderInfoService.OrderPay(payOrderModel);
                        Logger.Info($"微信支付回调，写入台账表成功，订单号{notify.OutTradeNo}");
                        return WeChatPayNotifyResult.Success;
                    }
                }
                payOrderModel.IsSuccessPay = false;
                await _orderInfoService.OrderPay(payOrderModel);
                return WeChatPayNotifyResult.Failure;
            }
            catch
            {
                return NoContent();
            }
        }

        /// <summary>
        /// 退款结果通知
        /// </summary>
        /// <returns></returns>
        [Route("refund")]
        [HttpPost]
        public async Task<IActionResult> Refund()
        {
            try
            {
                var notify = await _client.ExecuteAsync<WeChatPayRefundNotify>(Request, _optionsAccessor.Value);
                if (notify.ReturnCode == "SUCCESS")
                {
                    if (notify.RefundStatus == "SUCCESS")
                    {
                        Console.WriteLine("OutTradeNo: " + notify.OutTradeNo);
                        return WeChatPayNotifyResult.Success;
                    }
                }
                return NoContent();
            }
            catch
            {
                return NoContent();
            }
        }
    }

    #endregion

    #region 银联支付异步通知
    [ApiController]
    [ApiGroup(ApiGroupNames.Pay)]
    [Route("notify/unionpay")]
    public class UnionPayNotifyController : Controller
    {
        private readonly IUnionPayNotifyClient _client;
        private readonly IOptions<UnionPayOptions> _optionsAccessor;

        public UnionPayNotifyController(IUnionPayNotifyClient client, IOptions<UnionPayOptions> optionsAccessor)
        {
            _client = client;
            _optionsAccessor = optionsAccessor;
        }

        /// <summary>
        /// 网关支付 - 跳转网关页面支付通知
        /// </summary>
        /// <returns></returns>
        [Route("gatewaypayfrontconsume")]
        [HttpPost]
        public async Task<IActionResult> GatewayPayFrontConsume()
        {
            try
            {
                var notify = await _client.ExecuteAsync<UnionPayGatewayPayFrontConsumeNotify>(Request, _optionsAccessor.Value);
                Console.WriteLine("OrderId: " + notify.OrderId + " respCode :" + notify.RespCode);
                return UnionPayNotifyResult.Success;
            }
            catch
            {
                return NoContent();
            }
        }
    }

    #endregion

    #region 苹果异步通知

    [ApiController]
    [ApiGroup(ApiGroupNames.Pay)]
    [Route("notify/applepay")]
    public class ApplePayNotifyController : BaseApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("renew")]
        public async Task<IActionResult> GetRenewConsume()
        {
            try
            {
                var httpRequest = HttpContext.Request;
                Logger.Info(httpRequest.Query.ToString());
                Logger.Info(httpRequest.Form.Keys);

                return NoContent();
            }
            catch (Exception ex)
            {
                return NoContent();
            }
        }
    }
    #endregion

}
