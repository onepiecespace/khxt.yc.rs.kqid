﻿using Essensoft.AspNetCore.Payment.WeChatPay;
using Essensoft.AspNetCore.Payment.WeChatPay.Request;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Services.Order;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Pay;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Pay
{
    [Route("api/Pay/[controller]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Pay)]
    public class WeChatPayController : ControllerBase
    {

        private readonly IOrderInfoService _orderInfoService;
        private readonly IOptions<WeChatPayOptions> _optionsAccessor;
        public WeChatPayController(IOrderInfoService orderInfoService, IOptions<WeChatPayOptions> optionsAccessor)
        {
            _orderInfoService = orderInfoService;
            _optionsAccessor = optionsAccessor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId">订单号</param>
        /// <param name="businessType">业务类别</param>
        /// <param name="isRenew">是否自动续费 true 是 false 否 默认 false</param>
        /// <param name="tradeType">JSAPI，NATIVE，APP，MWEB </param>
        /// <param name="openId">微信内部拉起使用</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PayWxResult> AppPay([FromForm] string orderId, [FromForm] int businessType, [FromForm] bool isRenew, [FromForm] string tradeType = "APP", [FromForm] string openId = "")
        {
            var rlt = await _orderInfoService.PayOrder(PaymentType.WeChatPay, orderId, businessType, isRenew, tradeType, openId);
            return rlt.Code == (int)ResultCode.Success
                ? new PayWxResult() { Data = rlt.Data.PayWxInfo }
                : new PayWxResult() { Code = rlt.Code, Message = $"创建微信支付失败：{rlt.Message}" };
        }
        [Route("/api/wx/openid/{code}")]
        [HttpGet]
        public async Task<Result<string>> GetOpenId(string code)
        {
            try
            {
                var appId = _optionsAccessor.Value.GHAppId;
                var secret = _optionsAccessor.Value.GHSecret;
                var url = $"https://api.weixin.qq.com/sns/oauth2/access_token?appid={appId}&secret={secret}&code={code}&grant_type=authorization_code";

                var request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(url);
                request.Method = "GET";
                var response = request.GetResponse() as System.Net.HttpWebResponse;
                var ioStream = response.GetResponseStream();
                var sr = new System.IO.StreamReader(ioStream, System.Text.Encoding.UTF8);
                var html = sr.ReadToEnd();
                sr.Close();
                ioStream.Close();
                response.Close();
                string key = "\"openid\":\"";
                int startIndex = html.IndexOf(key);
                if (startIndex != -1)
                {
                    int endIndex = html.IndexOf("\",", startIndex);
                    string openid = html.Substring(startIndex + key.Length, endIndex - startIndex - key.Length);
                    return await Task.Run(() => new Result<string> { Data = openid });
                }
                else
                {
                    return new Result<string> { Code = (int)ResultCode.Fail, Message = $"为获取到openId,Code为{code}" };
                }
            }
            catch
            {
                return new Result<string> { Code = (int)ResultCode.Exception, Message = "获取openId异常" };
            }
        }
    }
}