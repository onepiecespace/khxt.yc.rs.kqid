﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.Pay;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Util;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Pay;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Payment.ApplePay;
using Senparc.Weixin;
using Senparc.Weixin.Entities;
using Senparc.Weixin.TenPay.V3;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using ResponseHandler = Senparc.Weixin.TenPay.V3.ResponseHandler;
using Result = KhXt.YcRs.Domain.Result;
using TenPayV3Type = Senparc.Weixin.TenPay.TenPayV3Type;

namespace KhXt.YcRs.HttpApi.Controllers.Pay
{
    /// <summary>
    /// 支付
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Pay)]

    public class PayController : BaseApiController
    {
        private readonly ICourseOrderService _courseOrderService;
        private readonly ICourseService _courseService;
        private readonly ICourseBuyCollectService _courseBuyCollectService;
        private readonly IOrderInfoService _orderInfoService;
        private readonly IOptions<AppleIapOptions> _options;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="courseOrderService"></param>
        /// <param name="courseService"></param>
        /// <param name="courseBuyCollectService"></param>
        /// <param name="orderInfoService"></param>
        /// <param name="options"></param>
        public PayController(ICourseOrderService courseOrderService,
            ICourseService courseService,
            ICourseBuyCollectService courseBuyCollectService,
            IOrderInfoService orderInfoService,
            IOptions<AppleIapOptions> options)
        {
            _courseOrderService = courseOrderService;
            _courseService = courseService;
            _courseBuyCollectService = courseBuyCollectService;
            _orderInfoService = orderInfoService;
            _options = options;
        }

        private static TenPayV3Info _tenPayV3Info;
        /// <summary>
        /// <para>微信全局配置</para>
        /// <para>注意：在程序运行过程中修改 SenparcWeixinSetting.Items 中的微信配置值，并不能修改 Container 中的对应信息（如AppSecret），</para>
        /// <para>如果需要修改微信信息（如AppSecret）应该使用 xxContainer.Register() 修改，这里的值也会随之更新。</para>
        /// </summary>
        public static SenparcWeixinSetting SenparcWeixinSetting { get; set; }
        public static TenPayV3Info TenPayV3Info
        {
            get
            {
                if (_tenPayV3Info == null)
                {
                    // var key = TenPayV3InfoCollection.Register(Config.SenparcWeixinSetting);
                    var key = "";
                    _tenPayV3Info = TenPayV3InfoCollection.Data[key];
                }
                return _tenPayV3Info;
            }
        }
        #region  参数初始化
        public class WeChetConfig
        {
            private readonly static string appId;
            private readonly static string mchId;
            private readonly static string payKey;
            private readonly static string body;
            private readonly static string ipAddress;
            private readonly static string out_trade_no;
            private readonly static int totalfee;
            private readonly static string notifyurl;

            static WeChetConfig()
            {
                appId = Senparc.Weixin.Config.SenparcWeixinSetting.TenPayV3_AppId;
                mchId = Senparc.Weixin.Config.SenparcWeixinSetting.TenPayV3_MchId;
                payKey = Senparc.Weixin.Config.SenparcWeixinSetting.TenPayV3_Key;
                body = "";//需要重新赋值
                out_trade_no = "";//需要重新赋值
                totalfee = 1; ;//需要重新赋值
                notifyurl = Senparc.Weixin.Config.SenparcWeixinSetting.TenPayV3_TenpayNotify;
                string name = Dns.GetHostName();
                IPAddress[] ipadrlist = Dns.GetHostAddresses(name);
                foreach (IPAddress ipa in ipadrlist)
                {
                    if (ipa.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipa.ToString(); break;
                    }
                }
            }
            public static string AppId => appId; //微信支付分配的公众账号ID（企业号corpid即为此appId）
            public static string MchId => mchId;//微信支付分配的商户号
            public static string Body => body; //商品描述
            public static string OutTradeNo => out_trade_no; //商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|* 且在同一个商户号下唯一。
            public static int TotalFee => totalfee; //订单总金额，单位为分
            public static string SpbillCreateIp => ipAddress; //APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。
            public static string NotifyUrl => notifyurl;//异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。
            public static string OpenId = ""; //trade_type=JSAPI时（即公众号支付），此参数必传，此参数为微信用户在商户对应appid下的唯一标识。
            public static string PayKey => payKey;


        }
        #endregion

        /// <summary>
        ///  步骤2：商户后台收到用户支付单，调用微信支付统一下单接口。参见【统一下单API】。
        /// 【返回统一下单接口返回正常的prepay_id，再按签名规范重新生成签名后，将数据传输给APP。参与签名的字段名为appid，partnerid，prepayid，noncestr，timestamp，package。注意：package的值格式为Sign=WXPay】
        /// </summary>
        /// <param name="courseId">courseId</param> 
        /// <param name="orderNo">订单 </param> 
        /// <returns>返回结果值</returns>
        [HttpGet()]
        public async Task<PayWxResult> SetPayWxOrder(long courseId, string orderNo)
        {
            PayWxResult result = new PayWxResult();
            try
            {
                //#if DEBUG
                //                courseId = 2;
                //#endif
                #region  业务参数判断
                if (courseId < 0)
                {

                    result.Message = "商品信息不存在";
                    result.Code = (int)ResultCode.Fail;
                    return result;
                }
                if (string.IsNullOrWhiteSpace(orderNo))
                {

                    result.Message = "单号不能为空";
                    result.Code = (int)ResultCode.Fail;
                    return result;
                }
                var courseOrder = _courseOrderService.Table().FirstOrDefault(n => n.OrderNo == orderNo);
                if (courseOrder == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"订单单号不存在";
                    return result;
                }
                #endregion
                var lrt = await _courseService.GetAsync(courseId);
                var course = lrt.Data;
                if (course == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"课程信息不存在";
                    return result;
                }
                else
                {
                    var appId = Senparc.Weixin.Config.SenparcWeixinSetting.TenPayV3_AppId;
                    var Key = Senparc.Weixin.Config.SenparcWeixinSetting.TenPayV3_Key;
                    string sp_billno = courseOrder.OrderNo;
                    var body = course.CourseName;
                    int totalfee = (int)(courseOrder.PayPrice * 100);
                    var data = new TenPayV3UnifiedorderRequestData(WeChetConfig.AppId, WeChetConfig.MchId, body, sp_billno, totalfee, WeChetConfig.SpbillCreateIp, WeChetConfig.NotifyUrl, TenPayV3Type.APP, WeChetConfig.OpenId, WeChetConfig.PayKey, TenPayV3Util.GetNoncestr());
                    var resultPay = TenPayV3.Unifiedorder(data);//调用统一下单接口
                    if (resultPay.result_code == "SUCCESS" && resultPay.return_code == "SUCCESS")
                    {
                        string nonceStr = TenPayV3Util.GetNoncestr();
                        string timeStamp = TenPayV3Util.GetTimestamp();
                        var package = string.Format("prepay_id={0}", resultPay.prepay_id);

                        var info = new PayWxInfo()
                        {
                            price = totalfee.ToString(),
                            orderNo = orderNo,
                            mch_id = resultPay.mch_id,
                            device_info = resultPay.device_info,
                            nonce_str = resultPay.nonce_str,
                            sign = resultPay.sign,
                            result_code = resultPay.result_code,
                            prepay_id = resultPay.prepay_id,
                            courseId = courseId,
                            AppId = WeChetConfig.AppId,
                            TimeStamp = timeStamp,
                            Package = package,
                            PaySign = TenPayV3.GetJsPaySign(appId, timeStamp, nonceStr, package, Key),
                        };
                        result.Data = info;

                        Redis.StringSetWithType<PayWxInfo>($"Pay:PayWx:{sp_billno}", info, TimeSpan.FromDays(24));

                        Redis.ListLeftPushWithType<PayWxInfo>($"Pay:PayWx:BillNo", info);

                    }
                    else
                    {
                        result.Code = (int)ResultCode.Fail;
                        result.Message = $"消息失败, {resultPay.err_code}{resultPay.err_code_des}";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"消息失败,{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }
        /// <summary>
        /// 微信调用使用，直接调用无效
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        [AllowAnonymous]
        public ActionResult PayNotifyUrl()
        {
            string returnResutStr = "";

            try
            {
                //建议增减队列，从队列中取值判断

                var code = Redis.ListLeftPopWithType<PayWxInfo>($"Pay:PayWx:BillNo");
                Logger.Info($"rediscode:" + code.orderNo + "获取订单成功");
                #region  获取参数值 根据需要解析对应的参数

                ResponseHandler resHandler = new ResponseHandler(HttpContext);
                string result_code = resHandler.GetParameter("result_code");//业务结果
                string return_code = resHandler.GetParameter("return_code");//返回状态吗
                string out_trade_no = resHandler.GetParameter("out_trade_no");//商户订单号
                string WEXIN_OPENID = resHandler.GetParameter("openid");//微信openid
                string transaction_id = resHandler.GetParameter("transaction_id");//微信支付单号
                string amount = resHandler.GetParameter("total_fee");//订单总金额，单位为分
                Logger.Debug("参数result_code：" + result_code);
                Logger.Debug("参数return_code：" + return_code);
                Logger.Debug("参数out_trade_no：" + out_trade_no);
                Logger.Debug("参数WEXIN_OPENID：" + WEXIN_OPENID);
                Logger.Debug("参数transaction_id：" + transaction_id);
                Logger.Debug("参数amount：" + amount);
                #endregion

                var payOrderModel = new PayOrderModel();
                // resHandler.SetKey(TenPayV3Info.Key);
                //验证请求是否从微信发过来（安全）
                if (result_code == "SUCCESS" && return_code == "SUCCESS") //验证 是否 微信 回调
                {

                    //这一步是去数据库查询是否有该订单号。
                    Logger.Debug("接受日志信息返回成功");
                    //out_trade_no 取数据查询是否存在该订单
                    //直到这里，才能认为交易真正成功了，可以进行数据库操作，但是别忘了返回规定格式的消息！
                    var orderEntity = _courseOrderService.Table().FirstOrDefault(n => n.OrderNo == out_trade_no);

                    if (orderEntity != null)
                    {
                        try
                        {
                            #region  数据库逻辑 修改订单状态以及其他相关业务
                            orderEntity.IsPay = 1;
                            orderEntity.OrderState = 1;
                            orderEntity.PayType = 2;
                            _courseOrderService.Update(orderEntity);

                            var orderInfo = _orderInfoService.Get(out_trade_no);
                            if (orderInfo != null)
                            {
                                orderInfo.Status = (int)OrderStatus.Success;
                                orderInfo.PaymentType = 0;
                                orderInfo.Payment = orderEntity == null ? "0" : orderEntity.PayPrice.ToString("F2");
                                orderInfo.PaymentTime = DateTime.Now;
                                orderInfo.EndTime = DateTime.Now;
                                orderInfo.UpdateTime = orderInfo.EndTime.Value;
                                orderInfo.IsFrom = 0;
                                orderInfo.PaymentNo = transaction_id;
                                orderInfo.Status = 5;
                                _orderInfoService.Update(orderInfo);
                            }
                            //修改优惠券的状态
                            //var entity = _userCouponRepository.Get(userCouponId);
                            //if (entity == null)
                            //    return -2;
                            //if (entity.Status != 0)
                            //    return entity.Status;
                            //entity.Status = (int)CouponStatus.HasUsed;
                            //var isUpdate = await _userCouponRepository.UpdateAsync(entity);
                            #endregion
                            //向任务REDIS中写数据 
                            // _taskService.SetTaskToRedis(ActionType.Scmk, 1, orderEntity.UserId);
                            Logger.Info($"OrderState:" + out_trade_no + "支付成功");

                            //判断是否存在收藏 
                            var courseBuyCollectEntity = _courseBuyCollectService.Table()
                                .FirstOrDefault(n => n.CourseId == code.courseId && n.UserId == orderEntity.UserId);
                            if (courseBuyCollectEntity != null)
                            {
                                courseBuyCollectEntity.CourseOrderId = code.orderNo;
                                courseBuyCollectEntity.IsBuy = 1;
                                courseBuyCollectEntity.CreateTime = DateTime.Now;
                                courseBuyCollectEntity.ExpirationDate = DateTime.Now.AddYears(1);
                                courseBuyCollectEntity.IsHide = 0;
                                _courseBuyCollectService.Update(courseBuyCollectEntity);
                                Logger.Info($"BuyCollect:" + code.orderNo + "修改支付成功记录");
                            }

                            //添加至台账表

                            payOrderModel.OrderId = out_trade_no;
                            payOrderModel.BusinessType = BusinessType.Course;
                            payOrderModel.UserId = orderInfo.UserId;
                            payOrderModel.IsAutoRenew = false;
                            payOrderModel.PaymentNo = transaction_id;
                            payOrderModel.PaymentType = (int)PaymentType.WeChatPay;
                            payOrderModel.ProductId = "";
                            payOrderModel.IsSuccessPay = true;
                            payOrderModel.IsTest = false;
                            payOrderModel.Transaction = transaction_id;
                            payOrderModel.Price = orderInfo == null ? "" : orderInfo.Payment;
                            _orderInfoService.OrderPay(payOrderModel).GetAwaiter();
                            returnResutStr = "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
                        }
                        catch (Exception ex)
                        {
                            #region 日志处理

                            Logger.Debug("微信支付回调更新订单支付状态，更新订单支付状态失败" + ex.Message + "订单号：" + out_trade_no);

                            #endregion
                            payOrderModel.IsSuccessPay = false;
                            _orderInfoService.OrderPay(payOrderModel).GetAwaiter();
                            returnResutStr = "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[error]]></return_msg></xml>";
                        }

                    }
                    else
                    {
                        payOrderModel.IsSuccessPay = false;
                        _orderInfoService.OrderPay(payOrderModel).GetAwaiter();
                        Logger.Debug("微信支付回调更新订单支付状态，查无此单，订单号：" + out_trade_no);
                    }
                }
                else //微信回调失败
                {
                    #region 日志处理
                    Logger.Debug("微信支付回调更新订单支付状态，验证失败，回调参数是非法的");
                    #endregion
                    returnResutStr = "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[error]]></return_msg></xml>";
                }

            }
            catch (Exception ex)
            {
                #region 添加错误日志

                Logger.Debug("微信支付回调更新订单支付状态，出现异常:" + ex.Message + "，堆栈：" + ex.StackTrace);
                #endregion

                returnResutStr = "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[error]]></return_msg></xml>";
            }
            return Content(returnResutStr);


        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderNo">订单号码</param>
        /// <returns></returns>
        [HttpGet()]
        [AllowAnonymous]
        public PayWxNotifyResult GetPayWxOrder(string orderNo)
        {
            PayWxNotifyResult result = new PayWxNotifyResult();
            try
            {
                var code = Redis.ListLeftPopWithType<PayWxInfo>($"Pay:PayWx:BillNo");
                var notifyGet = Redis.StringGetWithType<NotifyUrlInfo>($"Pay:NotifyUrl:{orderNo}");
                if (notifyGet == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"暂无数据,等待";
                    return result;
                }
                else
                {
                    result.Code = (int)ResultCode.Success;
                    result.Message = $"获取redis信息成功";
                    result.Data = notifyGet;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"消息失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }

        /// <summary>
        /// 苹果支付 todo 
        /// </summary>
        /// <param name="orderNo">订单编码</param>
        /// <param name="transcatonid">订单编号</param>
        /// <param name="receiptdata"></param>
        /// <returns></returns>
        [HttpPost()]
        public Result GetAppleOrder([FromForm] string orderNo, [FromForm] string transcatonid, [FromForm] string receiptdata)
        {

            Logger.Info($"receiptdata:{receiptdata}");
            Logger.Info($"orderNo:{orderNo},transcatonid:{transcatonid}");
            Result result = new Result();

            try
            {
                if (string.IsNullOrWhiteSpace(orderNo))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"消息失败，订单号不能空";
                    return result;
                }
                if (string.IsNullOrWhiteSpace(receiptdata))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"消息失败";
                    return result;
                }
                #region 苹果支付相关介绍
                /*
                     https://www.cnblogs.com/zhaoqingqing/p/4597794.html


            * 21000 App Store不能读取你提供的JSON对象
            * 21002 receipt-data域的数据有问题
            * 21003 receipt无法通过验证
            * 21004 提供的shared secret不匹配你账号中的shared secret
            * 21005 receipt服务器当前不可用
            * 21006 receipt合法，但是订阅已过期。服务器接收到这个状态码时，receipt数据仍然会解码并一起发送
            * 21007 receipt是Sandbox receipt，但却发送至生产系统的验证服务
            * 21008 receipt是生产receipt，但却发送至Sandbox环境的验证服务
            * $receipt_data 苹果返回的支付凭证
            * 正式 ： https://buy.itunes.apple.com/verifyReceipt
            * 沙箱 ： https://sandbox.itunes.apple.com/verifyReceipt
                     *
                     * {
            code = 400;
            data = "<null>";
            message = "\U5931\U8d25Value cannot be null.
            \nParameter name: value";
            }
            */
                #endregion

                //                string url = "https://buy.itunes.apple.com/verifyReceipt";
                //#if DEBUG
                //                orderNo = "1000000579960857";
                //                #region MyRegion
                //                string data = @"MIIWgwYJKoZIhvcNAQcCoIIWdDCCFnACAQExCzAJBgUrDgMCGgUAMIIGJAYJKoZIhvcNAQcBoIIGFQSCBhExggYNMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgEDAgEBBAMMATEwCwIBCwIBAQQDAgEAMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDAIBDgIBAQQEAgIAjjANAgENAgEBBAUCAwHWUTANAgETAgEBBAUMAzEuMDAOAgEJAgEBBAYCBFAyNTMwFwIBAgIBAQQPDA1kdWZ1c2Nob29sLmNuMBgCAQQCAQIEEEgyyTzvMApPv6J7Jd1QMRcwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAcAgEFAgEBBBQeCdKuJWg2n5763jDIi8Lj5FUWjjAeAgEMAgEBBBYWFDIwMTktMTAtMjFUMDU6MTM6MjBaMB4CARICAQEEFhYUMjAxMy0wOC0wMVQwNzowMDowMFowTQIBBwIBAQRFC2Q5WZnf4P4d/yPbHv5hfQj4Oh0xp4DRz3c+4XGA9ws6FaVf0ZMa0nIK1VZ9xVchIgbgusssA1kZGcJeD0mmUkKkGn2sMFQCAQYCAQEETLndl0EGbbGscvcF6u20+rLMmF/3Yz08+FMFnVBl9EdY/lbKavbglC8PCUTVoZ/iy1uPQnF/MeLhbIk9ZX8ChZ7Bg7JzhHBbyuEy1xgwggFUAgERAgEBBIIBSjGCAUYwCwICBqwCAQEEAhYAMAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQEwDAICBq4CAQEEAwIBADAMAgIGrwIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwGgICBqYCAQEEEQwPc3R1ZGVudHNndXNoaWNpMBsCAganAgEBBBIMEDEwMDAwMDA1ODE1ODEyMDUwGwICBqkCAQEEEgwQMTAwMDAwMDU4MTU4MTIwNTAfAgIGqAIBAQQWFhQyMDE5LTEwLTIxVDA1OjEzOjIwWjAfAgIGqgIBAQQWFhQyMDE5LTEwLTIxVDA1OjEzOjIwWjCCAVUCARECAQEEggFLMYIBRzALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpgIBAQQSDBBzdHVkeWNoaW5lc2V3ZWxsMBsCAganAgEBBBIMEDEwMDAwMDA1ODAxODAwMDkwGwICBqkCAQEEEgwQMTAwMDAwMDU4MDE4MDAwOTAfAgIGqAIBAQQWFhQyMDE5LTEwLTE3VDAyOjQ4OjIwWjAfAgIGqgIBAQQWFhQyMDE5LTEwLTE3VDAyOjQ4OjIwWjCCAVUCARECAQEEggFLMYIBRzALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpgIBAQQSDBBzdHVkeWNoaW5lc2V3ZWxsMBsCAganAgEBBBIMEDEwMDAwMDA1ODAyNjAzMjUwGwICBqkCAQEEEgwQMTAwMDAwMDU4MDI2MDMyNTAfAgIGqAIBAQQWFhQyMDE5LTEwLTE3VDA2OjQwOjM5WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTE3VDA2OjQwOjM5WqCCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAKAF37AXcPdix7iHHnRGab/ahwESmHNqKIiAyY/EV7Frn7emap9mntD3PMMFWPUs32f+N6Voo79p9bzL1gSnh4qh0So9rKBj1eWzPVJoOmwWHzAj5qeOwHfSrp7KgNkO80WDc9ijQtp9otSYNUJoigqGHcDWuhdMNhx26HdeUVOgghSr/NENx8dKwhNKz1wihtQjLjZQw26mzUt3zJsFhxgMzYKsWg8i7MsWpbMYnXO+shiEEmQlHKo8YuE6xdc7d2TZqlX8AoUYjgt9iHifTAigwIglAUI0niv/8C9J5yun6NPprcFFO/xDBP9FAOqD5VNPXOvL923xqw9Rh5XE3uw==";

                //                #endregion
                //                //1000000580180009

                //#endif

                #region 发送请求到苹果服务器返回数据

                //发送请求到苹果服务器
                /*
                 {
"receipt":{"receipt_type":"ProductionSandbox", "adam_id":0, "app_item_id":0, "bundle_id":"dufuschool.cn", "application_version":"1", "download_id":0, "version_external_identifier":0, "receipt_creation_date":"2019-10-21 05:13:20 Etc/GMT", "receipt_creation_date_ms":"1571634800000", "receipt_creation_date_pst":"2019-10-20 22:13:20 America/Los_Angeles", "request_date":"2019-10-21 06:00:19 Etc/GMT", "request_date_ms":"1571637619714", "request_date_pst":"2019-10-20 23:00:19 America/Los_Angeles", "original_purchase_date":"2013-08-01 07:00:00 Etc/GMT", "original_purchase_date_ms":"1375340400000", "original_purchase_date_pst":"2013-08-01 00:00:00 America/Los_Angeles", "original_application_version":"1.0", 
"in_app":[
{"quantity":"1", "product_id":"studentsgushici", "transaction_id":"1000000581581205", "original_transaction_id":"1000000581581205", "purchase_date":"2019-10-21 05:13:20 Etc/GMT", "purchase_date_ms":"1571634800000", "purchase_date_pst":"2019-10-20 22:13:20 America/Los_Angeles", "original_purchase_date":"2019-10-21 05:13:20 Etc/GMT", "original_purchase_date_ms":"1571634800000", "original_purchase_date_pst":"2019-10-20 22:13:20 America/Los_Angeles", "is_trial_period":"false"}, 
{"quantity":"1", "product_id":"studychinesewell", "transaction_id":"1000000580180009", "original_transaction_id":"1000000580180009", "purchase_date":"2019-10-17 02:48:20 Etc/GMT", "purchase_date_ms":"1571280500000", "purchase_date_pst":"2019-10-16 19:48:20 America/Los_Angeles", "original_purchase_date":"2019-10-17 02:48:20 Etc/GMT", "original_purchase_date_ms":"1571280500000", "original_purchase_date_pst":"2019-10-16 19:48:20 America/Los_Angeles", "is_trial_period":"false"}, 
{"quantity":"1", "product_id":"studychinesewell", "transaction_id":"1000000580260325", "original_transaction_id":"1000000580260325", "purchase_date":"2019-10-17 06:40:39 Etc/GMT", "purchase_date_ms":"1571294439000", "purchase_date_pst":"2019-10-16 23:40:39 America/Los_Angeles", "original_purchase_date":"2019-10-17 06:40:39 Etc/GMT", "original_purchase_date_ms":"1571294439000", "original_purchase_date_pst":"2019-10-16 23:40:39 America/Los_Angeles", "is_trial_period":"false"}]}, "status":0, "environment":"Sandbox"}
                 */
                #endregion


                var orderC = _courseOrderService.Table().FirstOrDefault(n => n.OrderNo == orderNo);
                if (orderC == null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"消息失败,订单号不存在";
                    return result;
                }

                //发送post 到苹果数据 获取验证
                string jsonText = "{\"password\":\"" + _options.Value.ShareKey + "\",\"receipt-data\":\"" + receiptdata + "\"}";
                //TODO
                #region TODO 苹果验证
                string result1 = HttpUtil.PostUrl("https://buy.itunes.apple.com/verifyReceipt", jsonText);
                if (!string.IsNullOrWhiteSpace(result1))
                {
                    //TODO 解析json
                    try
                    {
                        var dataInfo = JsonConvert.DeserializeObject<PayAppleOrder>(result1);
                        if (dataInfo != null && dataInfo.receipt != null && dataInfo.receipt.in_app.Count > 0)
                        {
                            var infoItems = dataInfo.receipt.in_app.Where(n => n.transaction_id == transcatonid);
                            //修改订单状态 订单存在表示订单已经支付，修改支付状态

                            orderC.IsPay = 1;
                            orderC.OrderState = 1;
                            orderC.PayType = 4;
                            _courseOrderService.Update(orderC);

                            var orderInfo = _orderInfoService.Get(orderNo);
                            if (orderInfo != null)
                            {
                                orderInfo.Status = (int)OrderStatus.Success;
                                orderInfo.PaymentType = (int)PaymentType.ApplePay;
                                orderInfo.PaymentNo = transcatonid;
                                orderInfo.Payment = orderC.PayPrice.ToString("F2");
                                orderInfo.PaymentTime = DateTime.Now;
                                orderInfo.EndTime = DateTime.Now;
                                orderInfo.UpdateTime = orderInfo.EndTime.Value;
                                orderInfo.IsFrom = 0;
                                _orderInfoService.Update(orderInfo);
                            }
                            //判断是否存在收藏 
                            var courseBuyCollectEntity = _courseBuyCollectService.Table()
                                .FirstOrDefault(n => n.CourseId == orderC.CourseId && n.UserId == orderC.UserId);
                            if (courseBuyCollectEntity != null)
                            {
                                courseBuyCollectEntity.CourseOrderId = orderC.OrderNo;
                                courseBuyCollectEntity.IsBuy = 1;
                                courseBuyCollectEntity.CreateTime = DateTime.Now;
                                courseBuyCollectEntity.ExpirationDate = DateTime.Now.AddYears(1);
                                courseBuyCollectEntity.IsHide = 0;
                                _courseBuyCollectService.Update(courseBuyCollectEntity);
                                Logger.Info($"BuyCollect:" + orderC.OrderNo + "修改支付成功记录");
                            }

                            //添加至台账表
                            var payOrderModel = new PayOrderModel
                            {
                                OrderId = orderNo,
                                BusinessType = BusinessType.Course,
                                UserId = orderInfo.UserId,
                                IsAutoRenew = false,
                                PaymentNo = transcatonid,
                                PaymentType = (int)PaymentType.ApplePay,
                                ProductId = orderInfo == null ? "" : orderInfo.ProductId,
                                IsSuccessPay = true,
                                IsTest = false,
                                Transaction = transcatonid,
                                Price = orderInfo == null ? "" : orderInfo.Payment,
                            };
                            _orderInfoService.OrderPay(payOrderModel).GetAwaiter();
                        }
                    }
                    catch (Exception e)
                    {
                        result.Code = (int)ResultCode.Fail;
                        result.Message = $"消息失败{e.Message}";
                        Logger.Error(e);
                    }
                }
                string result2 = HttpUtil.PostUrl("https://sandbox.itunes.apple.com/verifyReceipt", jsonText);
                if (!string.IsNullOrWhiteSpace(result2))
                {
                    //TODO 解析json
                    try
                    {
                        var dataInfo = JsonConvert.DeserializeObject<PayAppleOrder>(result2);
                        if (dataInfo != null && dataInfo.receipt != null && dataInfo.receipt.in_app.Count > 0)
                        {
                            var infoItems = dataInfo.receipt.in_app.Where(n => n.transaction_id == transcatonid);
                            //修改订单状态 订单存在表示订单已经支付，修改支付状态
                            orderC.IsPay = 1;
                            orderC.OrderState = 1;
                            orderC.PayType = 4;
                            _courseOrderService.Update(orderC);

                            var orderInfo = _orderInfoService.Get(orderNo);
                            if (orderInfo != null)
                            {
                                orderInfo.Status = (int)OrderStatus.Success;
                                orderInfo.PaymentType = (int)PaymentType.ApplePay;
                                orderInfo.PaymentNo = transcatonid;
                                orderInfo.Payment = orderC.PayPrice.ToString("F2");
                                orderInfo.PaymentTime = DateTime.Now;
                                orderInfo.EndTime = DateTime.Now;
                                orderInfo.UpdateTime = orderInfo.EndTime.Value;
                                orderInfo.IsFrom = 0;
                                _orderInfoService.Update(orderInfo);
                            }
                            //判断是否存在收藏 
                            var courseBuyCollectEntity = _courseBuyCollectService.Table()
                                .FirstOrDefault(n => n.CourseId == orderC.CourseId && n.UserId == orderC.UserId);
                            if (courseBuyCollectEntity != null)
                            {
                                courseBuyCollectEntity.CourseOrderId = orderC.OrderNo;
                                courseBuyCollectEntity.IsBuy = 1;
                                courseBuyCollectEntity.CreateTime = DateTime.Now;
                                courseBuyCollectEntity.ExpirationDate = DateTime.Now.AddYears(1);
                                courseBuyCollectEntity.IsHide = 0;
                                _courseBuyCollectService.Update(courseBuyCollectEntity);
                                Logger.Info($"BuyCollect:" + orderC.OrderNo + "修改支付成功记录");
                            }

                            //添加至台账表
                            var payOrderModel = new PayOrderModel
                            {
                                OrderId = orderNo,
                                BusinessType = BusinessType.Course,
                                UserId = orderInfo.UserId,
                                IsAutoRenew = false,
                                PaymentNo = transcatonid,
                                PaymentType = (int)PaymentType.ApplePay,
                                ProductId = orderInfo == null ? "" : orderInfo.ProductId,
                                IsSuccessPay = true,
                                IsTest = true,
                                Transaction = transcatonid,
                                Price = orderInfo == null ? "" : orderInfo.Payment,
                            };
                            _orderInfoService.OrderPay(payOrderModel).GetAwaiter();
                        }
                    }
                    catch (Exception e)
                    {
                        result.Code = (int)ResultCode.Fail;
                        result.Message = $"消息失败{e.Message}";
                        Logger.Error(e);
                    }
                }
                #endregion

                if (string.IsNullOrWhiteSpace(orderNo))
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"消息失败，订单号不存在";
                    return result;
                }
                else
                {
                    result.Code = (int)ResultCode.Success;
                    result.Message = $"操作成功";
                }

            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"消息失败{ex.Message}";
                Logger.Error(ex);
            }

            return result;
        }
    }



}

