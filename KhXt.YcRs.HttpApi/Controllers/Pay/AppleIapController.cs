﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Services.Order;
using Hx.Components;
using Hx.Logging;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.Pay;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Payment.ApplePay;
using Payment.ApplePay.Request;
using Senparc.CO2NET.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers.Pay
{
    [Route("api/Pay")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Pay)]
    public class AppleIapController : BaseApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IAppleIapSdkRequest _appleIapSdkRequest;
        /// <summary>
        /// 
        /// </summary>
        private readonly IOptions<AppleIapOptions> _options;

        private readonly IOrderInfoService _orderInfoService;

        public AppleIapController(IAppleIapSdkRequest appleIapSdkRequest, IOptions<AppleIapOptions> options, IOrderInfoService orderInfoService)
        {
            _appleIapSdkRequest = appleIapSdkRequest;
            _options = options;
            _orderInfoService = orderInfoService;
        }
        /// <summary>
        /// 普通支付 
        /// </summary>
        /// <param name="appleIap"></param>
        /// <returns>返回错误码对照苹果内购验证凭据返回结果状态码</returns>
        [Route("IapVerify")]
        [HttpPost]
        public async Task<Result> Post(AppleIapViewModel appleIap)
        {
            try
            {
                var iapReq = new AppleIapRequest
                {
                    ReceiptData = appleIap.Receipt,
                    Password = _options.Value.ShareKey
                };
                Logger.Info("调用苹果内购！");
                Logger.Info(iapReq);
                var rlt = await _appleIapSdkRequest.AppleIapVerify(iapReq, _options.Value, appleIap.TransactionIdentifier);
                Logger.Info("调用苹果内购返回数据！");
                Logger.Info(rlt);
                if (rlt.Code == "1")
                {
                    var payOrderModel = appleIap.MapTo<PayOrderModel>();
                    //调用业务模块
                    if (appleIap.State.Equals("Purchased"))//支付成功 
                    {
                        Logger.Info("支付成功！处理业务");
                        payOrderModel.UserId = UserIdValue;
                        payOrderModel.IsAutoRenew = false;
                        payOrderModel.ProductId = appleIap.ProductIdentifier;
                        payOrderModel.Transaction = rlt.Transaction.ToJson();
                        payOrderModel.PaymentNo = rlt.PayNo;
                        payOrderModel.IsTest = rlt.IsSandBox;
                        payOrderModel.PaymentType = (int)PaymentType.ApplePay;
                        payOrderModel.BusinessType = (BusinessType)appleIap.BusinessType;
                        Logger.Info(payOrderModel);
                        var payResult = await _orderInfoService.PayOrder(payOrderModel);
                        var isSuccess = payResult.Code.Equals((int)ResultCode.Success);
                        payOrderModel.IsSuccessPay = isSuccess;
                        await _orderInfoService.OrderPay(payOrderModel);
                        return payResult;
                    }
                    else if (appleIap.State.Equals("Restored"))//已经订阅过 
                    {
                        Logger.Info("已经订阅过");
                        return new Result() { Code = (int)ResultCode.Fail, Message = "已经订阅过！" };
                    }
                    else //支付失败
                    {
                        payOrderModel.IsSuccessPay = false;
                        await _orderInfoService.OrderPay(payOrderModel);
                        Logger.Info("支付失败");
                        return new Result() { Code = (int)ResultCode.Fail, Message = "支付失败！" };
                    }
                }
                else
                {
                    Logger.Info("支付失败,调用苹果内购");
                    return new Result() { Code = int.Parse(rlt.Code), Message = "支付失败！" };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"苹果普通支付{appleIap.BusinessType},{appleIap.OrderNo},{appleIap.ProductIdentifier},支付异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "普通支付异常！" };
            }
        }
        /// <summary>
        /// 自动续费 
        /// </summary>
        /// <param name="appleIap"></param>
        /// <returns>返回错误码对照苹果内购验证凭据返回结果状态码</returns>
        [Route("IapRenewVerify")]
        [HttpPost]
        public async Task<Result> RenewVerify(AppleIapViewModel appleIap)
        {
            try
            {
                var iapReq = new AppleIapRequest
                {
                    ReceiptData = appleIap.Receipt,
                    Password = _options.Value.ShareKey
                };
                var rlt = await _appleIapSdkRequest.AppleIapRenewVerify(iapReq, _options.Value, appleIap.TransactionIdentifier);
                if (rlt.Code == "1")
                {
                    var payOrderModel = appleIap.MapTo<PayOrderModel>();
                    //调用业务模块
                    if (appleIap.State.Equals("Purchased"))//支付成功 
                    {
                        payOrderModel.UserId = UserIdValue;
                        payOrderModel.IsAutoRenew = true;
                        payOrderModel.PaymentNo = rlt.PayNo;
                        payOrderModel.ProductId = appleIap.ProductIdentifier;
                        payOrderModel.Transaction = rlt.Transaction.ToJson();
                        payOrderModel.IsTest = rlt.IsSandBox;
                        payOrderModel.PaymentType = (int)PaymentType.ApplePay;
                        payOrderModel.BusinessType = (BusinessType)appleIap.BusinessType;
                        var payResult = await _orderInfoService.PayOrder(payOrderModel);
                        var isSuccess = payResult.Code.Equals((int)ResultCode.Success);
                        payOrderModel.IsSuccessPay = isSuccess;
                        await _orderInfoService.OrderPay(payOrderModel);
                        return payResult;
                    }
                    else if (appleIap.State.Equals("Restored"))//已经订阅过 
                    {
                        return new Result() { Code = (int)ResultCode.Fail, Message = "已经订阅过！" };
                    }
                    else //支付失败
                    {
                        payOrderModel.IsSuccessPay = false;
                        await _orderInfoService.OrderPay(payOrderModel);
                        return new Result() { Code = (int)ResultCode.Fail, Message = "支付失败！" };
                    }
                }
                else
                {
                    return new Result() { Code = int.Parse(rlt.Code), Message = "支付失败！" };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"苹果自动续费支付{appleIap.BusinessType},{appleIap.OrderNo},{appleIap.ProductIdentifier},支付异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "自动续费异常！" };
            }
        }
    }
}