using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.JWT;
using KhXt.YcRs.Domain.Models.Test;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx;
using Hx.Components;
using Hx.Configurations.Application;
using Hx.Extensions;
using Hx.ObjectMapping;
using KhXt.YcRs.HttpApi.ApiGroup;
using KhXt.YcRs.HttpApi.Models.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers
{
    /// <summary>
    /// 授权用避免切换登录
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiGroup(ApiGroupNames.Auth, ApiGroupNames.Course, ApiGroupNames.Order)]
    [AllowAnonymous]
    public class AuthController : BaseApiController
    {
        private ITestService _testService;
        private readonly IUserService _userService;
        private readonly IUserCacheService _userCacheService;
        //RedisHelper _redis;
        public AuthController(IUserService userService,
            ITestService testService, IUserCacheService userCacheService)
        {
            _testService = testService;
            _userService = userService;
            _userCacheService = userCacheService;
        }
        #region 获取登录手机号/或用户ID获取用户token
        [HttpGet]
        [AllowAnonymous]
        public async Task<string> GetTokenStr(string phone)
        {
            var rlt = await _userService.GetToken(phone);
            return rlt.Data;
        }
        /// <summary>
        /// 获取登录手机号/或用户ID获取用户token  JWT授权(数据将在请求头中进行传输) 直接在下框中输入冒号后的字符串信息，token:token字符串 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<Result> GetToken(string phone)
        {
            var rlt = await _userService.GetToken(phone);
            return rlt;
        }
        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<Result> GetSms(string phone)
        {
            var sms = "";
            Result result = new Result();
            if (phone.Length < 11)
            {
                try
                {
                    long.TryParse(phone, out long idResult);
                    var info = await _userService.Get(idResult);
                    phone = string.IsNullOrWhiteSpace(info.Phone) ? phone : info.Phone;

                }
                catch (Exception ex)
                {
                    result.Code = (int)ResultCode.Fail;
                    Logger.Error(ex);
                }
            }
            sms = await Redis.StringGetAsync("SMS:" + phone);
            if (!string.IsNullOrEmpty(sms))
            {
                result.Code = (int)ResultCode.Success;
                result.Data = $"{sms}";
                result.Message = "获取验证码成功";
            }
            return await Task.FromResult(result);

        }

        #endregion
        [HttpGet]
        [Route("/api/Node")]
        public async Task<Result<HxAppSettings>> GetNodeEnv()
        {
            Result<HxAppSettings> result = new Result<HxAppSettings>();
            try
            {

                HxAppSettings appSettings = new HxAppSettings();
                appSettings.NodeName = DomainEnv.NodeName;// DomainSetting.NodeName;
                appSettings.Mq = DomainEnv.Mq;
                appSettings.Redis = DomainEnv.Redis;
                appSettings.Db = DomainEnv.Db;
                appSettings.Convention = DomainEnv.Convention;
                string OS = DomainEnv.OS;
                string OSVersion = DomainEnv.OSVersion;
                string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                string tag = "1.0.5";
                // var token = Stopwatch.GetTimestamp().ToBinary(36);
                string message = "目前更新接口版本tag:" + tag + "|OS:" + OS + "|OSVersion:" + OSVersion;
                result.Code = (int)ResultCode.Success;
                result.Data = appSettings;
                result.Message = message;
                return await Task.FromResult(result);
            }
            catch (Exception ex)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = ex.Message;
                Console.WriteLine("----------------------------------------------:" + ex.Message);
                return await Task.FromResult(result);
            }
        }


    }
}

