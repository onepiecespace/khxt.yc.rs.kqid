﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Util;
using HuangLiCollege.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers
{
    public partial class UserController
    {
        /// <summary>
        /// 获取用户当前金币
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageIndex">金币变更历史记录分页索引,includeChangeFlow值为true时有效</param>
        /// <param name="pageSize">金币变更历史记录分页大小,includeChangeFlow值为true时有效</param>
        /// <param name="includeChangeFlow">true:返回分页的金币变更历史，false:只返回用户当前金币数</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<UserCoinResult> GetCoin(long userId, int pageIndex = 1, int pageSize = 10, bool includeChangeFlow = true)
        {
            try
            {

                //var currentCoin = await _rankingService.GetCoin(userId);
                var coinInfo = new UserCoinInfo { UserId = userId, Total = 0 };
                if (includeChangeFlow)
                {
                    var flows = await _userService.GetValueLogs(userId, KhXt.YcRs.Domain.ValueCategory.Coin, KhXt.YcRs.Domain.BusinessType.All, pageIndex - 1, pageSize);
                    coinInfo.ChangeFlow = flows;
                }
                //向任务REDIS中写数据 
                // await _taskService.SetTaskToRedis(ActionType.Ckjbmx, 1, userId);
                return new UserCoinResult { Data = coinInfo };
            }
            catch (Exception ex)
            {
                Logger.Error("uc.getcoin error", ex);
                return new UserCoinResult(KhXt.YcRs.Domain.ResultCode.Exception, ex.Message);
            }
        }
        #region 我的总经验
        /// <summary>
        /// 我的总经验
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<Result<MyExpInfo>> GetMyExp(long userId, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var rlt = await _userService.GetMyExp(userId, pageIndex, pageSize);
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取我的经验{userId}异常", ex);
                return new Result<MyExpInfo>() { Code = (int)ResultCode.Exception, Message = "获取我的经验异常" };
            }
        }

        #endregion
    }
}
