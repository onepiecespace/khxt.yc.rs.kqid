﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Sys;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.HttpApi.ApiGroup;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Controllers
{
    [Route("api/[controller]")]
    [ApiGroup(ApiGroupNames.Course)]
    [ApiController]
    public class HomeController : BaseApiController
    {
        private ICourseService _courseService;
        private ISettingService _settingService;
        private readonly IUserCacheService _userCacheService;
        public HomeController(ICourseService courseService, ISettingService settingService, IUserCacheService userCacheService)
        {
            _courseService = courseService;
            _settingService = settingService;
            _userCacheService = userCacheService;
        }
        /// <summary>
        ///两个黄鹂商学院app首页
        /// </summary>
        /// <returns></returns>
        [Route("/api/home")]
        [HttpGet]
        public async Task<Result<List<CategoryedCourseThinModel>>> Get(long userId = 0)
        {
            try
            {
                var rlt = new List<CategoryedCourseThinModel>();
                var user = await _userCacheService.GetUser(UserIdValue);
                if (user.UseDueDate < System.DateTime.Now)
                {
                    //查看Redis是否存在匹配项 存在移除token的值
                    string tokenstr = await ReadToken(user.Phone);
                    if (!string.IsNullOrEmpty(tokenstr))
                    {
                        string tokenkey = "token:" + user.Phone;
                        await Redis.KeyDeleteAsync(tokenkey); //删除token缓存
                    }
                    return new Result<List<CategoryedCourseThinModel>> { Code = (int)ResultCode.warning, Data = rlt, Message = "此手机号授权使用已过期,暂无使用权限！", Expire = 3 * 60 };
                }
                else
                {
                    var setting = await _settingService.GetHomeCourseSetting();
                    var hotData = await _courseService.GetHotList(setting, UserIdValue);

                    rlt.Add(new CategoryedCourseThinModel { Caption = "课程精选", Id = -1, Mode = 2, Values = hotData });
                    // var liveData = await _courseService.GetLiveList(setting, UserIdValue);
                    //var recData = await _courseService.GetRecommendList(setting, UserIdValue);
                    // var catedData = await _courseService.GetCategoriedTop(userId, setting);

                    // rlt.Add(new CategoryedCourseThinModel { Caption = "热门课程", Id = -1, Mode = 1, Values = hotData });
                    // rlt.Add(new CategoryedCourseThinModel { Caption = "直播课", Id = -2, Mode = 2, Values = liveData });
                    // rlt.Add(new CategoryedCourseThinModel { Caption = "推荐课程", Id = -3, Mode = 3, Values = recData });
                    // rlt.AddRange(catedData);
                    return new Result<List<CategoryedCourseThinModel>> { Data = rlt, Expire = 3 * 60 };
                }
            }
            catch (Exception ex)
            {
                Logger.Error("/api/home", ex);
                return new Result<List<CategoryedCourseThinModel>>(ResultCode.Exception, ex.Message);
            }
            finally
            {
                if (aid == "college")
                    await _courseService.SetCourseChildType();//处理直播状态

            }
        }
        private async Task<string> ReadToken(string phone)
        {
            return await Redis.StringGetAsync("token:" + phone);
        }
        /// <summary>
        /// pc端两个黄鹂商学院app首页
        /// </summary>
        /// <returns></returns>
        [Route("/api/webhome")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<Result<List<CategoryedCoursePcThinModel>>> webGet(long userId = 0)
        {
            try
            {
                var setting = await _settingService.GetHomeCourseSetting();
                var hotData = await _courseService.GetPcHotList(setting);
                var liveData = await _courseService.GetPcLiveList(setting);
                var recData = await _courseService.GetPcRecommendList(setting);
                //var catedData = await _courseService.GetPcCategoriedTop(userId, setting);
                var rlt = new List<CategoryedCoursePcThinModel>();
                rlt.Add(new CategoryedCoursePcThinModel { Caption = "热门课程", Id = -1, Mode = 1, Values = hotData });
                rlt.Add(new CategoryedCoursePcThinModel { Caption = "直播课", Id = -2, Mode = 2, Values = liveData });
                rlt.Add(new CategoryedCoursePcThinModel { Caption = "推荐课程", Id = -3, Mode = 3, Values = recData });
                //  rlt.AddRange(catedData);
                return new Result<List<CategoryedCoursePcThinModel>> { Data = rlt };
            }
            catch (Exception ex)
            {
                Logger.Error("/api/home", ex);
                return new Result<List<CategoryedCoursePcThinModel>>(ResultCode.Exception, ex.Message);
            }
            finally
            {
                if (aid == "college")
                    await _courseService.SetCourseChildType();//处理直播状态

            }
        }
    }
}
