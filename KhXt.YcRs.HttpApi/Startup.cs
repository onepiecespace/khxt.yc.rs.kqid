﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Hx.Components;
using Hx.Components.Autofac;
using Hx.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Senparc.CO2NET;
using Senparc.CO2NET.RegisterServices;
using Senparc.Weixin;
using Senparc.Weixin.Entities;
using Senparc.Weixin.RegisterServices;
using Senparc.Weixin.TenPay;
using System;
using System.IO;
using System.Linq;
using System.Text;
using Essensoft.AspNetCore.Payment.Alipay;
using KhXt.YcRs.Domain.Util;
using KhXt.YcRs.Infrastructure.Extension;
using KhXt.YcRs.Infrastructure.Config;
using KhXt.YcRs.HttpApi.ApiGroup;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace KhXt.YcRs.HttpApi
{
    public class Startup
    {
        private string _settingFile = "appsettings.Production.json";

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {

            _settingFile = env != null && !env.IsProduction() ? $"appsettings.{env.EnvironmentName}.json" : "appsettings.Production.json";
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile(_settingFile, optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; set; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {

            services.AddSingleton<IQRCode, RaffQRCode>();

            var basePath = Microsoft.DotNet.PlatformAbstractions.ApplicationEnvironment.ApplicationBasePath;
            //注册Swagger生成器，定义一个和多个Swagger 文档
            services.AddSwaggerGen(options =>
            {
                //遍历ApiGroupNames所有枚举值生成接口文档，Skip(1)是因为Enum第一个FieldInfo是内置的一个Int值
                typeof(ApiGroupNames).GetFields().Skip(1).ToList().ForEach(f =>
                {
                    //获取枚举值上的特性
                    var info = f.GetCustomAttributes(typeof(GroupInfoAttribute), false).OfType<GroupInfoAttribute>().FirstOrDefault();
                    options.SwaggerDoc(f.Name, new OpenApiInfo
                    {
                        Title = info?.Title,
                        Version = info?.Version,
                        Description = info?.Description
                    });
                });
                //没有加特性的分到这个NoGroup上
                options.SwaggerDoc("NoGroup", new OpenApiInfo
                {
                    Title = $"WebAPI 无分组 ",
                });
                //判断接口归于哪个分组
                options.DocInclusionPredicate((docName, apiDescription) =>
                {
                    if (docName == "NoGroup")
                    {
                        //当分组为NoGroup时，只要没加特性的都属于这个组
                        return string.IsNullOrEmpty(apiDescription.GroupName);
                    }
                    else
                    {
                        if (docName.Equals("All", StringComparison.OrdinalIgnoreCase))
                        {
                            return true;
                        };
                        if (apiDescription.GroupName.IsNullOrEmpty()) return false;
                        return apiDescription.GroupName.ToLower().Split(",").Contains(docName.ToLower());
                        // return (apiDescription.GroupName == docName) ;
                    }
                });

                // 为 Swagger JSON and UI设置xml文档注释路径
                options.IncludeXmlComments("KhXt.YcRs.Domain.xml", true);
                options.IncludeXmlComments("KhXt.YcRs.HttpApi.xml", true);

                #region Token绑定到ConfigureServices

                //添加header验证信息

                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "请求服务接口前，在header中增加自定义token，请填写其值即可。",
                    Name = "token",//jwt默认的参数名称
                    In = ParameterLocation.Header,//jwt默认存放Authorization信息的位置(请求头中)
                    Type = SecuritySchemeType.ApiKey
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement {
                {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference()
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                            }, Array.Empty<string>() }
                 });
                #endregion

            });
            #region 【认证】
            //读取配置文件
            var audienceConfig = Configuration.GetSection("Audience");
            var symmetricKeyAsBase64 = audienceConfig["Secret"];
            var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
            var signingKey = new SymmetricSecurityKey(keyByteArray);
            //2.1【认证】
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;


            })
             .AddJwtBearer(o =>
             {
                 o.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateIssuerSigningKey = true,
                     IssuerSigningKey = signingKey,
                     ValidateIssuer = true,
                     ValidIssuer = audienceConfig["Issuer"],//发行人
                     ValidateAudience = true,
                     ValidAudience = audienceConfig["Audience"],//订阅人
                     ValidateLifetime = true,
                     ClockSkew = TimeSpan.Zero,
                     RequireExpirationTime = true,
                 };

             });

            #endregion

            #region 组件注入
            services.AddWeChatPay();//微信支付
            services.AddAlipay();//支付宝支付
            //services.Configure<AlipayOptions>(Configuration.GetSection("Alipay"));
            //services.Configure<WeChatPayOptions>(Configuration.GetSection("WeChatPay"));
            //services.Configure<SmsSettings>(Configuration.GetSection("SmsSettings"));
            //services.Configure<OssOptions>(Configuration.GetSection("AliOssConfig"));//迁移到apimodule里
   

            services.AddCors(options => options.AddPolicy("any",
             policy => policy.WithOrigins("*").AllowAnyHeader().AllowAnyMethod()));

           services.AddMvc()
            .AddControllersAsServices()
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            });
            #region  HttpClientHelper 方法
            //services.AddHttpClient("mxh", client =>
            //{
            //    var apihost = Configuration.GetValue<string>("apihost");
            //    client.BaseAddress = new Uri(apihost);
            //});
            #endregion
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();//迁移到apimodule
            services.AddMemoryCache();//使用本地缓存必须添加
            services.AddSenparcGlobalServices(Configuration) //Senparc.CO2NET 全局注册
                .AddSenparcWeixinServices(Configuration);//Senparc.Weixin 注册
            #endregion
            
            Hx.BootStrapHelper.Boot<ApiModule>(builder =>
            {
                builder.Populate(services);

            }, "plugins", _settingFile);

            ApplicationContainer = ((AutofacObjectContainerEx)ObjectContainer.Current).Container;
            return new AutofacServiceProvider(ApplicationContainer);
        }

        /// <summary>
        /// /This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="senparcSetting"></param>
        /// <param name="senparcWeixinSetting"></param>
        public void Configure(IApplicationBuilder app,
            IWebHostEnvironment env,
            IOptions<SenparcSetting> senparcSetting,
            IOptions<SenparcWeixinSetting> senparcWeixinSetting)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //扩展log4net
            app.UseLog4net();
            //扩展多环境
            CustomerServiceProvider.ServiceProvider = app.ApplicationServices;
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            //启用中间件服务生成Swagger作为JSON终结点
            app.UseSwagger();
            //启用中间件服务对swagger-ui，指定Swagger JSON终结点
            app.UseSwaggerUI(c =>
            {
                //遍历ApiGroupNames所有枚举值生成接口文档，Skip(1)是因为Enum第一个FieldInfo是内置的一个Int值
                typeof(ApiGroupNames).GetFields().Skip(1).ToList().ForEach(f =>
                {
                    //获取枚举值上的特性
                    var info = f.GetCustomAttributes(typeof(GroupInfoAttribute), false).OfType<GroupInfoAttribute>().FirstOrDefault();
                    c.SwaggerEndpoint($"/swagger/{f.Name}/swagger.json", info != null ? info.Title : f.Name);

                });
                c.SwaggerEndpoint("/swagger/NoGroup/swagger.json", "无分组");
                c.RoutePrefix = string.Empty;
            });
            ////如果你想使用官方认证，必须在上边ConfigureService 中，配置JWT的认证服务 (.AddAuthentication 和 .AddJwtBearer 二者缺一不可)
            app.UseAuthentication();
            app.UseExceptionHandler(c =>
            {
                c.Run(async context =>
                {
                    //判断请求
                    if (context.Request != null)
                    {
                        var method = context.Request.Method;

                    }
                    var statusCodeReExecuteFeature = context.Features.Get<IStatusCodeReExecuteFeature>();

                    var exceptionHandlerPathFeature =
                        context.Features.Get<IExceptionHandlerPathFeature>();


                    if (exceptionHandlerPathFeature?.Error is FileNotFoundException)
                    {
                        await context.Response.WriteAsync("File error thrown!<br><br>\r\n");
                    }

                });
            });
            app.UseStaticFiles();
            app.UseCors("any");

            #region 微信支付配置

            IRegisterService register = RegisterService.Start(senparcSetting.Value)
                .UseSenparcGlobal();
            register.ChangeDefaultCacheNamespace("DefaultCO2NETCache");
            register.RegisterTraceLog(ConfigTraceLog);
            Senparc.CO2NET.APM.Config.DataExpire = TimeSpan.FromMinutes(60);
            register.UseSenparcWeixin(senparcWeixinSetting.Value, senparcSetting.Value)
                .RegisterTenpayOld(senparcWeixinSetting.Value, "【两个黄鹂小助手】公众号")
                .RegisterTenpayV3(senparcWeixinSetting.Value, "【两个黄鹂小助手】公众号")
            ;
            Senparc.Weixin.MP.Containers.AccessTokenContainer.RegisterAsync(Senparc.Weixin.Config.SenparcWeixinSetting.WeixinAppId, Senparc.Weixin.Config.SenparcWeixinSetting.WeixinAppSecret);

            #endregion

            //app.UseResponseBuffering();
            //app.UseResponseMiddleware();
           // app.UseIpRateLimiting();

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// 配置微信跟踪日志
        /// </summary>
        private void ConfigTraceLog()
        {
            //这里设为Debug状态时，/App_Data/WeixinTraceLog/目录下会生成日志文件记录所有的API请求日志，正式发布版本建议关闭

            //如果全局的IsDebug（Senparc.CO2NET.Config.IsDebug）为false，此处可以单独设置true，否则自动为true
            Senparc.CO2NET.Trace.SenparcTrace.SendCustomLog("系统日志", "系统启动");//只在Senparc.Weixin.Config.IsDebug = true的情况下生效

            //全局自定义日志记录回调
            Senparc.CO2NET.Trace.SenparcTrace.OnLogFunc = () =>
            {
                //加入每次触发Log后需要执行的代码
            };

            //当发生基于WeixinException的异常时触发
            WeixinTrace.OnWeixinExceptionFunc = ex =>
            {
                ////加入每次触发WeixinExceptionLog后需要执行的代码 
                //var eventService = new Senparc.Weixin.MP.Sample.CommonService.EventService();
                //await eventService.ConfigOnWeixinExceptionFunc(ex);      // DPBMARK_END
            };
        }
    }
}


