﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.HttpApi.Test
{
    public class Class
    {
        //Hx.Runtime.HxCallContext.SetTenantId("sss");
        //_repo.GetList();
        //Hx.Runtime.HxCallContext.Release();
    }

    public class SourceTest
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public AddressTest Address { get; set; }

        public override string ToString()
        {
            return $"Id:{Id},Name:{Name},Age:{Age},Address:{Address.Street},{Address.PostCode}";
        }
    }

    public class AddressTest
    {
        public string Street { get; set; }
        public string PostCode { get; set; }
    }
    public class DesTest
    {
        public string Caption { get; set; }
        public int Age { get; set; }

        public string Street { get; set; }
        public string PostCode { get; set; }

        public override string ToString()
        {
            return $"Caption:{Caption},Age:{Age},Street:{Street},PostCode:{PostCode}";
        }
    }

    public interface ISomeUtil
    {
        void SayHello(string name);
    }

    public class SomeUtil : ISomeUtil
    {
        public void SayHello(string name)
        {
            Console.WriteLine($"hello {name}");
        }
    }

    public class SomeOrder
    {
        public string Id { get; set; }
        public DateTime OrderTime { get; set; }

        public IEnumerable<SomeOrderItem> SomeOrderItems { get; set; }
    }


    public class SomeOrderItem
    {
        public string Id { get; set; }
        public string OrderId { get; set; }

        public string ProductId { get; set; }
    }
}
