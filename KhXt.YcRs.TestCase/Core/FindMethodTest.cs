﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace KhXt.YcRs.TestCase.Core
{
    public class FindMethodTest
    {
        [Fact]
        public void Do()
        {
            var list = new List<string> { "jaden", "kitty", "condy" };

            var s1 = list.Find(s => s.Equals("elsa"));
            Assert.Null(s1);

            //Assert.ThrowsAny<InvalidOperationException>(() =>
            //{
            //    var s=list.First(s => s.Equals("elsa"));
            //});

            var s2 = list.FirstOrDefault(s => s.Equals("elsa"));
            Assert.Null(s2);

        }
    }
}
