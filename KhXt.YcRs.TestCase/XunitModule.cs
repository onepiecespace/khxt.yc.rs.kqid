﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Services.Files;
using Hx.Components;
using Hx.Modules;
using Hx.MQ;
using Microsoft.Extensions.Configuration;
using Payment.ApplePay;

namespace HuanLiApp.TestCase
{
    [DependsOn(
        typeof(DomainModule),
        typeof(MqModule))]
    public class XunitModule : ModuleBase
    {
        public override void PreInitialize()
        {
            //Register<ITextCompare, TextCompare>(null, LifeStyle.Transient);
            Register<IAppleIapSdkRequest, AppleIap>(null, LifeStyle.Transient);
        }
        public override void Initialize()
        {
            var cfg = Resolve<IConfiguration>();
            //var hxsetting = cfg.GetSection("HuanXinSettings").Get<HuanXinSetting>();
            //ObjectContainer.RegisterInstance(hxsetting);
            var applePayConfig = cfg.GetSection("ApplePay").Get<AppleIapOptions>();
            ObjectContainer.RegisterInstance(applePayConfig);
            var aliOssConfig = cfg.GetSection("AliOssConfig").Get<OssOptions>();
            ObjectContainer.RegisterInstance(aliOssConfig);
            var ShanYanSetting = cfg.GetSection("ShanYanSetting").Get<ShanYanSetting>();
            ObjectContainer.RegisterInstance(ShanYanSetting);
        }
    }
}
