﻿using Hx.Logging;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace KhXt.YcRs.TestCase
{
    public class TestDemo : BaseTest
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void DateToStringTest(int value)
        {
            value.ShouldBeGreaterThan(0);
        }

        [Fact]
        public void LoggerShouldNotBeNull()
        {
            var logger = Resolve<ILogger>();
            Assert.NotNull(logger);

        }
    }
}
