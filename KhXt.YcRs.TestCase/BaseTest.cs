﻿using KhXt.YcRs.Domain;
using HuanLiApp.TestCase;
using Hx;
using Hx.Components;
using Hx.Configurations.Application;
using Hx.Domain.Services;
using Hx.Logging;
using Hx.Serializing;

namespace KhXt.YcRs.TestCase
{
    public abstract class BaseTest
    {
        private ILogger _logger;

        protected HxAppSettings AppSetting { get; set; }

        protected string NodeName => AppSetting.NodeName;

        /// <summary>
        /// 日志工厂
        /// </summary>
        protected ILoggerFactory LoggerFactory { get; set; }

        /// <summary>
        /// guid生成器
        /// </summary>
        protected IGuidGenerator GuidGenerator { get; set; }

        /// <summary>
        /// Uow服务调用
        /// </summary>
        protected IUnitOfWorkService UnitOfWorkService { get; set; }


        /// <summary>
        /// 序列化工具
        /// </summary>
        protected ISerializer Serializer { get; set; }

        /// <summary>
        /// 日志
        /// </summary>
        //protected ILogger Logger => _logger ?? (_logger = LoggerFactory.Create(GetType()));
        protected ILogger Logger => _logger ?? (_logger = LoggerFactory.Create());

        private Hx.Redis.RedisHelper _redis;

        protected Hx.Redis.RedisHelper Redis
        {
            get
            {
                if (_redis != null) return _redis;
                _redis = ObjectContainer.Resolve<Hx.Redis.RedisHelper>();
                return _redis;
            }
        }

        private DomainSetting _domainSetting;
        protected DomainSetting DomainSetting
        {
            get
            {
                if (_domainSetting != null) return _domainSetting;
                _domainSetting = ObjectContainer.Resolve<DomainSetting>();
                return _domainSetting;
            }
        }
        protected BaseTest()
        {
            Hx.BootStrapHelper.Boot<XunitModule>();
        }

        protected T Resolve<T>() where T : class
        {
            return ObjectContainer.Resolve<T>();
        }
    }
}
