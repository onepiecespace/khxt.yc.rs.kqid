﻿using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.User;
using Hx.ObjectMapping;
using System;
using Xunit;

namespace KhXt.YcRs.TestCase.Services.Order
{
    public class CourseOrderServiceTest : BaseTest
    {
        /// <summary>
        /// order_info
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task CreateDeteleOrderInfo()
        {
            var us = Resolve<IUserService>();
            var orderinfo = Resolve<IOrderInfoRepository>();
            var user = await us.GetUserByPhone("16811223344");
            var userid = user.Userid;
            //创建订单();
            OrderInfoModel OrderInfo = new OrderInfoModel();
            OrderInfo.ProductId = "chuangxinzuowena";
            OrderInfo.Payment = "0.00";
            OrderInfo.PaymentType = 4;//或者微信
            OrderInfo.PostFee = "0";
            OrderInfo.ShippingName = "";
            OrderInfo.ShippingCode = "";
            OrderInfo.IsFrom = 0;
            OrderInfo.BusinessType = 16;
            var orderInfoEntity = OrderInfo.MapTo<OrderInfoEntity>();
            orderInfoEntity.Id = "2003291936";
            orderInfoEntity.UserId = userid;
            orderInfoEntity.PaymentTime = DateTime.Now;
            orderInfoEntity.Status = 5;
            orderInfoEntity.PaymentNo = "单元测试";
            orderInfoEntity.CreateTime = DateTime.Now;
            orderInfoEntity.UpdateTime = DateTime.Now;
            //执行add
            //断言返回一条数据，则为真

            var body = UnitOfWorkService.Execute<string>(() =>
                {
                    return orderinfo.InsertAndGetId(orderInfoEntity);
                });
            Assert.True(Convert.ToInt32(body) > 0);
        }

        /// <summary>
        /// a_Course_Order
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task CreateDeteleOrder()
        {
            var us = Resolve<IUserService>();
            var order = Resolve<ICourseOrderService>();

            var user = await us.GetUserByPhone("16811223344");
            var userid = user.Userid;
            //创建订单();
            var courseOrder = order.Create(new CourseOrderEntity
            {
                OrderNo = "2003291936",
                UserId = userid,
                PayPrice = Convert.ToDecimal("0"),
                OrderState = 1,
                IsPay = 1,
                PayTime = DateTime.Now,
                PayType = 1,
                PayNo = "2003291936",
                CourseId = 93,
                IsSend = 0,
                SendTime = DateTime.Now,
                IsGet = 0,
                GetTime = DateTime.Now,
                PackageNo = "",
                PackageType = 0,
                OrderDes = "线下购买课程",
                AddressId = 0,
                IsDelete = 0,
                CreateId = 0,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now,
                TenantId = 0,
                IsInvoice = 0,
                InvoiceDes = ""
            });
            //执行add
            //断言返回一条数据，则为真
            Assert.True(Convert.ToInt32(courseOrder) > 0);
        }

        /// <summary>
        /// order_pay
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task CreateDeteleOrderPay()
        {
            var us = Resolve<IUserService>();
            var user = await us.GetUserByPhone("16811223344");
            var userid = user.Userid;
            //创建订单();
            OrderInfoModel OrderInfo = new OrderInfoModel();
            OrderInfo.ProductId = "chuangxinzuowena";
            OrderInfo.Payment = "0.00";
            OrderInfo.PaymentType = 4;//或者微信
            OrderInfo.PostFee = "0";
            OrderInfo.ShippingName = "";
            OrderInfo.ShippingCode = "";
            OrderInfo.IsFrom = 0;
            OrderInfo.BusinessType = 16;
            var orderInfoEntity = OrderInfo.MapTo<OrderInfoEntity>();
            orderInfoEntity.Id = "2003291936";
            orderInfoEntity.UserId = userid;
            orderInfoEntity.PaymentTime = DateTime.Now;
            orderInfoEntity.Status = 5;
            orderInfoEntity.PaymentNo = "单元测试";
            orderInfoEntity.CreateTime = DateTime.Now;
            orderInfoEntity.UpdateTime = DateTime.Now;
            OrderPayEntity orderPay = orderInfoEntity.MapTo<OrderPayEntity>();
            orderPay.UserId = userid;
            orderPay.UpdateTime = DateTime.Now;
            orderPay.CreateTime = DateTime.Now;
            orderPay.Transaction = "线下支付";
            orderPay.IsTest = false;
            orderPay.Aid = "college";
            var os = Resolve<IOrderInfoService>();
            var upay = os.Add(orderPay);
            //断言返回一条数据，则为真
            Assert.True(upay > 0);
        }

        /// <summary>
        /// a_Course_Buy_Collect
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task CreateDeteleOrderbuy()
        {
            var us = Resolve<IUserService>();
            var user = await us.GetUserByPhone("16811223344");
            var userid = user.Userid;
            var by = Resolve<ICourseBuyCollectService>();
            var rlt = by.Add(new CourseBuyCollectEntity
            {
                UserId = Convert.ToInt32(userid),
                CourseId = 93,
                CourseOrderId = "2003291936",
                CreateId = 0,
                CreateTime = DateTime.Now,
                IsBuy = 1,
                ExpirationDate = DateTime.Now.AddYears(1),
                IsHide = 0,
                IsCollect = 0,
            });
            Assert.True(rlt > 0);
        }
    }
}
