﻿using BaiduBce;
using BaiduBce.Auth;
using BaiduBce.Services.Bos;
using BaiduBce.Services.Bos.Model;
using KhXt.YcRs.Domain;
using System;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace KhXt.YcRs.TestCase.Services
{
    public class BaiduBceClientTest : BaseTest
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task BosClientBaseSample()
        {
            BosClient client = GenerateBosClient();
            const string bucketName = "lianggehuangli";    //指定Bucket名称
            const string objectKey = "liyin2020";     //指定object名称

            //创建Bucket
            client.CreateBucket(bucketName);

            //上传ObjectE:\tmp\lrc
            FileInfo file = new FileInfo("E:\\tmp\\lrc\\sample.txt");     //指定上传文件的路径
            PutObjectResponse putObjectFromFileResponse = client.PutObject(bucketName, objectKey, file);
            Console.WriteLine(putObjectFromFileResponse.ETAG);
            //查看Object
            ListObjectsResponse listObjectsResponse = client.ListObjects(bucketName);
            foreach (BosObjectSummary objectSummary in listObjectsResponse.Contents)
            {
                Console.WriteLine("ObjectKey: " + objectSummary.Key);
            }
            // 获取Object
            BosObject bosObject = client.GetObject(bucketName, objectKey);
            // 获取ObjectMeta
            ObjectMetadata meta = bosObject.ObjectMetadata;
            // 获取Object的输入流
            Stream objectContent = bosObject.ObjectContent;
            // 处理Object
            FileStream fileStream = new FileInfo("E:\\tmp\\lrc\\sampleout.txt").OpenWrite();      //指定下载文件的目录/文件名
            byte[] buffer = new byte[2048];
            int count = 0;
            while ((count = objectContent.Read(buffer, 0, buffer.Length)) > 0)
            {
                fileStream.Write(buffer, 0, count);
            }

            // 关闭流
            objectContent.Close();
            fileStream.Close();
            Console.WriteLine(meta.ETag);
            Console.WriteLine(meta.ContentLength);


        }
        private static BosClient GenerateBosClient()
        {
            const string accessKeyId = "4b84f9b344aa4d27b43d2b84b60a14cd"; // 您的Access Key ID
            const string secretAccessKey = "db1792437b3a454c8883d43490371d72"; // 您的Secret Access Key
            const string endpoint = "http://lianggehuangli.bj.bcebos.com";        //指定Bucket所在区域域名

            // 初始化一个BosClient
            BceClientConfiguration config = new BceClientConfiguration();
            config.Credentials = new DefaultBceCredentials(accessKeyId, secretAccessKey);
            config.Endpoint = endpoint;
            // 设置HTTP最大连接数为10
            config.ConnectionLimit = 10;

            // 设置TCP连接超时为5000毫秒
            config.TimeoutInMillis = 5000;

            // 设置读写数据超时的时间为50000毫秒
            config.ReadWriteTimeoutInMillis = 50000;
            return new BosClient(config);
        }
    }
}
