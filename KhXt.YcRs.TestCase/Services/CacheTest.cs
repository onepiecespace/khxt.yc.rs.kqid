﻿using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Sys;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace KhXt.YcRs.TestCase.Services
{
    public class CacheTest : BaseTest
    {
        [Fact]
        public void Do()
        {
            var svr = Resolve<ITestService>();
            var sysSvr = Resolve<ISysService>();
            var persons = svr.GetPersons(out var isFromCache);
            Assert.False(isFromCache, "首次访问,此处会创建,isFromCache:false");

            persons = svr.GetPersons(out isFromCache);
            Assert.True(isFromCache, "非首次访问,isFromCache:true");

            sysSvr.ClearServiceCache(typeof(ITestService)).GetAwaiter().GetResult();
            persons = svr.GetPersons(out isFromCache);
            Assert.False(isFromCache, "清除缓存,此处会再创建,isFromCache:false");

            Thread.Sleep(11 * 1000);

            persons = svr.GetPersons(out isFromCache);
            Assert.False(isFromCache, "超时自动清除,此处会再创建,isFromCache:false");
        }
    }
}
