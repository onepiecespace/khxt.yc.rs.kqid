﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Sys;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx.Extensions;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace KhXt.YcRs.TestCase.Services.User
{
    public class UserTest : BaseTest
    {
        [Fact]
        public async System.Threading.Tasks.Task GetAllUserAsync()
        {
            var us = Resolve<IUserService>();
            var user = await us.GetUserByPhone("18611729367");
            Assert.True(user != null && user.Username.Equals("紫渊"));
        }

        [Fact]
        public void CreateDeleteUser()
        {
            var us = Resolve<IUserService>();
            var nid = us.Create(new UserBaseInfo()
            {
                Phone = "16811223344",
                LoginType = 1,
                AccountType = (int)AccountType.Tmp,
                Creatime = DateTime.Now,
                Username = "16811223344",
                Sex = 1,
                Provincial = "",
                Grade = 1,
                City = "",
                Address = "",
                Areas = "",
                Headurl = "",
                Wx = "",
                Qq = "",
                IsFirstInputAddress = 1,
            });
            //Assert.True(nid.Result.Code);

            //Assert.True(us.Delete(nid));

            //Assert.True(us.Zap(nid));
        }

        [Fact]
        public async System.Threading.Tasks.Task VerificationMSg()
        {
            var us = Resolve<ISmsService>();
            var sendRlt = await us.SendValidCode("16811223344", "college");
            Assert.True(sendRlt.Data == "123456");
        }

        /// <summary>
        /// 验证第一步(注册用户存在)
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task VerificationFirstbing()
        {
            var us = Resolve<IUserService>();
            LoginWith2faModel model = new LoginWith2faModel();
            //填充值
            model.NickName = "擡頭、笑看人生";
            model.LogingType = LoginType.Qq;
            model.OpenId = "5F858C310819BE3098D18B7ACBF2BA55";
            model.UnionId = "UID_4368CB9206F159B46F34517E9D6F6B0F";
            model.Avatar = "http://thirdqq.qlogo.cn/g?b=oidb&k=BDyNIWJf1WgXxhZzIiaQFXA&s=100&t=1556113444";
            var rlt = us.LoginWith2fa(model);
            //断言
            Assert.True(rlt.Result.Message == "修改成功");

        }

        /// <summary>
        /// 验证第一步第二步(newthree,newphone)
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task VerificationFirstNobind()
        {
            //删除数据库
            //清除缓存
            //清redis
            var sy = Resolve<ISysService>();
            var us = Resolve<IUserService>();
            var sms = Resolve<ISmsService>();

            var user = await us.GetUserByPhone("16811223344");
            //var rltuserlist = us.GetAll().Result;
            //var rltuser = rltuserlist.Where(p => p.Phone == "16811223344");
            if (user != null)
            {
                await us.Delete(user.Userid);//这个不对，new一个删除，从数据库删除
                await sy.NoticeClearCache("", 680);
                Redis.KeyDelete("token:16811223344");
            }

            LoginWith2faModel model = new LoginWith2faModel();
            //填充值
            model.NickName = "独家记忆@青";
            model.LogingType = LoginType.Wx;
            model.OpenId = "o7s5Mv-E1Tx0Nz6mtpMH1uRhoIh8";
            model.UnionId = "ojP5vwuFoSpOE4Ik4RfFWQDsdx5A";
            model.Avatar = "http://thirdwx.qlogo.cn/mmopen/vi_32/8m54rk6T33iapwOJl0TIhml6Mnk8qibt5Jdo697Xj7tOK00CqCjzdcZwnHicdfLuvIPRMjJm56L49vAv0FhkUrEqA/132";
            var rlt = us.LoginWith2fa(model);
            Redis.StringSetWithType($"topenid_{model.UnionId}", model, TimeSpan.FromSeconds(60 * 5 * 24));
            //断言
            Assert.True(rlt.Result.Message == "修改成功");
            var sendRlt = await sms.SendValidCode("16811223344", "college");
            Assert.True(sendRlt.Data == "123456");
            Redis.StringSet(RedisKeyHelper.CreateSmsKey("16811223344"), "123456", TimeSpan.FromSeconds(60 * 5));
            //填充值
            LoginWith2faBindModel para = new LoginWith2faBindModel();
            para.UnionId = "";
            para.Phone = "";
            para.ValidCode = "123456";
            var serlt = us.LoginWith2faBind(para);
            Assert.True(serlt.Result.Code == 200 && serlt.Result.Data != "");
        }

        /// <summary>
        /// 验证第一步第二步(newthree,oldphone)
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task Verificationbind()
        {

            var us = Resolve<IUserService>();
            var sms = Resolve<ISmsService>();
            LoginWith2faModel model = new LoginWith2faModel();
            //填充值
            model.NickName = "暧妳";
            model.LogingType = LoginType.Wx;
            model.OpenId = "o7s5MvyYbDzbGRFfuxGrqU2B1ANI";
            model.UnionId = "ojP5vwo92sXAzHjIpIKCgQDnfAkc";
            model.Avatar = "http://thirdwx.qlogo.cn/mmopen/vi_32/8m54rk6T33iapwOJl0TIhml6Mnk8qibt5Jdo697Xj7tOK00CqCjzdcZwnHicdfLuvIPRMjJm56L49vAv0FhkUrEqA/132";
            var rlt = us.LoginWith2fa(model);
            Redis.StringSetWithType($"topenid_{model.UnionId}", model, TimeSpan.FromSeconds(60 * 5 * 24));
            //断言
            Assert.True(rlt.Result.Message == "修改成功");
            var sendRlt = await sms.SendValidCode("16811223344", "college");
            Assert.True(sendRlt.Data == "123456");
            Redis.StringSet(RedisKeyHelper.CreateSmsKey("16811223344"), "123456", TimeSpan.FromSeconds(60 * 5));
            //填充值
            LoginWith2faBindModel para = new LoginWith2faBindModel();
            para.UnionId = "";
            para.Phone = "";
            para.ValidCode = "123456";
            var serlt = us.LoginWith2faBind(para);
            Assert.True(serlt.Result.Code == 411);
        }
    }
}
