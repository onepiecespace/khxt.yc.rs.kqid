﻿using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Services.Course;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace KhXt.YcRs.TestCase.Services.Course
{
    public class CourseServiceTest : BaseTest
    {
        [Fact]
        public void GetWithChildModelsAsyncTest()
        {
            var svr = Resolve<ICourseService>();
            var models = svr.GetWithChildModelsAsync().ConfigureAwait(false).GetAwaiter().GetResult();
            Assert.NotNull(models);
            Assert.True(models.Count() > 0);
        }


        /// <summary>
        /// 文言文测试(0分无推荐，>0有推荐)
        /// </summary>
        [Fact]
        public async System.Threading.Tasks.Task Postitembank()
        {

            var courseService = Resolve<ICourseService>();
            var recommend = await courseService.GetCcRecommCourse();
            List<CourseThinModel> courses = null;
            foreach (var item in recommend)
            {
                if (item.Key > 0)
                {
                    courses = item.Value;
                    break;
                }
            }
            if (courses == null)
            {
                Assert.True(courses == null);
            }

            else
            {
                Assert.True(courses != null);
            }
        }
    }
}
