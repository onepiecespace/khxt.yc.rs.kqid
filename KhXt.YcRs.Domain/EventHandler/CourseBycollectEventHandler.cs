﻿
using Hx.Components;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.EventHandler
{
    /// <summary>
    /// 
    /// </summary>
    public class CourseBycollectEventHandler : IEventHandler<CourseBycollectEventData>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventData"></param>
        public void HandleEvent(CourseBycollectEventData eventData)
        {
            var svr = ObjectContainer.Resolve<ICourseBuyCollectService>();
            svr.DoSync(eventData.EventSource.ToString(), eventData);
        }
    }
}
