﻿using Hx.Components;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.Services.Sys;

namespace KhXt.YcRs.Domain.EventHandler
{
    /// <summary>
    /// 
    /// </summary>
    public class VersionEventHandler : IEventHandler<VersionEventData>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventData"></param>
        public void HandleEvent(VersionEventData eventData)
        {
            var svr = ObjectContainer.Resolve<IVersionService>();
            svr.DoSync(eventData.EventSource.ToString(), eventData);
        }
    }
}
