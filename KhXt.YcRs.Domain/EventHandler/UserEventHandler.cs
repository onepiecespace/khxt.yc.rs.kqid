﻿using Hx.Components;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.Services.User;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.EventHandler
{
    /// <summary>
    /// 
    /// </summary>
    public class UserEventHandler : IEventHandler<UserEventData>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventData"></param>
        public void HandleEvent(UserEventData eventData)
        {
            var svr = ObjectContainer.Resolve<IUserCacheService>();
            svr.DoSync(eventData.EventSource.ToString(), eventData);
        }
    }
}
