﻿using Hx.Components;
using Hx.Events.Bus;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Services.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KhXt.YcRs.Domain.EventHandler
{
    /// <summary>
    /// 
    /// </summary>
    public class CourseUserItemEventHandler : IEventHandler<CourseUserItemEventData>
    {
        public void HandleEvent(CourseUserItemEventData eventData)
        {
            var svr = ObjectContainer.Resolve<ICourseUserItemService>();
            svr.DoSync(eventData.EventSource.ToString(), eventData);
        }
    }
}
