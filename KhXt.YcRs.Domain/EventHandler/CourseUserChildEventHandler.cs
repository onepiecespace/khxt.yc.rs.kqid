﻿using Hx.Components;
using Hx.Events.Bus;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Services.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KhXt.YcRs.Domain.EventHandler
{
    /// <summary>
    /// 
    /// </summary>
    public class CourseUserChildEventHandler : IEventHandler<UserChildEventData>
    {
        public void HandleEvent(UserChildEventData eventData)
        {
            var svr = ObjectContainer.Resolve<IUserCourseChildProgessService>();
            svr.DoSync(eventData.EventSource.ToString(), eventData);
        }
    }
}
