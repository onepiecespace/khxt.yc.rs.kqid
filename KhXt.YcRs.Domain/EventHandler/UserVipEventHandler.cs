﻿using Hx.Components;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.EventData.VIP;
using KhXt.YcRs.Domain.Services.Vip;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.EventHandler
{
    public class UserVipEventHandler : IEventHandler<UserVipEventData>
    {
        public void HandleEvent(UserVipEventData eventData)
        {
            var svr = ObjectContainer.Resolve<IUserVipService>();
            svr.DoSync(eventData.EventSource.ToString(), eventData);
        }
    }
}
