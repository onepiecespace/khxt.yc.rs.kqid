﻿using Hx.Components;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.Services.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.EventHandler
{
    /// <summary>
    /// UserLevelEventHandler
    /// </summary>
    public class UserLevelEventHandler : IEventHandler<UserLevelEventData>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventData"></param>
        public void HandleEvent(UserLevelEventData eventData)
        {
            var svr = ObjectContainer.Resolve<IUserLevelCacheService>();
            svr.DoSync(eventData.EventSource.ToString(), eventData);
        }
    }
}
