﻿using Hx.Components;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Sys;
using KhXt.YcRs.Domain.Services.User;

namespace KhXt.YcRs.Domain.EventHandler
{
    public class DomainEventHandler : IEventHandler<DomainEventData>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventData"></param>
        public void HandleEvent(DomainEventData eventData)
        {
            var method = eventData.Method / 100;
            if (Consts.ServiceDictionary.TryGetValue(method, out var svrType))
            {
                var svr = ObjectContainer.Resolve(svrType) as IService;
                if (svr != null)
                {
                    svr.DoSync(eventData.EventSource.ToString(), eventData);
                }
            }
        }
    }
}
