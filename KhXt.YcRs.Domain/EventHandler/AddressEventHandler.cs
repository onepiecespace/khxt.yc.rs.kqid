﻿using Hx.Components;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Sys;
using KhXt.YcRs.Domain.Services.User;

namespace KhXt.YcRs.Domain.EventHandler
{
    /// <summary>
    /// 
    /// </summary>
    public class AddressEventHandler : IEventHandler<AddressEventData>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventData"></param>
        public void HandleEvent(AddressEventData eventData)
        {
            var svr = ObjectContainer.Resolve<IAddressService>();
            svr.DoSync(eventData.EventSource.ToString(), eventData);
        }
    }

}
