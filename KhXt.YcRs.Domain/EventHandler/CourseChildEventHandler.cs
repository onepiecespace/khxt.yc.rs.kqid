﻿
using Hx.Components;
using Hx.Events.Bus.Handlers;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.EventHandler
{
    /// <summary>
    /// 
    /// </summary>
    public class CourseChildEventHandler : IEventHandler<CourseChildEventData>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventData"></param>
        public void HandleEvent(CourseChildEventData eventData)
        {
            var svr = ObjectContainer.Resolve<ICourseChildService>();
            svr.DoSync(eventData.EventSource.ToString(), eventData);
        }
    }
}
