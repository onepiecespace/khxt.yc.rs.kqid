﻿
using Hx.Components;
using Hx.Events.Bus.Handlers;
using Hx.Extensions;
using KhXt.YcRs.Domain.EventData.Sys;
using KhXt.YcRs.Domain.Services.Sys;

namespace KhXt.YcRs.Domain.EventHandler
{
    public class ClearCacheEventHandler : IEventHandler<ClearCacheEventData>
    {
        public void HandleEvent(ClearCacheEventData eventData)
        {
            //var nodeName = eventData.EventSource?.ToString();
            var nodeName = eventData.NodeName;
            if (nodeName.IsNullOrEmpty() || nodeName.Equals(DomainEnv.NodeName, System.StringComparison.OrdinalIgnoreCase))
            {
                var svr = ObjectContainer.Resolve<ISysService>();
                if (eventData.ServiceType == null)
                {
                    svr.ClearAllServiceCache().ConfigureAwait(false).GetAwaiter().GetResult();
                }
                else
                {
                    svr.ClearServiceCache(eventData.ServiceType).ConfigureAwait(false).GetAwaiter().GetResult();
                }
            }
        }
    }
}
