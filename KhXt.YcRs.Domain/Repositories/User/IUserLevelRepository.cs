﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Repositories.User
{
    public interface IUserLevelRepository : IRepository<UserLeveEntity>
    {

    }
}
