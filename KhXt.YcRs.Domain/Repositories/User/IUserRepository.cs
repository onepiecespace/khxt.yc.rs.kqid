﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Repositories.User
{
    public interface IUserRepository : IRepository<UserEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        //UserEntity GetByPhone(string phone);

        List<UserInfoOfMsg> GetListOfMsg(string datetime);
    }
}
