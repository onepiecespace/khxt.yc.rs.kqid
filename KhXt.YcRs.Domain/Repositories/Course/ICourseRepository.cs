﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.Repositories.Course
{
    public interface ICourseRepository : IRepository<CourseEntity>
    {
        IEnumerable<OrderListOfCMS> GetOrderList();

        IEnumerable<CourseCMS> GetCourseChildList(Expression<Func<CourseCMS, bool>> predicate);

        IQueryable<CourseCMS> Table();
        IEnumerable<UserliveModel> GetLiveUserList(int CourseId);

    }


}
