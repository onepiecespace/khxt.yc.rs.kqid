﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.MongoLogEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Repositories.Course
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICourseLogRepository : IRepository<CourseLogEntity, string>
    {
    }

    public interface IOperationLogRepository : IRepository<OperationLogEntity, string>
    {
    }
}
