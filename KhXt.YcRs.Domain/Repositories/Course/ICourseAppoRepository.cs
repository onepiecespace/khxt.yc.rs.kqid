﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.Course;

namespace KhXt.YcRs.Domain.Repositories.Course
{
    public interface ICourseAppoRepository : IRepository<CourseAppointmentEntity>
    {

    }


}
