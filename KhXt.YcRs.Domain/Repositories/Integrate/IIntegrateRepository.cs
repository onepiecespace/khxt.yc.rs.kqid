﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.Integrate;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Repositories.Integrate
{
    public interface IIntegrateRepository : IRepository<UserIntegrateEntity, long>
    {

    }
}
