﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.Sys;

namespace KhXt.YcRs.Domain.Repositories.Sys
{
    public interface ISmsRepository : IRepository<SmsEntity> { }
}
