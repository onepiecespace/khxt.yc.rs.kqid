﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.Sys;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Repositories.Sys
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysLogRepository : IRepository<SysLogEntity, string>
    {
    }
}
