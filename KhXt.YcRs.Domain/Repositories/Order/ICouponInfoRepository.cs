﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.Order;

namespace KhXt.YcRs.Domain.Repositories.Order
{
    public interface ICouponInfoRepository : IRepository<CouponInfoEntity>
    {

    }
}
