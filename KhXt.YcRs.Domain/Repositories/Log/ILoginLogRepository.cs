﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.Log;

namespace KhXt.YcRs.Domain.Repositories.Log
{
    public interface ILoginLogRepository : IRepository<LoginLogEntity, string>
    {
    }
}
