﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Repositories
{
    public interface ITestRepository : IRepository<TestEntity>
    {

        /// <summary>
        /// 基本crud外的特殊需求，需要手工拼写sql示例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<string> GetBody(long id);
    }

    public interface IMongoTestRepository : IRepository<PersonEntity, string>
    { }

}
