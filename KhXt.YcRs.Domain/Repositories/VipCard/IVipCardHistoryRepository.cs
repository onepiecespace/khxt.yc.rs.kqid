﻿using Hx.Domain.Repositories;
using KhXt.YcRs.Domain.Entities.VipCard;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Repositories.VipCard
{
    public interface IVipCardHistoryRepository : IRepository<VipCardHistoryEntity, string>
    {

    }
}
