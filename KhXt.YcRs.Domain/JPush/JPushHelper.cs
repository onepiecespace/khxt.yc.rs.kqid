﻿using Jiguang.JPush;
using Jiguang.JPush.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 极光推送接口V3请求方法
    /// </summary>
    public class JPushHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pushPayload"></param>
        /// <param name="sysitting"></param>
        /// <returns></returns>
        public static HttpResponse SendPush(PushPayload pushPayload, JPushSetting sysitting)
        {
            JPushClient client = new JPushClient(sysitting.APP_KEY, sysitting.MASTER_SECRET);
            var response = client.SendPush(pushPayload);
            return response;
        }

    }
}
