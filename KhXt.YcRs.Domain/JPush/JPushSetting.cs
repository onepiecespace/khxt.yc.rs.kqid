﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Senparc.CO2NET;
using Senparc.CO2NET.HttpUtility;
using Senparc.CO2NET.RegisterServices;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace KhXt.YcRs.Domain
{
    /// <summary>
    /// 极光推送配置
    /// </summary>
    public class JPushSetting
    {
        public string APP_KEY { get; set; }
        public string MASTER_SECRET { get; set; }
        public bool isDebug { get; set; }

    }
}
