﻿using Hx.Extensions;
using Hx.Logging;
using Hx.Serializing;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain
{
    public static class HttpClientHelper
    {
        public static HttpClient Create()
        {
            return HttpClientFactory.CreateClient("mxh");
        }

        /// <summary>
        ///  Kong server,{host:port}
        /// </summary>
        public static string Host { get; set; }

        public static ISerializer Serializer { get; set; }

        public static IHttpClientFactory HttpClientFactory { private get; set; }

        public static ILogger Logger { get; set; }

        public static async Task<string> HttpPost<TData>(string url, string token, TData data)
        {

            return await HttpPost(url, data, token.IsNullOrEmpty() ? null : new Dictionary<string, string> { { "token", token } });
        }

        public static async Task<string> HttpPost<TData>(string url, TData data, Dictionary<string, string> headers = null)
        {
            var postData = data == null ? "" : Serializer.Serialize(data);

            var client = Create();
            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }
            using (var httpContent = new StringContent(postData, Encoding.UTF8, "application/json"))
            {
                var response = await client.PostAsync(url, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }
                throw new System.Exception($"HttpPost:{url},响应错误,statuscode:{response.StatusCode}");
            }
        }
        public static async Task<TResult> HttpPost<TData, TResult>(string url, string token, TData data) where TResult : class
        {
            return await HttpPost<TData, TResult>(url, data, token.IsNullOrEmpty() ? null : new Dictionary<string, string> { { "token", token } });
        }

        public static async Task<TResult> HttpPost<TData, TResult>(string url, TData data, Dictionary<string, string> headers = null) where TResult : class
        {
            var postData = data == null ? "" : Serializer.Serialize(data);

            var client = Create();
            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }
            using (var httpContent = new StringContent(postData, Encoding.UTF8, "application/json"))
            {
                var response = await client.PostAsync(url, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    return Serializer.Deserialize<TResult>(body);
                }
                throw new System.Exception($"HttpPost:{url},响应错误,statuscode:{response.StatusCode}");
            }
        }

        public static async Task<string> HttpPostForm(string url, string token, Dictionary<string, string> data)
        {
            return await HttpPostForm(url, data, token.IsNullOrEmpty() ? null : new Dictionary<string, string> { { "token", token } });
        }

        public static async Task<string> HttpPostForm(string url, Dictionary<string, string> data, Dictionary<string, string> headers = null)
        {
            var client = Create();
            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }
            using (var httpContent = new FormUrlEncodedContent(data))
            {
                var response = await client.PostAsync(url, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    return body;
                }
                throw new System.Exception($"HttpPostForm:{url},响应错误,statuscode:{response.StatusCode}");
            }
        }

        public static async Task<TResult> HttpPostForm<TResult>(string url, string token, Dictionary<string, string> data) where TResult : class
        {
            return await HttpPostForm<TResult>(url, data, token.IsNullOrEmpty() ? null : new Dictionary<string, string> { { "token", token } });
        }

        public static async Task<TResult> HttpPostForm<TResult>(string url, Dictionary<string, string> data, Dictionary<string, string> headers = null) where TResult : class
        {
            var client = Create();
            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }
            using (var httpContent = new FormUrlEncodedContent(data))
            {
                var response = await client.PostAsync(url, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    return Serializer.Deserialize<TResult>(body);
                }
                throw new System.Exception($"HttpPostForm:{url},响应错误,statuscode:{response.StatusCode}");
            }
        }

        public static async Task<string> HttpGet(string url, string token)
        {
            return await HttpGet(url, token.IsNullOrEmpty() ? null : new Dictionary<string, string> { { "token", token } });
        }
        public static async Task<string> HttpGet(string url, Dictionary<string, string> headers = null)
        {
            var client = Create();

            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }
            var response = await client.GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                throw new System.Exception($"httpGet error:{url},{response.StatusCode}");
            }
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<T> HttpGet<T>(string url, string token) where T : class
        {
            return await HttpGet<T>(url, token.IsNullOrEmpty() ? null : new Dictionary<string, string> { { "token", token } });
        }

        public static async Task<T> HttpGet<T>(string url, Dictionary<string, string> headers = null) where T : class
        {
            var client = Create();

            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }
            var response = await client.GetAsync(url);
            var body = await response.Content.ReadAsStringAsync();

            var o = Serializer.Deserialize<T>(body);
            return o;
        }


        public static async Task<bool> HttpDelete(string url, Dictionary<string, string> headers = null)
        {
            var client = Create();

            if (headers != null)
            {
                foreach (var header in headers)
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }

            var response = await client.DeleteAsync(url);

            if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                return true;
            }
            var result = await response.Content.ReadAsStringAsync();
            Logger.Error($"HttpDelete({url} fail,responseText:{result}");
            return false;
        }
    }
}
