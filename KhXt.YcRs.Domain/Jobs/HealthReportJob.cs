﻿using Hx.Extensions;
using Hx.QuartzCore;
using Hx.Redis;
using Quartz;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Jobs
{
    internal class HealthReportJob : JobBase
    {
        private RedisHelper _redis;
        public HealthReportJob(RedisHelper redisHelper)
        {
            _redis = redisHelper;
        }
        public override async Task Execute(IJobExecutionContext context)
        {
            var nodeName = DomainEnv.NodeName;
            var key = RedisKeyHelper.NODE_STATUS;
            await _redis.HashSetAsync(key, nodeName, DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString());
        }
    }
}
