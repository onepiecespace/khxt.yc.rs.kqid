﻿using System;
using System.ComponentModel;

namespace KhXt.YcRs.Domain
{
    /// <summary>
    /// 业务模块类型定义
    /// </summary>
    [Flags]
    public enum BusinessType
    {
        [Description("朗读")]
        Read = 1,

        [Description("听写")]
        Dictation = 2,

        [Description("PK")]
        Pk = 4,

        [Description("闯关")]
        Game = 8,

        [Description("课程")]
        Course = 16,

        [Description("任务")]
        Task = 32,

        [Description("背诵")]
        Recite = 64,

        [Description("打卡")]
        Punch = 128,

        [Description("直播")]
        Live = 256,

        [Description("录播")]
        Record = 512,

        [Description("VIP")]
        Vip = 1024,

        [Description("成语听写")]
        Idiomatic = 2048,

        [Description("活动")]
        Marketing = 4096,

        [Description("金币商城")]
        Product = 8192,

        [Description("钱包")]
        Wallet = 16384,
        [Description("全部")]
        All = Read | Dictation | Pk | Game | Course | Task | Recite | Punch | Live | Record | Vip | Idiomatic | Marketing | Product
    }



    public enum OrderPayType
    {
        //付款方式 付款方式0 免费 (FreeCollection)1线下付款(OfflinePay),2微信(WeChatPay) 3支付宝(AlipayPay) 4 Apple(ApplePay) 5银联(UnionPay)
        [Description("免费领取")]
        免费领取 = 0,

        [Description("线下付款")]
        线下支付 = 1,

        [Description("微信支付")]
        微信支付 = 2,

        [Description("支付宝")]
        支付宝支付 = 3,

        [Description("苹果支付")]
        苹果支付 = 4,

        [Description("银联支付")]
        银联支付 = 5,


    }
    /// <summary>
    /// 支付方式 支付类型 0 微信支付 1 支付宝 2 苹果支付 3 免费 4 线下 5 银联 6 金币兑换
    /// </summary>
    public enum PaymentType
    {
        /// <summary>
        /// 微信支付
        /// </summary>
        [Description("微信支付")]
        WeChatPay = 0,
        /// <summary>
        /// 支付宝
        /// </summary>
        [Description("支付宝支付")]
        AliPay = 1,
        /// <summary>
        /// 苹果支付
        /// </summary>
        [Description("苹果支付")]
        ApplePay = 2,
        /// <summary>
        /// 免费
        /// </summary>
        [Description("免费领取")]
        Free = 3,
        /// <summary>
        /// 线下
        /// </summary>
        [Description("财务线下收款")]
        Under = 4,
        /// <summary>
        /// 银联
        /// </summary>
        [Description("银联")]
        UnionPay = 5,
        /// <summary>
        /// 金币兑换
        /// </summary>
        [Description("金币兑换")]
        GoldExchange = 6,
        /// <summary>
        /// 线下微信
        /// </summary>
        [Description("财务线下微信")]
        UnderWx = 7,
        /// <summary>
        /// 线下支付宝
        /// </summary>
        [Description("财务线下支付宝")]
        UnderAliPay = 8,
        /// <summary>
        /// 内部员工免费
        /// </summary>
        [Description("内部员工免费")]
        OwnFree = 9,
        /// <summary>
        /// 校管家转入
        /// </summary>
        [Description("校管家转入")]
        XGJ = 10,
        /// <summary>
        /// 钱包支付
        /// </summary>
        [Description("钱包支付")]
        Wallet = 11,

    }

    public enum OrderStateType
    {
        //订单状态 0 进行中 1已完成 2取消交易 3已结算 4申请退款中 5已退款
        [Description("等待支付")]
        等待支付 = 0,

        [Description("支付成功")]
        支付成功 = 1,

        [Description("已取消")]
        已取消 = 2,

        [Description("已结算")]
        已结算 = 3,
        [Description("申请退款中")]
        申请退款中 = 4,
        [Description("已退款")]
        已退款 = 5
    }

    public enum CaptionType
    {
        [Description("排名")]
        Ranking = 1,

        [Description("级别")]
        Level = 2,

        [Description("总等级")]
        AllLevel = 512
    }

    [Flags]
    public enum LoginType
    {
        Phone = 1,
        Wx = 2,
        Qq = 4,
        Guest = 8,
        Bbk = 16,
        Dsl = 32,
        Apple = 64
    }

    [Flags]
    public enum PlatFormType
    {
        ios = 1,
        andriod = 2,
        web = 4,


    }


    [Flags]
    public enum AidType
    {
        [Description("马小哈")]
        mxh = 1,
        [Description("两个黄鹂")]
        huangli = 2,
        [Description("杜甫语文")]
        dufu = 4,
        [Description("黄鹂商学院")]
        college = 8,

    }

    public enum Sex
    {
        Woman = 0,
        Man = 1,
        Unknown = 2,
    }

    public enum AccountType
    {
        Normal = 1,
        Verify = 2,
        Guest = 512,
        InsideTest = 1024,
        Tmp = 8192 //压测临时用户
    }

    public enum ExpressType
    {
        [Description("无")]
        None = 0,
        [Description("EMS快递")]
        EMS = 1,
        [Description("申通")]
        ShenTong = 2,
        [Description("顺丰")]
        ShunFeng = 3,
        [Description("圆通速递")]
        YuanTong = 4,
        [Description("韵达快运")]
        YunDa = 5,
        [Description("EMS快递")]
        ZhongTong = 6,

    }


    public enum ActionType
    {
        [Description("设置一个好看的头像")]
        Xgtx = 1,
        [Description("修改自己的昵称")]
        Xgnc = 2,
        [Description("添加一张银行卡")]
        Tjyhk = 3,
        [Description("完善收货地址")]
        Wsshdz = 4,
        [Description("查看金币明细")]
        Ckjbmx = 5,
        [Description("完善个人资料")]
        Wsgrzl = 6,
        [Description("体验AI对话")]
        Tyai = 7,
        [Description("关注一个有用的公众号")]
        Gzgzh = 8,
        [Description("加入学习社群")]
        Jrsq = 9,
        [Description("收藏一门课程")]
        Sckc = 10,

        [Description("签到一次")]
        Qdyc = 11,
        [Description("首次购买一门课程")]
        Scmk = 12,
        [Description("观看课程20分钟")]
        Gkkc = 13,
        [Description("观看直播课30分钟")]
        Gkzb = 14,
        [Description("完成一次PK")]
        Wcpk = 15,
        [Description("完成一次闯关")]
        Wccg = 16,
        [Description("完成一次听写")]
        Wctx = 17,
        [Description("完成一次朗读")]
        Wcld = 18,
        [Description("错题本完成一次答题")]
        Wcctdt = 19,
        [Description("邀请好友免费学")]
        Yqhy = 20,
        [Description("报名学习一个免费课程")]
        Bmkc = 21,
        [Description("选择自己所在的年级")]
        Sznj = 22,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum LessonType
    {
        /// <summary>
        /// 单元
        /// </summary>
        [Description("单元")]
        Unit = 4,
        /// <summary>
        /// 课程
        /// </summary>
        [Description("课程")]
        Lesson = 5,
    }

    public enum TaskType
    {
        [Description("新手任务")]
        Typt = 1,
        [Description("日常任务")]
        Taskypt = 2,
    }
    /// <summary>
    /// 集成系统标识
    /// </summary>
    public enum IntegrateSign
    {
        [Description("环信")]
        HuanXin = 1,
    }
    public enum TaskReadType
    {
        [Description("关注")]
        follow = 1,
        [Description("取消关注")]
        Cancelfollow = 2,
        [Description("点赞")]
        Count = 3,
        [Description("取消点赞")]
        CancelCount = 4,

    }
    public enum TaskReadComentType
    {

        [Description("评论点赞")]
        ComentCount = 5,
        [Description("评论取消点赞")]
        ComentCancelCount = 6,

    }
    public enum TaskMyReadType
    {

        [Description("我的作品")]
        workTotal = 1,
        [Description("我的关注")]
        FollowTotal = 2,
        [Description("我的粉丝")]
        FansTotal = 3,
        [Description("我的赞数量")]
        ZanTotal = 4,

    }
    public enum TaskReadWorkType
    {

        [Description("发布作品")]
        addwork = 1,
        [Description("删除作品")]
        delwork = 2,

    }
    public enum TaskOperation
    {

        [Description("添加")]
        add = 1,
        [Description("删除")]
        del = 2,

    }
    public enum TaskGuidPage
    {

        [Description("首页")]
        index = 1,
        [Description("列表页")]
        list = 2,
        [Description("内容页")]
        detail = 3,
        [Description(" 拼写")]
        H5 = 4,
    }
    /// <summary>
    /// 合成状态
    /// </summary>
    public enum AudioSynthesis
    {
        [Description("合成中")]
        Doing = 0,
        [Description("合成成功")]
        Success = 1,
        [Description("合成失败")]
        Fail = -1,

    }
    /// <summary>
    /// 音频审核状态
    /// </summary>
    public enum AudioSynVCR
    {
        [Description("排队中")]
        PROVISIONING = 0,
        [Description("审核中")]
        PROCESSING = 1,
        [Description("审核完成")]
        FINISHED = 2,
        [Description("审核失败")]
        ERROR = -1,

    }
    public enum AudioSynAudioVCR
    {
        [Description("审核中")]
        PROCESSING = 0,
        [Description("拒绝")]
        REJECT = -1,
        [Description("正常")]
        NORMAL = 1
    }
    /// <summary>
    /// 进程类型
    /// </summary>
    [Flags]
    public enum ProcessType
    {
        [Description("FFmpeg")]
        FFmpeg = 1,

    }
    /// <summary>
    /// VIP类别
    /// </summary>
    public enum VipType
    {
        [Description("月卡")]
        Month = 1,
        [Description("半年卡")]
        HalfYear = 2,
        [Description("年卡")]
        Year = 4
    }
    /// <summary>
    /// 优惠券类别  0 立减券 1 满减券 2 折扣券 3 兑换券
    /// </summary>
    public enum CouponCategory
    {
        /// <summary>
        /// 立减券
        /// </summary>
        [Description("立减券")]
        LJQ = 0,
        /// <summary>
        /// 满减券
        /// </summary>
        [Description("满减券")]
        MJQ = 1,
        /// <summary>
        /// 折扣券
        /// </summary>
        [Description("折扣券")]
        ZKQ = 2,
        /// <summary>
        /// 兑换券
        /// </summary>
        [Description("兑换券")]
        DHQ = 3

    }
    /// <summary>
    /// 优惠券使用限制
    /// </summary>
    public enum CouponUseLimit
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("无限制")]
        No = 0,
        [Description("会员可用")]
        OnlyVip = 1,
        [Description("仅自己")]
        Own = 2,
        [Description("仅分享")]
        Share = 3,

    }
    /// <summary>
    /// 优惠券状态
    /// </summary>
    public enum CouponStatus
    {
        /// <summary>
        /// 已作废
        /// </summary>
        [Description("已作废")]
        No = -1,
        /// <summary>
        /// 未使用
        /// </summary>
        [Description("未使用")]
        NoUse = 0,
        /// <summary>
        /// 已使用
        /// </summary>
        [Description("已使用")]
        HasUsed = 1,
        /// <summary>
        /// 已过期
        /// </summary>
        [Description("已过期")]
        Expired = 2

    }
    /// <summary>
    ///  订单状态
    ///  1 未付款（未确认兑换） 2 已付款（已兑换） 3 未发货 4 已发货 5 交易成功（兑换成功） 6 交易关闭 
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// 未付款（未确认兑换）
        /// </summary>
        [Description("未付款")]
        UnPay = 1,
        /// <summary>
        /// 已付款（已兑换）
        /// </summary>
        [Description("已付款")]
        HasPay = 2,
        /// <summary>
        /// 未发货
        /// </summary>
        [Description("未发货")]
        NoShip = 3,
        /// <summary>
        /// 已发货
        /// </summary>
        [Description("已发货")]
        Shipped = 4,
        /// <summary>
        /// 交易成功 （兑换成功）
        /// </summary>
        [Description("交易成功")]
        Success = 5,
        /// <summary>
        /// 交易关闭
        /// </summary>
        [Description("交易关闭")]
        Close = 6,
        /// <summary>
        /// 交易取消
        /// </summary>
        [Description("交易取消")]
        Cancel = 7
    }

    /// <summary>
    /// 会员开通方式
    /// </summary>
    public enum VipFrom
    {
        /// <summary>
        /// 付费
        /// </summary>
        [Description("付费")]
        IsFee = 1,
        /// <summary>
        /// 会员卡兑换
        /// </summary>
        [Description("会员卡兑换")]
        IsVipCard = 2,
        /// <summary>
        /// 好友赠送
        /// </summary>
        [Description("好友赠送")]
        IsGifCard = 3,
    }
    /// <summary>
    /// 排序方式
    /// </summary>
    public enum SortKind
    {
        /// <summary>
        /// 升序
        /// </summary>
        ASC = 0,
        /// <summary>
        /// 降序
        /// </summary>
        DESC
    }
    /// <summary>
    /// 消息推送类别
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// 单推
        /// </summary>
        [Description("单推")]
        Single = 1,
        /// <summary>
        /// 群推
        /// </summary>
        [Description("群推")]
        All = 2,
        /// <summary>
        /// 组推
        /// </summary>
        [Description("组推")]
        Group = 3,

    }

    /// <summary>
    /// 活动增加类型
    /// </summary>
    public enum MarketingAddType
    {
        /// <summary>
        /// 金币
        /// </summary>
        [Description("金币")]
        Coin = 0,
        /// <summary>
        /// 经验
        /// </summary>
        [Description("经验")]
        Exp = 1,
        /// <summary>
        /// 金币和经验
        /// </summary>
        [Description("金币和经验")]
        All = 2

    }

    /// <summary>
    /// 活动状态
    /// </summary>
    public enum MarketingStatus
    {
        /// <summary>
        /// 进行中
        /// </summary>
        [Description("进行中")]
        Ing = 0,
        /// <summary>
        /// 已完成
        /// </summary>
        [Description("已完成")]
        End = 1,
        /// <summary>
        /// 未开始
        /// </summary>
        [Description("未开始")]
        NotStarted = 2,

    }
    /// <summary>
    /// 事件方法类别
    /// </summary>
    public enum EventDataMethod
    {
        /// <summary>
        /// 清空
        /// </summary>
        [Description("清空")]
        Clear = 0,
        /// <summary>
        /// 添加
        /// </summary>
        [Description("添加")]
        Add = 1,
        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")]
        Del = 2,
        /// <summary>
        /// 更新
        /// </summary>
        [Description("更新")]
        Update = 3,
        /// <summary>
        /// 批量删除
        /// </summary>
        [Description("批量删除")]
        BatchDel = 4,
        /// <summary>
        /// 批量添加
        /// </summary>
        [Description("批量添加")]
        BatchAdd = 5,
        /// <summary>
        /// 批量更新
        /// </summary>
        [Description("批量更新")]
        BatchUpdate = 6,

    }

    public enum ContentCategory
    {
        OuterHtml = 1,
        Course = 2,
        Pk = 3,
        Game = 4,
        Dication = 5,
        Task = 6,
        Ad = 7,
        Read = 8,
        Vip = 9,
        Teacher = 10, //讲师
        Web = 11
    }


    public enum DeviceType
    {
        [Description("Other")]
        Other = 0,
        [Description("IPhone")]
        IPhone = 1,
        [Description("IPad")]
        IPad = 2,
        [Description("APhone")]
        APhone = 3,
        [Description("APad")]
        APad = 4,
        [Description("Web")]
        Web = 5,
        [Description("Wap")]
        Wap = 6
    }

    public enum CategoryType
    {
        /// <summary>
        /// 
        /// </summary>
        Single = 1,
        /// <summary>
        /// 
        /// </summary>
        MuliLimit = 2,
        /// <summary>
        /// 
        /// </summary>
        Muli = 3
    }
    public enum PostType
    {
        [Description("校长")]
        校长 = 1,
        [Description("教师")]
        教师 = 2,
        [Description("顾问")]
        顾问 = 3,
        [Description("教务")]
        教务 = 4,
        [Description("市场")]
        市场 = 5
    }
    public enum ModeType
    {
        [Description("品牌加盟")]
        品牌加盟 = 1,
        [Description("直营校区")]
        直营校区 = 2,
        [Description("联营校区")]
        联营校区 = 3,
        [Description("课程合作")]
        课程合作 = 4
    }
}
