﻿using Hx.Components;
using NVelocity;
using NVelocity.App;
using NVelocity.Exception;
using NVelocity.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KhXt.YcRs.Domain.NVelocity
{
    /// <summary>
    /// 模版生成工具类
    /// </summary>
    public class NVelocityHelper
    {
        /// <summary>
        /// 
        /// </summary>
        protected VelocityContext context;
        /// <summary>
        /// 模板对象
        /// </summary>
        protected Template template;

        /// <summary>
        /// 输出模板文件目录
        /// </summary>
        public string DirectoryOfOutput { get; set; }

        /// <summary>
        /// 输出模板文件名称
        /// </summary>
        public string FileNameOfOutput { get; set; }

        /// <summary>
        /// 输出模板文件后缀名
        /// </summary>
        public string FileExtensionOfOutput { get; set; }
        /// <summary>
        /// 根目录
        /// </summary>
        private readonly string _rootPath = "";

        private DomainSetting _domainSetting;
        protected DomainSetting DomainSetting
        {
            get
            {
                if (_domainSetting != null) return DomainSetting;
                _domainSetting = ObjectContainer.Resolve<DomainSetting>();
                return _domainSetting;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="outDir">生成路径</param>
        /// <param name="outFileName">生成名称</param>
        /// <param name="outFileExt">生成后缀名</param>
        public NVelocityHelper(string outDir, string outFileName, string outFileExt)
        {
            _rootPath = DomainSetting.UploadPath;
            this.DirectoryOfOutput = outDir;
            this.FileNameOfOutput = outFileName;
            this.FileExtensionOfOutput = outFileExt;

        }

        /// <summary>
        /// 存放键值的字典内容
        /// </summary>
        private Dictionary<string, object> KeyObjDict = new Dictionary<string, object>();


        /// <summary>
        /// 添加一个键值对象
        /// </summary>
        /// <param name="key">键，不可重复</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        public NVelocityHelper AddKeyValue(string key, object value)
        {
            if (!KeyObjDict.ContainsKey(key))
            {
                KeyObjDict.Add(key, value);
            }
            return this;
        }

        /// <summary>
        /// 初始化模板引擎
        /// </summary>
        /// <param name="templateFile">template/xxx.yyy</param>
        public virtual void InitTemplateEngine(string templateFile)
        {
            try
            {
                VelocityEngine templateEngine = new VelocityEngine();
                //ExtendedProperties props = new ExtendedProperties();
                templateEngine.SetProperty(RuntimeConstants.RESOURCE_LOADER, "file");
                templateEngine.SetProperty(RuntimeConstants.INPUT_ENCODING, "utf-8");
                templateEngine.SetProperty(RuntimeConstants.OUTPUT_ENCODING, "utf-8");
                templateEngine.SetProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, _rootPath);
                //templateEngine.Init(props);
                templateEngine.Init();
                template = templateEngine.GetTemplate(templateFile);
            }
            catch (ResourceNotFoundException re)
            {
                string error = string.Format("找不到指定的{0}模板文件", templateFile);
                throw new Exception(error, re);
            }
            catch (ParseErrorException pee)
            {
                string error = string.Format("Syntax error in template " + templateFile + ":" + pee.StackTrace);
                throw new Exception(error, pee);
            }
            catch (VelocityException vee)
            {
                string error = string.Format("InitTemplateEngine初始化模板引擎错误 " + vee.StackTrace);
                throw new Exception(error, vee);
            }
        }

        /// <summary>
        /// 初始化上下文的内容
        /// </summary>
        private void InitContext()
        {
            context = new VelocityContext();
            foreach (string key in KeyObjDict.Keys)
            {
                context.Put(key, KeyObjDict[key]);
            }
        }

        /// <summary>
        ///根据模板创建输出的文件,并返回生成的文件路径
        /// </summary>
        public virtual string ExecuteFile()
        {
            string fileName = "";
            if (template != null)
            {
                string dirPath = DirectoryOfOutput;
                fileName = $"{dirPath}{FileNameOfOutput }{ FileExtensionOfOutput}";
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);
                InitContext();
                using (StreamWriter writer = new StreamWriter(fileName, false))
                {
                    template.Merge(context, writer);
                }
            }
            return fileName;
        }

        /// <summary>
        /// 根据模板输出字符串内容
        /// </summary>
        /// <returns></returns>
        public string ExecuteString()
        {
            InitContext();
            System.IO.StringWriter writer = new System.IO.StringWriter();
            template.Merge(context, writer);
            return writer.GetStringBuilder().ToString();
        }

        /// <summary>
        /// 合并字符串的内容
        /// </summary>
        /// <returns></returns>
        public string ExecuteMergeString(string inputString)
        {
            VelocityEngine templateEngine = new VelocityEngine();
            templateEngine.Init();
            InitContext();
            System.IO.StringWriter writer = new System.IO.StringWriter();
            templateEngine.Evaluate(context, writer, "mystring", inputString);
            return writer.GetStringBuilder().ToString();
        }
    }
}
