﻿using Hx.Extensions;
using System;

namespace KhXt.YcRs.Domain
{
    public class RedisKeyHelper
    {
        /*
            类型        名称前缀      
            hash        hsh_
            list        lst_
            set         set_
            sorted set  sset_
            string      s_

            cache 为缓存
         */

        //public const string RANKING_BOARD_PK = "sset_rb_pk";

        public const string GUEST_STATUS = "s_guest_status";

        public const string HASH_USER_COIN = "hsh_coin";
        /// <summary>
        /// 用户会背文章数
        /// </summary>
        public const string HASH_USER_CANRECITE = "hsh_recite_can_user";
        /// <summary>
        /// 课文已背
        /// </summary>
        public const string HASH_CANRECITE = "hsh_recite_can";
        /// <summary>
        /// 课文已读
        /// </summary>
        public const string HASH_HASRECITE = "hsh_recite_has";
        /// <summary>
        /// 背诵总排行
        /// </summary>
        public const string SSET_RANKRECITE = "sset_recite_rank";
        /// <summary>
        /// 背诵总排行Last
        /// </summary>
        public const string SSET_RANKRECITELAST = "sset_recite_rank_last";
        //public const string SSET_EXP = "sset_exp";

        public const string HASH_CC_RECOMMEND_COURSES = "hsh_cc_recommend_courses";
        public const string NODE_STATUS = "hsh_nodestatus";

        public const string HASH_SYS_SETTING = "hsh_sys_setting";

        public static string CreateTokenKey(string phone)
        {
            return $"token:{phone}";
        }

        public static string CreateDictationKey(string userId)
        {
            return $"hsh_d_s_{userId}";
        }


        #region  朗读模块key
        /// <summary>
        /// 我发布的作品统计
        /// </summary>
        /// <returns></returns>
        public static string UserMyWorkEnumKey()
        {
            return $"hsh_myworktask";
        }
        /// <summary>
        /// 我发布的作品得分是S的统计
        /// </summary>
        /// <returns></returns>
        public static string UserMyWorkSorceEnumKey()
        {
            return $"hsh_myworksorcetask";
        }
        public static string UserMyNotificationEnumKey()
        {
            return $"hsh_mynotificationtask";
        }
        /// <summary>
        /// 用户已关注与已关点赞的作品
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static string CreateUserWorkTaskKey(long userId, int action)
        {
            return $"hsh_worktask_{userId}_{action}";
        }
        /// <summary>
        ///  用户已关注与已关点赞的作品统计总数
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static string CreateWorkTaskKey(int action)
        {
            return $"hsh_worktask_{action}";
        }

        /// <summary>
        /// 作品评论任务KEY
        /// </summary>
        /// <param name="workid"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static string CreateWorkTaskKey(long workid, int action)
        {
            return $"hsh_worktask_{workid}_{action}";
        }
        public static string UserWorkEnumKey(long workid)
        {
            return $"{workid}";
        }
        public static string UserEnumKey(long userId)
        {
            return $"{userId}";
        }
        public static string CreateTaskBankLearnCount()
        {
            return "cache_taskbanklearncount";
        }
        public static string CreateTaskWorkLearnCount()
        {
            return "cache_taskworklearncount";
        }
        #endregion
        public static string CreateGuidKey(BusinessType businessType, TaskGuidPage guidpage)
        {
            return $"hsh_guid_{(int)businessType}_{(int)guidpage}";
        }
        public static string CreatePkActive()
        {
            return $"hsh_pk_current_activation_{DateTime.Today.ToString("yyMMdd")}";
        }

        /// <summary>
        /// 听写完成进度
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string CreateUserDictationProgressKey(long userId)
        {
            return $"hsh_dp_{userId}";
        }
        /// <summary>
        /// 历史听写进度
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string CreateUserHistoryDictationProgressKey(long userId)
        {
            return $"hsh_dp_{userId}";
        }

        public static string CreateCoinKey(BusinessType businessType)
        {
            return $"hsh_coin_{(int)businessType}";
        }


        /// <summary>
        /// 各模块经验信息
        /// </summary>
        /// <param name="businessType"></param>
        /// <returns></returns>
        public static string CreateExpKey(BusinessType businessType)
        {
            var key = $"sset_exp_{(int)businessType}";
            return key;
        }

        /// <summary>
        /// 各模块经验历史信息
        /// </summary>
        /// <param name="businessType"></param>
        /// <returns></returns>
        public static string CreateLastExpKey(BusinessType businessType)
        {
            var key = $"sset_exp_{(int)businessType}_last";
            return key;
        }

        /// <summary>
        ///PK胜负信息
        /// </summary>
        /// <param name="businessType"></param>
        /// <returns></returns>
        public static string CreateScoreBoardKey(BusinessType businessType)
        {
            return $"sset_score_{(int)businessType}";
        }


        public static string CreateRankingBoardKey(BusinessType businessType, int offset = 0)
        {
            var key = $"sset_rb_{(int)businessType}";
            if (offset > 0)
            {
                key = $"{key}_{DateTime.Now.AddDays(offset * -1).ToString8()}";
            }
            return key;
        }


        /// <summary>
        ///题目难度定义,按年级返回题目难度
        /// </summary>
        /// <returns></returns>
        public static string CreateItemDifficultyKey()
        {
            return $"sset_item_difficulty";
        }


        /// <summary>
        ///题目难度定义,按年级返回题目难度
        /// </summary>
        /// <param name="grade">年级</param>
        /// <returns></returns>
        public static string GetItemsByGrade(int grade)
        {
            return null;
        }


        public static string CreateMobileMonitorLogKey(string phone)
        {
            return $"mobile_{phone}";
        }

        public static string CreateSmsKey(string phone)
        {
            return $"SMS:{phone}";
        }
        public const string White_List = "whitelist";
        public const string White_CourseList = "whiteLimitCourselist";
        public const string ShiYong_CourseList = "shiYongLimitCourselist";
        public const string SMS_SPEED_LIMIT = "s_sms_speed_limit";

        public const string SMS_SPEED_LIMIT_IP = "s_sms_speed_limit_ip";
        /// <summary>
        /// 任务KEY
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string CreateUserTaskKey(long userId)
        {
            return $"hsh_task_{userId}_{DateTime.Today.ToString("yyMMdd")}";
        }


        /// <summary>
        /// 任务完成情况Key
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="taskType"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public static string CreateUserTaskIsfinishKey(long userId, int taskType, int taskId)
        {
            return $"hsh_task_{userId}_{DateTime.Today.ToString("yyMMdd")}_" + taskType + "_" + taskId;
        }

        /// <summary>
        /// 宝箱状态
        /// </summary>
        /// <param name="boxLocation"></param>
        /// <returns></returns>
        public static string CreateBoxState(int boxLocation)
        {
            return $"hsh_task_box_" + boxLocation;
        }

        /// <summary>
        /// 宝箱
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string CreateBox(long userId)
        {
            return $"hsh_taskboxnum_{userId}_{DateTime.Today.ToString("yyMMdd")}";

        }

        /// <summary>
        /// 活跃度
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string CreateActivity(long userId)
        {
            return $"hsh_task_activite_{userId}_{DateTime.Today.ToString("yyMMdd")}";

        }
        /// <summary>
        /// 创建活动及宝箱状态缓存
        /// </summary>
        /// <returns></returns>
        public static string CreateTaskActiveAndState(long userId)
        {
            return $"hsh_task_activation_state_{userId}_{DateTime.Today.ToString("yyMMdd")}";

        }

        public static string CreateAudioTaskKey(BusinessType businessType)
        {
            var key = $"hsh_audio_task_{(int)businessType}";
            return key;
        }
        public static string CreateAudioVCRTaskKey(BusinessType businessType)
        {
            var key = $"hsh_audioVCR_task_{(int)businessType}";
            return key;
        }
        #region 背诵相关
        public static string CreateReciteRankKey(string type)
        {
            var key = $"sset_recite_rank_{type}";
            return key;
        }
        public static string CreateReciteRankLastKey(string type)
        {
            var key = $"sset_recite_rank_{type}_last";
            return key;
        }
        /// <summary>
        ///  年级
        /// </summary>
        /// <param name="keyValue">version gradeId term </param>
        /// <returns></returns>
        public static string CreateReciteUserCanKey(string keyValue)
        {
            var key = $"sset_recite_user_can_{keyValue}";
            return key;
        }
        /// <summary>
        /// 创建文件对比任务
        /// </summary>
        /// <returns></returns>
        public static string CreateReciteTxCTask()
        {
            var key = $"hsh_recite_txc_task";
            return key;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string CreateReciteResultTask()
        {
            var key = $"hsh_recite_result";
            return key;
        }
        #endregion

        public static string CreateTaskInfo()
        {
            return "cache_taskinfo";
        }
        public static string CreateTaskBankInfo()
        {
            return "cache_taskbankinfo";
        }
        public static string CreateTaskCourseChildInfo()
        {
            return "cache_coursechildinfo";
        }
        public static string CreateTaskCourseItemInfo()
        {
            return "cache_courseIteminfo";
        }
        public static string CreateTaskTenCourseInfo(long tenantid)
        {
            return $"cache_tencourse_{tenantid}";
        }
        public static string CreateTaskWorkInfo()
        {
            return "cache_taskworkinfo";
        }

        /// <summary>
        /// 订单状态
        /// </summary>
        /// <returns></returns>
        public static string CreateOrderStatus()
        {

            var key = $"hsh_order_status";
            return key;
        }
        #region  课程相关key
        public static string CourseIndexCount()
        {
            return "cache_courseIndexcount";
        }
        public static string CreateTaskCourseLerunCount()
        {
            return "cache_taskcourseleruncount";
        }
        public static string CreateTaskCourseChidLerunCount()
        {
            return "cache_taskcoursechidleruncount";
        }

        public static string CreateChildLerunCountKey(long courseid, long courseChildId)
        {
            return $"hsh_childLerun_{courseid}_{courseChildId}";
        }
        #endregion
        #region 评论相关key
        public static string CreateTaskCommentCount()
        {
            return "cache_taskCommentcount";
        }
        #endregion

        #region 日志记录
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string CreateSysHhLog(string keyValue)
        {
            var key = $"hsh_sys_logs_hh_{keyValue}";
            return key;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string CreateSysLog(string keyValue)
        {
            var key = $"hsh_sys_logs_{keyValue}";
            return key;
        }
        #endregion

        public static string CreateVisitStatKey(DateTime? date = null)
        {
            var key = date.HasValue ? date.Value.ToString("yyMMdd") : DateTime.Now.ToString("yyMMdd");
            return $"hsh_visstat_{key}";
        }
        public const string SETTING_HOME_COURSE = "hsh_setting_homecourse";
        public const string SETTING_HLHOME_COURSE = "hsh_hlsetting_homecourse";
        public const string SETTING_LEVEL_COURSE = "hsh_setting_levelcourse";
    }
}
