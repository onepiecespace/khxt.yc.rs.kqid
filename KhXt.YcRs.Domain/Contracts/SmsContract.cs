﻿using Hx.MQ;
using System;

namespace KhXt.YcRs.Domain.Contracts
{
    public class SmsContract : IEventContract
    {
        public string Sn { get; set; }

        public string TaskId { get; set; }

        public string GwCode { get; set; }

        public string GwDesc { get; set; }


        public DateTime SubmitTime { get; set; }
        public DateTime ReceiptTime { get; set; }

        public int Status { get; set; }
        public string Content { get; set; }
    }
}
