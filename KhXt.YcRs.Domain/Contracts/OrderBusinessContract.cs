﻿using Hx.MQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Contracts
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderBusinessContract : IEventContract
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 订单Id
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public OrderStatus Status { get; set; }
        /// <summary>
        /// 所在模块
        /// </summary>
        public BusinessType BusinessType { get; set; }
        /// <summary>
        /// 是否自动续费
        /// </summary>
        public bool IsAutoRenew { get; set; }
    }
}
