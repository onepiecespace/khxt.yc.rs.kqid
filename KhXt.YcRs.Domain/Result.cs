﻿using Hx.Extensions;

namespace KhXt.YcRs.Domain
{
    public class Result<TData>
    {
        //public Result() 
        //{

        //}
        public Result(ResultCode resultCode = ResultCode.Success, string message = "", TData data = default)
        {
            Code = (int)resultCode;
            Message = message.IsNullOrEmpty() ? resultCode.GetDescription() : message;
            Data = data;
        }
        public int Code { get; set; } = (int)ResultCode.Success;
        public string Message { get; set; } = "";
        public int Expire = 0;//秒
        public TData Data { get; set; }
    }


    public class Result : Result<string>
    {

        //public Result() { }

        public Result(ResultCode resultCode = ResultCode.Success, string message = "", string data = null) : base(resultCode, message, data)
        {

        }
    }
}
