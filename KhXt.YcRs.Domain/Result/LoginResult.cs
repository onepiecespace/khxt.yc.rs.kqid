﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain
{
    /// <summary>
    /// 登录返回结果
    /// </summary>
    public class LoginResult : CommonResult
    {
        /// <summary>
        /// 跳转Url
        /// </summary>
        public string ReturnUrl;
        /// <summary>
        /// token
        /// </summary>
        public string AccessToken;
    }
}
