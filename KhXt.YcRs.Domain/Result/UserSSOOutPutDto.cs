using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KhXt.YcRs.Domain
{
    /// <summary>
    /// 输出对象模型
    /// </summary>
    [Serializable]
    public class UserSSOOutPutDto
    {

        #region Property Members

        /// <summary>
        /// 授权token码
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// AppCode
        /// </summary>
        public string AppCode { get; set; }

        /// <summary>
        /// 用户在微信开放平台的唯一标识符
        /// </summary>
        public string UnionId { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 账户
        /// </summary>
        public virtual string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public virtual string RealName { get; set; }

        /// <summary>
        /// 呢称
        /// </summary>
        public virtual string NickName { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public virtual string HeadIcon { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public virtual int? Gender { get; set; }



        /// <summary>
        /// 手机
        /// </summary>
        public virtual string MobilePhone { get; set; }



        /// <summary>
        /// 微信
        /// </summary>
        public virtual string WeChat { get; set; }


        /// <summary>
        /// 省份
        /// </summary>
        public virtual string Province { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public virtual string City { get; set; }
        /// <summary>
        /// 县区
        /// </summary>
        public virtual string District { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 登录IP地址
        /// </summary>
        public virtual string CurrentLoginIP { get; set; }
        /// <summary>
        /// 角色编码，多个角色，使用“,”分格
        /// </summary>
        public string Role { get; set; }

        #endregion

    }
}
