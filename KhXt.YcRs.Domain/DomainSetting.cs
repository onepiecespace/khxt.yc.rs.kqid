﻿using Hx.Configurations.Application;
using Hx.Extensions;
using System;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain
{
    public class DomainSetting
    {
        public HxAppSettings _appSetting;

        public DomainSetting(HxAppSettings appSettings)
        {
            _appSetting = appSettings;
        }
        public string NodeName
        {
            get
            {
                return _appSetting.NodeName;

            }
        }

        /// <summary>
        /// 资源服务器url
        /// </summary>
        public string ResourceUrl
        {
            get
            {
                var s = "";
                if (_appSetting.Parameters.ContainsKey("resServer")) s = _appSetting.Parameters["resServer"];
                if (s.IsNotNullOrEmpty() && !s.EndsWith("/")) s = s + "/";
                return s;
            }
        }
        public string ResFileServer
        {
            get
            {
                var s = "";
                if (_appSetting.Parameters.ContainsKey("resfileServer")) s = _appSetting.Parameters["resfileServer"];
                // if (s.IsNotNullOrEmpty() && !s.EndsWith("/")) s = s + "/";
                return s;
            }
        }
        public string ResServerLocal
        {
            get
            {
                var s = "";
                if (_appSetting.Parameters.ContainsKey("resServerLocal")) s = _appSetting.Parameters["resServerLocal"];
                // if (s.IsNotNullOrEmpty() && !s.EndsWith("/")) s = s + "/";
                return s;
            }
        }
        public string FFmpegPath
        {
            get
            {
                var s = "";
                if (_appSetting.Parameters.ContainsKey("FFmpegPath")) s = _appSetting.Parameters["FFmpegPath"];
                return s;
            }
        }
        public string appcode
        {
            get
            {
                var s = "";
                if (_appSetting.Parameters.ContainsKey("appcode")) s = _appSetting.Parameters["appcode"];
                return s;
            }
        }
        public string CourseTemplatePath
        {
            get
            {
                var s = "";
                if (_appSetting.Parameters.ContainsKey("CourseTemplate")) s = _appSetting.Parameters["CourseTemplate"];
                return s;
            }
        }


        public string CourseTemplateOutPath
        {
            get
            {
                var s = "";
                if (_appSetting.Parameters.ContainsKey("CourseTemplateOut")) s = _appSetting.Parameters["CourseTemplateOut"];
                return s;
            }
        }
        /// <summary>
        /// 文件路径
        /// </summary>
        public string UploadPath
        {
            get
            {
                var s = "";
                if (_appSetting.Parameters.ContainsKey("uploadPath")) s = _appSetting.Parameters["uploadPath"];
                return s;
            }
        }
        /// <summary>
        /// 未支付订单过期时间
        /// </summary>
        public long UnpayOrderExprie
        {
            get => GetConfigInt64Value("orderExpire", 24 * 60);
        }
        /// <summary>
        /// 微信支付回调通知
        /// </summary>
        public string WeChatPayNotifyUrl
        {
            get
            {
                var s = "";
                if (_appSetting.Parameters.ContainsKey("weChatPayNotifyUrl")) s = _appSetting.Parameters["weChatPayNotifyUrl"];
                return s;
            }
        }
        /// <summary>
        /// Token过期时间
        /// </summary>
        public long TokenExprie
        {
            get => GetConfigInt64Value("tokenExpire", 1440 * 365);
        }

        /// <summary>
        /// 短信过期时间
        /// </summary>
        public int SmsExpire
        {
            get => GetConfigInt32Value("smsExpire", 60 * 2);
        }


        public int GetConfigInt32Value(string key, int defaultValue = 0)
        {
            return GetConfigValue(key, Convert.ToInt32, defaultValue);
        }

        public long GetConfigInt64Value(string key, int defaultValue = 0)
        {
            return GetConfigValue(key, Convert.ToInt64, defaultValue);
        }


        public string GetConfigValue(string key, string defaultValue = "")
        {
            return GetConfigValue(key, v => v.Trim(), defaultValue);
        }

        public T GetConfigValue<T>(string key, Func<string, T> converter, T defaultValue = default)
        {
            if (_appSetting.Parameters.ContainsKey("key"))
            {
                try
                {
                    return converter(_appSetting.Parameters[key]);
                }
                catch (Exception ex)
                {
                    DomainEnv.CommonLogger.Error("GetConfigValue", ex);
                    return defaultValue;
                }
            }
            return defaultValue;
        }
    }
}
