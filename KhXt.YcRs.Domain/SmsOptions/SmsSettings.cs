﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.SmsOptions
{
    /// <summary>
    /// 短信平台参数配置
    /// </summary>
    //public class SmsSettings
    //{
    //    public Limit limitoptions { set; get; }
    //    //public Ali ali { set; get; }

    //}
    ////public class Ali
    ////{
    ////    public string Ali_Sms_AccessKeyId { set; get; }
    ////    public string Ali_Sms_AccessKeySecret { set; get; }
    ////    public string Ali_Sms_MethodActonName { set; get; }
    ////    public string Ali_Sms_Version { set; get; }
    ////    public string Ali_Sms_Domain { set; get; }
    ////    public int Ali_Sms_ExpireSeconds { set; get; }
    ////}
    //public class Limit
    //{
    //    //public string[] MobileWhiteList { set; get; }
    //    public int NotRegisterUserSendLimitCount { set; get; }
    //    public int AlreadyRegisterUserSendLimitCount { set; get; }
    //}

    public class Ali
    {
        /// <summary>
        /// 
        /// </summary>
        public string Ali_Sms_AccessKeyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Ali_Sms_AccessKeySecret { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Ali_Sms_MethodActonName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Ali_Sms_Version { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Ali_Sms_Domain { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Ali_Sms_ExpireSeconds { get; set; }
    }

    public class Limit
    {
        /// <summary>
        /// 
        /// </summary>
        public List<string> MobileWhiteList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int NotRegisterUserSendLimitCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AlreadyRegisterUserSendLimitCount { get; set; }
        public string OverDaySendMessage { set; get; }
    }

    public class SmsSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public Ali Ali { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Limit Limit { get; set; }
    }

    public class SmsSettingsRoot
    {
        /// <summary>
        /// 
        /// </summary>
        public SmsSettings SmsSettings { get; set; }
    }

}
