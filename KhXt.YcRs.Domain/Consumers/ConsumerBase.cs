﻿using Hx.Components;
using Hx.Domain.Services;
using Hx.Logging;
using Hx.MQ;
using Hx.Redis;
using Hx.Serializing;
using MassTransit;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Consumers
{
    [PropertyWired]
    public abstract class ConsumerBase<T> : IHxConsumer<T> where T : class, IContract
    {
        public abstract Task Consume(ConsumeContext<T> context);

        public ILogger Logger { get; set; }

        public IUnitOfWorkService UnitOfWorkService { get; set; }
        public ISerializer Serializer { get; set; }
        public RedisHelper Redis { get; set; }
    }
}
