﻿using KhXt.YcRs.Domain.Entities.VipCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.VipCard
{
    public interface IVipCardService : IService
    {
        Task<Result> Register(string phone, string cardNo, string passWord);
        VipCardEntity Get(object id);
        object Create(VipCardEntity entity);
    }
}
