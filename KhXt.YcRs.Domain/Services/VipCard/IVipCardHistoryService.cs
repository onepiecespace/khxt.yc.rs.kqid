﻿using KhXt.YcRs.Domain.Entities.VipCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace KhXt.YcRs.Domain.Services.VipCard
{
    public interface IVipCardHistoryService : IService
    {
        #region IComBaseService
        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        VipCardHistoryEntity Get(object id);

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(VipCardHistoryEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(VipCardHistoryEntity entity);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(VipCardHistoryEntity entity);

        /// <summary>
        /// 根据查询条件批量删除
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void Delete(Expression<Func<VipCardHistoryEntity, bool>> predicate);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<VipCardHistoryEntity> Table();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <returns>IQueryable类型的实体集合</returns>
        IQueryable<VipCardHistoryEntity> Fetch(Expression<Func<VipCardHistoryEntity, bool>> predicate, Action<Orderable<VipCardHistoryEntity>> order);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="pageIndex">第几页</param>
        PageList<VipCardHistoryEntity> Gets(Expression<Func<VipCardHistoryEntity, bool>> predicate, Action<Orderable<VipCardHistoryEntity>> order, int pageSize, int pageIndex);
        #endregion
    }
}
