﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Files
{
    /// <summary>
    /// 文件服务
    /// </summary>
    public interface IFilesService : IService
    {
        /// <summary>
        /// 上传文件到指定目录下
        /// </summary>
        /// <param name="fileData"></param>
        /// <param name="filePath">虚拟目录 img/xx/1.xx 或者 1.xxx </param>
        /// <returns></returns>
        Task<Result> UploadFiles(IFormFile fileData, string filePath);
        /// <summary>
        /// 上传文件到指定目录下
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="filePath">虚拟目录 img/xx/1.xx 或者 1.xxx </param>
        /// <returns></returns>
        Task<Result> UploadFiles(System.IO.Stream stream, string filePath);
        /// <summary>
        /// 移动服务器文件夹 指定路径到Oss指定路径
        /// </summary>
        /// <param name="fromPath">服务器文件路径</param>
        /// <param name="toPath">Oss路径</param>
        /// <returns></returns>
        Task<Result> MoveDirToOss(string fromPath, string toPath);
        /// <summary>
        /// 移动服务器文件 指定路径到Oss指定路径
        /// </summary>
        /// <param name="fromFileName">服务器文件路径</param>
        /// <param name="toFileName">Oss路径</param>
        /// <returns></returns>
        Task<Result> MoveFileToOss(string fromFileName, string toFileName);

        Task<Result> UploadFilePic(IFormFile fileData, string filePath);

        Task<Result> DownLoadFiles(string filePath);

        Task<Result> MoveFileToOtherPath(string oldFilePath, string filePath, string fileName);

        Task<Result> UploadBase64ToImg(string base64, string filePath);

        Task<Result> SavePicture(string picString, string name, string picString1, string name1, string picString2, string name2);

        Task<Result> SavePictureOptimization(string imgStrame, string imgName, string smallImgStrame, string samllImgName, string modularName, int category);

        Task<Result> UploadFileChunk(string uploadId, int chunk, int chunks, Stream fileStream, string filePath);

        Task<Result> SaveToFile(string uploadId, string path, int chunks);
        Task<Result> RemoveChunk(string uploadId, int chunks);
    }
}
