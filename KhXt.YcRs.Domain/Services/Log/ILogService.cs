﻿using KhXt.YcRs.Domain.Models.Log;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Log
{
    public interface ILogService
    {
        Task UserLogin(LoginLogModel model);

        List<LoginLogModel> GetNumAsync(string datime, string datetimesub, string datetimesec, string opt);
    }
}
