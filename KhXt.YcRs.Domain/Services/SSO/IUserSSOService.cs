﻿using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Util.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.SSO
{
    public interface IUserSSOService : IService
    {
        Task<LoginResult> ValidateOrRegister(string account, string password, string appCode);
        Task<CommonResult<UserSSOOutPutDto>> CheckAppToken(string token);


    }
}
