﻿using Essensoft.AspNetCore.Payment.WeChatPay.Request;
using KhXt.YcRs.Domain.Models.Pay;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Pay
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWeChatPayService : IService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="weChatPay"></param>
        /// <returns></returns>
        Task<Result<WeChatPayResult>> AppPay(WeChatPayUnifiedOrderRequest weChatPay);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="weChatPay"></param>
        /// <returns></returns>
        Task<Result<WeChatPayResult>> InnerPay(WeChatPayUnifiedOrderRequest weChatPay);
    }
}
