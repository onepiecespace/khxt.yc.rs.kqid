﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Models.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Order
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICouponInfoService : IService
    {

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<CouponInfoEntity> Table();
        /// <summary>
        /// 添加优惠券
        /// </summary>
        /// <param name="couponInfo"></param>
        /// <returns></returns>
        Task<Result> AddCouponInfo(CouponInfoEntity couponInfo);
        /// <summary>
        /// 更新优惠券
        /// </summary>
        /// <param name="id"></param>
        /// <param name="couponInfo"></param>
        /// <returns></returns>
        Task<Result> UpdateCouponInfo(long id, CouponInfoEntity couponInfo);

        /// <summary>
        /// 获取当前用户所有优惠券
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="status">状态</param>
        /// <param name="businessType">业务类别</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<Result<MyCouponList>> GetMyCoupons(long userId, int status = -2, int businessType = -1, int pageIndex = 1, int pageSize = 10);
        /// <summary>
        /// 获取用户可用优惠券列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="businessType"></param>
        /// <param name="vipType"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<Result<MyCanUseCouponList>> GetMyCanUseCoupons(long userId, int businessType = -1, int vipType = 0, int pageIndex = 1, int pageSize = 10);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <param name="businessType"></param>
        /// <returns></returns>
        Task<Result<List<MyCoupon>>> GetMyCouponAll(long userId, int status = -2, int businessType = -1);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <param name="businessType"></param>
        /// <returns></returns>
        Task<Result> GetUsableCount(long userId, int status = -2, int businessType = -1);
        /// <summary>
        /// 获取当前用户指定优惠券信息
        /// </summary>
        /// <param name="couponId">id</param>
        /// <returns></returns>
        Task<Result<MyCoupon>> GetMyCoupon(long couponId);
        /// <summary>
        /// 赠送优惠券给指定好友
        /// </summary>
        /// <param name="userId">我</param>
        /// <param name="userCouponId">优惠券Id</param>
        /// <param name="phone">指定好友</param>
        /// <returns></returns>
        Task<Result> GiveToFriend(long userId, long userCouponId, string phone);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userCouponId"></param>
        /// <returns></returns>
        Task<Result> userCouponHasUsed(long userId, long userCouponId);
        /// <summary>
        /// 优惠券Id
        /// </summary>
        /// <param name="userCouponId"></param>
        /// <param name="conflictIds">冲突信息</param>
        /// <returns></returns>
        Task<Result> GetIsCanUse(long userCouponId, string conflictIds);
    }

}
