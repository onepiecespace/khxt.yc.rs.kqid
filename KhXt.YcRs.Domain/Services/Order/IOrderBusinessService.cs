﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Order
{
    /// <summary>
    /// 订单业务处理类
    /// </summary>
    public interface IOrderBusinessService : IService
    {
        /// <summary>
        /// 订单业务处理
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="orderId">订单Id</param>
        /// <param name="businessType">业务类别</param>
        /// <param name="isAutoRenew">是否自动续费</param>
        /// <returns></returns>
        Task<Result> DealWithOrder(long userId, string orderId, BusinessType businessType, bool isAutoRenew = false);
    }
}
