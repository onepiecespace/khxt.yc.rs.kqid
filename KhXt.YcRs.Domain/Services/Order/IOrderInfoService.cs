﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.Pay;
using KhXt.YcRs.Domain.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Order
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOrderInfoService : IService
    {

        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        OrderInfoEntity Get(object id);
        OrderItemEntity GetItem(string orderId);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(OrderInfoEntity entity);
        void UpdateItem(OrderItemEntity entity);

        long Add(OrderPayEntity entity);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<OrderInfoEntity> Table();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderInfo"></param>
        /// <param name="orderItem"></param>
        /// <param name="orderShipping"></param>
        /// <param name="orderCoupons"></param>
        /// <param name="isync"></param>
        /// <returns></returns>
        Task<Result<OrderStatusModel>> CreateOrder(OrderInfoEntity orderInfo,
            OrderItemEntity orderItem,
            OrderShippingEntity orderShipping,
            List<OrderCouponEntity> orderCoupons, bool isync = false);
        /// <summary>
        /// 获取可用优惠券组合
        /// </summary>
        /// <param name="businessType"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<CanCouponInfo> GetCanCouponInfos(BusinessType businessType, long userId);
        /// <summary>
        /// 支付订单
        /// </summary>
        /// <param name="payOrderModel"></param>
        /// <returns></returns>
        Task<Result> PayOrder(PayOrderModel payOrderModel);

        /// <summary>
        /// /
        /// </summary>
        /// <param name="paymentType"></param>
        /// <param name="orderId"></param>
        /// <param name="businessType"></param>
        /// <param name="isRenew"></param>
        /// <param name="tradeType"></param>
        /// <param name="openId"></param>
        /// <returns></returns>

        Task<Result<PayResult>> PayOrder(PaymentType paymentType, string orderId, int businessType, bool isRenew, string tradeType = "APP", string openId = "");

        Task<Result<ValueChange>> GoldExchange(PaymentType paymentType, string orderId);

        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="businessType"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<Result<OrderStatusModels>> GetOrders(long userId, int businessType, int status, int pageIndex = 1, int pageSize = 10);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="businessType"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<Result<OldOrderStatusModels>> GetOldOrders(long userId, int businessType, int status, int pageIndex = 1, int pageSize = 10);
        /// <summary>
        /// 获取订单信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Task<Result<OrderStatusModel>> GetOrder(string orderId);

        /// <summary>
        /// 获取订单状态 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Task<Result> GetOrderStatus(long userId, string orderId);
        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Task<Result> CancelOrder(string orderId);
        /// <summary>
        /// 关闭订单
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Task<Result> CloseOrder(string orderId);
        /// <summary>
        /// 计算价格
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="price"></param>
        /// <param name="count"></param>
        /// <param name="userCouponId"></param>
        /// <returns></returns>
        Task<Result<string>> CountPrice(long userId, float price, int count, long userCouponId);
        /// <summary>
        /// 写入台账表
        /// </summary>
        /// <param name="payOrderModel"></param>
        /// <returns></returns>
        Task<Result> OrderPay(PayOrderModel payOrderModel);
        /// <summary>
        /// 修复指定订单
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="transaction">描述信息 默认 系统修复台账</param>
        /// <returns></returns>
        Task<Result> FixOrderPay(string orderId, string transaction = "系统修复台账");
        /// <summary>
        /// 台账表list(临时)
        /// </summary>
        /// <returns></returns>
        IQueryable<OrderPayEntity> OrderPayTable();
        /// <summary>
        /// 线下开课
        /// </summary>
        /// <param name="courseOfflinePay"></param>
        /// <returns></returns>
        Task<Result> OfflinePay(CourseOfflinePay courseOfflinePay);

        void commpay(CourseOfflinePay courseOfflinePay, bool isHas = false);
        Task NoticeMxhClearCache(string nodeName = "", int code = 0);
        /// <summary>
        /// 获取订单
        /// </summary>
        /// <param name="orderQueryModel"></param>
        /// <returns></returns>
        Task<Result<OrderQueryResultPageModel>> GetOrders(OrderQueryModel orderQueryModel);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="businessType"></param>
        /// <param name="isRenew"></param>
        /// <returns></returns>
        Task<Result<AlipayTradeAppPayViewModel>> AliPayOrder(string orderId, int businessType, bool isRenew);
    }

}
