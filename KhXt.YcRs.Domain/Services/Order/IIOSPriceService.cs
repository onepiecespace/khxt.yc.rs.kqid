﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Order
{
    public interface IIOSPriceService : IService
    {


        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<IOSPriceEntity> Table();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        Task<Result> AddIOSPrice(IOSPriceEntity addEntity);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        Task<Result<float>> GetIosPrice(string productId);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<Result<Dictionary<string, float>>> Gets();
    }

}
