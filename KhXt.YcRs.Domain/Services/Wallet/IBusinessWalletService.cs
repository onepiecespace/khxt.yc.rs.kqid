﻿using KhXt.YcRs.Domain.Entities.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Services.Wallet
{
    public interface IBusinessWalletService : IService
    {
        /// <summary>
        /// 订单充值
        /// </summary>
        /// <param name="orderInfo"></param>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        int WalletCharge(OrderInfoEntity orderInfo, OrderItemEntity orderItem);
    }
}
