﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Wallet;
using KhXt.YcRs.Domain.Models.Pay;
using KhXt.YcRs.Domain.Models.Wallet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Wallet
{
    public interface IWalletService : IService
    {

        #region IComBaseService
        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        WalletEntity Get(object id);

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(WalletEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(WalletEntity entity);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(WalletEntity entity);

        /// <summary>
        /// 根据查询条件批量删除
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void Delete(Expression<Func<WalletEntity, bool>> predicate);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<WalletEntity> Table();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <returns>IQueryable类型的实体集合</returns>
        IQueryable<WalletEntity> Fetch(Expression<Func<WalletEntity, bool>> predicate, Action<Orderable<WalletEntity>> order);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="pageIndex">第几页</param>
        PageList<WalletEntity> Gets(Expression<Func<WalletEntity, bool>> predicate, Action<Orderable<WalletEntity>> order, int pageSize, int pageIndex);
        #endregion
        /// <summary>
        /// 钱包充值 
        /// </summary>
        /// <param name="chargeModel"></param>
        /// <returns></returns>
        Result<WalletChargeResult> WalletCharge(WalletChargeModel chargeModel);

        /// <summary>
        /// 订单充值
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        int WalletCharge(string orderId, long userId);

        /// <summary>
        /// 获取用户的钱包余额
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Result<WalletResult> GetWallet(long userId);

        /// <summary>
        /// 获取用户充值记录
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Result<WalletLogsResult> GetWallets(long userId, int pageIndex = 1, int pageSize = 10);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderId"></param>
        /// <param name="businessType"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<Result<WalletPayResult>> PayAsync(long userId, string orderId, int businessType, string code);
    }

}
