﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Vip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Vip
{
    /// <summary>
    /// 
    /// </summary>
    public interface IVipPrivilegeService : IService
    {
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(VipPrivilegeEntity entity);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="size">图片尺寸</param>
        /// <returns></returns>
        Task<List<VipPrivilegeEntity>> GetList(int size);

        /// <summary>
        /// 获取会员服务
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        Task<List<VipPrivilegeEntity>> GetList(long vipId, int size);
    }

}
