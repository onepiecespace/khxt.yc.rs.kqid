﻿using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Models.Vip;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Vip
{
    /// <summary>
    /// 
    /// </summary>
    public interface IVipService : IService
    {
        /// <summary>
        /// 会员服务
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        Task<Result<VipServiceInfo>> GetVipServiceInfo(long userId, int size);
        /// <summary>
        /// 添加会员卡信息
        /// </summary>
        /// <param name="vipInfoEntity"></param>
        /// <returns></returns>
        Task<Result> AddVipInfo(VipInfoEntity vipInfoEntity);
        /// <summary>
        /// 添加会员特权信息
        /// </summary>
        /// <param name="vipPrivilegeEntity"></param>
        /// <returns></returns>
        Task<Result> AddVipPrivilegeInfo(VipPrivilegeEntity vipPrivilegeEntity);
        /// <summary>
        /// 付款成功 开通会员卡
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <param name="vipId"></param>
        /// <param name="isAuto"></param>
        /// <returns></returns>
        Task<int> OpenVip(string orderId, long userId, long vipId, bool isAuto = false);
        /// <summary>
        /// 获用户会员信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<Result<UserVipModel>> GetUserVip(long userId);
        /// <summary>
        /// 获取用户会员中心
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        Task<Result<MyVip>> GetUserCenter(long userId, int size);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //Task<Result> SyncVipCard();
    }
}
