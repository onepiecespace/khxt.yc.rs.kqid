﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Models.Vip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Vip
{
    public interface IVipInfoService : IService
    {
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(VipInfoEntity entity);
        Task<List<VipInfo>> GetAll();
        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<VipInfoEntity> Table();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vipId"></param>
        /// <returns></returns>
        Task<Result<VipInfo>> Get(long vipId);
    }

}
