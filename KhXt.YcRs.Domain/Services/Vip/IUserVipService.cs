﻿using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Models.Vip;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Vip
{
    public interface IUserVipService : IService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<UserVipModel> GetAll();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long Add(UserVipModel model);
        /// <summary>
        /// /
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Update(UserVipModel model);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Delete(UserVipModel model);

        Task<Result<UserVipModel>> GetUserVip(long userId);
    }
}
