﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.WalletLogs;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.Services.WalletLogs
{
    public interface IWalletLogsService : IService
    {

        #region IComBaseService
        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        WalletLogsEntity Get(object id);

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(WalletLogsEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(WalletLogsEntity entity);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(WalletLogsEntity entity);

        /// <summary>
        /// 根据查询条件批量删除
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void Delete(Expression<Func<WalletLogsEntity, bool>> predicate);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<WalletLogsEntity> Table();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <returns>IQueryable类型的实体集合</returns>
        IQueryable<WalletLogsEntity> Fetch(Expression<Func<WalletLogsEntity, bool>> predicate, Action<Orderable<WalletLogsEntity>> order);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="pageIndex">第几页</param>
        PageList<WalletLogsEntity> Gets(Expression<Func<WalletLogsEntity, bool>> predicate, Action<Orderable<WalletLogsEntity>> order, int pageSize, int pageIndex);
        #endregion

    }

}
