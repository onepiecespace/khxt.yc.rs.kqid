﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Product
{
    public interface IPro_classService : IService
    {

        #region IComBaseService
        Task<Result<List<ProCategoryInfo>>> GetAll();
        /// <summary>
        /// 实体集合
        /// </summary>
        Task<IQueryable<ProCategoryInfo>> Table();

        Task<Result<List<ProCategoryInfo>>> GetListAsync(Expression<Func<ProCategoryInfo, bool>> predicate, Action<Orderable<ProCategoryInfo>> order);

        Task<Result<ProCategoryListInfo>> GetList(int sid);
        Task<Result<ProCategoryInfo>> Get(int cid);
        #endregion

    }

}
