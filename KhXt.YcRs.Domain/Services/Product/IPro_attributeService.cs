﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Product
{
    public interface IPro_attributeService : IService
    {

        Task<Result<List<ProAttributeInfo>>> GetAll();
        /// <summary>
        /// 实体集合
        /// </summary>
        Task<IQueryable<ProAttributeInfo>> Table();

        Task<Result<List<ProAttributeInfo>>> GetListAsync(Expression<Func<ProAttributeInfo, bool>> predicate, Action<Orderable<ProAttributeInfo>> order);

        Task<Result<List<ProAttributeInfo>>> GetList(int proid);
        Task<Result<ProAttributeInfo>> Get(int attrId);

    }

}
