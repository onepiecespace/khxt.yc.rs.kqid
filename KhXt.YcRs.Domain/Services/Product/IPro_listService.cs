﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Product
{
    public interface IPro_listService : IService
    {

        #region IComBaseService
        Task<Result<List<ProInfo>>> GetAll();
        /// <summary>
        /// 实体集合
        /// </summary>
        Task<IQueryable<ProInfo>> Table();

        Task<Result<List<ProInfo>>> GetListAsync(Expression<Func<ProInfo, bool>> predicate, Action<Orderable<ProInfo>> order);
        Task<Result<ProListPageList>> ProPageList(int cid, int s_type, int pageIndex, int pageSize);
        Task<Result<ProListInfo>> GetList(int cid);
        Task<Result<ProDetailInfoDto>> Get(int proId);

        #endregion


    }

}
