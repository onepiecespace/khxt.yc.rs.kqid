﻿using Hx;
using KhXt.YcRs.Domain.Models.Test;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services
{
    public interface ITestService : IService
    {
        Task<string> GetSomeBody(int id);

        Task<Outcome<bool>> MongoDemo();

        List<PersonModel> AllPerson();

        string Add(PersonModel model);

        List<PersonModel> GetPersons(out bool isFromCache);
    }


}
