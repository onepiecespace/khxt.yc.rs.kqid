﻿
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Course
{
    public interface ICourseBuyCollectService : IService
    {

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        long Add(CourseBuyCollectEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        bool Update(CourseBuyCollectEntity entity);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<CourseBuyCollectEntity> Table();
        Task<List<CourseBycollectModel>> GetAll();
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="pageIndex">第几页</param>
        PageList<CourseBuyCollectEntity> Gets(Expression<Func<CourseBuyCollectEntity, bool>> predicate, Action<Orderable<CourseBuyCollectEntity>> order, int pageSize, int pageIndex);
        Task<PageList<CourseBycollectModel>> GetPageListAsync(Expression<Func<CourseBycollectModel, bool>> predicate, Action<Orderable<CourseBycollectModel>> order, int pageSize, int pageIndex);
        bool UpdateIshide(int ishide, long cbid);
        bool UpdateExpirationDate(DateTime ExpirationDate, long cbid);
        bool BuycollectClearCache();
    }

}
