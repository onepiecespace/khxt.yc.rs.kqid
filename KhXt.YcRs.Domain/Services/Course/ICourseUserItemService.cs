﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Course
{
    public interface ICourseUserItemService : IService
    {

        #region IComBaseService
        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        CourseUserItemEntity Get(object id);
        bool CourseUserItemClearCache();


        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(CourseUserItemEntity entity);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(CourseUserItemEntity entity);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<CourseUserItemEntity> Table();

        Task<Result<CourseItemDoneListInfo>> GetUserCourseWorkList(long userId, long courseId, long courseChildId, int osSign);

        Task<Result<CourseUserWorkResult>> SaveCourseUserItem(int userId, int courseChildId, List<CourseUserItemAddModel> courseUserItemAddModelList);
        #endregion
    }

}
