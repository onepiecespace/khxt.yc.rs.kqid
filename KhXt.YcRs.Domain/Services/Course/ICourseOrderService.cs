﻿
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Course
{
    public interface ICourseOrderService : IService
    {
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(CourseOrderEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(CourseOrderEntity entity);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<CourseOrderEntity> Table();

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="pageIndex">第几页</param>
        List<MyOrderInfo> Gets(Expression<Func<CourseOrderEntity, bool>> predicate, int pageSize, int pageIndex);

        IQueryable<OrderListOfCMS> GetOrderList(int pageSize = 10, int pageIndex = 1);
    }

}
