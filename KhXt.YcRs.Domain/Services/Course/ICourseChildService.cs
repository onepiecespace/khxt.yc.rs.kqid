﻿
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Course
{
    public interface ICourseChildService : IService
    {

        #region 复用缓存查询方法
        Task<List<CourseChildInfo>> GetAll();

        Task<PageList<CourseChildInfo>> GetPageListAsync(Expression<Func<CourseChildInfo, bool>> predicate, Action<Orderable<CourseChildInfo>> order, int pageSize, int pageIndex);

        #endregion
        #region  简单增删改

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="Info">实体</param>
        long Add(CourseChildInfo Info);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Update(CourseChildInfo Info);
        bool UpdateChildType(long courseChildId, int ChildType, string videoId);
        Task<Result<List<int>>> TaskSetChildType();
        Task<Result<List<CourseisliveChildInfo>>> GetliveChildlist();
        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Delete(CourseChildInfo Info);
        bool CourseChildClearCache();

        #endregion

        #region 自定义方法 接口

        Task<Result<CourseChildInfo>> Get(long id);
        Task<PageList<CourseUserListModel>> GetUserCourseList(long userId, long courseId, int pageIndex, int pageSize);
        Task<Result<CourseItemListInfo>> GetUserCourseListResult(long userId, long courseId, int pageIndex, int pageSize);
        Task<Result<List<CourseChildInfo>>> GetChildCountAsync(long userId, long courseId);
        Task<Result<List<UserCourseChildInfo>>> GetUserChildCountAsync(long userId, long courseId);
        #endregion

        #region cms
        List<CourseChildInfo> GetCourseChildList(CourseChildInfo info);
        Task<int> optionAsync(CourseChildModelCMS model, string optiden, string name);
        #endregion

        int BatchInsertExcel(List<CourseChildModelCMS> list);
    }

}
