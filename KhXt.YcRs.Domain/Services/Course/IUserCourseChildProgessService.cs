﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Course
{
    public interface IUserCourseChildProgessService : IService
    {


        Task<List<UserCourseChildInfo>> GetAll();


        /// <summary>
        /// 实体集合
        /// </summary>
        Task<IQueryable<UserCourseChildInfo>> Table();

        Task<Result<UserCourseChildInfo>> Get(long Userid, long CourseId, long CourseChildId);

        #region  简单增删改

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="Info">实体</param>
        long Add(UserCourseChildInfo Info);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Update(UserCourseChildInfo Info);
        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Delete(UserCourseChildInfo Info);
        bool UserChildClearCache();

        #endregion

    }

}
