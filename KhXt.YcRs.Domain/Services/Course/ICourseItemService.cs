﻿
using KhXt.YcRs.Domain.Entities.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.Services.Course
{
    public interface ICourseItemService : IService
    {

        #region 
        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(CourseItemEntity entity);
        int BatchInsertExcel(List<CourseItemEntity> list);
        bool BatchUpdatetExcel(long courseChildId, int IsDelete);
        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<CourseItemEntity> Table();
        bool CourseItemClearCache();


        #endregion

    }

}
