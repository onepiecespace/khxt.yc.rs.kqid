﻿
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Course
{
    public interface ICourseCateoryService : IService
    {

        Task<CourseCateoryModel> Get(long id);
        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<CourseCateoryModel> Table();
        ///
        List<CourseCateoryModel> GetCategoryList();
        List<CourseCateoryModel> GetCategoryTwoList(int Id);

        List<CourseCateoryModel> GetCourseCateoryList(CourseCateoryModel entity);


        int Option(CourseCateoryModel entity, string optiden);
    }

}
