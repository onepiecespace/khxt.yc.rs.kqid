﻿
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Home;
using KhXt.YcRs.Domain.Models.Message;
using KhXt.YcRs.Domain.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Course
{
    public interface ICourseService : IService
    {
        Task<List<CourseQueryModel>> GetAll();
        Task<Result<List<CourseQueryModel>>> GetListAsync(Expression<Func<CourseQueryModel, bool>> predicate, Action<Orderable<CourseQueryModel>> order);
        Task<PageList<CourseQueryModel>> GetPageListAsync(Expression<Func<CourseQueryModel, bool>> predicate, Action<Orderable<CourseQueryModel>> order, int pageSize, int pageIndex);

        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<Result<CourseQueryModel>> GetAsync(long Id);

        Task<List<CourseThinModel>> GetLevelList(List<LevelCourseSettingModel> setting, long userId, int level);
        Task<List<CourseThinModel>> GetHotList(List<HomeCourseSettingModel> setting, long userId);
        Task<List<CourseThinModel>> GetRecommendList(List<HomeCourseSettingModel> setting, long userId);
        Task<List<CourseThinModel>> GetLiveList(List<HomeCourseSettingModel> setting, long userId);
        Task<List<CategoryedCourseThinModel>> GetCategoriedTopCourses(List<HomeCourseSettingModel> setting);
        Task<List<CategoryedCourseThinModel>> GetCategoriedTop(long userId, List<HomeCourseSettingModel> setting);
        Task<List<CoursePcThinModel>> GetPcHotList(List<HomeCourseSettingModel> setting);
        Task<List<CoursePcThinModel>> GetPcRecommendList(List<HomeCourseSettingModel> setting);
        Task<List<CoursePcThinModel>> GetPcLiveList(List<HomeCourseSettingModel> setting);
        Task<List<CategoryedCoursePcThinModel>> GetPcCategoriedTopCourses(List<HomeCourseSettingModel> setting);
        Task<List<CategoryedCoursePcThinModel>> GetPcCategoriedTop(long userId, List<HomeCourseSettingModel> setting);
        /// <summary>
        /// 获取讲师课程，key为课程类型
        /// </summary>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        Task<Dictionary<int, List<CoursePcThinModel>>> GetTeacherCourses(long teacherId);
        /// <summary>
        /// 简化版课程信息，用于首页，列表页等展示
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<CourseThinModel>> GetWithChildModelsAsync();
        Task<IEnumerable<CoursePcThinModel>> GetPcWithChildModelsAsync();
        Task<List<CoursePcThinModel>> GetPcWithChildModelsList(long userId);

        Task<bool> SetCourseChildType();

        bool CourseClearCache();

        #region 自定义方法
        Task<Result<CouerseListInfo>> GetIndexListInfo(long userId, string aid, int classType, int pageIndex, int pageSize, int livePageSize);
        Task<Result<CouerseIndexListInfo>> GetIndexList(long userId, string aid, int classType, int pageIndex, int pageSize);
        Task<Result<CouerseListInfo>> GetCouerseList(long userId, string aid, int cateoryId, int selectType, int pageIndex, int pageSize);
        Task<Result<VideoLiveInfo>> GetVideoLiveInfo(long userId, int courseChildId, string UserIp);

        Task<Result<List<MessUserInfo>>> ExporChannelUser(long courseId, long channelid);
        Task<Result<List<CourseLogInfo>>> ExporOnlineUser(string ChannelId, long ChildId, int ChildType);
        #endregion
        #region CMS

        Task<IEnumerable<CourseQueryModel>> GetCourseList(CourseQueryModel entity);

        Task<IEnumerable<CourseQueryModel>> GetCourseAllList(CourseQueryModel model);
        IEnumerable<UserliveModel> GetLiveUserList(int CourseId);
        bool Update(CourseEntity entity);
        int Option(CourseQueryModel entity, string optiden, string name);
        #endregion
        /// <summary>
        /// 生成指定课程的html
        /// </summary>
        /// <param name="courseId">0 全部 ，课程Id</param>
        /// <returns></returns>
        Task<Dictionary<long, string>> GenerateHtml(long courseId);
        /// <summary>
        /// 给视频赋值时间
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        Task<Dictionary<string, string>> GenerateChildTimel(int courseId);

        /// <summary>
        /// 所有/时间（查询）
        /// </summary>
        /// <returns></returns>
        List<CourseAppointmentModel> SearchAllorTime(string datetime);

        Task<Result> CourseAppointmentAdd(string name, string phone);
        /// <summary>
        /// 开通课程
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        Task<int> OpenCourse(string orderId, long userId, long courseId);
        /// <summary>
        /// 创建课程订单
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <param name="orderId"></param>
        /// <param name="device"></param>
        /// <returns></returns>
        Task<Result> CreateCourseOrder(long userId, long courseId, string orderId, PlatFormType device = PlatFormType.ios);

        Task<Dictionary<int, List<CourseThinModel>>> GetCcRecommCourse();
        CourseEntity GetById(long id);
        void wirtelog(string name, string type);
        Task<Result<List<CourseChildInfo>>> GetChildCountAsync(long userId, long courseId);
        Task<Result<List<UserCourseChildInfo>>> GetUserChildCountAsync(long userId, long courseId);
        Task<Result<List<CategoryedCourseThinModel>>> HomeGet(long userId);
    }

}
