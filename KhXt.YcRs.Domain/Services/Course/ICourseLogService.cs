﻿using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Message;
using KhXt.YcRs.Domain.Models.MongologModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Course
{
    public interface ICourseLogService : IService
    {
        void AddCourselog(MessageInfo info);

        void AddLogOfOperation(OperationInfo info);
        /// <summary>
        /// 按天导出 某个课程人员情况
        /// </summary>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="channelId">频道号</param>
        /// <param name="ignore">是否排除已导出 默认排除 true 排除 false 不排除</param>
        /// <returns></returns>
        Task<Result<Dictionary<string, List<CourseLogModel>>>> ExportExcelBaseDay(string startTime, string endTime, string channelId, bool ignore = true);
        Task<Result<List<CourseLogInfo>>> ExportExcelBaseChildId(string ChannelId, long ChildId, int ChildType = 1, bool ignore = true);
    }
}
