﻿
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.User
{
    public interface IVersionApkService : IService
    {

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<VersionApkEntity> Table();
        /// <summary>
        /// 获取版本信息
        /// </summary>
        /// <param name="osSign">系统标识</param>
        /// <param name="currentVersion">当前</param>
        /// <returns></returns>
        Task<Result<VersionApkInfo>> GetVersionInfo(int osSign, string currentVersion);
        /// <summary>
        /// 获取版本信息
        /// </summary>
        /// <param name="osSign">系统标识</param>
        /// <param name="currentVersion">当前</param>
        /// <returns></returns>
        Task<Result> GetVersionInfo(int osSign);


    }

}
