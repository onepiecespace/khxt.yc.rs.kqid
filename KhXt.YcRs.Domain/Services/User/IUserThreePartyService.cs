﻿
using KhXt.YcRs.Domain.Entities.User;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.Services.User
{
    public interface IUserThreePartyService : IService
    {

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<UserThreePartyEntity> Table();

    }

}
