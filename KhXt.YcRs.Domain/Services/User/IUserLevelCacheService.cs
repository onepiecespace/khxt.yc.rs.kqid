﻿using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models.User;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Services.User
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUserLevelCacheService : IService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>

        long Add(UserLeveModel entity);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Update(UserLeveModel entity);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<UserLeveModel> GetList();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Dictionary<int, int> GetUserLevel(long userId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="businessType"></param>
        /// <returns></returns>
        int GetUserLevel(long userId, int businessType);

    }
}
