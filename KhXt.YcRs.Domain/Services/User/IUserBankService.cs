﻿
using KhXt.YcRs.Domain.Entities.User;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.Services.User
{
    public interface IUserBankService : IService
    {
        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(UserBankEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(UserBankEntity entity);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<UserBankEntity> Table();

    }
}
