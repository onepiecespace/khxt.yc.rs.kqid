﻿
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.User
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAddressService : IService
    {
        Task<List<AddressInfo>> GetAll();


        /// <summary>
        /// 实体集合
        /// </summary>
        Task<IQueryable<AddressInfo>> Table();

        Task<Result<AddressInfo>> Get(long Userid);

        #region  简单增删改

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="Info">实体</param>
        long Add(AddressInfo Info);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Update(AddressInfo Info);
        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Delete(AddressInfo Info);
        bool AddressClearCache();

        #endregion



    }

}
