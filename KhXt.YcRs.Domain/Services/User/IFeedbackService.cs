﻿
using KhXt.YcRs.Domain.Entities.User;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.Services.User
{
    /// <summary>
    /// Feedback FeedbackEntity FeedbackRepository
    /// </summary>
    public interface IFeedbackService : IService
    {
        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<FeedbackEntity> Table();

    }

}
