﻿using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.User
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUserCacheService : IService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tryUpdate"></param>
        /// <returns></returns>
        Task<UserBaseInfo> GetUser(long userId, bool tryUpdate = true);
        Task<List<UserBaseInfo>> GetUserList();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<Result> Update(UserBaseInfo entity);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        Task<Result<long>> Add(UserBaseInfo entity, bool ifRefreshCache = true);
        ///// <summary>
        ///// 
        ///// </summary>
        bool userCacheClearCache();
        /// <summary>
        /// 获取代理商
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<Result<string>> GetAid(long userId);
        Task<UserBaseInfo> GetUserByPhone(string phone, bool tryUpdate = true);
        Task<UserBaseInfo> GetUserByOpenId(string id, LoginType loginType, bool tryUpdate = true);
        Task<UserBaseInfo> GetUserByUnionId(string id, LoginType loginType, bool tryUpdate = true);
        /// <summary>
        /// 获取当前用户金币数量 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<Result<long>> GetCoin(long userId);

        /// <summary>
        /// 获取用户经验值排名、分值
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="businessType"></param>
        /// <returns></returns>
        Task<Result<Tuple<long, long>>> GetExp(long userId, BusinessType businessType);
    }
}
