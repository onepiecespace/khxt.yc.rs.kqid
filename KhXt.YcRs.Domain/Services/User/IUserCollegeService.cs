﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.User
{
    public interface IUserCollegeService : IService
    {

        Task<List<UserCollegeInfo>> GetAll();


        /// <summary>
        /// 实体集合
        /// </summary>
        Task<IQueryable<UserCollegeInfo>> Table();

        Task<Result<UserCollegeInfo>> Get(long Userid);

        #region  简单增删改

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="Info">实体</param>
        long Add(UserCollegeInfo Info);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Update(UserCollegeInfo Info);
        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Delete(UserCollegeInfo Info);
        bool CollegeClearCache();

        #endregion


    }

}
