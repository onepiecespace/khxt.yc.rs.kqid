﻿
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.User;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.User
{
    public interface IUserService : IService
    {

        #region IComBaseService
        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tryUpdate"></param>
        /// <returns></returns>
        Task<UserBaseInfo> Get(long id, bool tryUpdate = true);

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="ifRefreshCache"></param>
        Task<Result<long>> Create(UserBaseInfo entity, bool ifRefreshCache = true);


        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        Task<Result> Update(UserBaseInfo entity);

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Result> Delete(long id);

        /// <summary>
        /// 物理删除,慎用
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Result> Zap(long id);

        /// <summary>
        /// 实体集合
        /// </summary>
        Task<List<UserBaseInfo>> GetAll();
        bool userClearCache();

        #endregion



        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tryUpdate"></param>
        /// <returns></returns>
        //[Obsolete]
        Task<UserBaseInfo> GetUser(long userId, bool tryUpdate = true);  //WHY 抽空跟Get方法合并


        Task<UserBaseInfo> GetByUnionId(string unionId, LoginType loginType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="tryUpdate"></param>
        /// <returns></returns>
        Task<UserBaseInfo> GetUserByPhone(string phone, bool tryUpdate = true);
        Task<Result> GetToken(string phone);
        /// <summary>
        ///  获取用户收藏课程信息
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PageList<MyCourseCollectInfo> GetCourseCollectList(string phone, int pageIndex, int pageSize);
        List<CourseBuyCollectEntity> GetMyCourseOrderCollect(int courseid);
        /// <summary>
        /// 用户个人中心
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<Result<UserBaseInfo>> GetUserInfo(long userId);
        /// <summary>
        ///  1已购买 2收藏
        /// </summary>
        /// <param name="type"> 1已购买 2收藏</param>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PageList<CourseBuyCollectEntity> GetMyCourseOrderCollectList(int type, int userId, int pageIndex, int pageSize);



        /// <summary>
        /// 获取用户金币，经验等变更历史信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="businessType"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<Paged<ValueLogModel>> GetValueLogs(long userId, ValueCategory valueCategory, BusinessType businessType, int pageIndex, int pageSize);

        /// <summary>
        /// 新版第三方登录，使用unionid
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Result> LoginWith2fa(LoginWith2faModel model);
        /// <summary>
        /// 用户登录注册
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="aid">应用商家</param>
        /// <param name="tid">租户关联id</param> 
        /// <returns></returns>
        Task<Result> LoginOrRegister(string phone, string aid, string tid, string regid);
        /// <summary>
        /// 用户登录注册
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        Task<Result> SSOLoginOrRegister(string phone, string aid);

        /// <summary>
        /// 第三方登录（QQ，WX）
        /// </summary>
        /// <param name="model"></param>
        /// <returns>如果成功，返回token,否则返回空字符串</returns>
        Task<Result> ThreeLogin(ThreeLoginModel model);

        /// <summary>
        /// 第三方id,phone绑定
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="openId"></param>
        /// <param name="phone"></param>
        /// <param name="code"></param>
        /// <returns>如果成功，返回token,否则返回空字符串</returns>
        [Obsolete("改用unionid版本(LoginWith2faBind),app等旧版本停止支持后可删除")]
        Task<Result> ThreeLoginBind(string aid, string openId, string phone, string code);

        Task<Result> LoginWith2faBind(LoginWith2faBindModel para);
        /// <summary>
        /// 解绑手机号
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<Result> PhoneChangeAsync(string phone, string code, string oldphone);
        /// <summary>
        /// 获取用户地址
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        Task<Result<AddressInfo>> GetAddress(string userId);
        /// <summary>
        /// 获取用户地址
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<Result<List<AddressInfo>>> GetAddressInfos(long userId);

        /// <summary>
        /// 我的经验
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<Result<MyExpInfo>> GetMyExp(long userId, int pageIndex = 1, int pageSize = 10);

        Task<string> GetTokenByOpenId(string openId, LoginType loginType);

        List<UserInfoOfMsg> GetListOfMsg(string datetime);
        Task<Result<long>> ChargeCollegeAsync(AUserTobInput aUserTob);
        long SeOrCeUid(string phone);
        long CreateUserOpen(UserOpenReq req);
        Task<Result<UserQueryResultPageModel>> GetUsesPage(UserQueryModel userQueryModel);
    }
}
