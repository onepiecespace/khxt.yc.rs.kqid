﻿using KhXt.YcRs.Domain.EventData;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services
{
    public interface IService
    {
        void ClearCache();

        Task ClearCacheAsync();

        void DoSync(string sourceNodeName, IDomainEventData eventData);

        Task DoSyncAsync(string sourceNodeName, IDomainEventData eventData);

        Task<string> DebugAsync();

        Task InitAsync();
    }
}
