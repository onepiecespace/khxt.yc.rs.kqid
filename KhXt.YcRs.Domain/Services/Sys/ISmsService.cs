﻿using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services
{
    public interface ISmsService : IService
    {
        Task<Result> SendValidCode(string phone, string aid, string bizType = "1", string content = "");
        Task<Result> SendValidCodeV2(string phone, string aid, string bizType, string content);
        Task<Result> SendValidCodeTest(string phone);
        Task<Result> SendContentTest(string phone, string contentstr);
    }
}
