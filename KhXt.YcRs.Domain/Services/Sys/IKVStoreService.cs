﻿
using KhXt.YcRs.Domain.Entities.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.Services
{
    public interface IKVStoreService : IService
    {
        /// <summary>
        /// 获取用户年级信息
        /// </summary>
        /// <returns></returns>
        List<KeyValueStore> GetClassList();


        #region IComBaseService
        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        KVStoreEntity Get(object id);

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(KVStoreEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(KVStoreEntity entity);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(KVStoreEntity entity);

        /// <summary>
        /// 根据查询条件批量删除
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void Delete(Expression<Func<KVStoreEntity, bool>> predicate);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<KVStoreEntity> Table();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <returns>IQueryable类型的实体集合</returns>
        IQueryable<KVStoreEntity> Fetch(Expression<Func<KVStoreEntity, bool>> predicate, Action<Orderable<KVStoreEntity>> order);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="pageIndex">第几页</param>
        PageList<KVStoreEntity> Gets(Expression<Func<KVStoreEntity, bool>> predicate, Action<Orderable<KVStoreEntity>> order, int pageSize, int pageIndex);

        #endregion
    }
}
