﻿using KhXt.YcRs.Domain.Models.Home;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Sys
{
    public interface ISettingService : IService
    {
        Task<string> SetHomeCourse(long id, int mode, int sort, int picDirect, long[] values);
        Task<List<HomeCourseSettingModel>> GetHomeCourseSetting();
        Task<List<LevelCourseSettingModel>> GetLevelCourseSetting();
        Task<List<LevelCourseListSettingModel>> GetLevelCourselistSetting();

        Task RemoveHomeCourseSetting(long id);
    }
}
