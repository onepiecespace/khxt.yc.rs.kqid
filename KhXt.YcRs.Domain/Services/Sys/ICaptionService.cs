﻿
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Models.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services
{
    public interface ICaptionService : IService
    {

        Result<List<CaptionModel>> GetCaptions(BusinessType businessType);

        CaptionModel GetCaptionsByRankingId(BusinessType businessType, int rankingId);

        Result<List<LevelCaptionModel>> GetLevelCaptions(BusinessType businessType);
        Task<int> GetLevel(int businessType, long exp);
        LevelCaptionModel GetLevelCaptionsByLevel(BusinessType businessType, int level);

    }
}
