﻿using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.Sys;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Sys
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysService : IService
    {
        /// <summary>
        /// 当天
        /// </summary>
        /// <returns></returns>
        Task<Result<UserHourChars>> GetHourChar(string today, long userId);
        /// <summary>
        /// 最近7天
        /// </summary>
        /// <returns></returns>
        Task<Result<UserWeekChars>> GetWeekChar(long userId);
        /// <summary>
        /// 最近一年
        /// </summary>
        /// <returns></returns>
        Task<Result<UserYearChars>> GetYearChar(long userId);
        /// <summary>
        /// 支付统计
        /// </summary>
        /// <param name="aid"> 代理商</param>
        /// <param name="category"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        Task<Result<PayChars>> GetPayChars(long userId, int category, string startTime = "", string endTime = "");

        Task<List<Type>> GetAllServices();

        Task ClearAllServiceCache();
        Task ClearServiceCache(Type serviceImplType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeName">未赋值表示所有节点进行清理</param>
        /// <param name="ServiceType">null表示清理所有service cache,否则只清除指定service cache</param>
        /// <returns></returns>
        Task NoticeClearCache(string nodeName = "", Type ServiceType = null);
        Task NoticeMxhClearCache(string nodeName = "", Type ServiceType = null);

        Task NoticeClearCache(string nodeName = "", int code = 0);
        Task NoticeMxhClearCache(string nodeName = "", int code = 0);
        Task<Dictionary<string, long>> GetAllNodes();
        Task<Dictionary<string, long>> GetActiveNodes();
        Task<Dictionary<string, long>> GetInActiveNodes();

        Task SaveVisit(List<KeyValuePair<string, long>> data);
        Task<VisitStat> GetVisitStat(string path, DateTime? date = null);
        Task<List<VisitStat>> GetAllVisitStat(DateTime? date = null);

        Task<Result<SysOrderPayInfoResult>> GetSysOrderPayList(OrderQueryModel orderQueryModel);

        Task<Result> ExportOrderPay(OrderQueryModel orderQueryModel);

        Task<string> GetServiceDebugInfo(Type serviceType);
        Task<string> GetServiceDebugInfo(int serviceCode);
        bool Valid(string key);

        Task<int> GetGuestStatus();

        Task<bool> SetGuestStatus(int flag);
    }
}
