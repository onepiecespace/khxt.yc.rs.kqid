﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain.Entities.Sys;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.Services.Sys
{
    public interface IUser_ChannelService : IService
    {

        #region IComBaseService
        /// <summary>
        /// 根据Id查询实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        User_ChannelEntity Get(object id);

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="entity">实体</param>
        object Create(User_ChannelEntity entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(User_ChannelEntity entity);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(User_ChannelEntity entity);

        /// <summary>
        /// 根据查询条件批量删除
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void Delete(Expression<Func<User_ChannelEntity, bool>> predicate);

        /// <summary>
        /// 实体集合
        /// </summary>
        IQueryable<User_ChannelEntity> Table();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <returns>IQueryable类型的实体集合</returns>
        IQueryable<User_ChannelEntity> Fetch(Expression<Func<User_ChannelEntity, bool>> predicate, Action<Orderable<User_ChannelEntity>> order);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="order">排序</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="pageIndex">第几页</param>
        PageList<User_ChannelEntity> Gets(Expression<Func<User_ChannelEntity, bool>> predicate, Action<Orderable<User_ChannelEntity>> order, int pageSize, int pageIndex);
        #endregion

    }

}
