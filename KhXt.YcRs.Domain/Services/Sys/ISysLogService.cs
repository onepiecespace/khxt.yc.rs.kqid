﻿using KhXt.YcRs.Domain.Models.Sys;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Sys
{
    /// <summary>
    /// 系统日志统计
    /// </summary>
    public interface ISysLogService : IService
    {
        /// <summary>
        /// 从某天开始
        /// 同步服务器Redis记录 不同步当天
        /// </summary>
        /// <param name="startTime">开始时间</param>
        /// <param name="isClean">是否清理</param>
        /// <returns></returns>
        Task<Result> SyncRedis(string startTime, bool isClean = false);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<Result<SysLogStatistics>> LogsStatistics(int category, string startTime = "", string endTime = "");
    }
}
