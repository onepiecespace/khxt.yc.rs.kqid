﻿
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services.Sys
{
    public interface IVersionService : IService
    {
        Task<List<VersionLog>> GetAll();
        Task<Result<VersionLog>> Get(long id);
        Task<Result<VersionLog>> GetVersionCode(string Version, string Aid);

        Task<Result<VersionInfo>> GetVersionInfo(int osSign, string aid, int currentVersion);
        #region  简单增删改

        /// <summary>
        /// 创建实体
        /// </summary>
        /// <param name="Info">实体</param>
        long Add(VersionLog Info);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Update(VersionLog Info);
        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="Info">实体</param>
        bool Delete(VersionLog Info);
        bool VersionClearCache();

        #endregion

    }

}
