﻿
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Models.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace KhXt.YcRs.Domain.Services
{
    public interface IContentService : IService
    {
        bool addClearCache();
        long Create(ContentModel entity);
        Task<Result<List<AdModel>>> GetAll();
        Task<Result<List<AdModel>>> GetAllAd(ContentType type);

        Task<Result<List<AdModel>>> GetAdsByTag(ContentType type, string tag);
        Task<Result<List<AdModel>>> GetAdsByAction(ContentType type, int action);

        Task<Result<AdModel>> GetAdByTag(ContentType type, string tag);


    }
}
