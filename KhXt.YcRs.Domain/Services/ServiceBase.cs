﻿using Hx;
using Hx.Components;
using Hx.Configurations.Application;
using Hx.Domain.Services;
using Hx.Events.Bus;
using Hx.Extensions;
using Hx.Logging;
using Hx.Serializing;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.Services.Files;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Services
{
    [PropertyWired]
    public abstract class ServiceBase : IService
    {
        private ILogger _logger;

        private const string RES_SERVER_FORMAT = "http://r{0}.maxiaoha.cn/";

        public HxAppSettings AppSetting { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public OssOptions OssOptions { get; set; }

        protected string NodeName => AppSetting.NodeName;

        /// <summary>
        /// 日志工厂
        /// </summary>
        public ILoggerFactory LoggerFactory { get; set; }

        /// <summary>
        /// guid生成器
        /// </summary>
        public IGuidGenerator GuidGenerator { get; set; }

        /// <summary>
        /// Uow服务调用
        /// </summary>
        public IUnitOfWorkService UnitOfWorkService { get; set; }

        public IEventBusProxy EventBusProxy { get; set; }
        /// <summary>
        /// 序列化工具
        /// </summary>
        public ISerializer Serializer { get; set; }

        /// <summary>
        /// 日志
        /// </summary>
        //protected ILogger Logger => _logger ?? (_logger = LoggerFactory.Create(GetType()));
        protected ILogger Logger => _logger ?? (_logger = LoggerFactory.Create());

        private Hx.Redis.RedisHelper _redis;

        protected Hx.Redis.RedisHelper Redis
        {
            get
            {
                if (_redis != null) return _redis;
                _redis = ObjectContainer.Resolve<Hx.Redis.RedisHelper>();
                return _redis;
            }
        }

        private DomainSetting _domainSetting;
        protected DomainSetting DomainSetting
        {
            get
            {
                if (_domainSetting != null) return _domainSetting;
                _domainSetting = ObjectContainer.Resolve<DomainSetting>();
                return _domainSetting;
            }
        }

        public virtual void ClearCache()
        {

        }

        protected string GetConfig(string key)
        {
            string value;
            switch (key)
            {
                case "imgserver":
                    //value = AppSettings.Parameters["resServer"];
                    value = DomainSetting.ResourceUrl;
                    //var idx = Hx.Util.GetRandomNumber(1, 5);
                    //value = string.Format(RES_SERVER_FORMAT, idx);
                    break;
                default:
                    return "";
            }

            return value;
        }

        protected string ResUrl(string path)
        {
            if (path.Contains(DomainSetting.ResourceUrl))
                return path;
            return $"{DomainSetting.ResourceUrl}{path}";
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string RemoveResUrl(string path)
        {

            if (path.IsNotNullOrEmpty() && path.StartsWith(DomainSetting.ResourceUrl))
            {
                path = path.Replace(DomainSetting.ResourceUrl, "").Trim();
                return path;
            }
            else
                return path;
        }
        /// <summary>
        /// OssUrl
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        protected string RemoveOssUrl(string path)
        {
            if (path.IsNotNullOrEmpty() && path.StartsWith($"https://{OssOptions.Bucket}/"))
            {
                path = path.Replace($"https://{OssOptions.Bucket}/", "").Trim();
                return path;
            }
            else
                return path;
        }

        protected async Task<string[]> GetSysSetting(string settingKey, System.Func<string[]> init = null)
        {
            var key = RedisKeyHelper.HASH_SYS_SETTING;
            var settingValue = await Redis.HashGetAsync(key, settingKey);
            if (settingValue.IsNullOrEmpty())
            {
                if (init != null)
                {
                    var value = init();
                    await Redis.HashSetAsync(key, settingKey, value.JoinAsString(","));
                    return value;
                }
                return null;
            }
            return settingValue.Split(",");
        }


        /// <summary>
        /// OssUrl
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        protected string OssUrl(string path)
        {
            return $"https://{OssOptions.Bucket}/{path}";
        }


        public Task ClearCacheAsync()
        {
            throw new System.NotImplementedException();
        }

        public void DoSync(string sourceNodeName, IDomainEventData eventData)
        {
            var isSourceNode = sourceNodeName.Equals(NodeName, System.StringComparison.OrdinalIgnoreCase);

            Logger.Debug($"节点:{NodeName} 收到同步来自节点:{sourceNodeName}的消息:{Serializer.Serialize(eventData)}");

            DoSync(eventData, isSourceNode);
        }

        public async Task DoSyncAsync(string sourceNodeName, IDomainEventData eventData)
        {
            await Task.Yield();
            DoSync(sourceNodeName, eventData);
        }

        protected virtual void DoSync(IDomainEventData eventData)
        {

        }

        protected virtual void DoSync(IDomainEventData eventData, bool isSourceNode)
        {
            if (isSourceNode) return;
            try
            {
                DoSync(eventData);
            }
            catch (System.Exception ex)
            {
                Logger.Error("sync ", ex);
            }
        }


        protected void Trigger(IEventData eventData)
        {
            EventBusProxy.Trigger(eventData.GetType(), NodeName, eventData);
        }
        protected async Task TriggerAsync(IEventData eventData)
        {
            await EventBusProxy.TriggerAsync(eventData.GetType(), NodeName, eventData);
        }

        public virtual async Task<string> DebugAsync()
        {
            return await Task.FromResult("");
        }

        public virtual async Task InitAsync()
        {
            await Task.CompletedTask;
        }

        private HxAppSettings _appSettings;

        private HxAppSettings AppSettings
        {
            get
            {
                if (_appSettings != null) return _appSettings;
                _appSettings = ObjectContainer.Resolve<HxAppSettings>();
                return _appSettings;
            }
        }

        #region   HttpClientHelper 调用第三方接口通用方法
        protected static async Task<string> HttpPost<TData>(string url, string token, TData data)
        {
            return await HttpClientHelper.HttpPost(url, token, data);
        }

        protected static async Task<string> HttpPost<TData>(string url, TData data, Dictionary<string, string> headers = null)
        {
            return await HttpClientHelper.HttpPost(url, data, headers);
        }
        protected static async Task<TResult> HttpPost<TData, TResult>(string url, string token, TData data) where TResult : class
        {
            return await HttpClientHelper.HttpPost<TData, TResult>(url, token, data);
        }

        protected static async Task<TResult> HttpPost<TData, TResult>(string url, TData data, Dictionary<string, string> headers = null) where TResult : class
        {
            return await HttpClientHelper.HttpPost<TData, TResult>(url, data, headers);
        }

        protected static async Task<Result> HttpPostResult<TData>(string url, TData data, Dictionary<string, string> headers = null)
        {
            return await HttpClientHelper.HttpPost<TData, Result>(url, data, headers);
        }

        protected static async Task<string> HttpPostForm(string url, string token, Dictionary<string, string> data)
        {
            return await HttpClientHelper.HttpPostForm(url, token, data);
        }

        protected static async Task<string> HttpPostForm(string url, Dictionary<string, string> data, Dictionary<string, string> headers = null)
        {
            return await HttpClientHelper.HttpPostForm(url, data, headers);
        }
        protected static async Task<TResult> HttpPostForm<TResult>(string url, string token, Dictionary<string, string> data) where TResult : class
        {
            return await HttpClientHelper.HttpPostForm<TResult>(url, token, data);
        }
        protected static async Task<Result> HttpPostFormResult(string url, Dictionary<string, string> data, Dictionary<string, string> headers = null)
        {
            return await HttpClientHelper.HttpPostForm<Result>(url, data, headers);
        }
        protected static async Task<TResult> HttpPostForm<TResult>(string url, Dictionary<string, string> data, Dictionary<string, string> headers = null) where TResult : class
        {
            return await HttpClientHelper.HttpPostForm<TResult>(url, data, headers);
        }

        protected static async Task<string> HttpGet(string url, string token)
        {
            return await HttpClientHelper.HttpGet(url, token);
        }
        protected static async Task<string> HttpGet(string url, Dictionary<string, string> headers = null)
        {
            return await HttpClientHelper.HttpGet(url, headers);
        }

        protected static async Task<T> HttpGet<T>(string url, string token) where T : class
        {
            return await HttpClientHelper.HttpGet<T>(url, token);
        }

        protected static async Task<Result> HttpGetResult(string url, Dictionary<string, string> headers = null)
        {
            return await HttpGet<Result>(url, headers);
        }

        protected static async Task<T> HttpGet<T>(string url, Dictionary<string, string> headers = null) where T : class
        {
            return await HttpClientHelper.HttpGet<T>(url, headers);
        }

        protected static async Task<bool> HttpDelete(string url, Dictionary<string, string> headers = null)
        {
            return await HttpClientHelper.HttpDelete(url, headers);
        }

        protected static Dictionary<string, string> CreateHeader(string token = null)
        {
            var header = new Dictionary<string, string> {
                {"ver","0" },//版本编译号 18 19 20
                {"aid","college" },//代理商 mxh bbk dsl
                {"os","4" },//操作系统类型 1苹果 2 安卓 4 PC
                {"tid","0" },//租户编号
                {"appcode","20" }
            };
            if (token.IsNotNullOrEmpty()) header.Add("token", token);
            return header;
        }
        #endregion
    }
}
