﻿using DapperExtensions.Sql;
using Hx.Components;
using Hx.Dapper;
using Hx.Events.Bus;
using Hx.Logging;
using Hx.Modules;
using Hx.MongoDb;
using Hx.ObjectMapping;
using Hx.QuartzCore;
using Hx.Redis;
using Hx.Runtime.Caching.Memory;
using Hx.Runtime.LoadNode;
using Hx.Serializing;
using KhXt.YcRs.Domain.EventHandler;
using KhXt.YcRs.Domain.Jobs;
using KhXt.YcRs.Domain.Util;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using Quartz;
using System;
using System.Net.Http;

namespace KhXt.YcRs.Domain
{
    [DependsOn(typeof(BasicModule)
        , typeof(AutoMapperModule)
        , typeof(DapperModule)
        , typeof(RedisModule)
        , typeof(DirectMemoryCacheModule)
        //, typeof(QuartzModule)
        , typeof(MongoDbModule)
    )]
    public class DomainModule : ModuleBase
    {
        public override void PreInitialize()
        {
            Register<DomainSetting>();
            //RegisterJobs();
        }

        public override void Initialize()
        {
            var config = ObjectContainer.Resolve<IDapperConfiguration>();
            config.AddDbConnectionProvider<MysqlDbConnectionProvider>("mysql");
            DapperExtensions.DapperExtensions.SqlDialect = new MySqlDialect();
            try
            {
                BsonSerializer.RegisterSerializer(typeof(DateTime), new DateTimeSerializer(DateTimeKind.Local));
            }
            catch { }

        }

        public override void PostInitialize()
        {

            InitDomainEnv();
            EventHandlerRegister();
            //CreateJobs();

        }

        private void EventHandlerRegister()
        {
            var eventBus = Resolve<IEventBus>();

            eventBus.Register(new UserEventHandler());
            eventBus.Register(new UserLevelEventHandler());
            eventBus.Register(new VersionEventHandler());

            eventBus.Register(new CourseEventHandler());
            eventBus.Register(new CourseBycollectEventHandler());
            eventBus.Register(new CourseChildEventHandler());
            eventBus.Register(new CourseUserItemEventHandler());
            eventBus.Register(new CourseItemEventHandler());
            eventBus.Register(new CourseUserChildEventHandler());


            eventBus.Register(new ClearCacheEventHandler());
            eventBus.Register(new UserVipEventHandler());
            eventBus.Register(new DomainEventHandler());

        }


        private void InitDomainEnv()
        {
            DomainEnv.NodeName = Resolve<INodeAccessor>().GetNodeName();
            DomainEnv.CommonLogger = Resolve<ILoggerFactory>().Create();
            //HttpClientHelper.Serializer = Resolve<ISerializer>();
            //HttpClientHelper.HttpClientFactory = Resolve<IHttpClientFactory>();
            //HttpClientHelper.Logger = Resolve<ILoggerFactory>().Create("httpclient");
        }

        private void RegisterJobs()
        {
            Register<HealthReportJob>();
        }

        private void CreateJobs()
        {
            var schMgr = Resolve<IQuartzScheduleJobManager>();
            schMgr.ScheduleAsync<HealthReportJob>(job =>
            {
                job.WithIdentity("job_nhc", "grp_nhc");
            }, tr =>
            {
                tr.
                    StartAt(DateTimeOffset.Now)
                    .WithSimpleSchedule(x =>
                    {
                        x.
                            WithIntervalInMinutes(1)
                            .RepeatForever();
                    });
            });
        }
    }
}
