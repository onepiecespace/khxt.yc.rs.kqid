﻿namespace KhXt.YcRs.Domain
{
    public class CacheableResult<T> : Result<T>
    {
        public CacheableResult(ResultCode code = ResultCode.Success, string message = "", T data = default, long expire = 3600) : base(code, message, data)
        {
            Expire = expire;
        }

        public long Expire { get; set; }
    }
}
