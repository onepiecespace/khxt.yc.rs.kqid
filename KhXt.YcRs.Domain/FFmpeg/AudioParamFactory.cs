﻿using Hx.Components;
using Hx.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace KhXt.YcRs.Domain.FFmpeg
{
    public class AudioParamFactory
    {
        private readonly string _rootPath;
        private DomainSetting _domainSetting;
        public AudioParamFactory()
        {
            if (_domainSetting != null) return;
            _domainSetting = ObjectContainer.Resolve<DomainSetting>();
            _rootPath = _domainSetting.UploadPath;
        }


        /// <summary>
        /// 調整音量大小
        /// </summary>
        /// <param name="inputFilePath"></param>
        /// <param name="outPutFile"></param>
        /// <param name="volumeSize"></param>
        /// <returns></returns>
        public AudioParamConstructor AdjustVolume(string inputFilePath, string outPutFile, int volumeSize = 100)
        {
            return new AudioParamConstructor
            {
                Params = $"-i {inputFilePath} " +
                           $"-vol {volumeSize} {outPutFile} -y",
                NewFileName = outPutFile
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputFilePath"></param>
        /// <param name="outPutFile"></param>
        /// <returns></returns>
        public AudioParamConstructor PCMToMp3(string inputFilePath, string outPutFile)
        {
            try
            {
                //FileInfo inputFileInfo = new FileInfo(inputFilePath);
                //if (!inputFileInfo.Exists)
                //{
                //    throw new Exception($"PCM文件异常,文件{inputFilePath}不存在");
                //}
                FileInfo file = new FileInfo(outPutFile);
                if (file.Directory != null && !file.Directory.Exists)
                    file.Directory.Create();
                return new AudioParamConstructor
                {
                    Params = "-y -f s16le -ar 16k -ac 1 -i " +
                             $"{inputFilePath} " +
                             $"{outPutFile} ",
                    NewFileName = outPutFile
                };
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }

        }

        /// <summary>
        /// 合并两个音频文件
        /// </summary>
        /// <param name="inputFile1"></param>
        /// <param name="inputFile2"></param>
        /// <param name="outPutFile"></param>
        /// <returns></returns>
        public AudioParamConstructor MergeTwoAudio(string inputFile1, string inputFile2, string outPutFile)
        {
            try
            {
                //FileInfo inputFileInfo = new FileInfo(inputFile1);
                //if (!inputFileInfo.Exists)
                //{
                //    throw new Exception("原声文件不存在");
                //}
                //FileInfo bgFileInfo = new FileInfo(inputFile2);
                //if (!inputFileInfo.Exists)
                //{
                //    throw new Exception("背景音乐文件不存在");
                //}

                FileInfo file = new FileInfo(outPutFile);
                if (file.Directory != null && !file.Directory.Exists)
                    file.Directory.Create();
                return new AudioParamConstructor
                {
                    Params = $"-i {inputFile1} " +
                             $"-i {inputFile2} " +
                             "-filter_complex amix=inputs=2:duration=longest:dropout_transition=0 -f mp3 -y " +
                             $"{outPutFile}",
                    NewFileName = outPutFile
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// 拆分音频文件
        /// </summary>
        /// <param name="inputFile1"></param>
        /// <param name="outPutFile"></param>
        /// <param name="startTime"></param>
        /// <param name="durtionTime"></param>
        /// <returns></returns>
        public AudioParamConstructor InterceptAudio(string inputFile1, string outPutFile, TimeSpan startTime, TimeSpan durtionTime)
        {
            return new AudioParamConstructor
            {
                Params = $"-i {inputFile1} -vn -acodec copy -ss " +
                           $"{startTime.Hours:00}:{startTime.Minutes:00}:{startTime.Seconds:00}.{startTime.Milliseconds:000} -t " +
                           $"{durtionTime.Hours:00}:{durtionTime.Minutes:00}:{durtionTime.Seconds:00}.{durtionTime.Milliseconds:000} " +
                           $"{outPutFile}",
                NewFileName = outPutFile
            };
        }

        /// <summary>
        /// 拼接多个音频文件
        /// </summary>
        /// <param name="inputList"></param>
        /// <param name="outPutFile"></param>
        /// <returns></returns>
        public AudioParamConstructor SplicingAudio(IEnumerable<string> inputList, string outPutFile)
        {
            var splic = inputList.Aggregate("", (current, input) => current + input + "|");
            splic = splic.Remove(splic.Length - 1, 1);
            splic = $"\"concat:{splic}\"";
            return new AudioParamConstructor
            {
                Params = $"-i {splic} -acodec copy {outPutFile}",
                NewFileName = outPutFile
            };
        }

        /// <summary>
        /// 获取音频文件信息
        /// </summary>
        /// <param name="inputFile"></param>
        /// <returns></returns>
        public string GetFileInfo(string inputFile)
        {
            return $"-i {inputFile} -print_format json -v 0 -show_format";
        }

        /// <summary>
        /// 键入效果
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="startSecends"></param>
        /// <param name="outPutFile"></param>
        /// <returns></returns>
        public AudioParamConstructor FadeIn(string inputFile, int startSecends, string outPutFile)
        {
            return new AudioParamConstructor
            {
                Params = $"-i {inputFile} -filter_complex afade=t=in:ss={startSecends}:d=2 {outPutFile}",
                NewFileName = outPutFile
            };
        }

        /// <summary>
        /// 渐出效果
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="startSecends"></param>
        /// <param name="outputFile"></param>
        /// <returns></returns>
        public AudioParamConstructor FadeOut(string inputFile, int startSecends, string outputFile)
        {
            return new AudioParamConstructor
            {
                Params = $"-i {inputFile} -filter_complex afade=t=out:st={startSecends}:d=2 {outputFile}",
                NewFileName = outputFile
            };
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class AudioParamConstructor
    {
        /// <summary>
        /// 
        /// </summary>
        public string Params { get; internal set; }
        /// <summary>
        /// 
        /// </summary>
        public string NewFileName { get; internal set; }
    }
}
