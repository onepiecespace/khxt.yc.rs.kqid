﻿using Hx.Configurations.Application;
using Hx.Extensions;
using Hx.Logging;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace KhXt.YcRs.Domain
{
    public static class DomainEnv
    {

        public static string NodeName { get; internal set; }
        public static HxAppSettings AppSetting { get; internal set; }
        public static MqSetting Mq { get; internal set; }
        public static IEnumerable<RedisConnectionSetting> Redis { get; internal set; }
        public static DbSetting Db { get; internal set; }
        public static ConventionSetting Convention { get; internal set; }
        public static Dictionary<string, string> Parameters { get; internal set; }
        public static Dictionary<string, string[]> LoadBalancing { get; internal set; }
        public static int[] MinThreads { get; internal set; }
        public static int[] MaxThreads { get; internal set; }

        public static string OS
        {
            get
            {
                return RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "Windows" : RuntimeInformation.IsOSPlatform(OSPlatform.Linux) ? "Linux" : RuntimeInformation.IsOSPlatform(OSPlatform.OSX) ? "OSX" : "Other";
            }
        }
        public static string OSVersion
        {
            get
            {
                return Environment.OSVersion.VersionString;
            }
        }


        public static string GetAidName(string aid)
        {
            var aidName = "";
            switch (aid)
            {
                case "mxh":
                    aidName = "两个黄鹂";
                    break;
                case "dufu":
                    aidName = "杜甫语文";
                    break;
                case "college":
                    aidName = "黄鹂商学院";
                    break;
                case "bbk":
                    aidName = "步步高";
                    break;
                case "dsl":
                    aidName = "读书郎";
                    break;
                default:
                    aidName = "";
                    break;
            }
            return aidName;
        }
        public static ILogger CommonLogger { get; internal set; } = Hx.Logging.EmptyLogger.Instance;
    }
}
