﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.ValueObject.Vip
{
    /// <summary>
    /// 模块特权
    /// </summary>
    public class Privilege
    {
        /// <summary>
        /// 
        /// </summary>
        public List<PrivilegeInfo> Privileges { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PrivilegeInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int BusinessType { get; set; }
        /// <summary>
        /// 篇数
        /// </summary>
        public int Count { get; set; }
    }
}
