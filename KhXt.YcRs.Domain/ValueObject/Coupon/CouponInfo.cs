﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.ValueObject.Coupon
{
    /// <summary>
    /// 优惠券信息
    /// </summary>
    public class CouponInfo
    {
        /// <summary>
        /// 优惠券信息
        /// </summary>
        public List<Coupon> Coupons { get; set; }
    }
    /// <summary>
    /// 优惠券
    /// </summary>
    public class Coupon
    {
        /// <summary>
        /// 优惠券Id
        /// </summary>
        public long CouponId { get; set; }
        /// <summary>
        /// 优惠券数量
        /// </summary>
        public int Count { get; set; }
    }
}
