﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.ValueObject.Recite
{
    /// <summary>
    /// 段落内容
    /// </summary>
    public class SectionInfo
    {
        /// <summary>
        /// 段落下所有句子
        /// </summary>
        public List<Sentence> Sentences { get; set; }

    }

    public class Sentence
    {
        /// <summary>
        /// 顺序Id 01
        /// </summary>
        public string SortId { get; set; }
        /// <summary>
        /// 开始
        /// </summary>
        public string Start { get; set; }
        /// <summary>
        /// 毫秒数
        /// </summary>
        public double StartTimes { get; set; }

        /// <summary>
        /// 图片 默认从1开始
        /// </summary>
        public string Img { get; set; }
        /// <summary>
        /// 句子
        /// </summary>
        public string Text { get; set; }
    }
}
