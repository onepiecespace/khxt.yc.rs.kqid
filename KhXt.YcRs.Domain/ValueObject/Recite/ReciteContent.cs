﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Text;

namespace KhXt.YcRs.Domain.ValueObject.Recite
{
    /// <summary>
    /// 背诵内容
    /// </summary>
    public class ReciteContent
    {
        /// <summary>
        /// 课程所有段落信息
        /// </summary>
        public List<ReciteSectionContent> ReciteSectionContents { get; set; }
    }
    /// <summary>
    /// 背诵段落内容
    /// </summary>
    public class ReciteSectionContent
    {
        /// <summary>
        /// 段落Id
        /// </summary>
        public string SectionId { get; set; }
        /// <summary>
        /// 背诵的内容
        /// </summary>
        public string ReciteResultInfo { get; set; }
        /// <summary>
        /// 原文
        /// </summary>
        public string ReciteSectionInfo { get; set; }
        /// <summary>
        /// 正确率
        /// </summary>
        public double CorrectRate { get; set; }
        /// <summary>
        /// 正确率
        /// </summary>
        public double BestCorrectRate { get; set; }
        /// <summary>
        /// 次数
        /// </summary>
        public long ReciteCount { get; set; }
        /// <summary>
        /// 最好的分数
        /// </summary>
        public double BestScore { get; set; }
        /// <summary>
        /// 最后的分数
        /// </summary>
        public double Score { get; set; }
        /// <summary>
        /// 比重
        /// </summary>
        public double Proportion { get; set; }

        /// <summary>
        /// html
        /// </summary>
        public string HtmlTxt1 { get; set; }
        /// <summary>
        /// html
        /// </summary>
        public string HtmlTxt2 { get; set; }

    }
}
