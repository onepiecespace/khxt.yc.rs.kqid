﻿using Hx.Events.Bus;

namespace KhXt.YcRs.Domain.EventData
{
    /// <summary>
    /// 通用型domain事件数据，可减少eventdata数量
    /// </summary>
    public class DomainEventData : EventData<string>, IDomainEventData
    {
        /// <summary>
        /// 编号规则： Consts.ServiceDictionary中的服务编号 再加两位EventDataMethod中的操作类型编号 add 1,del 2,update 3
        /// 例：
        /// 78001：teacherService add
        /// 78002：teacherService del
        /// </summary>
        public int Method { get; set; }
    }
}
