﻿using Hx.Events.Bus;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.EventData.Test
{
    public class PersonCreatedEventData : EventData<string>, IDomainEventData
    {
    }

    public class PersonChangedEventData : EventData<string>, IDomainEventData
    {
    }

    public class PersonEventData : EventData<string>, IDomainEventData
    {
        /// <summary>
        /// 1:add 2:del 3:update 4......
        /// </summary>
        public int Method { get; set; }
    }
}
