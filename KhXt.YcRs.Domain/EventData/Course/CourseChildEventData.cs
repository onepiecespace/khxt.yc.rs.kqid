﻿using Hx.Events.Bus;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KhXt.YcRs.Domain.EventData.Course
{
    /// <summary>
    /// CourseChildEntity
    /// </summary>
    public class CourseChildEventData : EventData<CourseChildEntity>, IDomainEventData
    {
        /// <summary>
        ///  执行方法
        /// </summary>
        public int Method { get; set; }
    }
}
