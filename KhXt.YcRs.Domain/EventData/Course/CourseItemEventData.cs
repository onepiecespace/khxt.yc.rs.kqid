﻿using Hx.Events.Bus;
using KhXt.YcRs.Domain.Entities.Course;

namespace KhXt.YcRs.Domain.EventData.Course
{
    /// <summary>
    /// 
    /// </summary>
    public class CourseItemEventData : EventData<CourseItemEntity>, IDomainEventData
    {
        /// <summary>
        /// 1:add 2:del 3:update 4......
        /// </summary>
        public int Method { get; set; }
    }
}
