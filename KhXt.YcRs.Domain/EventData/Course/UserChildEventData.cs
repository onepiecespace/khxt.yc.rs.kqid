﻿using Hx.Events.Bus;
using KhXt.YcRs.Domain.Entities.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KhXt.YcRs.Domain.EventData.Course
{
    /// <summary>
    /// 
    /// </summary>
    public class UserChildEventData : EventData<UserCourseChildProgessEntity>, IDomainEventData
    {
        /// <summary>
        ///  执行方法
        /// </summary>
        public int Method { get; set; }
    }
}
