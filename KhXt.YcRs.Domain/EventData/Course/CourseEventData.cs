﻿using Hx.Events.Bus;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KhXt.YcRs.Domain.EventData.Course
{
    /// <summary>
    /// CourseEntity
    /// </summary>
    public class CourseEventData : EventData<CourseEntity>, IDomainEventData
    {
        /// <summary>
        ///  执行方法
        /// </summary>
        public int Method { get; set; }
    }
}
