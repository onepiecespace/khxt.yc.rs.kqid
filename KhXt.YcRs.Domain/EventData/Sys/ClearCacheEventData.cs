﻿using Hx.Events.Bus;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.EventData.Sys
{
    public class ClearCacheEventData : EventData<string>, IDomainEventData
    {
        public Type ServiceType { get; set; }
        public string NodeName { get; set; }
    }
}
