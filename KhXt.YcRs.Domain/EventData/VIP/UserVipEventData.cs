﻿using Hx.Events.Bus;
using KhXt.YcRs.Domain.Models.Vip;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.EventData.VIP
{
    public class UserVipEventData : EventData<UserVipModel>, IDomainEventData
    {
        /// <summary>
        /// 1:add 2:del 3:update 4......
        /// </summary>
        public int Method { get; set; }
    }
}
