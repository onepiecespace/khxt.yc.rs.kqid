﻿using Hx.Events.Bus;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.EventData.User
{
    /// <summary>
    /// 
    /// </summary>
    public class VersionEventData : EventData<VersionLog>, IDomainEventData
    {
        /// <summary>
        /// 1:add 2:del 3:update 4......
        /// </summary>
        public int Method { get; set; }
    }
}
