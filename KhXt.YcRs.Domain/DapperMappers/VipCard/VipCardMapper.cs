﻿using KhXt.YcRs.Domain.Entities.VipCard;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.DapperMappers.VipCard
{
    public class VipCardMapper : MapperBase<VipCardEntity, string>
    {
        public VipCardMapper() : base("u_vipcard")
        {

        }
    }
}
