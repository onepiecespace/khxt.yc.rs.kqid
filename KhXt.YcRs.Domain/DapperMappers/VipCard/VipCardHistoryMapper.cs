﻿using KhXt.YcRs.Domain.Entities.VipCard;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.DapperMappers.VipCard
{
    public class VipCardHistoryMapper : MapperBase<VipCardHistoryEntity, string>
    {
        public VipCardHistoryMapper() : base("u_vipcard_history")
        {
        }
    }
}
