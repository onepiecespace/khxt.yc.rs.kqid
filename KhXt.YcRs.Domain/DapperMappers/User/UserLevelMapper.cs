﻿using KhXt.YcRs.Domain.Entities.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.DapperMappers.User
{
    public class UserLevelMapper : MapperBase<UserLeveEntity, long>
    {
        public UserLevelMapper() : base("a_user_level")
        {

        }
    }
}
