﻿using KhXt.YcRs.Domain.Entities.User;

namespace KhXt.YcRs.Domain.DapperMappers.User
{
    public class OrganizationMapper : MapperBase<OrganizationEntity, long>
    {
        public OrganizationMapper() : base("a_User_Organization")
        {

        }
    }




}
