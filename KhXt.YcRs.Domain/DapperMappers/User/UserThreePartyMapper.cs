﻿using KhXt.YcRs.Domain.Entities.User;

namespace KhXt.YcRs.Domain.DapperMappers.User
{
    public class UserThreePartyMapper : MapperBase<UserThreePartyEntity, long>
    {
        public UserThreePartyMapper() : base("a_User_ThreeParty")
        {

        }
    }




}
