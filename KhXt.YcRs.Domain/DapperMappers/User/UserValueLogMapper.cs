﻿using DapperExtensions.Mapper;
using KhXt.YcRs.Domain.Entities.User;
using System;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.DapperMappers.User
{
    public class UserValueLogMapper : MapperBase<UserValueLogEntity, long>
    {
        public UserValueLogMapper() : base("u_valuelog")
        {
        }
        protected override void MapConfig(Func<Expression<Func<UserValueLogEntity, object>>, PropertyMap> map)
        {
            //map(x => x.Userid == x.Id);
        }
    }
}
