﻿using DapperExtensions.Mapper;
using KhXt.YcRs.Domain.Entities.User;
using System;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.DapperMappers.User
{
    public class UserMapper : MapperBase<UserEntity, long>
    {
        public UserMapper() : base("a_User")
        {
        }
        protected override void MapConfig(Func<Expression<Func<UserEntity, object>>, PropertyMap> map)
        {
            //map(x => x.Userid == x.Id);
        }
    }
}
