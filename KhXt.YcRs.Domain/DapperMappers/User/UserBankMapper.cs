﻿using DapperExtensions.Mapper;
using KhXt.YcRs.Domain.Entities;
using KhXt.YcRs.Domain.Entities.User;
using System;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.DapperMappers.User
{
    public class UserBankMapper : MapperBase<UserBankEntity, long>
    {
        public UserBankMapper() : base("a_User_Bank")
        {

        }
    }
    /// <summary>
    /// Feedback  FeedbackEntity
    /// </summary>
    public class FeedbackMapper : MapperBase<FeedbackEntity, long>
    {
        public FeedbackMapper() : base("a_Feedback")
        {

        }
    }





}
