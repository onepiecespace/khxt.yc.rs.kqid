﻿using KhXt.YcRs.Domain.Entities.Course;

namespace KhXt.YcRs.Domain.DapperMappers.Course
{
    public class CourseMapper : MapperBase<CourseEntity, long>
    {
        public CourseMapper() : base("a_Course")
        {

        }
    }

    public class CourseAppointmentMapper : MapperBase<CourseAppointmentEntity, long>
    {
        public CourseAppointmentMapper() : base("course_appo")
        {

        }
    }




}
