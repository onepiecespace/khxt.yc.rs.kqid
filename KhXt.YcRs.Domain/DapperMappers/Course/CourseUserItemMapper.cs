﻿using KhXt.YcRs.Domain.Entities.Course;

namespace KhXt.YcRs.Domain.DapperMappers.Course
{
    public class CourseUserItemMapper : MapperBase<CourseUserItemEntity, long>
    {
        public CourseUserItemMapper() : base("course_user_item")
        {

        }
    }




}
