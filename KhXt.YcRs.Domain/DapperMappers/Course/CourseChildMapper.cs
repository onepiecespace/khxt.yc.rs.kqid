﻿using KhXt.YcRs.Domain.Entities.Course;

namespace KhXt.YcRs.Domain.DapperMappers.Course
{
    public class CourseChildMapper : MapperBase<CourseChildEntity, long>
    {
        public CourseChildMapper() : base("a_Course_Child")
        {

        }
    }




}
