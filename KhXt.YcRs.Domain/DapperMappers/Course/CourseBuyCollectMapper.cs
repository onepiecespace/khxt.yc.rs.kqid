﻿using KhXt.YcRs.Domain.Entities.Course;

namespace KhXt.YcRs.Domain.DapperMappers.Course
{
    public class CourseBuyCollectMapper : MapperBase<CourseBuyCollectEntity, long>
    {
        public CourseBuyCollectMapper() : base("a_Course_Buy_Collect")
        {

        }
    }




}
