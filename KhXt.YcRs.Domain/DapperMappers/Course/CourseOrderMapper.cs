﻿using KhXt.YcRs.Domain.Entities.Course;

namespace KhXt.YcRs.Domain.DapperMappers.Course
{
    public class CourseOrderMapper : MapperBase<CourseOrderEntity, long>
    {
        public CourseOrderMapper() : base("a_Course_Order")
        {

        }
    }




}
