﻿using KhXt.YcRs.Domain.Entities.Course;

namespace KhXt.YcRs.Domain.DapperMappers.Course
{
    public class CourseItemMapper : MapperBase<CourseItemEntity, long>
    {
        public CourseItemMapper() : base("a_Course_Item")
        {

        }
    }




}
