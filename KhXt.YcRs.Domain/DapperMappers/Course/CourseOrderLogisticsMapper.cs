﻿using KhXt.YcRs.Domain.Entities.Course;

namespace KhXt.YcRs.Domain.DapperMappers.Course
{
    public class CourseOrderLogisticsMapper : MapperBase<CourseOrderLogisticsEntity, long>
    {
        public CourseOrderLogisticsMapper() : base("a_Course_Order_Logistics")
        {

        }
    }




}
