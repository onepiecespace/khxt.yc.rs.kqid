﻿using DapperExtensions.Mapper;
using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.DapperMappers
{
    public abstract class MapperBase<TEntity, TKey> : ClassMapper<TEntity> where TEntity : Entity<TKey>
    {
        protected MapperBase(string tableName, bool isTriggerIdentity = false)
        {
            Table(tableName);
            MapConfig(Map);
            if (isTriggerIdentity)
            {
                Map(x => x.Id).Key(KeyType.TriggerIdentity);
            }
            AutoMap();
        }

        protected virtual void MapConfig(Func<System.Linq.Expressions.Expression<Func<TEntity, object>>, PropertyMap> map)
        {
        }
    }
}
