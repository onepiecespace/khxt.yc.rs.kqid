﻿using KhXt.YcRs.Domain.Entities.WalletLogs;

namespace KhXt.YcRs.Domain.DapperMappers.WalletLogs
{
    public class WalletLogsMapper : MapperBase<WalletLogsEntity, long>
    {
        public WalletLogsMapper() : base("wallet_log")
        {

        }
    }




}
