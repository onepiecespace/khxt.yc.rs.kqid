﻿using KhXt.YcRs.Domain.Entities.Order;

namespace KhXt.YcRs.Domain.DapperMappers.Order
{
    public class OrderCouponMapper : MapperBase<OrderCouponEntity, long>
    {
        public OrderCouponMapper() : base("order_coupon")
        {

        }
    }




}
