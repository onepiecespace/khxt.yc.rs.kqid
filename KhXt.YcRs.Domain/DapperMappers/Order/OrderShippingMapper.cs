﻿using KhXt.YcRs.Domain.Entities.Order;

namespace KhXt.YcRs.Domain.DapperMappers.Order
{
    public class OrderShippingMapper : MapperBase<OrderShippingEntity, long>
    {
        public OrderShippingMapper() : base("order_shipping")
        {

        }
    }




}
