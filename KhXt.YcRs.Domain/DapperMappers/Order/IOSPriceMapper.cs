﻿using KhXt.YcRs.Domain.Entities.Order;

namespace KhXt.YcRs.Domain.DapperMappers.Order
{
    public class IOSPriceMapper : MapperBase<IOSPriceEntity, long>
    {
        public IOSPriceMapper() : base("ios_price")
        {

        }
    }




}
