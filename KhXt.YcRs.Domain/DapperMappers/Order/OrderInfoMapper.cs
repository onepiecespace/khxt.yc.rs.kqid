﻿using KhXt.YcRs.Domain.Entities.Order;

namespace KhXt.YcRs.Domain.DapperMappers.Order
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderInfoMapper : MapperBase<OrderInfoEntity, string>
    {
        /// <summary>
        /// 
        /// </summary>
        public OrderInfoMapper() : base("order_info")
        {

        }
    }




}
