﻿using KhXt.YcRs.Domain.Entities.Order;

namespace KhXt.YcRs.Domain.DapperMappers.Order
{
    public class OrderItemMapper : MapperBase<OrderItemEntity, long>
    {
        public OrderItemMapper() : base("order_item")
        {

        }
    }




}
