﻿using KhXt.YcRs.Domain.Entities.Integrate;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.DapperMappers.Integrate
{
    public class UserIntegrateMapper : MapperBase<UserIntegrateEntity, long>
    {
        public UserIntegrateMapper() : base("user_integrate")
        {

        }
    }
}
