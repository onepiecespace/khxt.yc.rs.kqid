﻿using DapperExtensions.Mapper;
using KhXt.YcRs.Domain.Entities.Sys;

namespace KhXt.YcRs.Domain.DapperMappers.Sys
{
    public class CommentsMapper : ClassMapper<CommentsEntity>
    {
        public CommentsMapper()
        {
            Table("a_Comments");
            AutoMap();
        }
    }
}
