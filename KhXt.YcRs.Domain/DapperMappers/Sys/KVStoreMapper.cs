﻿using DapperExtensions.Mapper;
using KhXt.YcRs.Domain.Entities.Sys;

namespace KhXt.YcRs.Domain.DapperMappers.Sys
{
    public class KVStoreMapper : ClassMapper<KVStoreEntity>
    {
        public KVStoreMapper()
        {
            Table("a_KVStore");
            AutoMap();
        }
    }
}
