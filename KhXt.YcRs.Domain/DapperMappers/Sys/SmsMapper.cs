﻿using KhXt.YcRs.Domain.Entities.Sys;

namespace KhXt.YcRs.Domain.DapperMappers.Sys
{
    public class SmsMapper : MapperBase<SmsEntity, long>
    {
        public SmsMapper() : base("s_sms") { }
    }
}
