﻿using DapperExtensions.Mapper;
using KhXt.YcRs.Domain.Entities.Sys;

namespace KhXt.YcRs.Domain.DapperMappers.Sys
{
    public class CaptionMapper : ClassMapper<CaptionEntity>
    {
        public CaptionMapper()
        {
            Table("s_caption");
            AutoMap();
        }
    }
}
