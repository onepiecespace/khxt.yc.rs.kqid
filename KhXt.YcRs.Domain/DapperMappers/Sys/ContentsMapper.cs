﻿using DapperExtensions.Mapper;
using KhXt.YcRs.Domain.Entities.Sys;

namespace KhXt.YcRs.Domain.DapperMappers.Sys
{
    public class ContentsMapper : ClassMapper<ContentEntity>
    {
        public ContentsMapper()
        {
            Table("s_content");
            AutoMap();
        }
    }
}
