﻿using DapperExtensions.Mapper;
using KhXt.YcRs.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace KhXt.YcRs.Domain.DapperMappers.Test
{
    public class TestMapper : MapperBase<TestEntity, long>
    {
        public TestMapper() : base("a_test")
        {

        }

        protected override void MapConfig(Func<Expression<Func<TestEntity, object>>, PropertyMap> map)
        {

            //map(x => x.Age)
        }
    }
}
