﻿using Hx.Domain.Uow;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace KhXt.YcRs.Domain
{
    public class MysqlDbConnectionProvider : IDbConnectionProvider
    {
        public string Name { get; } = "mysql";

        public IDbConnection Create(string connectionString, bool ifOpen = false)
        {
            var conn = new MySqlConnection { ConnectionString = connectionString };
            if (ifOpen) conn.Open();
            return conn;
        }
    }
}
