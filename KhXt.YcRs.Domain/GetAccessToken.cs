﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain
{
    public class GetAccessToken
    {
        public bool Status { get; set; }
        public string StatusMessage { get; set; }
        public MPAccessTokenInfo Accesstokeninfos { get; set; } = new MPAccessTokenInfo();
    }

    /// <summary>
    /// 自定义一个对外统一的AccessToken
    /// </summary>
    [Serializable]
    public class MPAccessTokenInfo
    {
        /// <summary>
        /// 获取到的凭证
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 凭证到什么时间过期
        /// </summary>
        public DateTime AccessTokenExpireTime { get; set; }

        public string AppId { get; set; }
        public string Timestamp { get; set; }
        public string NonceStr { get; set; }
        public string Signature { get; set; }

        public string Url { get; set; }
    }

    [Serializable]
    public class GetTicket : Result
    {
        public string Ticket { get; set; }

    }

    [Serializable]
    public class Summary : Result
    {
        public string AccessToken { get; set; }
        public string Ticket { get; set; }
    }


    [Serializable]
    public class AccToken
    {
        public string access_token { get; set; }
        public string expires_in { get; set; }
    }

    public class Ticket
    {
        public string errcode { get; set; }
        public string errmsg { get; set; }
        public string ticket { get; set; }
        public string expires_in { get; set; }
    }
}
