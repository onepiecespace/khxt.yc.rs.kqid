﻿using System.ComponentModel;

namespace KhXt.YcRs.Domain
{
    public enum ResultCode
    {
        Success = 200,

        /// <summary>
        /// 非授权信息。请求成功。但token数据不存在
        /// </summary>
        [Description(" 非授权信息。请求成功。但token数据不存在！")]
        NonAuthoritativeInformation = 203,



        [Description("失败")]
        Fail = 400,

        /// <summary>
        /// 新用户，未绑定
        /// </summary>
        [Description("新用户，未绑定")]
        NoBind = 410,

        /// <summary>
        /// 已绑定另一账号
        /// </summary>
        [Description("已绑定过授权登录")]
        AccountErr = 411,



        /// <summary>
        /// 验证码已过期，请重新发送验证码！
        /// </summary>
        [Description("验证码已过期，请重新发送验证码！")]
        ValidCodeExpired = 412,
        /// <summary>
        /// 验证码错误
        /// </summary>
        [Description("验证码错误！！")]
        ValidCodeError = 413,
        /// <summary>
        /// 手机已被已注册
        /// </summary>
        [Description(" 手机已被已注册！")]
        PhoneIsExist = 414,

        [Description("新用户，未绑定")]
        TempAccount = 415,


        [Description("已存在同手机号用户")]
        ExistPhoneUser = 416,



        [Description("速度超限，疑似被攻击")]
        SpeedLimit = 490,

        [Description("余额不足")]
        BalanceNotenough = 421,

        [Description("无效参数")]
        InvalidPara = 422,

        [Description("系统异常")]
        Exception = 500,

        [Description("警告消息提醒")]
        warning = 200300,

        [Description("暂未上线")]
        onlineing = 200301,

        [Description("系统未找到当前用户")]
        NoUser = 10005001,

        [Description("系统未找到会员信息")]
        NoVip = 10005002,

        [Description("开通会员异常")]
        OpenVipException = 10005500,

        [Description("系统未找到课程")]
        NoCourse = 10006001,

        [Description("未找到订单信息")]
        NoCourseOrder = 10007001,

        [Description("钱包充值异常")]
        WalletChargeException = 10008800,

        [Description("钱包充值失败,订单信息不存在")]
        WalletChargeOrderNot = 10008801,

        [Description("钱包充值失败,钱包余额不足")]
        WalletChargeNotEnough = 10008802,

    }
}