﻿using AutoMapper;
using KhXt.YcRs.Domain.Entities.WalletLogs;
using KhXt.YcRs.Domain.Models.Wallet;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Profiles
{
    public class WalletProfile : Profile
    {
        public WalletProfile()
        {
            CreateMap<WalletLogsEntity, WalletLogs>()
                .ForMember(d => d.LogId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.Coin, opt => opt.MapFrom(s => s.Coin.ToString("f2")))
                .ForMember(d => d.CoinBalance, opt => opt.MapFrom(s => s.CoinBalance.ToString("f2")));
        }
    }
}
