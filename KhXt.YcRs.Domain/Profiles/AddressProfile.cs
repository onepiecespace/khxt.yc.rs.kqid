﻿using AutoMapper;
using Hx.Extensions;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Profiles
{
    /// <summary>
    /// 
    /// </summary>
    public class AddressProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public AddressProfile()
        {
            CreateMap<AddressEntity, AddressInfo>()
                .ForMember(d => d.Createtime, opt => opt.MapFrom(s => s.Createtime.ToString10()))
                .ForMember(d => d.Updatetime, opt => opt.MapFrom(s => s.Updatetime.ToString10()));

            CreateMap<AddressInfo, AddressEntity>();
        }
    }
}
