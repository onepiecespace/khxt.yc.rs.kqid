﻿using AutoMapper;
using KhXt.YcRs.Domain.Entities.Log;
using KhXt.YcRs.Domain.Models.Log;

namespace KhXt.YcRs.Domain.Profiles
{
    public class LogProfile : Profile
    {
        public LogProfile()
        {
            CreateMap<LoginLogEntity, LoginLogModel>();
            CreateMap<LoginLogModel, LoginLogEntity>();
        }
    }
}
