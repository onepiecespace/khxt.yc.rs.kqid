﻿using AutoMapper;
using KhXt.YcRs.Domain.Contracts;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Models.Sys;

namespace KhXt.YcRs.Domain.Profiles
{
    public class SysProfile : Profile
    {
        public SysProfile()
        {
            CreateMap<CaptionEntity, CaptionModel>();
            CreateMap<ContentEntity, ContentModel>();
            CreateMap<ContentModel, ContentEntity>();
            CreateMap<ContentEntity, AdModel>()
             .ForMember(d => d.IsRecord, opt => opt.MapFrom(t => t.ContentType));
            CreateMap<SmsContract, SmsEntity>();
            CreateMap<CaptionEntity, LevelCaptionModel>();

            CreateMap<ContentEntity, ContentsQuery>();
            CreateMap<ContentsQuery, ContentEntity>();

            CreateMap<SysLogEntity, HourCount>()
                .ForMember(d => d.Time, opt => opt.MapFrom(s => s.CreateTime.ToString("yyyy-MM-dd")));


        }
    }
}
