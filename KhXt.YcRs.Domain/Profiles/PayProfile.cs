﻿using AutoMapper;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Pay;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Profiles
{
    /// <summary>
    /// 
    /// </summary>
    public class PayProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public PayProfile()
        {
            CreateMap<WeChatPayResult, PayWxInfo>()
                .ForMember(d => d.mch_id, opt => opt.MapFrom(s => s.Prepayid))
                .ForMember(d => d.prepay_id, opt => opt.MapFrom(s => s.Prepayid))
                .ForMember(d => d.AppId, opt => opt.MapFrom(s => s.Appid))
                .ForMember(d => d.TimeStamp, opt => opt.MapFrom(s => s.Timestamp))
                .ForMember(d => d.Package, opt => opt.MapFrom(s => s.Package))
                .ForMember(d => d.nonce_str, opt => opt.MapFrom(s => s.Noncestr))
                .ForMember(d => d.sign, opt => opt.MapFrom(s => s.Sign));
            CreateMap<OrderPayEntity, PayChar>()
                .ForMember(d => d.Time, opt => opt.MapFrom(s => s.CreateTime.ToString("yyyy-MM-dd")))
                .ForMember(d => d.Price, opt => opt.MapFrom(s => double.Parse(s.Price)));


        }

    }
}
