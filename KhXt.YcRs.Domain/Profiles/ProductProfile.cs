﻿using AutoMapper;
using Hx.Extensions;
using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Models.Product;
using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Pro_classEntity, ProCategoryInfo>()
       .ForMember(d => d.add_date, opt => opt.MapFrom(s => s.add_date.ToString("yyyy-MM-dd")));

            CreateMap<Pro_listEntity, ProInfo>()
              .ForMember(d => d.proId, opt => opt.MapFrom(s => s.Id))
             .ForMember(d => d.iscoins, opt => opt.Ignore())
             .ForMember(d => d.coins_price, opt => opt.Ignore())
               .ForMember(d => d.IsSoldout, opt => opt.Ignore());


            CreateMap<ProInfo, ProInfoDto>();

            CreateMap<ProInfo, ProDetailInfoDto>()
                            .ForMember(d => d.ProAttributeList, opt => opt.Ignore())
             .ForMember(d => d.contenturl, opt => opt.Ignore())
                   .ForMember(d => d.bannerurl, opt => opt.Ignore())
               .ForMember(d => d.baseurl, opt => opt.Ignore());


            CreateMap<Pro_attributeEntity, ProAttributeInfo>()
                              .ForMember(d => d.attrId, opt => opt.MapFrom(s => s.Id))
.ForMember(d => d.add_date, opt => opt.MapFrom(s => s.add_date.ToString("yyyy-MM-dd")));
        }
    }
}
