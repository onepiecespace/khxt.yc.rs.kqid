﻿using AutoMapper;
using Hx.Extensions;
using KhXt.YcRs.Domain.Entities;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Course;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace KhXt.YcRs.Domain.Profiles
{
    public class CourseOrderProfile : Profile
    {
        public CourseOrderProfile()
        {

            CreateMap<CourseOrderEntity, MyOrderInfo>()
                .ForMember(d => d.orderType, opt => opt.MapFrom(s => s.OrderState))
                .ForMember(d => d.CourseId, opt => opt.MapFrom(s => s.CourseId))
                .ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderNo))
                .ForMember(d => d.DateTime, opt => opt.MapFrom(s => s.CreateTime))
                .ForMember(d => d.Price, opt => opt.MapFrom(s => s.PayPrice))
                .ForMember(d => d.PackageType, opt => opt.MapFrom(s => ((ExpressType)s.PackageType).ToString().ToLower()))
                .ForMember(d => d.ExpressName, opt => opt.MapFrom(s => ((ExpressType)s.PackageType).GetDescription()))
                ;
            CreateMap<PayOrderInfo, CourseEntity>();


        }
    }
}
