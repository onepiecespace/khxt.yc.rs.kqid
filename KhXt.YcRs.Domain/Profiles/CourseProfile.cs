﻿using AutoMapper;
using Castle.Core.Internal;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static KhXt.YcRs.Domain.Models.Course.CourseThinModel;

namespace KhXt.YcRs.Domain.Profiles
{
    public class CourseProfile : Profile
    {
        public CourseProfile()
        {
            CreateMap<CourseChildEntity, CourseUserListModel>()
                .ForMember(d => d.CourseId, opt => opt.MapFrom(s => s.ParentId))
                .ForMember(d => d.CourseChildId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.ChildTimeLength, opt => opt.Ignore())
                .ForMember(d => d.ChildTimestr, opt => opt.MapFrom(s => s.ChildTimeLength))
               .ForMember(d => d.ChildStartTime, opt => opt.MapFrom(t => t.ChildStartTime == null ? "" : t.ChildStartTime.ToString("yyyy-MM-dd hh:mm")))
              .ForMember(d => d.ChildEndTime, opt => opt.MapFrom(t => t.ChildEndTime == null ? "" : t.ChildEndTime.ToString("yyyy-MM-dd hh:mm")));

            CreateMap<CourseChildInfo, CourseUserListModel>()
    .ForMember(d => d.CourseId, opt => opt.MapFrom(s => s.ParentId))
    .ForMember(d => d.CourseChildId, opt => opt.MapFrom(s => s.id))
    .ForMember(d => d.ChildTimeLength, opt => opt.Ignore())
    .ForMember(d => d.ChildTimestr, opt => opt.MapFrom(s => s.ChildTimeLength))
   .ForMember(d => d.ChildStartTime, opt => opt.MapFrom(t => t.ChildStartTime == null ? "" : t.ChildStartTime))
  .ForMember(d => d.ChildEndTime, opt => opt.MapFrom(t => t.ChildEndTime == null ? "" : t.ChildEndTime));

            CreateMap<CourseItemEntity, CourseUserWorkModel>()
                .ForMember(d => d.ItemId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.ItemOptions, opt => opt.MapFrom(s => s.Options))
                  .ForMember(d => d.Options, opt => opt.Ignore())
                .ForMember(d => d.CreateTime, opt => opt.Ignore())
                .ForMember(d => d.Creator, opt => opt.Ignore())
                .ForMember(d => d.UpdateTime, opt => opt.Ignore())
                .ForMember(d => d.Updater, opt => opt.Ignore());
            CreateMap<CourseUserItemEntity, CourseUserWorkModel>();
            CreateMap<CourseUserItemAddModel, CourseUserItemEntity>()
                .ForMember(d => d.CourseItemId, opt => opt.MapFrom(s => s.ItemId))
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.CourseUserItemId));
            CreateMap<CourseUserItemAddModel, CourseUserWorkResult>();

            CreateMap<CateoryInfo, CourseCateoryEntity>();
            CreateMap<CourseCateoryEntity, CateoryInfo>();

            CreateMap<LiveInfo, CourseEntity>();
            CreateMap<CourseEntity, LiveInfo>();


            CreateMap<CouerseInfo, CourseEntity>();
            CreateMap<CourseEntity, CouerseInfo>();

            CreateMap<CouerseDetailedListInfo, CourseEntity>();
            CreateMap<CourseEntity, CouerseDetailedListInfo>();

            CreateMap<CourseQueryModel, CouerseDetailedListInfo>()
               .ForMember(d => d.CourseId, opt => opt.MapFrom(s => s.Id));

            CreateMap<CourseChildInfo, CourseChildEntity>();
            CreateMap<CourseChildEntity, CourseChildInfo>();

            CreateMap<CourseEntity, PayOrderInfo>()
            .ForMember(d => d.UsableCount, opt => opt.Ignore());

            CreateMap<CourseQueryModel, PayOrderInfo>()
.ForMember(d => d.UsableCount, opt => opt.Ignore());

            CreateMap<CourseItemEntity, CourseItemInfo>()
             .ForMember(d => d.ChildTimeLength, opt => opt.Ignore());

            CreateMap<MessageInfo, CourseLogEntity>();
            CreateMap<CourseLogEntity, MessageInfo>();
            CreateMap<CourseEntity, CourseQueryModel>();
            CreateMap<CourseQueryModel, CouerseInfo>();

            CreateMap<CourseAppointmentModel, CourseAppointmentEntity>();
            CreateMap<CourseAppointmentEntity, CourseAppointmentModel>();

            CreateMap<CourseLogEntity, CourseLogModel>();

            CreateMap<CourseLogEntity, CourseLogInfo>();

            CreateMap<CourseCateoryModel, CourseCateoryEntity>();
            CreateMap<CourseCateoryEntity, CourseCateoryModel>();

            CreateMap<CourseCateoryModel, CateoryInfo>()
            .ForMember(d => d.Twolist, opt => opt.Ignore());

            CreateMap<CourseQueryModel, CourseEntity>();
            CreateMap<CourseEntity, CourseQueryModel>();


            CreateMap<CourseChildModelCMS, CourseChildEntity>();
            //     .ForMember(d => d.Id, opt => opt.Ignore());

            CreateMap<CourseChildEntity, CourseChildModelCMS>();

            CreateMap<CourseEntity, CourseThinModel>();
            CreateMap<CourseQueryModel, CourseThinModel>()
                .ForMember(d => d.Img, opt => opt.MapFrom(s => "img/collegecourse/"))
                .ForMember(d => d.PcImg, opt => opt.MapFrom(s => s.CoursePicPhoneMini))
                .ForMember(d => d.IsFree, opt => opt.MapFrom(s => s.DiscountPrice <= 0 ? 0 : 1)) //JADEN:暂时写反，下个版本改回来，200228
                .ForMember(d => d.DiscountPrice, opt => opt.MapFrom(s => s.DiscountPrice))
                .ForMember(d => d.IosPayPirce, opt => opt.MapFrom(s => s.IosPayPirce))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.CourseName))
                .ForMember(d => d.Summary, opt => opt.MapFrom(s => s.Detailed))
                .ForMember(d => d.Teacher, opt => opt.MapFrom(s => s.TeachersUserName))
                .ForMember(d => d.Url, opt => opt.MapFrom(s => s.H5Url))
                .ForMember(d => d.Category, opt => opt.MapFrom(s => s.Cateorys))
                .ForMember(d => d.IsRecord, opt => opt.MapFrom(s => s.IsRecord))
                .ForMember(d => d.Sort, opt => opt.MapFrom(s => s.Sort))
                .ForMember(d => d.Status, opt => opt.MapFrom(s => s.Status))
                .ForMember(d => d.GradeValues, opt => opt.MapFrom(s => s.GradeIds))
                .ForMember(d => d.LiveStart, opt => opt.MapFrom(s => s.LiveBeginTime))
                .ForMember(d => d.LiveEnd, opt => opt.MapFrom(s => s.LiveEndTime))
                .ForMember(d => d.TeacherIds, opt => opt.MapFrom((s, d) =>
                {
                    if (s.Teachers.IsNullOrEmpty()) return null;
                    return s.Teachers.Split(",").Select(t => Convert.ToInt64(t)).ToArray();
                }))
               .ForMember(d => d.CourseType, opt => opt.MapFrom(s => s.IsLive));


            CreateMap<CourseQueryModel, CoursePcThinModel>()
              .ForMember(d => d.Img, opt => opt.MapFrom(s => "img/collegecourse/"))
              .ForMember(d => d.PcImg, opt => opt.MapFrom(s => s.CoursePicPhoneMini))
              .ForMember(d => d.IsFree, opt => opt.MapFrom(s => s.DiscountPrice <= 0 ? 1 : 0))
              .ForMember(d => d.DiscountPrice, opt => opt.MapFrom(s => s.DiscountPrice))
              .ForMember(d => d.IosPayPirce, opt => opt.MapFrom(s => s.IosPayPirce))
              .ForMember(d => d.Name, opt => opt.MapFrom(s => s.CourseName))
              .ForMember(d => d.Sort, opt => opt.MapFrom(s => s.Sort))
              .ForMember(d => d.Status, opt => opt.MapFrom(s => s.Status))
              .ForMember(d => d.Summary, opt => opt.MapFrom(s => s.Detailed))
              .ForMember(d => d.Teacher, opt => opt.MapFrom(s => s.TeachersUserName))
              .ForMember(d => d.Url, opt => opt.MapFrom(s => s.H5Url))
              .ForMember(d => d.Category, opt => opt.MapFrom(s => s.Cateorys))
              .ForMember(d => d.ClassId, opt => opt.MapFrom(s => s.ClassId))
              .ForMember(d => d.GradeValues, opt => opt.MapFrom(s => s.GradeIds))
              .ForMember(d => d.LiveStart, opt => opt.MapFrom(s => s.LiveBeginTime))
              .ForMember(d => d.LiveEnd, opt => opt.MapFrom(s => s.LiveEndTime))
              .ForMember(d => d.TeacherIds, opt => opt.MapFrom((s, d) =>
              {
                  if (s.Teachers.IsNullOrEmpty()) return null;
                  return s.Teachers.Split(",").Select(t => Convert.ToInt64(t)).ToArray();
              }))
              .ForMember(d => d.CourseType, opt => opt.MapFrom(s => s.IsLive));



            CreateMap<CourseChildInfo, ChildCourseThinModel>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.ChildName))
                .ForMember(d => d.ChildType, opt => opt.MapFrom(s => s.ChildType))
                .ForMember(d => d.ChannelId, opt => opt.MapFrom(s => s.ChannelId))
                .ForMember(d => d.videoId, opt => opt.MapFrom(s => s.videoId))
                .ForMember(d => d.Teacher, opt => opt.MapFrom(s => s.ChildTeacher))
                .ForMember(d => d.LiveStart, opt => opt.MapFrom(s => s.ChildStartTime))
                .ForMember(d => d.LiveStart, opt => opt.MapFrom(s => s.ChildStartTime.IsNullOrEmpty() ? (DateTime?)null : Convert.ToDateTime(s.ChildStartTime)))
                .ForMember(d => d.TimeDesc, opt => opt.MapFrom(s => s.ChildTimeLength))
                .ForMember(d => d.Sort, opt => opt.MapFrom(s => s.ChildSort));
            CreateMap<CourseChildInfo, CourseisliveChildInfo>()
                         .ForMember(d => d.CourseName, opt => opt.Ignore())
                           .ForMember(d => d.IsLive, opt => opt.Ignore());
            CreateMap<CourseBycollectModel, CourseBuyCollectEntity>();
            CreateMap<CourseBuyCollectEntity, CourseBycollectModel>();
        }
    }
}
