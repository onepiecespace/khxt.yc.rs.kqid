﻿using AutoMapper;
using Hx.Extensions;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.User;

namespace KhXt.YcRs.Domain.Profiles
{
    /// <summary>
    /// 
    /// </summary>
    public class UserProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public UserProfile()
        {

            CreateMap<UserEntity, AUserBaseInfoOutput>()
                .ForMember(d => d.UserId, opt => opt.MapFrom(s => s.Id));
            //.ForMember(d => d.UserTitle, opt => opt.Ignore())
            //.ForMember(d => d.UserTitleIcon, opt => opt.Ignore())
            //.ForMember(d => d.WinningPercent, opt => opt.Ignore())
            //.ForMember(d => d.WinCount, opt => opt.Ignore())
            //.ForMember(d => d.FailCount, opt => opt.Ignore())
            //.ForMember(d => d.PkWinCount, opt => opt.Ignore());
            CreateMap<UserBaseInfo, AUserBaseInfoOutput>();


            CreateMap<AUserBaseInfoOutput, UserEntity>();
            CreateMap<AUserBaseInfoOutput, UserBaseInfo>();

            CreateMap<UserEntity, UserBaseInfo>()
                .ForMember(d => d.Userid, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.Levels, opt => opt.Ignore())
                .ForMember(d => d.Experiences, opt => opt.Ignore())
                .ForMember(d => d.GradeList, opt => opt.Ignore())
                .ForMember(d => d.PostType, opt => opt.Ignore())
                .ForMember(d => d.ModeType, opt => opt.Ignore())
                .ForMember(d => d.CreateTimeStr, opt => opt.MapFrom(s => s.Creatime.ToString("yyyy-MM-dd")));


            CreateMap<UserBaseInfo, UserEntity>()
                          .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Userid));




            CreateMap<KVStoreEntity, KeyValueStore>();
            CreateMap<KeyValueStore, KeyValue>();

            CreateMap<ThreeLoginModel, UserEntity>()
                .ForMember(d => d.Username, opt => opt.MapFrom(s => s.NickName))
                .ForMember(d => d.Headurl, opt => opt.MapFrom(s => s.Avatar));

            CreateMap<ThreeLoginModel, UserBaseInfo>()
       .ForMember(d => d.Username, opt => opt.MapFrom(s => s.NickName))
       .ForMember(d => d.Headurl, opt => opt.MapFrom(s => s.Avatar));


            //////用户相关的
            //KVStoreEntity

            CreateMap<VersionLog, VersionEntity>();
            CreateMap<VersionEntity, VersionLog>();


            CreateMap<ValueLogModel, UserValueLogEntity>();

            CreateMap<UserValueLogEntity, ValueLogModel>()
                .ForMember(d => d.TimeStamp, opt => opt.MapFrom(s => s.CreateTime == null ? "" : s.CreateTime.ToString19()));

            CreateMap<UserEntity, UserCharInfo>()
                .ForMember(d => d.CreateTimeString, opt => opt.MapFrom(s => s.Creatime.ToString("yyyy-MM-dd")))
                .ForMember(d => d.CreateTimeMonth, opt => opt.MapFrom(s => s.Creatime.Month));

            CreateMap<UserBaseInfo, UserCharInfo>()
                .ForMember(d => d.CreateTimeString, opt => opt.MapFrom(s => s.Creatime.ToString("yyyy-MM-dd")))
                .ForMember(d => d.CreateTimeMonth, opt => opt.MapFrom(s => s.Creatime.Month));

            CreateMap<UserCharInfo, UserBaseInfo>();

            CreateMap<UserLeveModel, UserLeveEntity>();
            CreateMap<UserLeveEntity, UserLeveModel>();
            CreateMap<UserEntity, UserQueryResultModel>();

            CreateMap<UserFeedbackEntity, UserFeedbackInfo>();
            CreateMap<UserFeedbackInfo, UserFeedbackEntity>();

        }
    }
}