﻿using AutoMapper;
using Hx.Extensions;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Profiles
{
    /// <summary>
    /// 
    /// </summary>
    public class UserCourseChildProgessProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public UserCourseChildProgessProfile()
        {
            CreateMap<UserCourseChildProgessEntity, UserCourseChildInfo>();

            CreateMap<UserCourseChildInfo, UserCourseChildProgessEntity>();
        }
    }
}
