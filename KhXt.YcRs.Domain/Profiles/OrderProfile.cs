﻿using AutoMapper;
using Hx.Extensions;
using Hx.Extensions;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Profiles
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public OrderProfile()
        {
            CreateMap<PayOrderModel, OrderPayEntity>()
                .ForMember(d => d.TransactionNo, opt => opt.MapFrom(t => t.PaymentNo));
            CreateMap<CouponInfoEntity, UserCouponEntity>()
                .ForMember(d => d.UseLimit, opt => opt.MapFrom(t => t.LimitCategory));
            CreateMap<OrderInfoEntity, OrderPayEntity>()
                .ForMember(d => d.Id, opt => opt.Ignore())
                .ForMember(d => d.OrderId, opt => opt.MapFrom(t => t.Id))
                .ForMember(d => d.Price, opt => opt.MapFrom(t => t.Payment))
                .ForMember(d => d.TransactionNo, opt => opt.MapFrom(t => t.PaymentNo))
                .ForMember(d => d.CreateTime, opt => opt.Ignore())
                .ForMember(d => d.UpdateTime, opt => opt.Ignore());


            CreateMap<OrderInfoEntity, OldOrderStatusModel>()
                .ForMember(d => d.PayPrice, opt => opt.MapFrom(t => t.Payment))
                .ForMember(d => d.OrderType, opt => opt.MapFrom(t => t.Status))
                .ForMember(d => d.OrderId, opt => opt.MapFrom(t => t.Id))
                .ForMember(d => d.ExpressName, opt => opt.MapFrom(t => t.ShippingName))
                .ForMember(d => d.PackageNo, opt => opt.MapFrom(t => t.ShippingCode))
                .ForMember(d => d.PackageType, opt => opt.MapFrom(t => t.ShippingType.ToLower()))
                .ForMember(d => d.DateTime, opt => opt.MapFrom(t => t.CreateTime.ToString19()))
                .ForMember(d => d.LogisticsId, opt => opt.MapFrom(t => t.ShippingCode))
                .ForMember(d => d.PaymentTime, opt => opt.MapFrom(t => t.PaymentTime == null ? "" : t.PaymentTime.Value.ToString19()));

            CreateMap<OrderPayEntity, SysOrderPayInfo>()
                .ForMember(d => d.CreateTime, opt => opt.MapFrom(t => t.CreateTime.ToString("yyyy-MM-dd HH:mm:ss")))
                .ForMember(d => d.UpdateTime, opt => opt.MapFrom(t => t.UpdateTime.ToString("yyyy-MM-dd HH:mm:ss")));

            CreateMap<OrderPayEntity, SysOrderPayInfoExport>()
                .ForMember(d => d.BusinessTypeName, opt => opt.MapFrom(t => EnumHelper.GetDescription<BusinessType>(t.BusinessType.ToString())))
                .ForMember(d => d.PaymentTypeName, opt => opt.MapFrom(t => EnumHelper.GetDescription<PaymentType>(t.PaymentType.ToString())))
                .ForMember(d => d.TransactionNo, opt => opt.MapFrom(t => t.TransactionNo))
                .ForMember(d => d.IsRefundValue, opt => opt.MapFrom(t => t.IsRefund == 1 ? "是" : "否"))
                .ForMember(d => d.Aid, opt => opt.MapFrom(t => DomainEnv.GetAidName(t.Aid)));

            CreateMap<OrderInfoEntity, OrderQueryResultModel>();

        }
    }
}
