﻿using AutoMapper;
using KhXt.YcRs.Domain.Entities;
using KhXt.YcRs.Domain.Models.Test;

namespace KhXt.YcRs.Domain.Profiles
{
    public class TestProfile : Profile
    {
        public TestProfile()
        {
            CreateMap<PersonEntity, PersonModel>();
            CreateMap<PersonModel, PersonEntity>();
        }
    }
}
