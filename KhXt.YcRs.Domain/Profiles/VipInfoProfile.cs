﻿using AutoMapper;
using Hx.Extensions;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Models.Vip;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Profiles
{
    /// <summary>
    /// 
    /// </summary>
    public class VipInfoProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public VipInfoProfile()
        {
            CreateMap<VipInfoEntity, VipInfo>();
            CreateMap<UserVipEntity, UserVipModel>()
                .ForMember(d => d.VipInfo, opt => opt.Ignore())
                .ForMember(d => d.ExpiredTimeStr, opt => opt.MapFrom(s => s.ExpiredTime.ToString10()));

            CreateMap<UserVipModel, UserVipEntity>();
            CreateMap<UserVipEntity, UserVipHistoryEntity>();
            CreateMap<UserVipModel, UserVipHistoryEntity>();

        }

    }
}
