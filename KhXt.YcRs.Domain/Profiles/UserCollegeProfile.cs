﻿using AutoMapper;
using Hx.Extensions;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Profiles
{
    /// <summary>
    /// 
    /// </summary>
    public class UserCollegeProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public UserCollegeProfile()
        {
            CreateMap<UserCollegeEntity, UserCollegeInfo>()
                .ForMember(d => d.RenextTime, opt => opt.MapFrom(s => s.RenextTime.ToString10()));

            CreateMap<UserCollegeInfo, UserCollegeEntity>();
            CreateMap<UserCollegeReq, UserCollegeInfo>()
             .ForMember(d => d.RenextTime, opt => opt.Ignore());
        }
    }
}
