﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models
{
    public class ValueLogModel
    {
        //public long UserId { get; set; }
        public long Value { get; set; }
        public long LastValue { get; set; }
        //public ValueCategory Category { get; set; }

        public string TimeStamp { get; set; }

        public BusinessType BusinessType { get; set; }

        public string Description { get; set; }
    }


}
