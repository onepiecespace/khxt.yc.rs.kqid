﻿using System;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    public class LogApiListParam : DateTimeParam
    {
        public string UserName { get; set; }
        public string ExecuteUrl { get; set; }
        public int? LogStatus { get; set; }
    }
}
