﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Models
{
    public class MenuListParam
    {
        public string MenuName { get; set; }
        public int? MenuStatus { get; set; }
    }
}
