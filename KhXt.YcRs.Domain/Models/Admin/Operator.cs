﻿using KhXt.YcRs.Domain.Util;
using KhXt.YcRs.Domain.Util.Extension;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace KhXt.YcRs.Domain.Models
{
    public class Operator
    {
        public static Operator Instance
        {
            get { return new Operator(); }
        }

        private string LoginProvider = GlobalContext.Configuration.GetSection("SystemConfig:LoginProvider").Value;
        private string TokenName = "UserToken"; //cookie name or session name
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task AddCurrent(string token)
        {
            switch (LoginProvider)
            {
                case "Cookie":
                    new CookieHelper().WriteCookie(TokenName, token);
                    break;

                case "Session":
                    new SessionHelper().WriteSession(TokenName, token);
                    break;

                case "WebApi":

                    var path = string.Format("User/GetUserByToken?token={0}", token);
                    await HttpClientHelper.HttpGet<OperatorInfo>(path, "");
                    break;

                default:
                    throw new Exception("未找到LoginProvider配置");
            }
        }

        /// <summary>
        /// Api接口需要传入apiToken
        /// </summary>
        /// <param name="apiToken"></param>
        public async Task RemoveCurrentAsync(string apiToken = "")
        {
            switch (LoginProvider)
            {
                case "Cookie":
                    new CookieHelper().RemoveCookie(TokenName);
                    break;

                case "Session":
                    new SessionHelper().RemoveSession(TokenName);
                    break;

                case "WebApi":
                    var path = string.Format("User/RemoveCache?token={0}", apiToken);
                    await HttpClientHelper.HttpGet<string>(path, "");
                    break;

                default:
                    throw new Exception("未找到LoginProvider配置");
            }
        }
        /// <summary>
        /// Api接口需要传入apiToken
        /// </summary>
        /// <param name="apiToken"></param>
        /// <returns></returns>
        public async Task<OperatorInfo> Current(string apiToken = "")
        {
            IHttpContextAccessor hca = GlobalContext.ServiceProvider?.GetService<IHttpContextAccessor>();
            OperatorInfo user = null;
            string token = string.Empty;
            switch (LoginProvider)
            {
                case "Cookie":
                    if (hca.HttpContext != null)
                    {
                        token = new CookieHelper().GetCookie(TokenName);
                    }
                    break;

                case "Session":
                    if (hca.HttpContext != null)
                    {
                        token = new SessionHelper().GetSession(TokenName);
                    }
                    break;

                case "WebApi":
                    if (string.IsNullOrEmpty(apiToken))
                    {
                        token = new CookieHelper().GetCookie(TokenName);
                    }
                    else
                    {
                        token = apiToken;
                    }
                    break;
            }
            if (string.IsNullOrEmpty(token))
            {
                return user;
            }
            token = token.Trim('"');
            var path = string.Format("User/GetUserByToken?token={0}", token);
            user = await HttpClientHelper.HttpGet<OperatorInfo>(path, "");
            return user;
        }
    }
}
