﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Models.Product
{
    public class ProStockInfo
    {
        /// <summary>
        /// 库存ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// product_id
        /// </summary>		
        private int _product_id;
        public int product_id
        {
            get { return _product_id; }
            set { _product_id = value; }
        }
        /// <summary>
        /// 属性id
        /// </summary>		
        private int _attribute_id;
        public int attribute_id
        {
            get { return _attribute_id; }
            set { _attribute_id = value; }
        }
        /// <summary>
        /// 入库/出库
        /// </summary>		
        private int _flowing_num;
        public int flowing_num
        {
            get { return _flowing_num; }
            set { _flowing_num = value; }
        }
        /// <summary>
        /// 类型 0.入库 1.出库 2.预警
        /// </summary>		
        private int _type;
        public int type
        {
            get { return _type; }
            set { _type = value; }
        }
        /// <summary>
        /// add_date
        /// </summary>		
        private DateTime _add_date;
        public DateTime add_date
        {
            get { return _add_date; }
            set { _add_date = value; }
        }

    }
}

