﻿using Hx.Domain.Entities;
using System;


namespace KhXt.YcRs.Domain.Models.Product
{
    public class ProAttributeInfo
    {
        /// <summary>
        /// 商品配置id
        /// </summary>
        public int attrId { get; set; }

        /// <summary>
        /// 属性配置名称
        /// </summary>		
        public string attributename
        { get; set; }
        /// <summary>
        /// 关联商品ID
        /// </summary>		
        public int product_id
        { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>		
        public string color
        { get; set; }
        /// <summary>
        /// 尺码
        /// </summary>		
        public string size
        { get; set; }
        /// <summary>
        /// 原价格
        /// </summary>		
        public decimal yprice
        { get; set; }
        /// <summary>
        /// 市场参考价
        /// </summary>		
        public decimal costprice
        { get; set; }
        /// <summary>
        /// 出售价格
        /// </summary>		
        public decimal price
        { get; set; }
        /// <summary>
        /// 是否开启金币兑换 0:未开启金币兑换 1:开启金币兑换
        /// </summary>		
        public int iscoins
        { get; set; }
        /// <summary>
        /// 金币兑换价格（所需金币数量）
        /// </summary>		
        public decimal coins_price
        { get; set; }

        /// <summary>
        /// 属性筛选小图图片
        /// </summary>		
        public string img
        { get; set; }

        /// <summary>
        /// 库存
        /// </summary>		
        public int num
        { get; set; }
        /// <summary>
        /// 单位
        /// </summary>		
        public string unit
        { get; set; }
        /// <summary>
        /// 状态 0:未上架1:已上架 2 缺货 3下架
        /// </summary>		
        public int status
        { get; set; }
        /// <summary>
        /// 属性
        /// </summary>		
        public string attribute
        { get; set; }
        /// <summary>
        /// 回收站 0.不回收 1.回收
        /// </summary>		
        public int recycle
        { get; set; }
        /// <summary>
        /// 总库存
        /// </summary>		
        public int total_num
        { get; set; }
        /// <summary>
        /// add_date 
        /// </summary>		
        public DateTime add_date
        { get; set; }
    }
}



