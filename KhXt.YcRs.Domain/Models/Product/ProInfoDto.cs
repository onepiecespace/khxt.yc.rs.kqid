﻿using Hx.Domain.Entities;
using System;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.Product
{
    public class ProInfoDto
    {
        /// <summary>
        /// 商品ID
        /// </summary>
        public int proId { get; set; }

        /// <summary>
        /// 商品编号
        /// </summary>		

        public string pro_number { get; set; }
        /// <summary>
        /// 产品名字
        /// </summary>		

        public string pro_title
        { get; set; }
        /// <summary>
        /// 副标题
        /// </summary>		

        public string subtitle
        { get; set; }

        /// <summary>
        /// 属性
        /// </summary>		
        public string attribute
        { get; set; }
        /// <summary>
        /// 产品类别
        /// </summary>		
        public int pro_classid
        { get; set; }
        /// <summary>
        /// 产品封面图片
        /// </summary>		

        public string imgurl
        { get; set; }

        /// <summary>
        /// sort
        /// </summary>		
        public int sort
        { get; set; }

        /// <summary>
        /// 是否已售罄（已抢光）
        /// </summary>		
        public bool IsSoldout
        { get; set; }
        /// <summary>
        /// 销量
        /// </summary>		
        public int volume
        { get; set; }
        /// <summary>
        /// 产品值属性 1：新品,2：热销，3：推荐
        /// </summary>		

        public int s_type
        { get; set; }
        /// <summary>
        /// 总数量
        /// </summary>		
        public int total_num
        { get; set; }

        /// <summary>
        /// 是否开启金币兑换 0:未开启金币兑换 1:开启金币兑换
        /// </summary>		
        public int iscoins
        { get; set; }
        /// <summary>
        /// 金币兑换价格（所需金币数量）
        /// </summary>		
        public decimal coins_price
        { get; set; }

        /// <summary>
        /// 重量
        /// </summary>		
        public string weight
        { get; set; }
        /// <summary>
        /// 运费
        /// </summary>		
        public string freight
        { get; set; }

        /// <summary>
        /// 是否开启打折
        /// </summary>		
        public int is_discount
        { get; set; }

        /// <summary>
        /// 状态 0::待上架 1:上架 2:下架
        /// </summary>		
        public int status
        { get; set; }
    }
}

