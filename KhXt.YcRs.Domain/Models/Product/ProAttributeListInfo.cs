﻿using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Models.Product;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.Product
{
    public class ProAttributeListInfo
    {
        public List<ProAttributeInfo> ProAttributeList { get; set; }


    }
}