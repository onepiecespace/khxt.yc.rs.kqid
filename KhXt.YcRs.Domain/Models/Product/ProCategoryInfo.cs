﻿using Hx.Domain.Entities;
using System;


namespace KhXt.YcRs.Domain.Models.Product
{
    public class ProCategoryInfo
    {
        /// <summary>
        /// 分类Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 上级id
        /// </summary>		
        public int sid
        { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>		
        public string pname
        { get; set; }
        /// <summary>
        /// 分类图片
        /// </summary>		
        public string img
        { get; set; }
        /// <summary>
        /// 小图标
        /// </summary>		
        public string bg
        { get; set; }
        /// <summary>
        /// 级别
        /// </summary>		
        public int level
        { get; set; }
        /// <summary>
        /// sort
        /// </summary>		
        public int sort
        { get; set; }
        /// <summary>
        /// add_date
        /// </summary>		
        public DateTime add_date
        { get; set; }
        /// <summary>
        /// 回收站 0.不回收 1.回收
        /// </summary>		
        public int recycle
        { get; set; }

    }
}
