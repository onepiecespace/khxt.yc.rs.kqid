﻿using KhXt.YcRs.Domain.Models.Product;
using KhXt.YcRs.Domain.Models.Sys;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.Product
{
    public class ProListPageList
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public List<ProInfoDto> ProList { get; set; }
    }


}