﻿using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Models.Sys;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Vip
{
    /// <summary>
    /// Vip会员接口
    /// </summary>
    public class VipServiceInfo
    {
        /// <summary>
        /// 会员卡信息
        /// </summary>
        public List<VipInfoEntity> VipInfo { get; set; }
        /// <summary>
        /// 会员卡信息
        /// </summary>
        public Dictionary<long, List<VipPrivilegeEntity>> VipPrivileges { get; set; }
        /// <summary>
        /// 广告信息
        /// </summary>
        public List<AdModel> AdModels { get; set; }

        /// <summary>
        /// 优惠券数量
        /// </summary>
        public Dictionary<long, Tuple<int, float>> Coupons { get; set; }

    }
}
