﻿using KhXt.YcRs.Domain.ValueObject.Coupon;
using KhXt.YcRs.Domain.ValueObject.Vip;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Vip
{
    /// <summary>
    ///会员卡信息
    /// </summary>
    public class VipInfo
    {
        /// <summary>
        /// 会员卡Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 原价格
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// 自动续费价格
        /// </summary>
        public float RenewPrice { get; set; }
        /// <summary>
        /// IOS自动续费价格
        /// </summary>
        public string IOSRenewPriceId { get; set; }
        /// <summary>
        /// IOS自动续费价格
        /// </summary>
        public float IOSRenewPrice { get; set; }
        /// <summary>
        /// 模块特权
        /// </summary>
        public Privilege Privilege { get; set; }
        /// <summary>
        /// 优惠券信息
        /// </summary>
        public CouponInfo Coupon { get; set; }
        /// <summary>
        /// 销售价格
        /// </summary>
        public float SalePrice { get; set; }
        /// <summary>
        /// IOS销售价格
        /// </summary>
        public string IOSSalePriceId { get; set; }
        /// <summary>
        /// IOS销售价格
        /// </summary>
        public float IOSSalePrice { get; set; }

        /// <summary>
        /// 有效天数
        /// </summary>
        public int ExpiredDays { get; set; }
        /// <summary>
        /// 背景图Url
        /// </summary>
        public string[] BgPath { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public long Updater { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
