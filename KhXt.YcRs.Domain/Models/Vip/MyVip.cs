﻿using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Models.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Vip
{
    /// <summary>
    /// 会员中心
    /// </summary>
    public class MyVip
    {
        /// <summary>
        /// 会员基础信息
        /// </summary>
        public UserVipModel UerVipModel { get; set; }
        /// <summary>
        /// 我的优惠券
        /// </summary>
        public List<MyCoupon> MyCoupons { get; set; }
        /// <summary>
        /// 会员的特权
        /// </summary>
        public List<VipPrivilegeEntity> Privileges { get; set; }
    }
}
