﻿using KhXt.YcRs.Domain.Entities.Vip;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Vip
{
    /// <summary>
    /// 用户Vip信息
    /// </summary>
    public class UserVipModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 类别
        /// </summary>
        public long VipType { get; set; }
        /// <summary>
        /// 会员卡信息
        /// </summary>
        public VipInfo VipInfo { get; set; } = new VipInfo();
        /// <summary>
        /// 开通方式 1 付费 2 会员卡兑换 3 好友赠送
        /// </summary>
        public int IsFrom { get; set; }
        /// <summary>
        /// 会员卡兑换Id
        /// </summary>
        public string ExchangeId { get; set; }
        /// <summary>
        /// 是否自动续费
        /// </summary>
        public bool IsAutoRenew { get; set; }
        /// <summary>
        /// 自动续费价格
        /// </summary>
        public float RenewPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? AutoRenewTime { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 到期时间
        /// </summary>
        public DateTime ExpiredTime { get; set; }
        /// <summary>
        /// 到期时间
        /// </summary>
        public string ExpiredTimeStr { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
