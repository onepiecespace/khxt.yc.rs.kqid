﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseCateoryModel
    {
        public long Id { get; set; }
        /// <summary>
        /// BusinessType
        /// </summary>		 
        public int ClassId { get; set; }

        /// <summary>
        /// ParentId
        /// </summary>		 
        public int ParentId { get; set; }

        /// <summary>
        /// ParentIdList
        /// </summary>		 
        public string ParentIdList { get; set; }

        /// <summary>
        /// Name
        /// </summary>		 
        public string Name { get; set; }

        /// <summary>
        /// Img
        /// </summary>		 
        public string Img { get; set; }

        /// <summary>
        /// IcoImg
        /// </summary>		 
        public string IcoImg { get; set; }

        /// <summary>
        /// Des
        /// </summary>		 
        public string Des { get; set; }

        /// <summary>
        /// KeyWord
        /// </summary>		 
        public string KeyWord { get; set; }

        /// <summary>
        /// Sort
        /// </summary>		 
        public int Sort { get; set; }

        /// <summary>
        /// IsShow
        /// </summary>		 
        public int IsShow { get; set; }

        /// <summary>
        /// IsNav
        /// </summary>		 
        public int IsNav { get; set; }

        /// <summary>
        /// IsHomeTop
        /// </summary>		 
        public int IsHomeTop { get; set; }

        /// <summary>
        /// IsDel
        /// </summary>		 
        public int IsDel { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>		 
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// UpdateTime
        /// </summary>		 
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// TenantId
        /// </summary>		 
        public int TenantId { get; set; }

        /// <summary>
        /// ChildCount
        /// </summary>		 
        public int ChildCount { get; set; }
    }
}
