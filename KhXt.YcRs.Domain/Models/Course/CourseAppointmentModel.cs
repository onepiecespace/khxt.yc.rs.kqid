﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseAppointmentModel
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public string Phone { get; set; }

        public DateTime CreateTime { get; set; }
        //  public string Remark { get; set; }
    }
}
