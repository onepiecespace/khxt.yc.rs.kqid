﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseThinWithChildModel : CourseThinModel
    {
        IEnumerable<CourseThinModel> Children { get; set; }
    }
}