﻿namespace KhXt.YcRs.Domain.Models.Course
{
    public class PayOrderInfo
    {
        public long CourseId { get; set; }

        public string OrderNo { get; set; }
        public string CourseName { get; set; }
        public string Detailed { get; set; }
        public string CoursePicPhoneMini { get; set; }
        public string ValidBeginTime { get; set; }
        public string ValidEndTime { get; set; }

        /// <summary>
        /// 原价 划线价格
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 售价现价
        /// </summary>
        public string DiscountPrice { get; set; }
        /// <summary>
        /// 是否可使用优惠券0否 1是(几个可用并选择优惠券列表（包括满减券 折扣券 目前只有兑换券）)
        /// </summary>
        public int IsDiscount { get; set; }
        /// <summary>
        /// IsDiscount=1 可使用的优惠券数量（目前只有兑换券（后期会有折扣券 满减券））
        /// </summary>
        public int UsableCount { get; set; }

        public string PreferentialBeginTime { get; set; }
        public string PreferentialEndTime { get; set; }
        public string TeachersUserName { get; set; }
        public int TeachersUserId { get; set; }


        public int IsExpress { get; set; }
        public string ExpressPirce { get; set; }

        public string Iosproductid { get; set; }
        public string IosPayPirce { get; set; }
        public string IosProductidName { get; set; }

    }
}