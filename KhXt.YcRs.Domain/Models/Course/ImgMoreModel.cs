﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class ImgMoreModel
    {
        public string picbase64 { get; set; }
        public string imgone { get; set; }
        public string picbase64two { get; set; }
        public string imgtwo { get; set; }
        public string picbase64three { get; set; }
        public string imgthree { get; set; }

        public string picbase64four { get; set; }
        public string imgfour { get; set; }
        public string picbase64five { get; set; }
        public string imgfive { get; set; }
        public string picbase64six { get; set; }
        public string imgsix { get; set; }

        public string picbase64seven { get; set; }
        public string imgseven { get; set; }
        public string picbase64night { get; set; }
        public string imgnight { get; set; }
    }
}
