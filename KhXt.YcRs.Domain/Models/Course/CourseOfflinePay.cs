﻿using KhXt.YcRs.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseOfflinePay
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 用户手机号码
        /// </summary>
        public string Phone { get; set; }


        /// <summary>
        /// 支付方式  7 线下微信 8 线下支付宝 9 内部员工免费 4 线下 10 校管家转入
        /// </summary>
        public int PaymentType { get; set; }

        /// <summary>
        /// 客户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 是否测试
        /// </summary>
        public bool IsTest { get; set; } = false;

        /// <summary>
        /// 交易号
        /// </summary>
        public string TransactionNo { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 交易详细信息
        /// </summary>
        public string Transaction { get; set; }
        /// <summary>
        /// 支付时间 （格式 2020-03-10 18:50）
        /// </summary>
        public DateTime PaymentTime { get; set; }
    }

    public class errorimoport
    {
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 交易号
        /// </summary>
        public string TransactionNo { get; set; }

        /// <summary>
        /// 交易号
        /// </summary>
        public string Phone { get; set; }
    }
}
