﻿using KhXt.YcRs.Domain.Models.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseUserWorkModel
    {
        /// <summary>
        /// 用户做题表Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程编码
        /// </summary>		 
        public int CourseId { get; set; }
        /// <summary>
        /// 子课程编码
        /// </summary>		 
        public int CourseChildId { get; set; }
        /// <summary>
        /// 课程作业Id
        /// </summary>
        public int ItemId { get; set; }
        /// <summary>
        /// 用户课程试题Id
        /// </summary>
        public long CourseUserItemId { get; set; }
        /// <summary>
        /// 错误次数
        /// </summary>
        public int ErrorNum { get; set; }
        /// <summary>
        /// 选择的答案 第一次
        /// </summary>
        public string SelectAnswer { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ErrKeyValue> Options { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        public int Creator { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now;
        public int Updater { get; set; }
        public DateTime UpdateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 操作标识  -1 未作答 0 错误 1 正确 
        /// </summary>
        public int OpSign { get; set; }
        /// <summary>
        /// 答题用时
        /// </summary>
        public string UseTime { get; set; }

        #region 课程作业信息
        /// <summary>
        /// 答题分类
        /// </summary>
        public int ClassId { get; set; }

        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题目备选答案，适用用选择题
        /// </summary>

        public Dictionary<string, string> ItemOptions { get; set; }

        /// <summary>
        /// 易错项
        /// </summary>
        public string Confuse { get; set; }
        /// <summary>
        /// 题目内容,适用于填空题型
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 用"@@"分割的答案,适用于选择（一个或多个key值）、填空题
        /// </summary>
        public string Answer { get; set; }

        public string ItemImg { get; set; }
        /// <summary>
        /// 难度
        /// </summary>
        public int Difficulty { get; set; }

        public string Analysis { get; set; }

        public int ItemSort { get; set; }

        public int IsShow { get; set; }

        public int IsDelete { get; set; }

        public string Remark { get; set; }

        #endregion

    }
}
