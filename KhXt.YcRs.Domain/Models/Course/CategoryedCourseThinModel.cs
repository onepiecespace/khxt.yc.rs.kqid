﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CategoryedCourseThinModel
    {
        public long Id { get; set; }

        /// <summary>
        /// 展示模式，1竖 2横 
        /// </summary>
        public int Mode { get; set; }

        [JsonProperty("cap")]
        public string Caption { get; set; }

        [JsonProperty("vals")]
        public List<CourseThinModel> Values { get; set; }
    }

    public class CategoryedCoursePcThinModel
    {
        public long Id { get; set; }

        /// <summary>
        /// 展示模式，1竖 2横 
        /// </summary>
        public int Mode { get; set; }

        [JsonProperty("cap")]
        public string Caption { get; set; }

        [JsonProperty("vals")]
        public List<CoursePcThinModel> Values { get; set; }
    }
}