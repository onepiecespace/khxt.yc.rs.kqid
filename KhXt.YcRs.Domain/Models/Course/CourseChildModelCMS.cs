﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseChildModelCMS
    {
        public long Id { get; set; }

        /// <summary>
        /// Pid
        /// </summary>		 
        public int ParentId { get; set; }

        /// <summary>
        /// ChildName
        /// </summary>		 
        public string ChildName { get; set; }

        /// <summary>
        /// ChildTeacher
        /// </summary>		 
        public string ChildTeacher { get; set; }

        /// <summary>
        /// ChildType
        /// </summary>		 
        public int ChildType { get; set; }

        /// <summary>
        /// ChildDes
        /// </summary>		 
        public string ChildDes { get; set; }

        /// <summary>
        /// ChildUrl
        /// </summary>		 
        public string ChildUrl { get; set; }

        /// <summary>
        /// ChildSort
        /// </summary>		 
        public int ChildSort { get; set; }

        /// <summary>
        /// ChildStartTime
        /// </summary>		 
        public DateTime ChildStartTime { get; set; }

        /// <summary>
        /// ChildEndTime
        /// </summary>		 
        public DateTime ChildEndTime { get; set; }

        /// <summary>
        /// ChildTimeLength
        /// </summary>		 
        public string ChildTimeLength { get; set; }

        /// <summary>
        /// ChildRelevanceFile
        /// </summary>		 
        public string ChildRelevanceFile { get; set; }
        public string ChannelId { get; set; }
        public string videoId { get; set; }


        /// <summary>
        /// VideoCode1
        /// </summary>		 
        public string VideoCode1 { get; set; }

        /// <summary>
        /// VideoCode2
        /// </summary>		 
        public string VideoCode2 { get; set; }

        /// <summary>
        /// CreateId
        /// </summary>		 
        public int CreateId { get; set; }

        /// <summary>
        /// IsDelete
        /// </summary>		 
        public int IsDelete { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>		 
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// UpdateTime
        /// </summary>		 
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// TenantId
        /// </summary>		 
        public int TenantId { get; set; }

    }
}
