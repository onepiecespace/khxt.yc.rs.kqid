﻿using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Pay;
using System;
using System.Collections.Generic;
using System.Security.Policy;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseChildListInfo
    {
        public string ChildIds { get; set; }
        /// <summary>
        ///  1已购买，0未购买
        /// </summary>
        public int IsBuy { get; set; }
        public string OrderId { get; set; }
        public bool authen { get; set; }
        public string SecretKey { get; set; }


        public List<CourseChildInfo> CourseChildList { get; set; }

    }
    public class CourseItemErrListInfo
    {
        public string ErrId { get; set; }

    }
    public class CourseItemDoneListInfo
    {
        public long CourseId { get; set; }
        public string CourseName { get; set; }
        public long CourseChildId { get; set; }
        public List<CourseUserWorkModel> BookList { get; set; }
        public int AlreadyDone { get; set; }
        public int TotalDone { get; set; }
    }




    public class CourseItemCountListInfo
    {

        public int TrueCount { get; set; }
        public int ErrCount { get; set; }
        public List<CourseItemCountInfo> BookList { get; set; }
        public int Coin { get; set; }
        public int Exp { get; set; }
    }

    public class CourseItemCountInfo
    {
        public int Id { get; set; }
        public bool IsTrue { get; set; }
    }












}

