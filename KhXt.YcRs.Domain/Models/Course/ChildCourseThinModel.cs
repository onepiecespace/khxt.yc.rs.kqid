﻿using System;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class ChildCourseThinModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int ChildType { get; set; }
        public string ChannelId { get; set; }
        public string videoId { get; set; }

        public string Teacher { get; set; }
        public DateTime? LiveStart { get; set; }
        public DateTime? LiveEnd { get; set; }
        public string TimeDesc { get; set; }

        public int Sort { get; set; }
    }
}