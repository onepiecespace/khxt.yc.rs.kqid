﻿namespace KhXt.YcRs.Domain.Models.Course
{
    public class LiveInfo
    {
        public int Id { get; set; }

        public string CourseName { get; set; }

        public string CoursePicPhoneMini { get; set; }

        public string CoursePicPhoneTran { get; set; }

        public string ChannelId { get; set; }
    }
}