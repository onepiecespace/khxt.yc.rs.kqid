﻿using System;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CouerseDetailedListInfo
    {
        public long CourseId { get; set; }
        public string CourseName { get; set; }
        public int ClassId { get; set; }

        public int Cateorys { get; set; }
        public string ChildIds { get; set; }

        public DateTime LiveBeginTime { get; set; }
        public DateTime LiveEndTime { get; set; }
        public DateTime ValidBeginTime { get; set; }
        public DateTime ValidEndTime { get; set; }
        public DateTime PreferentialBeginTime { get; set; }
        public DateTime PreferentialEndTime { get; set; }
        public string TeachersUserName { get; set; }
        public int TeachersUserId { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountPrice { get; set; }
        public string Detailed { get; set; }
        public decimal InternalPrice { get; set; }
        public string CoursePic { get; set; }
        public string CoursePicPhone { get; set; }
        public int VirtualSales { get; set; }
        public int Sales { get; set; }

        public int IsLongValid { get; set; }
        public int IsDiscount { get; set; }
        public int IsExpress { get; set; }
        public int IsComic { get; set; }
        public int IsRecord { get; set; }
        public int IsLive { get; set; }
        public int IsDelete { get; set; }
        public int Status { get; set; }
        public int Sort { get; set; }
        public string Iosproductid { get; set; }
        /// <summary>
        /// 学好语文的秘密,learnchinesewell,98
        /// 小学生必背古诗词,studentsgushici,198
        /// 成语里的作文课,chengyuzuowenclass,298
        /// 汉字背后的秘密,chinesebacksecrecy,298
        /// 超级口才明星班,superkoucaistarclass,898
        /// 高效阅读,gaoxiaoreading,1498
        /// 名著阅读,mingzhureading,1998
        /// 创新作文,chuangxinzuowen,1198
        /// 中国历史名人故事,chinesestarstory,98
        /// </summary>
        public string IosPayPirce { get; set; }
        public string IosProductidName { get; set; }
        /// <summary>
        /// 域名地址
        /// </summary>
        public string baseurl { get; set; }
        #region 扩展
        public List<string> CoursePicContent { get; set; }

        /// <summary>
        ///  1已购买，0未购买
        /// </summary>
        public int IsBuy { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpirationDate { get; set; }
        /// <summary>
        /// 过期是否隐藏：1隐藏0显示
        /// </summary>
        public int IsHide { get; set; }
        /// <summary>
        /// 是否过期 true 过期  false  未过期
        /// </summary>		 
        public bool Isexpired { get; set; }
        /// <summary>
        /// 过期消息
        /// </summary>
        public string exmsg { get; set; }
        /// <summary>
        /// 1收藏，0未收藏
        /// </summary>
        public int IsCollection { get; set; }
        /// <summary>
        /// 详情页
        /// </summary>
        public string H5Url { get; set; }
        public int ChannelId { get; set; }

        public string videoId { get; set; }

        public string SecretKey { get; set; }

        public string Teachers { get; set; }
        public string LiveBeginTimeStr { get; set; }
        public string LiveEndTimeStr { get; set; }
        public string ValidBeginTimeStr { get; set; }
        public string ValidEndTimeStr { get; set; }
        public string PreferentialBeginTimeStr { get; set; }
        public string PreferentialEndTimeStr { get; set; }
        #endregion
    }
}