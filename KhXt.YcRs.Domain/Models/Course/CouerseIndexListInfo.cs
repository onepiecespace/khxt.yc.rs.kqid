﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CouerseIndexListInfo
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public string IosVersion { get; set; }
        public PageList<CouerseInfo> CouerseList { get; set; }
        public List<CateoryInfo> NavCateoryList { get; set; }
        public List<CateoryInfo> TopCateoryList { get; set; }
    }
}