﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseLogModel
    {
        /// <summary>
        /// 直播频道Id
        /// </summary>
        [DisplayName("直播频道Id")]
        public string ChannelId { get; set; }
        /// <summary>
        /// 子课程类型1直播中2录播（回放）3点播 4 未开始 5 转存中
        /// </summary>
        [DisplayName("子课程类型")]
        public int ChildType { get; set; }


        /// <summary>
        /// 用户Id
        /// </summary>
        [DisplayName("用户Id")]
        public long UserId { get; set; }
        /// <summary>
        /// 用户IP
        /// </summary>
        [DisplayName("用户IP")]
        public string UserIp { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [DisplayName("标题")]
        public string Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [DisplayName("内容")]
        public string Content { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        [DisplayName("时间")]
        public DateTime Datatime { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [DisplayName("用户名")]
        public string UserName { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [DisplayName("手机号")]
        public string Phone { get; set; }
    }
}
