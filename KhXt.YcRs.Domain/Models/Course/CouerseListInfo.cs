﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CouerseListInfo
    {
        public PageList<CouerseInfo> CouerseList { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
    }
}