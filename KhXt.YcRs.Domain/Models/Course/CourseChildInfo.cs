﻿using System;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseChildInfo
    {
        public long id { get; set; }
        /// <summary>
        /// Pid
        /// </summary>		 
        public int ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ChildNameAndTeacher { get; set; }
        /// <summary>
        /// ChildName
        /// </summary>		 
        public string ChildName { get; set; }

        /// <summary>
        /// ChildTeacher
        /// </summary>		 
        public string ChildTeacher { get; set; }
        /// <summary>
        /// 是否试听1试听0不能试听
        /// </summary>
        public int IsTrialable { get; set; }
        /// <summary>
        /// ChildType
        /// </summary>		 
        public int ChildType { get; set; }

        /// <summary>
        /// ChildDes
        /// </summary>		 
        public string ChildDes { get; set; }
        /// <summary>
        /// 课程排序
        /// </summary>
        public int ChildSort { get; set; }
        /// <summary>
        /// 子课程时长
        /// </summary>
        public string ChildTimeLength { get; set; }
        /// <summary>
        /// 子课程学习人数
        /// </summary>
        public int ChildLerunCount { get; set; }
        /// <summary>
        /// 频道
        /// </summary>
        public string ChannelId { get; set; }

        /// <summary>
        /// 子课程的音视频表示
        /// </summary>
        public string videoId { get; set; }
        /// <summary>
        /// 子课程关联文件
        /// </summary>
        public string ChildRelevanceFile { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public string ChildStartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string ChildEndTime { get; set; }

        /// <summary>
        /// IsDelete
        /// </summary>		 
        public int IsDelete { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>		 
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// UpdateTime
        /// </summary>		 
        public DateTime UpdateTime { get; set; }
    }

    public class CourseisliveChildInfo
    {
        /// <summary>
        /// 子课程ID
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// Pid
        /// </summary>		 
        public int ParentId { get; set; }

        /// <summary>
        /// 课程ID
        /// </summary>		 
        public string CourseName { get; set; }
        /// <summary>
        /// 课程直播开关
        /// </summary>		 
        public int IsLive { get; set; }
        /// <summary>
        /// 课程横图片
        /// </summary>		 
        public string img { get; set; }
        /// <summary>
        /// ChildName
        /// </summary>		 
        public string ChildName { get; set; }
        /// <summary>
        /// ChildTeacher
        /// </summary>		 
        public string ChildTeacher { get; set; }

        /// <summary>
        /// ChildType
        /// </summary>		 
        public int ChildType { get; set; }

        /// <summary>
        /// 课程排序
        /// </summary>
        public int ChildSort { get; set; }
        /// <summary>
        /// 子课程时长
        /// </summary>
        public string ChildTimeLength { get; set; }

        /// <summary>
        /// 频道
        /// </summary>
        public string ChannelId { get; set; }

        /// <summary>
        /// 子课程的音视频表示
        /// </summary>
        public string videoId { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public string ChildStartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string ChildEndTime { get; set; }

    }
}