﻿using System;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class OrderListInfo
    {
        public int HaveAddress { get; set; }
        public AddressInfo AddressList { get; set; }
        public PayOrderInfo PayOrderList { get; set; }
    }


    public class OrderListOfCMS
    {
        public long Id { get; set; }
        public long CourseId { get; set; }
        public string OrderNo { get; set; }
        public long UserId { get; set; }
        public decimal PayPrice { get; set; }
        /// <summary>
        /// 订单状态 0 进行中 1已完成 2取消交易 3已结算
        /// </summary>
        public int OrderState { get; set; }
        public int IsPay { get; set; }
        public DateTime PayTime { get; set; }
        /// <summary>
        /// 付款方式0 免费 1线下付款,2微信 3支付宝 4 Apple 5银联
        /// </summary>
        public int PayType { get; set; }
        /// <summary>
        /// 支付平台 返回的 编号
        /// </summary>
        public string PayNo { get; set; }
        public int IsSend { get; set; }
        public DateTime SendTime { get; set; }
        public int IsGet { get; set; }
        public DateTime GetTime { get; set; }

        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }

        public string CourseName { get; set; }

        public decimal Price { get; set; }

        public string UserName { get; set; }
    }
}