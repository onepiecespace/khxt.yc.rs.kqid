﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class UserCourseChildInfo
    {
        /// <summary>
        /// 用户子课程进度记录Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 课程id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 子课程Id
        /// </summary>
        public long CourseChildId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 子课程总时长
        /// </summary>
        public int DurationTime { get; set; }
        /// <summary>
        /// 当前观看时长
        /// </summary>
        public int CurrentTime { get; set; }
        /// <summary>
        /// 观看次数 累加1
        /// </summary>
        public int LiveNum { get; set; }
        /// <summary>
        /// 观看进度
        /// </summary>
        public float ChildProgress { get; set; }
        /// <summary>
        /// 观看完成标识 0未完成 1 已完成 (DurationTime=CurrentTime)
        /// </summary>
        public bool IsFinish { get; set; }

    }

}
