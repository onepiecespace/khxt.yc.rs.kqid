﻿using System;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CouerseInfo
    {
        /// <summary>
        /// auto_increment
        /// </summary>		 
        public int Id { get; set; }
        /// <summary>
        /// CourseName
        /// </summary>		 
        public string CourseName { get; set; }

        public string Detailed { get; set; }
        /// <summary>
        /// BusinessType
        /// </summary>		 
        public int ClassId { get; set; }
        /// <summary>
        /// ChildIds
        /// </summary>		 
        public string ChildIds { get; set; }
        /// <summary>
        /// 关联年级id集合
        /// </summary>		 
        public string GradeIds { get; set; }
        /// <summary>
        /// 分类标签Id集合
        /// </summary>		 
        public string Cateorys { get; set; }
        /// <summary>
        /// VirtualSales
        /// </summary>
        public int VirtualSales { get; set; }
        /// <summary>
        /// 学习人数
        /// </summary>
        public string StudentsCount { get; set; }
        /// <summary>
        /// 讲师
        /// </summary>
        public string TeachersUserName { get; set; }
        /// <summary>
        /// IsTop
        /// </summary>
        public int IsTop { get; set; }
        /// <summary>
        /// IsHot
        /// </summary>
        public int IsHot { get; set; }

        public decimal DiscountPrice { get; set; }

        public decimal IosPayPirce { get; set; }
        /// <summary>
        /// CoursePicPhone
        /// </summary>
        public string CoursePicPhone { get; set; }

        public string CoursePicPhoneMini { get; set; }

        public int IsLive { get; set; }
        /// <summary>
        /// 课程是否测评0否1是
        /// </summary>
        public int IsRecord { get; set; }
        /// <summary>
        /// LiveBeginTime
        /// </summary>		 
        public DateTime LiveBeginTime { get; set; }
        /// <summary>
        /// LiveEndTime
        /// </summary>		 
        public DateTime LiveEndTime { get; set; }
        /// <summary>
        /// ValidBeginTime
        /// </summary>		 
        public DateTime ValidBeginTime { get; set; }
        /// <summary>
        /// ValidEndTime
        /// </summary>		 
        public DateTime ValidEndTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string H5Url { get; set; }



    }
}