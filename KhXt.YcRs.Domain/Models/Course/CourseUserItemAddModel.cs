﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseUserItemAddModel
    {
        /// <summary>
        /// 题目id 
        /// </summary>
        [Required]
        public string ItemId { get; set; }
        /// <summary>
        /// 用户课程试题Id
        /// </summary>
        public long CourseUserItemId { get; set; }
        /// <summary>
        /// 选择的答案
        /// </summary>
        [Required]
        public string SelectAnswer { get; set; }
        /// <summary>
        /// 花费时间
        /// </summary>
        [Required]
        public int UseTime { get; set; }
        /// <summary>
        /// 答题标识 -1 未作答 0 错误 1 正确
        /// </summary>
        [Required]
        public int AnswerSign { get; set; }
        /// <summary>
        /// 课程id
        /// </summary>
        [Required]
        public int CourseId { get; set; }
        /// <summary>
        /// 子课程Id
        /// </summary>
        [Required]
        public int CourseChildId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        [Required]
        public int UserId { get; set; }
    }
}
