﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseUserListModel
    {
        public int CourseId { get; set; }
        public int CourseChildId { get; set; }
        public string ChildName { get; set; }
        public string ChildTeacher { get; set; }
        public string ChildStartTime { get; set; }
        public string ChildEndTime { get; set; }
        public long ChildTimeLength { get; set; }
        public string ChildTimestr { get; set; }
        public int AlreadyDone { get; set; }
        public int TotalDone { get; set; }
        public double Progress =>
            TotalDone == 0 ? 0 : Convert.ToDouble(AlreadyDone) / Convert.ToDouble(TotalDone);
        //观看进度
        public double ChildProgress { get; set; }
        public int ChildType { get; set; }
        public int ChildSort { get; set; }
        public string ChildCode { get; set; }
        public string ChannelId { get; set; }
        public string videoId { get; set; }
        public string ChildRelevanceFile { get; set; }
        public UserCourseChildInfo UserChildProgess { get; set; }
    }
}
