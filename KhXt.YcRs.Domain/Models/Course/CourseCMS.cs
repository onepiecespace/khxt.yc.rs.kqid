﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseCMS
    {
        public long id { get; set; }
        /// <summary>
        /// Pid
        /// </summary>		 
        public int ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ChildNameAndTeacher { get; set; }
        /// <summary>
        /// ChildName
        /// </summary>		 
        public string ChildName { get; set; }

        /// <summary>
        /// ChildTeacher
        /// </summary>		 
        public string ChildTeacher { get; set; }
        /// <summary>
        /// 是否试听1试听0不能试听
        /// </summary>
        public int IsTrialable { get; set; }
        /// <summary>
        /// ChildType
        /// </summary>		 
        public int ChildType { get; set; }

        /// <summary>
        /// ChildDes
        /// </summary>		 
        public string ChildDes { get; set; }
        /// <summary>
        /// 课程排序
        /// </summary>
        public int ChildSort { get; set; }
        /// <summary>
        /// 子课程时长
        /// </summary>
        public string ChildTimeLength { get; set; }
        /// <summary>
        /// 子课程学习人数
        /// </summary>
        public int ChildLerunCount { get; set; }
        /// <summary>
        /// 子课程的音视频表示
        /// </summary>
        public string videoId { get; set; }

        /// <summary>
        /// CourseName
        /// </summary>		 
        public string CourseName { get; set; }


        public Decimal IosPayPirce { get; set; }
        public Decimal Price { get; set; }

        public int IsDelete { get; set; }

        public string Cateorys { get; set; }
        public string Name { get; set; }
    }
}
