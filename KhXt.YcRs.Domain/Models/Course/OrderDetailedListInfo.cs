﻿namespace KhXt.YcRs.Domain.Models.Course
{
    public class OrderDetailedListInfo
    {
        public int PayStatus { get; set; }
        public string PayStateCaption { get; set; }

        public string CoursePicPhoneMini { get; set; }
        public string CourseName { get; set; }
        public string Detailed { get; set; }
        public string OrderNo { get; set; }
        public string PayTime { get; set; }
        public int PayType { get; set; }

        public string payCaption { get; set; }
        public string OrderTime { get; set; }
        /// <summary>
        /// 原价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 折扣价 支付价（安卓与ios 价格）
        /// </summary>
        public decimal DiscountPrice { get; set; }
        /// <summary>
        /// 是否快递 0 否 1是
        /// </summary>
        public int IsExpress { get; set; }

        /// <summary>
        /// 运费
        /// </summary>      
        public decimal ExpressPirce { get; set; }


        public string Iosproductid { get; set; }
        public decimal IosPayPirce { get; set; }
        public string IosProductidName { get; set; }
        /// <summary>
        /// 过期是否隐藏：1隐藏0显示
        /// </summary>
        public int IsHide { get; set; }
        /// <summary>
        /// 是否过期 true 过期  false  未过期
        /// </summary>		 
        public bool Isexpired { get; set; }
        /// <summary>
        /// 过期消息
        /// </summary>
        public string exmsg { get; set; }
    }
}