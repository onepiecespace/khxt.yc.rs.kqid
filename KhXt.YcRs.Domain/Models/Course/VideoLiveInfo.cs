﻿namespace KhXt.YcRs.Domain.Models.Course
{
    public class VideoLiveInfo
    {
        public string videoId { get; set; }
        public string channelId { get; set; }
        public string title { get; set; }
    }
}