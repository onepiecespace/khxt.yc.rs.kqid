﻿namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseItemListInfo
    {
        /// <summary>
        /// 是否授权
        /// </summary>
        public bool authen { get; set; }
        /// <summary>
        /// 授权的密钥
        /// </summary>
        public string SecretKey { get; set; }
        /// <summary>
        ///  微信二维码 true 有false 否
        /// </summary>
        public bool wxstatus { get; set; }
        /// <summary>
        /// 二维码地址
        /// </summary>
        public string wxqrcode { get; set; }
        /// <summary>
        /// 每页显示的记录数
        /// </summary>
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        /// <summary>
        /// 要分页的数据总数
        /// </summary>
        public int TotalCount { get; set; }
        public PageList<CourseUserListModel> CourseItemList { get; set; }

    }
}