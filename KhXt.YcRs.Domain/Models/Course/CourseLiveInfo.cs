﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseThinModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonProperty("tea")]
        public string Teacher { get; set; }

        [JsonIgnore]
        public long[] TeacherIds { get; set; }

        public string Url { get; set; }

        public string Img { get; set; }
        public string PcImg { get; set; }
        /// <summary>
        ///0: 直播（未开始/已结束） 1:直播中 2:即将开始 
        /// </summary>
        [JsonProperty("lvst")]
        public int LiveStatus    //TIP：model 不应该有业务逻辑，抽空改
        {
            get; set;
            //get {
            //    var ct = DateTime.Now;
            //    if (ct > LiveEnd) return 0;
            //    if (ct> LiveStart) return 1;

            //    return 2; 
            //}
        }

        [JsonIgnore]
        public DateTime LiveStart { get; set; }
        [JsonIgnore]
        public DateTime LiveEnd { get; set; }

        /// <summary>
        /// 是否免费课程:1,0
        /// </summary>
        public int IsFree { get; set; }

        [JsonProperty("ct")]
        public int CourseType { get; set; }  //直播1，录播 2

        [JsonProperty("summ")]
        public string Summary { get; set; }

        [JsonProperty("cnt")]
        public string StudentsCount { get; set; }

        [JsonProperty("disprice")]
        public decimal DiscountPrice { get; set; }

        [JsonProperty("iosprice")]
        public decimal IosPayPirce { get; set; }

        public int Status { get; set; }
        //[JsonIgnore]
        public int Sort { get; set; }

        //[JsonIgnore]
        public int Category { get; set; }
        /// <summary>
        /// 课程是否测评0否1是
        /// </summary>
        public int IsRecord { get; set; }
        [JsonIgnore]
        public int[] Grade
        {
            get
            {
                return GradeValues.Split(",").Select(v => Convert.ToInt32(v)).ToArray();
            }
        }

        [JsonIgnore]
        public string GradeValues { get; set; }

        public int IsTop { get; set; }

        [JsonIgnore]
        public int IsHot { get; set; }

        [JsonProperty("tmr")]
        public string TimeDesc { get; set; }

        [JsonIgnore]
        public IEnumerable<ChildCourseThinModel> Children { get; set; }


        public CourseThinModel Clone()
        {
            var model = new CourseThinModel
            {
                Id = this.Id,
                Name = Name,
                Teacher = Teacher,
                Url = Url,
                Img = Img,
                PcImg = PcImg,
                LiveStatus = LiveStatus,
                LiveStart = LiveStart,
                LiveEnd = LiveEnd,
                IsFree = IsFree,
                DiscountPrice = DiscountPrice,
                IosPayPirce = IosPayPirce,
                CourseType = CourseType,
                Summary = Summary,
                StudentsCount = StudentsCount,
                Sort = Sort,
                Status = Status,
                Category = Category,
                GradeValues = GradeValues,
                IsTop = IsTop,
                IsHot = IsHot,
                IsRecord = IsRecord,
                TimeDesc = TimeDesc
            };
            return model;
        }



    }

    public class CoursePcThinModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonProperty("tea")]
        public string Teacher { get; set; }

        public long[] TeacherIds { get; set; }

        public string Url { get; set; }

        public string Img { get; set; }
        public string PcImg { get; set; }
        /// <summary>
        ///0: 非直播 1:直播中 2:即将开始
        /// </summary>
        [JsonProperty("lvst")]
        public int LiveStatus    //TIP：model 不应该有业务逻辑，抽空改
        {
            get; set;
        }

        public DateTime LiveStart { get; set; }
        public DateTime LiveEnd { get; set; }

        /// <summary>
        /// 是否免费课程:1,0
        /// </summary>
        public int IsFree { get; set; }

        [JsonProperty("ct")]
        public int CourseType { get; set; }  //直播1，录播 2

        [JsonProperty("summ")]
        public string Summary { get; set; }

        [JsonProperty("cnt")]
        public string StudentsCount { get; set; }

        [JsonProperty("disprice")]
        public decimal DiscountPrice { get; set; }

        [JsonProperty("iosprice")]
        public decimal IosPayPirce { get; set; }

        public int Sort { get; set; }
        public int Status { get; set; }
        public int Category { get; set; }
        public int ClassId { get; set; }
        [JsonIgnore]
        public int[] Grade
        {
            get
            {
                return GradeValues.Split(",").Select(v => Convert.ToInt32(v)).ToArray();
            }
        }

        public string GradeValues { get; set; }

        public int IsTop { get; set; }

        public int IsHot { get; set; }

        [JsonProperty("tmr")]
        public string TimeDesc { get; set; }

        [JsonIgnore]
        public IEnumerable<ChildCourseThinModel> Children { get; set; }
        /// <summary>
        /// 子章节数量
        /// </summary>
        public int ChildCount { get; set; }
        public CoursePcThinModel PcClone()
        {
            var model = new CoursePcThinModel
            {
                Id = this.Id,
                Name = Name,
                Teacher = Teacher,
                Url = Url,
                Img = Img,
                PcImg = PcImg,
                LiveStatus = LiveStatus,
                LiveStart = LiveStart,
                LiveEnd = LiveEnd,
                IsFree = IsFree,
                DiscountPrice = DiscountPrice,
                IosPayPirce = IosPayPirce,
                CourseType = CourseType,
                Summary = Summary,
                StudentsCount = StudentsCount,
                Sort = Sort,
                Status = Status,
                Category = Category,
                ClassId = ClassId,
                GradeValues = GradeValues,
                IsTop = IsTop,
                IsHot = IsHot,
                TimeDesc = TimeDesc,
                ChildCount = ChildCount
            };
            return model;
        }
    }

}