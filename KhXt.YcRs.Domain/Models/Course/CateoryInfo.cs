﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.Course
{
    public class CateoryInfo
    {
        public int Id { get; set; }
        /// <summary>
        /// 是否首页
        /// </summary>
        public int IsHomeTop { get; set; }
        /// <summary>
        /// 是否导航
        /// </summary>
        public int IsNav { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 分类图片
        /// </summary>
        public string Img { get; set; }
        /// <summary>
        /// 提示语
        /// </summary>
        public string Des { get; set; }
        /// <summary>
        /// 二级分类
        /// </summary>
        public List<CateoryInfo> Twolist { get; set; }
    }
}