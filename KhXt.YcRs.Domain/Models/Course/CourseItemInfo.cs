﻿namespace KhXt.YcRs.Domain.Models.Course
{
    public class CourseItemInfo
    {
        /// <summary>
        /// 已废弃
        /// </summary>
        public long Id { get; set; }
        public long CourseId { get; set; }
        public long CourseChildId { get; set; }
        /// <summary>
        /// 已废弃
        /// </summary>
        public string ItemId { get; set; }

        public string ChildName { get; set; }

        public string ChildTeacher { get; set; }
        public int ChildType { get; set; }
        public int ChildSort { get; set; }
        public string ChildStartTime { get; set; }

        public string ChildEndTime { get; set; }
        public long ChildTimeLength { get; set; }
        public long ChildDurationTime { get; set; }
        public int AlreadyDone { get; set; }
        public int TotalDone { get; set; }


    }
}