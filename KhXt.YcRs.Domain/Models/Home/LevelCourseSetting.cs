﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Home
{
    public class LevelCourseSettingModel
    {
        public long Id { get; set; }

        public int Mode { get; set; } = 2;

        public string Caption { get; set; }

        /// <summary>
        /// 1竖 2横
        /// </summary>
        public int PictureDirect { get; set; } = 2;

        public int Sort { get; set; } = 0;
        public int[] Cat { get; set; }
        public int[] Values { get; set; }

    }
    public class LevelCourseListSettingModel
    {
        public long Id { get; set; }

        public int Mode { get; set; } = 2;

        public string Caption { get; set; }

        /// <summary>
        /// 1竖 2横
        /// </summary>
        public int PictureDirect { get; set; } = 2;

        public int Sort { get; set; } = 0;
        public int[] Cat { get; set; }
        public long[] Values { get; set; }

    }
}
