﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Home
{
    public class HomeCourseSettingModel
    {
        public long Id { get; set; }

        public int Mode { get; set; } = 1;

        public string Caption { get; set; }

        /// <summary>
        /// 1竖 2横
        /// </summary>
        public int PictureDirect { get; set; } = 1;

        public int Sort { get; set; } = 0;
        public long[] Values { get; set; }

    }
}
