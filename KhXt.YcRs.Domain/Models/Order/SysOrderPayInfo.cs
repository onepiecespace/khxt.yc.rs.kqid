﻿using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    /// <summary>
    /// 
    /// </summary>
    public class SysOrderPayInfoResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<SysOrderPayInfo> Rows { get; set; }
        /// <summary>
        /// 总数
        /// </summary>
        public int Total { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    /// <summary>
    /// 订单支付信息
    /// </summary>

    public class SysOrderPayInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [ColName("订单Id")]
        public string OrderId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [ColName("用户名")]
        public string UserName { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        [ColName("手机号码")]
        public string Phone { get; set; }
        /// <summary>
        /// 购买名称
        /// </summary>
        [ColName("购买名称")]
        public string ItemName { get; set; }
        /// <summary>
        /// 交易号
        /// </summary>
        [ColName("交易号")]
        public string TransactionNo { get; set; }
        /// <summary>
        /// /
        /// </summary>
        [ColName("价格")]
        public string Price { get; set; }
        /// <summary>
        /// IosProductId
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PaymentType { get; set; }

        /// <summary>
        /// 是否自动续费
        /// </summary>
        public bool IsAutoRenew { get; set; } = false;

        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 代理商
        /// </summary>
        public string Aid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int BusinessType { get; set; }
        /// <summary>
        /// 更新
        /// </summary>
        public string UpdateTime { get; set; }
        /// <summary>
        /// 是否退款
        /// </summary>
        public int IsRefund { get; set; } = 0;
    }

    [Header(Color = ColorEnum.DARK_BLUE, FontSize = 10, IsBold = true)] //表头样式
    [WrapText] //自动换行
    public class SysOrderPayInfoExport
    {

        public long UserId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [ColName("用户名")]
        public string UserName { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        [ColName("手机号码")]
        public string Phone { get; set; }

        /// <summary>
        /// 购买名称
        /// </summary>
        [ColName("购买名称")]
        public string ItemName { get; set; }
        /// <summary>
        /// /
        /// </summary>
        [ColName("价格")]
        public string Price { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [ColName("购买时间")]
        public string CreateTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [ColName("类别")]
        public string BusinessTypeName { get; set; }

        /// <summary>
        /// 代理商
        /// </summary>
        [ColName("代理商名称")]
        public string Aid { get; set; }

        [ColName("支付方式")]
        public string PaymentTypeName { get; set; }

        /// <summary>
        /// 交易号
        /// </summary>
        [ColName("交易号")]
        public string TransactionNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [ColName("订单Id")]
        public string OrderId { get; set; }
        /// <summary>
        /// 是否退款
        /// </summary>
        [ColName("是否退款")]
        public string IsRefundValue { get; set; }
    }
}
