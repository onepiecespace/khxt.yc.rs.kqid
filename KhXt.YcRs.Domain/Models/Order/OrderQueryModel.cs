﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    public class OrderQueryModel
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int BusinessType { get; set; }
        public int PayType { get; set; } = -1;
        public string Aid { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string ItemName { get; set; }
        public string TransactionNo { get; set; }
        public string OrderId { get; set; }
        /// <summary>
        /// 是否退款
        /// </summary>
        public int? IsRefund { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 15;
    }
}
