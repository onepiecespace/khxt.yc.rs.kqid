﻿using KhXt.YcRs.Domain.Entities.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    /// <summary>
    /// 我的优惠券
    /// </summary>
    public class MyCoupon
    {
        /// <summary>
        /// 优惠券信息
        /// </summary>
        public CouponInfoEntity CouponInfo { get; set; }
        /// <summary>
        /// UserCouponId
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 优惠券Id
        /// </summary>
        public long CouponId { get; set; }

        /// <summary>
        /// 优惠券No
        /// </summary>
        public string CouponNo { get; set; }
        /// <summary>
        /// vip限制0 不限制 1月卡2半年卡 4年卡
        /// </summary>
        public long VipType { get; set; }

        /// <summary>
        ///限制类别 0 无限制 1 会员可用  2 仅自己可用 3 仅好友可用
        /// </summary>
        public int UseLimit { get; set; }

        /// <summary>
        /// 是否可以组合使用 0 不可以 1 可以
        /// </summary>
        public int IsCombination { get; set; }

        /// <summary>
        /// 领取时间
        /// </summary>
        public DateTime ReceiveTime { get; set; }

        /// <summary>
        /// 到期时间
        /// </summary>
        public DateTime ExpiredTime { get; set; }
        /// <summary>
        /// 花费金币
        /// </summary>
        public int SpendCoins { get; set; }

        /// <summary>
        /// 赠送人
        /// </summary>
        public string Giver { get; set; }

        /// <summary>
        /// 赠送时间
        /// </summary>
        public DateTime? GiveTime { get; set; } = null;

        /// <summary>
        /// 状态 -1 已作废  0 未使用 1 已使用 2 已过期 
        /// </summary>
        public int Status { get; set; } = 0;

        /// <summary>
        /// 使用时间
        /// </summary>
        public DateTime? UseTime { get; set; } = null;

        /// <summary>
        /// 优惠券来自 0 系统赠送 1 好友赠送 2 用户购买 
        /// </summary>
        public int IsFrom { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public long Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 业务类别
        /// </summary>
        public long BusinessType { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public long Updater { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 可用平台 0 无限制 1 App 
        /// </summary>
        public int PlatForm { get; set; } = 0;

        /// <summary>
        /// 是否可用
        /// </summary>
        public bool IsCanUse { get; set; } = true;

    }

    /// <summary>
    /// 我的优惠券列表
    /// </summary>
    public class MyCouponList
    {
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MyCoupon> CouponList { get; set; }
    }

    /// <summary>
    /// 我可用的优惠券列表
    /// </summary>
    public class MyCanUseCouponList
    {
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 可用优惠券个数
        /// </summary>
        public int TotalCount { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public List<MyCoupon> CouponList { get; set; }
    }
}
