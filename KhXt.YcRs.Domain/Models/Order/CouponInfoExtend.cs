﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    /// <summary>
    /// 优惠券详细信息
    /// </summary>
    public class CouponInfoExtend
    {
        /// <summary>
        /// 优惠券Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        ///分类 0 立减券 1 满减券 2 折扣券 3 兑换券
        /// </summary>
        public int Category { get; set; }
        /// <summary>
        /// 业务类别
        /// </summary>
        public long BusinessType { get; set; }
        /// <summary>
        /// 业务名称
        /// </summary>
        public long BusinessName { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 副标题
        /// </summary>
        public string Subtitle { get; set; }
        /// <summary>
        /// 活动链接
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// 折扣
        /// </summary>
        public float? Discount { get; set; }
        /// <summary>
        /// 限制价格 满多少减 0 无限制
        /// </summary>
        public float LimitPrice { get; set; }
        /// <summary>
        /// vip限制 1月卡2半年卡 4年卡
        /// </summary>
        public long VipType { get; set; }

        /// <summary>
        /// 限制类别 0 无限制 1 会员可用  2 仅自己可用 3 仅好友可用
        /// </summary>
        public int LimitCategory { get; set; }
        /// <summary>
        /// 领取次数 0 无限制 
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 发放数量 -1 无限制  
        /// </summary>
        public int IssueCount { get; set; } = -1;

        /// <summary>
        /// 是否已发放
        /// </summary>
        public int Issued { get; set; }

        /// <summary>
        /// 已经发放数量
        /// </summary>
        public long IssuedCount { get; set; }
        /// <summary>
        /// 花费金币 0 不需要
        /// </summary>
        public int SpendCoins { get; set; }
        /// <summary>
        /// 冲突优惠券
        /// </summary>
        public string ConflictIds { get; set; }
        /// <summary>
        /// 是否固定天数
        /// </summary>
        public int IsFixedDay { get; set; }
        /// <summary>
        /// 开始生效时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpiredTime { get; set; }

        /// <summary>
        /// 有效天数 0 无限制
        /// </summary>
        public int ValidDate { get; set; }
        /// <summary>
        /// -1 已作废  0 未启用 1 启用 
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public long Updater { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }

    }
}
