﻿using KhXt.YcRs.Domain.Entities.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    /// <summary>
    /// 
    /// </summary>
    public class OldOrderStatusModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 总数
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<OldOrderStatusModel> MyOrderList { get; set; } = new List<OldOrderStatusModel>();
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderStatusModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 总数
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<OrderStatusModel> MyOrderList { get; set; }
    }

    /// <summary>
    /// 订单状态
    /// </summary>
    public class OrderStatusModel
    {
        /// <summary>
        /// 是否过期 true 过期  false  未过期
        /// </summary>		 
        public bool Isexpired { get; set; }
        public string exmsg { get; set; }
        /// <summary>
        ///  微信二维码 true 有false 否
        /// </summary>
        public bool wxstatus { get; set; }
        /// <summary>
        /// 二维码地址
        /// </summary>
        public string wxqrcode { get; set; }
        /// <summary>
        /// 订单信息
        /// </summary>
        public OrderInfoEntity OrderInfo { get; set; }

        /// <summary>
        /// 订单详细
        /// </summary>
        public OrderItemEntity OrderItem { get; set; }
        /// <summary>
        /// 订单物流
        /// </summary>
        public OrderShippingEntity OrderShipping { get; set; }
        /// <summary>
        /// 订单优惠
        /// </summary>
        public List<OrderCouponEntity> OrderCoupons { get; set; }
        /// <summary>
        /// 优惠金额（优惠金币数）
        /// </summary>
        public string DiscountedPrice { get; set; }
        /// <summary>
        /// 超时自定取消时长
        /// </summary>
        public long CountDownTime { get; set; }
        /// <summary>
        /// 订单自动取消时间
        /// </summary>
        public DateTime OrderAutoCancelTime { get; set; }

    }
    /// <summary>
    /// 旧版
    /// </summary>
    public class OldOrderStatusModel
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 概述
        /// </summary>
        public string Summary { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal PayPrice { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string OrderImg { get; set; }
        /// <summary>
        /// 下单时间
        /// </summary>
        public string DateTime { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public float Price { get; set; }
        /// <summary>
        /// 课程Id 商品id  vip类型
        /// </summary>
        public string CourseId { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LogisticsId { get; set; }
        /// <summary>
        /// 订单支付类型（注意改字段意义取得是订单状态字段，并不是支付类型）
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// 包裹Id
        /// </summary>
        public string PackageNo { get; set; }
        /// <summary>
        /// 快递类型
        /// </summary>
        public string PackageType { get; set; }
        /// <summary>
        /// 邮费
        /// </summary>
        public string PostFee { get; set; }
        /// <summary>
        /// 快递名称
        /// </summary>
        public string ExpressName { get; set; }
        /// <summary>
        /// 商品Id 
        /// </summary>
        public string ItemId { get; set; }
        /// <summary>
        /// 是否已经评价 0 没有 1 买家已评论 2 双方已评论
        /// </summary>
        public int IsRate { get; set; } = 0;
        /// <summary>
        /// 评论Id
        /// </summary>
        public int CommentId { get; set; }
        /// <summary>
        /// 付款时间
        /// </summary>
        public string PaymentTime { get; set; }

        /// <summary>
        /// 业务类型
        /// </summary>
        public int BusinessType { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long CountDownTime { get; set; }
        /// <summary>
        /// 订单自动取消时间
        /// </summary>
        public DateTime OrderAutoCancelTime { get; set; }
    }
}
