﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    public class OrderQueryResultPageModel
    {
        public List<OrderQueryResultModel> Rows { get; set; }
        public int TotalItemCount { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class OrderQueryResultModel
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// 业务类别
        /// </summary>
        public string BusinessType { get; set; }
        /// <summary>
        /// 实付金额
        /// </summary>
        public string Payment { get; set; }
        /// <summary>
        /// 支付类型
        /// </summary>
        public int PaymentType { get; set; }
        /// <summary>
        /// 支付交易号
        /// </summary>
        public string PaymentNo { get; set; }
        /// <summary>
        /// 邮费
        /// </summary>
        public string PostFee { get; set; }

        /// <summary>
        /// 状态 1 未付款 2 已付款 3 未发货 4 已发货 5 交易成功 6 交易关闭 7交易取消
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 订单创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 订单更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime? PaymentTime { get; set; }
        /// <summary>
        /// 发货时间
        /// </summary>
        public DateTime? ConsignTime { get; set; }
        /// <summary>
        /// 交易完成时间
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 交易关闭时间
        /// </summary>
        public DateTime? CloseTime { get; set; }
        /// <summary>
        /// 物流类型
        /// </summary>
        public string ShippingType { get; set; }

        /// <summary>
        /// 物流名称
        /// </summary>
        public string ShippingName { get; set; }

        /// <summary>
        /// 物流单号
        /// </summary>
        public string ShippingCode { get; set; }

        /// <summary>
        /// 是否已经评价 0 没有 1 买家已评论 2 双方已评论
        /// </summary>
        public int IsRate { get; set; } = 0;

        /// <summary>
        /// 订单来自 0 App 1 Web
        /// </summary>
        public int IsFrom { get; set; } = 0;
    }
}
