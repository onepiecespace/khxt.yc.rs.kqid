﻿using KhXt.YcRs.Domain.Entities.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    /// <summary>
    /// 可用优惠券
    /// </summary>
    public class CanCouponInfo
    {
        /// <summary>
        /// 推荐优惠券组合
        /// </summary>
        public Dictionary<int, List<CouponInfoEntity>> CouponInfos { get; set; }
    }
}
