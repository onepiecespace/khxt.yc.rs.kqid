﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    /// <summary>
    /// 创建订单返回类
    /// </summary>
    public class PayOrderModel
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 业务类别
        /// </summary>
        public BusinessType BusinessType { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public OrderStatus Status { get; set; }
        /// <summary>
        /// 是否自动续费
        /// </summary>
        public bool IsAutoRenew { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PaymentNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PaymentType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// IosProductId
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Transaction { get; set; }
        /// <summary>
        /// 是否测试
        /// </summary>
        public bool IsTest { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        public DateTime? PayTime { get; set; }
        /// <summary>
        /// 是否成功支付
        /// </summary>
        public bool IsSuccessPay { get; set; }
    }
}
