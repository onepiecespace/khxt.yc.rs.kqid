﻿using KhXt.YcRs.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Models.Order
{
    /// <summary>
    /// 订单信息
    /// </summary>
    public class OrderInfoModel
    {
        /// <summary>
        /// 实付金额（金币商城实际支付0元）
        /// </summary>
        [Required]
        public string Payment { get; set; }
        /// <summary>
        /// 支付类型 0 微信支付 1 支付宝 2 苹果支付 3 免费 4 线下 5 银联 6 金币兑换（金币商城专用）
        /// </summary>
        [Required]
        public int PaymentType { get; set; }

        /// <summary>
        /// IosProductId（只有课程用到此字段 默认传 0）
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// 邮费 默认0
        /// </summary>
        [Required]
        public string PostFee { get; set; }

        /// <summary>
        /// 下单时间依据用户端app时间为准 
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 物流名称
        /// </summary>
        public string ShippingName { get; set; }

        /// <summary>
        /// 物流单号
        /// </summary>
        public string ShippingCode { get; set; }
        /// <summary>
        /// 订单来自 0 App 1 Web
        /// </summary>
        public int IsFrom { get; set; }
        /// <summary>
        /// 业务类别 16课程  1024 VIP   8192金币兑换 
        /// </summary>
        public int BusinessType { get; set; }
    }
    /// <summary>
    /// 订单详情
    /// </summary>
    public class OrderItemModel
    {
        /// <summary>
        /// 商品Id （proid）
        /// </summary>
        [Required]
        public string ItemId { get; set; }

        /// <summary>
        /// 商品标题 
        /// </summary>
        [Required]
        public string Title { get; set; }
        /// <summary>
        /// 商品描述（副标题）
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 商品单价(消耗金币数)
        /// </summary>
        [Required]
        public float Price { get; set; }
        /// <summary>
        /// 商品购买（兑换）数量
        /// </summary>
        [Required]
        public long Num { get; set; } = 1;

        /// <summary>
        /// 商品总金额 OR（总消耗金币数）
        /// </summary>
        [Required]
        public float TotalFee { get; set; }

        /// <summary>
        /// 商品封面图片地址
        /// </summary>
        [Required]
        public string PicPath { get; set; }
    }
    /// <summary>
    /// 优惠券信息（金币兑换可为空 无需赋值）
    /// </summary>
    public class OrderCouponInfoModel
    {
        /// <summary>
        /// 商品Id  （proid）
        /// </summary>
        public string ItemId { get; set; }

        /// <summary>
        /// 优惠券Id
        /// </summary>
        public int CouponId { get; set; }

        /// <summary>
        /// 优惠券No
        /// </summary>
        public string CouponNo { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; } = DateTime.Now;
    }
}
