﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    /// <summary>
    /// 订单支付状态码
    /// </summary>
    public enum OrderPayStatusEnum
    {
        /// <summary>
        /// 订单不存在
        /// </summary>
        [Description("订单不存在")]
        No = 10001,
        /// <summary>
        /// 经系统检测，此订单非您的订单
        /// </summary>
        [Description("非法订单")]
        Illegal = 10002,
        /// <summary>
        /// 订单商品信息不存在
        /// </summary>
        [Description("订单商品信息不存在")]
        NoItem = 10003,
        /// <summary>
        /// 更新订单失败
        /// </summary>
        [Description("更新订单失败")]
        UpdateErr = 10004,
        /// <summary>
        /// 以此开头的开通会员业务失败
        /// </summary>
        [Description("开通会员业务失败")]
        OpenVipErr = 10005,
        /// <summary>
        /// 订单通用失败
        /// </summary>
        [Description("订单支付失败")]
        Error = 100400,
        /// <summary>
        /// 订单支付异常
        /// </summary>
        [Description("订单支付异常")]
        Exception = 100500
    }
}
