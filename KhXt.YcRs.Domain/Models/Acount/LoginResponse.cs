﻿namespace KhXt.YcRs.Domain.Models
{
    public class LoginResponse
    {
        public string AccessToken { get; set; }
        public long ExpireIn { get; set; }
        public string RefreshToken { get; set; }
    }

    public class PrivacyResponse
    {
        public string accessToken { get; set; }
        public string idToken { get; set; }
        public string refreshToken { get; set; }

    }
}