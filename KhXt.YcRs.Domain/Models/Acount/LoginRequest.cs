﻿namespace KhXt.YcRs.Domain.Models
{
    public class LoginRequest
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}