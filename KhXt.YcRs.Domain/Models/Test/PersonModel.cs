﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Test
{
    public class PersonModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
