﻿using System;

namespace KhXt.YcRs.Domain
{
    [Serializable]
    public class RequestModelBase
    {
        public string TenantId { get; set; }
        public string AgentId { get; set; }
        public string Platform { get; set; }

        public string Version { get; set; }

        public string AppCode { get; set; }

        public string RegistrationId { get; set; }
    }
}
