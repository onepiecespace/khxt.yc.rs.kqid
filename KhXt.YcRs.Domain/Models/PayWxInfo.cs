﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace KhXt.YcRs.Domain.Models
{
    public class PayWxInfo
    {
        public string mch_id;
        public string prepay_id;
        public string AppId { get; set; }
        public string TimeStamp { get; set; }
        public string Package { get; set; }
        public string PaySign { get; set; }
        public long courseId { get; set; }
        public string device_info { get; set; }
        public string nonce_str { get; set; }
        public string result_code { get; set; }
        public string orderNo { get; set; }
        public string price { get; set; }

        public string sign { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("appid")]
        public string Appid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("noncestr")]
        public string Noncestr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("partnerid")]
        public string Partnerid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("prepayid")]
        public string Prepayid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("signType")]
        public string SignType { get; set; }
        /// <summary>
        /// 二维码链接	
        /// </summary>
        [JsonProperty("code_url")]
        public string CodeUrl { get; set; }

        /// <summary>
        /// 支付跳转链接	
        /// </summary>
        [JsonProperty("mweb_url")]
        public string MwebUrl { get; set; }

    }
}
