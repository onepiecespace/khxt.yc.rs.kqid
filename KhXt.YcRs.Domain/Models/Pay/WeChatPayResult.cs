﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Pay
{
    /// <summary>
    /// /
    /// </summary>
    public class WeChatPayResult
    {
        /// <summary>
        /// 结果返回码
        /// </summary>
        public string ResultCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("appid")]
        public string Appid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("noncestr")]
        public string Noncestr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("package")]
        public string Package { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("partnerid")]
        public string Partnerid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("prepayid")]
        public string Prepayid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("sign")]
        public string Sign { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("paySign")]
        public string PaySign { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("signType")]
        public string SignType { get; set; }
        /// <summary>
        /// 二维码链接	
        /// </summary>
        [JsonProperty("code_url")]
        public string CodeUrl { get; set; }

        /// <summary>
        /// 支付跳转链接	
        /// </summary>
        [JsonProperty("mweb_url")]
        public string MwebUrl { get; set; }

    }
}
