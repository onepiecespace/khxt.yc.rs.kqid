﻿namespace KhXt.YcRs.Domain.Models.Pay
{
    public class NotifyUrlInfo
    {

        /// <summary>
        /// 
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 支付测试
        /// </summary>
        public string attach { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string bank_type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fee_type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string is_subscribe { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string mch_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string nonce_str { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string out_trade_no { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string result_code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string return_code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string sign { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string sub_mch_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string time_end { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string total_fee { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string coupon_fee { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string coupon_count { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string coupon_type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string coupon_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string coupon_fee_0 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string trade_type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string transaction_id { get; set; }

    }


}
