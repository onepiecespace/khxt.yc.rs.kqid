﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Pay
{
    /// <summary>
    /// 订单支付返回
    /// </summary>
    public class PayResult
    {
        /// <summary>
        /// 是否支付成功
        /// </summary>
        public bool IsSuccess { get; set; }
        ///// <summary>
        ///// 微信支付返回
        ///// </summary>
        //public WeChatPayResult WeChatPayResult { get; set; }
        /// <summary>
        /// 旧版本微信支付
        /// </summary>
        public PayWxInfo PayWxInfo { get; set; }


    }
}
