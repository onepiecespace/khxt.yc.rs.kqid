﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Pay
{
    public class AlipayTradeAppPayViewModel
    {
        [Hx.ObjectMapping.Required]
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        [Hx.ObjectMapping.Required]
        [Display(Name = "subject")]
        public string Subject { get; set; }

        [Display(Name = "product_code")]
        public string ProductCode { get; set; }

        [Display(Name = "body")]
        public string Body { get; set; }

        [Hx.ObjectMapping.Required]
        [Display(Name = "total_amount")]
        public string TotalAmount { get; set; }
    }
}
