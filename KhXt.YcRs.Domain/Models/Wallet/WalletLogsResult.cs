﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Wallet
{
    /// <summary>
    /// 
    /// </summary>
    public class WalletLogsResult
    {
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int Total { get; set; }
        public List<WalletLogs> Rows { get; set; }
    }

    public class WalletLogs
    {
        /// <summary>
        /// /
        /// </summary>
        public long LogId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 0 充值 1 扣减
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 金币数量
        /// </summary>
        public string Coin { get; set; }
        /// <summary>
        /// 金币余额
        /// </summary>
        public string CoinBalance { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

    }
}
