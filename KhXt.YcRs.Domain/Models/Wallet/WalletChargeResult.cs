﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Wallet
{
    /// <summary>
    /// 钱包充值返回信息
    /// </summary>
    public class WalletChargeResult
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 金币总数
        /// </summary>
        public float Coin { get; set; }
        /// <summary>
        /// 本次充值金币数
        /// </summary>
        public float ChargeCoin { get; set; }

    }
}
