﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Wallet
{
    public class WalletChargeModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 订单Id
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 本次充值金币数
        /// </summary>
        public float ChargeCoin { get; set; }
        /// <summary>
        /// 充值描述
        /// </summary>
        public string Description { get; set; }
    }
}
