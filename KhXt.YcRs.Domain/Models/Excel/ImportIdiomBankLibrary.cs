﻿

using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Excel
{
    public class ImportIdiomBankLibrary
    {
        [ColName("题目名称")]
        [Regex(RegexConstant.NOT_EMPTY_REGEX, ErrorMsg = "必填")]
        public string Answer { get; set; }

        [ColName("注音")]
        public string Title { get; set; }

        [ColName("解释")]
        public string Analysis { get; set; }

        [ColName("出处")]
        public string Source { get; set; }

        [ColName("近义词")]
        public string Nearword { get; set; }

        [ColName("反义词")]
        public string Antonym { get; set; }

        [ColName("难易度")]
        public string Difficulty { get; set; }

        [ColName("题目分类")]
        public string Classstr { get; set; }

    }


}
