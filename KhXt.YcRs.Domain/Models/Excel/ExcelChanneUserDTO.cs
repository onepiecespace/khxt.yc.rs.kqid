﻿using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Models.Excel
{
    [Header(Color = ColorEnum.DARK_BLUE, FontSize = 10, IsBold = true)] //表头样式
    [WrapText] //自动换行
    public class ExcelChanneUserDTO
    {
        //[ColName("车牌号")]
        //[MergeCols] //相同数据自动合并单元格
        //public string CarCode { get; set; }

        //[MergeCols] //相同数据自动合并单元格
        //[ColName("直播频道Id")]
        //public long ChannelId { get; set; }
        //[ColName("主课程ID")]
        //public long CourseId { get; set; }

        [ColName("课程名称")]
        public string CourseName { get; set; }

        [ColName("课程讲师")]
        public string TeachersUserName { get; set; }

        //[ColName("用户Id")]
        //public long UserId { get; set; }

        [ColName("昵称")]
        public string UserName { get; set; }

        [ColName("手机号")]
        public string UserPhone { get; set; }

        [ColName("性别")]
        public string sex { get; set; }

        [ColName("省份")]
        public string provincial { get; set; }

        [ColName("城市")]
        public string city { get; set; }

        [ColName("时间")]
        [DateTime]
        public DateTime VistTime { get; set; }
    }
}