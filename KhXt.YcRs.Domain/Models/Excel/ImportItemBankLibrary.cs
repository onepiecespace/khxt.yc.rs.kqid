﻿
using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Excel
{
    public class ImportItemBankLibrary
    {
        [ColName("题目名称")]
        [Regex(RegexConstant.NOT_EMPTY_REGEX, ErrorMsg = "必填")]
        public string Title { get; set; }

        [ColName("题目描述")]
        public string Remark { get; set; }

        [ColName("题目答案")]
        public string Answer { get; set; }

        [ColName("答案解析")]
        public string Analysis { get; set; }

        [ColName("题目与选项图片")]
        public string ItemImg { get; set; }

        [ColName("答案解析图片")]
        public string AnalysisItemImg { get; set; }

        [ColName("难易度")]
        public string Difficulty { get; set; }
        [ColName("题目类型")]
        public string ItemTypestr { get; set; }
        [ColName("题目分类")]
        public string Classstr { get; set; }
        [ColName("等级")]
        public string ExtraLevel { get; set; }
    }
    public class OptionsModel
    {
        public int index { get; set; }
        public string name { get; set; }
    }
    public enum ClassEnum
    {
        小学一年级 = 1,
        小学二年级 = 2,
        小学三年级 = 3,
        小学四年级 = 4,
        小学五年级 = 5,
        小学六年级 = 6
    }

}
