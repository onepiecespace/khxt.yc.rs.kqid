﻿
using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Excel
{
    public class ImportCharLibrary
    {
        [ColName("年级")]
        [Regex(RegexConstant.NOT_EMPTY_REGEX, ErrorMsg = "必填")]
        public string NianCode { get; set; }

        [ColName("单元名")]
        public string UnitName { get; set; }

        [ColName("课")]
        [Range(0, 150)]
        public int KeNumber { get; set; }

        [ColName("课程名")]
        public string KeChengName { get; set; }

        [ColName("写生字")]
        public string WriteName { get; set; }

        [ColName("写生字拼音")]
        public string WritePinYinName { get; set; }

        [ColName("认生字")]
        public string ReadeName { get; set; }
        [ColName("认生字拼音")]
        public string ReadePinYinName { get; set; }

    }


}
