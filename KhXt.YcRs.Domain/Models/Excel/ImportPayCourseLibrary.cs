﻿using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Excel
{
    public class ImportPayCourseLibrary
    {
        [ColName("课程名称")]
        public string CourseName { get; set; }

        [ColName("手机号")]
        public string Phone { get; set; }

        [ColName("支付方式")]
        public int PaymentType { get; set; }

        [ColName("交易号")]
        public string TransactionNo { get; set; }

        [ColName("支付时间")]
        public string PaymentTime { get; set; }


    }
}
