﻿using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Excel
{
    public class ImportCourseChildLibrary
    {


        [ColName("子课程名称")]
        public string ChildName { get; set; }

        [ColName("子课程老师")]
        public string ChildTeacher { get; set; }

        [ColName("子课程排序")]
        public int ChildSort { get; set; }

        [ColName("子课程日期")]
        public string ChildDate { get; set; }

        [ColName("子课程时间")]
        public string ChildTime { get; set; }

        [ColName("回放视频标识")]
        public string videoId { get; set; }
        [ColName("子课程描述")]
        public string ChildDes { get; set; }


    }
}
