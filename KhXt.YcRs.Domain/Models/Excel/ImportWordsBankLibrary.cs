﻿

using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Excel
{
    public class ImportWordsBankLibrary
    {
        [ColName("一列")]
        public string 一 { get; set; }

        [ColName("二列")]
        public string 二 { get; set; }

        [ColName("三列")]
        public string 三 { get; set; }

        [ColName("四列")]
        public string 四 { get; set; }

        [ColName("五列")]
        public string 五 { get; set; }

        [ColName("六列")]
        public string 六 { get; set; }

        [ColName("七列")]
        public string 七 { get; set; }

        [ColName("八列")]
        public string 八 { get; set; }

        [ColName("九列")]
        public string 九 { get; set; }

        [ColName("十列")]
        public string 十 { get; set; }
    }


}
