﻿using KhXt.YcRs.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Models.Excel
{
    [Header(Color = ColorEnum.DARK_BLUE, FontSize = 10, IsBold = true)] //表头样式
    [WrapText] //自动换行
    public class ExcelChildUserDTO
    {
        [ColName("直播频道号")]
        public string ChannelId { get; set; }

        [ColName("主课程名称")]
        public string CourseName { get; set; }

        [ColName("主课程讲师")]
        public string TeachersUserName { get; set; }
        /// <summary>
        /// 子课程名称
        /// </summary>
        [ColName("子课程名称")]
        public string ChildName { get; set; }
        /// <summary>
        /// 子课程讲师
        /// </summary>
        [ColName("子课程讲师")]
        public string ChildTeacher { get; set; }
        /// <summary>
        /// 子课程直播日期
        /// </summary>
        [ColName("子课程直播日期")]
        public string ChildTimeLength { get; set; }


        [ColName("昵称")]
        public string UserName { get; set; }

        [ColName("手机号")]
        public string UserPhone { get; set; }

        [ColName("性别")]
        public string sex { get; set; }

        [ColName("省份")]
        public string provincial { get; set; }

        [ColName("城市")]
        public string city { get; set; }

        [ColName("访问时间")]

        public DateTime VistTime { get; set; }


    }
}