﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Security
{
    /// <summary>
    /// 字典配置项
    /// </summary>
    public class BootstrapDict
    {
        /// <summary>
        /// 获得/设置 字典主键 数据库自增列
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 获得/设置 字典分类
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// 获得/设置 字典名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 获得/设置 字典字典值
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 获得/设置 字典定义值 0 表示系统使用，1 表示用户自定义 默认为 1
        /// </summary>
        public int Define { get; set; } = 1;
    }
    /// <summary>
    /// BA 用户实例
    /// </summary>
    public class BootstrapUser
    {
        /// <summary>
        /// 获得/设置 系统登录用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 获得/设置 用户显示名称
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 获得/设置 用户头像图标路径
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 获得/设置 用户设置样式表名称
        /// </summary>
        public string Css { get; set; }

        /// <summary>
        /// 获得/设置 用户默认登陆 App 标识
        /// </summary>
        public string App { get; set; }

        /// <summary>
        /// 获得/设置 默认格式为 UserName (DisplayName)
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} ({1})", UserName, DisplayName);
        }
    }

    /// <summary>
    /// Bootstrap Admin 后台管理菜单相关操作实体类
    /// </summary>
    public class BootstrapMenu
    {
        /// <summary>
        /// 获得/设置 菜单主键ID
        /// </summary>
        public string Id { set; get; }

        /// <summary>
        /// 获得/设置 父级菜单ID 默认为0
        /// </summary>
        public string ParentId { set; get; }

        /// <summary>
        /// 获得/设置 父级菜单名称
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        /// 获得/设置 菜单名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 获得/设置 菜单序号
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// 获得/设置 菜单图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 获得/设置 菜单URL地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 获得/设置 菜单分类, 0 表示系统菜单 1 表示用户自定义菜单
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// 获得 菜单分类名称，取字典表中的Name category="菜单"
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 获得/设置 是否当前被选中 active为选中
        /// </summary>
        public string Active { get; set; }

        /// <summary>
        /// 获得/设置 链接目标
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        /// 获得/设置 是否为资源文件, 0 表示菜单 1 表示资源 2 表示按钮
        /// </summary>
        public int IsResource { get; set; }

        /// <summary>
        /// 获得/设置 所属应用程序，此属性由BA后台字典表分配
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// 获得/设置 当前菜单项的所有子菜单集合
        /// </summary>
        public IEnumerable<BootstrapMenu> Menus { get; set; }
    }
    /// <summary>
    /// Bootstrap Admin 后台管理部门相关操作实体类
    /// </summary>
    public class BootstrapGroup
    {
        /// <summary>
        /// 获得/设置 部门主键ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 获得/设置 部门编码
        /// </summary>
        public string GroupCode { get; set; }

        /// <summary>
        /// 获得/设置 部门名称
        /// </summary>
        public string GroupName { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class AppMenuOption
    {
        /// <summary>
        /// 所属用户登录名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        ///  所属应用程序 ID
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        ///  获得/设置 菜单地址 Url
        /// </summary>
        public string Url { get; set; }
    }
}
