﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain
{
    public class KeyValueStore
    {
        public long Id { get; set; }
        /// <summary>
        /// 租户ID,请在   HuangLiCollege.Common.Config.TenantTypes统一配置，方便管理
        /// </summary>
        public int TenantId { get; set; }
        /// <summary>
        /// Key
        /// </summary>
        public string KeyData { get; set; }
        /// <summary>
        /// value
        /// </summary>
        public string ValueData { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
