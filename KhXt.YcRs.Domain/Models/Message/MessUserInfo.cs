﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Message
{
    /// <summary>
    /// 消息返回类
    /// </summary>
    public class MessUserInfo
    {

        /// <summary>
        /// 直播频道Id
        /// </summary>
        public long ChannelId { get; set; }
        /// <summary>
        /// 主课程ID
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 主课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 主课程讲师
        /// </summary>
        public string TeachersUserName { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 用户手机号
        /// </summary>
        public string UserPhone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string sex { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        public string provincial { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string city { get; set; }


        public DateTime VistTime { get; set; }


    }
}
