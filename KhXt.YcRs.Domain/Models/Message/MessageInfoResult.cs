﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Message
{
    /// <summary>
    /// 消息返回类
    /// </summary>
    public class MessageInfoResult
    {
        /// <summary>
        /// 活动Id
        /// </summary>
        public string MarketingId { get; set; }

        /// <summary>
        /// 活动标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 活动内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 活动链接
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 模板Id 1 领取 2 金币
        /// </summary>
        public string TemplateId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
    }
}
