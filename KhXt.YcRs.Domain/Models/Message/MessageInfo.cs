﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Message
{
    /// <summary>
    /// 消息返回类
    /// </summary>
    public class MessageInfo
    {

        /// <summary>
        /// 直播频道Id
        /// </summary>
        public string ChannelId { get; set; }
        /// <summary>
        /// 子课程类型1直播中2录播（回放）3点播 4 未开始 5 转存中
        /// </summary>
        public int ChildType { get; set; }


        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 用户手机号
        /// </summary>
        public string UserPhone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string sex { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        public string provincial { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// 主课程ID
        /// </summary>
        public int CourseId { get; set; }
        /// <summary>
        /// 主课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 主课程讲师
        /// </summary>
        public string TeachersUserName { get; set; }
        /// <summary>
        /// 子课程ID
        /// </summary>
        public long ChildId { get; set; }

        /// <summary>
        /// 子课程名称
        /// </summary>
        public string ChildName { get; set; }
        /// <summary>
        /// 子课程讲师
        /// </summary>
        public string ChildTeacher { get; set; }
        /// <summary>
        /// 子课程直播日期
        /// </summary>
        public string ChildTimeLength { get; set; }
        /// <summary>
        /// 进入直播时间
        /// </summary>
        public DateTime VistTime { get; set; }

        /// <summary>
        /// 用户IP
        /// </summary>
        public string UserIp { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 录入时间
        /// </summary>
        public DateTime Datatime { get; set; }

    }
}
