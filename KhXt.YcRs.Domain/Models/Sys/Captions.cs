﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Models.Sys
{
    public class CaptionModel
    {
        //public long Id { get; set; }
        public int Ranking { get; set; }

        public string Caption { get; set; }
        public int Category { get; set; }
        public string Image { get; set; }

        //public CaptionType CaptionType { get; set; }
    }

    public class LevelCaptionModel : CaptionModel
    {
        public int Threshold { get; set; }
    }

}
