﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Sys
{
    /// <summary>
    /// 
    /// </summary>
    public class PayChars
    {
        /// <summary>
        /// 
        /// </summary>
        public List<PayDayChars> PayDayChars { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<PayWeekChars> PayWeekChars { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<PayYearChars> PayYearChars { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double TotalCount { get; set; }
        /// <summary>
        /// 总注册人数
        /// </summary>
        public double AllTotalCount { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PayDayChars
    {
        /// <summary>
        /// 
        /// </summary>
        public int Hour { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double Price { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PayWeekChars
    {
        /// <summary>
        /// 
        /// </summary>
        public string Time { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double Price { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PayYearChars
    {
        /// <summary>
        /// 
        /// </summary>
        public string Time { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Month { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double Price { get; set; }
    }
}
