﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Sys
{
    /// <summary>
    /// 
    /// </summary>
    public class UserHourChars
    {
        /// <summary>
        /// 
        /// </summary>
        public List<UserHourChar> Chars { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 总注册人数
        /// </summary>
        public int AllTotalCount { get; set; }
    }

    /// <summary>
    /// 用户当天各个小时注册统计
    /// </summary>
    public class UserHourChar
    {
        /// <summary>
        /// 小时
        /// </summary>
        public int Hour { get; set; }
        /// <summary>
        /// 当前小时注册数
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 安卓 IOS
        /// </summary>
        public int IsFrom { get; set; }

    }
    /// <summary>
    /// 本周用户注册数
    /// </summary>
    public class UserWeekChars
    {
        /// <summary>
        /// 
        /// </summary>
        public List<UserWeekChar> Chars { get; set; }
        /// <summary>
        /// 当天总数
        /// </summary>
        public int TotalCount { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UserWeekChar
    {

        /// <summary>
        /// 天
        /// </summary>
        public string Day { get; set; }
        /// <summary>
        /// 当前小时注册数
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 安卓 IOS
        /// </summary>
        public int IsFrom { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UserYearChars
    {
        /// <summary>
        /// 
        /// </summary>
        public List<UserYearChar> Chars { get; set; }
        /// <summary>
        /// 当天总数
        /// </summary>
        public int TotalCount { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UserYearChar
    {
        /// <summary>
        /// 
        /// </summary>
        public string Time { get; set; }
        /// <summary>
        /// 天
        /// </summary>
        public int Mouth { get; set; }
        /// <summary>
        /// 当前小时注册数
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 安卓 IOS
        /// </summary>
        public int IsFrom { get; set; }
    }
}
