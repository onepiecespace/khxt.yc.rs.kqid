﻿using System;
using System.ComponentModel;

namespace KhXt.YcRs.Domain.Models.Sys
{
    [Obsolete("移到Domain.Enums")]
    public enum ContentType
    {
        OuterHtml = 1,
        Course = 2,
        Pk = 3,
        Game = 4,
        Dication = 5,
        Task = 6,
        Ad = 7,
        Read = 8,
        Vip = 9,
        Teacher = 10,  //讲师
        web = 11,  //pc站
        newAd = 107
    }

    [Obsolete("移到Domain.Enums")]
    public enum TagType
    {

        [Description("home")]
        home = 1,

        [Description("read")]
        read = 2,

        [Description("course_top")]
        course_top = 4,

        [Description("course")]
        course = 8,

        [Description("course_bottom")]
        course_bottom = 16,

        [Description("pccourse")]
        pccourse = 32,

        [Description("gold")]
        gold = 64,

        [Description("jxj")]
        jxj = 128,

        [Description("activity")]
        activity = 256
    }
    public enum SubjectEnum
    {
        语文 = 1,
        数学 = 2,
        英语 = 3,
        物理 = 4,
        化学 = 5,
        地理 = 6,
        生物 = 7,
        历史 = 8,
        政治 = 9,
    }
}
