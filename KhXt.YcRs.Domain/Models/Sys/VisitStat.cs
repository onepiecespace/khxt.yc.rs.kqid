﻿using Newtonsoft.Json;

namespace KhXt.YcRs.Domain.Models.Sys
{
    public class VisitStat
    {
        public string Path { get; set; }

        [JsonProperty("vs")]
        public ulong Visits { get; set; }

        [JsonProperty("tl")]
        public ulong TotalLength { get; set; }

        [JsonProperty("al")]
        public uint AverageLength
        {
            get
            {
                if (Visits == 0) return 0;
                var v = TotalLength / Visits;
                return (uint)v;
            }
        }
    }
}
