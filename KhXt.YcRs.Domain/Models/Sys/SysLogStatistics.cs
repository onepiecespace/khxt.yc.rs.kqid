﻿using KhXt.YcRs.Domain.Entities.Sys;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Sys
{
    /// <summary>
    /// 系统日志访问
    /// </summary>
    public class SysLogStatistics
    {
        /// <summary>
        /// Api点击数
        /// </summary>
        public List<ApiCount> ApiHourChar { get; set; }
        /// <summary>
        /// 日接口点击数
        /// </summary>
        public List<HourCount> CountsHourChar { get; set; }
        /// <summary>
        /// 今日访问量
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 总访问量
        /// </summary>
        public int AllTotalCount { get; set; }
    }
    /// <summary>
    /// Api 模块访问数
    /// </summary>
    public class ApiCount
    {
        /// <summary>
        /// 小时
        /// </summary>
        public int Hour { get; set; }
        /// <summary>
        /// 创建时间 yyyy-MM-dd
        /// </summary>
        public string Time { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 模块
        /// </summary>
        public int BusinessType { get; set; }
        /// <summary>
        /// 访问数量
        /// </summary>
        public long Count { get; set; } = 0;
    }
    /// <summary>
    /// 
    /// </summary>
    public class HourCount
    {
        /// <summary>
        /// 小时
        /// </summary>
        public int Hour { get; set; }
        /// <summary>
        /// 月份
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// 创建时间 yyyy-MM-dd
        /// </summary>
        public string Time { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 访问数量
        /// </summary>
        public long Count { get; set; } = 0;
    }
}
