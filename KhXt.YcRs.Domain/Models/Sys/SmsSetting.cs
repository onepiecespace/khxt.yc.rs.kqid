﻿namespace KhXt.YcRs.Domain.Models.Sys
{
    public class SmsSetting
    {
        /*
        string encode = "UTF-8";
        string username = "lghljy";  //用户名
        string password_md5 = MD5Key.MD5Encrypt32("asdf1234");  ////32位MD5密码加密，不区分大小写
        string mobile = phone;  //手机号,只发一个号码：13800000001。发多个号码：13800000001,13800000002,...N 。使用半角逗号分隔。
        string apikey = "c3f6b184942b565e16071d163e848fc7";  //apikey秘钥
        string num = RandomHelper.GenerateRandomCode(6);
        */

        public string Url { get; set; }

        public string Encoding { get; set; } = "UTF-8";
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ApiKey { get; set; }


        public int SpeedThreshold { get; set; } = 20;
        public int SpeedDuring { get; set; } = 10;
        //lghljy,asdf1234,c3f6b184942b565e16071d163e848fc7
    }
}
