﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Models.Sys
{
    public class ContentModel
    {
        public long Id { get; set; }
        public string Title { get; set; }

        public string Content { get; set; }
        public string Image { get; set; }
        public string SmallImage { get; set; }
        public string Slide { get; set; }
        public string Summary { get; set; }
        public string Url { get; set; }
        public int Category { get; set; }

        public string Tag { get; set; }

        public int Action { get; set; }
    }
}
