﻿using System;

namespace KhXt.YcRs.Domain.Models.Sys
{
    public class AdModel
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 类别：1.html外跳，2.课程，3.pk,4.闯关，5.听写，6.任务 7.广告 8.朗读 9.Vip 10.Teacher 11.web11 107.newAd(所有新旧广告位的存放)
        /// </summary>
        public int Category { get; set; }

        /// <summary>
        /// 可以标记广告的类型、位置等信息（广告标记,可以标记位置，home = 1,read = 2, course_top = 4,course = 8,course_bottom = 16,pccourse = 32,gold = 64,jxj=128）
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// 广告标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 简短描述，也可作为广告副标题
        /// </summary>
        public string Summary { get; set; }
        /// <summary>
        /// 广告图片
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        ///  Action =1 跳转H5  Action =2 根据Content 的值 跳转课程
        /// </summary>
        public int Action { get; set; }
        /// <summary>
        /// 广告内容 （存放H5或要跳转的内容标识主键）
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 课程是否测评0否1是
        /// </summary>
        public int IsRecord { get; set; }
        /// <summary>
        /// 幻灯内容，可能广告不止一个图片或文件，此处可存H5的幻灯，或其他自定义格式
        /// </summary>
        public string Slide { get; set; }
        /// <summary>
        /// 广告缩略图（非必须）
        /// </summary>
        public string SmallImage { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// 关键词
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// 暂未用到
        /// </summary>
        public int ContentType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
    }
}
