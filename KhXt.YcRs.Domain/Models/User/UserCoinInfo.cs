﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.User
{
    public class UserCoinInfo
    {
        public long UserId { get; set; }

        public long Total { get; set; }

        public Paged<ValueLogModel> ChangeFlow { get; set; }
    }

    public class UserCoinResult : Result<UserCoinInfo>
    {
        public UserCoinResult() { }
        public UserCoinResult(ResultCode resultCode, string message = "") : base(resultCode, message)
        {

        }
    }
}
