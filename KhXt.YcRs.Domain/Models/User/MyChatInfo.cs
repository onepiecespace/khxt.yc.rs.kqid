﻿namespace KhXt.YcRs.Domain.Models
{
    public class MyChatInfo
    {
        public string Content { get; set; }
        public string DateTime { get; set; }
        public string HeadUrl { get; set; }
        public string ToUseId { get; set; }
        public string FromUserId { get; set; }

    }
}