﻿namespace KhXt.YcRs.Domain.Models
{
    public class GoldInfo
    {
        public int GoldSum { get; set; }

        public string GoldContent { get; set; }
        public string GoldTime { get; set; }
        public int GoldCoin { get; set; }
        public int GoldType { get; set; }
    }
}
