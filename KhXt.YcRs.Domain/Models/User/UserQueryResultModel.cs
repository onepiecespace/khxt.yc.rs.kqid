﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    public class UserQueryResultPageModel
    {
        public List<UserQueryResultModel> Rows { get; set; }
        public int TotalItemCount { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UserQueryResultModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int sex { get; set; }
        /// <summary>
        /// 账户总金额
        /// </summary>
        public decimal moneyTotal { get; set; }
        /// <summary>
        /// 金币
        /// </summary>
        public int gold { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        public int level { get; set; }
        /// <summary>
        /// 经验值
        /// </summary>
        public int experience { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime creatime { get; set; }

        /// <summary>
        /// 账户类型
        /// </summary>
        public int accountType { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int state { get; set; }
        /// <summary>
        /// 会员卡
        /// </summary>
        public int isMember { get; set; }
        /// <summary>
        /// 会员开通日期
        /// </summary>
        public DateTime memberDueDate { get; set; }
        /// <summary>
        /// UnionId
        /// </summary>
        public string unionId { get; set; }

        /// <summary>
        /// 登录方式
        /// </summary>
        public int loginType { get; set; }
        /// <summary>
        /// 课程类别
        /// </summary>
        public string authCateorys { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime useDueDate { get; set; }

    }
}
