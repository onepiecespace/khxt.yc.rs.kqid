﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.User
{
    public class MyExpInfo
    {
        /// <summary>
        /// 经验规则 说明
        /// </summary>
        public string ExpRule { get; set; }

        /// <summary>
        /// 总经验
        /// </summary>
        public long TotalExp { get; set; }
        /// <summary>
        /// 总条数
        /// </summary>
        public long TotalCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ValueLogModel> ExpList { get; set; }
    }
}
