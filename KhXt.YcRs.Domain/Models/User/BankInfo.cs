﻿namespace KhXt.YcRs.Domain.Models
{
    public class BankInfo
    {
        public string UserName { get; set; }
        public string BankName { get; set; }

        public string BankCode { get; set; }


        public string BankPhone { get; set; }


        public long UserId { get; set; }


        public string BankAbbrName { get; set; }


    }
}