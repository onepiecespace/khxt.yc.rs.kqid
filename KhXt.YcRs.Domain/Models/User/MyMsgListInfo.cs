﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    public class MyMsgListInfo
    {

        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int totalCount { get; set; }
        public PageList<MyMsgInfo> MyMsgList { get; set; }
    }
}