﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    public class GoldListInfo
    {
        public string GoldRule { get; set; }
        public int GoldSum { get; set; }
        public PageList<GoldInfo> GoldList { get; set; }



    }


}
