﻿namespace KhXt.YcRs.Domain.Models
{
    #region 邀请注册 
    /// <summary>
    /// 
    /// </summary>
    public class InviteInfo
    {
        /// <summary>
        /// 邀请最高钱数
        /// </summary>
        public decimal InviteMaxMoney { get; set; }
        /// <summary>
        ///课程学费
        /// </summary>
        public decimal PayCourseMoney { get; set; }
        /// <summary>
        /// 提现金额数
        /// </summary>
        public decimal CashbackMoney { get; set; }
        /// <summary>
        /// 邀请注册人数
        /// </summary>
        public int InviteRegisterCount { get; set; }
        /// <summary>
        /// 参与
        /// </summary>
        public string ParticipationRule { get; set; }
        /// <summary>
        /// 奖励
        /// </summary>
        public string RewardRule { get; set; }
        /// <summary>
        /// 问答
        /// </summary>
        public string AskRule { get; set; }
        /// <summary>
        /// 课程奖励
        /// </summary>
        public string CourseRule { get; set; }

    }


    #endregion
}
