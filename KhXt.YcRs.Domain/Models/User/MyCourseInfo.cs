﻿namespace KhXt.YcRs.Domain.Models
{
    public class MyCourseCollectInfo
    {
        /// <summary>
        /// 课程图片
        /// </summary>
        public string Logo { get; set; }
        /// <summary>
        /// 是否直播
        /// </summary>
        public int IsLive { get; set; }
        /// <summary>
        /// 课程标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DateTime { get; set; }
        public string LiveStartTime { get; set; }
        public string LiveEndTime { get; set; }
        public string CourseStartTime { get; set; }
        public string CourseEndTime { get; set; }
    }
}