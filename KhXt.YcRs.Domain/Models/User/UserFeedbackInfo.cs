﻿using System;

namespace KhXt.YcRs.Domain.Models
{
    public class UserFeedbackInfo
    {
        public long UserId { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Phone { get; set; }
        public string Content { get; set; }
        public string PicPath { get; set; }
        /// <summary>
        /// 租户列表 默认0
        /// </summary>
        public long TenantId { get; set; }
    }
}
