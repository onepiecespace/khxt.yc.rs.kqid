﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.User
{
    public class UserInfoOfMsg : Result
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string Phone { get; set; }

        public string CourseName { get; set; }
    }
}
