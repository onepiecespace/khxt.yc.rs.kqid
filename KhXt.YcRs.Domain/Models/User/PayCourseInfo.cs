﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    #region 累计获得学费
    /// <summary>
    /// 
    /// </summary>
    public class PayCourseInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal PayCourseMoney { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PayCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<PayCourseLog> PayCourseLogList { get; set; }
    }


    #endregion
}
