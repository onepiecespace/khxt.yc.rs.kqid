﻿using Hx.Domain.Entities;
using System;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models.User
{
    public class UserOrganizationInfo
    {
        ///<Summary>
        /// 申请id
        ///</Summary>
        public string Id { get; set; }
        /// <summary>
        /// 校区机构logo
        /// </summary>
        public string logo { get; set; }
        /// <summary>
        /// 校区机构名称
        /// </summary>
        public string organization { get; set; }

        /// <summary>
        /// tob账户id
        /// </summary>
        public int applyuserid { get; set; }
        /// <summary>
        /// tob真实名称
        /// </summary>
        public string applynickname { get; set; }
        /// <summary>
        /// tob账户手机号
        /// </summary>
        public string contactphone { get; set; }
        /// <summary>
        /// 客户端选择默认huangli 可选择dufu
        /// </summary>
        public string aid { get; set; }

        /// <summary>
        /// 代理课程大类 ，逗号分隔
        /// </summary>
        public string agentcurriculum { get; set; }
        ///<Summary>
        /// 省
        ///</Summary>
        public string provincial { get; set; }
        ///<Summary>
        /// 市
        ///</Summary>
        public string city { get; set; }
        ///<Summary>
        /// 区
        ///</Summary>
        public string areas { get; set; }
        ///<Summary>
        /// 经度
        ///</Summary>
        public float longitude { get; set; }
        ///<Summary>
        /// 纬度
        ///</Summary>
        public float latitude { get; set; }
        ///<Summary>
        /// 详细地址
        ///</Summary>
        public string address { get; set; }
        ///<Summary>
        /// 校区介绍
        ///</Summary>
        public string orgintroduction { get; set; }

        ///<Summary>
        /// 校区图片展示[{"name":"","picurl":"","introduction":""},{},{}]
        ///</Summary>
        public List<Orgexhibition> orgexhibition { get; set; }
        /// <summary>
        /// 师资介绍json 存储JSON串存在[{"name":"","picurl":"","introduction":""},{},{}]
        /// </summary>
        public List<Orgexhibition> teacherintroduction { get; set; }
        /// <summary>
        /// 课程内容存储JSON串存在[{"picurl":"","info":"","price":0,"discountPrice":0},{}]
        /// </summary>
        public List<Coursecontent> coursecontent { get; set; }
        /// <summary>
        ///  状态0 待审核 1审核通过 2 审核未通过
        /// </summary>
        public int status { get; set; }
        /// <summary>
        /// 未通过原因
        /// </summary>
        public string reasonfailure { get; set; }
    }


}
