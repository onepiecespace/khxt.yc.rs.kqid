﻿namespace KhXt.YcRs.Domain.Models
{
    public class InviteRegisterLog
    {
        /// <summary>
        /// 邀请注册的用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
    }



}
