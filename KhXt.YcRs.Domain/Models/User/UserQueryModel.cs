﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.Order
{
    public class UserQueryModel
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int AccountType { get; set; }
        public int IsMember { get; set; } = -1;
        public string UserName { get; set; }
        public string Phone { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 15;
    }
}
