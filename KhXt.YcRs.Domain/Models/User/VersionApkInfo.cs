﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    /// <summary>
    /// 客户端的字段
    /// </summary>
    public class VersionApkInfo
    {
        /// <summary>
        /// apk下载url
        /// </summary>
        public string apkUrl { get; set; }
        /// <summary>
        /// 是否有更新
        /// </summary>
        public bool isUpdate { get; set; }
        /// <summary>
        ///是否强制更新
        /// </summary>
        public bool forceUpdate { get; set; }

        /// <summary>
        /// 更新文案
        /// </summary>
        public string updateDescription { get; set; }
        /// <summary>
        /// apk文件大小
        /// </summary>
        public long apkSize { get; set; }
        /// <summary>
        /// apk的文件MD5值
        /// </summary>
        public string md5 { get; set; }


    }


}
