﻿namespace KhXt.YcRs.Domain.Models
{
    /// <summary>
    /// 返回用户基本信息
    /// </summary>
    public class AUserBaseInfoOutput
    {
        public long UserId { get; set; }
        public string Username { get; set; }
        public int Level { get; set; }
        public int Grade { get; set; }
        //public int State { get; set; }
        //public int? Score { get; set; }
        public int Gold { get; set; }
        public int Experience { get; set; }
        public string Headurl { get; set; }
        ////用户头衔
        //public string UserTitle { get; set; }

        ////用户头衔图片url
        //public string UserTitleIcon { get; set; } = "";
        //public string WinningPercent { get; set; }


        /////胜利场次 todo
        //public int? WinCount { get; set; } = 0;
        /////失败场次


        //public int? FailCount { get; set; } = 0;


        /////PK连胜利,失败场次 todo
        //public int? PkWinCount { get; set; } = 0;


        /////PK连胜利,是否连胜todo
        //public int IsPkStreaking { get; set; } = 0;


    }
}