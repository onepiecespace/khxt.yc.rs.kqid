﻿namespace KhXt.YcRs.Domain.Models
{
    public class LoginWith2faBindModel
    {
        public string UnionId { get; set; }

        public string Phone { get; set; }

        public string ValidCode { get; set; }
    }
}
