﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    public class MyChatListInfo
    {
        public List<MyChatInfo> MyChatList { get; set; }
    }
}