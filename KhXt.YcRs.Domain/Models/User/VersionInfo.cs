﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class VersionInfo
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 最新版本编号（安卓格式1.2.0.6 or ios格式 21）
        /// </summary> 
        public string NewVersionCode { get; set; }
        /// <summary>
        /// 最新版本号
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 最新版本数字
        /// </summary>
        public int NewVersion { get; set; }
        /// <summary>
        /// apk下载地址
        /// </summary>
        public string NewVersionDownLoadUrl { get; set; }
        /// <summary>
        /// 代理商标识
        /// </summary>
        public string Aid { get; set; }
        /// <summary>
        /// 最小支持版本
        /// </summary>
        public int MinVersion { get; set; }
        /// <summary>
        /// 是否有更新
        /// </summary>
        public int IsUpdate { get; set; }
        /// <summary>
        /// 是否强制更新
        /// </summary>
        public int ForceUpdate { get; set; }
        /// <summary>
        /// 更新的版本说明
        /// </summary>
        public string VersionLogListToStr { get; set; }

        /// <summary>
        /// 版本历史更新最新一条日志
        /// </summary>
        public List<VersionLog> VersionLogList { get; set; }

    }


}
