﻿namespace KhXt.YcRs.Domain.Models.User
{
    public class KeyValue
    {
        public string KeyData { get; set; }

        public string ValueData { get; set; }

    }
    public class CollegeKeyValue
    {
        public int Key { get; set; }

        public string Value { get; set; }

    }

    public class ErrKeyValue
    {

        /// <summary>
        /// 当前选择 
        /// </summary>
        public string KeyData { get; set; }
        /// <summary>
        /// 选项内容
        /// </summary>

        public string ValueData { get; set; }
        /// <summary>
        /// 当前选项是否是正确
        /// </summary>
        public bool IsTrue { get; set; }
        /// <summary>
        /// 你的选项
        /// </summary>
        public bool SelectWrong { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool MyLastSelect { get; set; }
    }

}