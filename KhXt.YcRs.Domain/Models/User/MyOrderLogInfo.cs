﻿namespace KhXt.YcRs.Domain.Models
{
    public class MyOrderLogInfo
    {
        public string Time { get; set; }
        public string Date { get; set; }
        public string DateTime { get; set; }
        public string LogisticsId { get; set; }
        public string OrderId { get; set; }
        public string Content { get; set; }
    }
}