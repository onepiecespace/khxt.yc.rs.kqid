﻿namespace KhXt.YcRs.Domain.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class AddressInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 收货人
        /// </summary>
        public string Consignee { get; set; }
        ///<Summary>
        /// 性别1.先生  0.女士
        ///</Summary>
        public int Sex { get; set; }
        ///<Summary>
        /// 手机号码
        ///</Summary>
        public string Phone { get; set; }
        ///<Summary>
        /// 省
        ///</Summary>
        public string Provincial { get; set; }
        ///<Summary>
        /// 市
        ///</Summary>
        public string City { get; set; }
        ///<Summary>
        /// 区
        ///</Summary>
        public string Areas { get; set; }
        ///<Summary>
        /// 地址
        ///</Summary>
        public string Address { get; set; }
        ///<Summary>
        /// 是否常用地址 0不是，1是
        ///</Summary>
        public int Iscommonly { get; set; }
        ///<Summary>
        /// 用户ID
        ///</Summary>
        public int Userid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Createtime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Updatetime { get; set; }
    }


}
