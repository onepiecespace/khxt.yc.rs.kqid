﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class AUserInput
    {
        public int LoginType { get; set; }
        ///<Summary>
        /// 用户id
        ///</Summary>
        public int Userid { get; set; }
        ///<Summary>
        /// 用户名
        ///</Summary>
        public string Username { get; set; }
        ///<Summary>
        /// 密码
        ///</Summary>
        public string Password { get; set; }
        ///<Summary>
        /// 性别
        ///</Summary>
        public int Sex { get; set; }
        ///<Summary>
        /// 生日
        ///</Summary>
        public string Birthday { get; set; }
        ///<Summary>
        /// 省
        ///</Summary>
        public string Provincial { get; set; }
        ///<Summary>
        /// 市
        ///</Summary>
        public string City { get; set; }
        ///<Summary>
        /// 区
        ///</Summary>
        public string Areas { get; set; }
        ///<Summary>
        /// 详细地址
        ///</Summary>
        public string Address { get; set; }
        ///<Summary>
        /// 年级
        ///</Summary>
        public int Grade { get; set; }
        ///<Summary>
        /// 头像
        ///</Summary>
        public string Headurl { get; set; }
        ///<Summary>
        /// 等级
        ///</Summary>
        public int Level { get; set; }
        ///<Summary>
        /// 手机号
        ///</Summary>
        public string Phone { get; set; }
        ///<Summary>
        /// 微信
        ///</Summary>
        public string Wx { get; set; }
        ///<Summary>
        /// QQ
        ///</Summary>
        public string Qq { get; set; }
        ///<Summary>
        /// 创建时间
        ///</Summary>
        public string Creatime { get; set; }
        ///<Summary>
        /// 状态 1.可用  2.禁用
        ///</Summary>
        public int State { get; set; }
        ///<Summary>
        /// 邀请人ID
        ///</Summary>
        public int Inviterid { get; set; }
        ///<Summary>
        /// 积分
        ///</Summary>
        public int Score { get; set; }
        ///<Summary>
        /// 金币
        ///</Summary>
        public int Gold { get; set; }
        ///<Summary>
        /// 经验
        ///</Summary>
        public int Experience { get; set; }
        ///<Summary>
        /// TenantId 
        ///</Summary>
        public int Tenantid { get; set; }
        ///<Summary>
        /// 是否删除 0 删除
        ///</Summary>
        public int Isdelete { get; set; }
        ///<Summary>
        /// 注册方式 1 app  2 后台
        ///</Summary>
        public int Logintype { get; set; }


        //分页
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        //新手机号
        public string Mobile { get; set; }
    }


    public class AUserEditInput
    {
        /// <summary>
        /// 用户名称
        /// </summary>
        public string Username { get; set; } = "";
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int Sex { get; set; } = 2;
        /// <summary>
        /// 生日
        /// </summary>
        public string Birthday { get; set; } = "";

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; } = "";

        /// <summary>
        /// 年纪
        /// </summary>
        public int Grade { get; set; } = 0;
    }

    public class AUserTobInput
    {
        /// <summary>
        /// TOB userID
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; } = "";
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int Sex { get; set; } = 0;

        /// <summary>
        /// 是否禁用 true冻结 false 正常
        /// </summary>
        public bool isfrozen { get; set; } = true;

    }

    public class AUserEditQQInput
    {


        public string QQ { get; set; }
    }

}
