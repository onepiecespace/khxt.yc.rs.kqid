﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    public class BankListInfo
    {

        public decimal MoneyTotal { get; set; }

        public List<BankInfo> BankList { get; set; }

    }
}