﻿using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Models.Vip;
using System;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class UserBaseInfo
    {
        ///<Summary>
        /// 用户id
        ///</Summary>
        public long Userid { get; set; }
        ///<Summary>
        /// 租户Id
        ///</Summary>
        public long TenantId { get; set; }
        ///<Summary>
        /// 用户名昵称
        ///</Summary>
        public string Username { get; set; } = "";

        ///<Summary>
        /// 性别 0 女 1 男 2未知
        ///</Summary>
        public int Sex { get; set; } = 2;
        ///<Summary>
        /// 生日
        ///</Summary>
        public string Birthday { get; set; } = "";
        ///<Summary>
        /// 省
        ///</Summary>
        public string Provincial { get; set; } = "";
        ///<Summary>
        /// 市
        ///</Summary>
        public string City { get; set; } = "";
        ///<Summary>
        /// 区
        ///</Summary>
        public string Areas { get; set; } = "";
        ///<Summary>
        /// 详细地址
        ///</Summary>
        public string Address { get; set; } = "";
        ///<Summary>
        /// 年级
        ///</Summary>
        public int Grade { get; set; }
        ///<Summary>
        /// 头像
        ///</Summary>
        public string Headurl { get; set; } = "";
        ///<Summary>
        /// 等级
        ///</Summary>
        public int Level { get; set; }
        /// <summary>
        /// 各个模块等级
        /// </summary>
        public Dictionary<int, int> Levels { get; set; }
        ///<Summary>
        /// 手机号
        ///</Summary>
        public string Phone { get; set; }
        ///<Summary>
        /// 微信
        ///</Summary>
        public string Wx { get; set; } = "";
        ///<Summary>
        /// QQ
        ///</Summary>
        public string Qq { get; set; } = "";
        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime Creatime { get; set; } = DateTime.MinValue;
        /// <summary>
        /// 创建时间 yyyy-MM-dd
        /// </summary>
        public string CreateTimeStr { get; set; }
        ///<Summary>
        /// 状态 1.可用  2.禁用
        ///</Summary>
        public int State { get; set; }
        ///<Summary>
        /// 邀请人ID
        ///</Summary>
        public int Inviterid { get; set; }
        ///<Summary>
        /// 积分
        ///</Summary>
        public int Score { get; set; }
        ///<Summary>
        /// 金币
        ///</Summary>
        public long Gold { get; set; }
        ///<Summary>
        /// 账户总金额
        ///</Summary>
        public decimal MoneyTotal { get; set; }
        ///<Summary>
        /// 经验
        ///</Summary>
        public long Experience { get; set; }
        /// <summary>
        /// 各个模块经验
        /// </summary>
        public Dictionary<int, long> Experiences { get; set; }

        ///<Summary>
        /// 是否删除 0 删除
        ///</Summary>
        public int Isdelete { get; set; }

        ///<Summary>
        ///  登录方式 1 app 2 wx 4 qq 8 guest
        ///</Summary>
        public int LoginType { get; set; }
        ///<Summary>
        /// 微信小程序登录地址
        ///</Summary>
        public string MiniWX { get; set; }
        /// <summary>
        /// 账户类型：1：正常，2 内部审核 512：试用 8192:压力测试
        /// </summary>
        public int AccountType { get; set; }
        /// <summary>
        /// 会员标识 0非会员 1是会员
        /// </summary>
        public int IsMember { get; set; }
        /// <summary>
        /// 会员卡类型  0非会员 1月卡2半年卡 4年卡
        /// </summary>
        public int Category { get; set; }
        /// <summary>
        /// 会员开通日期
        /// </summary>
        public DateTime? MemberDueDate { get; set; }

        /// <summary>
        /// 代理商 默认mxh 
        /// </summary>
        public string Aid { get; set; }
        /// <summary>
        /// 是否第一次输入地址信息
        /// </summary>
        public int IsFirstInputAddress { get; set; }
        /// <summary>
        /// 是否第一次校区信息
        /// </summary>
        public int IsFirstCollege { get; set; }
        /// <summary>
        /// 授权课程一级分类Ids
        /// </summary>
        public string AuthCateorys { get; set; }
        /// <summary>
        /// 账户使用限制日期
        /// </summary>
        public DateTime? UseDueDate { get; set; }

        public string OpenId { get; set; }
        /// <summary>
        /// 三方登录UnionId
        /// </summary>
        public string UnionId { get; set; }
        /// <summary>
        /// 具体收货地址
        /// </summary>
        public string DetailAddress { get; set; } = "";
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        public List<KeyValue> GradeList { get; set; } = new List<KeyValue>();
        /// <summary>
        /// 用户会员信息
        /// </summary>
        public UserVipModel VipInfo { get; set; }
        /// <summary>
        /// 用户完善的校区信息
        /// </summary>
        public UserCollegeInfo CollegeInfo { get; set; } = new UserCollegeInfo();
        /// <summary>
        ///岗位
        /// </summary>
        public List<CollegeKeyValue> PostType { get; set; } = new List<CollegeKeyValue>();
        /// <summary>
        /// 合作模式
        /// </summary>
        public List<CollegeKeyValue> ModeType { get; set; } = new List<CollegeKeyValue>();
    }
}