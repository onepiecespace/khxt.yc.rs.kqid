﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    #region 提现 
    /// <summary>
    /// 
    /// </summary>
    public class CashbackInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal CashbackMoney { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<CashbackLog> CashbackLogList { get; set; }
    }


    #endregion
}
