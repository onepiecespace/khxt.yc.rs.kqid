﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class InviteRegisterInfo
    {
        public int InviteRegisterCount { get; set; }

        public string InviteRegisterCode { get; set; }
        public List<InviteRegisterLog> InviteRegisterLogList { get; set; }
    }


}
