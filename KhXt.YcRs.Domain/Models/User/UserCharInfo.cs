﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.User
{
    /// <summary>
    /// 
    /// </summary>
    public class UserCharInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 租户标识IHaveTenant
        /// </summary>
        public long TenantId { get; set; }
        ///<Summary>
        /// 用户名
        ///</Summary>
        public string Username { get; set; }
        ///<Summary>
        /// 密码
        ///</Summary>
        public string Password { get; set; }
        ///<Summary>
        /// 性别 0 女  1男 2 未知
        ///</Summary>
        public int Sex { get; set; }
        ///<Summary>
        /// 生日
        ///</Summary>
        public string Birthday { get; set; }
        ///<Summary>
        /// 省
        ///</Summary>
        public string Provincial { get; set; }
        ///<Summary>
        /// 市
        ///</Summary>
        public string City { get; set; }
        ///<Summary>
        /// 区
        ///</Summary>
        public string Areas { get; set; }
        ///<Summary>
        /// 详细地址
        ///</Summary>
        public string Address { get; set; }

        ///<Summary>
        /// 年级 0 表示为未填写
        ///</Summary>
        public int Grade { get; set; } = 1;
        ///<Summary>
        /// 头像
        ///</Summary>
        public string Headurl { get; set; }
        ///<Summary>
        /// 等级
        ///</Summary>
        public int Level { get; set; }
        ///<Summary>
        /// 手机号
        ///</Summary>
        public string Phone { get; set; }
        ///<Summary>
        /// 微信
        ///</Summary>
        public string Wx { get; set; }
        ///<Summary>
        /// QQ
        ///</Summary>
        public string Qq { get; set; }
        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime Creatime { get; set; }
        ///<Summary>
        /// 状态 1.可用  2.禁用
        ///</Summary>
        public int State { get; set; }
        ///<Summary>
        /// 邀请人ID
        ///</Summary>
        public int Inviterid { get; set; }
        ///<Summary>
        /// 积分
        ///</Summary>
        public int Score { get; set; }
        ///<Summary>
        /// 金币
        ///</Summary>
        public int Gold { get; set; }
        ///<Summary>
        /// 账户总金额
        ///</Summary>
        public decimal MoneyTotal { get; set; }
        ///<Summary>
        /// 经验
        ///</Summary>
        public int Experience { get; set; }

        //public int TenantId { get; set; }

        ///<Summary>
        /// 是否删除 0 删除 1存在
        ///</Summary>
        public int Isdelete { get; set; }
        ///<Summary>
        /// 注册方式 1 app  2 后台
        ///</Summary>
        public int LoginType { get; set; }
        ///<Summary>
        /// 微信小程序登录地址
        ///</Summary>
        public string MiniWX { get; set; }
        ///<Summary>
        /// 是否第一次输入地址信息
        ///</Summary>
        public int IsFirstInputAddress { get; set; }


        ///<Summary>
        ///
        ///</Summary>
        public string OpenId { get; set; }


        ///<Summary>
        ///账户类型：1：正常，512：试用
        ///</Summary>
        public int AccountType { get; set; } = 1;

        public int IsMember { get; set; }
        public DateTime? MemberDueDate { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public string CreateTimeString { get; set; }
        /// <summary>
        /// 月份
        /// </summary>
        public int CreateTimeMonth { get; set; }
    }
}
