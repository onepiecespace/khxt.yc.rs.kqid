﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.User
{
    /// <summary>
    /// 校区展示json 
    /// </summary>
    public class Orgexhibition
    {
        //public List<Orgchildjson> orgexhibition { get; set; }
        public string Name { get; set; }
        public string Picurl { get; set; }
        public string Introduction { get; set; }
    }

    public class Coursecontent
    {
        public string title { get; set; }
        public string picurl { get; set; }
        public string info { get; set; }
        public string price { get; set; }
        public string discountPrice { get; set; }
        public string detailtitle { get; set; }
        public string detailimg { get; set; }
        public string detailscontent { get; set; }
    }
}
