﻿using System;

namespace KhXt.YcRs.Domain.Models
{
    public class MyCourseOrderInfo
    {
        /// <summary>
        /// 课程图片
        /// </summary>
        public string Logo { get; set; }
        /// <summary>
        /// 是否直播
        /// </summary>
        public int IsLive { get; set; }
        /// <summary>
        /// 课程标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DateTime { get; set; }
        public string LiveStartTime { get; set; }
        public string LiveEndTime { get; set; }
        public string CourseStartTime { get; set; }
        public string CourseEndTime { get; set; }


    }

    public class MyCourseOrderCollectInfo
    {
        /// <summary>
        /// 购买表主键ID
        /// </summary>
        public long cbid { get; set; }
        /// <summary>
        /// 主课程courseid
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Cateorys { get; set; }
        /// <summary>
        /// 子课程免费试听
        /// </summary>
        public string childIds { get; set; }
        /// <summary>
        /// 课程图片
        /// </summary>
        public string CoverImg { get; set; }
        /// <summary>
        /// 课程图片封面图
        /// </summary>
        public string PicPhoneMini { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TeachersUserName { get; set; }
        /// <summary>
        /// 是否直播 0 否录播课 1 直播开启中 2 是直播课
        /// </summary>
        public int IsLive { get; set; }
        /// <summary>
        /// 课程是否测评0否1是
        /// </summary>
        public int IsRecord { get; set; }
        /// <summary>
        /// 课程标题
        /// </summary>
        public string Title { get; set; }
        public string Detailed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DateTime { get; set; }
        public string LiveStartTime { get; set; }
        public string LiveEndTime { get; set; }
        public string CourseStartTime { get; set; }
        public string CourseEndTime { get; set; }
        /// <summary>
        /// 是否购买
        /// </summary>
        public int isbuy { get; set; }

        /// <summary>
        /// 课程下单支付时间 截止日期减去一年
        /// </summary>		 
        public string PayTime { get; set; }
        /// <summary>
        /// 课程有效期截止时间 也称作过期时间
        /// </summary>		 
        public string ExpirationDate { get; set; }

        /// <summary>
        /// 是否隐藏
        /// </summary>		 
        public int IsHide { get; set; }
        /// <summary>
        /// 是否过期 true 过期  false  未过期
        /// </summary>		 
        public bool Isexpired { get; set; }
        /// <summary>
        /// 过期消息
        /// </summary>
        public string exmsg { get; set; }

        public bool authen { get; set; }
        public string SecretKey { get; set; }
        /// <summary>
        /// 进度比例
        /// </summary>
        public double progress { get; set; }
        //已学习的子课程
        public int AlreadyDone { get; set; }
        /// <summary>
        ///全部子课程
        /// </summary>
        public int TotalDone { get; set; }

    }

    public class CourseBycollectModel
    {
        public long Id { get; set; }
        /// <summary>
        /// UserId
        /// </summary>		 
        public int UserId { get; set; }

        /// <summary>
        /// CourseOrderId
        /// </summary>		 
        public string CourseOrderId { get; set; }

        /// <summary>
        /// CourseId
        /// </summary>		 
        public int CourseId { get; set; }

        /// <summary>
        /// CreateId
        /// </summary>		 
        public int CreateId { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>		 
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// IsBuy
        /// </summary>		 
        public int IsBuy { get; set; }

        /// <summary>
        /// IsCollect
        /// </summary>		 
        public int IsCollect { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>		 
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// 是否隐藏
        /// </summary>		 
        public int IsHide { get; set; }
    }

}