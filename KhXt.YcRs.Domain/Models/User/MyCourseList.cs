﻿using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Models
{
    public class MyCourseList
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int totalCount { get; set; }

        public PageList<MyCourseOrderCollectInfo> MyCourseOrderCollectList { get; set; }


    }
}