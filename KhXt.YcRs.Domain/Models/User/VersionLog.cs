﻿using System;

namespace KhXt.YcRs.Domain.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class VersionLog
    {
        public int Id { get; set; }
        /// <summary>
        /// 代理商标识
        /// </summary>
        public string Aid { get; set; }
        /// <summary>
        /// 设备类型 0 安卓 1 苹果
        /// </summary>
        public int OsSign { get; set; }
        /// <summary>
        /// 版本编号
        /// </summary>
        public string VersionCode { get; set; }

        /// <summary>
        /// 当前最新版本
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 当前最新版本编译
        /// </summary>
        public int Build { get; set; }
        /// <summary>
        ///版本内容
        /// </summary>

        public string Content { get; set; }
        /// <summary>
        /// 下载地址
        /// </summary>
        public string ApkUrl { get; set; }

        /// <summary>
        /// 最小支持版本
        /// </summary>
        public int MinVersion { get; set; }
        /// <summary>
        /// 是否有更新
        /// </summary>
        public int IsUpdate { get; set; }
        /// <summary>
        /// 是否强制更新
        /// </summary>
        public int ForceUpdate { get; set; }
        public DateTime CreateTime { get; set; }

    }

}
