﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.User
{
    /// <summary>
    /// 
    /// </summary>
    public class UserLeveModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public int BusinessType { get; set; }
    }
    public class UserliveModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long userid { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string phone { get; set; }
        /// <summary>
        /// 账户类型：1：正常，2 内部审核测试 512：试用 8192:压力测试
        /// </summary>
        public int AccountType { get; set; }

    }
}
