﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Models.User
{
    /// <summary>
    /// 用户积分、金币、经验等等变化情况
    /// </summary>
    public class ValueChange<T>
    {

        public bool IsChanged { get; set; }
        /// <summary>
        /// 变化量
        /// </summary>
        public T ChangedValue { get; set; }

        /// <summary>
        /// 当前量（变化后的值 ）
        /// </summary>
        public T CurrentValue { get; set; }

        /// <summary>
        /// 用于不同类型值的额外定义，根据上下文含义可能不同，比如用户金币、经验的变动可能会带来相关称号的变动，可在此处一并传出
        /// </summary>
        public string Tag { get; set; }
    }

    public class ValueChange : ValueChange<int>
    {

    }

    public class StringValueChange : ValueChange<string> { }

    public class ValueAndLevel<T>
    {
        public long CurrentValue { get; set; }

        public ValueChange<T> LevelChangeState { get; set; }
    }
}
