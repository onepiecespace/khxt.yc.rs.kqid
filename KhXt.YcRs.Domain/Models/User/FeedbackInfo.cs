﻿namespace KhXt.YcRs.Domain.Models
{
    public class FeedbackInfo
    {
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
