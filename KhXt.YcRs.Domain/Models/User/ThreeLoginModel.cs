﻿namespace KhXt.YcRs.Domain.Models
{
    public class ThreeLoginModel
    {
        public LoginType LogingType { get; set; }

        public string UnionId { get; set; }

        public string OpenId { get; set; }

        public string NickName { get; set; }

        public string Avatar { get; set; }

        public Sex Sex { get; set; }

        public string Aid { get; set; }
    }

    public class LoginWith2faModel : RequestModelBase
    {
        public LoginType LogingType { get; set; }

        public string UnionId { get; set; }

        public string OpenId { get; set; }

        public string NickName { get; set; }

        public string Avatar { get; set; }

        public Sex Sex { get; set; }
    }
}
