﻿namespace KhXt.YcRs.Domain.Models
{
    public class MyOrderInfo
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 概述
        /// </summary>
        public string Summary { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal PayPrice { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string OrderImg { get; set; }
        /// <summary>
        /// 下单时间
        /// </summary>
        public string DateTime { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 课程Id
        /// </summary>
        public string CourseId { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LogisticsId { get; set; }
        /// <summary>
        /// 订单支付类型
        /// </summary>
        public int orderType { get; set; }
        /// <summary>
        /// 包裹Id
        /// </summary>
        public string PackageNo { get; set; }
        /// <summary>
        /// 快递类型
        /// </summary>
        public string PackageType { get; set; }
        /// <summary>
        /// 快递名称
        /// </summary>
        public string ExpressName { get; set; }
    }
}