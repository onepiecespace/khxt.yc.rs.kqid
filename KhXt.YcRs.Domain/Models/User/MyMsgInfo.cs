﻿namespace KhXt.YcRs.Domain.Models
{
    public class MyMsgInfo
    {
        public long Id { get; set; }
        public string Headurl { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }
        public string DateTime { get; set; }

        public int IsOldSummary { get; set; }
        public int Islive { get; set; }
        public string MsgImg { get; set; }
    }
}