﻿using System;

namespace KhXt.YcRs.Domain.Models
{
    public class UserOpenReq
    {
        public string phone { get; set; }

        /// <summary>
        /// 用户级别
        /// </summary>
        public int level { get; set; }
        /// <summary>
        ///// 授权课程一级分类Ids
        ///// </summary>
        //public string authCateorys { get; set; }
        /// <summary>
        /// 账户使用限制日期
        /// </summary>
        public DateTime? useDueDate { get; set; }
    }
}