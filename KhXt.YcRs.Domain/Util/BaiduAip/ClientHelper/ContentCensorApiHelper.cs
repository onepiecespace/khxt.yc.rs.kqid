﻿using Baidu.Aip;
using Baidu.Aip.ContentCensor;
using Baidu.Aip.Speech;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 百度ai接口内容审核对接
    /// </summary>
    public class ContentCensorApiHelper
    {
        #region 自定义接内容审核接口

        // 百度云中开通对应服务应用的 API Key 建议开通应用的时候多选服务
        private static String clientId = "XURFRRytaPhy9onXyhYsrGAC";
        // 百度云中开通对应服务应用的 Secret Key
        private static String clientSecret = "nAq5yehHxUGfCgYnbvi6MNHmr624NAln";
        /// <summary>
        /// 文本审核json 字符串
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static JObject GetForTextJson(string text)
        {
            JObject result = new JObject();
            try
            {
                var client = new TextCensor(clientId, clientSecret);
                client.Timeout = 60000;
                result = client.TextCensorUserDefined(text);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return result;
            }
        }

        public static JObject GetForImgJson(Stream Stream)
        {
            JObject result = new JObject();
            try
            {
                var client = new ImageCensor(clientId, clientSecret);
                client.Timeout = 60000;  // 超时，毫秒
                                         // var image = File.ReadAllBytes(path);
                var image = DirFileHelper.StreamToBytes(Stream);
                result = client.UserDefined(image);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return result;
            }
        }

        public static JObject GetForImgUrlJson(string imageUrl)
        {
            JObject result = new JObject();
            try
            {
                var client = new ImageCensor(clientId, clientSecret);
                client.Timeout = 60000;  // 超时，毫秒
                result = client.UserDefinedUrl(imageUrl);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return result;
            }
        }
        public static JObject GetForAudioUrlJson(byte[] data, string format, int rate)
        {
            JObject result = new JObject();
            try
            {   // 设置APPID/AK/SK
                var APP_ID = "18159314";
                var API_KEY = "B6FeRhciiz3X3BW50y2pc74ozCLigaLl";
                var SECRET_KEY = "UvYzl7KyoaNRCoh5065pys9ygW56xplg";

                //Stream inputStream = flie.OpenReadStream();
                var client = new Asr(APP_ID, API_KEY, SECRET_KEY);
                // 可选参数
                var options = new Dictionary<string, object> { { "dev_pid", 1536 } };
                client.Timeout = 120000; // 若语音较长，建议设置更大的超时时间. ms
                result = client.Recognize(data, format, rate, options);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return result;
            }
        }
        #endregion

    }
}
