﻿using Baidu.Aip;
using Baidu.Aip.ContentCensor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 百度接口签名帮助类
    /// </summary>
    public class BaiduApiHelper
    {
        #region 构造函数

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="accessKeyId">百度AccessKeyId(AK)</param>
        /// <param name="secretAccessKey">百度SecretAccessKey(SK)</param>
        public BaiduApiHelper(string accessKeyId, string secretAccessKey)
        {
            _accessKeyId = accessKeyId;
            _secretAccessKey = secretAccessKey;
        }

        #endregion

        #region 内部成员 

        private string _accessKeyId { get; }
        private string _secretAccessKey { get; }
        private string UriEncode(string input, bool encodeSlash = false)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in Encoding.UTF8.GetBytes(input))
            {
                if ((b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z') || (b >= '0' && b <= '9') || b == '_' || b == '-' || b == '~' || b == '.')
                {
                    builder.Append((char)b);
                }
                else if (b == '/')
                {
                    if (encodeSlash)
                    {
                        builder.Append("%2F");
                    }
                    else
                    {
                        builder.Append((char)b);
                    }
                }
                else
                {
                    builder.Append('%').Append(b.ToString("X2"));
                }
            }
            return builder.ToString();
        }
        private string Hex(byte[] data)
        {
            var sb = new StringBuilder();
            foreach (var b in data)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }
        private string CanonicalRequest(HttpWebRequest req)
        {
            Uri uri = req.RequestUri;
            StringBuilder canonicalReq = new StringBuilder();
            canonicalReq.Append(req.Method).Append("\n").Append(UriEncode(Uri.UnescapeDataString(uri.AbsolutePath))).Append("\n");

            var parameters = HttpUtility.ParseQueryString(uri.Query);
            List<string> parameterStrings = new List<string>();
            foreach (KeyValuePair<string, string> entry in parameters)
            {
                parameterStrings.Add(UriEncode(entry.Key) + '=' + UriEncode(entry.Value));
            }
            parameterStrings.Sort();
            canonicalReq.Append(string.Join("&", parameterStrings.ToArray())).Append("\n");

            string host = uri.Host;
            if (!(uri.Scheme == "https" && uri.Port == 443) && !(uri.Scheme == "http" && uri.Port == 80))
            {
                host += ":" + uri.Port;
            }
            canonicalReq.Append("host:" + UriEncode(host));
            return canonicalReq.ToString();
        }

        #endregion

        #region 外部接口

        /// <summary>
        /// 发送POST请求
        /// </summary>
        /// <param name="method">请求方法，需要大写，列如(POST)</param>
        /// <param name="host">主机地址列如(http://sms.bj.baidubce.com)</param>
        /// <param name="url">接口地址列如(/bce/v2/message)</param>
        /// <param name="paramters">参数列表</param>
        /// <returns></returns>
        public string RequestData(string method, string host, string url, Dictionary<string, object> paramters = null)
        {
            string ak = _accessKeyId;
            string sk = _secretAccessKey;
            DateTime now = DateTime.Now;
            int expirationInSeconds = 1200;

            HttpWebRequest req = WebRequest.Create(host + url) as HttpWebRequest;
            Uri uri = req.RequestUri;
            req.Method = method;
            req.ContentType = "application/json";

            if (paramters != null)
            {
                Stream requestStream = req.GetRequestStream();
                byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paramters));
                requestStream.Write(data, 0, data.Length);
            }

            string signDate = now.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");
            string authString = "bce-auth-v1/" + ak + "/" + signDate + "/" + expirationInSeconds;
            string signingKey = Hex(new HMACSHA256(Encoding.UTF8.GetBytes(sk)).ComputeHash(Encoding.UTF8.GetBytes(authString)));

            string canonicalRequestString = CanonicalRequest(req);

            string signature = Hex(new HMACSHA256(Encoding.UTF8.GetBytes(signingKey)).ComputeHash(Encoding.UTF8.GetBytes(canonicalRequestString)));
            string authorization = authString + "/host/" + signature;

            req.Headers.Add("x-bce-date", signDate);
            req.Headers.Add(HttpRequestHeader.Authorization, authorization);

            HttpWebResponse res;
            string message = "";
            try
            {
                res = req.GetResponse() as HttpWebResponse;
            }
            catch (WebException e)
            {
                res = e.Response as HttpWebResponse;
            }
            message = new StreamReader(res.GetResponseStream()).ReadToEnd();

            return message;
        }

        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="phoneNum">手机号码</param>
        /// <param name="code">验证码</param>
        /// <returns></returns>
        public static bool SendMsg(string phoneNum, string code)
        {
            try
            {
                BaiduApiHelper baiduApiHelper = new BaiduApiHelper("XURFRRytaPhy9onXyhYsrGAC", "nAq5yehHxUGfCgYnbvi6MNHmr624NAln");

                string host = "http://sms.bj.baidubce.com";
                string url = "/bce/v2/message";
                Dictionary<string, object> paramters = new Dictionary<string, object>();
                paramters.Add("invokeId", "SGf96VPF-WBBx-qbJK");
                paramters.Add("phoneNumber", phoneNum);
                paramters.Add("templateCode", "smsTpl:e7476122a1c24e37b3b0de19d04ae900");
                paramters.Add("contentVar", new { others = "你好", code = code });

                string resJsonStr = baiduApiHelper.RequestData("POST", host, url, paramters);
                var resJson = JsonConvert.DeserializeObject<JObject>(resJsonStr);

                return resJson["code"]?.ToString() == "1000";
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region 自定义接内容审核接口

        // 调用getAccessToken()获取的 access_token建议根据expires_in 时间 设置缓存

        // 百度云中开通对应服务应用的 API Key 建议开通应用的时候多选服务
        private static String clientId = "XURFRRytaPhy9onXyhYsrGAC";
        // 百度云中开通对应服务应用的 Secret Key
        private static String clientSecret = "nAq5yehHxUGfCgYnbvi6MNHmr624NAln";

        public static String getAccessToken()
        {
            string Token = "";
            var resp = Auth.OpenApiFetchToken(clientId, clientSecret);

            if (resp != null)
            {
                Token = (string)resp["access_token"];
            }
            return Token;
        }

        /// <summary>
        /// 文本审核json 字符串
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string GetCensorForTextJson(string text)
        {
            string token = getAccessToken();
            string host = "https://aip.baidubce.com/rest/2.0/antispam/v2/spam?access_token=" + token;
            Encoding encoding = Encoding.Default;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(host);
            request.Method = "post";
            request.KeepAlive = true;
            string str = "content=" + text;
            byte[] buffer = encoding.GetBytes(str);
            request.ContentLength = buffer.Length;
            request.GetRequestStream().Write(buffer, 0, buffer.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.Default);
            string result = reader.ReadToEnd();
            return result;
        }
        public static List<string> OnPostCensorForTextAsync(string text)
        {
            List<string> msg = new List<string>();
            string result = GetCensorForTextJson(text);
            JObject jo = (JObject)JsonConvert.DeserializeObject(result);
            try
            {
                #region  返回说明
                //logid uint64  正确调用生成的唯一标识码，用于问题定位
                //result  object 包含审核结果详情
                //+spam   int 请求中是否包含违禁，0表示非违禁，1表示违禁，2表示建议人工复审
                //+ reject array 审核未通过的类别列表与详情
                //+review array 待人工复审的类别列表与详情
                //+pass   array 审核通过的类别列表与详情
                //++label int 请求中的违禁类型
                //++score float 违禁检测分，范围0~1，数值从低到高代表风险程度的高低
                //++hit array   违禁类型对应命中的违禁词集合，可能为空
                #endregion
                JObject data = (JObject)JsonConvert.DeserializeObject(jo["result"].ToString());
                msg.Add(data["spam"].ToString());//请求中是否包含违禁，0表示非违禁，1表示违禁，2表示建议人工复审
                msg.Add(data["review"].ToString());//array	审核未通过的类别列表与详情
                msg.Add(data["reject"].ToString());//待人工复审的类别列表与详情
                msg.Add(data["pass"].ToString());//审核通过的类别列表与详情
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                msg.Add(result);
            }

            return msg;
        }
        public static JObject GetForImgJson(Stream Stream)
        {
            JObject result = new JObject();
            try
            {
                var client = new AntiPorn(clientId, clientSecret);
                client.Timeout = 60000;  // 超时，毫秒
                                         // var image = File.ReadAllBytes(path);
                var image = DirFileHelper.StreamToBytes(Stream);
                result = client.Detect(image);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return result;
            }
        }


        #endregion

    }
}
