﻿
using Baidu.Aip;
using BaiduBce;
using BaiduBce.Auth;
using BaiduBce.Services.Bos;
using BaiduBce.Services.Bos.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// BosClient是与BOS服务交互的客户端，BOS C# SDK的BOS操作都是通过BosClient完成的
    /// </summary>
    public class BosClientHelper
    {
        #region 构造函数

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="accessKeyId">百度AccessKeyId(AK)</param>
        /// <param name="secretAccessKey">百度SecretAccessKey(SK)</param>
        public BosClientHelper(string accessKeyId, string secretAccessKey)
        {
            _accessKeyId = accessKeyId;
            _secretAccessKey = secretAccessKey;
        }

        #endregion



        #region 公共私有方法
        private string _accessKeyId { get; }
        private string _secretAccessKey { get; }
        // 调用getAccessToken()获取的 access_token建议根据expires_in 时间 设置缓存
        // 返回token示例
        // 百度云中开通对应服务应用的 API Key 建议开通应用的时候多选服务
        private static String accessKeyId = "4b84f9b344aa4d27b43d2b84b60a14cd";
        // 百度云中开通对应服务应用的 Secret Key
        private static String secretAccessKey = "db1792437b3a454c8883d43490371d72";
        private static String endpoint = "http://lianggehuangli.bj.bcebos.com";//传入Bucket所在区域域名
        private static string UriEncode(string input, bool encodeSlash = false)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in Encoding.UTF8.GetBytes(input))
            {
                if ((b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z') || (b >= '0' && b <= '9') || b == '_' || b == '-' || b == '~' || b == '.')
                {
                    builder.Append((char)b);
                }
                else if (b == '/')
                {
                    if (encodeSlash)
                    {
                        builder.Append("%2F");
                    }
                    else
                    {
                        builder.Append((char)b);
                    }
                }
                else
                {
                    builder.Append('%').Append(b.ToString("X2"));
                }
            }
            return builder.ToString();
        }

        private string Hex(byte[] data)
        {
            var sb = new StringBuilder();
            foreach (var b in data)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }
        public static string BuildQuery(IDictionary<string, string> dictionary)
        {
            if (dictionary == null || dictionary.Count == 0)
            {
                throw new ArgumentNullException(nameof(dictionary));
            }

            var content = new StringBuilder();
            foreach (var iter in dictionary)
            {
                if (!string.IsNullOrEmpty(iter.Value))
                {
                    content.Append(iter.Key + "=" + UriEncode(iter.Value) + "&");
                }
            }
            return content.ToString().Substring(0, content.Length - 1);
        }
        private string CanonicalRequest(HttpWebRequest req)
        {
            Uri uri = req.RequestUri;
            StringBuilder canonicalReq = new StringBuilder();
            canonicalReq.Append(req.Method).Append("\n").Append(UriEncode(Uri.UnescapeDataString(uri.AbsolutePath))).Append("\n");

            var parameters = HttpUtility.ParseQueryString(uri.Query);
            List<string> parameterStrings = new List<string>();
            List<KeyValuePair<String, String>> paraList = new List<KeyValuePair<string, string>>();
            if (req.Method == "GET")
            {

                paraList.Add(new KeyValuePair<string, string>("source", "http://r.lianggehuangli.com/read/av/174/10100020/2019-12-09/3866204.mp3"));
                foreach (KeyValuePair<string, string> entry in paraList)
                {
                    parameterStrings.Add(UriEncode(entry.Key) + '=' + UriEncode(entry.Value));
                }
            }
            else
            {
                foreach (KeyValuePair<string, string> entry in parameters)
                {
                    parameterStrings.Add(UriEncode(entry.Key) + '=' + UriEncode(entry.Value));
                }
            }
            parameterStrings.Sort();
            canonicalReq.Append(string.Join("&", parameterStrings.ToArray())).Append("\n");
            string host = uri.Host;
            if (!(uri.Scheme == "https" && uri.Port == 443) && !(uri.Scheme == "http" && uri.Port == 80))
            {
                host += ":" + uri.Port;
            }
            canonicalReq.Append("host:" + UriEncode(host));
            return canonicalReq.ToString();
        }
        #endregion

        #region 外部请求APIgon公共接口

        /// <summary>
        /// 发送method请求
        /// </summary>
        /// <param name="method">请求方法，需要大写，列如(POST)</param>
        /// <param name="host">主机地址列如(http://sms.bj.baidubce.com)</param>
        /// <param name="url">接口地址列如(/bce/v2/message)</param>
        /// <param name="v">版本号(v2)</param>
        /// <param name="paramters">参数列表</param>
        /// <returns></returns>
        public string RequestData(string method, string host, string url, string v, Dictionary<string, object> paramters = null)
        {
            string ak = _accessKeyId;
            string sk = _secretAccessKey;
            DateTime now = DateTime.Now;
            int expirationInSeconds = 1200;

            HttpWebRequest req = WebRequest.Create(host + url) as HttpWebRequest;
            Uri uri = req.RequestUri;
            req.Method = method;
            req.ContentType = "application/json";

            if (paramters != null)
            {
                if (paramters.Count > 0)
                {
                    Stream requestStream = req.GetRequestStream();
                    byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(paramters));
                    requestStream.Write(data, 0, data.Length);
                }
            }
            if (v == "v1")
            {
                string signDate = now.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");
                string authString = "bce-auth-v1/" + ak + "/" + signDate + "/" + expirationInSeconds;
                string signingKey = Hex(new HMACSHA256(Encoding.UTF8.GetBytes(sk)).ComputeHash(Encoding.UTF8.GetBytes(authString)));
                string canonicalRequestString = CanonicalRequest(req);
                string signature = Hex(new HMACSHA256(Encoding.UTF8.GetBytes(signingKey)).ComputeHash(Encoding.UTF8.GetBytes(canonicalRequestString)));
                string authorization = authString + "/host/" + signature;
                req.Headers.Add("x-bce-date", signDate);
                req.Headers.Add(HttpRequestHeader.Authorization, authorization);
            }
            else if (v == "v2")
            {
                string signDate = now.ToUniversalTime().ToString("yyyymmdd");
                string region = "bj";
                string service = "bos";
                string authString = "bce-auth-v2/" + ak + "/" + signDate + "/" + region + "/" + service;
                string signingKey = Hex(new HMACSHA256(Encoding.UTF8.GetBytes(sk)).ComputeHash(Encoding.UTF8.GetBytes(authString)));
                string canonicalRequestString = CanonicalRequest(req);
                string signature = Hex(new HMACSHA256(Encoding.UTF8.GetBytes(signingKey)).ComputeHash(Encoding.UTF8.GetBytes(canonicalRequestString)));
                string authorization = authString + "/host/" + signature;
                req.Headers.Add("x-bce-date", signDate);
                req.Headers.Add(HttpRequestHeader.Authorization, authorization);
            }


            HttpWebResponse res;
            string message = "";
            try
            {
                res = req.GetResponse() as HttpWebResponse;
            }
            catch (WebException e)
            {
                res = e.Response as HttpWebResponse;
            }
            message = new StreamReader(res.GetResponseStream()).ReadToEnd();

            return message;
        }

        #endregion

        public static String getAccessToken()
        {

            var resp = Auth.OpenApiFetchToken("XURFRRytaPhy9onXyhYsrGAC", "nAq5yehHxUGfCgYnbvi6MNHmr624NAln");
            string token = resp["access_token"].ToString();
            return token;
        }
        #region 文本审核接口API
        /// <summary>
        /// 文本审核接口API
        /// </summary>
        /// <returns></returns>
        public static object GetCensorForTextJson(string text)
        {
            JObject result = new JObject();
            try
            {
                string token = getAccessToken();
                BosClientHelper BosClientHelper = new BosClientHelper(accessKeyId, secretAccessKey);

                string host = "http://vcr.bj.baidubce.com";
                string url = "/v1/text";
                Dictionary<string, object> paramters = new Dictionary<string, object>();
                paramters.Add("text", text);
                string resJsonStr = BosClientHelper.RequestData("PUT", host, url, "v1", paramters);
                result = JsonConvert.DeserializeObject<JObject>(resJsonStr);

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }
        #endregion
        #region 图片审核接口API
        /// <summary>
        /// 图片审核接口API
        /// </summary>
        /// <returns></returns>
        public static object GetCensorForImgJson(string source)
        {
            JObject result = new JObject();
            try
            {
                BosClientHelper BosClientHelper = new BosClientHelper(accessKeyId, secretAccessKey);

                string host = "http://vcr.bj.baidubce.com";
                string url = "/v1/image";
                Dictionary<string, object> paramters = new Dictionary<string, object>();
                paramters.Add("source", source);
                paramters.Add("preset", "default");
                paramters.Add("notification", "");
                string resJsonStr = BosClientHelper.RequestData("PUT", host, url, "v1", paramters);
                result = JsonConvert.DeserializeObject<JObject>(resJsonStr);

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }
        #endregion
        #region 音频审核接口API
        /// <summary>
        /// 音频审核接口API
        /// </summary>
        /// <returns></returns>
        public static bool PostCensorForAudioJson(string source)
        {
            bool result = false;
            try
            {
                BosClientHelper BosClientHelper = new BosClientHelper(accessKeyId, secretAccessKey);

                string host = "http://vcr.bj.baidubce.com";
                string url = "/v2/audio";
                Dictionary<string, object> paramters = new Dictionary<string, object>();
                paramters.Add("source", source);
                string resJsonStr = BosClientHelper.RequestData("PUT", host, url, "v1", paramters);
                var resultjson = JsonConvert.DeserializeObject<JObject>(resJsonStr);
                result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }
        #endregion
        #region 读取音频审核结果接口API
        /// <summary>
        /// 读取音频审核结果接口API
        /// </summary>
        /// <returns></returns>
        public static object GetCensorForAudioJson(string source)
        {
            JObject result = new JObject();
            try
            {
                BosClientHelper BosClientHelper = new BosClientHelper(accessKeyId, secretAccessKey);

                string host = "http://vcr.bj.baidubce.com";
                string urlsource = UriEncode(source);
                string url = "/v2/audio?source=" + urlsource + "";
                Dictionary<string, object> paramters = new Dictionary<string, object>();
                //paramters.Add("source", source);
                string resJsonStr = BosClientHelper.RequestData("GET", host, url, "v1", paramters);
                result = JsonConvert.DeserializeObject<JObject>(resJsonStr);

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }
        #endregion
        #region sdk鉴权
        public static BosClient GenerateBosClient()
        {
            // 初始化一个BosClient
            BceClientConfiguration config = new BceClientConfiguration();
            config.Credentials = new DefaultBceCredentials(accessKeyId, secretAccessKey);
            config.Endpoint = endpoint;
            // 设置HTTP最大连接数为10
            config.ConnectionLimit = 10;

            // 设置TCP连接超时为5000毫秒
            config.TimeoutInMillis = 5000;

            // 设置读写数据超时的时间为50000毫秒
            config.ReadWriteTimeoutInMillis = 50000;
            BosClient client = new BosClient(config);
            return client;
        }
        #endregion
        #region 初始化Bucket 与上传资源
        /// <summary>
        /// 新建一个Bucket。Bucket是BOS上的命名空间，相当于数据的容器，可以存储若干数据实体（Object）。在您上传数据前，必须先创建一个Bucket。
        /// </summary>
        /// <param name="bucketName"></param>
        public static void CreateBucket(string bucketName)
        {
            BosClient client = GenerateBosClient();
            // 新建一个Bucket
            client.CreateBucket(bucketName);
        }

        /// <summary>
        /// 上传Object。Object是BOS中最基本的数据单元，
        /// 您可以把Object简单的理解为文件。对于一个简单的Object的上传，
        /// BOS为您提供了四种方式：文件形式上传、数据流形式上传、二进制串上传和字符串上传。
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="objectKey">文件形式上传</param>
        /// <param name="file">文件路径</param>
        public static void putObjectFromFile(String bucketName, String objectKey, FileInfo file)
        {
            BosClient client = GenerateBosClient();
            // 以文件形式上传Object
            PutObjectResponse putObjectFromFileResponse = client.PutObject(bucketName, objectKey, file);
            // 打印ETag
            Console.WriteLine(putObjectFromFileResponse.ETAG);
        }
        /// <summary>
        /// 以数据流形式上传Object
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="objectKey"></param>
        /// <param name="inputStream"></param>
        public static void putObjectFromStream(String bucketName, String objectKey, Stream inputStream)
        {
            BosClient client = GenerateBosClient();
            // 获取数据流
            // Stream inputStream = file.OpenRead();
            // 以数据流形式上传Object
            PutObjectResponse putObjectResponseFromInputStream = client.PutObject(bucketName, objectKey, inputStream);
            // 打印ETag
            Console.WriteLine(putObjectResponseFromInputStream.ETAG);
        }
        /// <summary>
        /// 以数据流形式上传Object
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="objectKey"></param>
        /// <param name="bytes"></param>
        public static void putObjectFromByte(String bucketName, String objectKey, byte[] bytes)
        {
            BosClient client = GenerateBosClient();
            // 以二进制串上传Object Encoding.Default.GetBytes("sampledata")
            PutObjectResponse putObjectResponseFromByte = client.PutObject(bucketName, objectKey, bytes);
            // 打印ETag
            Console.WriteLine(putObjectResponseFromByte.ETAG);
        }
        /// <summary>
        /// 以字符串上传Object
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="objectKey"></param>
        /// <param name="stingstr"></param>
        public static void putObjectFromString(String bucketName, String objectKey, String stingstr)
        {
            BosClient client = GenerateBosClient();
            // 以字符串上传Object
            PutObjectResponse putObjectResponseFromString = client.PutObject(bucketName, objectKey, stingstr);

            // 打印ETag
            Console.WriteLine(putObjectResponseFromString.ETAG);
        }
        #endregion
        #region 读取上传资源
        /// <summary>
        /// 当您完成一系列上传后，可以参考如下代码来查看Bucket下的全部Object。
        /// </summary>
        /// <param name="bucketName"></param>
        public static void ListObjects(string bucketName)
        {
            BosClient client = GenerateBosClient();
            // 获取指定Bucket下的所有Object信息
            ListObjectsResponse listObjectsResponse = client.ListObjects(bucketName);

            // 遍历所有Object
            foreach (BosObjectSummary objectSummary in listObjectsResponse.Contents)
            {
                Console.WriteLine("ObjectKey: " + objectSummary.Key);
            }

        }
        public void GetObject(String bucketName, String objectKey)
        {
            BosClient client = GenerateBosClient();
            // 获取Object，返回结果为BosObject对象
            BosObject bosObject = client.GetObject(bucketName, objectKey);

            // 获取ObjectMeta
            ObjectMetadata meta = bosObject.ObjectMetadata;

            // 获取Object的输入流
            Stream objectContent = bosObject.ObjectContent;

            // 处理Object

            // 关闭流
            objectContent.Close();
        }
        #endregion
    }
}
