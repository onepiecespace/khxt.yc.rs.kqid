﻿using Baidu.Aip;
using Baidu.Aip.ContentCensor;
using Baidu.Aip.Speech;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 百度ai接口多媒体VCR内容审核对接
    /// </summary>
    public class VCRApiHelper
    {
        #region 百度ai接口多媒体VCR内容审核对接

        // 百度云中开通对应服务应用的 API Key 建议开通应用的时候多选服务
        private static String accessKeyId = "4b84f9b344aa4d27b43d2b84b60a14cd";
        // 百度云中开通对应服务应用的 Secret Key
        private static String secretAccessKey = "db1792437b3a454c8883d43490371d72";
        #endregion

        #region demo
        static string UriEncode(string input, bool encodeSlash = false)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in Encoding.UTF8.GetBytes(input))
            {
                if ((b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z') || (b >= '0' && b <= '9') || b == '_' || b == '-' || b == '~' || b == '.')
                {
                    builder.Append((char)b);
                }
                else if (b == '/')
                {
                    if (encodeSlash)
                    {
                        builder.Append("%2F");
                    }
                    else
                    {
                        builder.Append((char)b);
                    }
                }
                else
                {
                    builder.Append('%').Append(b.ToString("X2"));
                }
            }
            return builder.ToString();
        }

        static string Hex(byte[] data)
        {
            var sb = new StringBuilder();
            foreach (var b in data)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }
        static string CanonicalRequest(HttpWebRequest req)
        {
            Uri uri = req.RequestUri;
            StringBuilder canonicalReq = new StringBuilder();
            canonicalReq.Append(req.Method).Append("\n").Append(UriEncode(Uri.UnescapeDataString(uri.AbsolutePath))).Append("\n");

            var parameters = HttpUtility.ParseQueryString(uri.Query);
            List<string> parameterStrings = new List<string>();
            foreach (KeyValuePair<string, string> entry in parameters)
            {
                parameterStrings.Add(UriEncode(entry.Key) + '=' + UriEncode(entry.Value));
            }
            parameterStrings.Sort();
            canonicalReq.Append(string.Join("&", parameterStrings.ToArray())).Append("\n");

            string host = uri.Host;
            if (!(uri.Scheme == "https" && uri.Port == 443) && !(uri.Scheme == "http" && uri.Port == 80))
            {
                host += ":" + uri.Port;
            }
            canonicalReq.Append("host:" + UriEncode(host));
            return canonicalReq.ToString();
        }
        public static void AudioResultDefined(string source)
        {
            string ak = accessKeyId;
            string sk = secretAccessKey;
            DateTime now = DateTime.Now;
            int expirationInSeconds = 1200;

            HttpWebRequest req = WebRequest.Create("http://vcr.bj.baidubce.com/v2/audio?source=" + source) as HttpWebRequest;
            Uri uri = req.RequestUri;
            req.Method = "GET";

            string signDate = now.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");
            Console.WriteLine(signDate);
            string authString = "bce-auth-v1/" + ak + "/" + signDate + "/" + expirationInSeconds;
            string signingKey = Hex(new HMACSHA256(Encoding.UTF8.GetBytes(sk)).ComputeHash(Encoding.UTF8.GetBytes(authString)));
            Console.WriteLine(signingKey);

            string canonicalRequestString = CanonicalRequest(req);
            Console.WriteLine(canonicalRequestString);

            string signature = Hex(new HMACSHA256(Encoding.UTF8.GetBytes(signingKey)).ComputeHash(Encoding.UTF8.GetBytes(canonicalRequestString)));
            string authorization = authString + "/host/" + signature;
            Console.WriteLine(authorization);

            req.Headers.Add("x-bce-date", signDate);
            req.Headers.Add(HttpRequestHeader.Authorization, authorization);

            HttpWebResponse res;
            string message = "";
            try
            {
                res = req.GetResponse() as HttpWebResponse;
            }
            catch (WebException e)
            {
                res = e.Response as HttpWebResponse;
                message = new StreamReader(res.GetResponseStream()).ReadToEnd();
            }
            Console.WriteLine((int)res.StatusCode);
            Console.WriteLine(res.Headers);
            Console.WriteLine(message);
            Console.ReadLine();
        }
        #endregion

        #region 文本审核json 字符串
        /// <summary>
        /// 文本审核json 字符串
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static JObject TextDefinedJson(string text)
        {
            JObject result = new JObject();
            try
            {
                var client = new VCR(accessKeyId, secretAccessKey);
                client.Timeout = 60000;
                result = client.TextDefined(text);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return result;
            }
        }
        public static Result ResultTextDefined(string text)
        {
            var result = TextDefinedJson(text);//NORMAL正常 REJECT拒绝
            if (result["label"].ToString() == "REJECT" || result["label"].ToString() == "REVIEW")
            {
                var rejectmsg = "";
                var jarray = result["results"] as JArray;
                foreach (var array in jarray)
                {
                    var items = array["items"];
                    foreach (var item in items)
                    {
                        Console.WriteLine(item);

                        rejectmsg += item["extra"].ToString() + ",";

                    }
                }
                rejectmsg = rejectmsg.Substring(0, rejectmsg.Length - 1);
                return new Result() { Code = (int)ResultCode.warning, Message = rejectmsg };
            }
            else
            {
                return new Result() { Code = (int)ResultCode.Success, Message = "正常" };
            }
        }
        #endregion

        #region 图片审核接口API
        /// <summary>
        /// 图片审核接口API
        /// </summary>
        /// <returns></returns>
        public static JObject ImageDefinedJson(string source)
        {
            JObject result = new JObject();
            try
            {
                var client = new VCR(accessKeyId, secretAccessKey);
                client.Timeout = 60000;
                result = client.ImageDefined(source);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }
        /// <summary>
        /// 图片审核查询接口API
        /// </summary>
        /// <returns></returns>
        public static JObject ImageAuthDefinedJson(string source)
        {
            JObject result = new JObject();
            try
            {
                var client = new VCR(accessKeyId, secretAccessKey);
                client.Timeout = 60000;
                result = client.ImageAuthDefined(source);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }
        public static Result ResultImageDefinedJson(string source)
        {
            var result = ImageDefinedJson(source);//NORMAL正常 REJECT拒绝
            if (result["label"].ToString() == "REJECT" || result["label"].ToString() == "REVIEW")
            {
                var rejectmsg = "";
                var jarray = result["results"] as JArray;
                foreach (var array in jarray)
                {
                    var items = array["items"];
                    foreach (var item in items)
                    {
                        rejectmsg += item["extra"].ToString() + ",";
                    }
                }
                rejectmsg = rejectmsg.Substring(0, rejectmsg.Length - 1);
                return new Result() { Code = (int)ResultCode.warning, Message = rejectmsg };
            }
            else
            {
                return new Result() { Code = (int)ResultCode.Success, Message = "正常" };
            }
        }
        #endregion

        #region 音频审核接口API
        /// <summary>
        /// 音频审核接口API
        /// </summary>
        /// <param name="source">原生音频mp3</param>
        /// <returns></returns>
        public static JObject AudioDefinedJson(string source)
        {
            JObject result = new JObject();
            try
            {
                var client = new VCR(accessKeyId, secretAccessKey);
                client.Timeout = 60000;
                result = client.AudioDefined(source);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return result;
            }
        }
        #endregion

        #region 音频审核结果接口API
        /// <summary>
        ///音频审核结果接口API
        /// </summary>
        /// <returns></returns>
        public static JObject AudioResultDefinedJson(string source)
        {
            JObject result = new JObject();
            try
            {
                var client = new VCR(accessKeyId, secretAccessKey);
                client.Timeout = 60000;
                result = client.AudioResultDefined(source);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }
        #endregion

    }
}
