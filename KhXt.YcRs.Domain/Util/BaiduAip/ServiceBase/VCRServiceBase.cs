﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Options = System.Collections.Generic.Dictionary<string, string>;

namespace Baidu.Aip
{
    public abstract class VCRServiceBase
    {
        protected readonly object AuthLock = new object();
        protected volatile bool HasDoneAuthoried; // 是否已经走过鉴权流程
        protected volatile bool IsDev;

        protected VCRServiceBase(string apiKey, string secretKey) : this("", apiKey, secretKey)
        {

        }

        protected VCRServiceBase(string appId, string apiKey, string secretKey)
        {
            AppId = appId;
            ApiKey = apiKey;
            SecretKey = secretKey;
            ExpireAt = DateTime.Now;
            DebugLog = false;
            Timeout = 60000;
        }

        protected string Token { get; set; }
        protected DateTime ExpireAt { get; set; }

        public string AppId { get; set; }
        public string ApiKey { get; set; }
        public string SecretKey { get; set; }
        public bool DebugLog { get; set; }

        /// <summary>
        /// in millisecond
        /// </summary>
        public int Timeout { get; set; }

        protected virtual JObject PostAction(VCRHttpRequest aipReq)
        {
            var respStr = SendRequet(aipReq);
            JObject respObj;
            try
            {
                respObj = JsonConvert.DeserializeObject(respStr) as JObject;
            }
            catch (Exception e)
            {
                // 非json应该抛异常
                throw new AipException(e.Message + ": " + respStr);
            }

            return respObj;
        }

        protected virtual HttpWebRequest GenerateWebRequest(VCRHttpRequest aipRequest)
        {

            return aipRequest.GenerateCloudRequest(ApiKey, SecretKey, Timeout);
        }

        protected string SendRequet(VCRHttpRequest aipRequest)
        {
            return Utils.StreamToString(SendRequetRaw(aipRequest).GetResponseStream(), aipRequest.ContentEncoding);
        }

        protected HttpWebResponse SendRequetRaw(VCRHttpRequest aipRequest)
        {
            var webReq = GenerateWebRequest(aipRequest);
            Log(webReq.RequestUri.ToString());
            HttpWebResponse resp;
            try
            {
                resp = (HttpWebResponse)webReq.GetResponse();
            }
            catch (WebException e)
            {
                // 网络请求失败应该抛异常
                throw new AipException((int)e.Status, e.Message);
            }

            if (resp.StatusCode != HttpStatusCode.OK)
                throw new AipException((int)resp.StatusCode, "Server response code：" + (int)resp.StatusCode);

            return resp;
        }

        protected void CheckNotNull(object obj, string name)
        {
            if (obj == null)
                throw new AipException(name + " cannot be null.");
        }

        protected string ImagesToParams(IEnumerable<byte[]> images)
        {
            return images
                .Select(Convert.ToBase64String)
                .Aggregate((a, b) => a + "," + b);
        }

        protected string StrJoin(IEnumerable<string> data, string sep = ",")
        {
            return data.Aggregate((a, b) => a + sep + b);
        }

        protected virtual void Log(string msg)
        {
            if (DebugLog)
            {
                var dateStr = DateTime.Now.ToString("[yyyyMMdd HH:mm:ss]");
                Console.WriteLine("{0} [{1}] {2}", dateStr, GetType().FullName, msg);
            }
        }

        public class Type
        {
            public Type(string url)
            {
                Url = url;
            }

            public string Url { get; set; }
        }


    }
}