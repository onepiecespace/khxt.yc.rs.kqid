﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Baidu.Aip.Speech
{
    /// <summary>
    ///  媒体内容审核相关接口
    /// </summary>
    public class VCR : VCRServiceBase
    {
        private const string host = "http://vcr.bj.baidubce.com";
        public VCR(string apiKey, string secretKey) : base(apiKey, secretKey)
        {
        }

        protected VCRHttpRequest DefaultRequest(string uri, string method)
        {
            return new VCRHttpRequest(host + uri)
            {
                Method = method,
                BodyType = VCRHttpRequest.BodyFormat.Json,
                ContentEncoding = Encoding.GetEncoding("UTF-8")
            };
        }

        /// <summary>
        /// 内容审核文本API接口
        /// </summary>
        /// <param name="text"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public JObject TextDefined(string text, Dictionary<string, object> options = null)
        {

            var aipReq = DefaultRequest("/v1/text", "PUT");
            aipReq.Bodys["text"] = text;
            if (options != null)
                foreach (var pair in options)
                    aipReq.Bodys[pair.Key] = pair.Value;
            return PostAction(aipReq);
        }
        /// <summary>
        /// 图像审核接口
        /// </summary>
        /// <param name="source"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public JObject ImageDefined(string source, Dictionary<string, object> options = null)
        {
            var aipReq = DefaultRequest("/v1/image", "PUT");
            aipReq.Bodys["source"] = source;
            aipReq.Bodys["preset"] = "default";
            //aipReq.Bodys["notification"] = "vcr_callback";
            if (options != null)
                foreach (var pair in options)
                    aipReq.Bodys[pair.Key] = pair.Value;
            return PostAction(aipReq);
        }
        /// <summary>
        /// 图像审核接口
        /// </summary>
        /// <param name="source"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public JObject ImageAuthDefined(string source, Dictionary<string, object> options = null)
        {
            var aipReq = DefaultRequest("/v2/image", "POST");
            aipReq.Querys["source"] = source;
            if (options != null)
                foreach (var pair in options)
                    aipReq.Bodys[pair.Key] = pair.Value;
            return PostAction(aipReq);
        }
        /// <summary>
        /// 音频审核接口
        /// </summary>
        /// <param name="source"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public JObject AudioDefined(string source, Dictionary<string, object> options = null)
        {
            var aipReq = DefaultRequest("/v2/audio", "PUT");
            aipReq.Bodys["source"] = source;
            aipReq.Bodys["notification"] = "vcr_callback";
            if (options != null)
                foreach (var pair in options)
                    aipReq.Bodys[pair.Key] = pair.Value;
            return PostAction(aipReq);
        }
        /// <summary>
        /// 音频审核结果接口
        /// </summary>
        /// <param name="source"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public JObject AudioResultDefined(string source, Dictionary<string, object> options = null)
        {
            var aipReq = DefaultRequest("/v2/audio", "POST");
            aipReq.Querys["source"] = source;
            if (options != null)
                foreach (var pair in options)
                    aipReq.Bodys[pair.Key] = pair.Value;
            return PostAction(aipReq);
        }
    }
}