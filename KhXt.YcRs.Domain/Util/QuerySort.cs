﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 排序查询
    /// </summary>
    public class QuerySort
    {

        /// <summary>
        /// 将实体对象集合按字段排序
        /// </summary>
        /// <typeparam name="T">实体对象类型</typeparam>
        /// <param name="list">实体对象集合</param>
        /// <param name="field">排序字段</param>
        /// <param name="kind">排序方式</param>
        /// <returns>排序后的集合</returns>
        public static List<T> Sort<T>(List<T> list, string field, SortKind kind)
        {
            if (list != null)
            {
                switch (kind)
                {
                    case SortKind.ASC: list = list.OrderBy(t => GetValue(t, field)).ToList(); break;
                    case SortKind.DESC: list = list.OrderByDescending(t => GetValue(t, field)).ToList(); break;
                }
            }
            return list;
        }
        public static List<T> Sort<T>(List<T> list, Dictionary<string, SortKind> dicsort)
        {
            if (list != null)
            {

                IOrderedEnumerable<T> tmporderlist = null;

                foreach (var item in dicsort)
                {
                    switch (item.Value)
                    {
                        case SortKind.ASC:
                            {
                                if (tmporderlist == null)
                                {
                                    tmporderlist = list.OrderBy(t => GetValue(t, item.Key));
                                }
                                else
                                {
                                    tmporderlist = tmporderlist.ThenBy(t => GetValue(t, item.Key));
                                }
                            }
                            break;
                        case SortKind.DESC:
                            {
                                if (tmporderlist == null)
                                {
                                    tmporderlist = list.OrderByDescending(t => GetValue(t, item.Key));
                                }
                                else
                                {
                                    tmporderlist = tmporderlist.ThenByDescending(t => GetValue(t, item.Key));
                                }
                            }
                            break;
                    }
                }
                list = tmporderlist.ToList();
            }
            return list;
        }

        /// <summary>
        /// 动态获取如model对象中的XX对象的XX属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        private static object GetValue<T>(T model, string field)
        {
            if (field.Split('.').Length == 1)
            {
                var tmppro = model.GetType().GetProperty(field);
                object tmpValue = null;
                if (tmppro != null)
                {
                    tmpValue = tmppro.GetValue(model, null);
                }
                return tmpValue;
            }

            int index = field.IndexOf('.');
            string f1 = field.Substring(0, index);
            string f2 = field.Substring(index + 1);
            object obj = model.GetType().GetProperty(f1).GetValue(model, null);
            return GetValue(obj, f2);
        }
    }
}



