﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 数据加密类，提供各种加密解密算法及防篡改散列算法
    /// </summary>·
    public static class EncryptionUtility
    {

        #region 私有属性

        /// <summary>
        /// 缓冲区大小
        /// </summary>
        private static readonly int BufferSize = 128 * 1024;
        /// <summary>
        /// 密钥salt
        /// </summary>
        private static readonly byte[] Salt = { 134, 216, 7, 36, 88, 164, 91, 227, 174, 76, 191, 197, 192, 154, 200, 248 };
        /// <summary>
        /// 初始化向量
        /// </summary>
        private static readonly byte[] Iv = { 134, 216, 7, 36, 88, 164, 91, 227, 174, 76, 191, 197, 192, 154, 200, 248 };

        /// <summary>
        /// 初始化并返回对称加密算法
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        private static SymmetricAlgorithm CreateRijndael(string password, byte[] salt)
        {
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, salt, "SHA256", 1000);
            SymmetricAlgorithm sma = Rijndael.Create();
            sma.KeySize = 256;
#pragma warning disable 618
            sma.Key = pdb.GetBytes(32);
#pragma warning restore 618
            sma.Padding = PaddingMode.PKCS7;
            return sma;
        }


        /// <summary>
        /// 随机密钥
        /// </summary>
        private static readonly byte[] Keys = new byte[]
        {
            239,171,86,20,144,52,205,182,22,38,123,137,215
        };

        #endregion


        #region MD5加密

        /// <summary>
        /// 生成指定字符串的MD5散列值，返回大写串
        /// </summary>
        /// <param name="srcValue">源字符串</param>
        /// <param name="encodeType">type类型：16位还是32位</param>
        /// <param name="encoding"></param>
        /// <returns>MD5值</returns>
        public static string Md5Encode(string srcValue, int encodeType, Encoding encoding)
        {
            var bytes = encoding.GetBytes(srcValue);
            MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
            byte[] value = mD5CryptoServiceProvider.ComputeHash(bytes);
            if (encodeType == 16)
            {
                return BitConverter.ToString(value).Replace("-", "").ToUpper().Substring(8, 16);
            }
            return BitConverter.ToString(value).Replace("-", "").ToUpper();
        }
        /// <summary>
        /// 生成指定字符串的MD5散列值，返回大写串(默认default)
        /// </summary>
        /// <param name="srcValue">源字符串</param>
        /// <param name="encodeType">type类型：16位还是32位</param>
        /// <returns>MD5值</returns>
        public static string Md5Encode(string srcValue, int encodeType = 32)
        {
            return Md5Encode(srcValue, encodeType, Encoding.Default);
        }
        #endregion

        #region SHA加密


        /// <summary>
        /// 生成指定字符串的SHA256散列值
        /// </summary>
        /// <param name="srcValue">源字符串</param>
        /// <returns>SHA256值</returns>
        public static string Sha256Encode(string srcValue)
        {
            SHA256 sHa = new SHA256Managed();
            byte[] inArray = sHa.ComputeHash(Encoding.Default.GetBytes(srcValue));
            sHa.Clear();
            return Convert.ToBase64String(inArray);
        }
        /// <summary>
        /// 生成指定字符串的SHA384散列值
        /// </summary>
        /// <param name="srcValue">源字符串</param>
        /// <returns>SHA384值</returns>
        public static string Sha384Encode(string srcValue)
        {
            SHA384 sHa = new SHA384Managed();
            var inArray = sHa.ComputeHash(Encoding.Default.GetBytes(srcValue));
            sHa.Clear();
            return Convert.ToBase64String(inArray);
        }
        /// <summary>
        /// 生成指定字符串的SHA512散列值(不可逆)
        /// </summary>
        /// <param name="srcValue">源字符串</param>
        /// <returns>SHA512值</returns>
        public static string Sha512Encode(string srcValue)
        {
            SHA512 sHa = new SHA512Managed();
            byte[] inArray = sHa.ComputeHash(Encoding.Default.GetBytes(srcValue));

            sHa.Clear();
            return Convert.ToBase64String(inArray);
        }


        #endregion

        #region DES加密、解密
        /// <summary>
        /// DES加密字符串
        /// </summary>
        /// <param name="encryptString">待加密的字符串</param>
        /// <param name="encryptKey">加密密钥,要求为8位</param>
        /// <returns>加密成功返回加密后的字符串，失败返回源串</returns>
        public static string DES_Encrypt(string encryptString, string encryptKey)
        {
            string result;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(encryptKey.Substring(0, 8));
                byte[] keys = Keys;
                byte[] bytes2 = Encoding.UTF8.GetBytes(encryptString);
                DESCryptoServiceProvider dEsCryptoServiceProvider = new DESCryptoServiceProvider();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, dEsCryptoServiceProvider.CreateEncryptor(bytes, keys), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(bytes2, 0, bytes2.Length);
                        cryptoStream.FlushFinalBlock();
                    }
                    result = Convert.ToBase64String(memoryStream.ToArray());
                }
            }
            catch
            {
                result = encryptString;
            }
            return result;
        }
        /// <summary>
        ///  DES解密字符串
        /// </summary>
        /// <param name="decryptString"> 待解密的字符串</param>
        /// <param name="decryptKey">解密密钥,要求为8位,和加密密钥相同 </param>
        /// <returns>解密成功返回解密后的字符串，失败返源串</returns>
        public static string DES_Decrypt(string decryptString, string decryptKey)
        {
            string result;
            try
            {
                var bytes = Encoding.UTF8.GetBytes(decryptKey.Substring(0, 8));
                var keys = Keys;
                var array = Convert.FromBase64String(decryptString);
                DESCryptoServiceProvider dEsCryptoServiceProvider = new DESCryptoServiceProvider();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, dEsCryptoServiceProvider.CreateDecryptor(bytes, keys), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(array, 0, array.Length);
                        cryptoStream.FlushFinalBlock();
                    }
                    result = Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
            catch
            {
                result = decryptString;
            }
            return result;
        }

        #endregion

        #region AES加密、解密

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="encryptStr">待加密的字符串</param>
        /// <param name="key">加密密钥</param>
        /// <returns>加密成功返回加密后的字符串，失败返回源串</returns>
        public static string AES_Encrypt(string encryptStr, string key)
        {
            try
            {
                Byte[] bKey = new Byte[32];
                string str = key.PadRight(bKey.Length);
                byte[] keyArray = Encoding.UTF8.GetBytes(str);
                Array.Copy(keyArray, bKey, bKey.Length);

                byte[] toEncryptArray = Encoding.UTF8.GetBytes(encryptStr);
                var aes = new AesManaged()
                {
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7,
                    KeySize = 256,
                    Key = bKey
                };
                var resultArray = aes.CreateEncryptor().TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                aes.Clear();
                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch (Exception)
            {
                return encryptStr;
            }

        }


        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="decryptStr">待解密的字符串</param>
        /// <param name="key">加密密钥,Key是24位</param>
        /// <returns>解密成功返回解密后的字符串，失败返源串</returns>
        public static string AES_Decrypt(string decryptStr, string key)
        {

            try
            {
                Byte[] bKey = new Byte[32];
                string str = key.PadRight(bKey.Length);
                byte[] keyArray = Encoding.UTF8.GetBytes(str);
                Array.Copy(keyArray, bKey, bKey.Length);

                byte[] toEncryptArray = Convert.FromBase64String(decryptStr);

                var aes = new AesManaged()
                {
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7,
                    KeySize = 256,
                    Key = bKey
                };
                byte[] resultArray = aes.CreateDecryptor().TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                aes.Clear();
                return Encoding.UTF8.GetString(resultArray);

            }
            catch (Exception ex)
            {
                //
                Console.WriteLine(ex.Message);
                return decryptStr;
            }

        }

        #endregion

        #region HMACSHA
        /// <summary>
        /// 
        /// </summary>
        /// <param name="secret">密钥</param>
        /// <param name="signKey">key</param>
        /// <returns></returns>
        public static string HmacSHA256(string secret, string signKey)
        {
            string signRet = string.Empty;
            using (HMACSHA256 mac = new HMACSHA256(Encoding.UTF8.GetBytes(signKey)))
            {
                byte[] hash = mac.ComputeHash(Encoding.UTF8.GetBytes(secret));
                signRet = Convert.ToBase64String(hash);
                //signRet = ToHexString(hash); ;
            }
            return signRet;
        }
        private static string ToHexString(byte[] bytes)
        {
            string hexString = string.Empty;
            if (bytes != null)
            {
                StringBuilder strB = new StringBuilder();
                foreach (byte b in bytes)
                {
                    strB.AppendFormat("{0:x2}", b);
                }
                hexString = strB.ToString();
            }
            return hexString;
        }

        #endregion

        #region Base64加密、解密

        /// <summary>
        /// base64编码
        /// </summary>
        /// <param name="str">待编码的字符串</param>
        /// <returns>编码后的字符串</returns>
        public static string Base64_Encode(string str)
        {
            byte[] encbuff = Encoding.UTF8.GetBytes(str);
            return Convert.ToBase64String(encbuff);
        }

        /// <summary>
        /// base64解码
        /// </summary>
        /// <param name="str">待解码的字符串</param>
        /// <returns>解码后的字符串</returns>
        public static string Base64_Decode(string str)
        {
            byte[] decbuff = Convert.FromBase64String(str);
            return Encoding.UTF8.GetString(decbuff);
        }



        #endregion

        #region 文件加密、解密


        /// <summary>
        /// 文件加密
        /// </summary>
        /// <param name="inFile"></param>
        /// <param name="outFile"></param>
        /// <param name="password"></param>
        public static void EncryptFile(string inFile, string outFile, string password)
        {
            using (FileStream inFileStream = File.OpenRead(inFile), outFileStream = File.Open(outFile, FileMode.OpenOrCreate))
            using (SymmetricAlgorithm algorithm = CreateRijndael(password, Salt))
            {
                algorithm.IV = Iv;
                using (CryptoStream cryptoStream = new CryptoStream(outFileStream, algorithm.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    byte[] bytes = new byte[BufferSize];
                    int readSize = -1;
                    while ((readSize = inFileStream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        cryptoStream.Write(bytes, 0, readSize);
                    }
                    cryptoStream.Flush();
                }
            }
        }

        /// <summary>
        /// 文件解密
        /// </summary>
        /// <param name="inFile"></param>
        /// <param name="outFile"></param>
        /// <param name="password"></param>
        public static void DecryptFile(string inFile, string outFile, string password)
        {
            using (FileStream inFileStream = File.OpenRead(inFile), outFileStream = File.OpenWrite(outFile))
            using (SymmetricAlgorithm algorithm = CreateRijndael(password, Salt))
            {
                algorithm.IV = Iv;
                using (CryptoStream cryptoStream = new CryptoStream(inFileStream, algorithm.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    byte[] bytes = new byte[BufferSize];
                    int readSize = -1;
                    int numReads = (int)(inFileStream.Length / BufferSize);
                    int slack = (int)(inFileStream.Length % BufferSize);
                    for (int i = 0; i < numReads; ++i)
                    {
                        readSize = cryptoStream.Read(bytes, 0, bytes.Length);
                        outFileStream.Write(bytes, 0, readSize);
                    }
                    if (slack > 0)
                    {
                        readSize = cryptoStream.Read(bytes, 0, (int)slack);
                        outFileStream.Write(bytes, 0, readSize);
                    }
                    outFileStream.Flush();
                }
            }
        }


        #endregion

        #region 文件转换流
        /// <summary>
        /// 文件转换成Base64字符串
        /// </summary>
        /// <param name="fileName">文件绝对路径</param>
        /// <returns></returns>
        public static String FileToBase64(string fileName)
        {
            string strRet = null;

            try
            {
                FileStream fs = new FileStream(fileName, FileMode.Open);
                byte[] bt = new byte[fs.Length];
                fs.Read(bt, 0, bt.Length);
                strRet = Convert.ToBase64String(bt);
                fs.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strRet;
        }

        /// <summary>
        /// Base64字符串转换成文件
        /// </summary>
        /// <param name="strInput">base64字符串</param>
        /// <param name="fileName">保存文件的绝对路径</param>
        /// <returns></returns>
        public static bool Base64ToFileAndSave(string strInput, string fileName)
        {
            bool bTrue = false;

            try
            {
                byte[] buffer = Convert.FromBase64String(strInput);
                FileStream fs = new FileStream(fileName, FileMode.CreateNew);
                fs.Write(buffer, 0, buffer.Length);
                fs.Close();
                bTrue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bTrue;
        }
        #endregion

        #region 获取订单号
        /// <summary>
        /// 微信订单单号
        /// </summary>
        /// <returns></returns>
        public static string OrderCode()
        {
            string RandomCode = DateTime.Now.ToString("yyMMddHHmmss");

            for (int i = 0; i < 6; i++)
            {
                //获取随机数的方法
                Random rand = new Random();

                int RandKey = rand.Next(0, 10);
                RandomCode += RandKey.ToString();
            }
            return RandomCode;
        }
        public static string GetOrderNumber()
        {
            string Number = DateTime.Now.ToString("yyMMddHHmmss");
            return Number + Next(1000, 1).ToString();
        }
        private static int Next(int numSeeds, int length)
        {
            byte[] buffer = new byte[length];
            System.Security.Cryptography.RNGCryptoServiceProvider Gen = new System.Security.Cryptography.RNGCryptoServiceProvider();
            Gen.GetBytes(buffer);
            uint randomResult = 0x0;
            for (int i = 0; i < length; i++)
            {
                randomResult |= ((uint)buffer[i] << ((length - 1 - i) * 8));
            }
            return (int)(randomResult % numSeeds);
        }
        #endregion
        #region 获取租户号
        /// <summary>
        /// 租户号
        /// </summary>
        /// <returns></returns>
        public static string TenantCode()
        {
            string RandomCode = DateTime.Now.ToString("YYMMdd");

            for (int i = 0; i < 2; i++)
            {
                //获取随机数的方法
                Random rand = new Random();
                int RandKey = rand.Next(0, 10);
                RandomCode += RandKey.ToString();
            }
            return RandomCode;
        }

        #endregion
    }

}