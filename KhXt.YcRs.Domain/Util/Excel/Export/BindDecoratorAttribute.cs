﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class BindDecoratorAttribute : Attribute
    {
        public BindDecoratorAttribute(Type decoratorType)
        {
            this.DecoratorType = decoratorType;
        }

        public Type DecoratorType { get; set; }
    }
}
