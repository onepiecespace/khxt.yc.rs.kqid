﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public interface IDecorator
    {
        IWorkbook Decorate(IWorkbook workbook, DecoratorContext context);
    }
}
