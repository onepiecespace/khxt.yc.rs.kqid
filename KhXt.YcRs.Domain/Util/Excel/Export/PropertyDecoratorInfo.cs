﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class PropertyDecoratorInfo
    {
        public int ColIndex { get; set; }
        public List<BaseDecorateAttribute> DecoratorAttrs { get; set; }
    }
}
