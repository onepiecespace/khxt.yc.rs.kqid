﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class PropertyFilterInfo
    {
        public int ColIndex { get; set; }
        public List<BaseFilterAttribute> FilterAttrs { get; set; }
    }
}
