﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class DatabaseFilterContext
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string Value { get; set; }
    }
}
