﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class ExcelHeaderRow
    {
        public List<ExcelCol> Cells { get; set; } = new List<ExcelCol>();
    }
}
