﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class ExcelCol
    {
        public int ColIndex { get; set; }
        public string ColName { get; set; }
    }
}
