﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 文件存储类
    /// </summary>
    public class ItemFilelogInput
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }

        public string PhysicalFilePath { get; set; }

        public int CreateId { get; set; }
        public DateTime CreateTime { get; set; }

    }
}
