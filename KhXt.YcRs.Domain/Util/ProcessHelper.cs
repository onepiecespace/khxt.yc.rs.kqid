﻿using KhXt.YcRs.Domain.Services;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class ProcessHelper : ServiceBase
    {
        private readonly string _ffMepgProcessName;

        public ProcessHelper()
        {
            _ffMepgProcessName = DomainSetting.FFmpegPath;
        }
        public void DoProcessing(string param, ProcessType processType)
        {
            Process process = new Process();
            try
            {
                var processName = "";
                if (processType == ProcessType.FFmpeg)
                    processName = _ffMepgProcessName;
                process.StartInfo.FileName = processName;
                process.StartInfo.Arguments = param;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardError = true;

                process.ErrorDataReceived += (sender, args) =>
                     {
                         if (sender is Process p && p.HasExited && p.ExitCode == 1)
                         {
                             Console.WriteLine("have an error：" + args.Data);
                         }
                     };
                process.Start();
                process.BeginErrorReadLine();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("调用FFmepg");
                throw;
            }
            finally
            {
                process.Close();
                process.Dispose();
            }
        }
    }
}
