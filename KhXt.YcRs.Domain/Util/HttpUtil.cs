﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class HttpUtil
    {
        public static string PostRequest(string url, byte[] bData)
        {
            System.Net.HttpWebRequest hwRequest;
            System.Net.HttpWebResponse hwResponse;

            string strResult = string.Empty;
            try
            {
                //获取上面的URL链接
                hwRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                //设置超时时间
                hwRequest.Timeout = 30000;
                //发送请求为POST
                hwRequest.Method = "POST";
                hwRequest.ContentType = " application/json";
                //bData的长度（也就是获取用户名，密码，手机号，apikey，短信内容，编码格式总长度）
                hwRequest.ContentLength = bData.Length;
                //发送
                System.IO.Stream smWrite = hwRequest.GetRequestStream();
                smWrite.Write(bData, 0, bData.Length);
                //释放资源
                smWrite.Close();
            }
            catch (System.Exception err)
            {
                WriteErrLog(err.ToString());
                return strResult;
            }

            //get response
            try
            {
                //使用hwResponse来获取数据
                hwResponse = (HttpWebResponse)hwRequest.GetResponse();
                StreamReader srReader = new StreamReader(hwResponse.GetResponseStream(), Encoding.ASCII);
                strResult = srReader.ReadToEnd();
                srReader.Close();
                hwResponse.Close();
            }
            catch (System.Exception err)
            {
                WriteErrLog(err.ToString());
            }

            return strResult;
        }

        public static string PostUrl(string url, string postData)
        {
            string result = "";

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

            req.Method = "POST";

            req.Timeout = 10000;//设置请求超时时间，单位为毫秒

            req.ContentType = "application/json";

            byte[] data = Encoding.UTF8.GetBytes(postData);

            req.ContentLength = data.Length;

            using (Stream reqStream = req.GetRequestStream())
            {
                reqStream.Write(data, 0, data.Length);

                reqStream.Close();
            }

            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            Stream stream = resp.GetResponseStream();

            //获取响应内容
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetLocalIp()
        {
            var strHostName = Dns.GetHostName(); //得到本机的主机名
            var ipEntry = Dns.GetHostAddresses(strHostName); //取得本机IP
            if (ipEntry.Length < 1)
                return "127.0.0.1";
            var first = ipEntry.FirstOrDefault(t => t.AddressFamily.ToString().Equals("InterNetwork"));

            if (first == null) return "127.0.0.1";
            var strAdd = first.ToString();
            return strAdd;
        }
        public static void WriteErrLog(string strErr)
        {
            Console.WriteLine(strErr);
            System.Diagnostics.Trace.WriteLine(strErr);
        }
    }
}
