﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 闪验后端服务本机号校验接口V2请求方法
    /// </summary>
    public class FlashValidateHelper
    {




        public static string mobilevalidate(string token, string mobile, PlatFormType os, ShanYanSetting sysitting)
        {
            string appId = ""; //应用APPID
            string appKey = "";  //应用APPKEY
                                 // string outId = "";//客户流水号，可以不传，不传则不用拼接签名
                                 //string mobile = "";//待校验的手机号码
                                 //string outId = "";
                                 //string clientIp = "";
                                 //string encryptType = "0";
            string VALIDATE_API_URL = sysitting.apiurl + "/mobile-validate";
            switch ((int)os)
            {
                case 1://ios
                    appId = sysitting.iosappId;
                    appKey = sysitting.iosappKey;
                    break;
                case 2:
                    appId = sysitting.adappId;
                    appKey = sysitting.adappKey;
                    break;
                default:
                    break;
            }
            // 闪验sdk返回的参数
            IDictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("appId", appId);
            dic.Add("token", token); //SDK返回的token
            dic.Add("mobile", mobile); //待校验的手机号码
            //dic.Add("outId", outId); //客户流水号，可空


            //拼接请求参数，如不传字段可以不拼接进来，按字段名正序排列
            string requestStr = "appId" + appId + "mobile" + mobile + "token" + token;
            //HmacSHA256签名
            string sign = HmacSHA256(requestStr, appKey);
            dic.Add("sign", sign);

            //校验结果，1校验成功，是本机号  0不是本机号
            string isVerify = invokeApi(VALIDATE_API_URL, dic, appKey);
            Console.WriteLine("校验结果:" + isVerify);
            Console.ReadLine();
            return isVerify;

        }


        static string invokeApi(string VALIDATE_API_URL, IDictionary<string, string> dic, string appKey)
        {

            // 1.调用api
            Console.WriteLine(VALIDATE_API_URL);
            string result = FlashHttpUtility.Post(VALIDATE_API_URL, dic);
            Console.WriteLine(result);

            JObject jsonObject = string.IsNullOrEmpty(result) ? null : (JObject)JsonConvert.DeserializeObject(result);

            // 2.处理返回结果
            if (jsonObject != null)
            {
                //响应code码。200000：成功，其他失败
                string code = jsonObject["code"].ToString();
                if ("200000".Equals(code) && null != jsonObject["data"])
                {
                    // 调用成功
                    // 解析结果数据，进行业务处理
                    // 检测结果
                    Console.WriteLine("调用成功,data:" + jsonObject["data"].ToString());

                    //校验结果，1校验成功，是本机号  0不是本机号
                    string isVerify = jsonObject["data"]["isVerify"].ToString();

                    return isVerify;

                }
                else
                {
                    // 记录错误日志，正式项目中请换成log打印
                    Console.WriteLine("调用失败,code:" + code + ",msg:" + jsonObject["message"]);
                }
            }
            return "0";
        }

        public static string HmacSHA256(string message, string secret)
        {
            var encoding = new System.Text.UTF8Encoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < hashmessage.Length; i++)
                {
                    builder.Append(hashmessage[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }


    }
}
