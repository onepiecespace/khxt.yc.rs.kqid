﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Util
{
    public class APIErroTools
    {
        public static string GetErrorName(int errorCode)
        {
            string errorName = string.Empty;
            switch (errorCode)
            {
                case 90001: errorName = "签名错误"; break;
                case 90002: errorName = "未提交任何参数"; break;
                case 90003: errorName = "未传入API版本号"; break;
                case 90004: errorName = "未传入API签名"; break;
                case 10001: errorName = "未传入商家ID"; break;
                case 10002: errorName = "未传入账号"; break;
                case 10003: errorName = "未传入密码"; break;
                case 10004: errorName = "账号或者密码错误"; break;
                case 10005: errorName = "请至少传入商品编号、商品名称、商品条形码、商品串号中任一参数"; break;
                case 10006: errorName = "未传入商品ID"; break;
                case 10007: errorName = "未传入商品识别串号"; break;
                case 10008: errorName = "未传入传递类型"; break;
                case 10009: errorName = "未传入商品ID或者仓库ID或者商品数量"; break;
                case 10010: errorName = "未传入订单明细ID"; break;
                case 10011: errorName = "未传入订单ID"; break;
                case 10012: errorName = "未传入商家商品ID"; break;
                case 10013: errorName = "不存在此商品数据"; break;
                case 10014: errorName = "未传入手机号码"; break;
                case 10015: errorName = "未传入短信业务类型"; break;
                case 10016: errorName = "未传入区域ID"; break;
                case 10017: errorName = "未传入商家商品列表所有参数"; break;
                case 10018: errorName = "未传入来源平台"; break;
                case 10019: errorName = "未传入短信验证码"; break;
                case 10020: errorName = "未传入图形验证码"; break;
                case 10021: errorName = "请传入查询订单的天数"; break;
                case 10022: errorName = "请传入用户ID"; break;
                case 10023: errorName = "请传入收藏ID"; break;
                case 10024: errorName = "请传入地址ID"; break;
                case 10025: errorName = "请传入配送方式ID"; break;
                case 10026: errorName = "请传入支付方式ID"; break;
                case 10027: errorName = "请传入收货地址"; break;
                case 10028: errorName = "请传入收货人姓名"; break;
                case 10029: errorName = "请传入城市ID"; break;
                case 10030: errorName = "请传入省ID"; break;
                case 10031: errorName = "用户ID格式错误"; break;
                case 10032: errorName = "任务状态ID格式错误"; break;
                case 10033: errorName = "请传入分配人"; break;
                case 10034: errorName = "批次号ID格式不正确"; break;
                case 10035: errorName = "请传入分拣人员"; break;
                case 10036: errorName = "请传入分拣数量"; break;
                case 10037: errorName = "批次明细ID格式不正确"; break;
                case 10038: errorName = "分拣人员和分拣数量队列数不匹配"; break;
                case 10039: errorName = "未传入任务ID"; break;
                case 10040: errorName = "未传入货位名称"; break;
                case 10041: errorName = "未传入商品条形码或唯一识别码"; break;
                case 10042: errorName = "未传入拣货数量"; break;
                case 10043: errorName = "未传入异常内容"; break;
                case 10044: errorName = "未传入异常商品数量"; break;
                case 10045: errorName = "未传入异常处理说明"; break;
                case 10046: errorName = "不存在此商家"; break;
                case 10047: errorName = "设置MemoCache失败"; break;
                case 10048: errorName = "未传入CookieName"; break;
                case 10049: errorName = "验证失败"; break;
                case 10050: errorName = "未传入法人姓名"; break;
                case 10051: errorName = "未传入商家名称"; break;
                case 10052: errorName = "未传入身份证号"; break;
                case 10053: errorName = "未传入身份证号照片凭证"; break;
                case 10054: errorName = "未传入营业执照编号"; break;
                case 10055: errorName = "未传入营业执照照片凭证"; break;
                case 10056: errorName = "申请资质认证ID错误"; break;
                case 10057: errorName = "购买数量必须大于1"; break;
                case 10058: errorName = "当前集采活动不存在"; break;
                case 10059: errorName = "不存在此用户"; break;
                case 10060: errorName = "你的用户等级不支持本次集采活动"; break;
                case 10061: errorName = "你集采购买的数量已经超过本次集采活动的最大数量"; break;
                case 20001: errorName = "预约单插入失败"; break;
                case 20002: errorName = "不存在商家数据"; break;
                case 20003: errorName = "不存在仓库数据"; break;
                case 20004: errorName = "该订单不存在"; break;
                case 20005: errorName = "更改ERP订单状态失败"; break;
                case 20006: errorName = "订单取消失败"; break;
                case 20007: errorName = "不存在该用户银行卡信息"; break;
                case 20008: errorName = "提现单创建失败"; break;
                case 20009: errorName = "银行卡信息创建失败"; break;
                case 20010: errorName = "未传入银行卡号"; break;
                case 20011: errorName = "未传入银行名称"; break;
                case 20012: errorName = "未传入银行账号名称"; break;
                case 20013: errorName = "创建订单评论失败"; break;
                case 20014: errorName = "无此订单评论"; break;
                case 20015: errorName = "该用户无评论"; break;
                case 20016: errorName = "本月已提现"; break;
                case 20017: errorName = "您未通过企业资质审核，请尽快认证企业信息"; break;
                case 20018: errorName = "未传入位置"; break;
                case 20019: errorName = "云仓ID不能为空"; break;
                case 20020: errorName = "无此云仓信息"; break;
                case 20021: errorName = "云仓ID为空"; break;
                case 20022: errorName = "未传入联系电话"; break;
                case 20023: errorName = "申请仓库预约失败"; break;
                case 20024: errorName = "未传入支付凭证照片"; break;
                case 20025: errorName = "上传图片失败"; break;
                case 20026: errorName = "请上传企业门头照片"; break;
                case 20027: errorName = "数据插入失败"; break;
                case 20028: errorName = "未传入手持照片"; break;
                case 20029: errorName = "更新数据失败"; break;
                case 20030: errorName = "未传入姓名"; break;
                case 20031: errorName = "该用户未成为推广员"; break;
                case 20032: errorName = "合同生成失败"; break;
                case 20033: errorName = "合同ID为空"; break;
                case 20034: errorName = "数据产品详情为空"; break;
                case 20035: errorName = "签章数据为空"; break;
                case 20036: errorName = "寻货需求ID为空"; break;
                case 20037: errorName = "签章类型为空"; break;
                case 20038: errorName = "未传入截止时间"; break;
                case 20039: errorName = "截止时间应大于当天时间"; break;
                case 20040: errorName = "无支付凭证照片"; break;
                case 20041: errorName = "未传入平台ID"; break;
                case 20042: errorName = "该用户未企业认证"; break;
                case 20043: errorName = "未传入企业审核ID"; break;
                case 20044: errorName = "未传入授权书图片"; break;
                case 20045: errorName = "未传入手持照图片"; break;
                case 20046: errorName = "未传入邮件地址"; break;
                case 20047: errorName = "请传入搜索商品的筛选条件"; break;
                case 20048: errorName = "您不符合集采活动参与的限制"; break;
                case 20049: errorName = "该用户未开户"; break;
                case 20050: errorName = "未传入发票类型"; break;
                case 20051: errorName = "未传入渠道账户类型"; break;
                case 20052: errorName = "未传入用户真实姓名"; break;
                case 20053: errorName = "您还没有上传门头照片，门头照片需包含法人合影"; break;
                case 20054: errorName = "您还没有填写公司地址，地址需与营业执照一致"; break;
                case 20055: errorName = "超过可提现金额"; break;
                case 20056: errorName = "请登录后再进行企业认证提交"; break;
                case 20057: errorName = "请传入定位的城市信息"; break;
                case 20058: errorName = "金额不合法"; break;
                case 20059: errorName = "订单不存在或信息有误"; break;
                case 20060: errorName = "业务请求流水为空"; break;
                case 20061: errorName = "短信验证码错误"; break;
                case 20062: errorName = "未传入企业信息ID"; break;
                case 20063: errorName = "未传入操作类型"; break;
                case 20064: errorName = "旧密码错误"; break;
                case 20065: errorName = "密码必须大于6位"; break;
                case 20066: errorName = "未传入资金所属类型"; break;
                case 20067: errorName = "未传入账户手机号"; break;
                case 20068: errorName = "未传入绑卡类型"; break;
                case 20069: errorName = "未传入页面类型"; break;
                case 20070: errorName = "未传入绑卡ID"; break;
                case 20071: errorName = "未传入银行预留手机号"; break;
                case 20072: errorName = "未传入页面跳转地址"; break;
                case 20073: errorName = "未传入区域省ID"; break;
                case 20074: errorName = "未传入区域区ID"; break;
                case 20075: errorName = "未传入短信验证码ID"; break;
                case 20076: errorName = "未传入唯一标识类型"; break;
                case 20077: errorName = "未传入唯一标识"; break;
                case 20078: errorName = "入账信息不合法"; break;
                case 20079: errorName = "该卡已绑定，请选择其他卡"; break;
                case 20080: errorName = "该订单已支付，无法切换支付方式"; break;
                case 20081: errorName = "创建苏宁订单失败"; break;
                case 20082: errorName = "未传入快递公司"; break;
                case 20083: errorName = "未传入快递单号"; break;
                case 20084: errorName = "请传入优惠券ID"; break;
                case 20085: errorName = "请传入优惠券号"; break;
                case 20086: errorName = "未获取到优惠券信息"; break;
                case 20087: errorName = "优惠券已领取完"; break;
                case 20088: errorName = "已超过每人限领次数"; break;
                case 20089: errorName = "有未处理完成的售后订单，不能提交确认收货"; break;
                case 20200: errorName = "不是有效商户"; break;
                case 20201: errorName = "商品库存不足"; break;
                case 30001: errorName = "未传入消息类型"; break;
                case 30002: errorName = "未传入商品数量"; break;
                case 30003: errorName = "传入的商家商品ID和商家商品数量不一致"; break;
                case 30004: errorName = "您所在的地区暂不支持线下支付，请更换其他支付方式。"; break;

            }
            return errorName;
        }



        public static string GetErrorNameByPayMent(int errorCode)
        {
            string errorName = string.Empty;
            switch (errorCode)
            {
                case 90001: errorName = "签名错误"; break;
                case 90002: errorName = "未提交任何参数"; break;
                case 90003: errorName = "未传入API版本号"; break;
                case 90004: errorName = "未传入API签名"; break;
                case 10001: errorName = "订单信息错误"; break;
                case 10002: errorName = "订单未在线支付"; break;
                case 10003: errorName = "订单已支付"; break;
                case 10004: errorName = "订单无支付金额"; break;
            }
            return errorName;
        }
    }
}
