﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 租户号生成
    /// </summary>
    public class TenantsGenerator
    {

        /// <summary>
        /// 生成8位唯一的数字 并发可用
        /// </summary>
        /// <returns></returns>
        public static long GenerateTenantId()
        {
            System.Threading.Thread.Sleep(1); //保证yyyyMMddHHmmssffff唯一
            string TenantCode = string.Format("{0}", EncryptionUtility.TenantCode());
            //var buffer = Guid.NewGuid().ToByteArray();
            var TenantId = Convert.ToInt64(TenantCode);
            return TenantId;
        }
    }
}
