﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 美联软通短信接口帮助类
    /// </summary>
    [Obsolete]
    public class SMSHelper
    {
        /// <summary>
        /// POST方式发送得结果
        /// </summary>
        /// <param name="url"></param>
        /// <param name="bData"></param>
        /// <returns></returns>
        public static string PostRequest(string url, byte[] bData)
        {
            System.Net.HttpWebRequest hwRequest;
            System.Net.HttpWebResponse hwResponse;

            string strResult = string.Empty;
            try
            {
                //获取上面的URL链接
                hwRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                //设置超时时间
                hwRequest.Timeout = 5000;
                //发送请求为POST
                hwRequest.Method = "POST";
                hwRequest.ContentType = "application/x-www-form-urlencoded";
                //bData的长度（也就是获取用户名，密码，手机号，apikey，短信内容，编码格式总长度）
                hwRequest.ContentLength = bData.Length;
                //发送
                System.IO.Stream smWrite = hwRequest.GetRequestStream();
                smWrite.Write(bData, 0, bData.Length);
                //释放资源
                smWrite.Close();
            }
            catch (Exception ex)
            {
                DomainEnv.CommonLogger.Error("Sms", ex);
                return strResult;
            }

            //get response
            try
            {
                //使用hwResponse来获取数据
                hwResponse = (HttpWebResponse)hwRequest.GetResponse();
                StreamReader srReader = new StreamReader(hwResponse.GetResponseStream(), Encoding.ASCII);
                strResult = srReader.ReadToEnd();
                srReader.Close();
                hwResponse.Close();
            }
            catch (Exception ex)
            {
                DomainEnv.CommonLogger.Error("Sms", ex);
            }

            return strResult;
        }
        /// <summary>
        /// URL加密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string UrlEncode(string str)
        {
            StringBuilder sb = new StringBuilder();
            byte[] byStr = System.Text.Encoding.UTF8.GetBytes(str); //默认是System.Text.Encoding.Default.GetBytes(str)
            for (int i = 0; i < byStr.Length; i++)
            {
                sb.Append(@"%" + Convert.ToString(byStr[i], 16));
            }

            return (sb.ToString());
        }
    }
}
