﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace KhXt.YcRs.Domain
{
    /// <summary>
    /// Json/Obj实体互转工具类
    /// </summary>
    public class JsonUtil
    {
        /// <summary>
        /// 对象转换为JSON
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetJson<T>(T obj)
        {
            //记住 添加引用 System.ServiceModel.Web 
            /**
             * 如果不添加上面的引用,System.Runtime.Serialization.Json; Json是出不来的哦
             * */
            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream())
            {
                json.WriteObject(ms, obj);
                string szJson = Encoding.UTF8.GetString(ms.ToArray());
                return szJson;
            }
        }
        /// <summary>
        /// 把JSON字符串还原为对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="szJson">JSON字符串</param>
        /// <returns>对象实体</returns>
        public static T ParseFormJson<T>(string szJson)
        {
            T obj = Activator.CreateInstance<T>();
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(szJson)))
            {
                DataContractJsonSerializer dcj = new DataContractJsonSerializer(typeof(T));
                return (T)dcj.ReadObject(ms);
            }
        }
        /// <summary>
        /// 去除json key双引号
        /// </summary>
        /// <param name="jsonInput">json</param>
        /// <returns>去除key引号</returns>
        public static string JsonRegex(string jsonInput)
        {
            string result = string.Empty;
            try
            {
                string pattern = "\"(\\w+)\"(\\s*:\\s*)";
                string replacement = "$1$2";
                System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(pattern);
                result = rgx.Replace(jsonInput, replacement);
            }
            catch (Exception ex)
            {
                result = jsonInput;
            }
            return result;
        }
    }
}
