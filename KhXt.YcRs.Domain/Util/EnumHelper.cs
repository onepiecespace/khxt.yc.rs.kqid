﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public static class EnumHelper
    {
        /// <summary>
        /// 从枚举中获取Description
        /// </summary>
        /// <param name="enumName">需要获取枚举描述的枚举</param>
        /// <returns>描述内容</returns>
        public static string GetDescription(Enum enumName)
        {
            var description = string.Empty;
            var fieldInfo = enumName.GetType().GetField(enumName.ToString());
            var attributes = GetDescriptAttr(fieldInfo);
            if (attributes != null && attributes.Length > 0)
            {
                description = attributes[0].Description;
            }
            else
            {
                description = enumName.ToString();
            }
            return description;
        }

        /// <summary>
        /// 获取字段Description
        /// </summary>
        /// <param name="fieldInfo">FieldInfo</param>
        /// <returns>DescriptionAttribute[] </returns>
        private static DescriptionAttribute[] GetDescriptAttr(FieldInfo fieldInfo)
        {
            if (fieldInfo != null)
            {
                return (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            }
            return null;
        }

        /// <summary>
        /// 获取枚举所有名称
        /// </summary>
        /// <param name="enumType">枚举类型typeof(T)</param>
        /// <returns>枚举名称列表</returns>
        public static List<string> GetEnumNames(Type enumType)
        {
            return Enum.GetNames(enumType).ToList();
        }

        /// <summary>
        /// 获取所有枚举对应的值
        /// </summary>
        /// <param name="enumType">枚举类型typeof(T)</param>
        /// <returns>枚举值列表</returns>
        public static List<int> GetEnumValues(Type enumType)
        {
            var list = new List<int>();
            foreach (var value in Enum.GetValues(enumType))
            {
                list.Add(Convert.ToInt32(value));
            }
            return list;
        }

        /// <summary>
        /// 获取枚举名以及对应的Description
        /// </summary>
        /// <param name="type">枚举类型typeof(T)</param>
        /// <returns>返回Dictionary  ,Key为枚举名，  Value为枚举对应的Description</returns>
        public static Dictionary<object, object> GetNameAndDescriptions(Type type)
        {
            if (type.IsEnum)
            {
                var dic = new Dictionary<object, object>();
                var enumValues = Enum.GetValues(type);
                foreach (Enum value in enumValues)
                {
                    dic.Add(value, GetDescription(value));
                }
                return dic;
            }
            return null;
        }

        public static Enum GetEnumFromStr<T>(string name)
        {
            return (Enum)Enum.Parse(typeof(T), name);
        }

        public static int GetEnumValue(Type enumType, string enumName)
        {
            if (!enumType.IsEnum)
                throw new ArgumentException("enumType必须是枚举类型");
            var values = Enum.GetValues(enumType);
            var ht = new Hashtable();
            foreach (var val in values)
            {
                ht.Add(Enum.GetName(enumType, val) ?? throw new InvalidOperationException(), val);
            }
            return (int)ht[enumName];
        }
        //根据数值获取枚举的Name

        public static string GetEnumNameByKey(Type enumType, int key)
        {
            return Enum.GetName(enumType, key);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetDescription<T>(string name)
        {
            return GetDescription(GetEnumFromStr<T>(name));
        }

    }
}
