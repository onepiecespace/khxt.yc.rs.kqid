﻿using Hx.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="C"></typeparam>
    public class Compare<T, C> : IEqualityComparer<T>
    {
        private Func<T, C> _getField;
        public Compare(Func<T, C> getfield)
        {
            this._getField = getfield;
        }
        public bool Equals(T x, T y)
        {
            return EqualityComparer<C>.Default.Equals(_getField(x), _getField(y));
        }
        public int GetHashCode(T obj)
        {
            return EqualityComparer<C>.Default.GetHashCode(this._getField(obj));
        }
    }

    public static class CommonHelper
    {
        /// <summary>
        /// 自定义Distinct扩展方法
        /// </summary>
        /// <typeparam name="T">要去重的对象类</typeparam>
        /// <typeparam name="C">自定义去重的字段类型</typeparam>
        /// <param name="source">要去重的对象</param>
        /// <param name="getfield">获取自定义去重字段的委托</param>
        /// <returns></returns>
        public static IEnumerable<T> MyDistinct<T, C>(this IEnumerable<T> source, Func<T, C> getfield)
        {
            return source.Distinct(new Compare<T, C>(getfield));
        }

        public static string Sha1(string content)
        {
            return Sha1(content, Encoding.UTF8);
        }

        public static string Sha1(string content, Encoding encode)
        {
            try
            {
                var sha1 = new SHA1CryptoServiceProvider();//创建SHA1对象
                var bytes_in = encode.GetBytes(content);//将待加密字符串转为byte类型
                var bytes_out = sha1.ComputeHash(bytes_in);//Hash运算
                sha1.Dispose();//释放当前实例使用的所有资源
                var result = BitConverter.ToString(bytes_out);//将运算结果转为string类型
                result = result.Replace("-", "");//替换并转为大写
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
        }

        /// <summary>
        /// 获取Dictionary中不重复的随机Dictionary
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dict">原dictionary</param>
        /// <param name="count">返回随机个数(如果dict总个数少于count 则返回dict总个数)</param>
        /// <returns></returns>
        public static Dictionary<TKey, TValue> RandomValues<TKey, TValue>(Dictionary<TKey, TValue> dict, int count)
        {
            Random rand = new Random();
            Dictionary<TKey, TValue> dic = new Dictionary<TKey, TValue>();
            int size = dict.Count;
            count = count > size ? size : count;
            List<TKey> values = Enumerable.ToList(dict.Keys);
            while (dic.Count < count)
            {
                TKey tk = values[rand.Next(size)];
                if (!dic.Keys.Contains(tk))
                {
                    dic[tk] = dict[tk];
                }
            }
            return dic;
        }
        /// <summary>
        /// 默认评论图
        /// </summary>
        public static Dictionary<int, string> headdic()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();
            dic.Add(1, "四叶草的小星星");
            dic.Add(2, "精灵之舞");
            dic.Add(3, "冬日暖阳");
            dic.Add(4, "猫扑风铃");
            dic.Add(5, "素锦流年");
            dic.Add(6, "花香和花");
            dic.Add(7, "柠檬绿");
            dic.Add(8, "樱桃");
            dic.Add(9, "柠檬乖乖");
            dic.Add(10, "草莓味的风");

            dic.Add(11, "光着脚丫子");
            dic.Add(12, "夏花冬雪");
            dic.Add(13, "櫻之舞");
            dic.Add(14, "港湾");
            dic.Add(15, "泡沫般梦幻");
            dic.Add(16, "刺眼的阳光");
            dic.Add(17, "棒棒冰");
            dic.Add(18, "自若清风");
            dic.Add(19, "暖心向阳");
            dic.Add(20, "聆听风音");
            dic.Add(21, "塔塔猫");

            dic.Add(22, "风雨桥上");
            dic.Add(23, "沫夏");
            dic.Add(24, "向日葵");
            dic.Add(25, "馋猫");
            dic.Add(26, "十里春风");
            dic.Add(27, "清晨的雨");
            dic.Add(28, "淡淡绿茶香");
            dic.Add(29, "初雪");
            dic.Add(30, "尒雨点");


            dic.Add(31, "空城");
            dic.Add(32, "花海");
            dic.Add(33, "半寸时光");
            dic.Add(34, "森树白云");
            dic.Add(35, "春风袅袅");
            dic.Add(36, "时光请留下我们");
            dic.Add(37, "北极星的泪");
            dic.Add(38, "无声飞雪");
            dic.Add(39, "月色真美");
            dic.Add(40, "悠悠桃花甜");

            dic.Add(41, "逆光飞翔");
            dic.Add(42, "清风与我");
            dic.Add(43, "余音");
            dic.Add(44, "惬意");
            dic.Add(45, "雨后玫瑰");
            dic.Add(46, "抹茶");
            dic.Add(47, "绝版小猫");
            dic.Add(48, "卡菲猫");
            dic.Add(49, "倾听");
            dic.Add(50, "初晴");
            return dic;
        }

        public static string HMACSHA256Sign(string value, string key)
        {
            var bkey = Encoding.UTF8.GetBytes(key);
            return HMACSHA256Sign(value, bkey);
        }
        public static string HMACSHA256Sign(string value, byte[] key)
        {
            using (var hmac = new HMACSHA256(key))
            {
                var bytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(value));
                var sb = new StringBuilder();
                bytes.ForEach(b =>
                {
                    sb.Append(string.Format("0:X2"));
                });
                return sb.ToString();
            }
        }
        public static string AesDecrypt(string data, string key)
        {
            return AesDecrypt(data, key, key);
        }

        public static string AesDecrypt(string data, string key, string iv)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new ArgumentException("data is empty.");
            }

            if (key == null || iv == null)
            {
                throw new ArgumentException("Key/Iv is null.");
            }

            byte[] bkey = System.Text.Encoding.UTF8.GetBytes(key);
            byte[] biv = System.Text.Encoding.UTF8.GetBytes(iv);

            using (var rijndaelManaged = new RijndaelManaged()
            {
                Key = bkey,
                IV = biv,
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            {
                using (var transform = rijndaelManaged.CreateDecryptor(bkey, biv))
                {
                    var inputBytes = StrToHexByte(data);
                    var encryptedBytes = transform.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
                    return Encoding.UTF8.GetString(encryptedBytes);
                }
            }
        }


        public static byte[] StrToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        public static string ToHexString(byte[] bytes)
        {
            string hexString = string.Empty;
            if (bytes != null)
            {
                StringBuilder strB = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    strB.Append(bytes[i].ToString("X2"));
                }
                hexString = strB.ToString();
            }
            return hexString;
        }


        public static int GetRandNum(int min, int max)
        {
            Random r = new Random(Guid.NewGuid().GetHashCode());
            return r.Next(min, max);
        }
        /// <summary>
        /// 获取时间戳
        /// </summary>
        /// <returns></returns>
        public static long GetTimeStamp()
        {
            TimeSpan ts = DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }

    }


}
