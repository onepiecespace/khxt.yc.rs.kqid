﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 卡号生成
    /// </summary>
    public class CardNumberGenerator
    {
        /// <summary>
        /// 卡号前缀
        /// </summary>
        public static string[] CardPrefixList = new[] { "1888" };
        private static string StrReversed(string str)
        {
            if (str == null)
                return "";
            var revStr = "";
            for (int i = str.Length - 1; i >= 0; i--)
            {
                revStr += str[i];
            }
            return revStr;
        }

        /*
        * 'prefix' is the start of the CC number as a string, any number of digits.
        * 'length' is the length of the CC number to generate. Typically 13 or 16
        */

        private static string CompletedNumber(string prefix, int length)
        {
            var number = prefix;
            // generate digits
            while (number.Length < (length - 1))
            {
                var rnd = (new Random().NextDouble() * 1.0f - 0f);
                number += Math.Floor(rnd * 10);
                Thread.Sleep(20);
            }
            // reverse number and convert to int
            var reversedCNumberings = StrReversed(number);
            var reversedCNumberList = new List<int>();
            foreach (var t in reversedCNumberings)
            {
                reversedCNumberList.Add(Convert.ToInt32(t.ToString()));
            }
            // calculate sum
            var sum = 0;
            var pos = 0;
            var reversedCNumber = reversedCNumberList.ToArray();
            while (pos < length - 1)
            {
                var odd = reversedCNumber[pos] * 2;
                if (odd > 9)
                {
                    odd -= 9;
                }
                sum += odd;
                if (pos != (length - 2))
                {
                    sum += reversedCNumber[pos + 1];
                }
                pos += 2;
            }
            // calculate check digit
            var checkGInt32 = Convert.ToInt32((Math.Floor((decimal)sum / 10) + 1) * 10 - sum) % 10;
            number += checkGInt32;
            return number;
        }

        private static string[] GenerateByPrefix(string[] prefixList, int length, int howMany)
        {
            var result = new Stack<string>();
            for (var i = howMany - 1; i >= 0; i--)
            {
                var r = new Random(Guid.NewGuid().GetHashCode());
                var next = r.Next(0, prefixList.Length - 1);
                var number = prefixList[next];
                result.Push(CompletedNumber(number, length));
            }
            return result.ToArray();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="howMany"></param>
        /// <returns></returns>
        public static string[] GenerateCardNumbers(int howMany)
        {
            return GenerateByPrefix(CardPrefixList, 16, howMany);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GenerateCardNumber()
        {
            return GenerateByPrefix(CardPrefixList, 16, 1)[0];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string Reverse(string str)
        {
            // convert the string to char array
            char[] charArray = str.ToCharArray();
            int len = str.Length - 1;
            /*
            now this for is a bit unconventional at first glance because there
            are 2 variables that we're changing values of: i++ and len--.
            the order of them is irrelevant. so basicaly we're going with i from 
            start to end of the array. with len we're shortening the array by one
            each time. this is probably understandable.
            */
            for (int i = 0; i < len; i++, len--)
            {
                /*
                now this is the tricky part people that should know about it don't.
                look at the table below to see what's going on exactly here.
                */
                charArray[i] ^= charArray[len];
                charArray[len] ^= charArray[i];
                charArray[i] ^= charArray[len];
            }
            return new string(charArray);
        }
        /// <summary>
        /// 验证卡号
        /// </summary>
        /// <param name="creditCardNumber"></param>
        /// <returns></returns>
        public static bool IsValidCardNumber(string creditCardNumber)
        {
            bool isValid = false;
            try
            {
                string reversedNumber = Reverse(creditCardNumber);
                int mod10Count = 0;
                for (int i = 0; i < reversedNumber.Length; i++)
                {
                    int augend = Convert.ToInt32(reversedNumber[i]);
                    if (((i + 1) % 2) == 0)
                    {
                        string productstring = (augend * 2).ToString();
                        augend = 0;
                        for (int j = 0; j < productstring.Length; j++)
                        {
                            augend += Convert.ToInt32(productstring[j]);
                        }
                    }
                    mod10Count += augend;
                }
                if ((mod10Count % 10) == 0)
                {
                    isValid = true;
                }
            }
            catch
            {
            }
            return isValid;
        }
        //数字转换为中文
        public static string GetCountRefundInfoInChanese(string inputNum)
        {
            string[] intArr = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
            string[] strArr = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九", };
            string[] Chinese = { "", "十", "百", "千", "万", "十", "百", "千", "亿" };
            //金额
            //string[] Chinese = { "元", "十", "百", "千", "万", "十", "百", "千", "亿" };
            char[] tmpArr = inputNum.ToString().ToArray();
            string tmpVal = "";
            for (int i = 0; i < tmpArr.Length; i++)
            {
                tmpVal += strArr[tmpArr[i] - 48];//ASCII编码 0为48
                tmpVal += Chinese[tmpArr.Length - 1 - i];//根据对应的位数插入对应的单位
            }

            return tmpVal;
        }
    }
}
