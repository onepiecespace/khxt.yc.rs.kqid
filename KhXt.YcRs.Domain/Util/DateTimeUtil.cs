﻿
using System;
using System.Globalization;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class DateTimeUtil
    {

        public static string GetTimeToFriendly(DateTime value)
        {
            StringBuilder sb = new StringBuilder();
            bool approximate = true;
            string suffix = (value > DateTime.Now) ? "刚刚" : "前";

            TimeSpan timeSpan = new TimeSpan(Math.Abs(DateTime.Now.Subtract(value).Ticks));

            if (timeSpan.Days > 0)
            {
                sb.AppendFormat("{0}{1}", timeSpan.Days,
                  (timeSpan.Days > 1) ? "天" : "天");
                if (approximate) return sb.ToString() + suffix;
            }
            if (timeSpan.Hours > 0)
            {
                sb.AppendFormat("{0}{1}{2}", (sb.Length > 0) ? ", " : string.Empty,
                  timeSpan.Hours, (timeSpan.Hours > 1) ? "小时" : "小时");
                if (approximate) return sb.ToString() + suffix;
            }
            if (timeSpan.Minutes > 0)
            {
                sb.AppendFormat("{0}{1}{2}", (sb.Length > 0) ? ", " : string.Empty,
                  timeSpan.Minutes, (timeSpan.Minutes > 1) ? "分钟" : "分钟");
                if (approximate) return sb.ToString() + suffix;
            }
            if (timeSpan.Seconds > 0)
            {
                sb.AppendFormat("{0}{1}{2}", (sb.Length > 0) ? ", " : string.Empty,
                  timeSpan.Seconds, (timeSpan.Seconds > 1) ? "秒" : "秒");
                if (approximate) return sb.ToString() + suffix;
            }
            if (sb.Length == 0) return "刚刚";

            sb.Append(suffix);
            return sb.ToString();
        }
        public static string GetChildTime(string str)
        {
            var rlttime = "";
            if (str.IndexOf(":") >= 0)
            {
                var time = str.Split(":");
                if (time.Length > 2)
                {

                    if (time[0][0] == '0')
                    {
                        if (time[0][1] == '0')
                        {
                            rlttime = time[1] + "分" + time[2] + "秒";// time[0].Replace("0", "") + "小时" +
                        }
                        else
                        {
                            rlttime = time[0].Replace("0", "") + "小时" + time[1] + "分" + time[2] + "秒";
                        }
                    }
                    else
                    {
                        rlttime = time[0] + "小时" + time[1] + "分" + time[2] + "秒";
                    }

                }
                else
                {
                    if (time[1][0] == '0')
                    {
                        rlttime = time[1] + "分" + time[2] + "秒";
                    }
                    else
                    {
                        rlttime = time[1] + "分" + time[2] + "秒";
                    }

                }
            }
            return rlttime;
        }

        /// <summary>
        /// DateTime时间格式转换为Unix时间戳格式
        /// </summary>
        /// <param name="dateTime"> DateTime时间格式</param>
        /// <returns>Unix时间戳格式</returns>
        public static long ConverToTimestamp(DateTime dateTime)
        {
            var tome = (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
            return (long)tome;

        }
        /// <summary>
        /// 获取时间戳
        /// </summary>
        /// <returns></returns>
        public static string GetTimeStamp(System.DateTime time)
        {
            long ts = ConvertDateTimeToInt(time);
            return ts.ToString();
        }

        /// <summary>
        /// 获取时间戳
        /// </summary>
        /// <returns></returns>
        public static string GetTimeStamp(System.DateTime time, int length = 13)
        {
            long ts = ConvertDateTimeToInt(time);
            return ts.ToString().Substring(0, length);
        }
        /// <summary> 
        /// 将c# DateTime时间格式转换为Unix时间戳格式 
        /// </summary> 
        /// <param name="time">时间</param> 
        /// <returns>long</returns> 
        public static long ConvertDateTimeToInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000;  //除10000调整为13位   
            return t;
        }
        /// <summary>    
        /// 时间戳转为C#格式时间    
        /// </summary>    
        /// <param name=”timeStamp”></param>    
        /// <returns></returns>    
        public static DateTime ConvertStringToDateTime(string timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000");
            TimeSpan toNow = new TimeSpan(lTime);
            return dtStart.Add(toNow);
        }

        /// <summary>
        /// 时间戳转为C#格式时间10位
        /// </summary>
        /// <param name="timeStamp">Unix时间戳格式</param>
        /// <returns>C#格式时间</returns>
        public static DateTime GetDateTimeFrom1970Ticks(long curSeconds)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            return dtStart.AddSeconds(curSeconds);
        }

        /// <summary>
        /// 验证时间戳
        /// </summary>
        /// <param name="time"></param>
        /// <param name="interval">差值（分钟）</param>
        /// <returns></returns>
        public static bool IsTime(long time, double interval)
        {
            DateTime dt = GetDateTimeFrom1970Ticks(time);
            //取现在时间
            DateTime dt1 = DateTime.Now.AddMinutes(interval);
            DateTime dt2 = DateTime.Now.AddMinutes(interval * -1);
            if (dt > dt2 && dt < dt1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 判断时间戳是否正确（验证前8位）
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static bool IsTime(string time)
        {
            string str = GetTimeStamp(DateTime.Now, 8);
            if (str.Equals(time))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}