﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    public class RESTfulPath
    {
        #region AdminAPI
        public const string NODE = "/";
        public const string STATUS = "/status";
        public const string CONSUMERS = "/consumers";
        public const string TAGS = "/tags";
        public const string PLUGINS = "/plugins";
        public const string SERVICES = "/services";
        public const string ROUTES = "/routes";
        public const string UPSTREAMS = "/upstreams";
        public const string TARGETS = "/targets";
        public const string CERTIFICATES = "/certificates";
        public const string SNIS = "/snis";
        #endregion
        #region mxhapi
        public const string mxhapi = "api";
        public const string mxhAd = "api/Ad/types";
        public const string mxhhome = "api/webhome";
        public const string mxhtest = "api/test";
        public const string mxhUser = "api/User";
        public const string MXHAPICOURSE = "api/Course";
        public const string mxhPay = "api/Pay";
        public const string mxhOrder = "api/Order";
        public const string mxhLeague = "api/League";
        #endregion
    }
}
