﻿namespace KhXt.YcRs.Domain.Util
{
    public enum RequestMethod
    {
        Get = 0,
        Post,
        Put,
        Delete,
        Patch
    }
}
