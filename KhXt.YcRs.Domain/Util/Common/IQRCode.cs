﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Domain.Util
{
    public interface IQRCode
    {
        Bitmap GetQRCode(string url, int pixel);
    }
}
