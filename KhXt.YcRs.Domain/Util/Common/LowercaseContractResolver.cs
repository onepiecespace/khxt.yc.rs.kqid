﻿using Newtonsoft.Json.Serialization;

namespace KhXt.YcRs.Domain.Util
{
    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }
}


