﻿namespace KhXt.YcRs.Domain
{
    public class ResultInfo
    {
        /// <summary>
        /// 返回code
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 返回消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 返回状态
        /// </summary>
        public bool State { get; set; }

        /// <summary>
        /// 返回自定义类型
        /// </summary>
        public object obj { get; set; }
    }
}
