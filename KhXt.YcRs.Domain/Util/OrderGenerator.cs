﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Util
{
    /// <summary>
    /// 订单号生成
    /// </summary>
    public class OrderGenerator
    {

        /// <summary>
        /// 生成22位唯一的数字 并发可用
        /// </summary>
        /// <returns></returns>
        public static string GenerateOrderId()
        {
            System.Threading.Thread.Sleep(1); //保证yyyyMMddHHmmssffff唯一
            var buffer = Guid.NewGuid().ToByteArray();
            var guidLongId = BitConverter.ToInt64(buffer, 0);
            return guidLongId.ToString();
        }
    }
}
