﻿using System;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain
{
    public class Consts
    {
        public const string SEPARATOR = "@@";


        /// <summary>
        /// 题目类型1单选2多选3填空4组词成语5对对碰
        /// </summary>
        public const int QUESTIONTYPE_TENANTID = 100;
        /// <summary>
        /// 获取错题本 PK 直播、背诵等
        /// </summary>
        public const int ERRQUESTYPE_TENANTID = 101;

        /// <summary>
        /// 获取APP 安卓最新现在连接
        /// </summary>
        public const int DOWNLOADURL_TENANTID = 103;

        /// <summary>
        /// 获取用户年级
        /// </summary>
        public const int USER_CLASS_TENANTID = 104;
        /// <summary>
        /// 获取APP 安卓应用宝最新现在连接
        /// </summary>
        public const int DOWNLOADURL_AndrodID = 105;
        /// <summary>
        /// 获取APP 苹果商店最新现在连接
        /// </summary>
        public const int DOWNLOADURL_IOSID = 106;
        /// <summary>
        ///缓存过期时间
        /// </summary>
        public const int CACHE_EXPIRE_TIME = 8;
        #region 评论相关的租户信息
        /// <summary>
        ///  获取朗读
        /// </summary>
        public const int COMMENT_READ_TENANTID = 1001;

        /// <summary>
        /// 课程评价详细页的评论内容租户ID
        /// </summary>
        public const int COMMENT_COURSE_DETAILED_TENANTID = 2001;
        #endregion

        public static Dictionary<int, long> DictationLevelAndExp = new Dictionary<int, long>
        {
            {1,100 },
            {2,230 },
            {3,500 },
            {4,1300 },
            {5,3300 }

        };


        public static Dictionary<int, Type> ServiceDictionary = new Dictionary<int, Type>
        {
            {0, null},
            {80, typeof(KhXt.YcRs.Domain.Services.Course.ICourseBuyCollectService)},
            {90, typeof(KhXt.YcRs.Domain.Services.Course.ICourseCateoryService)},
            {100, typeof(KhXt.YcRs.Domain.Services.Course.ICourseChildService)},
            {110, typeof(KhXt.YcRs.Domain.Services.Course.ICourseItemService)},
            {120, typeof(KhXt.YcRs.Domain.Services.Course.ICourseLogService)},
            {130, typeof(KhXt.YcRs.Domain.Services.Course.ICourseOrderService)},
            {140, typeof(KhXt.YcRs.Domain.Services.Course.ICourseService)},
            {150, typeof(KhXt.YcRs.Domain.Services.Course.ICourseUserItemService)},
            {200, typeof(KhXt.YcRs.Domain.Services.Files.IFilesService)},
            {230, typeof(KhXt.YcRs.Domain.Services.ICaptionService)},
            {250, typeof(KhXt.YcRs.Domain.Services.IContentService)},
            {260, typeof(KhXt.YcRs.Domain.Services.IKVStoreService)},
            {270, typeof(KhXt.YcRs.Domain.Services.ISmsService)},
            {300, typeof(KhXt.YcRs.Domain.Services.ITestService)},
            {350, typeof(KhXt.YcRs.Domain.Services.Manager.IManagerService)},
            {390, typeof(KhXt.YcRs.Domain.Services.Order.ICouponInfoService)},
            {400, typeof(KhXt.YcRs.Domain.Services.Order.IIOSPriceService)},
            {410, typeof(KhXt.YcRs.Domain.Services.Order.IOrderBusinessService)},
            {420, typeof(KhXt.YcRs.Domain.Services.Order.IOrderInfoService)},
            {430, typeof(KhXt.YcRs.Domain.Services.Pay.IWeChatPayService)},
            {460, typeof(KhXt.YcRs.Domain.Services.Product.IPro_attributeService)},
            {470, typeof(KhXt.YcRs.Domain.Services.Product.IPro_classService)},
            {480, typeof(KhXt.YcRs.Domain.Services.Product.IPro_listService)},
            {550, typeof(KhXt.YcRs.Domain.Services.Sys.ISysLogService)},
            {560, typeof(KhXt.YcRs.Domain.Services.Sys.ISysService)},
            {570, typeof(KhXt.YcRs.Domain.Services.Sys.IUser_ChannelService)},
            {580, typeof(KhXt.YcRs.Domain.Services.Sys.IVersionService)},
            {610, typeof(KhXt.YcRs.Domain.Services.User.IAddressService)},
            {620, typeof(KhXt.YcRs.Domain.Services.User.IFeedbackService)},
            {650, typeof(KhXt.YcRs.Domain.Services.User.IUserBankService)},
            {660, typeof(KhXt.YcRs.Domain.Services.User.IUserCacheService)},
            {670, typeof(KhXt.YcRs.Domain.Services.User.IUserLevelCacheService)},
            {680, typeof(KhXt.YcRs.Domain.Services.User.IUserService)},
            {690, typeof(KhXt.YcRs.Domain.Services.User.IUserThreePartyService)},
            {700, typeof(KhXt.YcRs.Domain.Services.User.IVersionApkService)},
            {710, typeof(KhXt.YcRs.Domain.Services.Vip.IUserVipService)},
            {720, typeof(KhXt.YcRs.Domain.Services.Vip.IVipInfoService)},
            {730, typeof(KhXt.YcRs.Domain.Services.Vip.IVipPrivilegeService)},
            {740, typeof(KhXt.YcRs.Domain.Services.Vip.IVipService)},
            {750, typeof(KhXt.YcRs.Domain.Services.VipCard.IVipCardHistoryService)},
            {760, typeof(KhXt.YcRs.Domain.Services.VipCard.IVipCardService)}
        };
    }

    /// <summary>
    /// 分页类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Paged<T>
    {
        public Paged(int total, List<T> data, int pageIndex, int pageSize)
        {
            Total = total;
            PageSize = pageSize;
            PageIndex = pageIndex;
            Data = data;
        }

        public int Total { get; }
        public int PageIndex { get; }
        public int PageSize { get; }
        public List<T> Data { get; }
    }

}
