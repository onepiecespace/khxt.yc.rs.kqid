﻿using System;
using System.ComponentModel;

namespace KhXt.YcRs.Domain
{
    [Flags]
    public enum ValueCategory
    {
        [Description("金币")]
        Coin = 1,

        [Description("经验")]
        Exp = 2
    }
}
