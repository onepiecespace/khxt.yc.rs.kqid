﻿using Hx.Dapper;
using KhXt.YcRs.Domain.Models.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.TypeHandlers
{
    public class OrgexhibitionTypeHandler : JsonTypeHandlerBase<List<Orgexhibition>> { }
    public class CoursecontentTypeHandler : JsonTypeHandlerBase<List<Coursecontent>> { }
}
