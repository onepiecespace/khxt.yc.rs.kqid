﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Vip
{
    /// <summary>
    /// 
    /// </summary>
    public class UserVipHistoryEntity : Entity
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 类别
        /// </summary>
        public long VipType { get; set; }

        /// <summary>
        /// 开通方式 1 付费 2 会员卡兑换 3 好友赠送
        /// </summary>
        public int IsFrom { get; set; }
        /// <summary>
        /// 会员卡兑换Id
        /// </summary>
        public string ExchangeId { get; set; }
        /// <summary>
        /// 是否自动续费
        /// </summary>
        public bool IsAutoRenew { get; set; }

        /// <summary>
        /// 自动续费价格
        /// </summary>
        public float RenewPrice { get; set; }


        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 到期时间
        /// </summary>
        public DateTime ExpiredTime { get; set; }
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }


    }
}
