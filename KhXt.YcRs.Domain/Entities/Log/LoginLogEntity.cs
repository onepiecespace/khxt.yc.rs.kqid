﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KhXt.YcRs.Domain.Entities.Log
{
    [Table("loginlog")]
    public class LoginLogEntity : EntityWithStringKey
    {
        public long UserId { get; set; }
        public string Phone { get; set; }
        public DateTime LogingTime { get; set; }

        /// <summary>
        /// 操作系统 具体类型  Other = 0,  IPhone = 1, IPad = 2,APhone = 3,APad = 4,Web = 5, Wap = 6
        /// </summary>
        public DeviceType DeviceType { get; set; }
        /// <summary>
        /// 操作系统 具体类型名称  Other = 0,  IPhone = 1, IPad = 2,APhone = 3,APad = 4,Web = 5, Wap = 6
        /// </summary>
        public string DeviceTypeName { get; set; }

        /// <summary>
        /// 操作设备型号 Mi Note 3  iPad7,5
        /// </summary>
        public string DeviceName { get; set; }

        /// <summary>
        ///设备系统版本 iOS13.3.1
        /// </summary>
        public string Os { get; set; }

        /// <summary>
        /// APP版本编译编码 v1.3.6.dev
        /// </summary>
        public string AppCode { get; set; }

        /// <summary>
        /// 版本编译号 18 19 20
        /// </summary>
        public int Ver { get; set; }
        /// <summary>
        /// 代理商 mxh bbk dsl
        /// </summary>
        public string Aid { get; set; }
        /// <summary>
        /// 租户编号
        /// </summary>
        public string Tid { get; set; }
        /// <summary>
        /// 设备极光注入Id
        /// </summary>
        public string RegistrationId { get; set; }
    }
}
