﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace KhXt.YcRs.Domain.Entities.Wallet
{
    public class WalletEntity : Entity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [Required]
        public long UserId { get; set; }

        /// <summary>
        /// 金币余额
        /// </summary>
        [Required]
        public float Coin { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Required]
        public DateTime UpdateTime { get; set; } = DateTime.Now;
    }
}
