﻿using Hx.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Entities.Integrate
{
    /// <summary>
    /// 
    /// </summary>
    public class UserIntegrateEntity : Entity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 集成系统标识 1.环信
        /// </summary>
        public int IntegrateSign { get; set; }
        /// <summary>
        /// 集成系统返回的Id
        /// </summary>
        public string IntegrateId { get; set; }
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDelete { get; set; }
        /// <summary>
        /// 是否管理员
        /// </summary>
        public bool IsManager { get; set; }
    }
}
