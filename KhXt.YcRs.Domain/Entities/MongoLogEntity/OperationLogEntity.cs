﻿using Hx.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Entities.MongoLogEntity
{
    public class OperationLogEntity : EntityWithStringKey
    {
        /// <summary>
        /// 用户账号名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public string BusinessType { get; set; }

        /// <summary>
        /// 课程id
        /// </summary>
        public int CourseId { get; set; }
        /// <summary>
        /// 课程名
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string dec { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
