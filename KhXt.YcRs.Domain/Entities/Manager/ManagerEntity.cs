﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Manager
{
    public class ManagerEntity : Entity
    {
        ///<Summary>
        /// 密码
        ///</Summary>
        public string Password { get; set; }
        ///<Summary>
        /// 手机号
        ///</Summary>
        public string Phone { get; set; }
        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 状态 1.可用  2.禁用
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        public string Deptname { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string Realname { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// 是否删除 0 删除  
        /// </summary>
        public int IsDelete { get; set; }
    }
}
