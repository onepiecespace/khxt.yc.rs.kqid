﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Product
{
    public class Pro_listEntity : Entity
    {


        /// <summary>
        /// 商品编号
        /// </summary>		
        private string _pro_number;
        public string pro_number
        {
            get { return _pro_number; }
            set { _pro_number = value; }
        }
        /// <summary>
        /// 产品名字
        /// </summary>		
        private string _pro_title;
        public string pro_title
        {
            get { return _pro_title; }
            set { _pro_title = value; }
        }
        /// <summary>
        /// 副标题
        /// </summary>		
        private string _subtitle;
        public string subtitle
        {
            get { return _subtitle; }
            set { _subtitle = value; }
        }
        /// <summary>
        /// 产品类别
        /// </summary>		
        private int _pro_classid;
        public int pro_classid
        {
            get { return _pro_classid; }
            set { _pro_classid = value; }
        }
        /// <summary>
        /// 产品图片
        /// </summary>		
        private string _imgurl;
        public string imgurl
        {
            get { return _imgurl; }
            set { _imgurl = value; }
        }
        /// <summary>
        /// 产品Banner图片
        /// </summary>		
        private string _bannerurl;
        public string bannerurl
        {
            get { return _bannerurl; }
            set { _bannerurl = value; }
        }
        /// <summary>
        /// 产品内容大图片
        /// </summary>		
        private string _contenturl;
        public string contenturl
        {
            get { return _contenturl; }
            set { _contenturl = value; }
        }
        /// <summary>
        /// 兑换说明
        /// </summary>		
        private string _remark;
        public string Remark
        {
            get { return _remark; }
            set { _remark = value; }
        }
        /// <summary>
        /// 产品内容
        /// </summary>		
        private string _content;
        public string content
        {
            get { return _content; }
            set { _content = value; }
        }
        /// <summary>
        /// sort
        /// </summary>		
        private int _sort;
        public int sort
        {
            get { return _sort; }
            set { _sort = value; }
        }

        /// <summary>
        /// 销量
        /// </summary>		
        private int _volume;
        public int volume
        {
            get { return _volume; }
            set { _volume = value; }
        }
        /// <summary>
        /// 产品值属性 1：新品,2：热销，3：推荐
        /// </summary>		
        private int _s_type;
        public int s_type
        {
            get { return _s_type; }
            set { _s_type = value; }
        }
        /// <summary>
        /// 总数量
        /// </summary>		
        private int _total_num;
        public int total_num
        {
            get { return _total_num; }
            set { _total_num = value; }
        }


        /// <summary>
        /// 重量
        /// </summary>		
        private string _weight;
        public string weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        /// <summary>
        /// 运费
        /// </summary>		
        private string _freight;
        public string freight
        {
            get { return _freight; }
            set { _freight = value; }
        }
        /// <summary>
        /// 总数量
        /// </summary>		
        private int _discount;
        public int is_discount
        {
            get { return _discount; }
            set { _discount = value; }
        }
        /// <summary>
        /// 状态 0::待上架 1:上架 2:下架
        /// </summary>		
        private int _status;
        public int status
        {
            get { return _status; }
            set { _status = value; }
        }

        /// <summary>
        /// 租户id
        /// </summary>		
        private string _seller_id;
        public string seller_id
        {
            get { return _seller_id; }
            set { _seller_id = value; }
        }
        /// <summary>
        /// 品牌ID
        /// </summary>		
        private int _brand_id;
        public int brand_id
        {
            get { return _brand_id; }
            set { _brand_id = value; }
        }
        /// <summary>
        /// 回收站 0.显示 1.回收
        /// </summary>		
        private int _recycle;
        public int recycle
        {
            get { return _recycle; }
            set { _recycle = value; }
        }
        /// <summary>
        /// add_date
        /// </summary>		
        private DateTime _add_date;
        public DateTime add_date
        {
            get { return _add_date; }
            set { _add_date = value; }
        }

    }
}

