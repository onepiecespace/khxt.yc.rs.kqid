﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Product
{
    public class Pro_classEntity : Entity
    {

        /// <summary>
        /// 上级id
        /// </summary>		
        private int _sid;
        public int sid
        {
            get { return _sid; }
            set { _sid = value; }
        }
        /// <summary>
        /// 分类名称
        /// </summary>		
        private string _pname;
        public string pname
        {
            get { return _pname; }
            set { _pname = value; }
        }
        /// <summary>
        /// 分类图片
        /// </summary>		
        private string _img;
        public string img
        {
            get { return _img; }
            set { _img = value; }
        }
        /// <summary>
        /// 小图标
        /// </summary>		
        private string _bg;
        public string bg
        {
            get { return _bg; }
            set { _bg = value; }
        }
        /// <summary>
        /// 级别
        /// </summary>		
        private int _level;
        public int level
        {
            get { return _level; }
            set { _level = value; }
        }
        /// <summary>
        /// sort
        /// </summary>		
        private int _sort;
        public int sort
        {
            get { return _sort; }
            set { _sort = value; }
        }
        /// <summary>
        /// add_date
        /// </summary>		
        private DateTime _add_date;
        public DateTime add_date
        {
            get { return _add_date; }
            set { _add_date = value; }
        }
        /// <summary>
        /// 回收站 0.不回收 1.回收
        /// </summary>		
        private int _recycle;
        public int recycle
        {
            get { return _recycle; }
            set { _recycle = value; }
        }


    }
}

