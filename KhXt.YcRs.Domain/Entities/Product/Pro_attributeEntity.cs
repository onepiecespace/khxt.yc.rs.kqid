﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Product
{
    public class Pro_attributeEntity : Entity
    {
        /// <summary>
        /// 属性配置名称
        /// </summary>		
        private string _attributename;
        public string attributename
        {
            get { return _attributename; }
            set { _attributename = value; }
        }
        /// <summary>
        /// 关联商品ID
        /// </summary>		
        private string _product_id;
        public string product_id
        {
            get { return _product_id; }
            set { _product_id = value; }
        }
        /// <summary>
        /// 颜色
        /// </summary>		
        private string _color;
        public string color
        {
            get { return _color; }
            set { _color = value; }
        }
        /// <summary>
        /// 尺码
        /// </summary>		
        private string _size;
        public string size
        {
            get { return _size; }
            set { _size = value; }
        }
        /// <summary>
        /// 原价格
        /// </summary>		
        private decimal _yprice;
        public decimal yprice
        {
            get { return _yprice; }
            set { _yprice = value; }
        }
        /// <summary>
        /// 成本价
        /// </summary>		
        private decimal _costprice;
        public decimal costprice
        {
            get { return _costprice; }
            set { _costprice = value; }
        }
        /// <summary>
        /// 出售价格
        /// </summary>		
        private decimal _price;
        public decimal price
        {
            get { return _price; }
            set { _price = value; }
        }
        /// <summary>
        /// 是否开启金币兑换 0:未开启金币兑换 1:开启金币兑换
        /// </summary>		
        private int _iscoins;
        public int iscoins
        {
            get { return _iscoins; }
            set { _iscoins = value; }
        }
        /// <summary>
        /// 金币兑换价格（所需金币数量）
        /// </summary>		
        private decimal _coins_price;
        public decimal coins_price
        {
            get { return _coins_price; }
            set { _coins_price = value; }
        }

        /// <summary>
        /// 图片
        /// </summary>		
        private string _img;
        public string img
        {
            get { return _img; }
            set { _img = value; }
        }

        /// <summary>
        /// 库存
        /// </summary>		
        private int _num;
        public int num
        {
            get { return _num; }
            set { _num = value; }
        }
        /// <summary>
        /// 单位
        /// </summary>		
        private string _unit;
        public string unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        /// <summary>
        /// 状态 0:未上架1:已上架 2 缺货 3下架
        /// </summary>		
        private int _status;
        public int status
        {
            get { return _status; }
            set { _status = value; }
        }
        /// <summary>
        /// 属性
        /// </summary>		
        private string _attribute;
        public string attribute
        {
            get { return _attribute; }
            set { _attribute = value; }
        }
        /// <summary>
        /// 回收站 0.不回收 1.回收
        /// </summary>		
        private int _recycle;
        public int recycle
        {
            get { return _recycle; }
            set { _recycle = value; }
        }
        /// <summary>
        /// 总库存
        /// </summary>		
        private int _total_num;
        public int total_num
        {
            get { return _total_num; }
            set { _total_num = value; }
        }
        /// <summary>
        /// add_date
        /// </summary>		
        private DateTime _add_date;
        public DateTime add_date
        {
            get { return _add_date; }
            set { _add_date = value; }
        }
    }
}


