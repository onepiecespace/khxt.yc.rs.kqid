﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace KhXt.YcRs.Domain.Entities.Order
{
    /// <summary>
    /// 用户优惠券
    /// </summary>
    public class UserCouponEntity : Entity
    {
        /// <summary>
        /// 优惠券Id
        /// </summary>
        public long CouponId { get; set; }

        /// <summary>
        /// 优惠券No
        /// </summary>
        public string CouponNo { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// vip限制 1月卡2半年卡 4年卡
        /// </summary>
        public long VipType { get; set; }

        /// <summary>
        /// 使用限制 0 仅自己 1仅分享 2 不限
        /// </summary>
        [Required]
        public int UseLimit { get; set; }

        /// <summary>
        /// 是否可以组合使用 0 不可以 1 可以
        /// </summary>
        public int IsCombination { get; set; }

        /// <summary>
        /// 领取时间
        /// </summary>
        public DateTime ReceiveTime { get; set; }

        /// <summary>
        /// 到期时间
        /// </summary>
        public DateTime ExpiredTime { get; set; }
        /// <summary>
        /// 花费金币
        /// </summary>
        public int SpendCoins { get; set; }

        /// <summary>
        /// 赠送人
        /// </summary>
        public long Giver { get; set; }

        /// <summary>
        /// 赠送时间
        /// </summary>
        public DateTime? GiveTime { get; set; } = null;

        /// <summary>
        /// 状态 -1 已作废  0 未使用 1 已使用 2 已过期 
        /// </summary>
        public int Status { get; set; } = 0;

        /// <summary>
        /// 使用时间
        /// </summary>
        public DateTime? UseTime { get; set; } = null;

        /// <summary>
        /// 优惠券来自 0 系统赠送 1 好友赠送 2 用户购买 
        /// </summary>
        [Required]
        public int IsFrom { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public long Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 业务类别
        /// </summary>
        public long BusinessType { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public long Updater { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 可用平台 0 无限制 1 App 
        /// </summary>
        public int PlatForm { get; set; } = 0;

    }
}
