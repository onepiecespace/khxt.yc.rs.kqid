﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace KhXt.YcRs.Domain.Entities.Order
{
    /// <summary>
    /// 订单优惠券
    /// </summary>
    public class OrderCouponEntity : Entity
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        [Required]
        [DisplayName("订单Id")]
        public string OrderId { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        [DisplayName("商品Id")]
        public string ItemId { get; set; }

        /// <summary>
        /// 优惠券Id
        /// </summary>
        [Required]
        [DisplayName("优惠券Id")]
        public int CouponId { get; set; }

        /// <summary>
        /// 优惠券No
        /// </summary>
        [Required]
        public string CouponNo { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName("创建时间")]
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [DisplayName("更新时间")]
        public DateTime? UpdateTime { get; set; }

    }
}
