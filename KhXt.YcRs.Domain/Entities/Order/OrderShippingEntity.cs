﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace KhXt.YcRs.Domain.Entities.Order
{
    /// <summary>
    /// 订单物流
    /// </summary>
    public class OrderShippingEntity : Entity
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        [Required]
        [DisplayName("订单Id")]
        public string OrderId { get; set; }

        /// <summary>
        /// 收货人
        /// </summary>
        [DisplayName("收货人")]
        public string ReceiverName { get; set; }

        /// <summary>
        /// 收货人固定电话
        /// </summary>
        [DisplayName("收货人固定电话")]
        public string ReceiverPhone { get; set; }

        /// <summary>
        /// 收货人移动电话
        /// </summary>
        [DisplayName("收货人移动电话")]
        public string ReceiverMobile { get; set; }

        /// <summary>
        /// 国家 三位 CHN
        /// </summary>
        [DisplayName("国家 三位 CHN")]
        public string ReceiverState { get; set; }

        /// <summary>
        /// 省/市
        /// </summary>
        [DisplayName("省/市")]
        public string ReceiverProvince { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        [DisplayName("城市")]
        public string ReceiverCity { get; set; }

        /// <summary>
        /// 区/县
        /// </summary>
        [DisplayName("区/县")]
        public string ReceiverDistrict { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        [DisplayName("地址")]
        public string ReceiverAddress { get; set; }

        /// <summary>
        /// 邮政编码
        /// </summary>
        [DisplayName("邮政编码")]
        public string ReceiverZip { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName("创建时间")]
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [DisplayName("更新时间")]
        public DateTime? UpdateTime { get; set; }

    }
}
