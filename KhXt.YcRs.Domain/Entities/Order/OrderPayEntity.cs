﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Order
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderPayEntity : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 交易号
        /// </summary>
        public string TransactionNo { get; set; }
        /// <summary>
        /// /
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// IosProductId
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// 交易详细信息
        /// </summary>
        public string Transaction { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PaymentType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsTest { get; set; }
        /// <summary>
        /// 设备号
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// 是否自动续费
        /// </summary>
        public bool IsAutoRenew { get; set; } = false;

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess { get; set; } = true;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 代理商
        /// </summary>
        public string Aid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long TenantId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int BusinessType { get; set; }
        /// <summary>
        /// 更新
        /// </summary>
        public DateTime UpdateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 是否退款
        /// </summary>
        public int IsRefund { get; set; } = 0;
    }
}
