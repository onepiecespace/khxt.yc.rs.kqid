﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace KhXt.YcRs.Domain.Entities.Order
{
    /// <summary>
    /// 订单详情
    /// </summary>
    public class OrderItemEntity : Entity
    {
        /// <summary>
        /// 商品Id
        /// </summary>
        [Required]
        [DisplayName("商品Id")]
        public string ItemId { get; set; }

        /// <summary>
        /// 订单Id
        /// </summary>
        [Required]
        [DisplayName("订单Id")]
        public string OrderId { get; set; }

        /// <summary>
        /// 商品数量
        /// </summary>
        [DisplayName("商品数量")]
        public long Num { get; set; }

        /// <summary>
        /// 商品标题
        /// </summary>
        [DisplayName("商品标题")]
        public string Title { get; set; }
        /// <summary>
        /// 商品描述
        /// </summary>
        [DisplayName("商品描述")]
        public string Description { get; set; }
        /// <summary>
        /// 商品单价
        /// </summary>
        [DisplayName("商品单价")]
        public float Price { get; set; }

        /// <summary>
        /// 商品总金额
        /// </summary>
        [DisplayName("商品总金额")]
        public float TotalFee { get; set; }

        /// <summary>
        /// 商品图片地址
        /// </summary>
        [DisplayName("商品图片地址")]
        public string PicPath { get; set; }
        /// <summary>
        /// 评论Id
        /// </summary>
        [Required]
        [DisplayName("评论Id")]
        public int CommentId { get; set; }

    }
}
