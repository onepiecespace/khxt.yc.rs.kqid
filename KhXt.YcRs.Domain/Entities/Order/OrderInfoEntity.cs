﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel;

namespace KhXt.YcRs.Domain.Entities.Order
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderInfoEntity : EntityWithStringKey
    {
        /// <summary>
		/// 实付金额
		/// </summary>
		[DisplayName("实付金额")]
        public string Payment { get; set; }

        /// <summary>
        /// 支付类型 0 微信支付 1 支付宝 2 苹果支付 3 免费 4 线下 5 银联 6 金币兑换
        /// </summary>
        [DisplayName("支付类型 0 微信支付 1 支付宝 2 苹果支付 3 免费 4 线下 5 银联  6 金币兑换")]
        public int PaymentType { get; set; }
        /// <summary>
        /// Ios价格
        /// </summary>
        [DisplayName("IosProductId")]
        public string ProductId { get; set; }
        /// <summary>
        /// 支付交易号
        /// </summary>
        [DisplayName("支付交易号")]
        public string PaymentNo { get; set; }

        /// <summary>
        /// 邮费
        /// </summary>
        [DisplayName("邮费")]
        public string PostFee { get; set; }

        /// <summary>
        /// 状态 1 未付款 2 已付款 3 未发货 4 已发货 5 交易成功 6 交易关闭 7交易取消
        /// </summary>
        [DisplayName("状态 1 未付款 2 已付款 3 未发货 4 已发货 5 交易成功 6 交易关闭 7交易取消 ")]
        public int Status { get; set; }

        /// <summary>
        /// 订单创建时间
        /// </summary>
        [DisplayName("订单创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 订单更新时间
        /// </summary>
        [DisplayName("订单更新时间")]
        public DateTime UpdateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 付款时间
        /// </summary>
        [DisplayName("付款时间")]
        public DateTime? PaymentTime { get; set; }

        /// <summary>
        /// 发货时间
        /// </summary>
        [DisplayName("发货时间")]
        public DateTime? ConsignTime { get; set; }

        /// <summary>
        /// 交易完成时间
        /// </summary>
        [DisplayName("交易完成时间")]
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// 交易关闭时间
        /// </summary>
        [DisplayName("交易关闭时间")]
        public DateTime? CloseTime { get; set; }

        /// <summary>
        /// 物流类型
        /// </summary>
        [DisplayName("物流类型")]
        public string ShippingType { get; set; }
        /// <summary>
        /// 物流名称
        /// </summary>
        [DisplayName("物流名称")]
        public string ShippingName { get; set; }

        /// <summary>
        /// 物流单号
        /// </summary>
        [DisplayName("物流单号")]
        public string ShippingCode { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        [DisplayName("用户Id")]
        public long UserId { get; set; }

        /// <summary>
        /// 是否已经评价 0 没有 1 买家已评论 2 双方已评论
        /// </summary>
        [DisplayName("是否已经评价 0 没有 1 买家已评论 2 双方已评论")]
        public int IsRate { get; set; } = 0;

        /// <summary>
        /// 订单来自 0 App 1 Web
        /// </summary>
        [DisplayName("订单来自 0 App 1 Web")]
        public int IsFrom { get; set; } = 0;
        /// <summary>
        /// 业务类型
        /// </summary>
        public int BusinessType { get; set; }
        /// <summary>
        /// Aid
        /// </summary>
        public string Aid { get; set; }
        /// <summary>
        /// 租户Id
        /// </summary>
        public long TenantId { get; set; }



    }
}
