﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Order
{
    /// <summary>
    /// 
    /// </summary>
    public class IOSPriceEntity : Entity
    {
        /// <summary>
        /// 产品Id
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public float Price { get; set; }
    }
}
