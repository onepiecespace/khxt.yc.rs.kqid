﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Course
{
    public class UserCourseChildProgessEntity : Entity
    {
        /// <summary>
        /// 课程编码
        /// </summary>		 
        public int CourseId { get; set; }

        /// <summary>
        /// 子课程编码
        /// </summary>		 
        public int CourseChildId { get; set; }
        /// <summary>
        /// 子课程总时长
        /// </summary>
        public int DurationTime { get; set; }

        /// <summary>
        /// 当前观看时长
        /// </summary>
        public int CurrentTime { get; set; } = 0;
        /// <summary>
        /// 观看次数
        /// </summary>
        public int LiveNum { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 观看进度
        /// </summary>
        public float ChildProgress { get; set; }
        /// <summary>
        /// 操作标识 0未完成 1 已完成 (var a=currentTime/duration)var b = (a * 100).toFixed(0) + "%"if(currentTime==duration){已完成  }
        /// </summary>
        public bool IsFinish { get; set; }
        /// <summary>
        /// 金币是否添加
        /// </summary>
        public bool IsHasCoin { get; set; } = false;
        /// <summary>
        /// 经验是否添加
        /// </summary>
        public bool IsHasExp { get; set; } = false;
        public int Creator { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now;
        public int Updater { get; set; }
        public DateTime UpdateTime { get; set; } = DateTime.Now;

    }
}
