﻿using Hx.Domain.Entities;
using System;
using System.Collections.Generic;

namespace KhXt.YcRs.Domain.Entities.Course
{
    public class CourseItemEntity : Entity
    {
        /// <summary>
        /// 课程编码
        /// </summary>		 
        public int CourseId { get; set; }
        /// <summary>
        /// 子课程编码
        /// </summary>		 
        public int CourseChildId { get; set; }

        /// <summary>
        /// 答题分类
        /// </summary>
        public int ClassId { get; set; }

        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 题目备选答案，适用用选择题
        /// </summary>

        public Dictionary<string, string> Options { get; set; }

        /// <summary>
        /// 易错项
        /// </summary>
        public string Confuse { get; set; }
        /// <summary>
        /// 题目内容,适用于填空题型
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 用"@@"分割的答案,适用于选择（一个或多个key值）、填空题
        /// </summary>
        public string Answer { get; set; }

        public string ItemImg { get; set; }
        /// <summary>
        /// 难度
        /// </summary>
        public int Difficulty { get; set; }

        public string Analysis { get; set; }

        public int ItemSort { get; set; }

        public int IsShow { get; set; }
        public int Creator { get; set; }

        public int IsDelete { get; set; }
        public DateTime CreateTime { get; set; }
        public int Updater { get; set; }

        public DateTime UpdateTime { get; set; }

        public string Remark { get; set; }

    }
}
