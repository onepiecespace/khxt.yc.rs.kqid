﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Course
{
    public class CourseAppointmentEntity : Entity
    {
        public string Name { get; set; }

        public string Phone { get; set; }

        public DateTime CreateTime { get; set; }
        public string Remark { get; set; }
    }
}
