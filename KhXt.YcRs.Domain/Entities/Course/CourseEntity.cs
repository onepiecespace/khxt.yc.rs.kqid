﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Course
{
    public class CourseEntity : Entity
    {

        public string CourseName { get; set; }
        public int ClassId { get; set; }
        public string ChildIds { get; set; }
        public string GradeIds { get; set; }
        public int Cateorys { get; set; }
        public int IsTop { get; set; }
        public int IsHot { get; set; }
        public DateTime LiveBeginTime { get; set; }
        public DateTime LiveEndTime { get; set; }
        public DateTime ValidBeginTime { get; set; }
        public DateTime ValidEndTime { get; set; }
        public DateTime PreferentialBeginTime { get; set; }
        public DateTime PreferentialEndTime { get; set; }
        public string TeachersUserName { get; set; }
        public int TeachersUserId { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountPrice { get; set; }
        public string Detailed { get; set; }
        public decimal InternalPrice { get; set; }
        public string CoursePic { get; set; }
        public string CoursePicContent { get; set; }
        public int VirtualSales { get; set; }
        public int Sales { get; set; }
        public string CoursePicMini { get; set; }
        public string CoursePicPhone { get; set; }
        public string CoursePicPhoneMini { get; set; }
        public string CoursePicPhoneTran { get; set; }
        public int IsLongValid { get; set; }
        public int IsDiscount { get; set; }
        public int IsExpress { get; set; }
        public int IsComic { get; set; }
        public int IsRecord { get; set; }
        public int IsLive { get; set; }
        public int AppSourse { get; set; }
        public int IsDelete { get; set; }
        public int Status { get; set; }
        public bool WxStatus { get; set; }

        public int Sort { get; set; }
        public int TenantId { get; set; }


        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public int ChannelId { get; set; }

        /// <summary>
        /// 学好语文的秘密,learnchinesewell,98
        /// 小学生必背古诗词,studentsgushici,198
        /// 成语里的作文课,chengyuzuowenclass,298
        /// 汉字背后的秘密,chinesebacksecrecy,298
        /// 超级口才明星班,superkoucaistarclass,898
        /// 高效阅读,gaoxiaoreading,1498
        /// 名著阅读,mingzhureading,1998
        /// 创新作文,chuangxinzuowen,1198
        /// 中国历史名人故事,chinesestarstory,98
        /// </summary>
        public string Iosproductid { get; set; }
        public decimal IosPayPirce { get; set; }
        public string IosProductidName { get; set; }

        public decimal ExpressPirce { get; set; }

        public string videoId { get; set; }
        public string SecretKey { get; set; }

        public string Teachers { get; set; }
    }
}
