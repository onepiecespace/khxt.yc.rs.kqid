﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Course
{
    public class CourseBuyCollectEntity : Entity
    {
        /// <summary>
        /// UserId
        /// </summary>		 
        public int UserId { get; set; }

        /// <summary>
        /// CourseOrderId
        /// </summary>		 
        public string CourseOrderId { get; set; }

        /// <summary>
        /// CourseId
        /// </summary>		 
        public int CourseId { get; set; }

        /// <summary>
        /// CreateId
        /// </summary>		 
        public int CreateId { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>		 
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// IsBuy
        /// </summary>		 
        public int IsBuy { get; set; }

        /// <summary>
        /// IsCollect
        /// </summary>		 
        public int IsCollect { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>		 
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// 是否隐藏
        /// </summary>		 
        public int IsHide { get; set; }
    }
}
