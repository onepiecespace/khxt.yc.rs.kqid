﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Course
{
    public class CourseCateoryMiddleEntity : Entity
    {
        /// <summary>
        /// CourseId
        /// </summary>		 
        public int CourseId { get; set; }

        /// <summary>
        /// CourseCateoryId
        /// </summary>		 
        public int CourseCateoryId { get; set; }

        /// <summary>
        /// CourseCateoryIdList
        /// </summary>		 
        public string CourseCateoryIdList { get; set; }

        /// <summary>
        /// IsDelete
        /// </summary>		 
        public int IsDelete { get; set; }

        /// <summary>
        /// CreateId
        /// </summary>		 
        public int CreateId { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>		 
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// UpdateTime
        /// </summary>		 
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// TenantId
        /// </summary>		 
        public int TenantId { get; set; }

    }
}
