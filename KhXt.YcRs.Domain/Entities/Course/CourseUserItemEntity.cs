﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KhXt.YcRs.Domain.Entities.Course
{
    public class CourseUserItemEntity : Entity
    {
        /// <summary>
        /// 课程编码
        /// </summary>		 
        public int CourseId { get; set; }

        /// <summary>
        /// 子课程编码
        /// </summary>		 
        public int CourseChildId { get; set; }
        /// <summary>
        /// 课程作业Id
        /// </summary>
        public int CourseItemId { get; set; }

        /// <summary>
        /// 错误次数
        /// </summary>
        public int ErrorNum { get; set; } = 0;
        /// <summary>
        /// 答题用时
        /// </summary>
        public int UseTime { get; set; }
        /// <summary>
        /// 选择的答案 第一次
        /// </summary>
        public string SelectAnswer { get; set; }
        /// <summary>
        /// 更新答案
        /// </summary>
        public string UpdateAnswer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        public int Creator { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now;
        public int Updater { get; set; }
        public DateTime UpdateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 操作标识  -1 未作答 0 错误 1 正确 
        /// </summary>
        public int OpSign { get; set; }
        /// <summary>
        /// 金币是否添加
        /// </summary>
        public bool IsHasCoin { get; set; } = false;
        /// <summary>
        /// 经验是否添加
        /// </summary>
        public bool IsHasExp { get; set; } = false;
    }
}
