﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Course
{
    public class CourseOrderEntity : Entity
    {

        public long CourseId { get; set; }
        public string OrderNo { get; set; }
        public long UserId { get; set; }
        public decimal PayPrice { get; set; }
        /// <summary>
        /// 订单状态 99全部 0 进行中 1已完成 2取消交易 3已结算 4申请退款中 5已退款
        /// </summary>
        public int OrderState { get; set; }
        public int IsPay { get; set; }
        public DateTime PayTime { get; set; }
        /// <summary>
        /// 付款方式0 免费 1线下付款,2微信 3支付宝 4 Apple 5银联
        /// </summary>
        public int PayType { get; set; }
        /// <summary>
        /// 支付平台 返回的 编号
        /// </summary>
        public string PayNo { get; set; }
        public int IsSend { get; set; }
        public DateTime SendTime { get; set; }
        public int IsGet { get; set; }
        public DateTime GetTime { get; set; }
        public string PackageNo { get; set; }
        public int PackageType { get; set; }
        public string OrderDes { get; set; }
        public int AddressId { get; set; }
        public int IsDelete { get; set; }
        public int CreateId { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public int TenantId { get; set; }
        public int IsInvoice { get; set; }
        public string InvoiceDes { get; set; }

    }
}
