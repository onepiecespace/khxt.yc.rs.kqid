﻿using KhXt.YcRs.Domain.Models.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Entities.Course
{
    /// <summary>
    /// 课程提交返回结构
    /// </summary>
    public class CourseUserWorkResult
    {
        /// <summary>
        /// 课程id 
        /// </summary>
        public int CourseId { get; set; }
        /// <summary>
        /// 子课程id
        /// </summary>
        public int CourseChildId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 金币值
        /// </summary>
        public int CoinValue { get; set; }
        /// <summary>
        /// 经验值
        /// </summary>
        public int ExpValue { get; set; }
        /// <summary>
        /// 题目总数
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 错误数量
        /// </summary>
        public int FailCount { get; set; }
        /// <summary>
        /// 课程题目信息
        /// </summary>
        public List<CourseUserWorkModel> CourseItem { get; set; }
    }
}
