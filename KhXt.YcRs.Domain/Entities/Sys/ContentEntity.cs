﻿using Hx.Domain.Entities;
using System;


namespace KhXt.YcRs.Domain.Entities.Sys
{
    public class ContentEntity : Entity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int ContentType { get; set; }
        public string Image { get; set; }
        public string SmallImage { get; set; }
        public string Slide { get; set; }
        public string Summary { get; set; }
        public string Url { get; set; }
        public int Category { get; set; }

        public string Tag { get; set; }
        public int Action { get; set; }

        public string Author { get; set; }

        public string KeyWord { get; set; }

        public int IsDelete { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }


        public string TentantId { get; set; }
        public string Remark { get; set; }

        public int Sort { get; set; }
    }
}
