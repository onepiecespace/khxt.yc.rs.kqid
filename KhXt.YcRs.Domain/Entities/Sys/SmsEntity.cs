﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Sys
{
    public class SmsEntity : Entity
    {
        public string Sn { get; set; }

        public string TaskId { get; set; }

        public string GwCode { get; set; }

        public string GwDesc { get; set; }


        public DateTime SubmitTime { get; set; }
        public DateTime ReceiptTime { get; set; }

        public int Status { get; set; }
        public string Content { get; set; }

        public DateTime CreateTime { get; set; }

    }
}