﻿using Hx.Domain.Entities;
using System;


namespace KhXt.YcRs.Domain.Entities.Sys
{
    public class CaptionEntity : Entity
    {
        public int Ranking { get; set; }

        public string Caption { get; set; }
        public int Category { get; set; }
        public string Image { get; set; }
        public string Remark { get; set; }

        public int Threshold { get; set; }

        public int CaptionType { get; set; }
    }
}
