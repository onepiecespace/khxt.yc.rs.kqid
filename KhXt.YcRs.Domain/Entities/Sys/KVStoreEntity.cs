﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Sys
{
    /// <summary>
    /// a_KVStore  
    /// </summary>
    [Serializable]
    public class KVStoreEntity : Entity
    {

        ///<Summary>
        /// 租户ID
        ///</Summary>
        public int TenantId { get; set; }
        ///<Summary>
        /// key
        ///</Summary>
        public string KeyData { get; set; }
        ///<Summary>
        /// value
        ///</Summary>
        public string ValueData { get; set; }
        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime CreateTime { get; set; }
    }
}