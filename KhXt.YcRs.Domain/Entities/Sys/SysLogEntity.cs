﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KhXt.YcRs.Domain.Entities.Sys
{
    /// <summary>
    /// 
    /// </summary>
    [Table("sysLogs")]
    public class SysLogEntity : EntityWithStringKey
    {
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 小时
        /// </summary>
        public int Hour { get; set; }
        /// <summary>
        /// Api地址
        /// </summary>
        public string ApiUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int BusinessType { get; set; }
        /// <summary>
        /// 类别 0 时间访问量  1 Api 访问量
        /// </summary>
        public int Category { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public long Count { get; set; }
    }
}
