﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Sys
{
    public class User_ChannelEntity : Entity
    {
        public long channelid { get; set; }
        public int userid { get; set; }

        public DateTime vt { get; set; }

    }
}
