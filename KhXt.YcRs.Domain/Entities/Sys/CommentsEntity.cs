﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.Sys
{
    /// <summary>
    /// a_Comments  
    /// </summary>
    [Serializable]
    public class CommentsEntity : Entity
    {

        ///<Summary>
        /// 父评论Id
        ///</Summary>
        public int ParentId { get; set; }
        ///<Summary>
        /// 父评论Id
        ///</Summary>
        public string ParentIdList { get; set; }
        ///<Summary>
        /// 被评论对象Id(是朗读还是PK......)
        ///</Summary>
        public int CommentedObjectId { get; set; }
        ///<Summary>
        /// 所属ID
        ///</Summary>
        public int OwnerId { get; set; }
        ///<Summary>
        /// 租户类型Id
        ///</Summary>
        public int TenantTypeId { get; set; }
        ///<Summary>
        /// 评论人UserId
        ///</Summary>
        public int UserId { get; set; }
        ///<Summary>
        /// 评论人名称
        ///</Summary>
        public string UserName { get; set; }
        ///<Summary>
        /// 被回复UserId（一级ToUserId为0）
        ///</Summary>
        public int ToUserId { get; set; }
        ///<Summary>
        /// 被回复人名称（一级ToUserDisplayName为空字符串）
        ///</Summary>
        public string ToUserName { get; set; }
        ///<Summary>
        /// 标题
        ///</Summary>
        public string Title { get; set; }
        ///<Summary>
        /// 内容
        ///</Summary>
        public string Body { get; set; }
        ///<Summary>
        /// 是否悄悄话
        ///</Summary>
        public int IsPrivate { get; set; }
        ///<Summary>
        /// 是否匿名评论
        ///</Summary>
        public int IsAnonymous { get; set; }
        ///<Summary>
        /// 审核状态
        ///</Summary>
        public int AuditStatus { get; set; }
        ///<Summary>
        /// 子级评论数量
        ///</Summary>
        public int ChildCount { get; set; }
        ///<Summary>
        /// 是否删除 0 存在 1删除
        ///</Summary>
        public int IsDel { get; set; }
        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime CreateTime { get; set; }
        ///<Summary>
        /// 喜欢数量
        ///</Summary>
        public int LikeCount { get; set; }

        public int StarCount { get; set; }
    }
}