﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KhXt.YcRs.Domain.Entities
{
    public class TestEntity : Entity
    {
        public int Age { get; set; }

        public string Url { get; set; }

        public decimal Rating { get; set; }
        public string Name { get; set; }
        public DateTime DateTime { get; set; }
        public string Body { get; set; }

    }

    [Table("person")]
    public class PersonEntity : EntityWithStringKey
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
