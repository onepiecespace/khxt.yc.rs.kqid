﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class AddressEntity : Entity
    {
        /////<Summary>
        ///// 地址ID ，自增长，主键
        /////</Summary>
        //public int Id { get; set; }
        ///<Summary>
        /// 收货人
        ///</Summary>
        public string Consignee { get; set; }
        ///<Summary>
        /// 性别1.先生  0.女士
        ///</Summary>
        public int Sex { get; set; }
        ///<Summary>
        /// 手机号码
        ///</Summary>
        public string Phone { get; set; }
        ///<Summary>
        /// 省
        ///</Summary>
        public string Provincial { get; set; }
        ///<Summary>
        /// 市
        ///</Summary>
        public string City { get; set; }
        ///<Summary>
        /// 区
        ///</Summary>
        public string Areas { get; set; }
        ///<Summary>
        /// 地址
        ///</Summary>
        public string Address { get; set; }
        ///<Summary>
        /// 是否常用地址 0不是，1是
        ///</Summary>
        public int Iscommonly { get; set; }
        ///<Summary>
        /// 用户ID
        ///</Summary>
        public int Userid { get; set; }
        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime Createtime { get; set; }
        ///<Summary>
        /// 修改时间
        ///</Summary>
        public DateTime Updatetime { get; set; }

    }
}
