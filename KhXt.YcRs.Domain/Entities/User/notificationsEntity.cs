﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class notificationsEntity : Entity
    {

        /// <summary>
        ///发起者id
        /// </summary>
        public long userId { get; set; }
        /// <summary>
        /// 通知userid
        /// </summary>		 
        public long notifiuser_id { get; set; }
        /// <summary>
        ///通知分类( 0：系统消息1：APP推送，)
        /// </summary>		 
        public int notifiable_type { get; set; }
        /// <summary>
        ///消息业务模块  枚举 enum 
        /// </summary>		 
        public int businesstype { get; set; }
        /// <summary>
        ///平台类型（0:全部平台，1：PC，2：M，3：APP-IOS，4：APP-Android)
        /// </summary>		 
        public int msgplatform { get; set; }

        public string title { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string content { get; set; }
        /// <summary>
        /// 消息标题素略图
        /// </summary>
        public string msgImg { get; set; }
        /// <summary>
        /// 消息目的类型 默认0（朗读(1：关注消息，2：点赞消息，3：评论/评论被回复 4 发布成功消息 5 得分消息)）
        /// </summary>
        public int targettype { get; set; }
        /// <summary>
        /// 推送目标URL
        /// </summary>
        public string targeturl { get; set; }
        /// <summary>
        /// businesstype=1 关联用户作品ID
        /// </summary>
        public int targetId { get; set; }

        /// <summary>
        /// 是否发送(0 否 1 是)
        /// </summary>
        public int isSend { get; set; }
        /// <summary>
        /// 发送时间
        /// </summary>		 
        public DateTime senddata { get; set; }
        /// <summary>
        /// 状态 是否已读(0 否 1 是)
        /// </summary>		 
        public int IsRead { get; set; }
        /// <summary>
        /// UserId
        /// </summary>		 
        public DateTime read_time { get; set; }
        /// <summary>
        /// created_time
        /// </summary>		 
        public DateTime created_time { get; set; }

    }
}
