﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class UserEntity : Entity
    {
        /// <summary>
        /// 租户标识IHaveTenant
        /// </summary>
        public long TenantId { get; set; }
        ///<Summary>
        /// 用户名
        ///</Summary>
        public string Username { get; set; }
        ///<Summary>
        /// 密码
        ///</Summary>
        public string Password { get; set; }
        ///<Summary>
        /// 性别 0 女  1男 2 未知
        ///</Summary>
        public int Sex { get; set; }
        ///<Summary>
        /// 生日
        ///</Summary>
        public string Birthday { get; set; }
        ///<Summary>
        /// 省
        ///</Summary>
        public string Provincial { get; set; }
        ///<Summary>
        /// 市
        ///</Summary>
        public string City { get; set; }
        ///<Summary>
        /// 区
        ///</Summary>
        public string Areas { get; set; }
        ///<Summary>
        /// 详细地址
        ///</Summary>
        public string Address { get; set; }

        ///<Summary>
        /// 年级 0 表示为未填写
        ///</Summary>
        public int Grade { get; set; } = 1;
        ///<Summary>
        /// 头像
        ///</Summary>
        public string Headurl { get; set; }
        ///<Summary>
        /// 等级
        ///</Summary>
        public int Level { get; set; }
        ///<Summary>
        /// 手机号
        ///</Summary>
        public string Phone { get; set; }
        ///<Summary>
        /// 微信
        ///</Summary>
        public string Wx { get; set; }
        ///<Summary>
        /// QQ
        ///</Summary>
        public string Qq { get; set; }
        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime Creatime { get; set; }
        ///<Summary>
        /// 状态 1.可用  2.禁用
        ///</Summary>
        public int State { get; set; }
        ///<Summary>
        /// 邀请人ID
        ///</Summary>
        public int Inviterid { get; set; }
        ///<Summary>
        /// 积分
        ///</Summary>
        public int Score { get; set; }
        ///<Summary>
        /// 金币
        ///</Summary>
        public int Gold { get; set; }
        ///<Summary>
        /// 账户总金额
        ///</Summary>
        public decimal MoneyTotal { get; set; }
        ///<Summary>
        /// 经验
        ///</Summary>
        public int Experience { get; set; }
        /////<Summary>
        ///// TenantId 
        /////</Summary>
        //public int TenantId { get; set; }
        ///<Summary>
        /// 是否删除 0 正常 1 软删除
        ///</Summary>
        public int IsDelete { get; set; }
        ///<Summary>
        /// 登录方式 1 phone 2 wx 4 qq 8 guest
        ///</Summary>
        public int LoginType { get; set; }
        ///<Summary>
        /// 微信小程序登录地址
        ///</Summary>
        public string MiniWX { get; set; }
        ///<Summary>
        /// 是否第一次输入地址信息
        ///</Summary>
        public int IsFirstInputAddress { get; set; }
        /// <summary>
        /// 是否第一次校区信息
        /// </summary>
        public int IsFirstCollege { get; set; } = 0;

        public string OpenId { get; set; }
        public string UnionId { get; set; }

        ///<Summary>
        ///账户类型：1：正常，2 内部审核测试 512：试用 8192:压力测试
        ///</Summary>
        public int AccountType { get; set; } = 2;

        public int IsMember { get; set; }
        public DateTime? MemberDueDate { get; set; }
        /// <summary>
        /// 代理商 默认mxh 
        /// </summary>
        public string Aid { get; set; }

        public string Remark { get; set; }
        /// <summary>
        /// 授权课程一级分类Ids
        /// </summary>
        public string AuthCateorys { get; set; }
        /// <summary>
        /// 账户使用限制日期
        /// </summary>
        public DateTime? UseDueDate { get; set; }

    }
}
