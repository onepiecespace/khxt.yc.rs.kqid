﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class FeedbackEntity : Entity
    {
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Phone { get; set; }

        public string Email { get; set; }
        public string Content { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 租户列表 默认1表示系统消息
        /// </summary>
        public int TenantId { get; set; }

    }













}
