﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class UserCollegeEntity : Entity
    {
        ///<Summary>
        /// 用户ID
        ///</Summary>
        public long UserId { get; set; }
        ///<Summary>
        /// 真实姓名
        ///</Summary>
        public string RealName { get; set; }
        ///<Summary>
        /// 校区名称
        ///</Summary>
        public string SchoolName { get; set; }
        ///<Summary>
        /// 岗位 1销售，2运营，3校长，4教务, 5老师
        ///</Summary>
        public int PostType { get; set; }

        ///<Summary>
        /// 省
        ///</Summary>
        public string Provincial { get; set; }
        ///<Summary>
        /// 市
        ///</Summary>
        public string City { get; set; }
        ///<Summary>
        /// 区
        ///</Summary>
        public string Areas { get; set; }

        ///<Summary>
        /// 合作方式 1品牌加盟，2直营校区，3联营校区，4课程合作
        ///</Summary>
        public int ModeType { get; set; }

        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime CreateTime { get; set; }
        ///<Summary>
        /// 修改时间
        ///</Summary>
        public DateTime UpdateTime { get; set; }
        ///<Summary>
        /// 下次修改提醒时间
        ///</Summary>
        public DateTime RenextTime { get; set; }

    }
}
