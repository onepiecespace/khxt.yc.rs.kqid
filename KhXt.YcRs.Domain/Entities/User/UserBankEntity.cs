﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class UserBankEntity : Entity
    {

        public string UserName { get; set; }
        public string BankName { get; set; }
        public long UserId { get; set; }
        public string BankCode { get; set; }

        public string BankPhone { get; set; }

        public DateTime CreateTime { get; set; }

    }













}
