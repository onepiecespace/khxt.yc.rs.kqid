﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class UserFeedbackEntity : Entity
    {
        public long UserId { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Phone { get; set; }
        public string Content { get; set; }
        public string PicPath { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 租户列表 默认0
        /// </summary>
        public long TenantId { get; set; }

    }
}
