﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    /// <summary>
    /// 用户金币，经验等变更历史
    /// </summary>
    public class UserValueLogEntity : Entity
    {
        public long UserId { get; set; }
        public long Value { get; set; }
        public long LastValue { get; set; }
        public int Category { get; set; }

        public int BusinessType { get; set; }
        public string Description { get; set; }
        public DateTime CreateTime { get; set; }


    }
}
