﻿using Hx.Domain.Entities;
using KhXt.YcRs.Domain.Models.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class UserLeveEntity : Entity
    {
        public long UserId { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public int BusinessType { get; set; }

        public static implicit operator UserLeveEntity(UserLeveModel v)
        {
            throw new NotImplementedException();
        }
    }
}
