﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class UserThreePartyEntity : Entity
    {
        /// <summary>
        /// UserId
        /// </summary>		 
        public long UserId { get; set; }

        /// <summary>
        /// AccounKeyType qq wx 第三方便是类型使用小写字符表示
        /// </summary>		 
        public string AccounKeyType { get; set; }

        /// <summary>
        /// AccessToken
        /// </summary>		 
        public string AccessToken { get; set; }

        /// <summary>
        /// IsDel
        /// </summary>		 
        public int IsDel { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>		 
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// NickName
        /// </summary>		 
        public string NickName { get; set; }

        /// <summary>
        /// Avatar
        /// </summary>		 
        public string Avatar { get; set; }

        public int Sex { get; set; }

    }
}
