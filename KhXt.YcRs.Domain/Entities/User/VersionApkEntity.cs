﻿using Hx.Domain.Entities;
using System;

namespace KhXt.YcRs.Domain.Entities.User
{
    public class VersionApkEntity : Entity
    {
        /// <summary>
        /// 最新版本号
        /// </summary> 
        public string newVersion { get; set; }
        /// <summary>
        /// 最小支持版本号
        /// </summary>
        public string minVersion { get; set; }
        /// <summary>
        /// apk下载url
        /// </summary>
        public string apkUrl { get; set; }
        /// <summary>
        /// 是否有更新
        /// </summary>
        public bool isUpdate { get; set; }
        /// <summary>
        ///是否强制更新
        /// </summary>
        public bool forceUpdate { get; set; }
        /// <summary>
        /// 终端 类型 0 安卓 1 苹果
        /// </summary>
        public int OsSign { get; set; }
        /// <summary>
        /// 更新文案
        /// </summary>
        public string updateDescription { get; set; }
        /// <summary>
        /// apk文件大小
        /// </summary>
        public long apkSize { get; set; }
        /// <summary>
        /// apk的文件MD5值
        /// </summary>
        public string md5 { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

    }
}
