﻿using Hx.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Entities.VipCard
{
    public class VipCardEntity : EntityWithStringKey
    {
        ///<Summary>
        /// 密码
        ///</Summary>
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Category { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }

        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime CreateTime { get; set; }

        public string Remark { get; set; }
    }
}
