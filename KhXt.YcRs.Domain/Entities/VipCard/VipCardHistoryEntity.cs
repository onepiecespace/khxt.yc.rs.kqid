﻿using Hx.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Domain.Entities.VipCard
{
    public class VipCardHistoryEntity : EntityWithStringKey
    {

        public long UserId { get; set; } //用户Id

        public DateTime? Before { get; set; }
        /// <summary>
        /// 本次开卡增加天数
        /// </summary>
        public int Value { get; set; }
        /// <summary>
        /// 开卡后会员到期日期
        /// </summary>
        public DateTime After { get; set; }

        ///<Summary>
        /// 创建时间
        ///</Summary>
        public DateTime CreateTime { get; set; }

        public string Remark { get; set; }

    }
}
