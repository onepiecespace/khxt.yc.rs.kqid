﻿using Hx.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace KhXt.YcRs.Domain.Entities.WalletLogs
{
    public class WalletLogsEntity : Entity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [Required]
        public long UserId { get; set; }
        /// <summary>
        /// 0 充值 1 扣减
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 订单Id
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 余额数量
        /// </summary>
        [Required]
        public float Coin { get; set; }
        /// <summary>
        /// 金币余额
        /// </summary>
        [Required]
        public float CoinBalance { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

    }
}
