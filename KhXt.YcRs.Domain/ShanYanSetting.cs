﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Senparc.CO2NET;
using Senparc.CO2NET.HttpUtility;
using Senparc.CO2NET.RegisterServices;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace KhXt.YcRs.Domain
{
    /// <summary>
    /// 闪验配置
    /// </summary>
    public class ShanYanSetting
    {
        public string apiurl { get; set; }
        public string iosappId { get; set; }
        public string iosappKey { get; set; }
        public string adappId { get; set; }
        public string adappKey { get; set; }

    }
}
