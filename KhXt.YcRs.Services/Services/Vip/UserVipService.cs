﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.VIP;
using KhXt.YcRs.Domain.Models.Vip;
using KhXt.YcRs.Domain.Repositories.Vip;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Vip;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Vip
{
    public class UserVipService : ServiceBase, IUserVipService
    {
        private readonly IDirectMemoryCache _cache;
        private const string VIP_USERVIP_KEY = "uservips";
        private IUserVipRepository _repo;
        public UserVipService(IDirectMemoryCacheManager cacheManager, IUserVipRepository userVipRepository)
        {
            _cache = cacheManager.GetCache("__vip");
            _repo = userVipRepository;
        }
        public long Add(UserVipModel model)
        {
            if (model == null)
                return 0;
            var entity = model.MapTo<UserVipEntity>();
            if (entity == null)
                return 0;
            var rlt = UnitOfWorkService.Execute<long>(() => _repo.InsertAndGetId(entity));
            if (rlt < 1)
                return rlt;
            entity.Id = rlt;
            model = entity.MapTo<UserVipModel>();
            var list = GetAll();
            list.Add(model);
            //Trigger(new UserVipEventData() { Data = model, Method = (int)EventDataMethod.Add });
            return rlt;
        }

        public bool Delete(UserVipModel model)
        {
            if (model == null)
                return false;
            var entity = model.MapTo<UserVipEntity>();
            if (entity == null)
                return false;
            var rlt = UnitOfWorkService.Execute<bool>(() => _repo.Delete(entity));
            if (!rlt)
                return false;
            var list = GetAll();
            var info = list.Find(t => t.Id == entity.Id);
            if (info != null)
                list.Remove(info);
            //Trigger(new UserVipEventData() { Data = model, Method = (int)EventDataMethod.Del });
            return true;
        }

        public List<UserVipModel> GetAll()
        {
            try
            {
                var list = _cache.Get(VIP_USERVIP_KEY, () =>
                {
                    var entityList = UnitOfWorkService.Execute(() => _repo.GetList().ToList());
                    return entityList.MapTo<List<UserVipModel>>();
                }, TimeSpan.FromHours(24));
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("用户VIP缓存异常", ex);
                return null;
            }
        }
        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is UserVipEventData data)) return;
            if (data.Data == null) return;

            var method = (EventDataMethod)data.Method;
            var list = GetAll();
            switch (method)
            {
                case EventDataMethod.Clear:
                    ClearCache();
                    break;
                case EventDataMethod.Add:
                    if (list != null)
                    {
                        list.Add(data.Data);
                    }
                    break;
                case EventDataMethod.Del:
                    if (list != null)
                    {
                        var model = list.Find(t => t.Id == data.Data.Id);
                        if (model != null)
                        {
                            list.Remove(model);
                        }
                    }
                    break;
                case EventDataMethod.Update:
                    if (list != null)
                    {
                        var model = list.Find(t => t.Id == data.Data.Id);
                        if (model != null)
                        {
                            list.Remove(model);
                        }
                        list.Add(data.Data);
                    }
                    break;
            }

        }
        public override void ClearCache()
        {
            _cache.Clear();
            GetAll();
        }

        public async Task<Result<UserVipModel>> GetUserVip(long userId)
        {
            var list = GetAll();
            var info = list.Find(t => t.UserId == userId);
            return await Task.Run(() => new Result<UserVipModel> { Data = info });
        }

        public bool Update(UserVipModel model)
        {
            if (model == null)
                return false;
            var entity = model.MapTo<UserVipEntity>();
            if (entity == null)
                return false;
            var rlt = UnitOfWorkService.Execute<bool>(() => _repo.Update(entity));
            if (!rlt)
                return false;
            var list = GetAll();
            var info = list.Find(t => t.Id == model.Id);
            list.Remove(info);
            list.Add(model);
            //Trigger(new UserVipEventData() { Data = model, Method = (int)EventDataMethod.Update });
            return true;
        }
    }
}
