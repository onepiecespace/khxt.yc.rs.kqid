﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Repositories.Vip;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Vip;
using Hx.Extensions;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Vip
{
    public class VipPrivilegeService : ServiceBase, IVipPrivilegeService
    {
        IVipPrivilegeRepository _repo;
        private readonly string _basePath = Path.Combine("vip", "privilege");
        public VipPrivilegeService(IVipPrivilegeRepository repo)
        {
            _repo = repo;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(VipPrivilegeEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public async Task<List<VipPrivilegeEntity>> GetList(int size)
        {
            var queryList = await UnitOfWorkService.Execute(() => _repo.GetListAsync());
            if (queryList == null || !queryList.Any()) return new List<VipPrivilegeEntity>();
            var list = queryList.ToList();
            foreach (var infoEntity in list)
            {

                var name = $"{infoEntity.Id}.png";
                if (size > 1)
                    name = $"{infoEntity.Id}@{size}x.png";
                var path = Path.Combine(_basePath, infoEntity.VipId.ToString(), name);
                infoEntity.PrivilegeImg = $"{DomainSetting.ResourceUrl}{path}";
            }

            return list;
        }

        public async Task<List<VipPrivilegeEntity>> GetList(long vipId, int size)
        {
            var queryList = await UnitOfWorkService.Execute(() => _repo.GetListAsync());
            if (queryList == null || !queryList.Any()) return new List<VipPrivilegeEntity>();
            var list = queryList.Where(t => t.VipId.Equals(vipId)).ToList();
            foreach (var infoEntity in list)
            {

                var name = $"{infoEntity.Id}.png";
                if (size > 1)
                    name = $"{infoEntity.Id}@{size}x.png";
                var path = Path.Combine(_basePath, infoEntity.VipId.ToString(), name);
                infoEntity.PrivilegeImg = $"{DomainSetting.ResourceUrl}{path}";
            }
            return list;
        }
    }









}
