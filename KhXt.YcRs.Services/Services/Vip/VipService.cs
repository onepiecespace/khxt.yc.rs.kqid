﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Sys;
using KhXt.YcRs.Domain.Models.Vip;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Repositories.Vip;
using KhXt.YcRs.Domain.Repositories.VipCard;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Services.Vip;
using KhXt.YcRs.Domain.Util;
using Hx.Components;
using Hx.Extensions;
using Hx.ObjectMapping;
using Hx.Runtime.Caching.Memory;
using Senparc.Weixin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Vip
{
    public class VipService : ServiceBase, IVipService
    {
        private readonly IVipInfoService _vipInfoService;
        private readonly IVipPrivilegeService _privilegeService;
        private readonly IUserVipService _userVipService;
        private readonly ICouponInfoService _couponInfoService;
        private readonly IUserCouponRepository _userCouponRepository;
        private readonly ICouponInfoRepository _couponInfoRepository;
        private readonly IUserVipHistoryRepository _userVipHistoryRepository;
        private readonly IVipInfoRepository _vipInfoRepository;
        private readonly IUserCacheService _userCacheService;
        private readonly IVipCardHistoryRepository _vipCardHistoryRepository;
        private readonly IContentService _contentService;
        public VipService(IVipInfoService vipInfoService,
            IVipPrivilegeService privilegeService,
            ICouponInfoService couponInfoService,
            IUserVipService userVipService,
            IUserCouponRepository userCouponRepository,
            IUserVipHistoryRepository userVipHistoryRepository,
            ICouponInfoRepository couponInfoRepository,
            IVipInfoRepository vipInfoRepository,
            IUserCacheService userCacheService,
            IVipCardHistoryRepository vipCardHistoryRepository,
            IContentService contentService)
        {
            _vipInfoService = vipInfoService;
            _privilegeService = privilegeService;
            _couponInfoService = couponInfoService;
            _userVipService = userVipService;
            _userVipHistoryRepository = userVipHistoryRepository;
            _vipInfoRepository = vipInfoRepository;
            _userCacheService = userCacheService;
            _userCouponRepository = userCouponRepository;
            _couponInfoRepository = couponInfoRepository;
            _vipCardHistoryRepository = vipCardHistoryRepository;
            _contentService = contentService;
        }

        public async Task<Result<VipServiceInfo>> GetVipServiceInfo(long userId, int size = 1)
        {
            try
            {
                var vipInfos = _vipInfoService.Table();
                if (vipInfos.Any())
                {
                    var privileges = await _privilegeService.GetList(size);
                    var list = await UnitOfWorkService.Execute(async () => await _userCouponRepository.GetListAsync());
                    var queryList = list.AsQueryable();
                    queryList = queryList.Where(t => t.UserId.Equals(userId) &&
                                                     t.Status == (int)CouponStatus.NoUse &&
                                                     t.UseLimit != (int)CouponUseLimit.Share && t.BusinessType == (int)BusinessType.Vip);
                    var couponList = UnitOfWorkService.Execute(() => _couponInfoRepository.GetList()).ToList();
                    var keyValue = new Dictionary<long, List<VipPrivilegeEntity>>();
                    var coupon = new Dictionary<long, Tuple<int, float>>();

                    foreach (var vipInfo in vipInfos)
                    {
                        var value = privileges.Where(t => t.VipId.Equals(vipInfo.Id)).ToList();
                        coupon.Add(vipInfo.Id, GetCount(queryList, couponList, vipInfo.Id));
                        keyValue.Add(vipInfo.Id, value);
                    }
                    //获取会员是否有广告信息
                    var adsResult = await _contentService.GetAdsByTag(KhXt.YcRs.Domain.Models.Sys.ContentType.Vip, "vipcenter");
                    var ads = new List<AdModel>();
                    if (adsResult.Code == (int)ResultCode.Success)
                        ads = adsResult.Data;
                    else
                        ads = null;
                    return new Result<VipServiceInfo>() { Data = new VipServiceInfo() { VipInfo = vipInfos.ToList(), VipPrivileges = keyValue, Coupons = coupon, AdModels = ads } };
                }
                else
                {
                    return new Result<VipServiceInfo>() { Data = new VipServiceInfo() };
                }
            }
            catch (Exception ex)
            {
                Logger.Error("获取会员服务信息异常", ex);
                return new Result<VipServiceInfo>() { Code = (int)ResultCode.Fail, Message = "获取会员服务信息异常！" };

            }
        }

        private Tuple<int, float> GetCount(IQueryable<UserCouponEntity> listUserCouponEntities, List<CouponInfoEntity> couponInfoEntities, long vipType)
        {
            var listUser = listUserCouponEntities.Where(t => t.VipType == vipType);
            if (!listUser.Any())
                return new Tuple<int, float>(0, 0);
            float discount = 5;
            foreach (var item in listUser)
            {
                var tempDiscount = couponInfoEntities.FirstOrDefault(t => t.Id == item.CouponId).Discount.Value;
                if (discount > tempDiscount)
                    discount = tempDiscount;
            }
            return new Tuple<int, float>(1, discount);
        }
        public async Task<Result> AddVipInfo(VipInfoEntity vipInfoEntity)
        {
            try
            {
                var newId = _vipInfoService.Create(vipInfoEntity);
                if (newId != null && newId.ToString().IsNotNullOrEmpty())
                    return await Task.Run(() => new Result() { Data = newId.ToString(), Message = "保存会员信息成功！" });
                else
                    return new Result() { Code = (int)ResultCode.Fail, Message = "保存会员信息失败！" };
            }
            catch (Exception ex)
            {
                Logger.Error("保存会员信息异常", ex);
                return new Result() { Code = (int)ResultCode.Fail, Message = "保存会员信息异常！" };

            }
        }

        public async Task<Result> AddVipPrivilegeInfo(VipPrivilegeEntity vipPrivilegeEntity)
        {
            try
            {
                var newId = _privilegeService.Create(vipPrivilegeEntity);
                if (newId != null && newId.ToString().IsNotNullOrEmpty())
                    return await Task.Run(() => new Result() { Data = newId.ToString(), Message = "保存会员特权信息成功！" });
                else
                    return new Result() { Code = (int)ResultCode.Fail, Message = "保存会员特权信息失败！" };
            }
            catch (Exception ex)
            {
                Logger.Error("保存会员信息异常", ex);
                return new Result() { Code = (int)ResultCode.Fail, Message = "保存会员特权信息异常！" };

            }
        }

        public async Task<Result<UserVipModel>> GetUserVip(long userId)
        {
            try
            {
                var vipinlist = _userVipService.GetAll();
                //var userVip = UnitOfWorkService.Execute(() => _userVipService.GetAll().FirstOrDefault(t => t.UserId == userId));
                var userVip = vipinlist.FirstOrDefault(t => t.UserId == userId);
                if (userVip == null)
                    return new Result<UserVipModel>() { Data = null };
                // var entity = userVip.MapTo<UserVipModel>();
                var rlt = await _vipInfoService.Get(userVip.VipType);
                if (rlt.Code == (int)ResultCode.Success)
                    userVip.VipInfo = rlt.Data;
                return new Result<UserVipModel>() { Data = userVip };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取用户{userId}会员卡异常", ex);
                return new Result<UserVipModel>() { Code = (int)ResultCode.Exception, Message = "获取会员卡异常" };
            }

        }
        public async Task<Result<MyVip>> GetUserCenter(long userId, int size)
        {
            try
            {
                var rlt = await GetUserVip(userId);
                if (rlt.Code != (int)ResultCode.Success)
                    return new Result<MyVip>() { Code = rlt.Code, Message = "获取会员中心失败！" };
                var myVipInfo = rlt.Data;
                if (myVipInfo == null)
                {
                    return new Result<MyVip>() { Code = (int)ResultCode.Fail, Message = "非会员，获取会员中心失败" };
                }
                var rltCouponAll = await _couponInfoService.GetMyCouponAll(userId, (int)CouponStatus.NoUse);
                if (rltCouponAll.Code != (int)ResultCode.Success)
                    return new Result<MyVip>() { Code = (int)ResultCode.Fail, Message = "获取用户优惠券失败！" };
                var myVip = new MyVip
                {
                    UerVipModel = myVipInfo,
                    MyCoupons = rltCouponAll.Data,
                    Privileges = await _privilegeService.GetList(myVipInfo.VipType, size)
                };
                return new Result<MyVip>() { Data = myVip };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取用户{userId}会员中心异常", ex);
                return new Result<MyVip>() { Code = (int)ResultCode.Exception, Message = "获取用户会员中心异常" };
            }
        }

        public async Task<int> OpenVip(string orderId, long userId, long vipId, bool isAuto = false)
        {
            try
            {
                var user = await _userCacheService.GetUser(userId);
                if (user == null) return (int)ResultCode.NoUser;
                var vipInfo = await _vipInfoRepository.FirstOrDefaultAsync(t => t.Id == vipId);
                if (vipInfo == null) return (int)ResultCode.NoVip;
                var userVip = _userVipService.GetAll().FirstOrDefault(t => t.UserId == userId);
                var startTime = userVip == null ? DateTime.Now : userVip.ExpiredTime;
                var expiredTime = startTime.AddDays(vipInfo.ExpiredDays);
                if (userVip == null)
                {
                    userVip = new UserVipModel
                    {
                        OrderId = orderId,
                        UserId = user.Userid,
                        VipType = vipInfo.Id,
                        IsFrom = (int)VipFrom.IsFee,
                        IsAutoRenew = isAuto,
                        BeginTime = DateTime.Now,
                        ExpiredTime = expiredTime,
                        ExchangeId = "",
                        CreateTime = DateTime.Now,
                        UpdateTime = DateTime.Now
                    };
                    if (userVip.IsAutoRenew)
                        userVip.AutoRenewTime = expiredTime;
                    _userVipService.Add(userVip);
                }
                else
                {
                    //ToDo:会员卡覆盖策略
                    if (vipInfo.Id > userVip.VipType)
                        userVip.VipType = vipInfo.Id;
                    if (isAuto)
                        userVip.AutoRenewTime = expiredTime;
                    userVip.ExpiredTime = expiredTime;
                    userVip.UpdateTime = DateTime.Now;
                    userVip.OrderId = orderId;
                    _userVipService.Update(userVip);
                }
                //插入优惠券
                if (vipInfo.Coupon != null)
                {
                    var userCouponEntities = new List<UserCouponEntity>();
                    var vipCoupons = vipInfo.Coupon.Coupons;
                    foreach (var vipCoupon in vipCoupons)
                    {
                        var couponInfo = _couponInfoRepository.Get(vipCoupon.CouponId);
                        for (var i = 0; i < vipCoupon.Count; i++)
                        {
                            var entity = couponInfo.MapTo<UserCouponEntity>();
                            entity.CouponId = couponInfo.Id;
                            entity.CouponNo = RandomHelper.GenerateRandom(10);
                            entity.IsFrom = 0;
                            entity.UserId = userId;
                            entity.ReceiveTime = DateTime.Now;
                            if (couponInfo.IsFixedDay == 1)
                                entity.ExpiredTime = entity.ReceiveTime.AddDays(couponInfo.ExpiredDays);
                            entity.Status = 0;
                            userCouponEntities.Add(entity);
                        }

                    }
                    _userCouponRepository.BatchInsert(userCouponEntities);
                }
                else
                {
                    Logger.Info($"vip未设置优惠券信息，订单信息{orderId},{userId}");
                }

                //插入 user_vip_history
                var userVipHis = userVip.MapTo<UserVipHistoryEntity>();

                userVipHis.IsSuccess = true;
                _userVipHistoryRepository.InsertAndGetId(userVipHis);
                //更新用户 会员信息 
                user.Category = (int)userVip.VipType;
                user.IsMember = (int)userVip.VipType;
                user.MemberDueDate = startTime.AddDays(vipInfo.ExpiredDays);
                var rlt = Update(user);
                Logger.Info($"开通用户VIP成功{Newtonsoft.Json.JsonConvert.SerializeObject(user)}");

                if (!rlt)
                    Logger.Error($"更新用户表会员信息失败,用户{user.Userid}，vip等级{user.IsMember }");
                return 200;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("开通会员异常", ex);
                return (int)ResultCode.OpenVipException;
            }
        }

        bool Update(UserBaseInfo Info)
        {

            var repo = ObjectContainer.Resolve<IUserRepository>();
            var entity = Info.MapTo<UserEntity>();
            var rlt = UnitOfWorkService.Execute<bool>(() =>
            {
                entity.Headurl = RemoveResUrl(entity.Headurl);
                return repo.Update(entity);
            });
            if (rlt) _userCacheService.Update(Info);
            return rlt;
        }

        //public async Task<Result> SyncVipCard()
        //{

        //    try
        //    {
        //        var call = await UnitOfWorkService.ExecuteAsync(async () =>
        //         {
        //             var vipCardHistorys = await _vipCardHistoryRepository.GetListAsync();
        //             var vips = _userVipService.GetAll().;
        //             var userVips = new List<UserVipEntity>();
        //             var userVipHistories = new List<UserVipHistoryEntity>();
        //             foreach (var item in vipCardHistorys)
        //             {
        //                 if (vips.Exists(t => t.UserId == item.UserId))
        //                     continue;
        //                 var userVip = new UserVipEntity
        //                 {
        //                     OrderId = "",
        //                     UserId = item.UserId,
        //                     VipType = 2,
        //                     IsFrom = (int)VipFrom.IsVipCard,
        //                     IsAutoRenew = false,
        //                     BeginTime = DateTime.Now,
        //                     ExpiredTime = item.After,
        //                     ExchangeId = item.Id,
        //                     CreateTime = item.CreateTime,
        //                     UpdateTime = item.CreateTime
        //                 };
        //                 userVips.Add(userVip);
        //                 //插入 user_vip_history
        //                 var userVipHis = userVip.MapTo<UserVipHistoryEntity>();
        //                 userVipHis.IsSuccess = true;
        //                 userVipHistories.Add(userVipHis);
        //             }
        //             //_userVipRepository.BatchInsert(userVips);
        //             _userVipHistoryRepository.BatchInsert(userVipHistories);
        //             return true;
        //         });
        //        return new Result { Message = "同步完成" };
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //        Logger.Error("开通会员异常", ex);
        //        return new Result { Code = (int)ResultCode.Exception, Message = ex.Message };
        //    }
        //}
    }
}
