﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.Models.Vip;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Repositories.Vip;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Vip;
using Hx.Extensions;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Vip
{
    public class VipInfoService : ServiceBase, IVipInfoService
    {
        IVipInfoRepository _repo;
        private readonly IDirectMemoryCache _cache;
        private IIOSPriceRepository _iosPriceRepository;
        private readonly string _VipInfoCacheKey = "VipInfo";
        private readonly string _basePath = Path.Combine("vip", "card", "bg");
        public VipInfoService(IVipInfoRepository repo, IIOSPriceRepository iosPriceRepository, IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _iosPriceRepository = iosPriceRepository;
            _cache = cacheManager.GetCache("__VipInfoCache");
        }

        #region 复用缓存查询方法
        public async Task<List<VipInfo>> GetAll()
        {
            try
            {
                var rlt = _cache.Get(_VipInfoCacheKey, async () =>
                {
                    var body = await UnitOfWorkService.Execute(async () =>
                    {
                        var list = await _repo.GetListAsync();
                        var taskList = list.MapTo<List<VipInfo>>();
                        return taskList;
                    });
                    return body;
                }, TimeSpan.FromHours(8));

                return await rlt;
            }
            catch (Exception ex)
            {
                Logger.Error("vip列表缓存异常", ex);
                return null;
            }

        }

        #endregion


        public IQueryable<VipInfoEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<VipInfoEntity>>(() =>
            {
                return _repo.GetList().AsQueryable();
            });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(VipInfoEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }
        public async Task<Result<VipInfo>> Get(long vipId)
        {
            try
            {
                var vipinlist = await GetAll();
                var vipInfo = vipinlist.FirstOrDefault(t => t.Id == vipId);
                // var entity = await UnitOfWorkService.Execute(async () => await _repo.GetAsync(vipId));
                if (vipInfo == null)
                    return new Result<VipInfo> { Code = (int)ResultCode.Fail, Message = "获取会员卡信息失败" };

                // var vipInfo = entity.MapTo<VipInfo>();
                if (vipInfo.IOSRenewPriceId.IsNotNullOrEmpty())
                    switch (vipInfo.IOSRenewPriceId)
                    {

                        case "vipMonth":
                            vipInfo.IOSRenewPrice = 158;
                            break;
                        case "vipYear":
                            vipInfo.IOSRenewPrice = 998;
                            break;
                        case "vipHalfYear":
                            vipInfo.IOSRenewPrice = 698;
                            break;
                        default:
                            vipInfo.IOSRenewPrice = 1;
                            break;
                    }

                //vipInfo.IOSRenewPrice = UnitOfWorkService.Execute(() =>
                //    {
                //        var price = _iosPriceRepository.FirstOrDefault(t => t.ProductId.Equals(vipInfo.IOSRenewPriceId));
                //        return price == null ? 0 : price.Price;
                //    });
                if (vipInfo.IOSSalePriceId.IsNotNullOrEmpty())
                    switch (vipInfo.IOSSalePriceId)
                    {

                        case "vipMonth":
                            vipInfo.IOSSalePrice = 158;
                            break;
                        case "vipYear":
                            vipInfo.IOSSalePrice = 998;
                            break;
                        case "vipHalfYear":
                            vipInfo.IOSSalePrice = 698;
                            break;
                        default:
                            vipInfo.IOSSalePrice = 1;
                            break;
                    }
                //vipInfo.IOSSalePrice = UnitOfWorkService.Execute(() =>
                //{
                //    var price = _iosPriceRepository.FirstOrDefault(t => t.ProductId.Equals(vipInfo.IOSSalePriceId));
                //    return price == null ? 0 : price.Price;
                //});
                var bgPath = new string[3];
                bgPath[0] = DomainSetting.ResourceUrl + Path.Combine(_basePath, $"{vipInfo.Id}.png");
                bgPath[1] = DomainSetting.ResourceUrl + Path.Combine(_basePath, $"{vipInfo.Id}@2x.png");
                bgPath[2] = DomainSetting.ResourceUrl + Path.Combine(_basePath, $"{vipInfo.Id}@3x.png");
                vipInfo.BgPath = bgPath;
                return new Result<VipInfo> { Data = vipInfo };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取会员卡{vipId}信息异常", ex);
                throw;
            }
        }
    }









}
