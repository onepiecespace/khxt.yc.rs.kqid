﻿
using Aliyun.OSS;
using Aliyun.OSS.Common;
using Castle.Core.Internal;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Files;
using KhXt.YcRs.Domain.Util;
using Hx.Runtime.Caching.Memory;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Files
{
    public partial class FilesService : ServiceBase, IFilesService
    {
        private readonly string _bucketName;
        private readonly OssClient _client;
        private readonly string _rootPath;
        private readonly IDirectMemoryCache _cache;

        public FilesService(OssOptions optionsAccessor, IDirectMemoryCacheManager cacheManager)
        {
            _rootPath = DomainSetting.UploadPath;
            var endpoint = optionsAccessor.EndPoint;
            var accessKeyId = optionsAccessor.AccessKeyId;
            var accessKeySecret = optionsAccessor.AccessKeySecret;
            _bucketName = optionsAccessor.BucketName;
            _client = new OssClient(endpoint, accessKeyId, accessKeySecret);
            _cache = cacheManager.GetCache("__fileCache");
        }

        public async Task<Result> DownLoadFiles(string filePath)
        {
            throw new NotImplementedException();
        }

        public Task<Result> MoveFileToOtherPath(string oldFilePath, string filePath, string fileName)
        {
            try
            {
                if (oldFilePath.Contains(DomainSetting.ResourceUrl))
                    oldFilePath = oldFilePath.Replace(DomainSetting.ResourceUrl, "").Trim();
                if (oldFilePath.StartsWith("\\"))
                    oldFilePath = oldFilePath.TrimStart('\\');
                if (oldFilePath.StartsWith("/"))
                    oldFilePath = oldFilePath.TrimStart('/');
                //创建一个FileInfo对象
                var file = new FileInfo(oldFilePath);
                var isHas = FileExist(oldFilePath);
                var newFilePath = Path.Combine(filePath, $"{fileName + file.Extension}");
                newFilePath = DealPathToOssPath(newFilePath);
                if (oldFilePath == newFilePath || oldFilePath.Substring(0, oldFilePath.LastIndexOf('/')) == newFilePath.Substring(0, newFilePath.LastIndexOf('/')))
                    return Task.FromResult(new Result() { Code = (int)ResultCode.Success, Message = oldFilePath });
                if (isHas)
                {
                    MoveFileAndDel(oldFilePath, newFilePath);
                    return Task.FromResult(new Result() { Code = (int)ResultCode.Success, Message = newFilePath });
                }
                else
                {
                    return Task.FromResult(new Result() { Code = (int)ResultCode.Fail, Message = "移动文件失败！" });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("移动文件异常！", ex);
                return Task.FromResult(new Result() { Code = (int)ResultCode.Exception, Message = "移动文件异常" });
            }

        }

        private string DealPathToOssPath(string filePath)
        {
            if (filePath.StartsWith("\\"))
                filePath = filePath.TrimStart('\\');
            if (filePath.StartsWith("/"))
                filePath = filePath.TrimStart('/');
            if (filePath.Contains("\\"))
                filePath = filePath.Replace("\\", "/");
            if (filePath.StartsWith('/'))
                filePath = filePath.TrimStart('/');
            return filePath;
        }
        /// <summary>
        /// 上传文件到指定目录下
        /// </summary>
        /// <param name="fileData"></param>
        /// <param name="filePath">虚拟目录 img/xx/1.xx 或者 1.xxx </param>
        /// <returns></returns>
        public async Task<Result> UploadFiles(IFormFile fileData, string filePath)
        {
            MemoryStream ms = null;
            try
            {
                if (filePath.Contains(DomainSetting.UploadPath))
                    filePath = filePath.Replace(DomainSetting.UploadPath, "").Trim();
                if (filePath.StartsWith("\\"))
                    filePath = filePath.TrimStart('\\');
                if (filePath.StartsWith("/"))
                    filePath = filePath.TrimStart('/');
                if (filePath.Contains("\\"))
                    filePath = filePath.Replace("\\", "/");
                if (filePath.StartsWith('/'))
                    filePath = filePath.TrimStart('/');
                if (fileData == null || fileData.Name.IsNullOrEmpty() || fileData.Length <= 0)
                    return new Result() { Code = (int)ResultCode.Fail, Message = "文件为空！" };
                using (ms = new MemoryStream())
                {
                    fileData.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    PutObjectFromFile(filePath, ms);
                }

                filePath = $"{DomainSetting.ResourceUrl}/{filePath}";
                return await Task.FromResult(new Result() { Message = "上传成功", Data = filePath });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("上传文件失败！", ex);
                throw;
            }
            finally
            {
                ms?.Close();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<Result> UploadFiles(Stream stream, string filePath)
        {
            try
            {
                if (filePath.Contains(DomainSetting.UploadPath))
                    filePath = filePath.Replace(DomainSetting.UploadPath, "").Trim();
                if (filePath.StartsWith("\\"))
                    filePath = filePath.TrimStart('\\');
                if (filePath.StartsWith("/"))
                    filePath = filePath.TrimStart('/');
                if (filePath.Contains("\\"))
                    filePath = filePath.Replace("\\", "/");
                if (filePath.StartsWith('/'))
                    filePath = filePath.TrimStart('/');
                if (stream == null || !stream.CanRead)
                    return new Result() { Code = (int)ResultCode.Fail, Message = "文件为空！" };
                stream.Seek(0, SeekOrigin.Begin);
                PutObjectFromFile(filePath, stream);
                filePath = $"{DomainSetting.ResourceUrl}/{filePath}";
                return await Task.FromResult(new Result() { Message = "上传成功", Data = filePath });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("上传文件失败！", ex);
                throw;
            }
            finally
            {
                stream?.Close();
            }
        }
        public async Task<Result> LocalUploadLFiles(IFormFile fileData, string filePath)
        {
            FileStream fs = null;
            try
            {
                if (filePath.StartsWith("\\"))
                    filePath = filePath.TrimStart('\\');
                if (filePath.StartsWith("/"))
                    filePath = filePath.TrimStart('/');
                filePath = Path.Combine(_rootPath, filePath);
                if (fileData == null || fileData.Name.IsNullOrEmpty() || fileData.Length <= 0)
                    return new Result() { Code = (int)ResultCode.Fail, Message = "文件为空！" };

                //创建一个FileInfo对象
                FileInfo file = new FileInfo(filePath);
                if (file.Directory != null && !file.Directory.Exists)
                    file.Directory.Create();
                using (fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    await fileData.CopyToAsync(fs);
                }
                filePath = $"{DomainSetting.ResourceUrl}{filePath.Substring(_rootPath.Length)}";
                return await Task.FromResult(new Result() { Message = "上传成功", Data = filePath });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("上传文件失败！", ex);
                throw;
            }
            finally
            {
                fs?.Close();
            }

        }

        public async Task<Result> UploadFilePic(IFormFile fileData, string filePath)
        {
            return await UploadFiles(fileData, filePath);
        }


        //本地路径返回文件名
        public static string GetShorterFileName(string filename)
        {
            int index = filename.LastIndexOf('/');
            return filename.Substring(index + 1, filename.Length - index - 1);

        }


        /// <summary>
        /// 上传文件到指定目录下
        /// </summary>
        /// <param name="fileData"></param>
        /// <param name="filePath">虚拟目录 img/xx/1.xx 或者 1.xxx </param>
        /// <returns></returns>
        public async Task<Result> UploadBase64ToImg(string base64, string filePath)
        {
            MemoryStream ms = null;
            try
            {
                if (filePath.Contains(DomainSetting.UploadPath))
                    filePath = filePath.Replace(DomainSetting.UploadPath, "").Trim();
                if (filePath.StartsWith("\\"))
                    filePath = filePath.TrimStart('\\');
                if (filePath.StartsWith("/"))
                    filePath = filePath.TrimStart('/');
                if (filePath.Contains("\\"))
                    filePath = filePath.Replace("\\", "/");
                if (filePath.StartsWith('/'))
                    filePath = filePath.TrimStart('/');
                byte[] bytes = Convert.FromBase64String(base64.Split(',')[1]);

                using (ms = new MemoryStream(bytes))
                {
                    ms.Write(bytes, 0, bytes.Length);
                    ms.Seek(0, SeekOrigin.Begin);
                    PutObjectFromFile(filePath, ms);
                }

                filePath = $"{DomainSetting.ResourceUrl}/{filePath}";
                return await Task.FromResult(new Result() { Message = "上传成功", Data = filePath });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("上传文件失败！", ex);
                throw;
            }
            finally
            {
                ms?.Close();
            }

        }

        public async Task<Result> SavePicture(string picString, string name, string picString1, string name1, string picString2, string name2)
        {
            try
            {
                //////测试
                //var ServerPath = AppDomain.CurrentDomain.BaseDirectory + "img/collegecourse/";

                // 正式
                var ServerPath = _rootPath + "img/collegecourse/";

                var PathPhone = ServerPath + name;
                var PathPhoneMini = ServerPath + name1;
                var PathContent = ServerPath + name2;

                if (File.Exists(PathPhone) || File.Exists(PathPhoneMini) || File.Exists(PathContent))
                {
                    if (File.Exists(PathPhone))
                    {
                        File.Delete(PathPhone);
                    }
                    if (File.Exists(PathPhoneMini))
                    {
                        File.Delete(PathPhoneMini);
                    }
                    if (File.Exists(PathContent))
                    {
                        File.Delete(PathContent);
                    }
                }

                var fileName = name;
                var Path = ServerPath;
                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                }
                if (picString != null)
                {
                    var tmpArr = picString.Split(',');
                    byte[] bytes = Convert.FromBase64String(tmpArr[1]);
                    MemoryStream ms = new MemoryStream(bytes);
                    ms.Write(bytes, 0, bytes.Length);
                    var img = Image.FromStream(ms, true);
                    img.Save(Path + fileName);
                }
                if (picString1 != null)
                {
                    var tmpArr1 = picString1.Split(',');
                    byte[] bytes1 = Convert.FromBase64String(tmpArr1[1]);
                    MemoryStream ms1 = new MemoryStream(bytes1);
                    ms1.Write(bytes1, 0, bytes1.Length);
                    var img1 = Image.FromStream(ms1, true);
                    img1.Save(Path + name1);

                }
                if (picString2 != null)
                {
                    var tmpArr2 = picString2.Split(',');
                    byte[] bytes2 = Convert.FromBase64String(tmpArr2[1]);
                    MemoryStream ms2 = new MemoryStream(bytes2);
                    ms2.Write(bytes2, 0, bytes2.Length);
                    var img2 = Image.FromStream(ms2, true);
                    img2.Save(Path + name2);
                }
                var showserverurl = DomainSetting.ResourceUrl + "img/collegecourse/";
                string msg = showserverurl + fileName + "|" + showserverurl + name1 + "|" + showserverurl + name2;

                // string msg = showserverurl + "81_1.jpg" + "|" + showserverurl + "81_1.jpg" + "|" + showserverurl + "81_1.jpg";
                // string msg = ServerPath + fileName + "|" + name1 + "|" + name2;
                return await Task.FromResult(new Result() { Message = "上传成功", Data = msg });
            }
            catch (Exception ex)
            {
                return await Task.FromResult(new Result() { Message = ex.ToString() });
            }
        }


        public async Task<Result> SavePictureOptimization(string imgStrame, string imgName, string smallImgStrame, string samllImgName, string modularName, int categorynum)
        {
            try
            {
                #region 正式  根据模块名来确定传到那个文件夹

                var ServerPath = "";
                if (modularName == "content")
                {
                    ServerPath = _rootPath + "img/category/" + categorynum;
                }
                if (modularName == "joinschool")
                {
                    ServerPath = _rootPath + "temp";
                }

                #endregion

                var resultupload = new Result();
                switch (modularName)
                {
                    case "content":
                        resultupload = CaseContent(ServerPath, imgName, imgStrame, samllImgName, smallImgStrame);
                        return resultupload;

                    case "joinschool":
                        resultupload = CaseJoinSschool(ServerPath, imgName, imgStrame);
                        return resultupload;
                    default:
                        break;
                }

                return await Task.FromResult(new Result() { Message = resultupload.Message, Data = resultupload.Data.ToString() });
            }
            catch (Exception ex)
            {
                return await Task.FromResult(new Result() { Message = ex.ToString() });
            }
        }

        private Result CaseContent(string ServerPath, string imgName, string imgStrame, string smallImgStrame, string samllImgName)
        {
            var Path = ServerPath;
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }

            var PathPhone = ServerPath + imgName;

            if (File.Exists(PathPhone))
            {
                return new Result { Code = (int)ResultCode.Fail, Message = "服务器已存在图片，请更换图片名称或重新上传图片", Data = imgName };
            }

            var tmpArr = imgStrame.Split(',');
            byte[] bytes = Convert.FromBase64String(tmpArr[1]);
            MemoryStream ms = new MemoryStream(bytes);
            ms.Write(bytes, 0, bytes.Length);
            var img = Image.FromStream(ms, true);
            img.Save(Path + imgName);

            if (samllImgName != "" && samllImgName != null)
            {
                PathPhone = ServerPath + samllImgName;

                if (File.Exists(PathPhone))
                {
                    return new Result { Code = (int)ResultCode.Fail, Message = "服务器已存在图片，请更换图片名称或重新上传图片", Data = samllImgName };
                }

                tmpArr = smallImgStrame.Split(',');
                bytes = Convert.FromBase64String(tmpArr[1]);
                ms = new MemoryStream(bytes);
                ms.Write(bytes, 0, bytes.Length);
                img = Image.FromStream(ms, true);
                img.Save(Path + samllImgName);
            }

            //string msg = Path + imgName + "-" + samllImgName;
            //返回数据查看（正式/或197)
            string msg = DomainSetting.ResourceUrl + "temp" + samllImgName;
            return new Result() { Message = "上传成功", Data = msg };
        }


        private Result CaseJoinSschool(string ServerPath, string imgName, string imgStrame)
        {
            var sPath = ServerPath;
            if (!Directory.Exists(sPath))
            {
                Directory.CreateDirectory(sPath);
            }
            var startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1), TimeZoneInfo.Local); // 当地时区
            var timeStamp = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数 
                                                                                // var path = Path.Combine(sPath, timeStamp + RandomHelper.GenerateRandomCode(5) + imgName);
            var newimgName = timeStamp + RandomHelper.GenerateRandomCode(5) + imgName;

            // var PathPhone = ServerPath + newimgName;

            var tmpArr = imgStrame.Split(',');
            byte[] bytes = Convert.FromBase64String(tmpArr[1]);
            MemoryStream ms = new MemoryStream(bytes);
            ms.Write(bytes, 0, bytes.Length);
            var img = Image.FromStream(ms, true);
            img.Save(sPath + newimgName);

            //string msg = sPath + newimgName;
            //返回数据查看（正式/或197)
            string msg = DomainSetting.ResourceUrl + "temp" + newimgName;
            return new Result() { Message = "上传成功", Data = msg };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromPath"></param>
        /// <param name="toPath"></param>
        /// <returns></returns>
        public async Task<Result> MoveDirToOss(string fromPath, string toPath)
        {
            try
            {
                var count = 0;
                var pathList = new List<string>();
                var temp = new List<string>();
                Director(fromPath, pathList);
                foreach (var key in pathList)
                {
                    var path = key.Replace(fromPath, "").Trim();
                    path = $"{DealPathToOssPath(toPath)}/{ DealPathToOssPath(path)}";
                    if (!FileExist(path))
                    {
                        PutObjectFromFile(path, key);
                        temp.Add($"{key}→{path}");
                        count++;
                    }
                }
                return await Task.FromResult(new Result() { Code = (int)ResultCode.Success, Data = Newtonsoft.Json.JsonConvert.SerializeObject(temp), Message = $"迁移文件{count}个" });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"移动服务器文件异常,路径{fromPath}到Oss指定路径{toPath}！", ex);
                throw;
            }
        }

        private void Director(string path, List<string> pathList)
        {
            var theFolder = new DirectoryInfo(path);
            if (!theFolder.Exists)
                return;
            //遍历文件
            foreach (var nextFile in theFolder.GetFiles())
            {
                if (!nextFile.Exists)
                    continue;
                pathList.Add(nextFile.FullName);
            }

            //遍历文件夹
            foreach (var nextFolder in theFolder.GetDirectories())
            {
                Director(nextFolder.FullName, pathList);
            }
        }

        public async Task<Result> MoveFileToOss(string fromFileName, string toFileName)
        {
            try
            {
                var file = new FileInfo(fromFileName);
                if (file.Directory == null || !file.Directory.Exists || !file.Exists)
                    return new Result { Code = (int)ResultCode.Fail, Message = "文件不存在" };
                toFileName = DealPathToOssPath(toFileName);
                PutObjectFromFile(toFileName, fromFileName);
                return await Task.FromResult(new Result() { Code = (int)ResultCode.Success, Data = $"移动服务器文件成功,路径{fromFileName}到Oss指定路径{toFileName}！", Message = $"迁移文件成功" });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"移动服务器文件异常,路径{fromFileName}到Oss指定路径{toFileName}！", ex);
                throw;
            }
        }

        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="folderPath"></param>
        private void CreateEmptyFolder(string folderPath)
        {
            try
            {
                // put object with zero bytes stream.
                using (var memStream = new MemoryStream())
                {
                    _client.PutObject(_bucketName, folderPath, memStream);
                    Console.WriteLine("创建文件夹:{0} 成功", folderPath);
                }
            }
            catch (OssException ex)
            {
                Console.WriteLine("创建文件夹失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("上传文件失败: {0}", ex.Message);
                throw;
            }
        }


        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="folderPath">image/1.png 或者 1.png</param>
        /// <param name="filePath">完整文件路径</param>
        private void PutObjectFromFile(string folderPath, string filePath)
        {
            try
            {
                _client.PutObject(_bucketName, folderPath, filePath);
                Console.WriteLine("上传文件:{0} 成功", folderPath);
            }
            catch (OssException ex)
            {
                Console.WriteLine("上传文件失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("上传文件失败: {0}", ex.Message);
                throw;
            }

        }
        /// <summary>
        /// 上传文件 流方式
        /// </summary>
        /// <param name="folderPath">image/1.png 或者 1.png</param>
        /// <param name="stream">文件流</param>
        private void PutObjectFromFile(string folderPath, Stream stream)
        {
            try
            {
                _client.PutObject(_bucketName, folderPath, stream);
                Console.WriteLine("上传文件:{0} 成功", folderPath);
            }
            catch (OssException ex)
            {
                Console.WriteLine("上传文件失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("上传文件失败: {0}", ex.Message);
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromPath"></param>
        /// <param name="toPath"></param>
        private void MoveFileAndDel(string fromPath, string toPath)
        {
            try
            {
                var metadata = new ObjectMetadata();
                metadata.AddHeader("mk1", "mv1");
                metadata.AddHeader("mk2", "mv2");
                var req = new CopyObjectRequest(_bucketName, fromPath, _bucketName, toPath)
                {
                    NewObjectMetadata = metadata
                };
                _client.CopyObject(req);
                DeleteFile(fromPath);
                Console.WriteLine("移动文件成功");
            }
            catch (OssException ex)
            {
                Console.WriteLine("移动文件失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("移动文件失败，错误信息: {0}", ex.Message);
                throw;
            }
        }

        private void DeleteFile(string fileName)
        {
            try
            {
                _client.DeleteObject(_bucketName, fileName);
                Console.WriteLine("删除文件成功");
            }
            catch (OssException ex)
            {
                Console.WriteLine("删除文件失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("删除文件失败，错误信息: {0}", ex.Message);
                throw;
            }
        }

        private bool FileExist(string filePath)
        {
            try
            {
                return _client.DoesObjectExist(_bucketName, filePath);
            }
            catch (OssException ex)
            {
                Console.WriteLine("获取文件是否存在失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("获取文件是否存在失败，错误信息: {0}", ex.Message);
                throw;
            }
        }

        private OssObject GetObject(string filePath)
        {
            try
            {
                return _client.GetObject(_bucketName, filePath);
            }
            catch (OssException ex)
            {
                Console.WriteLine("获取文件信息失败，错误代码: {0}; 错误信息: {1}. \nRequestID:{2}\tHostID:{3}",
                    ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("获取文件信息失败，错误信息: {0}", ex.Message);
                throw;
            }
        }
        private readonly string _cacheKey = "uploadChunk";
        public async Task<Result> UploadFileChunk(string uploadId, int chunk, int chunks, Stream fileStream, string filePath)
        {
            try
            {
                byte[] bytes = new byte[fileStream.Length];
                fileStream.Read(bytes, 0, bytes.Length);
                var cacheKey = _cacheKey + chunk + "_" + uploadId;
                var isSucc = await Redis.HashSetWithTypeAsync<byte[]>(cacheKey, chunk.ToString(), bytes);
                if (isSucc)
                    //return await Task.Run(() => new Result { Data = uploadId });
                    return new Result { Data = uploadId };
                else
                    return new Result { Code = (int)ResultCode.Fail, Message = "上传失败" };
            }
            catch
            {
                throw;
            }
        }

        public async Task<Result> SaveToFile(string uploadId, string path, int chunks)
        {
            try
            {
                var filesize = 0;
                //创建一个FileInfo对象
                var file = new FileInfo(path);
                //创建文件
                FileStream fs = file.Create();
                for (int i = 0; i < chunks; i++)
                {
                    var cacheKey = _cacheKey + i + "_" + uploadId;
                    byte[] bufferByRedis = await Redis.HashGetWithTypeAsync<byte[]>(cacheKey, i.ToString());
                    if (bufferByRedis == null)
                    {
                        return new Result { Code = (int)ResultCode.Fail, Message = "数据为空" };
                    }
                    //写入二进制流
                    fs.Write(bufferByRedis, 0, bufferByRedis.Length);
                    filesize += bufferByRedis.Length;
                    await Redis.HashDeleteAsync(cacheKey, i.ToString());
                }
                //关闭文件流
                fs.Close();
                return new Result { Message = filesize.ToString(), Data = uploadId };
            }
            catch
            {
                throw;
            }
        }

        public async Task<Result> RemoveChunk(string uploadId, int chunks)
        {
            try
            {
                for (int i = 0; i < chunks; i++)
                {
                    var cacheKey = _cacheKey + i + "_" + uploadId;
                    await Redis.HashDeleteAsync(cacheKey, i.ToString());
                }
                return new Result { Data = uploadId };
            }
            catch (Exception ex)
            {
                Logger.Error($"删除分片异常{uploadId}", ex);
                throw;
            }
        }
    }

}
