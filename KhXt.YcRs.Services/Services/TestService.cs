﻿using KhXt.YcRs.Domain.Entities;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.Test;
using KhXt.YcRs.Domain.Models.Test;
using KhXt.YcRs.Domain.Repositories;
using KhXt.YcRs.Domain.Services;
using Hx;
using Hx.Components;
using Hx.Extensions;
using Hx.MongoDb;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services
{
    public class TestService : ServiceBase, ITestService
    {
        ITestRepository _repo;

        IMongoUnitOfWorkService _mongoUow;
        IMongoTestRepository _mongoTestRepo;
        IDirectMemoryCache _cache;
        public TestService(ITestRepository repo, IMongoUnitOfWorkService mongoUow, IMongoTestRepository mongoTestRepo, IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _mongoUow = mongoUow;
            _mongoTestRepo = mongoTestRepo;
            _cache = cacheManager.GetCache("_testCache");
        }

        public async Task<string> GetSomeBody(int id)
        {
            //使用repo时也不用构造注入，而是通过下面语句获取：
            //_repo = ObjectContainer.Resolve<ITestRepository>();
            var path = "/read/av/797/10100118/temp/2020-02-18/0115247.pcm";
            path = OssUrl(path);



            var body = await UnitOfWorkService.Execute(async () =>
            {
                var all = _repo.GetList();  //此方法内部多处调用repo,但是在一个上下文中，实现事务管理
                var first = all.FirstOrDefault();
                var rlt = await _repo.GetBody(id);
                rlt = rlt.IsNullOrEmpty() ? "body is null" : $"body which id {id} is :{rlt}";
                rlt = $"{rlt},first :{first.Id},{first.Name}";
                return rlt;
            });
            return body;
        }

        public List<PersonModel> AllPerson()
        {

            var persons = _cache.Get("persons", () =>
              {
                  return _mongoUow.Execute(() =>
                  {
                      return _mongoTestRepo.GetList().MapTo<List<PersonModel>>();
                  });
              }, TimeSpan.FromSeconds(60));
            return persons;
        }

        protected override void DoSync(IDomainEventData eventData)
        {
            if (eventData is PersonCreatedEventData || eventData is PersonChangedEventData)
            {
                _cache.Clear();
            }
        }

        public async Task<Outcome<bool>> MongoDemo()
        {
            var person = new PersonEntity { Id = "1", Name = "jaden", Age = 18, TimeStamp = DateTime.Now };
            var nid = _mongoUow.Execute(() =>
            {
                return _mongoTestRepo.InsertAndGetId(person);
            });
            Console.WriteLine($"new id should be 1?{nid}");
            var rlt = _mongoUow.Execute(() =>
             {
                 var entity = _mongoTestRepo.Get("1");
                 Console.WriteLine($"{entity.Name},{entity.Age}");
                 return _mongoTestRepo.Delete("1");
             });
            return await Task.FromResult(new Outcome<bool>(body: rlt));
        }

        public string Add(PersonModel model)
        {
            //var entity = new PersonEntity { Id = Hx.Util.GetRandomString(5), Name = Hx.Util.GetRandomString(8), Age = Hx.Util.GetRandomNumber(18, 40), Sex = false };
            var entity = model.MapTo<PersonEntity>();
            var rlt = _mongoUow.Execute(() =>
            {
                return _mongoTestRepo.InsertAndGetId(entity);
            });
            if (rlt.IsNotNullOrEmpty())
            {
                _cache.Clear();
                //Trigger(new PersonCreatedEventData { Data = rlt });
            }
            return rlt;
        }

        public List<PersonModel> GetPersons(out bool isFromCache)
        {

            var flag = true;
            var persons = _cache.Get("persons", () =>
            {
                flag = false;
                return _mongoUow.Execute(() =>
                {
                    return _mongoTestRepo.GetList().MapTo<List<PersonModel>>();
                });
            }, TimeSpan.FromSeconds(10));
            isFromCache = flag;
            return persons;
        }

        public override void ClearCache()
        {
            Console.WriteLine("TestService cache cleared");
            _cache.Clear();
        }

        public override async Task<string> DebugAsync()
        {
            var persons = _cache.Get<List<PersonModel>>("persons");
            if (persons == null) return "cache空";
            var str = Serializer.Serialize(persons);
            return await Task.FromResult(str);
        }

        public override async Task InitAsync()
        {
            await "TestService 初始化".ConsoleWriteLineAsync();
        }
    }
}
