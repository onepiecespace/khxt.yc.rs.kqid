﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Repositories.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Course
{
    public class UserCourseChildProgessService : ServiceBase, IUserCourseChildProgessService
    {
        IUserCourseChildProgessRepository _repo;
        private readonly IDirectMemoryCache _cache;
        private readonly string _collegeCacheKey = "UserChild";

        public UserCourseChildProgessService(IUserCourseChildProgessRepository repo, IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _cache = cacheManager.GetCache("__UserChildCache");
        }

        #region 复用缓存查询方法
        public async Task<List<UserCourseChildInfo>> GetAll()
        {
            try
            {
                var rlt = _cache.Get(_collegeCacheKey, async () =>
                {
                    var body = await UnitOfWorkService.Execute(async () =>
                    {
                        var list = await _repo.GetListAsync();
                        var taskList = list.MapTo<List<UserCourseChildInfo>>();
                        return taskList;
                    });
                    return body;
                }, TimeSpan.FromMinutes(10));
                return await rlt;
            }
            catch (Exception ex)
            {
                Logger.Error("用户子课程视频进度列表缓存异常", ex);
                return null;
            }

        }
        #endregion
        #region  DoSync增删修改缓存

        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is UserChildEventData data)) return;
            var method = (EventDataMethod)data.Method;
            if (method == EventDataMethod.Clear)
            {
                ClearCache();
            }
        }
        public void RebuildCache()
        {
            ClearCache();
            //Trigger(new UserChildEventData() { Method = (int)EventDataMethod.Clear });
        }
        public override void ClearCache()
        {
            _cache.Clear();
            GetAll().GetAwaiter().GetResult();
        }

        public long Add(UserCourseChildInfo Info)
        {
            var entity = Info.MapTo<UserCourseChildProgessEntity>();
            entity.CreateTime = DateTime.Now;
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            if (body > 0) RebuildCache();
            return body;
        }
        public bool Update(UserCourseChildInfo Info)
        {
            var entity = Info.MapTo<UserCourseChildProgessEntity>();
            entity.UpdateTime = DateTime.Now;
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var tempEntity = _repo.Get(Info.Id);
                if (tempEntity != null)
                {
                    entity.CreateTime = tempEntity.CreateTime;
                    return _repo.Update(entity);
                }
                else
                {
                    return false;
                }

            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(UserCourseChildInfo Info)
        {
            var entity = Info.MapTo<UserCourseChildProgessEntity>();
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var tempEntity = _repo.Get(Info.Id);
                if (tempEntity != null)
                {
                    return _repo.Delete(entity);
                }
                else
                {
                    return false;
                }
            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool UserChildClearCache()
        {
            RebuildCache();
            return true;
        }
        #endregion
        #region 自定义查询方法

        public async Task<IQueryable<UserCourseChildInfo>> Table()
        {
            var List = await GetAll();
            return List.AsQueryable();

        }
        public async Task<Result<UserCourseChildInfo>> Get(long Userid, long CourseId, long CourseChildId)
        {
            try
            {
                var list = await GetAll();
                var info = list.Find(t => t.UserId == Userid && t.CourseId == CourseId && t.CourseChildId == CourseChildId);
                return await Task.FromResult(new Result<UserCourseChildInfo>() { Data = info });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取学校信息{Userid}异常！", exception);
                return new Result<UserCourseChildInfo> { Code = (int)ResultCode.Exception, Message = $"获取地址异常" };
            }
        }

        #endregion

    }









}
