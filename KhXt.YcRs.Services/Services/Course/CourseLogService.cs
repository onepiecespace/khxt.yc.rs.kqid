﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.MongoLogEntity;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Message;
using KhXt.YcRs.Domain.Models.MongologModel;
using KhXt.YcRs.Domain.Repositories.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.User;
using Hx.Extensions;
using Hx.MongoDb;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Course
{
    public class CourseLogService : ServiceBase, ICourseLogService
    {

        private readonly IMongoUnitOfWorkService _mongoUow;
        private readonly ICourseLogRepository _repo;
        private readonly IOperationLogRepository _repopt;
        private readonly IUserService _userService;
        public CourseLogService(IMongoUnitOfWorkService mongoUow,
            ICourseLogRepository repo,
            IUserService userService,
            IOperationLogRepository repopt)
        {
            _mongoUow = mongoUow;
            _repo = repo;
            _userService = userService;
            _repopt = repopt;
        }

        public void AddCourselog(MessageInfo info)
        {

            var entity = info.MapTo<CourseLogEntity>();
            entity.Id = Guid.NewGuid().ToString();
            var nid = _mongoUow.Execute(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
        }

        /// <summary>
        /// 增加日志（谁修改了什么）
        /// </summary>
        public void AddLogOfOperation(OperationInfo info)
        {
            var entity = info.MapTo<OperationLogEntity>();
            entity.Id = Guid.NewGuid().ToString();
            var nid = _mongoUow.Execute(() =>
            {
                return _repopt.InsertAndGetId(entity);
            });
        }

        #region 自定义导出mongo方法 
        public async Task<Result<Dictionary<string, List<CourseLogModel>>>> ExportExcelBaseDay(string startTime, string endTime, string channelId, bool ignore = true)
        {
            try
            {
                if (startTime.IsNullOrEmpty())
                    startTime = $"{DateTime.Now.ToString("yyyy-MM-dd")} 00:00:00";
                var dateStart = DateTime.Parse(startTime);
                if (endTime.IsNullOrEmpty())
                    endTime = $"{DateTime.Now.AddDays(1).ToString("yyyy-MM-dd")}  00:00:00";
                var dateEnd = DateTime.Parse(endTime);
                var list = await _mongoUow.Execute(() => _repo.GetListAsync());
                var allList = list.Where(t => t.ChannelId == channelId).OrderBy(t => t.Datatime).MapTo<List<CourseLogModel>>();
                var allModelList = new List<CourseLogModel>();
                var dicUsers = new Dictionary<string, List<long>>();
                if (allList == null || allList.Count <= 0)
                    return new Result<Dictionary<string, List<CourseLogModel>>> { };
                var users = await _userService.GetAll();
                for (int i = 0; i < allList.Count; i++)
                {
                    allList[i].Datatime = allList[i].Datatime.AddHours(-8);
                    var model = allList[i];
                    if (allModelList.Find(t => t.Datatime.DayOfYear == model.Datatime.DayOfYear && t.UserId == model.UserId && t.ChannelId == model.ChannelId) == null)
                    {
                        var user = users.Find(t => t.Userid == model.UserId);
                        if (user != null)
                        {
                            model.UserName = user.Username;
                            model.Phone = user.Phone;
                        }
                        allModelList.Add(model);
                        //if (dicUsers.ContainsKey(allList[i].Datatime.ToString("yyyy-MM-dd")))
                        //    dicUsers.Remove(allList[i].Datatime.ToString("yyyy-MM-dd"));
                        //dicUsers.Add(allList[i].Datatime.ToString("yyyy-MM-dd"),
                        //    allModelList.FindAll(t => model.Datatime <= t.Datatime && t.UserId == model.UserId && t.ChannelId == model.ChannelId).Select(t => t.UserId).ToList());
                    }
                }
                var length = (dateEnd - dateStart).Days;
                var tempUserList = new List<long>();
                var dic = new Dictionary<string, List<CourseLogModel>>();
                for (int i = 0; i <= length; i++)
                {
                    var tempList = allModelList.FindAll(t => t.Datatime >= dateStart.AddDays(i) && t.Datatime <= dateStart.AddDays(i + 1));
                    if (channelId.IsNotNullOrEmpty())
                        tempList = tempList.FindAll(t => t.ChannelId == channelId);

                    if (ignore && i > 0)
                    {
                        tempList = tempList.FindAll(t => !tempUserList.Contains(t.UserId));
                    }
                    tempUserList.AddRange(tempList.Select(t => t.UserId).ToList());
                    dic.Add(dateStart.AddDays(i).ToString("yyyy-MM-dd"), tempList);
                }


                return new Result<Dictionary<string, List<CourseLogModel>>> { Data = dic };

            }
            catch (Exception ex)
            {
                Logger.Error("按天导出 某个课程人员情况 异常", ex);
                return new Result<Dictionary<string, List<CourseLogModel>>> { Code = (int)ResultCode.Exception, Message = "按天导出 某个课程人员情况 异常" };
            }
        }


        #endregion

        #region 自定义导出mongox新方法 
        public async Task<Result<List<CourseLogInfo>>> ExportExcelBaseChildId(string ChannelId, long ChildId, int ChildType = 1, bool ignore = true)
        {
            try
            {
                var allList = new List<CourseLogInfo>();
                if (!string.IsNullOrEmpty(ChannelId) && ChildId > 0)
                {
                    var list = await _mongoUow.Execute(() => _repo.GetListAsync(t => t.ChannelId == ChannelId && t.ChildId == ChildId && t.ChildType == ChildType));
                    allList = list.OrderBy(t => t.VistTime).MapTo<List<CourseLogInfo>>();
                }
                var allModelList = new List<CourseLogInfo>();
                if (allList == null || allList.Count <= 0)
                    return new Result<List<CourseLogInfo>> { };

                for (int i = 0; i < allList.Count; i++)
                {
                    var model = allList[i];
                    model.VistTime = model.VistTime.AddHours(-8);
                    if (allModelList.Find(t => t.UserId == model.UserId && t.ChannelId == ChannelId && t.ChildId == model.ChildId && t.ChildType == ChildType) == null)
                    {
                        allModelList.Add(model);
                    }
                }

                return new Result<List<CourseLogInfo>> { Data = allModelList };

            }
            catch (Exception ex)
            {
                Logger.Error("按子课程导出 某个课程人员情况 异常", ex);
                return new Result<List<CourseLogInfo>> { Code = (int)ResultCode.Exception, Message = "按子课程导出 某个课程人员情况 异常" };
            }
        }


        #endregion
    }
}
