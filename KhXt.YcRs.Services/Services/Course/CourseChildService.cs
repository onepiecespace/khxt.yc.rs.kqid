﻿using Castle.Components.DictionaryAdapter;
using Castle.Core.Internal;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.MongologModel;
using KhXt.YcRs.Domain.Repositories.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Util;
using Hx.Extensions;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using Pipelines.Sockets.Unofficial.Arenas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Course
{
    //WHY:Create 改Add,返回值为主键类型，update,del返回值改为bool类型，其它服务也类似，不一一标注
    public class CourseChildService : ServiceBase, ICourseChildService
    {
        private readonly ICourseChildRepository _repo;
        private readonly ICourseItemService _courseItemService;
        private readonly ICourseUserItemService _courseUserItemService;
        private readonly IUserCourseChildProgessService _userCourseChildProgessService;
        private readonly ICourseLogService _courseLogService;
        private readonly IDirectMemoryCache _cache;
        private readonly string _courseChildCacheKey = "coursesChild";

        public CourseChildService(ICourseChildRepository repo,
            ICourseItemService courseItemService,
            IUserCourseChildProgessService userCourseChildProgessService,
            ICourseLogService courseLogService,
            ICourseUserItemService courseUserItemService,
            IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _courseItemService = courseItemService;
            _userCourseChildProgessService = userCourseChildProgessService;
            _courseUserItemService = courseUserItemService;
            _courseLogService = courseLogService;
            _cache = cacheManager.GetCache("__courseChildCache");
        }
        #region 复用缓存查询方法
        public async Task<List<CourseChildInfo>> GetAll()
        {
            try
            {
                var imgSvr = GetConfig("imgserver");
                var List = _cache.Get(_courseChildCacheKey, async () =>
                {

                    var body = await UnitOfWorkService.Execute(async () =>
                    {
                        var list = await _repo.GetListAsync();
                        var queryList = list.AsQueryable().Where(p => p.IsDelete == 0);
                        var taskList = queryList.MapTo<List<CourseChildInfo>>();
                        return taskList;
                    });
                    return body;
                }, TimeSpan.FromHours(8));

                return await List;
            }
            catch (Exception ex)
            {
                Logger.Error("子课程列表缓存异常", ex);
                return null;
            }

        }
        private async Task<List<CourseChildInfo>> GetChildtype(int ChildType)
        {
            try
            {
                var taskList = await GetAll();
                return taskList.Where(p => p.ChildType == ChildType).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("课程子章节列表缓存异常", ex);
                return null;
            }
        }
        private async Task<IQueryable<CourseChildInfo>> Table()
        {
            try
            {
                var taskList = await GetAll();
                return taskList.AsQueryable();
            }
            catch (Exception ex)
            {
                Logger.Error("课程子章节列表缓存异常", ex);
                return null;
            }
        }

        /// <summary>
        /// 条件排序有分页查询
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<PageList<CourseChildInfo>> GetPageListAsync(Expression<Func<CourseChildInfo, bool>> predicate, Action<Orderable<CourseChildInfo>> order, int pageSize, int pageIndex)
        {
            var body = await Table();
            if (order == null)
            {
                return await Task.FromResult(new PageList<CourseChildInfo>(body.Where(predicate), pageIndex, pageSize));
            }
            var deferrable = new Orderable<CourseChildInfo>(body.Where(predicate));
            order(deferrable);
            var ts = deferrable.Queryable;
            var totalCount = ts.Count();
            var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            if (!pagingTs.Any())
                return await Task.FromResult(new PageList<CourseChildInfo>(pagingTs, pageIndex, pageSize, totalCount));

            return await Task.FromResult(new PageList<CourseChildInfo>(pagingTs, pageIndex, pageSize, totalCount));

        }


        #endregion

        #region  DoSync增删修改缓存

        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is CourseChildEventData data)) return;
            var method = (EventDataMethod)data.Method;
            if (method == EventDataMethod.Clear)
            {
                ClearCache();
            }
        }
        public void RebuildCache()
        {
            ClearCache();
            //Trigger(new CourseChildEventData() { Method = (int)EventDataMethod.Clear });
        }
        public override void ClearCache()
        {
            _cache.Clear();
            Table().GetAwaiter();
        }

        public long Add(CourseChildInfo Info)
        {
            var entity = Info.MapTo<CourseChildEntity>();
            entity.CreateTime = DateTime.Now;
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            if (body > 0) RebuildCache();
            return body;

        }
        public bool Update(CourseChildInfo Info)
        {
            var entity = Info.MapTo<CourseChildEntity>();
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var tempEntity = _repo.Get(Info.id);
                if (tempEntity != null)
                {
                    return _repo.Update(entity);
                }
                else
                {
                    return false;
                }

            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdateChildType(long courseChildId, int ChildType, string videoId)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var tempEntity = _repo.Get(courseChildId);
                if (tempEntity != null)
                {
                    tempEntity.ChildType = ChildType;
                    if (!string.IsNullOrEmpty(videoId))
                    {
                        tempEntity.videoId = videoId;
                    }
                    return _repo.Update(tempEntity);
                }
                else
                {
                    return false;
                }
            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 查询课程即将开始的子课程
        /// </summary>
        /// <returns></returns>
        public async Task<Result<List<CourseisliveChildInfo>>> GetliveChildlist()
        {
            var taskList = await GetAll();
            List<CourseisliveChildInfo> chidelist = new List<CourseisliveChildInfo>();
            ///获取当前时间
            DateTime dt = DateTime.Now;
            var year = dt.Year.ToString();
            var month = dt.Month.ToString();
            var day = dt.Day.ToString();
            var childstr = month + "月" + day + "日";
            var chides = taskList.Where(p => p.ChildType == 4 && p.ChildTimeLength.IsNotNullOrEmpty() && p.ChildTimeLength.Contains(childstr)).OrderBy(p => p.ChildStartTime).ToList();
            if (chides.Count > 0)
            {
                chidelist = chides.MapTo<List<CourseisliveChildInfo>>();
            }
            return await Task.FromResult(new Result<List<CourseisliveChildInfo>>() { Data = chidelist });

        }
        /// <summary>
        /// 处理子课程直播状态
        /// </summary>
        /// <returns></returns>
        public async Task<Result<List<int>>> TaskSetChildType()
        {
            var taskList = await GetChildtype(4);
            List<CourseChildInfo> chidelist = new List<CourseChildInfo>();
            ///获取当前时间
            DateTime dt = DateTime.Now;
            var year = dt.Year.ToString();
            var month = dt.Month.ToString();
            var day = dt.Day.ToString();
            var childstr = month + "月" + day + "日";
            chidelist = taskList.Where(p => p.ChildTimeLength.IsNotNullOrEmpty() && p.ChildTimeLength.Contains(childstr)).OrderBy(p => p.ChildStartTime).ToList();

            List<int> courselist = new List<int>();
            if (chidelist.Count > 0)
            {
                foreach (CourseChildInfo item in chidelist)
                {
                    //判断过滤几点几分
                    DateTime StartTime = Convert.ToDateTime(item.ChildStartTime);
                    string starttimestr = StartTime.AddMinutes(-30).ToString("HH:mm");//减去30分并取出时分秒的字符串
                    string endtimestr = StartTime.ToString("HH:mm");//取出时分秒的字符串
                    string Nowtime = DateTime.Now.ToString();
                    bool resultBool = getTimeSpan(starttimestr, endtimestr, Nowtime);
                    if (resultBool)//开始时间减去当前时间间隔30分钟内尝试修改并提醒
                    {
                        bool isok = UpdateChildType(item.id, 1, "");
                        if (isok)
                        {
                            courselist.Add(item.ParentId);
                            //记录mongo日志
                            OperationInfo info = new OperationInfo();
                            info.UserName = "自动任务";
                            info.BusinessType = "子课程状态开启";
                            info.CourseId = item.ParentId;
                            info.CourseName = item.ChildName;
                            info.CreateTime = DateTime.Now;
                            var content = item.ParentId + "|" + item.id + "|" + item.ChildName + "|" + item.ChildTimeLength + "|" + starttimestr + "|" + endtimestr;
                            info.dec = "用户" + info.UserName + "修改了" + content + ",修改项为：" + item.ChildType + ",为1";
                            _courseLogService.AddLogOfOperation(info);
                        }
                    }
                }
            }
            return await Task.FromResult(new Result<List<int>>() { Data = courselist });
        }
        /// <summary>
        /// .判断时间是否在某一时间段内
        /// </summary>
        /// <param name="timeStr"></param>
        /// <returns></returns>
        private bool getTimeSpan(string start, string entime, string timeStr)
        {
            //判断当前时间是否在工作时间段内
            string _strWorkingDayAM = start;//工作时间上午08:30
            string _strWorkingDayPM = entime;
            TimeSpan dspWorkingDayAM = DateTime.Parse(_strWorkingDayAM).TimeOfDay;
            TimeSpan dspWorkingDayPM = DateTime.Parse(_strWorkingDayPM).TimeOfDay;

            //string time1 = "2017-2-17 8:10:00";
            DateTime t1 = Convert.ToDateTime(timeStr);

            TimeSpan dspNow = t1.TimeOfDay;
            if (dspNow > dspWorkingDayAM && dspNow < dspWorkingDayPM)
            {
                return true;
            }
            return false;
        }
        public bool Delete(CourseChildInfo Info)
        {
            var entity = Info.MapTo<CourseChildEntity>();
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var tempEntity = _repo.Get(Info.id);
                if (tempEntity != null)
                {
                    return _repo.Delete(entity);
                }
                else
                {
                    return false;
                }
            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool CourseChildClearCache()
        {
            RebuildCache();
            return true;
        }
        #endregion

        #region 自定义查询方法
        public async Task<Result<CourseChildInfo>> Get(long id)
        {
            try
            {
                var body = await Table();
                var list = body.ToList();
                var info = list.Find(t => t.id == id);
                return await Task.FromResult(new Result<CourseChildInfo>() { Data = info });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取课子程{id}异常！", exception);
                return new Result<CourseChildInfo> { Code = (int)ResultCode.Exception, Message = $"获取课程异常" };
            }
        }
        /// <summary>
        /// 获取主课程的子课程列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<Result<List<CourseChildInfo>>> GetChildCountAsync(long userId, long courseId)
        {
            var childrlt = await Table();
            var ChildAllList = childrlt.Where(t => t.ParentId == courseId).ToList();
            return await Task.FromResult(new Result<List<CourseChildInfo>>() { Data = ChildAllList });
        }
        /// <summary>
        /// 获取主课程子课程学习完成的列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<Result<List<UserCourseChildInfo>>> GetUserChildCountAsync(long userId, long courseId)
        {
            var userchildrlt = await _userCourseChildProgessService.Table();
            var userChildAllList = userchildrlt.Where(t => t.UserId == userId).ToList();
            var userchildlist = userChildAllList.Where(t => t.CourseId == courseId && t.IsFinish == true).ToList();
            return await Task.FromResult(new Result<List<UserCourseChildInfo>>() { Data = userchildlist });
        }

        public async Task<PageList<CourseUserListModel>> GetUserCourseList(long userId, long courseId,
        int pageIndex, int pageSize)
        {
            var courseUserWorkPageLists = new PageList<CourseUserListModel>();
            try
            {
                var list = new List<CourseUserListModel>();
                //获取当前用户 课程下子课题列表 课程已做筛选 自课程不做筛选
                #region  条件判断
                Expression<Func<CourseChildInfo, bool>> predicate = null;
                Action<Orderable<CourseChildInfo>> preorder = null;
                preorder = orderch => orderch.Asc(n => n.ChildSort);
                predicate = n => n.ParentId == courseId;
                #endregion
                var pageList = await GetPageListAsync(predicate, preorder, pageSize, pageIndex);
                if (pageList.Count <= 0)
                    return new PageList<CourseUserListModel>(list, pageIndex, pageSize, list.Count);
                var workAllList = _courseItemService.Table()
                    .Where(t => t.IsDelete == 0).ToList();
                var allUserWorkList = _courseUserItemService.Table().Where(t => t.UserId == userId && t.OpSign == 1).ToList();
                //获取进度
                var userchildrlt = await _userCourseChildProgessService.Table();
                var userChildAllList = userchildrlt.Where(t => t.UserId == userId).ToList();
                courseUserWorkPageLists = UnitOfWorkService.Execute<PageList<CourseUserListModel>>(() =>
                {
                    //获取课程下面作业
                    foreach (var courseChild in pageList)
                    {
                        var courseUserListModel = courseChild.MapTo<CourseUserListModel>();
                        var workList = workAllList.Where(t => t.CourseChildId == courseChild.id).ToList();
                        var userWorkList = allUserWorkList.Where(t => t.CourseChildId == courseChild.id).ToList();
                        courseUserListModel.TotalDone = workList.Count;
                        courseUserListModel.AlreadyDone = userWorkList.Count;
                        string codestr = NumChineseHelper.NumToChinese(courseChild.ChildSort.ToString());
                        courseUserListModel.ChildCode = "第" + codestr + "课";
                        var userchildinfo = userChildAllList.Where(t => t.CourseChildId == courseChild.id).FirstOrDefault();
                        if (userchildinfo != null)
                        {
                            #region  改为保存时计算
                            //if (userchildinfo.CurrentTime > 0 && userchildinfo.DurationTime > 0)
                            //{
                            //    int m = userchildinfo.CurrentTime;
                            //    int n = userchildinfo.DurationTime;
                            //    float x;
                            //    x = (float)m / n;
                            //    courseUserListModel.ChildProgress = x;
                            //}
                            //else
                            //{
                            //    courseUserListModel.ChildProgress = 0;
                            //}
                            #endregion
                            courseUserListModel.ChildProgress = userchildinfo.ChildProgress;
                            courseUserListModel.UserChildProgess = userchildinfo;
                        }
                        else
                        {
                            UserCourseChildInfo userchilditem = new UserCourseChildInfo();
                            userchilditem = new UserCourseChildInfo
                            {
                                Id = 0,
                                CourseId = courseId,
                                CourseChildId = courseChild.id,
                                DurationTime = 0,
                                CurrentTime = 0,
                                UserId = userId,
                                LiveNum = 0,
                                IsFinish = false
                            };
                            courseUserListModel.ChildProgress = 0;
                            courseUserListModel.UserChildProgess = userchilditem;
                        }
                        list.Add(courseUserListModel);
                    }

                    return new PageList<CourseUserListModel>(list, pageIndex, pageSize, list.Count);
                });

                return await Task.Run(() => courseUserWorkPageLists); ;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("GetUserCourseList:", ex);
                throw;
            }
        }

        public async Task<Result<CourseItemListInfo>> GetUserCourseListResult(long userId,
            long courseId, int pageIndex, int pageSize)
        {
            Result<CourseItemListInfo> result = new Result<CourseItemListInfo>();
            try
            {

                var list = await GetUserCourseList(userId, courseId, pageIndex, pageSize);
                var courseItem = new CourseItemListInfo()
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    CourseItemList = list,// await GetUserCourseList(userId, courseId, order, pageIndex, pageSize),
                    TotalCount = list.Count
                };
                result.Data = courseItem;
            }
            catch (Exception ex)
            {

                result.Code = (int)ResultCode.Exception;
                result.Message = "获取我的课程列表出错";
                Logger.Error("获取我的课程列表出错(GetUserCourseListResult):", ex);
                Logger.Error($"{userId}|{courseId}");
            }
            return result;
        }

        #endregion

        #region CMS

        public List<CourseChildInfo> GetCourseChildList(CourseChildInfo info)
        {

            var allCourseChild = GetAll().Result;
            if (info.ParentId != 0 && info.ParentId.ToString().IsNotNullOrEmpty())
            {
                allCourseChild = allCourseChild.Where(p => p.ParentId == info.ParentId).ToList();
            }
            if (info.ChildName.IsNotNullOrEmpty())
            {
                allCourseChild = allCourseChild.Where(p => p.ChildName.Contains(info.ChildName)).ToList();
            }
            if (info.ChildTeacher.IsNotNullOrEmpty())
            {
                allCourseChild = allCourseChild.Where(p => p.ChildTeacher.Contains(info.ChildTeacher)).ToList();
            }
            return allCourseChild;
        }

        /// <summary>
        /// 增删改
        /// </summary>
        /// <param name="jsons"></param>
        /// <param name="optiden"></param>
        /// <returns></returns>
        public async Task<int> optionAsync(CourseChildModelCMS model, string optiden, string name)
        {
            var body = 0;
            var entity = model.MapTo<CourseChildEntity>();
            if (optiden == "add" || optiden == null || optiden == "")
            {
                body = UnitOfWorkService.Execute(() =>
                {
                    entity.CreateTime = DateTime.Now;
                    entity.UpdateTime = DateTime.Now;
                    long returnId = _repo.InsertAndGetId(entity);
                    if (returnId > 0)
                    {
                        return 1;
                    }

                    return 0;
                });
            }
            if (optiden == "edit")
            {
                var lrtchild = await Get(entity.Id);
                if (string.IsNullOrEmpty(model.ChildRelevanceFile))
                {
                    entity.ChildRelevanceFile = lrtchild.Data.ChildRelevanceFile;
                }
                else
                {
                    entity.ChildRelevanceFile = model.ChildRelevanceFile;
                }
                body = UnitOfWorkService.Execute(() =>
                {
                    entity.UpdateTime = DateTime.Now;
                    var result = _repo.Update(entity);
                    if (result == true)
                    {
                        return 1;
                    }
                    return 0;
                });
            }
            if (optiden == "del")
            {
                body = UnitOfWorkService.Execute(() =>
                {
                    entity.IsDelete = 1;
                    entity.UpdateTime = DateTime.Now;
                    var result = _repo.Update(entity);
                    if (result == true)
                    {
                        return 1;
                    }
                    return 0;
                });
            }
            if (body == 1) RebuildCache();
            //记录mongo日志
            OperationInfo info = new OperationInfo();
            info.UserName = name;
            info.BusinessType = "子课程管理";
            info.CourseId = model.Id;
            info.CourseName = model.ChildName;
            info.CreateTime = DateTime.Now;
            info.dec = "用户" + info.UserName + ",在" + info.CreateTime + "操作了" + info.BusinessType + "模块" + "执行的操作为" + optiden + ",修改项为：" + info.CourseName + ",id为：" + model.Id;
            _courseLogService.AddLogOfOperation(info);
            return body;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public int BatchInsertExcel(List<CourseChildModelCMS> list)
        {
            var entityList = list?.MapTo<List<CourseChildEntity>>();
            if (entityList == null)
                return 0;
            var body = UnitOfWorkService.Execute<int>(() => _repo.BatchInsert(entityList));
            if (body > 0)
                RebuildCache();
            return body;

        }

    }
}
