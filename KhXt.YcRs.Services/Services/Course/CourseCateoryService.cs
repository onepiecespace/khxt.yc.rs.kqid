﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Repositories.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Course
{
    public class CourseCateoryService : ServiceBase, ICourseCateoryService
    {
        ICourseCateoryRepository _repo;
        private readonly IDirectMemoryCache _cache;
        private readonly string _cateCacheKey = "Cates";
        private readonly string _path;
        public CourseCateoryService(ICourseCateoryRepository repo, IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _cache = cacheManager.GetCache("__courseCateCache");
            _path = "img/collegecourse/";
        }


        public async Task<CourseCateoryModel> Get(long id)
        {
            var model = Table().FirstOrDefault(m => m.Id == id);
            return await Task.FromResult(model);
        }

        public IQueryable<CourseCateoryModel> Table()
        {
            var imgSvr = GetConfig("imgserver");
            var Cateorys = _cache.Get(_cateCacheKey, () =>
            {
                var models = UnitOfWorkService.Execute(() =>
               {
                   var alllist = _repo.GetList(p => p.IsDel == 0);
                   var adslist = alllist.OrderBy(p => p.Sort).ToList();
                   var cateoryList = adslist.MapTo<List<CourseCateoryModel>>();
                   cateoryList.ForEach(ad =>
                   {
                       //WHY:此处类似CourseServie,下面字段在执行修改方法时需要特殊第一桌
                       ad.IcoImg = $"{imgSvr}{ad.IcoImg}";
                       ad.Img = $"{imgSvr}{ad.Img}";
                   });
                   return cateoryList;
               });
                return models.AsQueryable();
            }, TimeSpan.FromHours(24));
            return Cateorys;
        }
        #region CMS

        public List<CourseCateoryModel> GetCourseCateoryList(CourseCateoryModel entity)
        {
            return Table().ToList();
        }

        /// <summary>
        /// 增删改
        /// </summary>
        /// <param name="jsons"></param>
        /// <param name="optiden"></param>
        /// <returns></returns>
        public int Option(CourseCateoryModel model, string optiden)
        {
            var entity = model.MapTo<CourseCateoryEntity>();
            var body = 0;
            if (optiden == "add")
            {
                body = UnitOfWorkService.Execute(() =>
                {
                    entity.CreateTime = DateTime.Now;
                    entity.UpdateTime = DateTime.Now;
                    entity.Img = _path + entity.Img;
                    entity.IcoImg = entity.Img;
                    long returnId = _repo.InsertAndGetId(entity);
                    if (returnId > 0)
                    {
                        return 1;
                    }

                    return 0;
                });
            }
            if (optiden == "edit")
            {
                body = UnitOfWorkService.Execute(() =>
                {
                    entity.UpdateTime = DateTime.Now;
                    entity.Img = _path + entity.Img;
                    entity.IcoImg = entity.Img;
                    var result = _repo.Update(entity);
                    if (result == true)
                    {
                        return 1;
                    }
                    return 0;
                });
            }
            if (optiden == "del")
            {
                body = UnitOfWorkService.Execute(() =>
                {
                    entity.IsDel = 1;
                    entity.UpdateTime = DateTime.Now;
                    var result = _repo.Update(entity);
                    if (result == true)
                    {
                        return 1;
                    }
                    return 0;
                });
            }
            if (body == 1) RebuildCache();
            return body;
        }
        private void RebuildCache()
        {
            ClearCache();
        }
        public override void ClearCache()
        {
            _cache.Clear();
            Table();
        }

        public List<CourseCateoryModel> GetCategoryList()
        {
            var allcateory = Table();
            allcateory = allcateory.Where(p => p.IsShow == 1 && p.ParentId == 0);
            return allcateory.ToList();
        }
        public List<CourseCateoryModel> GetCategoryTwoList(int Id)
        {
            var allcateory = Table();
            if (Id > 0)
            {
                allcateory = allcateory.Where(p => p.IsShow == 1 && p.ParentId == Id);
            }
            else
            {
                allcateory = allcateory.Where(p => p.IsShow == 1 && p.ParentId != 0);
            }
            return allcateory.ToList();
        }
        #endregion
    }









}
