﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Repositories.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KhXt.YcRs.Services.Course
{
    //WHY:Create 改Add,返回值为主键类型，update,del返回值改为bool类型

    public class CourseItemService : ServiceBase, ICourseItemService
    {
        ICourseItemRepository _repo;
        private readonly IDirectMemoryCache _cache;
        private readonly string _courseItemCacheKey = "courseItem";
        public CourseItemService(ICourseItemRepository repo, IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _cache = cacheManager.GetCache("__courseItemsCache");
        }

        public IQueryable<CourseItemEntity> Table()
        {
            try
            {
                var userItems = _cache.Get(_courseItemCacheKey, () =>
                {
                    var AllList = UnitOfWorkService.Execute(() =>
                    {
                        return _repo.GetList().AsQueryable();
                    });
                    // return AllList;
                    var list = AllList.Where(p => p.IsDelete == 0).OrderBy(t => t.ItemSort).ToList();
                    return list.AsQueryable();
                }, TimeSpan.FromHours(24));
                return userItems;
            }
            catch (Exception ex)
            {
                Logger.Error("课程用户作业列表缓存异常", ex);
                return null;
            }
        }
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool CourseItemClearCache()
        {
            RebuildCache();
            return true;
        }
        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is CourseItemEventData data)) return;
            var method = (EventDataMethod)data.Method;
            if (method == EventDataMethod.Clear)
            {
                ClearCache();
            }
        }

        public void RebuildCache()
        {
            ClearCache();
            //Trigger(new CourseItemEventData() { Method = (int)EventDataMethod.Clear });
        }

        public override void ClearCache()
        {
            _cache.Clear();
            Table();
        }

        /// <summary>
        /// 批量插入
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int BatchInsertExcel(List<CourseItemEntity> list)
        {
            var body = UnitOfWorkService.Execute<int>(() =>
            {
                return _repo.BatchInsert(list);
            });
            if (body > 0)
                RebuildCache();
            return body;

        }
        public bool BatchUpdatetExcel(long courseChildId, int IsDelete)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var courseItemlist = _repo.GetList().Where(n => n.CourseChildId == courseChildId).ToList();
                foreach (var Entity in courseItemlist)
                {
                    Entity.IsDelete = IsDelete;
                    _repo.Update(Entity);
                }
                return true;
            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }

        }

        public void Update(CourseItemEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });
            if (body)
                RebuildCache();


        }

    }









}
