﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Repositories.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Sys;
using Hx.Extensions;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Course
{
    //WHY:加入缓存逻辑，实现ClearCache、DoSync方法，注意要增量加，不要clear重建,add,delete等方法加同步消息发送
    public class CourseBuyCollectService : ServiceBase, ICourseBuyCollectService
    {
        ICourseBuyCollectRepository _repo;
        private readonly string _courseBuyCacheKey = "coursesBuyCollect";
        private readonly IDirectMemoryCache _cache;
        public CourseBuyCollectService(ICourseBuyCollectRepository repo, IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _cache = cacheManager.GetCache("__courseBuyCollectCache");
        }

        #region IComBaseService
        public IQueryable<CourseBuyCollectEntity> Table()
        {
            var body = UnitOfWorkService.Execute(() =>
                {
                    return _repo.GetList().AsQueryable();
                });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public long Add(CourseBuyCollectEntity entity)
        {
            var rlt = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            if (rlt > 0) RebuildCache();
            return rlt;

        }

        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList<CourseBuyCollectEntity> Gets(Expression<Func<CourseBuyCollectEntity, bool>> predicate, Action<Orderable<CourseBuyCollectEntity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList<CourseBuyCollectEntity>>(() =>
           {
               if (order == null)
               {
                   return new PageList<CourseBuyCollectEntity>(Table().Where(predicate), pageIndex, pageSize);
               }
               var deferrable = new Orderable<CourseBuyCollectEntity>(Table().Where(predicate));
               order(deferrable);
               var ts = deferrable.Queryable;
               var totalCount = ts.Count();
               var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

               return new PageList<CourseBuyCollectEntity>(pagingTs, pageIndex, pageSize, totalCount);

           });
            return body;
        }
        #region 缓存全部购买的课程数据

        public async Task<List<CourseBycollectModel>> GetAll()
        {
            try
            {
                var imgSvr = GetConfig("imgserver");
                var cacheList = await _cache.Get(_courseBuyCacheKey, async () =>
                {
                    var body = await UnitOfWorkService.Execute(async () =>
                    {
                        var alllist = await _repo.GetListAsync();
                        var list = alllist.MapTo<List<CourseBycollectModel>>();
                        return list;
                    });
                    return body;
                }, TimeSpan.FromHours(3));
                return cacheList;
            }
            catch (Exception ex)
            {
                Logger.Error("列表缓存异常", ex);
                return null;
            }
        }
        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is CourseBycollectEventData data)) return;
            var method = (EventDataMethod)data.Method;
            if (method == EventDataMethod.Clear)
            {
                ClearCache();
            }
        }
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool BuycollectClearCache()
        {
            RebuildCache();
            return true;
        }
        private void RebuildCache()
        {
            ClearCache();
            //Trigger(new CourseBycollectEventData() { Method = (int)EventDataMethod.Clear });
        }
        public override void ClearCache()
        {
            _cache.Clear();

            GetAll().GetAwaiter().GetResult();

        }

        /// <summary>
        /// 条件排序有分页查询
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<PageList<CourseBycollectModel>> GetPageListAsync(Expression<Func<CourseBycollectModel, bool>> predicate, Action<Orderable<CourseBycollectModel>> order, int pageSize, int pageIndex)
        {
            var body = (await GetAll()).AsQueryable();
            if (order == null)
            {
                return await Task.FromResult(new PageList<CourseBycollectModel>(body.Where(predicate), pageIndex, pageSize));
            }

            var deferrable = new Orderable<CourseBycollectModel>(body.Where(predicate));
            order(deferrable);
            var ts = deferrable.Queryable;
            var totalCount = ts.Count();
            var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return await Task.FromResult(new PageList<CourseBycollectModel>(pagingTs, pageIndex, pageSize, totalCount));

        }
        #endregion
        public bool Update(CourseBuyCollectEntity entity)
        {
            var rlt = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });
            if (rlt) RebuildCache();
            return rlt;

        }

        public bool UpdateIshide(int ishide, long cbid)
        {
            var rlt = UnitOfWorkService.Execute<bool>(() =>
            {
                CourseBycollectModel model = new CourseBycollectModel();
                model = _repo.Get(cbid).MapTo<CourseBycollectModel>();
                model.IsHide = ishide;
                var entity = model.MapTo<CourseBuyCollectEntity>();
                return _repo.Update(entity);
            });

            if (rlt) RebuildCache();
            return rlt;
        }
        public bool UpdateExpirationDate(DateTime ExpirationDate, long cbid)
        {
            var rlt = UnitOfWorkService.Execute<bool>(() =>
            {
                CourseBycollectModel model = new CourseBycollectModel();
                model = _repo.Get(cbid).MapTo<CourseBycollectModel>();
                model.ExpirationDate = ExpirationDate;
                var entity = model.MapTo<CourseBuyCollectEntity>();
                return _repo.Update(entity);
            });
            if (rlt) RebuildCache();
            return rlt;
        }
        #endregion
    }
}
