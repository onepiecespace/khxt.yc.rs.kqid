﻿
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Home;
using KhXt.YcRs.Domain.Models.Message;
using KhXt.YcRs.Domain.Models.MongologModel;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.NVelocity;
using KhXt.YcRs.Domain.Repositories.Course;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Files;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.Sys;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx;
using Hx.Components;
using Hx.Extensions;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static KhXt.YcRs.Domain.Models.Course.CourseThinModel;

namespace KhXt.YcRs.Services.Course
{
    public class CourseService : ServiceBase, ICourseService
    {
        ICourseRepository _repo;
        ICourseAppoRepository _repoAppo;
        private readonly IVersionService _versionService;
        private readonly ICourseCateoryService _courseCateoryService;
        private readonly ICourseChildService _courseChildService;
        private readonly ICourseLogService _courseLogService;
        private readonly IUserCacheService _userCacheService;
        private readonly IUser_ChannelService _userChannelService;
        private readonly IDirectMemoryCache _cache;
        private readonly ICourseOrderService _courseOrderService;
        private readonly ICouponInfoService _couponInfoService;
        //private readonly IOrderInfoRepository _orderInfoRepository;
        private readonly ICourseBuyCollectService _courseBuyCollectService;
        private ISettingService _settingService;
        private readonly string _courseCacheKey = "courses";
        private readonly string _templatePath = "";
        private readonly string _outPath = "";
        private readonly IFilesService _filesService;

        private const string COURSE_THIN_MODEL_CACHE = "__ctm";
        private const string COURSE_THIN_LEVEL_MODEL_CACHE = "__levelctm";

        public CourseService(ICourseRepository repo,
            IDirectMemoryCacheManager cacheManager,
            ICourseAppoRepository repoAppo,
            IVersionService versionService,
            ICourseCateoryService courseCateoryService,
            ICourseLogService courseLogService,
            IUserCacheService userCacheService,
            IUser_ChannelService userChannelService,
            ICourseChildService courseChildService,
            ICourseBuyCollectService courseBuyCollectService,
            ICouponInfoService couponInfoService,
            ICourseOrderService courseOrderService,
            IFilesService filesService,
            ISettingService settingService)
        {
            _repo = repo;
            _repoAppo = repoAppo;
            _versionService = versionService;
            _courseCateoryService = courseCateoryService;
            _courseChildService = courseChildService;
            _courseLogService = courseLogService;
            _userCacheService = userCacheService;
            _userChannelService = userChannelService;
            _courseOrderService = courseOrderService;
            //_orderInfoRepository = _orderInfoRepository;
            _courseBuyCollectService = courseBuyCollectService;
            _couponInfoService = couponInfoService;
            _settingService = settingService;
            _filesService = filesService;
            _templatePath = $"{DomainSetting.CourseTemplatePath}";
            _outPath = DomainSetting.CourseTemplateOutPath;
            _cache = cacheManager.GetCache("__courseCache");
        }

        public async Task<List<CourseQueryModel>> GetAll()
        {
            try
            {
                var imgSvr = GetConfig("imgserver");
                var rlt = _cache.Get(_courseCacheKey, async () =>
                {
                    var body = await UnitOfWorkService.Execute(async () =>
                    {
                        var alllist = await _repo.GetListAsync(p => p.IsDelete == 0);

                        var list = alllist.OrderBy(t => t.Sort).ToList();
                        var CList = list.MapTo<List<CourseQueryModel>>();
                        // var timestr = "?_=" + CommonHelper.GetTimeStamp();
                        CList.ForEach(ad =>
                        {
                            //WHY:此处会带来update逻辑问题，修改下面的Option方法，特殊处理这几个字段
                            ad.CoursePicPhoneMini = $"{imgSvr}{ad.CoursePicPhoneMini}";
                            ad.CoursePicPhone = $"{imgSvr}{ad.CoursePicPhone}";
                            ad.CoursePicContent = $"{ad.CoursePicContent}";
                            ad.H5Url = $"{DomainSetting.ResServerLocal}{_outPath}/{ad.Id}.html";
                        });
                        return CList;
                    });
                    return body;
                }, TimeSpan.FromHours(8));

                return await rlt;
            }
            catch (Exception ex)
            {
                Logger.Error("主课程列表缓存异常", ex);
                return null;
            }

        }

        /// <summary>
        /// List异步的分页
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<Result<List<CourseQueryModel>>> GetListAsync(Expression<Func<CourseQueryModel, bool>> predicate, Action<Orderable<CourseQueryModel>> order)
        {
            var body = (await GetAll()).AsQueryable();
            if (order == null)
            {
                var orderlist = body.Where(predicate).ToList();
                return await Task.FromResult(new Result<List<CourseQueryModel>>() { Data = orderlist });
            }
            var deferrable = new Orderable<CourseQueryModel>(body.Where(predicate));
            order(deferrable);
            var ts = deferrable.Queryable;
            var list = ts.ToList();
            return await Task.FromResult(new Result<List<CourseQueryModel>>() { Data = list });

        }
        /// <summary>
        /// 条件排序有分页查询
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<PageList<CourseQueryModel>> GetPageListAsync(Expression<Func<CourseQueryModel, bool>> predicate, Action<Orderable<CourseQueryModel>> order, int pageSize, int pageIndex)
        {
            var body = (await GetAll()).AsQueryable();
            if (order == null)
            {
                return await Task.FromResult(new PageList<CourseQueryModel>(body.Where(predicate), pageIndex, pageSize));
            }

            var deferrable = new Orderable<CourseQueryModel>(body.Where(predicate));
            order(deferrable);
            var ts = deferrable.Queryable;
            var totalCount = ts.Count();
            var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return await Task.FromResult(new PageList<CourseQueryModel>(pagingTs, pageIndex, pageSize, totalCount));

        }
        public async Task<Result<CourseQueryModel>> GetAsync(long Id)
        {
            try
            {
                var list = await GetAll();
                var info = list.Find(t => t.Id == Id);
                return await Task.FromResult(new Result<CourseQueryModel>() { Data = info });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取课程{Id}异常！", exception);
                return new Result<CourseQueryModel> { Code = (int)ResultCode.Exception, Message = $"获取课程异常" };
            }

        }
        public CourseEntity GetById(long id)
        {
            var body = UnitOfWorkService.Execute<CourseEntity>(() =>
            {
                long.TryParse(id.ToString(), out long nid);
                return _repo.Get(id);
            });
            return body;

        }
        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is CourseEventData data)) return;
            var method = (EventDataMethod)data.Method;
            if (method == EventDataMethod.Clear)
            {
                ClearCache();
            }
        }
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool CourseClearCache()
        {
            RebuildCache();
            return true;
        }
        private void RebuildCache()
        {
            ClearCache();
            //Trigger(new CourseEventData() { Method = (int)EventDataMethod.Clear });
        }
        public override void ClearCache()
        {
            _cache.Clear();
            GetAll().GetAwaiter().GetResult();
        }

        public async Task<bool> SetCourseChildType()
        {
            bool isok = false;
            List<int> courselist = new List<int>();
            var rlt = await _courseChildService.TaskSetChildType();
            courselist = rlt.Data;
            int success = 0;
            //courselist.Add(185);
            if (courselist.Count > 0)
            {
                #region 处理主课程开播状态
                foreach (int courseid in courselist)
                {
                    var lrt = await GetAsync(courseid);
                    var info = lrt.Data;
                    if (info != null)
                    {
                        var infoEntity = info.MapTo<CourseEntity>();
                        infoEntity.IsLive = 1;
                        var result = Update(infoEntity);
                        if (result)
                        {
                            success++;
                        }
                    }
                }
                #endregion
                if (success > 0)
                {
                    isok = true;
                }

            }
            return await Task.FromResult(isok);
        }
        #region  自定义接口方法
        #region 首页app

        public async Task<List<CourseThinModel>> GetHotList(List<HomeCourseSettingModel> setting, long userId)
        {
            UserBaseInfo userinfo = new UserBaseInfo();
            if (userId > 0)
            {
                userinfo = await _userCacheService.GetUser(userId);
            }


            var rlt = await _cache.Get("__hot", async () =>
            {

                var models = await GetWithChildModelsAsync();
                var cateSetting = setting.Find(c => c.Id == -1);
                if (cateSetting == null) return null;
                var data = cateSetting.Values.Select(id => models.FirstOrDefault(m => m.Id == id)).ToList();
                var cdata = data.Select(d => d.Clone()).ToList();
                cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");
                #region 迁移到外部
                //if (userinfo != null)
                //{
                //    List<CourseThinModel> Clist = new List<CourseThinModel>();
                //    foreach (CourseThinModel item in cdata)
                //    {
                //        item.Img = $"{item.Img}{cateSetting.PictureDirect}.png";

                //        if (userinfo.AuthCateorys.Contains(item.Category.ToString()))
                //        {
                //            Clist.Add(item);
                //        }
                //    }
                //    cdata = Clist;
                //}
                //else
                //{
                // cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");
                // }
                #endregion
                return cdata;
            }, null, TimeSpan.FromSeconds(60));

            if (userId > 0 && userinfo.Level <= 3)
            {
                var settingService = ObjectContainer.Resolve<ISettingService>();
                var levelsetting = await settingService.GetLevelCourseSetting();
                List<CourseThinModel> Clist = new List<CourseThinModel>();
                Clist = LevelThinCourse(rlt, levelsetting, userinfo.Level);
                rlt = Clist;
                #region  注释多于的判断

                //foreach (CourseThinModel item in rlt)
                //{
                //    if (userinfo.AuthCateorys.Contains(item.Category.ToString()))
                //    {
                //        Clist.Add(item);
                //    }
                //}
                //List<long> removelist = new List<long>();
                //var useInfo = await _userCacheService.GetUser(userId);
                //if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
                //{
                //    removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);
                //    RemoveThinCourse(Clist, removelist, useInfo.AccountType);
                //}
                // rlt = Clist;
                #endregion
            }
            return rlt;
        }
        public async Task<List<CourseThinModel>> GetLiveList(List<HomeCourseSettingModel> setting, long userId)
        {

            UserBaseInfo userinfo = new UserBaseInfo();
            if (userId > 0)
            {
                userinfo = await _userCacheService.GetUser(userId);
            }
            var rlt = await _cache.Get("__live", async () =>
            {
                var models = await GetWithChildModelsAsync();
                var cateSetting = setting.Find(c => c.Id == -2);
                if (cateSetting == null) return null;
                var data = cateSetting.Values.Select(id => models.FirstOrDefault(m => m.Id == id)).ToList();
                var cdata = data.Select(d => d.Clone()).ToList();
                cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");
                #region 迁移出
                //if (userinfo != null)
                //{
                //    List<CourseThinModel> Clist = new List<CourseThinModel>();
                //    foreach (CourseThinModel item in cdata)
                //    {
                //        item.Img = $"{item.Img}{cateSetting.PictureDirect}.png";

                //        if (userinfo.AuthCateorys.Contains(item.Category.ToString()))
                //        {
                //            Clist.Add(item);
                //        }
                //    }
                //    cdata = Clist;
                //}
                //else
                //{
                // cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");
                // }
                #endregion
                return cdata;
            }, null, TimeSpan.FromSeconds(60));
            if (userId > 0 && userinfo.Level <= 3)
            {
                var settingService = ObjectContainer.Resolve<ISettingService>();
                var levelsetting = await settingService.GetLevelCourseSetting();
                List<CourseThinModel> Clist = new List<CourseThinModel>();
                Clist = LevelThinCourse(rlt, levelsetting, userinfo.Level);
                rlt = Clist;
                #endregion
            }
            #region 旧方法
            //if (userId > 0)
            //{
            //    List<CourseThinModel> Clist = new List<CourseThinModel>();
            //    foreach (CourseThinModel item in rlt)
            //    {
            //        if (userinfo.AuthCateorys.Contains(item.Category.ToString()))
            //        {
            //            Clist.Add(item);
            //        }
            //    }
            //    List<long> removelist = new List<long>();
            //    var useInfo = await _userCacheService.GetUser(userId);
            //    if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
            //    {
            //        removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);
            //        RemoveThinCourse(Clist, removelist, useInfo.AccountType);
            //    }
            //    rlt = Clist;
            //}
            #endregion
            return rlt;
        }
        public async Task<List<CourseThinModel>> GetRecommendList(List<HomeCourseSettingModel> setting, long userId)
        {
            UserBaseInfo userinfo = new UserBaseInfo();
            if (userId > 0)
            {
                userinfo = await _userCacheService.GetUser(userId);
            }
            var rlt = await _cache.Get("__recomment", async () =>
            {
                var models = await GetWithChildModelsAsync();
                var cateSetting = setting.Find(c => c.Id == -3);
                if (cateSetting == null) return null;
                var data = cateSetting.Values.Select(id => models.FirstOrDefault(m => m.Id == id)).ToList();
                //var data = models.Where(m => cateSetting.Values.Contains(m.Id)).ToList();
                var cdata = data.Select(d => d.Clone()).ToList();
                cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");
                #region 移出
                //if (userinfo != null)
                //{
                //    List<CourseThinModel> Clist = new List<CourseThinModel>();
                //    foreach (CourseThinModel item in cdata)
                //    {
                //        item.Img = $"{item.Img}{cateSetting.PictureDirect}.png";

                //        if (userinfo.AuthCateorys.Contains(item.Category.ToString()))
                //        {
                //            Clist.Add(item);
                //        }
                //    }
                //    cdata = Clist;
                //}
                //else
                //{
                //    cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");
                //}
                #endregion
                return cdata;
            }, null, TimeSpan.FromSeconds(60));
            if (userId > 0 && userinfo.Level <= 3)
            {
                var settingService = ObjectContainer.Resolve<ISettingService>();
                var levelsetting = await settingService.GetLevelCourseSetting();
                List<CourseThinModel> Clist = new List<CourseThinModel>();
                Clist = LevelThinCourse(rlt, levelsetting, userinfo.Level);
                rlt = Clist;
            }
            #region 旧方法
            //if (userId > 0)
            //{
            //    List<CourseThinModel> Clist = new List<CourseThinModel>();
            //    foreach (CourseThinModel item in rlt)
            //    {
            //        if (userinfo.AuthCateorys.Contains(item.Category.ToString()))
            //        {
            //            Clist.Add(item);
            //        }
            //    }
            //    List<long> removelist = new List<long>();
            //    var useInfo = await _userCacheService.GetUser(userId);
            //    if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
            //    {
            //        removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);
            //        RemoveThinCourse(Clist, removelist, useInfo.AccountType);
            //    }
            //    rlt = Clist;
            //}
            #endregion
            return rlt;
        }

        public async Task<List<CategoryedCourseThinModel>> GetCategoriedTopCourses(List<HomeCourseSettingModel> setting)
        {
            var imgSvr = GetConfig("imgserver");
            var rlt = await _cache.Get("__cate", async () =>
            {
                var catedData = new List<CategoryedCourseThinModel>();
                var cates = new List<CourseCateoryModel>();
                var models = await GetWithChildModelsAsync();
                var cids = setting.Where(s => s.Id > 0).OrderBy(s => s.Sort).Select(s => s.Id).ToList();
                if (cids.Count() == 0) return null;
                var cateCategorys = _courseCateoryService.Table();
                foreach (var cid in cids)
                {
                    var cate = cateCategorys.FirstOrDefault(c => c.Id == cid);
                    if (cate != null)
                    {
                        cates.Add(cate);
                    }
                }
                if (cates.Count() > 0)
                {
                    foreach (var cate in cates)
                    {
                        var cateSetting = setting.Find(c => c.Id == cate.Id);
                        if (cateSetting == null) continue;
                        var mode = cateSetting.Mode;
                        var data = cateSetting.Values.Select(id => models.FirstOrDefault(m => m.Id == id)).ToList();

                        //var data = models.Where(m => cateSetting.Values.Contains(m.Id)).ToList();
                        var cdata = data.Select(d => d.Clone()).ToList();

                        cdata.ForEach(d =>
                        {
                            d.Img = $"{d.Img}{cateSetting.PictureDirect}.png";
                        });
                        catedData.Add(new CategoryedCourseThinModel
                        {
                            Caption = cate.Name,
                            Id = cate.Id,
                            Mode = mode,
                            Values = cdata
                        });
                    }
                }
                return catedData;
            }, null, TimeSpan.FromSeconds(60));
            return rlt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="setting"></param>
        /// <returns></returns>
        public async Task<List<CategoryedCourseThinModel>> GetCategoriedTop(long userId, List<HomeCourseSettingModel> setting)
        {
            var courselist = await GetCategoriedTopCourses(setting);
            if (userId > 0)
            {
                var useInfo = await _userCacheService.GetUser(userId);
                if (userId > 0 && useInfo.Level <= 3)
                {
                    var settingService = ObjectContainer.Resolve<ISettingService>();
                    var levelsetting = await settingService.GetLevelCourseSetting();
                    for (int i = courselist.Count - 1; i >= 0; i--)
                    {

                        List<CourseThinModel> Clist = new List<CourseThinModel>();
                        Clist = LevelThinCourse(courselist[i].Values, levelsetting, useInfo.Level);
                        courselist[i].Values = Clist;
                    }
                }
                #region  旧方法
                //if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
                //{
                //    for (int i = courselist.Count - 1; i >= 0; i--)
                //    {
                //        #region  移除非会员不显示的课程
                //        List<long> removelist = new List<long>();
                //        removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);
                //        RemoveThinCourse(courselist[i].Values, removelist, useInfo.AccountType);
                //        #endregion
                //    }
                //}
                #endregion
            }
            return courselist;
        }
        public async Task<IEnumerable<CourseThinModel>> GetWithChildModelsAsync()
        {
            var rlt = await _cache.Get(COURSE_THIN_MODEL_CACHE, async () =>
            {
                var imgSvr = GetConfig("imgserver");
                var courses = await GetAll();
                var child = await _courseChildService.GetAll();

                var models = courses.OrderBy(c => c.Sort).MapTo<List<CourseThinModel>>();
                var grp = child.OrderBy(c => c.ChildSort).GroupBy(c => c.ParentId).ToDictionary(d => d.Key, d => d.ToList());
                var key = RedisKeyHelper.CreateTaskCourseLerunCount();
                var dict = await Redis.HashGetDictionaryWithTypeAsync<long>(key);
                var ct = DateTime.Now;
                foreach (var model in models)
                {
                    model.Img = $"{imgSvr}{model.Img}{model.Id}_";
                    if (dict.TryGetValue(model.Id.ToString(), out var value))
                    {
                        model.StudentsCount = FormatLearningNumber(value);
                    }
                    else
                    {
                        model.StudentsCount = FormatLearningNumber(model.Id);
                    }

                    if (grp.TryGetValue(model.Id, out var ch))
                    {
                        model.Children = ch.MapTo<List<ChildCourseThinModel>>();
                    }


                    if (model.CourseType == 1)
                    {

                        if (ct > model.LiveStart)
                        {
                            model.LiveStatus = 1;
                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.Where(c => c.ChildType == 1 && (c.LiveStart.HasValue)).OrderBy(c => Math.Abs((c.LiveStart.Value - ct).TotalSeconds)).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }

                        }
                        else if (ct.AddMinutes(30) > model.LiveStart)
                        {
                            model.LiveStatus = 2;
                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.Where(c => (c.LiveStart.HasValue)).OrderBy(c => Math.Abs((c.LiveStart.Value - ct).TotalSeconds)).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }
                        }
                        else
                        {
                            model.LiveStatus = 0;
                            if (model.Children != null && model.Children.Count() > 0)
                            {

                                var cmodel = model.Children.Where(c => c.ChildType == 1).OrderBy(c => c.LiveStart).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }

                        }
                    }
                    else if (model.CourseType == 2)
                    {
                        if (ct > model.LiveStart && ct < model.LiveEnd)
                        {

                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.Where(c => c.ChildType == 4).OrderBy(c => c.LiveStart).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }
                        }
                        else
                        {

                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.OrderBy(c => c.LiveStart).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }
                        }
                        model.LiveStatus = 0;
                        model.CourseType = 1;//直播课  app上线后解注
                    }
                    else
                    {
                        model.LiveStatus = 0;
                        model.CourseType = 2;//录播课
                        model.TimeDesc = model.LiveStart.ToString("yyyy-MM-dd hh:mm");
                    }
                }
                return models;
            }, null, TimeSpan.FromSeconds(60));
            return rlt;
        }
        #endregion

        #region 级别配置课程
        public async Task<IEnumerable<CourseThinModel>> GetWithLevelChildModelsAsync()
        {
            var rlt = await _cache.Get(COURSE_THIN_LEVEL_MODEL_CACHE, async () =>
            {
                var imgSvr = GetConfig("imgserver");
                var courses = await GetAll();
                var child = await _courseChildService.GetAll();

                var models = courses.OrderBy(c => c.Sort).MapTo<List<CourseThinModel>>();
                var grp = child.OrderBy(c => c.ChildSort).GroupBy(c => c.ParentId).ToDictionary(d => d.Key, d => d.ToList());
                var key = RedisKeyHelper.CreateTaskCourseLerunCount();
                var dict = await Redis.HashGetDictionaryWithTypeAsync<long>(key);
                var ct = DateTime.Now;
                foreach (var model in models)
                {
                    model.Img = $"{imgSvr}{model.Img}{model.Id}_";
                    if (dict.TryGetValue(model.Id.ToString(), out var value))
                    {
                        model.StudentsCount = FormatLearningNumber(value);
                    }
                    else
                    {
                        model.StudentsCount = FormatLearningNumber(model.Id);
                    }

                    if (grp.TryGetValue(model.Id, out var ch))
                    {
                        model.Children = ch.MapTo<List<ChildCourseThinModel>>();
                    }


                    if (model.CourseType == 1)
                    {

                        if (ct > model.LiveStart)
                        {
                            model.LiveStatus = 1;
                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.Where(c => c.ChildType == 1 && (c.LiveStart.HasValue)).OrderBy(c => Math.Abs((c.LiveStart.Value - ct).TotalSeconds)).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }

                        }
                        else if (ct.AddMinutes(30) > model.LiveStart)
                        {
                            model.LiveStatus = 2;
                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.Where(c => (c.LiveStart.HasValue)).OrderBy(c => Math.Abs((c.LiveStart.Value - ct).TotalSeconds)).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }
                        }
                        else
                        {
                            model.LiveStatus = 0;
                            if (model.Children != null && model.Children.Count() > 0)
                            {

                                var cmodel = model.Children.Where(c => c.ChildType == 1).OrderBy(c => c.LiveStart).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }

                        }
                    }
                    else if (model.CourseType == 2)
                    {
                        if (ct > model.LiveStart && ct < model.LiveEnd)
                        {

                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.Where(c => c.ChildType == 4).OrderBy(c => c.LiveStart).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }
                        }
                        else
                        {

                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.OrderBy(c => c.LiveStart).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }
                        }
                        model.LiveStatus = 0;
                        model.CourseType = 1;//直播课  app上线后解注
                    }
                    else
                    {
                        model.LiveStatus = 0;
                        model.CourseType = 2;//录播课
                        model.TimeDesc = model.LiveStart.ToString("yyyy-MM-dd hh:mm");
                    }
                }
                return models;
            }, null, TimeSpan.FromSeconds(60));
            return rlt;
        }
        public async Task<List<CourseThinModel>> GetLevelList(List<LevelCourseSettingModel> setting, long userId, int level)
        {
            UserBaseInfo userinfo = new UserBaseInfo();
            if (userId > 0)
            {
                userinfo = await _userCacheService.GetUser(userId);
            }
            var cateSetting = setting.Find(c => c.Id == level);
            if (cateSetting == null) return null;
            var models = await GetWithLevelChildModelsAsync();
            var data = cateSetting.Values.Select(id => models.FirstOrDefault(m => m.Id == id)).ToList();
            var cdata = data.Select(d => d.Clone()).ToList();
            cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");

            if (userId > 0 && userinfo.Level <= 3)
            {
                var settingService = ObjectContainer.Resolve<ISettingService>();
                var levelsetting = await settingService.GetLevelCourseSetting();
                List<CourseThinModel> Clist = new List<CourseThinModel>();
                Clist = LevelThinCourse(cdata, levelsetting, userinfo.Level);
                cdata = Clist;
            }
            #region 注释旧方法
            //if (userId > 0)
            //{
            //    List<CourseThinModel> Clist = new List<CourseThinModel>();
            //    foreach (CourseThinModel item in cdata)
            //    {
            //        if (userinfo.AuthCateorys.Contains(item.Category.ToString()))
            //        {
            //            Clist.Add(item);
            //        }
            //    }
            //    List<long> removelist = new List<long>();
            //    var useInfo = await _userCacheService.GetUser(userId);
            //    if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
            //    {
            //        removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);
            //        RemoveThinCourse(Clist, removelist, useInfo.AccountType);
            //    }
            //    cdata = Clist;
            //}
            #endregion
            return cdata;
        }
        #endregion
        #region web 首页



        public async Task<List<CoursePcThinModel>> GetPcHotList(List<HomeCourseSettingModel> setting)
        {
            var rlt = await _cache.Get("__Pchot", async () =>
            {
                var models = await GetPcWithChildModelsAsync();
                var cateSetting = setting.Find(c => c.Id == -1);
                if (cateSetting == null) return null;
                var data = cateSetting.Values.Select(id => models.FirstOrDefault(m => m.Id == id)).ToList();
                //var data = models.Where(m => cateSetting.Values.Contains(m.Id)).ToList();
                var cdata = data.Select(d => d.PcClone()).ToList();
                cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");
                return cdata;
            }, null, TimeSpan.FromSeconds(60));
            return rlt;
        }
        public async Task<List<CoursePcThinModel>> GetPcLiveList(List<HomeCourseSettingModel> setting)
        {
            var rlt = await _cache.Get("__Pclive", async () =>
            {
                var models = await GetPcWithChildModelsAsync();
                var cateSetting = setting.Find(c => c.Id == -2);
                if (cateSetting == null) return null;
                var data = cateSetting.Values.Select(id => models.FirstOrDefault(m => m.Id == id)).ToList();
                //var data = models.Where(m => cateSetting.Values.Contains(m.Id)).ToList();
                var cdata = data.Select(d => d.PcClone()).ToList();
                cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");
                return cdata;
            }, null, TimeSpan.FromSeconds(60));
            return rlt;
        }
        public async Task<List<CoursePcThinModel>> GetPcRecommendList(List<HomeCourseSettingModel> setting)
        {
            var rlt = await _cache.Get("__Pcrecomment", async () =>
            {
                var models = await GetPcWithChildModelsAsync();
                var cateSetting = setting.Find(c => c.Id == -3);
                if (cateSetting == null) return null;
                var data = cateSetting.Values.Select(id => models.FirstOrDefault(m => m.Id == id)).ToList();
                //var data = models.Where(m => cateSetting.Values.Contains(m.Id)).ToList();
                var cdata = data.Select(d => d.PcClone()).ToList();
                cdata.ForEach(d => d.Img = $"{d.Img}{cateSetting.PictureDirect}.png");
                return cdata;
            }, null, TimeSpan.FromSeconds(60));
            return rlt;
        }
        public async Task<List<CategoryedCoursePcThinModel>> GetPcCategoriedTopCourses(List<HomeCourseSettingModel> setting)
        {
            var imgSvr = GetConfig("imgserver");
            var rlt = await _cache.Get("__Pccate", async () =>
            {
                var catedData = new List<CategoryedCoursePcThinModel>();
                var cates = new List<CourseCateoryModel>();
                var models = await GetPcWithChildModelsAsync();

                var cids = setting.Where(s => s.Id > 0).OrderBy(s => s.Sort).Select(s => s.Id).ToList();
                if (cids.Count() == 0) return null;
                var cateCategorys = _courseCateoryService.Table();
                foreach (var cid in cids)
                {
                    var cate = cateCategorys.FirstOrDefault(c => c.Id == cid);
                    if (cate != null)
                    {
                        cates.Add(cate);
                    }
                }

                foreach (var cate in cates)
                {
                    var cateSetting = setting.Find(c => c.Id == cate.Id);
                    if (cateSetting == null) continue;
                    var mode = cateSetting.Mode;
                    var data = cateSetting.Values.Select(id => models.FirstOrDefault(m => m.Id == id)).ToList();

                    //var data = models.Where(m => cateSetting.Values.Contains(m.Id)).ToList();
                    var cdata = data.Select(d => d.PcClone()).ToList();

                    cdata.ForEach(d =>
                    {
                        d.Img = $"{d.Img}{cateSetting.PictureDirect}.png";
                    });
                    catedData.Add(new CategoryedCoursePcThinModel
                    {
                        Caption = cate.Name,
                        Id = cate.Id,
                        Mode = mode,
                        Values = cdata
                    });
                }

                return catedData;
            }, null, TimeSpan.FromSeconds(60));
            return rlt;
        }
        public async Task<List<CategoryedCoursePcThinModel>> GetPcCategoriedTop(long userId, List<HomeCourseSettingModel> setting)
        {
            var courselist = await GetPcCategoriedTopCourses(setting);
            if (userId > 0)
            {
                var useInfo = await _userCacheService.GetUser(userId);
                if (userId > 0 && useInfo.Level <= 3)
                {
                    var settingService = ObjectContainer.Resolve<ISettingService>();
                    var levelsetting = await settingService.GetLevelCourseSetting();
                    for (int i = courselist.Count - 1; i >= 0; i--)
                    {
                        List<CoursePcThinModel> newlist = new List<CoursePcThinModel>();
                        newlist = LevelPcThinCourse(courselist[i].Values, levelsetting, useInfo.Level);
                        courselist[i].Values = newlist;
                    }
                }
                #region 移除旧方法
                //if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
                //{
                //    for (int i = courselist.Count - 1; i >= 0; i--)
                //    {
                //        #region  移除非会员不显示的课程
                //        List<long> removelist = new List<long>();
                //        removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);
                //        RemovePcThinCourse(courselist[i].Values, removelist, useInfo.AccountType);
                //        #endregion
                //    }

                //}
                #endregion
            }
            return courselist;
        }


        public async Task<Dictionary<int, List<CoursePcThinModel>>> GetTeacherCourses(long teacherId)
        {
            var all = await GetPcWithChildModelsAsync();
            return all.Where(c => c.TeacherIds.Contains(teacherId)).GroupBy(c => c.Category).ToDictionary(g => g.Key, g => g.ToList());
        }

        public async Task<IEnumerable<CoursePcThinModel>> GetPcWithChildModelsAsync()
        {
            var rlt = await _cache.Get("__pctm", async () =>
            {
                var imgSvr = GetConfig("imgserver");
                var courses = await GetAll();
                var child = await _courseChildService.GetAll();

                var models = courses.OrderBy(c => c.Sort).MapTo<List<CoursePcThinModel>>();
                var grp = child.OrderBy(c => c.ChildSort).GroupBy(c => c.ParentId).ToDictionary(d => d.Key, d => d.ToList());
                var key = RedisKeyHelper.CreateTaskCourseLerunCount();
                var dict = await Redis.HashGetDictionaryWithTypeAsync<long>(key);
                var ct = DateTime.Now;
                foreach (var model in models)
                {
                    model.Img = $"{imgSvr}{model.Img}{model.Id}_";
                    if (dict.TryGetValue(model.Id.ToString(), out var value))
                    {
                        model.StudentsCount = FormatLearningNumber(value);
                    }
                    else
                    {
                        model.StudentsCount = FormatLearningNumber(model.Id);
                    }

                    if (grp.TryGetValue(model.Id, out var ch))
                    {
                        model.Children = ch.MapTo<List<ChildCourseThinModel>>();
                        model.ChildCount = model.Children.Count();
                    }

                    if (model.CourseType == 1)
                    {

                        if (ct > model.LiveStart)
                        {
                            model.LiveStatus = 1;
                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.Where(c => c.ChildType == 1 && (c.LiveStart.HasValue)).OrderBy(c => Math.Abs((c.LiveStart.Value - ct).TotalSeconds)).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }

                        }
                        else if (ct.AddMinutes(30) > model.LiveStart)
                        {
                            model.LiveStatus = 2;
                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.Where(c => (c.LiveStart.HasValue)).OrderBy(c => Math.Abs((c.LiveStart.Value - ct).TotalSeconds)).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }
                        }
                        else
                        {
                            model.LiveStatus = 0;
                            if (model.Children != null && model.Children.Count() > 0)
                            {

                                var cmodel = model.Children.Where(c => c.ChildType == 1).OrderBy(c => c.LiveStart).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }

                        }
                    }
                    else if (model.CourseType == 2)
                    {
                        if (ct > model.LiveStart && ct < model.LiveEnd)
                        {

                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.Where(c => c.ChildType == 4).OrderBy(c => c.LiveStart).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }
                        }
                        else
                        {

                            if (model.Children != null && model.Children.Count() > 0)
                            {
                                var cmodel = model.Children.OrderBy(c => c.LiveStart).FirstOrDefault();
                                if (cmodel != null) model.TimeDesc = cmodel.TimeDesc;
                            }
                        }
                        model.LiveStatus = 0;

                    }
                    else
                    {
                        model.LiveStatus = 0;
                        model.TimeDesc = model.LiveStart.ToString("yyyy-MM-dd hh:mm");

                    }

                }
                return models;
            }, null, TimeSpan.FromSeconds(60));

            //from c in courses
            //    join ch in child
            //    on c.Id equals ch.ParentId
            //    group new CourseThinModel() by c.Id into g

            return rlt;
        }
        #endregion
        public async Task<List<CoursePcThinModel>> GetPcWithChildModelsList(long userId)
        {

            var models = await GetPcWithChildModelsAsync();
            var list = models.ToList();
            if (userId > 0)
            {
                var useInfo = await _userCacheService.GetUser(userId);
                if (useInfo.Level <= 3)
                {
                    var settingService = ObjectContainer.Resolve<ISettingService>();
                    var levelsetting = await settingService.GetLevelCourseSetting();
                    List<CoursePcThinModel> newlist = new List<CoursePcThinModel>();
                    newlist = LevelPcThinCourse(list, levelsetting, useInfo.Level);
                    list = newlist;
                }
            }
            #region  移除非会员不显示的课程
            //List<long> removelist = new List<long>();
            //if (userId > 0)
            //{
            //    var useInfo = await _userCacheService.GetUser(userId);
            //    if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
            //    {

            //        int[] shiyonglist;
            //        if (useInfo.AccountType == 512)//使用账号过滤设置的白名单的课程
            //        {
            //            List<CoursePcThinModel> newClist = new List<CoursePcThinModel>();
            //            shiyonglist = Redis.StringGetWithType<int[]>(RedisKeyHelper.ShiYong_CourseList);
            //            // Clist.Where(a => shiyonglist.Contains(t => t.Equals(a.Id))).ToList();
            //            newClist = list.Where(a => a.Id.IsIn(shiyonglist)).ToList();
            //            return newClist;
            //        }
            //        else
            //        {
            //            removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);

            //            for (int i = list.Count - 1; i >= 0; i--)
            //            {
            //                if (removelist.Contains(t => t.Equals(list[i].Id)))
            //                {
            //                    list.Remove(list[i]);
            //                    Logger.Error("首页PC课程移除成功" + list[i].Name + "课Id" + list[i].Id);
            //                }
            //            }
            //            return list;
            //        }
            //    }
            //    else
            //    {
            //        return list;
            //    }
            //}
            //else
            //{
            //    return list;
            //}
            #endregion
            return list;
        }
        private string FormatLearningNumber(long n)
        {
            if (n < 10000) return $"{n}人在学";
            var t = (decimal)n / 10000;
            t = Math.Round(t, 2);
            var v = t.ToString().RemovePostFix(new string[] { ".00" });
            return $"{v}万人在学";
        }
        //递归移除符合条件的课程列表
        public void RemoveCourse(PageList<CourseQueryModel> list, List<long> removelist, int acounttype)
        {
            if (acounttype == 512)//使用账号过滤设置的白名单的课程
            {
                List<long> shiyonglist = new List<long>();
                List<CourseQueryModel> newClist = new List<CourseQueryModel>();
                shiyonglist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.ShiYong_CourseList);
                newClist = list.Where(a => shiyonglist.Contains(t => t.Equals(a.Id))).ToList();
                if (newClist.Count > 0)
                {
                    list = new PageList<CourseQueryModel>();
                    list.AddRange(newClist);
                }
            }
            else
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (removelist.Contains(t => t.Equals(list[i].Id)))
                    {
                        list.Remove(list[i]);
                        Logger.Error("课程列表移除成功" + list[i].CourseName + "课Id" + list[i].Id);
                    }
                }
            }
        }
        public List<CourseQueryModel> LevelCourse(PageList<CourseQueryModel> list, List<LevelCourseListSettingModel> levelsetting, int level)
        {

            #region  对账号指定level级别配置课程

            List<CourseQueryModel> newClist = new List<CourseQueryModel>();
            long[] values = null;
            switch (level)
            {
                case 1:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 1).Values;
                        break;
                    }
                case 2:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 2).Values;
                        break;
                    }
                case 3:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 3).Values;
                        break;
                    }
                case 4:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 4).Values;
                        break;
                    }
                default:
                    {

                        break;
                    }
            }
            if (values.Length > 0)
            {

                newClist = list.Where(a => a.Id.IsIn(values)).ToList();
                //var Clist = list.Where(a => values.Contains(t => t.Equals(a.Id))).ToList();
                //newClist.AddRange(Clist);
                //if (newClist.Count > 0)
                //{
                //    list = new PageList<CourseQueryModel>();
                //    list.AddRange(newClist);
                //}
            }
            return newClist;
            #endregion
        }

        //递归移除符合条件的app home课程
        public void RemoveThinCourse(List<CourseThinModel> list, List<long> removelist, int acounttype)
        {
            if (acounttype == 512)//使用账号过滤设置的白名单的课程
            {
                #region  对试用账号指定的配置课程
                int[] shiyonglist;
                List<CourseThinModel> newClist = new List<CourseThinModel>();
                shiyonglist = Redis.StringGetWithType<int[]>(RedisKeyHelper.ShiYong_CourseList);
                newClist = list.Where(a => a.Id.IsIn(shiyonglist)).ToList();
                if (newClist.Count > 0)
                {
                    list = new List<CourseThinModel>();
                    list.AddRange(newClist);
                }
                #endregion
            }
            else
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (removelist.Contains(t => t.Equals(list[i].Id)))
                    {
                        list.Remove(list[i]);
                        Logger.Error("app首页课程移除成功" + list[i].Name + "课Id" + list[i].Id);
                    }
                }
            }
        }
        public List<CourseThinModel> LevelThinCourse(List<CourseThinModel> list, List<LevelCourseSettingModel> levelsetting, int level)
        {

            #region  对账号指定level级别配置课程
            List<CourseThinModel> newClist = new List<CourseThinModel>();
            int[] values = null;
            switch (level)
            {
                case 1:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 1).Values;
                        break;
                    }
                case 2:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 2).Values;
                        break;
                    }
                case 3:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 3).Values;
                        break;
                    }
                case 4:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 4).Values;
                        break;
                    }
                default:
                    {

                        break;
                    }
            }
            if (values.Length > 0)
            {
                newClist = list.Where(a => a.Id.IsIn(values)).ToList();
            }
            return newClist;
            #endregion
        }

        /// <summary>
        /// 递归移除符合条件的pc home课程
        /// </summary>
        /// <param name="list"></param>
        /// <param name="removelist"></param>
        public void RemovePcThinCourse(List<CoursePcThinModel> list, List<long> removelist, int acounttype)
        {
            if (acounttype == 512)//使用账号过滤设置的白名单的课程
            {
                #region  对试用账号指定的配置课程
                int[] shiyonglist;
                List<CoursePcThinModel> newClist = new List<CoursePcThinModel>();
                shiyonglist = Redis.StringGetWithType<int[]>(RedisKeyHelper.ShiYong_CourseList);
                newClist = list.Where(a => a.Id.IsIn(shiyonglist)).ToList();
                if (newClist.Count > 0)
                {
                    list = new List<CoursePcThinModel>();
                    list.AddRange(newClist);
                }
                #endregion
            }
            else
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (removelist.Contains(t => t.Equals(list[i].Id)))
                    {
                        list.Remove(list[i]);
                        Logger.Error("首页PC课程移除成功" + list[i].Name + "课Id" + list[i].Id);
                    }
                }
            }

        }
        public List<CoursePcThinModel> LevelPcThinCourse(List<CoursePcThinModel> list, List<LevelCourseSettingModel> levelsetting, int level)
        {

            #region  对账号指定level级别配置课程
            List<CoursePcThinModel> newClist = new List<CoursePcThinModel>();
            int[] values = null;
            switch (level)
            {
                case 1:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 1).Values;
                        break;
                    }
                case 2:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 2).Values;
                        break;
                    }
                case 3:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 3).Values;
                        break;
                    }
                case 4:
                    {
                        values = levelsetting.FirstOrDefault(s => s.Id == 4).Values;
                        break;
                    }
                default:
                    {

                        break;
                    }
            }
            if (values.Length > 0)
            {
                newClist = list.Where(a => a.Id.IsIn(values)).ToList();
            }
            return newClist;
            #endregion
        }

        public async Task<Result<CouerseListInfo>> GetIndexListInfo(long userId, string aid, int classType, int pageIndex, int pageSize, int livePageSize)
        {
            try
            {
                #region  查询条件
                Expression<Func<CourseQueryModel, bool>> predicate = null;
                if (classType == 0)
                {
                    predicate = n => n.AppSourse == 0 && n.Status == 0 && n.IsDelete == 0 && n.IsTop == 1;
                }
                else
                {
                    predicate = n => n.AppSourse == 0 && n.Status == 0 && n.IsDelete == 0 && n.GradeIds.Contains(classType.ToString()) && n.IsTop == 1;
                }
                if (livePageSize >= 3)
                {
                    var pageSizekey = RedisKeyHelper.CourseIndexCount();
                    if (Redis.KeyExists(pageSizekey))
                    {
                        string livePageSizestr = Redis.StringGet(pageSizekey);
                        livePageSize = int.Parse(livePageSizestr);
                    }
                    else
                    {
                        livePageSize = livePageSize + 6;
                        Redis.StringSet(pageSizekey, livePageSize.ToString());
                    }
                }
                #endregion

                var liveList = await GetPageListAsync(predicate, order => order.Asc(n => n.Sort), livePageSize, 1);
                var coureList = await GetPageListAsync(predicate, order => order.Asc(n => n.Sort), livePageSize, pageIndex);
                var key = RedisKeyHelper.CreateTaskCourseLerunCount();
                var dict = await Redis.HashGetDictionaryWithTypeAsync<long>(key);
                #region  移除非会员不显示的课程
                List<CourseQueryModel> newClist = new List<CourseQueryModel>();
                List<long> removelist = new List<long>();
                if (userId > 0)
                {
                    var useInfo = await _userCacheService.GetUser(userId);
                    if (useInfo.Level <= 3)//非会员 内部身份
                    {
                        var settingService = ObjectContainer.Resolve<ISettingService>();
                        var levelsetting = await settingService.GetLevelCourselistSetting();
                        newClist = LevelCourse(coureList, levelsetting, useInfo.Level);
                    }
                    //if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
                    //{
                    //    removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);
                    // RemoveCourse(coureList, removelist, useInfo.AccountType);
                    //}
                }
                else
                {
                    newClist = coureList.ToList();
                }
                #endregion
                newClist.ForEach(n =>
               {

                   if (dict.TryGetValue(n.Id.ToString(), out var value))
                   {
                       n.VirtualSales = Convert.ToInt32(value);
                       n.StudentsCount = FormatLearningNumber(value);
                   }
                   else
                   {
                       n.VirtualSales = 0;
                       n.StudentsCount = FormatLearningNumber(n.Id);
                   }
               });
                var result = new CouerseListInfo()
                {
                    TotalCount = coureList.TotalCount,
                    PageIndex = coureList.PageIndex,
                    PageSize = coureList.PageSize,
                    // LiveList = liveList.MapTo<PageList<CouerseInfo>>(),
                    CouerseList = newClist.MapTo<PageList<CouerseInfo>>()
                };

                return await Task.FromResult(new Result<CouerseListInfo>() { Data = result });

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询GetIndexListInfo主课程异常", ex);
                return await Task.FromResult(new Result<CouerseListInfo>() { Code = (int)ResultCode.Exception, Message = "查询GetIndexListInfo主课程异常！" });
            }
        }

        public async Task<Result<CouerseIndexListInfo>> GetIndexList(long userId, string aid, int classType, int pageIndex, int pageSize)
        {
            try
            {
                //读取IsLive == 1 的推荐课程
                string IosVersion = "0";

                var topList = _courseCateoryService.Table().Where(n => n.IsShow >= 1 && n.ParentId == 0 && n.IsHomeTop == 1).ToList();
                var navList = _courseCateoryService.Table().Where(n => n.IsShow >= 1 && n.ParentId == 0 && n.IsNav == 1).ToList();
                #region  查询条件
                Expression<Func<CourseQueryModel, bool>> predicate = null;
                if (classType == 0)
                {
                    predicate = n => n.AppSourse == 0 && n.Status == 0 && n.IsDelete == 0 && n.IsTop == 1;
                }
                else
                {
                    predicate = n => n.AppSourse == 0 && n.Status == 0 && n.IsDelete == 0 && n.GradeIds.Contains(classType.ToString()) && n.IsTop == 1;
                }
                #endregion
                var coureList = await GetPageListAsync(predicate, order => order.Asc(n => n.Sort), pageSize, pageIndex);
                var key = RedisKeyHelper.CreateTaskCourseLerunCount();
                var dict = await Redis.HashGetDictionaryWithTypeAsync<long>(key);
                #region  移除非会员不显示的课程
                List<long> removelist = new List<long>();
                List<CourseQueryModel> newClist = new List<CourseQueryModel>();
                if (userId > 0)
                {
                    var useInfo = await _userCacheService.GetUser(userId);
                    if (useInfo.Level <= 3)//非会员 内部身份
                    {
                        var settingService = ObjectContainer.Resolve<ISettingService>();
                        var levelsetting = await settingService.GetLevelCourselistSetting();
                        newClist = LevelCourse(coureList, levelsetting, useInfo.Level);
                    }
                    //if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
                    //{
                    //    removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);
                    //    RemoveCourse(coureList, removelist, useInfo.AccountType);

                    //}
                }
                else
                {
                    newClist = coureList.ToList();
                }
                #endregion
                newClist.ForEach(n =>
                {
                    //var rescount = 0;
                    //var taskkey = RedisKeyHelper.CreateTaskCourseLerunCount();
                    //var Total = Redis.HashGet(taskkey, n.Id.ToString());
                    //if (!string.IsNullOrEmpty(Total))
                    //{
                    //    rescount = Convert.ToInt32(Total);
                    //}
                    //n.VirtualSales = n.Sales + Convert.ToInt32(n.Id) + rescount;
                    if (dict.TryGetValue(n.Id.ToString(), out var value))
                    {
                        n.VirtualSales = Convert.ToInt32(value);
                        n.StudentsCount = FormatLearningNumber(value);
                    }
                    else
                    {
                        n.VirtualSales = 0;
                        n.StudentsCount = FormatLearningNumber(n.Id);
                    }
                });
                var result = new CouerseIndexListInfo()
                {
                    IosVersion = IosVersion,
                    TotalCount = coureList.TotalCount,
                    PageIndex = coureList.PageIndex,
                    PageSize = coureList.PageSize,
                    CouerseList = newClist.MapTo<PageList<CouerseInfo>>(),
                    TopCateoryList = topList.MapTo<List<CateoryInfo>>(),
                    NavCateoryList = navList.MapTo<List<CateoryInfo>>()
                };
                return await Task.FromResult(new Result<CouerseIndexListInfo>() { Data = result });

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询GetIndexList主课程异常", ex);
                return await Task.FromResult(new Result<CouerseIndexListInfo>() { Code = (int)ResultCode.Exception, Message = "查询GetIndexList主课程异常！" });
            }

        }

        //WHY:编写测试用例 GetCouerseList
        public async Task<Result<CouerseListInfo>> GetCouerseList(long userId, string aid, int cateoryId, int selectType, int pageIndex, int pageSize)
        {
            try
            {
                #region 查询条件
                Expression<Func<CourseQueryModel, bool>> predicate = n => n.AppSourse == 0 && n.IsDelete == 0 && n.Status == 0;
                if (cateoryId > 0)
                {
                    predicate = predicate.And(n => n.Cateorys == cateoryId);
                }
                if (selectType > 0)//全部
                {
                    predicate = predicate.And(n => n.ClassId == selectType);
                }

                #endregion
                var coureList = await GetPageListAsync(predicate, order => order.Asc(n => n.Sort), pageSize, pageIndex);
                var key = RedisKeyHelper.CreateTaskCourseLerunCount();
                var dict = await Redis.HashGetDictionaryWithTypeAsync<long>(key);
                List<CourseQueryModel> newClist = new List<CourseQueryModel>();
                #region  移除非会员不显示的课程
                List<long> removelist = new List<long>();
                if (userId > 0)
                {
                    var useInfo = await _userCacheService.GetUser(userId);
                    if (useInfo.Level <= 3)//非会员 内部身份
                    {
                        var settingService = ObjectContainer.Resolve<ISettingService>();
                        var levelsetting = await settingService.GetLevelCourselistSetting();
                        newClist = LevelCourse(coureList, levelsetting, useInfo.Level);
                    }
                    else
                    {
                        newClist = coureList.ToList();
                    }
                    //if (useInfo.Aid == "college" && useInfo.IsMember == 0)//非会员 内部身份
                    //{
                    //    removelist = Redis.StringGetWithType<List<long>>(RedisKeyHelper.White_CourseList);
                    //    RemoveCourse(coureList, removelist, useInfo.AccountType);
                    //}
                }
                else
                {
                    newClist = coureList.ToList();
                }
                #endregion
                newClist.ForEach(n =>
                {
                    if (dict.TryGetValue(n.Id.ToString(), out var value))
                    {
                        n.VirtualSales = Convert.ToInt32(value);
                        n.StudentsCount = FormatLearningNumber(value);
                    }
                    else
                    {
                        n.VirtualSales = 0;
                        n.StudentsCount = FormatLearningNumber(n.Id);
                    }
                    n.CoursePicPhone = n.CoursePicPhone;
                    n.CoursePicPhoneMini = n.CoursePicPhoneMini;

                });
                var result = new CouerseListInfo()
                {
                    TotalCount = coureList.TotalCount,
                    PageIndex = coureList.PageIndex,
                    PageSize = coureList.PageSize,
                    CouerseList = newClist.MapTo<PageList<CouerseInfo>>()
                };
                return await Task.FromResult(new Result<CouerseListInfo>() { Data = result });

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询GetCouerseList主课程异常", ex);
                return await Task.FromResult(new Result<CouerseListInfo>() { Code = (int)ResultCode.Exception, Message = "查询GetCouerseList主课程异常！" });
            }

        }
        /// <summary>
        /// 获取主课程的子课程列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<Result<List<CourseChildInfo>>> GetChildCountAsync(long userId, long courseId)
        {
            return await _courseChildService.GetChildCountAsync(userId, courseId);
        }
        /// <summary>
        /// 获取主课程子课程学习完成的列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<Result<List<UserCourseChildInfo>>> GetUserChildCountAsync(long userId, long courseId)
        {
            return await _courseChildService.GetUserChildCountAsync(userId, courseId);
        }
        public async Task<Result<VideoLiveInfo>> GetVideoLiveInfo(long userId, int courseChildId, string UserIp)
        {
            try
            {


                var lrtchild = await _courseChildService.Get(courseChildId);
                var myInfo = await _userCacheService.GetUser(userId);
                var childInfo = lrtchild.Data;
                var lrt = await GetAsync(childInfo.ParentId);
                var courseInfo = lrt.Data;

                #region 写入mongo日志
                MessageInfo info = new MessageInfo();
                #region 子课程
                info.ChannelId = childInfo.ChannelId.Trim();
                info.ChildType = childInfo.ChildType;
                info.ChildId = childInfo.id;
                info.ChildName = childInfo.ChildName;
                info.ChildTeacher = childInfo.ChildTeacher;
                info.ChildTimeLength = childInfo.ChildTimeLength;
                #endregion
                #region  用户信息
                info.UserId = userId;
                info.UserName = myInfo.Username;
                info.UserPhone = myInfo.Phone;
                switch (myInfo.Sex)

                {
                    case 0:
                        info.sex = "女";
                        break;
                    case 1:
                        info.sex = "男";
                        break;
                    case 2:
                        info.sex = "未知";
                        break;
                    default:
                        info.sex = "未知";
                        break;
                }
                info.provincial = myInfo.Provincial;
                info.city = myInfo.City;
                #endregion
                #region 主课程
                info.CourseId = childInfo.ParentId;
                info.CourseName = courseInfo.CourseName;
                info.TeachersUserName = courseInfo.TeachersUserName;
                #endregion
                info.UserIp = UserIp;
                info.Datatime = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
                info.VistTime = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
                #region  详情
                if (childInfo.ChildType == 1)
                {
                    info.Title = "进入直播课";
                    info.Content = "学习了:" + childInfo.ChildName;
                }
                else if (childInfo.ChildType == 2)
                {
                    info.Title = "观看直播回放视频";
                    info.Content = "回看了:" + childInfo.ChildName;
                }
                else if (childInfo.ChildType == 3)
                {
                    info.Title = "观看了录播视频";
                    info.Content = "观看了:" + childInfo.ChildName;
                }
                else
                {
                    info.Title = "直播还未开启";
                    info.Content = "请求了:" + childInfo.ChildName;
                }
                #endregion
                //改为存到mogon
                _courseLogService.AddCourselog(info);
                //子课程播放量统计增加
                double value = 1;
                var taskkey = RedisKeyHelper.CreateTaskCourseChidLerunCount();
                var enumKey = RedisKeyHelper.CreateChildLerunCountKey(childInfo.ParentId, courseChildId);
                Redis.HashIncrement(taskkey, enumKey.ToString(), value);
                #endregion
                var result = new VideoLiveInfo()
                {
                    videoId = childInfo.videoId.Trim(),
                    channelId = childInfo.ChannelId.Trim(),
                    title = childInfo.ChildName
                };
                return await Task.FromResult(new Result<VideoLiveInfo>() { Data = result });

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询GetVideoLiveInfo子课程异常", ex);
                return await Task.FromResult(new Result<VideoLiveInfo>() { Code = (int)ResultCode.Exception, Message = "查询GetVideoLiveInfo子课程异常！" });
            }

        }

        /// <summary>
        /// 导出用户频道
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public async Task<Result<List<MessUserInfo>>> ExporChannelUser(long courseId, long channelid)
        {
            List<MessUserInfo> alllist = new List<MessUserInfo>();

            var lrt = await GetAsync(courseId);
            var courseInfo = lrt.Data;
            var userlist = await _userCacheService.GetUserList();
            List<User_ChannelEntity> list = new List<User_ChannelEntity>();
            if (channelid == 0)
            {
                list = _userChannelService.Table().ToList();
            }
            else
            {
                list = _userChannelService.Table().Where(p => p.channelid == channelid).ToList();
            }
            list.ForEach(n =>
            {
                MessUserInfo info = new MessUserInfo();
                #region 主课程
                if (courseInfo != null)
                {
                    info.CourseId = courseId;
                    info.CourseName = courseInfo.CourseName;
                    info.TeachersUserName = courseInfo.TeachersUserName;
                }
                #endregion
                #region  用户信息
                info.UserId = n.userid;
                var myInfo = userlist.Find(t => t.Userid == n.userid);
                if (myInfo != null)
                {
                    info.UserName = myInfo.Username;
                    info.UserPhone = myInfo.Phone;
                    switch (myInfo.Sex)

                    {
                        case 0:
                            info.sex = "女";
                            break;
                        case 1:
                            info.sex = "男";
                            break;
                        case 2:
                            info.sex = "未知";
                            break;
                        default:
                            info.sex = "未知";
                            break;
                    }
                    info.provincial = myInfo.Provincial;
                    info.city = myInfo.City;
                }
                #endregion
                info.VistTime = n.vt;
                alllist.Add(info);

            });

            return await Task.FromResult(new Result<List<MessUserInfo>>() { Data = alllist });
        }


        /// <summary>
        /// 导出用户频道
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public async Task<Result<List<CourseLogInfo>>> ExporOnlineUser(string ChannelId, long ChildId, int ChildType = 1)
        {

            var rlt = await _courseLogService.ExportExcelBaseChildId(ChannelId, ChildId, ChildType);

            return await Task.FromResult(new Result<List<CourseLogInfo>>() { Data = rlt.Data });
        }

        #region CMS


        public async Task<IEnumerable<CourseQueryModel>> GetCourseList(CourseQueryModel model)
        {
            var allCourse = await GetAll();
            var rlt = allCourse.AsQueryable();
            if (model.AppSourse != 0)
            {
                rlt = rlt.Where(p => p.AppSourse == 1);
            }
            else
            {
                rlt = rlt.Where(p => p.AppSourse == 0);
            }
            if (model.Status != 0)
            {
                rlt = rlt.Where(p => p.Status == 1);
            }
            else
            {
                rlt = rlt.Where(p => p.Status == 0);
            }
            if (model.GradeIds.IsNotNullOrEmpty())
            {
                rlt = rlt.Where(p => p.GradeIds == model.GradeIds);
            }
            if (model.ClassId > 0)
            {
                rlt = rlt.Where(p => p.ClassId == model.ClassId);
            }
            if (model.CourseName.IsNotNullOrEmpty())
            {
                rlt = rlt.Where(p => p.CourseName.Contains(model.CourseName));
            }
            if (model.TeachersUserName.IsNotNullOrEmpty())
            {
                rlt = rlt.Where(p => p.TeachersUserName.Contains(model.TeachersUserName));
            }
            if (model.Cateorys != 0)
            {
                rlt = rlt.Where(p => p.Cateorys.Equals(model.Cateorys));
            }
            return rlt;
        }

        public async Task<IEnumerable<CourseQueryModel>> GetCourseAllList(CourseQueryModel model)
        {
            var allCourse = await GetAll();

            var rlt = allCourse.AsQueryable();
            if (model.AppSourse != 0)
            {
                rlt = rlt.Where(p => p.AppSourse == 1);
            }
            else
            {
                rlt = rlt.Where(p => p.AppSourse == 0);
            }
            if (model.Status != 0)
            {
                rlt = rlt.Where(p => p.Status == 1);
            }
            else
            {
                rlt = rlt.Where(p => p.Status == 0);
            }
            if (model.GradeIds.IsNotNullOrEmpty())
            {
                rlt = rlt.Where(p => p.GradeIds == model.GradeIds);
            }
            if (model.ClassId > 0)
            {
                rlt = rlt.Where(p => p.ClassId == model.ClassId);
            }
            if (model.CourseName.IsNotNullOrEmpty())
            {
                rlt = rlt.Where(p => p.CourseName.Contains(model.CourseName));
            }
            if (model.TeachersUserName.IsNotNullOrEmpty())
            {
                rlt = rlt.Where(p => p.TeachersUserName.Contains(model.TeachersUserName));
            }
            if (model.Cateorys != 0)
            {
                rlt = rlt.Where(p => p.Cateorys.Equals(model.Cateorys));
            }
            return rlt;
        }

        public IEnumerable<CourseCMS> GetCourseListAll(CourseCMS model)
        {
            try
            {
                var body = UnitOfWorkService.Execute(() =>
                {
                    Expression<Func<CourseCMS, bool>> predicate = null;

                    predicate = n => n.IsDelete == 0;
                    if (model.Cateorys != "0" && model.Cateorys.IsNotNullOrEmpty())
                    {
                        predicate = n => n.IsDelete == 0 && n.Cateorys == model.Cateorys;
                    }
                    if (model.ChildType != 0 && model.ChildType.ToString().IsNotNullOrEmpty())
                    {
                        predicate = n => n.IsDelete == 0 && n.ChildType == model.ChildType;
                    }
                    return _repo.GetCourseChildList(predicate);

                });

                return body;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("GetCourseListAll", ex);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private long Add(CourseEntity entity)
        {
            var rlt = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            if (rlt > 0) RebuildCache();
            return rlt;

        }
        public bool Update(CourseEntity entity)
        {
            entity.CoursePicMini = RemoveResUrl(entity.CoursePicMini);
            entity.CoursePicPhone = RemoveResUrl(entity.CoursePicPhone);
            entity.CoursePicPhoneMini = RemoveResUrl(entity.CoursePicPhoneMini);
            entity.CoursePicContent = RemoveResUrl(entity.CoursePicContent);
            var rlt = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });
            if (rlt) RebuildCache();
            return rlt;

        }

        /// <summary>
        /// 增删改
        /// </summary>
        /// <param name="jsons"></param>
        /// <param name="optiden"></param>
        /// <returns></returns>
        public int Option(CourseQueryModel model, string optiden, string name)
        {
            try
            {
                //WHY:改为调用add,update,delete等方法，否则无法触发同步消息
                var body = 0;
                if (optiden == "add" || optiden == null || optiden == "")
                {

                    model.CreateTime = DateTime.Now;
                    model.UpdateTime = DateTime.Now;
                    //model.Status = 1;
                    model.CoursePicPhone = "img/collegecourse/" + model.CoursePicPhone;
                    model.CoursePicPhoneMini = "img/collegecourse/" + model.CoursePicPhoneMini;
                    model.CoursePicContent = RemoveResUrl(model.CoursePicContent);
                    var courseEntity = model.MapTo<CourseEntity>();
                    long returnId = Add(courseEntity);
                    if (returnId > 0)
                    {
                        //记录mongo日志
                        OperationInfo info = new OperationInfo();
                        info.UserName = name;
                        info.BusinessType = "课程管理";
                        info.CourseId = model.Id;
                        info.CourseName = model.CourseName;
                        info.CreateTime = DateTime.Now;
                        info.dec = "用户" + info.UserName + ",在" + info.CreateTime + "操作了" + info.BusinessType + "模块" + "增加了一门课,增加的课程名为：" + info.CourseName + ",id为：" + returnId;
                        _courseLogService.AddLogOfOperation(info);
                        return 1;
                    }

                    return 0;

                }
                if (optiden == "edit")
                {

                    model.UpdateTime = DateTime.Now;
                    bool isContain = model.CoursePicPhone.Contains("?");
                    if (isContain)
                    {
                        string[] sArray = model.CoursePicPhone.Split("?");
                        model.CoursePicPhone = "img/collegecourse/" + sArray[0];
                    }
                    else
                    {
                        model.CoursePicPhone = "img/collegecourse/" + model.CoursePicPhone;
                    }
                    bool isMiniContain = model.CoursePicPhoneMini.Contains("?");
                    if (isMiniContain)
                    {
                        string[] sMiniArray = model.CoursePicPhoneMini.Split("?");
                        model.CoursePicPhoneMini = "img/collegecourse/" + sMiniArray[0];
                    }
                    else
                    {
                        model.CoursePicPhoneMini = "img/collegecourse/" + model.CoursePicPhoneMini;
                    }
                    model.CoursePicContent = RemoveResUrl(model.CoursePicContent);
                    var courseEntity = model.MapTo<CourseEntity>();
                    var result = Update(courseEntity);
                    if (result == true)
                    {
                        //记录mongo日志
                        OperationInfo info = new OperationInfo();
                        info.UserName = name;
                        info.BusinessType = "课程管理";
                        info.CourseId = model.Id;
                        info.CourseName = model.CourseName;
                        info.CreateTime = DateTime.Now;
                        info.dec = "用户" + info.UserName + ",在" + info.CreateTime + "操作了" + info.BusinessType + "模块" + "修改了一门课,修改的课程名为：" + info.CourseName + ",id为：" + model.Id;
                        _courseLogService.AddLogOfOperation(info);
                        return 1;
                    }
                    return 0;

                }
                if (optiden == "del")
                {

                    model.IsDelete = 1;
                    model.Status = 1;
                    model.UpdateTime = DateTime.Now;
                    bool isContain = model.CoursePicPhone.Contains("?");
                    if (isContain)
                    {
                        string[] sArray = model.CoursePicPhone.Split("?");
                        model.CoursePicPhone = "img/collegecourse/" + sArray[0];
                    }
                    else
                    {
                        model.CoursePicPhone = "img/collegecourse/" + model.CoursePicPhone;
                    }
                    bool isMiniContain = model.CoursePicPhoneMini.Contains("?");
                    if (isMiniContain)
                    {
                        string[] sMiniArray = model.CoursePicPhoneMini.Split("?");
                        model.CoursePicPhoneMini = "img/collegecourse/" + sMiniArray[0];
                    }
                    else
                    {
                        model.CoursePicPhoneMini = "img/collegecourse/" + model.CoursePicPhoneMini;
                    }
                    model.CoursePicContent = RemoveResUrl(model.CoursePicContent);
                    var courseEntity = model.MapTo<CourseEntity>();
                    var result = Update(courseEntity);
                    if (result == true)
                    {
                        //记录mongo日志
                        OperationInfo info = new OperationInfo();
                        info.UserName = name;
                        info.BusinessType = "课程管理";
                        info.CourseId = model.Id;
                        info.CourseName = model.CourseName;
                        info.CreateTime = DateTime.Now;
                        info.dec = "用户" + info.UserName + ",在" + info.CreateTime + "操作了" + info.BusinessType + "模块" + "删除了一门课程，课程名称为：" + info.CourseName + ",id为：" + model.Id;
                        _courseLogService.AddLogOfOperation(info);
                        return 1;
                    }
                    return 0;

                }

                return body;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error("Option 课程操作异常", exception);
                throw;
            }
        }

        public IEnumerable<UserliveModel> GetLiveUserList(int CourseId)
        {
            try
            {
                var body = UnitOfWorkService.Execute(() =>
                {
                    return _repo.GetLiveUserList(CourseId);
                });
                return body;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error("GetLiveUserList异常", exception);
                throw;
            }
        }

        #endregion

        #region 生成html
        public async Task<Dictionary<long, string>> GenerateHtml(long courseId)
        {
            try
            {
                var dic = new Dictionary<long, string>();
                if (courseId == 0)
                {
                    var courseList = await GetAll();
                    foreach (var courseInfo in courseList)
                    {
                        var temps = new List<string>();
                        if (courseInfo.CoursePicContent.Contains("|"))
                        {
                            temps = courseInfo.CoursePicContent.Split('|', StringSplitOptions.RemoveEmptyEntries).Select(t => ResUrl(t)).ToList();
                        }
                        else
                            temps.Add(courseInfo.CoursePicContent);
                        courseInfo.CoursePicContents = temps;
                        var htmlPath = GenerateHtml(courseInfo);
                        dic.Add(courseInfo.Id, htmlPath);
                    }
                }
                else
                {
                    //获取当前课程
                    var lrt = await GetAsync(courseId);
                    var temps = new List<string>();
                    var courseInfo = lrt.Data;
                    if (courseInfo.CoursePicContent.Contains("|"))
                    {
                        temps = courseInfo.CoursePicContent.Split('|', StringSplitOptions.RemoveEmptyEntries).Select(t => ResUrl(t)).ToList();
                    }
                    else
                        temps.Add(courseInfo.CoursePicContent);
                    courseInfo.CoursePicContents = temps;
                    var htmlPath = GenerateHtml(courseInfo);
                    dic.Add(courseInfo.Id, htmlPath);
                }
                return dic;
            }
            catch (Exception ex)
            {
                if (courseId == 0)
                    Logger.Error($"生成所有课程html页面异常！", ex);
                else
                    Logger.Error($"生成课程{courseId}，html页面异常！", ex);
                throw;
            }
        }
        public async Task<Dictionary<string, string>> GenerateChildTimel(int courseId)
        {
            try
            {
                var dic = new Dictionary<string, string>();
                if (courseId == 0)
                {
                    CourseChildInfo model = new CourseChildInfo();
                    var list = _courseChildService.GetCourseChildList(model);
                    var uplist = list.Where(n => n.ChildType == 3).ToList();
                    foreach (var item in uplist)
                    {
                        if (!string.IsNullOrEmpty(item.videoId))
                        {
                            //            var timestamp1 = Date.parse(new Date());
                            var ptime = DateTimeUtil.GetTimeStamp(DateTime.Now);
                            var sha1Sign = GetShA1(item.videoId, ptime);
                            string posturl = "http://api.polyv.net/v2/video/be625c3cf9/get-video-msg?vid=" + item.videoId + "&ptime=" + ptime + "&sign=" + sha1Sign + "&format=json";
                            var result = HttpClientUtil.Get(posturl);
                            var jsonParams = JsonConvert.DeserializeObject<dynamic>(result.Result);
                            var jarray = jsonParams["data"] as JArray;
                            //获取dynamic里边的数据
                            string duration = jarray[0]["duration"].ToString();
                            var childtimestr = DateTimeUtil.GetChildTime(duration);
                            string vid = jarray[0]["vid"].ToString();
                            //转换时间格式
                            if (string.IsNullOrEmpty(item.ChildTimeLength))
                            {
                                item.ChildTimeLength = childtimestr;
                                _courseChildService.Update(item);
                                dic.Add(item.ChildName + "(" + item.id + ")" + "(" + item.ParentId + ")", childtimestr);
                            }
                        }
                    }
                }
                else
                {
                    CourseChildInfo model = new CourseChildInfo();
                    model.ParentId = courseId;
                    var list = _courseChildService.GetCourseChildList(model);
                    var uplist = list.Where(n => n.ChildType == 3).ToList();
                    foreach (var item in uplist)
                    {
                        if (!string.IsNullOrEmpty(item.videoId))
                        {
                            //            var timestamp1 = Date.parse(new Date());
                            var ptime = DateTimeUtil.GetTimeStamp(DateTime.Now);
                            var sha1Sign = GetShA1(item.videoId, ptime);
                            string posturl = "http://api.polyv.net/v2/video/be625c3cf9/get-video-msg?vid=" + item.videoId + "&ptime=" + ptime + "&sign=" + sha1Sign + "&format=json";
                            var result = HttpClientUtil.Get(posturl);
                            // JObject jsonObject = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(result.Result);
                            //把jsonObject反序列化成dynamic
                            // string jsonStr = JsonConvert.SerializeObject(jsonObject);
                            var jsonParams = JsonConvert.DeserializeObject<dynamic>(result.Result);
                            var jarray = jsonParams["data"] as JArray;
                            //获取dynamic里边的数据
                            string duration = jarray[0]["duration"].ToString();
                            var childtimestr = DateTimeUtil.GetChildTime(duration);
                            string vid = jarray[0]["vid"].ToString();
                            //转换时间格式
                            if (string.IsNullOrEmpty(item.ChildTimeLength))
                            {
                                item.ChildTimeLength = childtimestr;
                                _courseChildService.Update(item);
                                dic.Add(item.ChildName, childtimestr);
                            }
                        }
                    }
                }
                return dic;
            }
            catch (Exception ex)
            {
                if (courseId == 0)
                    Logger.Error($"获取所有子课程的视频时长异常！", ex);
                else
                    Logger.Error($"获取课程{courseId}，课程的视频时长异常！", ex);
                throw;
            }

        }
        public string GetShA1(string vid, string ptime)
        {
            string str = "format=json&ptime=" + ptime + "&vid=" + vid + "0fNUnkNdWI";

            //6、将字符串进行sha1加密  
            string signature = SHA1(str);
            return signature;
        }


        public static string SHA1(string decript)
        {
            return CommonHelper.Sha1(decript);
        }
        private string GenerateHtml(CourseQueryModel courseInfo)
        {
            var imgSvr = GetConfig("imgserver");
            if (courseInfo == null)
                throw new Exception("课程不存在");
            var path = $"{DomainSetting.UploadPath}{_outPath}";
            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);
            NVelocityHelper nVelocityHelper = new NVelocityHelper(
                DomainSetting.UploadPath,
                $"{_outPath}/{courseInfo.Id}",
                ".html"
                );
            nVelocityHelper.InitTemplateEngine(_templatePath);
            nVelocityHelper.AddKeyValue("courseInfo", courseInfo);
            var htmlPath = nVelocityHelper.ExecuteFile();
            if (htmlPath.IsNullOrEmpty())
                return "";
            //迁移到OSS
            _filesService.MoveFileToOss($"{path}/{courseInfo.Id}.html", $"{_outPath}/{courseInfo.Id}.html");
            return $"{DomainSetting.ResServerLocal}{_outPath}/{courseInfo.Id}.html";//{DomainSetting.ResourceUrl}
        }

        #endregion

        /// <summary>
        /// datetime(例：2020-02-17)
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public List<CourseAppointmentModel> SearchAllorTime(string datetime)
        {
            var allAppo = UnitOfWorkService.Execute(() => { return _repoAppo.GetList().OrderByDescending(p => p.Id).AsQueryable(); });
            if (datetime != null && datetime != "")
            {
                allAppo = allAppo.Where(p => p.CreateTime.ToShortDateString() == Convert.ToDateTime(datetime).ToShortDateString());
            }
            return allAppo.MapTo<List<CourseAppointmentModel>>();
        }

        public Task<Result> CourseAppointmentAdd(string name, string phone)
        {
            var body = UnitOfWorkService.Execute(() =>
             {
                 CourseAppointmentModel model = new CourseAppointmentModel();
                 model.Name = name;
                 model.Phone = phone;
                 model.CreateTime = DateTime.Now;
                 var entity = model.MapTo<CourseAppointmentEntity>();

                 return _repoAppo.InsertAndGetId(entity);
             });
            return Task.FromResult(new Result { Data = body.ToString() });
        }

        public async Task<Result> CreateCourseOrder(long userId, long courseId, string orderId, PlatFormType device = PlatFormType.ios)
        {
            var result = new Result();
            var lrt = await GetAsync(courseId);
            var courseEntity = lrt.Data;
            if (courseEntity == null)
            {
                result.Code = (int)ResultCode.Fail;
                result.Message = $"生成课程订单失败,课程不存在{courseId}";
                return result;
            }
            if (device != PlatFormType.web)
            {
                var isBuy = _courseBuyCollectService.Table().FirstOrDefault(t => t.UserId == userId && t.CourseId == courseId && t.IsBuy == 1 && t.IsHide == 0);
                if (isBuy != null)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"您已经购买了此课程，无需重复购买";
                    return result;
                }
            }

            courseEntity.ExpressPirce = courseEntity.IsExpress == 0 ? 0M : courseEntity.ExpressPirce;
            string spBillno = orderId;

            var payInfo = courseEntity.MapTo<PayOrderInfo>();
            payInfo.CourseId = courseEntity.Id;
            payInfo.IsDiscount = courseEntity.IsDiscount;
            decimal PayPrice = 0;
            int PayType = 0;
            if (device == PlatFormType.ios)
            {
                PayPrice = courseEntity.IosPayPirce;
                PayType = 4;
            }
            else
            {
                PayPrice = courseEntity.DiscountPrice;
                PayType = 2;

            }
            int UsableCount = 0;
            //如果开启优惠 录播课 可以兑换  收费直播课限制使用兑换券&& courseEntity.IsLive == 0
            if (courseEntity.IsDiscount == 1)
            {
                #region  获取可用的课程兑换券
                var rltCoupon = _couponInfoService.GetUsableCount(userId, (int)CouponStatus.NoUse, (int)BusinessType.Course);

                if (rltCoupon != null)
                {
                    if (rltCoupon.Result.Code == 200)
                    {
                        UsableCount = Convert.ToInt32(rltCoupon.Result.Data);
                    }
                }
                #endregion
            }
            payInfo.UsableCount = UsableCount;
            payInfo.PreferentialBeginTime = courseEntity.PreferentialBeginTime.ToString("yyyy.MM.dd");
            payInfo.PreferentialEndTime = courseEntity.PreferentialEndTime.ToString("yyyy.MM.dd");
            payInfo.ValidBeginTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
            payInfo.ValidEndTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
            payInfo.OrderNo = spBillno;
            if (courseEntity.DiscountPrice > 0)
            {
                CourseOrderEntity orderentity = new CourseOrderEntity();
                var courseOrder = _courseOrderService.Create(new CourseOrderEntity
                {
                    OrderNo = spBillno,
                    UserId = userId,
                    PayPrice = PayPrice,
                    OrderState = 0,
                    IsPay = 0,
                    PayTime = DateTime.Now,
                    PayType = PayType,
                    PayNo = spBillno,
                    CourseId = courseEntity.Id,
                    IsSend = 0,
                    SendTime = DateTime.Now,
                    IsGet = 0,
                    GetTime = DateTime.Now,
                    PackageNo = "",
                    PackageType = 0,
                    OrderDes = null,
                    AddressId = 0,
                    IsDelete = 0,
                    CreateId = 0,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                    TenantId = 0,
                    IsInvoice = 0,
                    InvoiceDes = ""
                });
                long.TryParse(courseOrder.ToString(), out long nid);
                if (nid == 0)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = $"生成订单失败";
                    return result;
                }

                //生成购买课程记录 回调修改状态
                var courseBuyCollectEntity = _courseBuyCollectService.Table()
                    .FirstOrDefault(n => n.CourseId == courseEntity.Id && n.UserId == userId);
                if (courseBuyCollectEntity == null)
                {
                    _courseBuyCollectService.Add(new CourseBuyCollectEntity
                    {
                        UserId = (int)userId,
                        CourseId = (int)courseEntity.Id,
                        CourseOrderId = spBillno,
                        CreateId = 0,
                        CreateTime = DateTime.Now,
                        IsBuy = 0,
                        IsHide = 0,
                        IsCollect = 0,
                    });
                }
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<int> OpenCourse(string orderId, long userId, long courseId)
        {
            try
            {
                var user = await _userCacheService.GetUser(userId);
                if (user == null) return (int)ResultCode.NoUser;
                var course = await GetAsync(courseId);
                if (course == null) return (int)ResultCode.NoCourse;
                var orderEntity = _courseOrderService.Table().FirstOrDefault(n => n.OrderNo == orderId);

                if (orderEntity == null)
                    return (int)ResultCode.NoUser;

                //数据库逻辑 修改订单状态以及其他相关业务
                orderEntity.IsPay = 1;
                orderEntity.OrderState = 1;
                orderEntity.PayType = 2;
                _courseOrderService.Update(orderEntity);

                //判断是否存在收藏 
                var courseBuyCollectEntity = _courseBuyCollectService.Table()
                    .FirstOrDefault(n => n.CourseId == courseId && n.UserId == orderEntity.UserId);
                if (courseBuyCollectEntity != null)
                {
                    courseBuyCollectEntity.CourseOrderId = orderId;
                    courseBuyCollectEntity.IsBuy = 1;
                    courseBuyCollectEntity.CreateTime = DateTime.Now;
                    courseBuyCollectEntity.ExpirationDate = DateTime.Now.AddYears(1);
                    courseBuyCollectEntity.IsHide = 0;
                    _courseBuyCollectService.Update(courseBuyCollectEntity);
                    Logger.Info($"BuyCollect:" + orderId + "修改支付成功记录");
                }
                //var orderInfo = _orderInfoRepository.Get(orderId);
                Logger.Info($"OpenCourse:" + orderId + "支付成功");
                return 200;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("开通会员异常", ex);
                return 10005500;
            }
        }
        public async Task<Dictionary<int, List<CourseThinModel>>> GetCcRecommCourse()
        {
            var rlt = await _cache.Get("_ccrecommend", async () =>
            {
                var setting = Redis.HashGetDictionaryWithType<List<int>>(RedisKeyHelper.HASH_CC_RECOMMEND_COURSES);
                if (setting == null) return null;
                var courses = await GetWithChildModelsAsync();
                courses.ForEach(d => d.Img = $"{d.Img}{2}.png");
                var cates = setting.Keys.Select(v => Convert.ToInt32(v)).OrderByDescending(v => v);
                var dict = new Dictionary<int, List<CourseThinModel>>();
                foreach (var c in cates)
                {
                    var cids = setting[c.ToString()];
                    if (cids != null)
                    {
                        var cs = new List<CourseThinModel>();
                        foreach (var cid in cids)
                        {
                            var course = courses.FirstOrDefault(m => m.Id == cid);
                            if (course != null)
                            {

                                cs.Add(course);
                            }
                        }
                        dict.Add(c, cs);
                    }
                }
                return dict;
            }, null, TimeSpan.FromSeconds(60));

            return rlt;
        }

        public void wirtelog(string name, string type)
        {
            OperationInfo info = new OperationInfo();
            info.UserName = name;
            info.BusinessType = "线下开课";
            info.CreateTime = DateTime.Now;
            type = type == "1" ? "单条开通" : "批量导入";
            info.dec = "用户" + info.UserName + ",在" + info.CreateTime + "操作了" + info.BusinessType + "模块" + "执行了：" + type;
            _courseLogService.AddLogOfOperation(info);
        }


        public async Task<Result<List<CategoryedCourseThinModel>>> HomeGet(long userId)
        {
            try
            {
                var setting = await _settingService.GetHomeCourseSetting();
                var recData = await GetRecommendList(setting, userId);
                var hotData = await GetHotList(setting, userId);
                var catedData = await GetCategoriedTop(userId, setting);
                var setlive = await SetCourseChildType();//处理直播状态

                var rlt = new List<CategoryedCourseThinModel>();
                rlt.Add(new CategoryedCourseThinModel { Caption = "rec", Id = -1, Mode = 2, Values = recData });
                rlt.Add(new CategoryedCourseThinModel { Caption = "推荐课程", Id = -2, Mode = 1, Values = hotData });
                rlt.AddRange(catedData);

                return new Result<List<CategoryedCourseThinModel>> { Data = rlt };
            }
            catch (Exception ex)
            {
                Logger.Error("/api/home", ex);
                return new Result<List<CategoryedCourseThinModel>>(ResultCode.Exception, ex.Message);
            }

        }
    }
}
