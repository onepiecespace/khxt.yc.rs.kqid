﻿using Castle.Components.DictionaryAdapter;
using Castle.Core.Internal;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.Course;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Repositories.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using Hx;
using Hx.Extensions;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CollectionExtensions = Castle.Core.Internal.CollectionExtensions;

namespace KhXt.YcRs.Services.Course
{
    //WHY:Create 改Add,返回值为主键类型，update,del返回值改为bool类型
    public class CourseUserItemService : ServiceBase, ICourseUserItemService
    {
        private readonly ICourseUserItemRepository _repo;

        private readonly ICourseItemService _courseItemService;
        private readonly int _coinValue = 10;
        private readonly int _expValue = 10;
        private readonly IDirectMemoryCache _cache;
        private readonly string _userItemsCacheKey = "userItems";
        public CourseUserItemService(ICourseUserItemRepository repo,
            ICourseItemService courseItemService,
            IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _courseItemService = courseItemService;
            _cache = cacheManager.GetCache("__userItemsCache");
        }

        #region
        public IQueryable<CourseUserItemEntity> Table()
        {
            try
            {
                var userItems = _cache.Get(_userItemsCacheKey, () =>
                {
                    var AllList = UnitOfWorkService.Execute(() =>
                    {
                        return _repo.GetList().AsQueryable();
                    });
                    var list = AllList.ToList();
                    return list.AsQueryable();
                }, TimeSpan.FromHours(24));
                return userItems;
            }
            catch (Exception ex)
            {
                Logger.Error("课程用户作业列表缓存异常", ex);
                return null;
            }

        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool CourseUserItemClearCache()
        {
            RebuildCache();
            return true;
        }
        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is CourseUserItemEventData data)) return;
            var method = (EventDataMethod)data.Method;
            if (method == EventDataMethod.Clear)
            {
                _cache.Clear();
                Table();
            }
        }
        public void RebuildCache()
        {
            ClearCache();
            //Trigger(new CourseUserItemEventData() { Method = (int)EventDataMethod.Clear });
        }
        public override void ClearCache()
        {
            _cache.Clear();
            Table();
        }



        public void Delete(CourseUserItemEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() => _repo.Delete(entity));
            if (body) RebuildCache();
        }

        public void Delete(Expression<Func<CourseUserItemEntity, bool>> predicate)
        {

            var body = UnitOfWorkService.Execute<bool>(() => _repo.Delete(predicate));
            if (body) RebuildCache();
        }





        public CourseUserItemEntity Get(object id)
        {
            //var courseuseritem = Table();
            //return courseuseritem.FirstOrDefault(g => (g.Id == Convert.ToInt64(id)));
            var body = UnitOfWorkService.Execute<CourseUserItemEntity>(() =>
           {
               long.TryParse(id.ToString(), out long nid);
               return _repo.Get(nid);

           });
            return body;

        }


        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList<CourseUserItemEntity> Gets(Expression<Func<CourseUserItemEntity, bool>> predicate, Action<Orderable<CourseUserItemEntity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList<CourseUserItemEntity>>(() =>
           {
               if (order == null)
               {
                   return new PageList<CourseUserItemEntity>(Table().Where(predicate), pageIndex, pageSize);
               }
               var deferrable = new Orderable<CourseUserItemEntity>(Table().Where(predicate));
               order(deferrable);
               var ts = deferrable.Queryable;
               var totalCount = ts.Count();
               var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

               return new PageList<CourseUserItemEntity>(pagingTs, pageIndex, pageSize, totalCount);

           });
            return body;
        }



        public void Update(CourseUserItemEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() => _repo.Update(entity));
            if (body) RebuildCache();
        }
        #endregion

        public async Task<Result<CourseItemDoneListInfo>> GetUserCourseWorkList(long userId, long courseId, long courseChildId, int osSign)
        {
            var result = new Result<CourseItemDoneListInfo>();
            var courseItemDone = new CourseItemDoneListInfo();
            var totalCount = 0;
            var rightCount = 0;
            //获取课程下面所有题目
            var listCourseItem = _courseItemService.Table()
                .Where(t => t.CourseId == courseId && t.CourseChildId == courseChildId && t.IsDelete == 0).ToList();
            Expression<Func<CourseUserItemEntity, bool>> exp = t =>
                t.UserId == (int)userId && t.CourseId == (int)courseId && t.CourseChildId == (int)courseChildId;
            if (osSign != -2)
            {
                exp.And(t => t.OpSign == osSign);
            }
            //获取课程下本用户作业列表
            var listUserCourseItem = Table().Where(exp).ToList();
            var list = new List<CourseUserWorkModel>();
            foreach (var course in listCourseItem)
            {
                CourseUserWorkModel entity;

                if (listUserCourseItem.Exists(t => t.CourseItemId == course.Id))
                {
                    var temp = listUserCourseItem.Find(t => t.CourseItemId == course.Id);
                    entity = course.MapTo<CourseUserWorkModel>();
                    entity.ItemId = temp.CourseItemId;
                    entity.Creator = temp.Creator;
                    entity.CreateTime = temp.CreateTime;
                    entity.Updater = temp.Updater;
                    entity.UpdateTime = temp.UpdateTime;
                    entity.SelectAnswer = temp.UpdateAnswer.IsNotNullOrEmpty() ? temp.SelectAnswer : temp.UpdateAnswer;
                    entity.CourseUserItemId = temp.Id;
                }
                else
                {
                    entity = course.MapTo<CourseUserWorkModel>();
                }
                totalCount = totalCount + 1;
                list.Add(entity);
            }

            foreach (var item in list)
            {
                if (item.SelectAnswer.IsNotNullOrEmpty() && item.SelectAnswer.Equals(item.Answer))
                {
                    rightCount = rightCount + 1;
                }

                if (item.ItemOptions == null || item.ItemOptions.Count <= 0) continue;
                var listErrKeyValues = new List<ErrKeyValue>();
                foreach (var dic in item.ItemOptions)
                {
                    var errKeyValue = new ErrKeyValue
                    {
                        KeyData = dic.Key,
                        ValueData = dic.Value,
                        IsTrue = item.Answer.Equals(dic.Key),
                        MyLastSelect = item.SelectAnswer != null && (item.SelectAnswer.IsNotNullOrEmpty() && item.SelectAnswer.Equals(dic.Key)),
                        SelectWrong = item.SelectAnswer != null && (item.SelectAnswer.IsNotNullOrEmpty() && item.SelectAnswer.Equals(dic.Key))
                    };
                    listErrKeyValues.Add(errKeyValue);
                }
                item.Options = listErrKeyValues;
            }
            courseItemDone.CourseId = courseId;
            courseItemDone.CourseName = "";
            courseItemDone.CourseChildId = courseChildId;
            courseItemDone.TotalDone = totalCount;
            courseItemDone.BookList = list;
            courseItemDone.AlreadyDone = rightCount;
            result.Data = courseItemDone;
            return await Task.Run(() => result);
        }

        public async Task<Result<CourseUserWorkResult>> SaveCourseUserItem(int userId, int courseChildId, List<CourseUserItemAddModel> courseUserItemAddModelList)
        {
            try
            {
                var courseId = courseUserItemAddModelList[0].CourseId;
                var rightItem = 0;
                var errorCount = 0;
                var items = new List<long>();
                var itemUpdate = new List<string>();
                courseUserItemAddModelList.ForEach(t =>
                {
                    items.Add(long.Parse(t.ItemId));
                    itemUpdate.Add(t.ItemId);
                });
                //获取试题信息
                var itemList = _courseItemService.Table().Where(t => items.Contains(t.Id)).ToList();
                var courseUserItems = Table().Where(t => items.Contains(t.CourseItemId) && t.UserId == userId).ToList();
                //1.保存数据至用户答题记录 
                //2.保存错误题目到错题本 ToDo:重复题目不加入错误本
                //3.增加经验 暂时（经验计算规则 答对题目*金币 答对题目*经验）
                var courseUserItemList = courseUserItemAddModelList.MapTo<List<CourseUserItemEntity>>();

                var body = UnitOfWorkService.Execute<long>(() =>
               {
                   long flagCount = 0;
                   foreach (var itemEntity in courseUserItemList)
                   {
                       var selectAnswer = itemEntity.UpdateAnswer.IsNullOrEmpty()
                           ? itemEntity.SelectAnswer
                           : itemEntity.UpdateAnswer;

                       if (itemEntity.Id == 0)
                       {
                           rightItem = 0;
                           errorCount += 0;
                           flagCount += _repo.InsertAndGetId(itemEntity);

                       }
                       else
                       {
                           rightItem = 0;
                           errorCount += 0;
                           flagCount += _repo.Update(itemEntity) ? 1 : 0;
                       }
                   }

                   return flagCount;
               });
                var result = new CourseUserWorkResult
                {
                    CoinValue = _coinValue * rightItem,
                    ExpValue = _expValue * rightItem,
                    CourseChildId = courseChildId,
                    CourseId = courseId,
                    TotalCount = courseUserItemAddModelList.Count
                };
                result.FailCount = errorCount;
                result.UserId = userId;
                result.CourseItem = DealCourseItemDoneListInfo(courseUserItemList, itemList);
                RebuildCache();
                return await Task.FromResult(new Result<CourseUserWorkResult>() { Data = result });

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error("我的作业提交失败！", exception);
                return new Result<CourseUserWorkResult>() { Code = (int)ResultCode.Exception, Message = exception.Message };
            }
        }


        private List<CourseUserWorkModel> DealCourseItemDoneListInfo(List<CourseUserItemEntity> courseUserItems,
            List<CourseItemEntity> courseItems)
        {
            var list = new List<CourseUserWorkModel>();
            list = courseItems.MapTo<List<CourseUserWorkModel>>();
            foreach (var entity in list)
            {
                var itemEntity = courseUserItems.Find(t => entity.Id.Equals((long)t.CourseItemId));
                entity.OpSign = itemEntity.OpSign;
                entity.ErrorNum = itemEntity.ErrorNum;
                entity.SelectAnswer = itemEntity.UpdateAnswer.IsNullOrEmpty() ? itemEntity.SelectAnswer : itemEntity.UpdateAnswer;
                entity.CreateTime = itemEntity.CreateTime;
                entity.Creator = itemEntity.Creator;
                entity.Updater = itemEntity.Updater;
                entity.UseTime = itemEntity.UseTime.ToString();
                entity.UpdateTime = itemEntity.UpdateTime;
                var listErrKeyValues = entity.ItemOptions.Select(dic => new ErrKeyValue
                {
                    KeyData = dic.Key,
                    ValueData = dic.Value,
                    IsTrue = entity.Answer.Equals(dic.Key),
                    MyLastSelect = entity.SelectAnswer != null && (!entity.SelectAnswer.IsNullOrEmpty() && entity.SelectAnswer.Equals(dic.Key)),
                    SelectWrong = entity.SelectAnswer != null && (!entity.SelectAnswer.IsNullOrEmpty() && entity.SelectAnswer.Equals(dic.Key))
                })
                    .ToList();

                entity.Options = listErrKeyValues;
                entity.CourseUserItemId = itemEntity.Id;
            }
            return list;
        }
    }

}
