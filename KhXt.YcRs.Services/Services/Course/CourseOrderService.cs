﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Repositories.Course;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Course
{
    //WHY:Create 改Add,返回值为主键类型，update,del返回值改为bool类型
    public class CourseOrderService : ServiceBase, ICourseOrderService
    {
        ICourseOrderRepository _repo;
        ICourseRepository _courseRepo;
        public CourseOrderService(ICourseOrderRepository repo, ICourseRepository courseRepo)
        {
            _repo = repo;
            _courseRepo = courseRepo;
        }

        #region 生成方法
        public IQueryable<CourseOrderEntity> Table()
        {
            var body = UnitOfWorkService.Execute(() =>
                {
                    return _repo.GetList().AsQueryable();
                });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(CourseOrderEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }


        /// <summary>
        ///
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public List<MyOrderInfo> Gets(Expression<Func<CourseOrderEntity, bool>> predicate, int pageSize, int pageIndex)
        {

            var total = 0;

            var imgSvr = GetConfig("imgserver");
            var body = UnitOfWorkService.Execute(() =>
            {
                var orders = _repo.GetPaged(predicate, pageIndex - 1, pageSize, out total, false, e => e.Id).MapTo<List<MyOrderInfo>>();
                var courses = _courseRepo.GetList();
                orders.ForEach(order =>
                {
                    var c = courses.FirstOrDefault(p => p.Id == Convert.ToInt64(order.CourseId));
                    if (c != null)
                    {
                        order.Title = c.CourseName;
                        order.Summary = c.Detailed;
                        order.Price = order.PayPrice;
                        order.PayPrice = order.PayPrice;
                        if (!string.IsNullOrEmpty(c.CoursePicPhone))
                        {
                            order.OrderImg = $"{imgSvr}{c.CoursePicPhone}";
                        }
                    }

                });
                return orders;
            });

            return body;
        }

        public void Update(CourseOrderEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }
        #endregion
        #region  CMS方法
        public IQueryable<OrderListOfCMS> GetOrderList(int pageSize = 10, int pageIndex = 1)
        {

            try
            {
                var body = UnitOfWorkService.Execute(() =>
                {
                    return _courseRepo.GetOrderList().AsQueryable();
                });

                return body;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error("GetHistory异常", exception);
                throw;
            }
        }
        #endregion


    }
}
