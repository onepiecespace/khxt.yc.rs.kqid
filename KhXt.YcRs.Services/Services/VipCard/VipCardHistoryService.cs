﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.VipCard;
using KhXt.YcRs.Domain.Repositories.VipCard;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.VipCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.VipCard
{
    public class VipCardHistoryService : ServiceBase, IVipCardHistoryService
    {
        IVipCardHistoryRepository _repo;

        public VipCardHistoryService(IVipCardHistoryRepository repo)
        {
            _repo = repo;
        }

        #region IComBaseService
        public IQueryable<VipCardHistoryEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<VipCardHistoryEntity>>(() =>
            {
                return _repo.GetList().AsQueryable();
            });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(VipCardHistoryEntity entity)
        {
            var body = UnitOfWorkService.Execute<string>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }



        public void Delete(VipCardHistoryEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(entity);
            });
        }

        public void Delete(Expression<Func<VipCardHistoryEntity, bool>> predicate)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(predicate);
            });
        }



        public IQueryable<VipCardHistoryEntity> Fetch(Expression<Func<VipCardHistoryEntity, bool>> predicate, Action<Orderable<VipCardHistoryEntity>> order)
        {
            var body = UnitOfWorkService.Execute<IQueryable<VipCardHistoryEntity>>(() =>
            {
                if (order == null)
                {
                    return Table().Where(predicate);
                }
                var deferrable = new Orderable<VipCardHistoryEntity>(Table().Where(predicate));
                order(deferrable);
                return deferrable.Queryable;

            });
            return body;

        }

        public VipCardHistoryEntity Get(object id)
        {
            var body = UnitOfWorkService.Execute<VipCardHistoryEntity>(() =>
            {
                return _repo.Get((string)id);

            });
            return body;

        }


        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList<VipCardHistoryEntity> Gets(Expression<Func<VipCardHistoryEntity, bool>> predicate, Action<Orderable<VipCardHistoryEntity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList<VipCardHistoryEntity>>(() =>
            {
                if (order == null)
                {
                    return new PageList<VipCardHistoryEntity>(Table().Where(predicate), pageIndex, pageSize);
                }
                var deferrable = new Orderable<VipCardHistoryEntity>(Table().Where(predicate));
                order(deferrable);
                var ts = deferrable.Queryable;
                var totalCount = ts.Count();
                var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

                return new PageList<VipCardHistoryEntity>(pagingTs, pageIndex, pageSize, totalCount);

            });
            return body;
        }

        public void Update(VipCardHistoryEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }
        #endregion

    }
}
