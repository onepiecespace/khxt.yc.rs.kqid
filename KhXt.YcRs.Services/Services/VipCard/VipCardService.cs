﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Vip;
using KhXt.YcRs.Domain.Entities.VipCard;
using KhXt.YcRs.Domain.Models.Vip;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Repositories.Vip;
using KhXt.YcRs.Domain.Repositories.VipCard;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Services.Vip;
using KhXt.YcRs.Domain.Services.VipCard;
using Hx.Extensions;
using Hx.ObjectMapping;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.VipCard
{
    public class VipCardService : ServiceBase, IVipCardService
    {
        private readonly IVipCardRepository _repo;
        private readonly IVipCardHistoryRepository _hisRepo;
        private readonly IUserVipService _userVipService;
        private readonly IUserVipHistoryRepository _userVipHistoryRepository;
        private readonly IUserService _userService;
        public VipCardService(IUserService userService
            , IVipCardRepository repo
            , IVipCardHistoryRepository repoHis,
            IUserVipService userVipService,
            IUserVipHistoryRepository userVipHistoryRepository)
        {
            _repo = repo;
            _userService = userService;
            _hisRepo = repoHis;
            _userVipService = userVipService;
            _userVipHistoryRepository = userVipHistoryRepository;
        }

        //查询是否有卡
        //查询卡是否被使用
        //更新用户信息、记录开卡历史
        public async Task<Result> Register(string phone, string cardNo, string passWord)
        {
            var rlt = UnitOfWorkService.Execute(() =>
            {
                //var cardInfo = _repo.FirstOrDefault(u => u.Id.Equals(cardNo) && u.password.Equals(passWord));
                //if (cardInfo == null) return new Result(ResultCode.Fail, "卡号或密码错误");
                var cardInfo = _repo.FirstOrDefault(u => u.Id.Equals(cardNo));
                if (cardInfo == null) return new Result(ResultCode.Fail, "卡号错误");
                if (!cardInfo.Password.Equals(passWord))
                {
                    return new Result(ResultCode.Fail, "激活码错误");
                }
                var cardHis = _hisRepo.FirstOrDefault(u => u.Id.Equals(cardNo));
                if (cardHis != null)
                {
                    return new Result(ResultCode.Fail, "卡已经被使用");
                }
                var m = cardInfo.Category == 1 ? 1 : (cardInfo.Category == 2 ? 6 : 12);
                //var returnCategory = m == 6 ? 2 : (m == 1 ? 1 : 4);
                var user = _userService.GetAll().GetAwaiter().GetResult().FirstOrDefault(u => u.Phone == phone);
                //var user = _userRepo.Get(userId);
                if (user == null) return new Result(ResultCode.Fail, "无此用户");
                var userVips = _userVipService.GetAll().Where(t => t.UserId.Equals(user.Userid)).ToList();
                UserVipModel userVip = null;
                if (userVips.Count > 0)
                    userVip = userVips[0];
                if (userVip != null && userVip.IsAutoRenew)
                    return new Result(ResultCode.Fail, "自动续费用户，暂时无法使用兑换卡！");
                var before = user.MemberDueDate;
                DateTime ct = DateTime.Now;
                var after = DateTime.Now.Date;

                if (user.MemberDueDate.HasValue && user.MemberDueDate >= DateTime.Now)
                {
                    after = user.MemberDueDate.Value.AddMonths(m);
                }
                else
                {
                    after = after.AddMonths(m);
                }

                #region  相差天数
                DateTime afterTime = Convert.ToDateTime(after);
                DateTime nowTime = Convert.ToDateTime(DateTime.Now);
                TimeSpan ts = afterTime - nowTime;
                int sub = ts.Days; //sub就是两天相差的天数
                #endregion
                //value存具体天数，还是类型（124），需确认，此处改成了具体天数
                var his = new VipCardHistoryEntity
                {
                    Id = cardInfo.Id,
                    UserId = user.Userid,
                    Before = before,
                    Value = sub,//m,
                    After = after,
                    CreateTime = ct
                };
                //兑换历史表中添加数据
                _hisRepo.InsertAndGetId(his);
                //更新 user_vip 
                if (userVip == null)
                {
                    userVip = new UserVipModel
                    {
                        OrderId = "",
                        UserId = user.Userid,
                        VipType = cardInfo.Category,
                        IsFrom = (int)VipFrom.IsVipCard,
                        IsAutoRenew = false,
                        BeginTime = DateTime.Now,
                        ExpiredTime = after,
                        ExchangeId = cardNo,
                        CreateTime = DateTime.Now,
                        UpdateTime = DateTime.Now
                    };
                    _userVipService.Add(userVip);
                }
                else
                {
                    userVip.OrderId = "";
                    if (cardInfo.Category > userVip.VipType)
                        userVip.VipType = cardInfo.Category;
                    userVip.IsFrom = (int)VipFrom.IsVipCard;
                    userVip.ExchangeId = cardNo;
                    userVip.ExpiredTime = after;
                    userVip.UpdateTime = DateTime.Now;
                    _userVipService.Update(userVip);
                }
                //插入 user_vip_history
                var userVipHis = userVip.MapTo<UserVipHistoryEntity>();
                userVipHis.IsSuccess = true;
                _userVipHistoryRepository.InsertAndGetId(userVipHis);
                //更新用户 会员信息 
                user.IsMember = cardInfo.Category;
                user.MemberDueDate = after;
                _userService.Update(user).GetAwaiter().GetResult();
                return new Result { Data = after.ToString10() + "|" + cardInfo.Category, Message = "会员卡激活成功！" };
            });
            return await Task.FromResult(rlt);
        }

        public VipCardEntity Get(object id)
        {
            var body = UnitOfWorkService.Execute<VipCardEntity>(() =>
            {
                return _repo.Get((string)id);

            });
            return body;

        }

        public object Create(VipCardEntity entity)
        {
            var body = UnitOfWorkService.Execute<string>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }

    }
}
