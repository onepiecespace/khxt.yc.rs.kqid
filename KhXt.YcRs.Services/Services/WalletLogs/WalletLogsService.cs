﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.WalletLogs;
using KhXt.YcRs.Domain.Repositories.WalletLogs;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.WalletLogs;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Services.WalletLogs
{
    public class WalletLogsService : ServiceBase, IWalletLogsService
    {
        IWalletLogsRepository _repo;

        public WalletLogsService(IWalletLogsRepository repo)
        {
            _repo = repo;
        }

        #region IComBaseService
        public IQueryable<WalletLogsEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<WalletLogsEntity>>(() =>
               {
                   return _repo.GetList().AsQueryable();
               });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(WalletLogsEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }



        public void Delete(WalletLogsEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(entity);
            });
        }

        public void Delete(Expression<Func<WalletLogsEntity, bool>> predicate)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(predicate);
            });
        }



        public IQueryable<WalletLogsEntity> Fetch(Expression<Func<WalletLogsEntity, bool>> predicate, Action<Orderable<WalletLogsEntity>> order)
        {
            var body = UnitOfWorkService.Execute<IQueryable<WalletLogsEntity>>(() =>
           {
               if (order == null)
               {
                   return Table().Where(predicate);
               }
               var deferrable = new Orderable<WalletLogsEntity>(Table().Where(predicate));
               order(deferrable);
               return deferrable.Queryable;

           });
            return body;

        }

        public WalletLogsEntity Get(object id)
        {
            var body = UnitOfWorkService.Execute<WalletLogsEntity>(() =>
           {
               long.TryParse(id.ToString(), out long nid);
               return _repo.Get(nid);

           });
            return body;

        }


        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList<WalletLogsEntity> Gets(Expression<Func<WalletLogsEntity, bool>> predicate, Action<Orderable<WalletLogsEntity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList<WalletLogsEntity>>(() =>
           {
               if (order == null)
               {
                   return new PageList<WalletLogsEntity>(Table().Where(predicate), pageIndex, pageSize);
               }
               var deferrable = new Orderable<WalletLogsEntity>(Table().Where(predicate));
               order(deferrable);
               var ts = deferrable.Queryable;
               var totalCount = ts.Count();
               var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

               return new PageList<WalletLogsEntity>(pagingTs, pageIndex, pageSize, totalCount);

           });
            return body;
        }

        public void Update(WalletLogsEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }
        #endregion
    }









}
