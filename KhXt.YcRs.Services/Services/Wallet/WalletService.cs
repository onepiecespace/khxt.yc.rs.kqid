﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.AppInfo;
using KhXt.YcRs.Domain.Entities.Wallet;
using KhXt.YcRs.Domain.Entities.WalletLogs;
using KhXt.YcRs.Domain.Models.Pay;
using KhXt.YcRs.Domain.Models.Wallet;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Repositories.Wallet;
using KhXt.YcRs.Domain.Repositories.WalletLogs;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.Wallet;
using KhXt.YcRs.Domain.Util;
using Hx.Extensions;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Wallet
{
    public class WalletService : ServiceBase, IWalletService
    {
        IWalletRepository _repo;
        private readonly IOrderInfoRepository _orderInfoRepository;
        IWalletLogsRepository _walletLogsRepository;
        IOrderBusinessService _orderBusinessService;
        private readonly IOrderInfoService _orderInfoService;
        public WalletService(IWalletRepository repo,
            IOrderInfoService orderInfoService,
            IOrderBusinessService orderBusinessService,
            IOrderInfoRepository orderInfoRepository,
            IWalletLogsRepository walletLogsRepository)
        {
            _repo = repo;
            _walletLogsRepository = walletLogsRepository;
            _orderInfoRepository = orderInfoRepository;
            _orderInfoService = orderInfoService;
            _orderBusinessService = orderBusinessService;
        }

        #region IComBaseService
        public IQueryable<WalletEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<WalletEntity>>(() =>
               {
                   return _repo.GetList().AsQueryable();
               });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(WalletEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }



        public void Delete(WalletEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(entity);
            });
        }

        public void Delete(Expression<Func<WalletEntity, bool>> predicate)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(predicate);
            });
        }



        public IQueryable<WalletEntity> Fetch(Expression<Func<WalletEntity, bool>> predicate, Action<Orderable<WalletEntity>> order)
        {
            var body = UnitOfWorkService.Execute<IQueryable<WalletEntity>>(() =>
           {
               if (order == null)
               {
                   return Table().Where(predicate);
               }
               var deferrable = new Orderable<WalletEntity>(Table().Where(predicate));
               order(deferrable);
               return deferrable.Queryable;

           });
            return body;

        }

        public WalletEntity Get(object id)
        {
            var body = UnitOfWorkService.Execute<WalletEntity>(() =>
           {
               long.TryParse(id.ToString(), out long nid);
               return _repo.Get(nid);

           });
            return body;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WalletEntity Get(long userId)
        {
            var body = UnitOfWorkService.Execute<WalletEntity>(() =>
            {
                return _repo.GetList().FirstOrDefault(t => t.UserId == userId);

            });
            return body;

        }


        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList<WalletEntity> Gets(Expression<Func<WalletEntity, bool>> predicate, Action<Orderable<WalletEntity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList<WalletEntity>>(() =>
           {
               if (order == null)
               {
                   return new PageList<WalletEntity>(Table().Where(predicate), pageIndex, pageSize);
               }
               var deferrable = new Orderable<WalletEntity>(Table().Where(predicate));
               order(deferrable);
               var ts = deferrable.Queryable;
               var totalCount = ts.Count();
               var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

               return new PageList<WalletEntity>(pagingTs, pageIndex, pageSize, totalCount);

           });
            return body;
        }

        public void Update(WalletEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }


        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chargeModel"></param>
        /// <returns></returns>
        public Result<WalletChargeResult> WalletCharge(WalletChargeModel chargeModel)
        {
            try
            {
                var rlt = UnitOfWorkService.Execute<Result<WalletChargeResult>>(() =>
                {
                    var dt = DateTime.Now;

                    var walletInfo = _repo.GetList().FirstOrDefault(t => t.UserId == chargeModel.UserId);
                    if (walletInfo == null)
                    {
                        walletInfo = new WalletEntity
                        {
                            Coin = chargeModel.ChargeCoin,
                            UpdateTime = dt,
                            CreateTime = dt,
                            UserId = chargeModel.UserId
                        };
                        _repo.InsertAndGetId(walletInfo);

                    }
                    else
                    {
                        walletInfo.Coin = walletInfo.Coin + chargeModel.ChargeCoin;
                        _repo.Update(walletInfo);
                    }
                    var walletLog = new WalletLogsEntity
                    {
                        Type = 0,
                        Coin = chargeModel.ChargeCoin,
                        CoinBalance = walletInfo.Coin,
                        OrderId = chargeModel.OrderId,
                        CreateTime = dt,
                        Description = chargeModel.Description,
                        UserId = chargeModel.UserId
                    };
                    var logId = _walletLogsRepository.InsertAndGetId(walletLog);

                    return new Result<WalletChargeResult>
                    {
                        Data = new WalletChargeResult
                        {
                            Coin = walletInfo.Coin,
                            ChargeCoin = chargeModel.ChargeCoin,
                            UserId = chargeModel.UserId
                        }
                    };
                });
                return rlt;
            }
            catch (Exception ex)
            {
                Logger.Error($"用户：{chargeModel.UserId}，充值异常", ex);
                return new Result<WalletChargeResult> { Code = (int)ResultCode.Exception, Message = "充值异常" };
            }

        }

        public Result<WalletResult> GetWallet(long userId)
        {
            try
            {
                var info = Get(userId);
                if (info == null)
                    return new Result<WalletResult> { Data = new WalletResult { Coin = "0.00", UpdateTime = DateTime.Now, UserId = userId } };
                else
                    return new Result<WalletResult> { Data = new WalletResult { Coin = info.Coin.ToString("f2"), UpdateTime = info.UpdateTime, UserId = userId } };

            }
            catch (Exception ex)
            {
                Logger.Error($"用户{userId},获取钱包余额异常", ex);
                return new Result<WalletResult> { Code = (int)ResultCode.Exception, Message = "获取钱包余额异常" };
            }
        }

        public Result<WalletLogsResult> GetWallets(long userId, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var list = UnitOfWorkService.Execute<WalletLogsResult>(() =>
                {
                    var query = _walletLogsRepository.GetList().Where(t => t.UserId == userId).OrderByDescending(t => t.CreateTime);
                    var total = query.Count();
                    var tmpList = query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    var rows = new List<Domain.Models.Wallet.WalletLogs>();
                    if (tmpList != null && tmpList.Count > 0)
                        rows = tmpList.MapTo<List<Domain.Models.Wallet.WalletLogs>>();
                    var logs = new WalletLogsResult
                    {
                        PageIndex = pageIndex,
                        PageSize = pageSize,
                        Total = total,
                        Rows = rows
                    };
                    return logs;
                });
                return new Result<WalletLogsResult> { Data = list };
            }
            catch (Exception ex)
            {
                Logger.Error($"用户{userId},获取钱包记录异常", ex);
                return new Result<WalletLogsResult> { Code = (int)ResultCode.Exception, Message = "获取钱包记录异常" };
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public int WalletCharge(string orderId, long userId)
        {
            try
            {
                var orderInfo = _orderInfoService.Get(orderId);
                if (orderInfo == null)
                    return (int)ResultCode.WalletChargeOrderNot;
                var orderItemInfo = _orderInfoService.GetItem(orderId);
                var rlt = UnitOfWorkService.Execute<int>(() =>
                {
                    var chargeModel = new WalletChargeModel();
                    chargeModel.ChargeCoin = float.Parse(orderInfo.Payment);
                    chargeModel.UserId = userId;
                    chargeModel.Description = orderItemInfo == null ? "" : orderItemInfo.Title;

                    var dt = DateTime.Now;

                    var walletInfo = _repo.GetList().FirstOrDefault(t => t.UserId == chargeModel.UserId);
                    if (walletInfo == null)
                    {
                        walletInfo = new WalletEntity
                        {
                            Coin = chargeModel.ChargeCoin,
                            UpdateTime = dt,
                            CreateTime = dt,
                            UserId = chargeModel.UserId
                        };
                        _repo.InsertAndGetId(walletInfo);

                    }
                    else
                    {
                        walletInfo.Coin = walletInfo.Coin + chargeModel.ChargeCoin;
                        _repo.Update(walletInfo);
                    }

                    var walletLog = new WalletLogsEntity
                    {
                        Type = 0,
                        Coin = chargeModel.ChargeCoin,
                        CoinBalance = walletInfo.Coin,
                        OrderId = orderId,
                        CreateTime = dt,
                        Description = chargeModel.Description,
                        UserId = chargeModel.UserId
                    };
                    var logId = _walletLogsRepository.InsertAndGetId(walletLog);

                    return 200;

                });
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("钱包充值异常", ex);
                return (int)ResultCode.WalletChargeException;
            }
        }

        public async Task<Result<WalletPayResult>> PayAsync(long userId, string orderId, int businessType, string code)
        {
            try
            {
                var appInfoService = new AppInfoService();
                var appId = appInfoService._appList[0].AppId;
                var appSercet = appInfoService._appList[0].AppSercet;
                //验证合法性
                var dtString = DateTime.UtcNow.ToString("yyyyMMddHH");
                var verifStr = $"{userId}{orderId}{businessType}{appId}{appSercet}{dtString}";
                var encryptStr = EncryptionUtility.Md5Encode(verifStr, 32, System.Text.Encoding.UTF8);
                if (!code.Equals(encryptStr, StringComparison.OrdinalIgnoreCase))
                    return new Result<WalletPayResult> { Code = (int)ResultCode.warning, Message = "钱包支付失败,非法操作" };
                var key = RedisKeyHelper.CreateOrderStatus();
                var orderInfo = _orderInfoService.Get(orderId);
                if (orderInfo == null)
                    return new Result<WalletPayResult> { Code = (int)ResultCode.warning, Message = "钱包支付失败,订单信息不存在" };
                var status = orderInfo.Status;
                if (status == (int)OrderStatus.Cancel)
                    return new Result<WalletPayResult>() { Code = (int)ResultCode.warning, Message = "订单已取消，请刷新" };
                if (status == (int)OrderStatus.Success)
                    return new Result<WalletPayResult>() { Code = (int)ResultCode.warning, Message = "订单已完成，请刷新" };
                if (status == (int)OrderStatus.HasPay)
                    return new Result<WalletPayResult>() { Code = (int)ResultCode.warning, Message = "订单已支付，请刷新" };

                var info = Get(userId);
                if (info == null)
                    return new Result<WalletPayResult> { Code = (int)ResultCode.WalletChargeNotEnough, Message = "钱包支付失败,获取余额失败" };
                if (info.Coin < float.Parse(orderInfo.Payment))
                    return new Result<WalletPayResult> { Code = (int)ResultCode.WalletChargeNotEnough, Message = "钱包支付失败,余额不足" };
                var orderItemInfo = _orderInfoService.GetItem(orderId);
                //处理订单状态
                var rlt = UnitOfWorkService.Execute(() =>
               {
                   var chargeModel = new WalletChargeModel();
                   chargeModel.ChargeCoin = float.Parse(orderInfo.Payment);
                   chargeModel.UserId = userId;
                   chargeModel.Description = orderItemInfo == null ? "" : orderItemInfo.Title;

                   var dt = DateTime.Now;
                   var walletInfo = _repo.GetList().FirstOrDefault(t => t.UserId == chargeModel.UserId);
                   if (walletInfo == null)
                   {
                       walletInfo = new WalletEntity
                       {
                           Coin = chargeModel.ChargeCoin,
                           UpdateTime = dt,
                           CreateTime = dt,
                           UserId = chargeModel.UserId
                       };
                       _repo.InsertAndGetId(walletInfo);

                   }
                   else
                   {
                       walletInfo.Coin = walletInfo.Coin - chargeModel.ChargeCoin;
                       _repo.Update(walletInfo);
                   }

                   var walletLog = new WalletLogsEntity
                   {
                       Type = 1,
                       Coin = chargeModel.ChargeCoin,
                       CoinBalance = walletInfo.Coin,
                       OrderId = orderId,
                       CreateTime = dt,
                       Description = chargeModel.Description,
                       UserId = chargeModel.UserId
                   };
                   var logId = _walletLogsRepository.InsertAndGetId(walletLog);
                   orderInfo.PaymentType = (int)PaymentType.Wallet;
                   orderInfo.Status = (int)OrderStatus.HasPay;
                   orderInfo.PaymentNo = walletLog.Id.ToString();
                   orderInfo.PaymentTime = DateTime.Now;
                   orderInfo.UpdateTime = DateTime.Now;
                   var orderRlt = _orderInfoRepository.Update(orderInfo);
                   return orderRlt;

               });
                if (!rlt)
                    return new Result<WalletPayResult> { Code = (int)ResultCode.warning, Message = "钱包支付失败" };

                var businessTypeEnum = (BusinessType)Domain.Util.EnumHelper.GetEnumFromStr<BusinessType>(businessType.ToString());
                var rltBuiness = await _orderBusinessService.DealWithOrder(userId, orderId, businessTypeEnum);

                if (rltBuiness.Code != (int)ResultCode.Success)
                    return new Result<WalletPayResult> { Code = (int)ResultCode.warning, Message = "钱包支付失败,处理业务数据失败" };

                return new Result<WalletPayResult> { Message = "钱包支付成功！" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("钱包支付异常", ex);
                return new Result<WalletPayResult> { Code = (int)ResultCode.Exception, Message = "钱包支付异常" };

            }
        }
    }









}
