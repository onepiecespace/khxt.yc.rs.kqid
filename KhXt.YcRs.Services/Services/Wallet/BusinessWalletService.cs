﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Entities.Wallet;
using KhXt.YcRs.Domain.Entities.WalletLogs;
using KhXt.YcRs.Domain.Models.Wallet;
using KhXt.YcRs.Domain.Repositories.Wallet;
using KhXt.YcRs.Domain.Repositories.WalletLogs;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.Wallet;
using Hx.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KhXt.YcRs.Services.Wallet
{
    public class BusinessWalletService : ServiceBase, IBusinessWalletService
    {
        IWalletRepository _repo;
        IWalletLogsRepository _walletLogsRepository;

        public BusinessWalletService(IWalletRepository repo,
            IWalletLogsRepository walletLogsRepository)
        {
            _repo = repo;
            _walletLogsRepository = walletLogsRepository;
        }
        public int WalletCharge(OrderInfoEntity orderInfo, OrderItemEntity orderItem)
        {
            try
            {
                if (orderInfo == null || orderItem == null)
                    return (int)ResultCode.WalletChargeOrderNot;
                var rlt = UnitOfWorkService.Execute<int>(() =>
                {
                    var chargeModel = new WalletChargeModel();
                    chargeModel.ChargeCoin = float.Parse(orderInfo.Payment);
                    chargeModel.UserId = orderInfo.UserId;
                    chargeModel.Description = orderItem == null ? "" : orderItem.Title;

                    var dt = DateTime.Now;

                    var walletInfo = _repo.GetList().FirstOrDefault(t => t.UserId == chargeModel.UserId);
                    if (walletInfo == null)
                    {
                        walletInfo = new WalletEntity
                        {
                            Coin = chargeModel.ChargeCoin,
                            UpdateTime = dt,
                            CreateTime = dt,
                            UserId = chargeModel.UserId
                        };
                        _repo.InsertAndGetId(walletInfo);

                    }
                    else
                    {
                        walletInfo.Coin = walletInfo.Coin + chargeModel.ChargeCoin;
                        _repo.Update(walletInfo);
                    }

                    var walletLog = new WalletLogsEntity
                    {
                        Type = 0,
                        Coin = chargeModel.ChargeCoin,
                        CoinBalance = walletInfo.Coin,
                        OrderId = orderInfo.Id,
                        CreateTime = dt,
                        Description = chargeModel.Description,
                        UserId = chargeModel.UserId
                    };
                    var logId = _walletLogsRepository.InsertAndGetId(walletLog);

                    return 200;

                });
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("钱包充值异常", ex);
                return (int)ResultCode.WalletChargeException;
            }
        }
    }
}
