﻿using KhXt.YcRs.Domain.Entities.Log;
using KhXt.YcRs.Domain.Models.Log;
using KhXt.YcRs.Domain.Repositories.Log;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Log;
using Hx.Extensions;
using Hx.MongoDb;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Log
{
    public class LogService : ServiceBase, ILogService
    {
        IMongoUnitOfWorkService _mongoUow;
        ILoginLogRepository _loginLogRepo;
        public LogService(
            IMongoUnitOfWorkService mongoUow,
            ILoginLogRepository loginLogRepo)
        {
            _mongoUow = mongoUow;
            _loginLogRepo = loginLogRepo;
        }

        public async Task UserLogin(LoginLogModel model)
        {
            if (model == null) return;
            var entity = model.MapTo<LoginLogEntity>();
            entity.LogingTime = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
            entity.Id = GuidGenerator.Create().ToString();
            if (entity == null)
            {
                Logger.Warn($"UserLogin:entity is null");
                return;
            }
            await _mongoUow.ExecuteNonQueryAsync(() =>
            {
                _loginLogRepo.Insert(entity);
            });
        }

        public async Task<List<LoginLogModel>> Table()
        {
            //t => t.LogingTime <= Convert.ToDateTime("2020-04-01 00:00:00.000")
            var rlt = await _mongoUow.Execute(async () => await _loginLogRepo.GetListAsync());
            return rlt.MapTo<List<LoginLogModel>>();

        }

        public List<LoginLogModel> GetNumAsync(string datime, string datetimesub, string datetimesec, string opt)
        {
            var rlt = Table().Result;
            var tltb = rlt.AsQueryable();
            if (opt == "today")
            {
                tltb = tltb.Where(p => p.LogingTime >= Convert.ToDateTime(datetimesub));
            }
            if (opt == "yestoday")
            {
                tltb = tltb.Where(p => p.LogingTime >= Convert.ToDateTime(datetimesub + " 00:00:00.000") && p.LogingTime <= Convert.ToDateTime(datetimesub + " 23:59:59.000"));
            }
            if (opt == "week" || opt == "lastwek")
            {
                tltb = tltb.Where(p => p.LogingTime >= Convert.ToDateTime(datime) && p.LogingTime <= Convert.ToDateTime(datetimesec));
            }

            return tltb.ToList();
        }
    }
}
