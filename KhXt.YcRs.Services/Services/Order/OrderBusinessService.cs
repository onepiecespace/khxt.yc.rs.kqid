﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.Vip;
using KhXt.YcRs.Domain.Services.Wallet;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Order
{
    public class OrderBusinessService : ServiceBase, IOrderBusinessService
    {
        private readonly IOrderInfoRepository _orderInfoRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IVipService _vipService;
        private readonly IUserCouponRepository _userCouponRepository;
        private readonly IOrderCouponRepository _orderCouponRepository;
        private readonly ICourseService _courseService;
        private readonly IBusinessWalletService _walletService;
        public OrderBusinessService(IVipService vipService,
            IOrderInfoRepository orderInfoRepository,
            IOrderItemRepository orderItemRepository,
            IUserCouponRepository userCouponRepository,
            ICourseService courseService,
            IOrderCouponRepository orderCouponRepository,
            IBusinessWalletService walletService)
        {
            _vipService = vipService;
            _courseService = courseService;
            _orderInfoRepository = orderInfoRepository;
            _orderItemRepository = orderItemRepository;
            _userCouponRepository = userCouponRepository;
            _orderCouponRepository = orderCouponRepository;
            _walletService = walletService;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderId"></param>
        /// <param name="businessType"></param>
        /// <param name="isAutoRenew"></param>
        /// <returns></returns>
        public async Task<Result> DealWithOrder(long userId, string orderId, BusinessType businessType, bool isAutoRenew = false)
        {
            try
            {
                Logger.Info($"订单业务处理：{(int)businessType}，订单id:{orderId}");
                //1.更新订单状态 
                //2.修改业务
                var rlt = await UnitOfWorkService.Execute(async () =>
                {
                    var orderInfo = await _orderInfoRepository.GetAsync(orderId);
                    if (orderInfo == null)
                        return new Result() { Code = (int)OrderPayStatusEnum.No, Message = "订单不存在" };
                    if (orderInfo.UserId != userId)
                        return new Result() { Code = (int)OrderPayStatusEnum.Illegal, Message = "经系统检测，此订单非您的订单" };
                    orderInfo.Status = (int)OrderStatus.Success;
                    orderInfo.EndTime = DateTime.Now;
                    orderInfo.UpdateTime = DateTime.Now;
                    var orderItem = await _orderItemRepository.FirstOrDefaultAsync(t => t.OrderId.Equals(orderId));
                    if (orderItem == null)
                        return new Result() { Code = (int)OrderPayStatusEnum.NoItem, Message = "订单商品信息不存在" };
                    var itemId = orderItem.ItemId;
                    var orderRlt = await _orderInfoRepository.UpdateAsync(orderInfo);
                    if (!orderRlt)
                        return new Result() { Code = (int)OrderPayStatusEnum.UpdateErr, Message = "更新订单失败" };
                    var businessRlt = 0;
                    businessType = (BusinessType)Domain.Util.EnumHelper.GetEnumFromStr<BusinessType>(orderInfo.BusinessType.ToString());
                    switch (businessType)
                    {
                        case BusinessType.Read:
                            break;
                        case BusinessType.Dictation:
                            break;
                        case BusinessType.Pk:
                            break;
                        case BusinessType.Game:
                            break;
                        case BusinessType.Course:
                            businessRlt = await _courseService.OpenCourse(orderId, userId, Convert.ToInt64(itemId));
                            break;
                        case BusinessType.Task:
                            break;
                        case BusinessType.Recite:
                            break;
                        case BusinessType.Punch:
                            break;
                        case BusinessType.Live:
                            break;
                        case BusinessType.Record:
                            break;
                        case BusinessType.Vip:
                            businessRlt = await _vipService.OpenVip(orderId, userId, Convert.ToInt64(itemId), isAutoRenew);
                            break;
                        case BusinessType.Idiomatic:
                            break;
                        case BusinessType.Marketing:
                            break;
                        case BusinessType.Product:
                            break;
                        case BusinessType.Wallet:
                            businessRlt = _walletService.WalletCharge(orderInfo, orderItem);
                            break;
                        case BusinessType.All:
                            break;
                        default:
                            break;
                    }
                    if (businessRlt.Equals(200))
                    {
                        var orderCoupon = _orderCouponRepository.GetList().FirstOrDefault(t => t.OrderId == orderId);
                        if (orderCoupon == null)
                            return new Result() { Message = "订单处理成功！", Data = orderInfo.Status.ToString() };

                        var userCoupon = _userCouponRepository.GetList()
                            .FirstOrDefault(t => t.Id == orderCoupon.CouponId);
                        if (userCoupon == null)
                            return new Result() { Message = "订单处理成功！", Data = orderInfo.Status.ToString() };
                        userCoupon.Status = (int)CouponStatus.HasUsed;
                        userCoupon.UpdateTime = DateTime.Now;
                        _userCouponRepository.Update(userCoupon);
                        Logger.Info($"业务处理成功，减少用户优惠券，用户{userId}，订单号{orderId},用户券Id:{userCoupon.Id}");
                        return new Result() { Message = "订单处理成功！", Data = orderInfo.Status.ToString() };
                    }
                    else
                        return new Result() { Code = businessRlt, Message = "保存业务数据失败！" };
                });
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("订单处理异常", ex);
                return new Result() { Code = (int)OrderPayStatusEnum.Exception, Message = "订单处理异常" };
            }
        }
    }
}
