﻿using Essensoft.AspNetCore.Payment.WeChatPay.Request;
using GreenPipes;
using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Contracts;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Course;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.Pay;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Pagination;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.Pay;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx;
using Hx.Extensions;
using Hx.MQ;
using Hx.ObjectMapping;
using Hx.Runtime.Caching.Memory;
using Senparc.NeuChar.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Order
{
    public class OrderInfoService : ServiceBase, IOrderInfoService
    {
        private readonly IOrderInfoRepository _repo;
        private readonly IOrderCouponRepository _orderCouponRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IOrderShippingRepository _orderShippingRepository;
        private readonly IOrderPayRepository _payRepository;
        private readonly IWeChatPayService _weChatPayService;
        private readonly IUserCacheService _userCacheService;
        private readonly IUserCouponRepository _userCouponRepository;
        private readonly ICouponInfoRepository _couponInfoRepository;
        private readonly IDirectMemoryCache _cache;
        private readonly ICourseBuyCollectService _courseBuyCollectService;
        private readonly ICourseService _courseService;
        private readonly IUserService _userService;
        private readonly IAddressService _addressService;
        private readonly string ordersCacheKey = "orders";
        public OrderInfoService(IOrderInfoRepository repo,
            IOrderCouponRepository orderCouponRepository,
            IOrderItemRepository orderItemRepository,
            IOrderShippingRepository orderShippingRepository,
            IOrderPayRepository payRepository,
            IWeChatPayService weChatPayService,
            IUserCacheService userCacheService,
            IUserCouponRepository userCouponRepository,
            ICouponInfoRepository couponInfoRepository,
            IDirectMemoryCacheManager cacheManager, ICourseBuyCollectService courseBuyCollectService, ICourseService courseService, IUserService userService,
            IAddressService addressService)
        {
            _repo = repo;
            _orderCouponRepository = orderCouponRepository;
            _orderItemRepository = orderItemRepository;
            _orderShippingRepository = orderShippingRepository;
            _payRepository = payRepository;
            _weChatPayService = weChatPayService;
            _userCacheService = userCacheService;
            _userCouponRepository = userCouponRepository;
            _couponInfoRepository = couponInfoRepository;
            _courseBuyCollectService = courseBuyCollectService;
            _courseService = courseService;
            _userService = userService;
            _addressService = addressService;
            _cache = cacheManager.GetCache("__orderCache");
        }

        public IQueryable<OrderInfoEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<OrderInfoEntity>>(() =>
               {
                   return _repo.GetList().AsQueryable();
               });
            return body;
        }

        public OrderInfoEntity Get(object id)
        {
            var body = UnitOfWorkService.Execute<OrderInfoEntity>(() =>
           {
               return _repo.Get(id.ToString());

           });
            return body;

        }
        public OrderItemEntity GetItem(string orderId)
        {
            var body = UnitOfWorkService.Execute<OrderItemEntity>(() =>
            {

                return _orderItemRepository.FirstOrDefault(t => t.OrderId == orderId);

            });
            return body;

        }
        public void Update(OrderInfoEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }

        public void UpdateItem(OrderItemEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _orderItemRepository.Update(entity);
            });

        }

        public long Add(OrderPayEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _payRepository.InsertAndGetId(entity);
            });
            return body;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="businessType"></param>
        /// <param name="orderInfo"></param>
        /// <param name="orderItem"></param>
        /// <param name="orderShipping"></param>
        /// <param name="orderCoupons"></param>
        /// <returns></returns>
        public async Task<Result<OrderStatusModel>> CreateOrder(OrderInfoEntity orderInfo,
            OrderItemEntity orderItem,
            OrderShippingEntity orderShipping,
            List<OrderCouponEntity> orderCoupons, bool isync = false)
        {
            try
            {
                OrderStatusModel orderStatusModel = null;
                var orderId = orderInfo.Id;
                //生成订单号
                if (string.IsNullOrEmpty(orderInfo.Id))
                {
                    orderId = OrderGenerator.GenerateOrderId();
                }

                var payment = float.Parse(orderInfo.Payment).ToString("f2");
                orderInfo.Payment = payment;
                orderInfo.Id = orderId;
                orderInfo.CreateTime = DateTime.Now;

                if (orderInfo.BusinessType == (int)BusinessType.Course)
                {
                    PlatFormType platFormType;
                    switch (orderInfo.IsFrom)
                    {
                        case 1:
                            platFormType = PlatFormType.ios;
                            break;
                        case 2:
                            platFormType = PlatFormType.andriod;
                            break;
                        case 4:
                            platFormType = PlatFormType.web;
                            break;
                        default:
                            platFormType = PlatFormType.andriod;
                            break;
                    }
                    var courseRlt = await _courseService.CreateCourseOrder(orderInfo.UserId, long.Parse(orderItem.ItemId), orderId, platFormType);
                    if (courseRlt.Code != (int)ResultCode.Success)
                        return new Result<OrderStatusModel>() { Code = (int)ResultCode.warning, Data = orderStatusModel, Message = courseRlt.Message };
                }
                if (orderInfo.PaymentType == 3)
                {
                    orderInfo.Status = (int)OrderStatus.Success;
                    orderInfo.PaymentTime = DateTime.Now;
                    orderInfo.PaymentNo = orderId;
                }
                else
                {
                    if (isync == false)
                    {
                        orderInfo.Status = (int)OrderStatus.UnPay;
                    }
                }
                orderItem.OrderId = orderId;
                orderItem.Price = float.Parse(orderItem.TotalFee.ToString("#0.00"));
                orderItem.TotalFee = float.Parse(orderItem.TotalFee.ToString("#0.00"));
                if (orderCoupons != null && orderCoupons.Count > 0)
                    orderCoupons.ForEach(t => t.OrderId = orderId);
                var isFlag = await UnitOfWorkService.Execute(async () =>
                 {
                     var orderRlt = _repo.InsertAndGetId(orderInfo);
                     if (orderRlt.IsNullOrEmpty())
                         return false;
                     await _orderItemRepository.InsertAndGetIdAsync(orderItem);
                     if (orderShipping != null)
                     {
                         orderShipping.OrderId = orderId;
                         await _orderShippingRepository.InsertAndGetIdAsync(orderShipping);
                     }

                     if (orderCoupons != null && orderCoupons.Count > 0)
                         _orderCouponRepository.BatchInsert(orderCoupons);
                     orderStatusModel = new OrderStatusModel
                     {
                         OrderInfo = orderInfo,
                         OrderItem = orderItem,
                         OrderCoupons = orderCoupons,
                         OrderShipping = orderShipping
                     };
                     return true;
                 });
                if (isFlag)
                {
                    var key = RedisKeyHelper.CreateOrderStatus();
                    await Redis.HashSetAsync(key, orderId, orderInfo.Status.ToString());
                    return new Result<OrderStatusModel>() { Data = orderStatusModel, Message = "生成订单成功" };
                }
                else
                    return new Result<OrderStatusModel> { Code = (int)ResultCode.Fail, Message = "生成订单失败" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"用户：{orderInfo.UserId}，生成订单异常", ex);
                return new Result<OrderStatusModel>() { Code = (int)ResultCode.Exception, Message = "生成订单异常！" };
            }

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="businessType"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<CanCouponInfo> GetCanCouponInfos(BusinessType businessType, long userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="businessType"></param>
        /// <param name="status"></param>
        /// <returns></returns>GetListPaged
        public async Task<Result<OrderStatusModels>> GetOrders(long userId, int businessType, int status, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var rlt = await UnitOfWorkService.Execute(async () =>
                {
                    var listModels = new OrderStatusModels();
                    var exp = PredicateBuilder.Default<OrderInfoEntity>().And(v => v.UserId == userId);
                    if (businessType > 0)
                        exp.And(v => v.BusinessType == businessType);
                    if (status > 0)
                        exp.And(v => v.Status == status);
                    var orderInfos = _repo.GetPaged(exp, pageIndex, pageSize, out var totalCount, false,
                        t => t.CreateTime).ToList();
                    foreach (var orderInfo in orderInfos)
                    {
                        var orderStatus = new OrderStatusModel
                        {
                            OrderInfo = orderInfo,
                            OrderItem = await _orderItemRepository.FirstOrDefaultAsync(t => t.OrderId == orderInfo.Id),
                            OrderShipping =
                                await _orderShippingRepository.FirstOrDefaultAsync(t => t.OrderId == orderInfo.Id),
                            OrderCoupons = _orderCouponRepository.GetList(t => t.OrderId == orderInfo.Id).ToList()
                        };
                        listModels.MyOrderList.Add(orderStatus);
                    }
                    listModels.TotalCount = totalCount;
                    listModels.PageIndex = pageIndex;
                    listModels.PageSize = pageSize;
                    return new Result<OrderStatusModels>() { Data = listModels };
                });
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"用户：{userId}，获取订单列表信息异常", ex);
                return new Result<OrderStatusModels>() { Code = (int)ResultCode.Exception, Message = "获取订单列表信息异常！" };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="businessType"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<Result<OldOrderStatusModels>> GetOldOrders(long userId, int businessType, int status, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                Expression<Func<OrderInfoEntity, bool>> exp = v => v.UserId == userId;
                if (businessType > 0)
                    exp = v => v.UserId == userId && v.BusinessType == businessType && v.BusinessType != (int)BusinessType.Wallet;
                if (status > 0)
                    exp = v => v.UserId == userId && v.Status == status && v.BusinessType != (int)BusinessType.Wallet;
                if (businessType > 0 && status > 0)
                    exp = v => v.UserId == userId && v.BusinessType == businessType && v.Status == status && v.BusinessType != (int)BusinessType.Wallet;

                var rlt = await UnitOfWorkService.Execute(async () =>
                {
                    var listModels = new OldOrderStatusModels();
                    //                    var orderInfos = _repo.GetPaged(exp, pageIndex, pageSize, out var totalCount, false,
                    //t => t.CreateTime).ToList();
                    var body = _repo.GetList().AsQueryable();
                    var list = body.Where(exp).OrderByDescending(t => t.CreateTime).ToList();
                    var count = list.Count;
                    var orderInfos = list.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    if (orderInfos.Count > 0)
                    {
                        var now = DateTime.Now;
                        foreach (var orderInfo in orderInfos)
                        {
                            var orderStatus = new OrderStatusModel
                            {
                                OrderInfo = orderInfo,
                                OrderItem = await _orderItemRepository.FirstOrDefaultAsync(t => t.OrderId == orderInfo.Id),
                                OrderShipping =
                                    await _orderShippingRepository.FirstOrDefaultAsync(t => t.OrderId == orderInfo.Id),
                                OrderCoupons = _orderCouponRepository.GetList(t => t.OrderId == orderInfo.Id).ToList()
                            };
                            var oldOrderStatus = orderStatus.OrderInfo.MapTo<OldOrderStatusModel>();
                            //oldOrderStatus.PackageType=处理快递公司编码
                            oldOrderStatus.IsRate = orderStatus.OrderInfo.IsRate;//购买成功
                            oldOrderStatus.CommentId = orderStatus.OrderItem.CommentId;
                            oldOrderStatus.CourseId = orderStatus.OrderItem.ItemId.ToString();
                            oldOrderStatus.ItemId = orderStatus.OrderItem.ItemId.ToString();
                            oldOrderStatus.Title = orderStatus.OrderItem.Title;
                            oldOrderStatus.OrderImg = orderStatus.OrderItem.PicPath;
                            oldOrderStatus.Price = orderStatus.OrderItem.TotalFee;
                            oldOrderStatus.Description = orderStatus.OrderItem.Description;
                            if (orderInfo.Status == (int)OrderStatus.UnPay)
                            {
                                if (now > orderInfo.CreateTime.AddMinutes(DomainSetting.UnpayOrderExprie))
                                {
                                    orderInfo.Status = (int)OrderStatus.Cancel;
                                    orderInfo.CloseTime = DateTime.Now;
                                    orderInfo.EndTime = DateTime.Now;
                                    orderInfo.UpdateTime = DateTime.Now;
                                    await _repo.UpdateAsync(orderInfo);
                                }
                            }
                            listModels.MyOrderList.Add(oldOrderStatus);
                        }
                    }
                    listModels.TotalCount = count;
                    listModels.PageIndex = pageIndex;
                    listModels.PageSize = pageSize;
                    return new Result<OldOrderStatusModels>() { Data = listModels };
                });
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"用户：{userId}，获取订单列表信息异常", ex);
                return new Result<OldOrderStatusModels>() { Code = (int)ResultCode.Exception, Message = "获取订单列表信息异常！" };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public async Task<Result<OrderStatusModel>> GetOrder(string orderId)
        {
            try
            {
                var rlt = await UnitOfWorkService.Execute(async () =>
                 {
                     var orderStatus = new OrderStatusModel();
                     var orderInfo = await _repo.GetAsync(orderId);
                     if (orderInfo == null)
                         return new Result<OrderStatusModel>() { Code = (int)ResultCode.Fail, Message = "订单不存在" };

                     orderStatus.OrderInfo = orderInfo;
                     if (orderInfo != null)
                     {
                         if (orderInfo.BusinessType == (int)BusinessType.Course)
                         {
                             var courseBuyCollect = _courseBuyCollectService.Table().FirstOrDefault(n => n.CourseOrderId == orderInfo.Id && n.UserId == orderInfo.UserId);
                             if (courseBuyCollect != null)
                             {
                                 if (DateTime.Compare(DateTime.Now, courseBuyCollect.ExpirationDate) > 0)
                                 {
                                     orderStatus.exmsg = "此课程免费期已过，建议购买观看";
                                     orderStatus.Isexpired = true;
                                 }
                                 else
                                 {
                                     orderStatus.exmsg = "";
                                     orderStatus.Isexpired = false;
                                 }
                             }
                         }
                         else
                         {
                             orderStatus.exmsg = "";
                             orderStatus.Isexpired = false;
                         }
                     }
                     orderStatus.OrderItem = await _orderItemRepository.FirstOrDefaultAsync(t => t.OrderId == orderId);
                     if (orderStatus.OrderItem != null)
                     {
                         if (orderInfo.BusinessType == (int)BusinessType.Course)
                         {
                             var courrlt = await _courseService.GetAsync(long.Parse(orderStatus.OrderItem.ItemId));
                             var courseinfo = courrlt.Data;
                             if (courseinfo != null)
                             {
                                 if (courseinfo.WxStatus)
                                 {
                                     orderStatus.wxqrcode = $"{DomainSetting.ResourceUrl}img/collegecourse/{courseinfo.Id}_wxqr.png";
                                     orderStatus.wxstatus = true;
                                 }
                                 else
                                 {
                                     orderStatus.wxqrcode = "";
                                     orderStatus.wxstatus = false;
                                 }

                             }
                         }
                         else
                         {
                             orderStatus.wxqrcode = "";
                             orderStatus.wxstatus = false;
                         }
                     }
                     orderStatus.OrderShipping = await _orderShippingRepository.FirstOrDefaultAsync(t => t.OrderId == orderId);
                     orderStatus.OrderCoupons = _orderCouponRepository.GetList(t => t.OrderId == orderId).ToList();
                     var discountedPrice = orderStatus.OrderItem.TotalFee - float.Parse(orderStatus.OrderInfo.Payment);
                     orderStatus.DiscountedPrice = discountedPrice.ToString("F2");
                     var currentTicks = orderInfo.CreateTime.AddMinutes(DomainSetting.UnpayOrderExprie).Ticks;
                     var dtFrom = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                     var currentMillis = (currentTicks - dtFrom.Ticks) / 10000;
                     orderStatus.CountDownTime = currentMillis;
                     orderStatus.OrderAutoCancelTime = orderInfo.CreateTime.AddMinutes(DomainSetting.UnpayOrderExprie);
                     return new Result<OrderStatusModel>() { Data = orderStatus };
                 });
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取某个{orderId}订单信息异常", ex);
                return new Result<OrderStatusModel>() { Code = (int)ResultCode.Exception, Message = "获取订单信息异常！" };
            }
        }
        /// <summary>
        /// 订单支付
        /// </summary>
        /// <param name="paymentType"></param>
        /// <param name="orderId"></param>
        /// <param name="businessType"></param>
        /// <param name="isRenew"></param>
        /// <returns></returns>
        public async Task<Result<PayResult>> PayOrder(PaymentType paymentType, string orderId, int businessType, bool isRenew, string tradeType = "APP", string openId = "")
        {
            var body = "";
            var totalFee = -1;
            var detail = "";
            var rlt = await UnitOfWorkService.Execute(async () =>
            {
                var orderInfo = await _repo.GetAsync(orderId);
                var fee = float.Parse(orderInfo.Payment);
                totalFee = (int)(fee * 100);
                var orderItem = await _orderItemRepository.FirstOrDefaultAsync(t => t.OrderId == orderId);
                body = orderItem.Title;
                detail = orderItem.Description;
                return true;
            });
            if (!rlt)
                return new Result<PayResult>() { Code = (int)ResultCode.Fail, Message = "获取订单异常" };

            if (totalFee == -1)
            {
                return new Result<PayResult>() { Code = (int)ResultCode.Fail, Message = "获取订单费用异常" };
            }
            var request = new WeChatPayUnifiedOrderRequest
            {
                Body = body,
                OutTradeNo = orderId,
                Detail = detail,
                TotalFee = totalFee,
                SpBillCreateIp = HttpUtil.GetLocalIp(),
                NotifyUrl = DomainSetting.WeChatPayNotifyUrl,
                TradeType = tradeType,
            };
            Result<WeChatPayResult> weChatPayResult = null;
            if (openId.IsNullOrEmpty())
                weChatPayResult = await _weChatPayService.AppPay(request);
            else
            {
                request.OpenId = openId;
                request.TradeType = "JSAPI";
                weChatPayResult = await _weChatPayService.InnerPay(request);
            }


            if (weChatPayResult.Code == (int)ResultCode.Success)
            {
                var result = weChatPayResult.Data;
                var payResult = result.MapTo<PayWxInfo>();
                return new Result<PayResult>() { Data = new PayResult() { IsSuccess = true, PayWxInfo = payResult } };
            }
            else
            {
                return new Result<PayResult>() { Code = (int)ResultCode.Fail, Message = "微信支付返回错误" };
            }

        }

        /// <summary>
        /// 订单支付
        /// </summary>
        /// <param name="paymentType"></param>
        /// <param name="orderId"></param>
        /// <param name="businessType"></param>
        /// <param name="isRenew"></param>
        /// <returns></returns>
        public async Task<Result<AlipayTradeAppPayViewModel>> AliPayOrder(string orderId, int businessType, bool isRenew)
        {
            try
            {
                var model = await UnitOfWorkService.Execute(async () =>
                {
                    var orderInfo = await _repo.GetAsync(orderId);
                    var orderItem = await _orderItemRepository.FirstOrDefaultAsync(t => t.OrderId == orderId);
                    var viewModel = new AlipayTradeAppPayViewModel
                    {
                        OutTradeNo = orderInfo.Id,
                        TotalAmount = orderInfo.Payment,
                        Subject = orderItem.Title,
                        Body = orderItem.Description,
                        ProductCode = orderItem.ItemId
                    };
                    return viewModel;
                });
                return model != null ? new Result<AlipayTradeAppPayViewModel>() { Data = model } : new Result<AlipayTradeAppPayViewModel>() { Code = (int)ResultCode.Fail };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"支付宝支付，获取订单异常：{orderId}", ex);
                return new Result<AlipayTradeAppPayViewModel> { Code = (int)ResultCode.Fail };
            }

        }
        /// <summary>
        ///金币兑换订单支付
        /// </summary>
        /// <param name="paymentType"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public async Task<Result<ValueChange>> GoldExchange(PaymentType paymentType, string orderId)
        {
            var body = "";
            var totalFee = -1;
            var detail = "";
            long userId = 0;
            long coins = 0;
            var rlt = await UnitOfWorkService.Execute(async () =>
            {
                var orderInfo = await _repo.GetAsync(orderId);
                var fee = float.Parse(orderInfo.Payment);
                totalFee = (int)(fee * 100);
                var orderItem = await _orderItemRepository.FirstOrDefaultAsync(t => t.OrderId == orderId);
                body = orderItem.Title;
                detail = orderItem.Description;
                userId = orderInfo.UserId;
                coins = (long)orderItem.TotalFee;
                return true;
            });
            if (rlt)
            {


                var coinCount = await _userCacheService.GetCoin(userId);
                if (coinCount.Code != (int)ResultCode.Success)
                    return new Result<ValueChange>() { Code = coinCount.Code, Message = coinCount.Message };
                if (System.Math.Abs(coins) > coinCount.Data)
                {
                    return new Result<ValueChange>() { Code = (int)ResultCode.warning, Message = "您的金币值不够兑换该商品" };
                }
                else
                {
                    if (totalFee == -1)
                    {
                        return new Result<ValueChange>() { Code = (int)ResultCode.Fail, Message = "获取订单费用异常" };
                    }
                    else if (totalFee > 0)
                    {
                        return new Result<ValueChange>() { Code = (int)ResultCode.warning, Message = "获取订单信息金额不为0" };
                    }
                    else
                    {
                        //去判断 金币值是否小于当前用户的金币余额
                        var payOrderModel = new PayOrderModel
                        {
                            OrderId = orderId,
                            IsAutoRenew = false,
                            PaymentNo = "",
                            Transaction = "",
                            IsTest = false,
                            PaymentType = (int)paymentType,
                            BusinessType = BusinessType.Product,
                        };
                        var payResult = await PayOrder(payOrderModel);
                        var isSuccess = payResult.Code.Equals((int)ResultCode.Success);

                        //支付金额为0免运费 可以不写入order_pay表
                        //payOrderModel.IsSuccessPay = isSuccess;
                        //var lrtResult = await OrderPay(payOrderModel);

                        if (isSuccess)
                        {
                            //减金币
                            //var res = _rankingService.AddCoin(userId, coins * (-1), BusinessType.Product, $"兑换商品扣减金币：{coins * (-1)}");
                            return new Result<ValueChange> { Code = (int)ResultCode.Success, Message = "金币兑换商品成功", Data = new ValueChange { ChangedValue = 0, CurrentValue = 0 } };

                        }
                        else
                        {
                            return new Result<ValueChange> { Code = (int)ResultCode.warning, Message = "金币兑换商品失败" };

                        }

                    }
                }
            }
            else
            {
                return new Result<ValueChange>() { Code = (int)ResultCode.Fail, Message = "获取订单异常" };
            }


        }


        /// <summary>
        /// 获取订单状态
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public async Task<Result> GetOrderStatus(long userId, string orderId)
        {
            try
            {
                var status = "";
                var orderInfo = UnitOfWorkService.Execute(() => _repo.Get(orderId));
                if (orderInfo != null)
                    status = orderInfo.Status.ToString();
                if (status.IsNullOrEmpty())
                    return new Result() { Code = (int)ResultCode.Fail, Message = "订单不存在" };
                return new Result() { Data = status };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取订单状态异常,订单号{orderId}");
                return new Result() { Code = (int)ResultCode.Exception };
            }
        }

        public async Task<Result> PayOrder(PayOrderModel payOrderModel)
        {
            try
            {
                var key = RedisKeyHelper.CreateOrderStatus();
                var rlt = await UnitOfWorkService.Execute(async () =>
                  {
                      var orderInfo = await _repo.GetAsync(payOrderModel.OrderId);
                      if (orderInfo == null)
                          return new Result() { Code = (int)ResultCode.Fail, Message = "订单不存在" };
                      var status = await Redis.HashGetAsync(key, payOrderModel.OrderId);
                      if (status.IsNotNullOrEmpty() && status.Trim() == ((int)OrderStatus.Cancel).ToString())
                          return new Result() { Code = (int)ResultCode.Fail, Message = "订单已取消，请刷新" };
                      if (status.IsNotNullOrEmpty() && status.Trim() == ((int)OrderStatus.Success).ToString())
                          return new Result() { Code = (int)ResultCode.Fail, Message = "订单已完成，请刷新" };
                      if (status.IsNotNullOrEmpty() && status.Trim() == ((int)OrderStatus.HasPay).ToString())
                          return new Result() { Code = (int)ResultCode.Fail, Message = "订单已支付，请刷新" };
                      orderInfo.Status = (int)OrderStatus.HasPay;
                      orderInfo.PaymentNo = payOrderModel.PaymentNo;
                      orderInfo.PaymentTime = DateTime.Now;
                      orderInfo.UpdateTime = DateTime.Now;
                      payOrderModel.UserId = orderInfo.UserId;
                      var orderRlt = await _repo.UpdateAsync(orderInfo);
                      return !orderRlt ?
                          new Result() { Code = (int)ResultCode.Fail, Message = "订单支付失败" } :
                          new Result() { Message = "订单支付成功" };
                  });
                if (rlt.Code != (int)ResultCode.Success)
                    return rlt;
                var orderBusiness = new OrderBusinessContract
                {
                    UserId = payOrderModel.UserId,
                    OrderId = payOrderModel.OrderId,
                    Status = OrderStatus.HasPay,
                    BusinessType = payOrderModel.BusinessType,
                    IsAutoRenew = payOrderModel.IsAutoRenew
                };
                await Redis.HashSetAsync(key, payOrderModel.OrderId, payOrderModel.Status.ToString());
                await orderBusiness.Publish();
                Console.WriteLine($"订单支付成功：订单Id：{orderBusiness.OrderId}");
                return new Result() { Message = "订单支付成功！", Data = payOrderModel.OrderId };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"支付订单{payOrderModel.OrderId}异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "支付订单异常！" };
            }
        }


        public async Task<Result> OrderPay(PayOrderModel payOrderModel)
        {
            try
            {
                var key = RedisKeyHelper.CreateOrderStatus();
                var iRlt = await UnitOfWorkService.Execute(async () =>
                 {
                     var isFlag = false;
                     var orderInfo = _repo.Get(payOrderModel.OrderId);
                     if (orderInfo == null)
                         throw new Exception("订单不存在");
                     var entity = payOrderModel.MapTo<OrderPayEntity>();
                     var payOrder = _payRepository.GetList()
                         .FirstOrDefault(t => t.OrderId == payOrderModel.OrderId && t.UserId == payOrderModel.UserId);
                     if (!payOrderModel.IsSuccessPay)
                     {
                         entity.IsSuccess = false;
                     }
                     else
                     {
                         if (orderInfo.Status == (int)OrderStatus.UnPay)
                             throw new Exception("订单未付款，请刷新");
                         if (orderInfo.Status == (int)OrderStatus.Cancel)
                             throw new Exception("订单已取消，请刷新");
                         if (orderInfo.Status == (int)OrderStatus.Close)
                             throw new Exception("订单已关闭，请刷新");
                         entity.IsSuccess = true;
                     }
                     entity.BusinessType = orderInfo.BusinessType;
                     entity.Price = orderInfo.Payment;
                     var lrt = await _userCacheService.GetAid(payOrderModel.UserId);
                     entity.Aid = lrt.Data;
                     if (payOrderModel.PayTime != null)
                     {
                         entity.CreateTime = payOrderModel.PayTime.GetValueOrDefault();
                         Logger.Info($"系统手动更新了订单支付台账:用户：{payOrderModel.UserId},订单号{payOrderModel.OrderId},更新时间:{DateTime.Now}");
                     }
                     if (payOrder == null)
                     {
                         entity.CreateTime = DateTime.Now;
                         isFlag = await _payRepository.InsertAndGetIdAsync(entity) > 0;
                     }
                     else
                     {
                         entity.Id = payOrder.Id;
                         isFlag = await _payRepository.UpdateAsync(entity);
                     }
                     return isFlag;
                 });
                if (iRlt)
                {
                    Logger.Info($"订单支付台账写入成功:订单号{payOrderModel.OrderId}，用户{payOrderModel.UserId}");
                    return new Result { Message = "订单支付台账写入成功" };
                }
                else
                {
                    Logger.Error($"订单支付台账写入失败:订单号{payOrderModel.OrderId}，用户{payOrderModel.UserId}");
                    return new Result { Code = (int)ResultCode.Fail, Message = "订单支付台账写入失败" };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("订单支付台账写入异常:订单号{payOrderModel.OrderId}，用户{payOrderModel.UserId}", ex);
                throw;
            }

        }


        public async Task<Result> CancelOrder(string orderId)
        {
            try
            {
                var rlt = await UnitOfWorkService.Execute(async () =>
                 {
                     var orderInfo = await _repo.GetAsync(orderId);
                     if (orderInfo == null)
                         return new Result() { Code = (int)ResultCode.Fail, Message = "订单不存在" };
                     if (orderInfo.Status != (int)OrderStatus.UnPay)
                         return new Result() { Code = (int)ResultCode.Fail, Message = $"订单{EnumHelper.GetEnumFromStr<OrderStatus>(orderInfo.Status.ToString())}，请刷新" };

                     orderInfo.Status = (int)OrderStatus.Cancel;
                     orderInfo.CloseTime = DateTime.Now;
                     orderInfo.EndTime = DateTime.Now;
                     orderInfo.UpdateTime = DateTime.Now;
                     var orderRlt = await _repo.UpdateAsync(orderInfo);
                     if (!orderRlt)
                         return new Result() { Code = (int)ResultCode.Fail, Message = "取消订单失败" };
                     else
                     {
                         var key = RedisKeyHelper.CreateOrderStatus();
                         await Redis.HashSetAsync(key, orderId, orderInfo.Status.ToString());
                         return new Result() { Data = orderId, Message = "取消订单成功" };
                     }
                 });
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"取消订单{orderId}异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "取消订单异常" };
            }
        }

        public async Task<Result> CloseOrder(string orderId)
        {
            try
            {
                var rlt = await UnitOfWorkService.Execute(async () =>
                {
                    var orderInfo = await _repo.GetAsync(orderId);
                    if (orderInfo == null)
                        return new Result() { Code = (int)ResultCode.Fail, Message = "订单不存在" };
                    if (orderInfo.Status != (int)OrderStatus.Success)
                        return new Result() { Code = (int)ResultCode.Fail, Message = $"订单{EnumHelper.GetEnumFromStr<OrderStatus>(orderInfo.Status.ToString())}，请刷新" };
                    var userId = orderInfo.UserId;
                    orderInfo.Status = (int)OrderStatus.Cancel;
                    orderInfo.CloseTime = DateTime.Now;
                    orderInfo.EndTime = DateTime.Now;
                    orderInfo.UpdateTime = DateTime.Now;
                    var orderRlt = await _repo.UpdateAsync(orderInfo);
                    //更新 
                    var orderPay = _payRepository.GetList().FirstOrDefault(t => t.OrderId == orderId);
                    if (orderPay == null)
                        return new Result() { Code = (int)ResultCode.Fail, Message = "订单异常" };
                    orderPay.IsRefund = 1;
                    orderPay.UpdateTime = DateTime.Now;
                    _payRepository.Update(orderPay);
                    //更新课程
                    if (orderInfo.BusinessType == (int)BusinessType.Course)
                    {
                        var orderItem = _orderItemRepository.GetList(t => t.OrderId == orderId).FirstOrDefault();
                        var courseId = long.Parse(orderItem.ItemId);
                        var courseBuyCollectEntity = _courseBuyCollectService.Table()
                            .FirstOrDefault(n => n.CourseId == courseId && n.UserId == userId);
                        if (courseBuyCollectEntity == null)
                        {
                            return new Result() { Code = (int)ResultCode.Fail, Message = "课程信息为空" };
                        }
                        courseBuyCollectEntity.IsBuy = 0;
                        courseBuyCollectEntity.ExpirationDate = DateTime.Now;
                        courseBuyCollectEntity.IsHide = 1;
                        _courseBuyCollectService.Update(courseBuyCollectEntity);
                    }

                    if (!orderRlt)
                        return new Result() { Code = (int)ResultCode.Fail, Message = "关闭订单失败" };
                    else
                    {
                        var key = RedisKeyHelper.CreateOrderStatus();
                        await Redis.HashSetAsync(key, orderId, orderInfo.Status.ToString());
                        return new Result() { Data = orderId, Message = "关闭订单成功" };
                    }
                });
                return rlt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"关闭订单{orderId}异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "关闭订单异常" };
            }
        }

        public async Task<Result<string>> CountPrice(long userId, float price, int count, long userCouponId)
        {
            try
            {
                var priceCount = price * count;
                var coupon = await UnitOfWorkService.Execute(async () =>
                 {
                     //获取用户的优惠券是否存在
                     var usrCouponInfo = await _userCouponRepository.GetAsync(userCouponId);
                     if (usrCouponInfo == null)
                         return null;
                     var couponInfo = await _couponInfoRepository.GetAsync(usrCouponInfo.CouponId);
                     return couponInfo;
                 });
                if (coupon == null)
                    return new Result<string>() { Data = priceCount.ToString("0.00") };
                if (coupon.Category == (int)CouponCategory.ZKQ)
                {
                    if (coupon.Discount != null)
                        priceCount = priceCount * coupon.Discount.Value * 0.1F;
                }
                return new Result<string>() { Data = priceCount.ToString("0.00") };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("计算订单价格异常", ex);
                return new Result<string>() { Code = (int)ResultCode.Exception, Message = "计算订单价格异常" };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public async Task<Result> FixOrderPay(string orderId, string transaction)
        {
            try
            {
                var rlt = UnitOfWorkService.Execute(() =>
                {
                    var orderInfo = _repo.Get(orderId);
                    if (orderInfo == null)
                        throw new Exception("系统创建订单台账失败,订单不存在");
                    var orderPay = orderInfo.MapTo<OrderPayEntity>();
                    orderPay.UpdateTime = orderInfo.UpdateTime;
                    orderPay.CreateTime = orderInfo.UpdateTime;
                    orderPay.Transaction = transaction;
                    orderPay.IsTest = false;
                    orderPay.Aid = "college";
                    return _payRepository.InsertAndGetId(orderPay);
                });

                if (rlt > 0)
                    return new Result { Message = "系统创建订单台账成功" };
                Logger.Error($"系统创建订单台账失败");
                return await Task.Run(() => new Result { Code = (int)ResultCode.Fail, Message = "系统创建订单台账失败" });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw new Exception("系统创建订单台账异常");
            }

        }


        public IQueryable<OrderPayEntity> OrderPayTable()
        {
            var body = UnitOfWorkService.Execute<IQueryable<OrderPayEntity>>(() =>
            {
                return _payRepository.GetList(p => p.PaymentType == 4).AsQueryable();
            });
            return body;
        }

        /// <summary>
        /// 清空黄鹂接口api
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task NoticeMxhClearCache(string nodeName = "", int code = 0)
        {
            var path = string.Format("/api/NoticeClearCache?nodeName={0}&code={1}", nodeName, code);
            var rlt = await HttpPost<string>(path, null);

        }
        public async Task<Result> OfflinePay(CourseOfflinePay courseOfflinePay)
        {
            try
            {
                var orderexit = _courseBuyCollectService.Table().Where(p => p.CourseId == courseOfflinePay.CourseId && p.UserId == courseOfflinePay.UserId && p.IsBuy == 1);
                if (orderexit.Count() > 0)
                {
                    //return new Result { Message = "改用户已经开通过改课程，如有疑问，请联系后台管理员" };
                    commpay(courseOfflinePay, true);
                }
                else
                    commpay(courseOfflinePay);

                return new Result { Message = "线下支付课程开通成功" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("线下支付课程开通异常");
                return new Result { Code = (int)ResultCode.Exception, Message = "线下支付课程开通异常" };
            }
        }

        public async void commpay(CourseOfflinePay courseOfflinePay, bool isHas = false)
        {
            var courseId = courseOfflinePay.CourseId;
            var useInfo = await _userService.GetUserByPhone(courseOfflinePay.Phone);
            courseOfflinePay.Transaction = courseOfflinePay.Phone + "购买了" + courseOfflinePay.CourseId;
            if (courseOfflinePay.TransactionNo.Contains("免费") || courseOfflinePay.TransactionNo.Contains("赠送"))
            {
                courseOfflinePay.Price = 0;
            }
            if (courseOfflinePay.PaymentType == 9)
            {
                courseOfflinePay.Price = 0;
            }
            var useAddress = await _addressService.Get(useInfo.Userid);
            var addInfo = useAddress.Data;
            int ok = addInfo != null && !string.IsNullOrWhiteSpace(addInfo.Phone) ? 1 : 0;
            var lrt = await _courseService.GetAsync(courseId);
            var courseEntity = lrt.Data;
            courseEntity.ExpressPirce = courseEntity.IsExpress == 0 ? 0M : courseEntity.ExpressPirce;
            string spBillno = string.Format("{0}{1}", EncryptionUtility.OrderCode(), Senparc.Weixin.TenPay.V3.TenPayV3Util.BuildRandomStr(6));
            var payInfo = courseEntity.MapTo<PayOrderInfo>();
            payInfo.CourseId = courseEntity.Id;
            payInfo.PreferentialBeginTime = courseEntity.PreferentialBeginTime.ToString("yyyy.MM.dd");
            payInfo.PreferentialEndTime = courseEntity.PreferentialEndTime.ToString("yyyy.MM.dd");
            payInfo.ValidBeginTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
            payInfo.ValidEndTime = courseEntity.ValidBeginTime.ToString("yyyy.MM.dd");
            payInfo.OrderNo = spBillno;

            #region  订单主体
            OrderInfoModel OrderInfo = new OrderInfoModel();
            OrderInfo.ProductId = courseEntity.Iosproductid;
            OrderInfo.Payment = courseOfflinePay.Price.ToString("F2");
            OrderInfo.PaymentType = courseOfflinePay.PaymentType;
            OrderInfo.PostFee = "0";
            OrderInfo.ShippingName = "";
            OrderInfo.ShippingCode = "";
            OrderInfo.IsFrom = 4;//默认是后台web端不验证是否续费
            OrderInfo.BusinessType = (int)BusinessType.Course;
            var orderInfoEntity = OrderInfo.MapTo<OrderInfoEntity>();
            orderInfoEntity.Id = spBillno;
            orderInfoEntity.UserId = courseOfflinePay.UserId;
            orderInfoEntity.PaymentTime = courseOfflinePay.PaymentTime;
            orderInfoEntity.Status = (int)OrderStatus.Success;
            orderInfoEntity.PaymentNo = courseOfflinePay.TransactionNo;
            //orderInfoEntity.EndTime = courseOfflinePay.PaymentTime;
            orderInfoEntity.CreateTime = courseOfflinePay.PaymentTime;
            orderInfoEntity.UpdateTime = DateTime.Now;
            #endregion
            #region  订单明细
            OrderItemModel OrderItem = new OrderItemModel();
            OrderItem.ItemId = courseEntity.Id.ToString();
            OrderItem.Num = 1;
            OrderItem.Title = courseEntity.CourseName;
            OrderItem.Description = courseEntity.Detailed;
            OrderItem.Price = float.Parse(courseOfflinePay.Price.ToString("F2"));
            OrderItem.TotalFee = float.Parse(courseOfflinePay.Price.ToString("F2")); ;
            OrderItem.PicPath = courseEntity.CoursePicPhone;

            var orderItemEntity = OrderItem.MapTo<OrderItemEntity>();
            #endregion
            #region  收货地址明细
            AddressInfo AddressInfo = new AddressInfo();
            AddressInfo.Userid = (int)courseOfflinePay.UserId;
            if (addInfo != null)
            {
                AddressInfo.Sex = addInfo.Sex;
                AddressInfo.Phone = addInfo.Phone;
                AddressInfo.Consignee = addInfo.Consignee;
                AddressInfo.Provincial = addInfo.Provincial;
                AddressInfo.City = addInfo.City;
                AddressInfo.Areas = addInfo.Areas;
                AddressInfo.Address = addInfo.Address;
            }
            var orderShippingEntity = AddressInfo.MapTo<OrderShippingEntity>();
            #endregion

            #region  使用优惠券明细
            OrderCouponInfoModel OrderCouponInfo = new OrderCouponInfoModel();
            //OrderCouponInfo.ItemId= courseEntity.Id.ToString();
            var orderCouponEntities = OrderCouponInfo.MapTo<List<OrderCouponEntity>>();
            #endregion

            var orderPay = orderInfoEntity.MapTo<OrderPayEntity>();
            orderPay.UpdateTime = orderInfoEntity.UpdateTime;
            orderPay.CreateTime = orderInfoEntity.CreateTime;
            orderPay.Transaction = "线下支付";
            orderPay.IsTest = false;
            var node = KhXt.YcRs.Domain.DomainEnv.NodeName;
            string aid = "college";
            switch (node)
            {
                case "cms_45":
                    aid = "huangli";
                    break;
                case "df_cms_45":
                    aid = "dufu";
                    break;
                default: /* 可选的 */
                    aid = "college";
                    break;
            }
            orderPay.Aid = aid;//动态读取
            Add(orderPay);
            // _payRepository.InsertAndGetId(orderPay);
            var rlt = await CreateOrder(orderInfoEntity, orderItemEntity, orderShippingEntity,
            orderCouponEntities, true);
            //生成购买课程记录 回调修改状态
            var courseBuyCollectEntity = _courseBuyCollectService.Table()
                .FirstOrDefault(n => n.CourseId == courseEntity.Id && n.UserId == useInfo.Userid);
            if (courseBuyCollectEntity == null)
            {
                _courseBuyCollectService.Add(new CourseBuyCollectEntity
                {
                    UserId = (int)useInfo.Userid,
                    CourseId = (int)courseEntity.Id,
                    CourseOrderId = spBillno,
                    CreateId = 0,
                    CreateTime = DateTime.Now,
                    IsBuy = 1,
                    ExpirationDate = DateTime.Now.AddYears(1),
                    IsHide = 0,
                    IsCollect = 0,
                });
            }
            else
            {
                courseBuyCollectEntity.CourseOrderId = spBillno;
                courseBuyCollectEntity.IsBuy = 1;
                courseBuyCollectEntity.ExpirationDate = DateTime.Now.AddYears(1);
                courseBuyCollectEntity.IsHide = 0;
                _courseBuyCollectService.Update(courseBuyCollectEntity);
            }
        }

        public async Task<Result<OrderQueryResultPageModel>> GetOrders(OrderQueryModel orderQueryModel)
        {
            try
            {
                OrderQueryResultPageModel data = null;
                DateTime dtStart;
                DateTime dtEnd;
                var users = await _userCacheService.GetUserList();
                var now = DateTime.Now;
                if (orderQueryModel.StartTime.IsNullOrEmpty())
                    orderQueryModel.StartTime = $"{now.ToString("yyyy-MM-dd")} 00:00:00";
                if (orderQueryModel.EndTime.IsNullOrEmpty())
                    orderQueryModel.EndTime = now.ToString("yyyy-MM-dd HH:mm:ss");
                if (!DateTime.TryParse(orderQueryModel.StartTime, out dtStart))
                    return new Result<OrderQueryResultPageModel> { Code = (int)ResultCode.Fail, Message = "开始时间格式不正确！" };
                if (!DateTime.TryParse(orderQueryModel.EndTime, out dtEnd))
                    return new Result<OrderQueryResultPageModel> { Code = (int)ResultCode.Fail, Message = "结束时间格式不正确！" };

                if (dtStart == DateTime.MinValue || dtStart > now)
                    dtStart = DateTime.Parse($"{now.ToString("yyyy-MM-dd")} 00:00:00");
                if (dtEnd == DateTime.MinValue || dtEnd > now)
                    dtEnd = now;
                if (dtEnd < dtStart)
                    return new Result<OrderQueryResultPageModel> { Code = (int)ResultCode.Fail, Message = "开始时间大于结束时间！" };

                var list = UnitOfWorkService.Execute(() => { return _repo.GetList(t => t.CreateTime >= dtStart && t.CreateTime <= dtEnd); });

                if (orderQueryModel.BusinessType > 0)
                    list = list.Where(t => t.BusinessType == orderQueryModel.BusinessType);
                if (orderQueryModel.PayType > -1)
                    list = list.Where(t => t.PaymentType == orderQueryModel.PayType);
                if (orderQueryModel.UserName.IsNotNullOrEmpty())
                {
                    var tmpUsers = users.FindAll(t => t.Username.IsNotNullOrEmpty());
                    var userInfo = tmpUsers.Where(t => t.Username.Contains(orderQueryModel.UserName.Trim()));
                    var ids = userInfo.Select(t => t.Userid).ToList();
                    if (tmpUsers != null)
                        list = list.Where(t => ids.Contains(t.UserId));
                }
                if (orderQueryModel.Phone.IsNotNullOrEmpty())
                {
                    var tmpUsers = users.FindAll(t => t.Phone.IsNotNullOrEmpty());
                    var userInfo = tmpUsers.Where(t => t.Phone.Contains(orderQueryModel.Phone.Trim()));
                    var ids = userInfo.Select(t => t.Userid).ToList();
                    if (tmpUsers != null)
                        list = list.Where(t => ids.Contains(t.UserId));
                }
                if (orderQueryModel.TransactionNo.IsNotNullOrEmpty())
                    list = list.Where(t => t.PaymentNo.Contains(orderQueryModel.TransactionNo));
                if (orderQueryModel.OrderId.IsNotNullOrEmpty())
                    list = list.Where(t => t.Id.Contains(orderQueryModel.OrderId));

                list = list.OrderByDescending(t => t.CreateTime).ToList();

                var entitys = list.Page(orderQueryModel.PageSize, orderQueryModel.PageIndex);

                if (entitys != null && entitys.Items != null && entitys.Items.Length > 0)
                {
                    data = new OrderQueryResultPageModel();
                    data.Rows = entitys.Items.ToList().MapTo<List<OrderQueryResultModel>>();
                    data.PageSize = orderQueryModel.PageSize;
                    data.PageIndex = orderQueryModel.PageIndex;
                    data.TotalItemCount = list.Count();
                }
                if (data != null && data.Rows != null && data.Rows.Count > 0)
                {
                    var orders = data.Rows.Select(s => s.Id);
                    //var items = UnitOfWorkService.Execute(() => _orderItemRepository.GetList());
                    var items = UnitOfWorkService.Execute(() => _orderItemRepository.Query<Domain.Entities.Order.OrderItemEntity>("SELECT `order_item`.`ItemId`, `order_item`.`OrderId`,`order_item`.`Title`,  `order_item`.`Id` FROM `order_item`"));
                    var tmp = items.Where(t => orders.Contains(t.OrderId)).Select(t => new { t.Title, t.OrderId });

                    var itemInfos = tmp.ToList();
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        var userInfo = users.Find(t => t.Userid == data.Rows[i].UserId);
                        if (userInfo != null)
                        {
                            data.Rows[i].UserName = userInfo.Username;
                            data.Rows[i].Phone = userInfo.Phone;
                        }
                        var itemInfo = itemInfos.FirstOrDefault(t => t.OrderId == data.Rows[i].Id);
                        if (itemInfo != null)
                        {
                            data.Rows[i].ItemName = itemInfo.Title;
                        }
                    }
                }
                return new Result<OrderQueryResultPageModel> { Data = data };
            }
            catch (Exception ex)
            {
                return new Result<OrderQueryResultPageModel>
                { Code = (int)ResultCode.Exception, Message = ex.Message };
            }
        }
    }
}
