﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Order;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Order
{
    //LHT:Create 改Add,返回值为主键类型，update,del返回值改为bool类型，加入缓存，实现clearcache,dosync,相关方法加入同步消息通知,清除0引用方法
    public class CouponInfoService : ServiceBase, ICouponInfoService
    {
        private readonly ICouponInfoRepository _repo;
        private readonly IUserCouponRepository _userCouponRepository;
        private readonly IUserRepository _userRepository;
        public CouponInfoService(ICouponInfoRepository repo,
            IUserCouponRepository userCouponRepository,
            IUserRepository userRepository)
        {
            _repo = repo;
            _userCouponRepository = userCouponRepository;
            _userRepository = userRepository;
        }

        public IQueryable<CouponInfoEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<CouponInfoEntity>>(() =>
               {
                   return _repo.GetList().AsQueryable();
               });
            return body;
        }

        public async Task<Result> AddCouponInfo(CouponInfoEntity couponInfo)
        {
            try
            {
                var rlt = await UnitOfWorkService.Execute(async () => await _repo.InsertAndGetIdAsync(couponInfo));
                return rlt != 0
                    ? new Result { Data = rlt.ToString(), Message = "保存优惠券成功！" }
                    : new Result { Code = (int)ResultCode.Fail, Data = rlt.ToString(), Message = "保存优惠券失败！" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("保存优惠券异常", ex);
                return new Result { Code = (int)ResultCode.Fail, Message = "保存优惠券异常！" };
            }
        }

        public async Task<Result> UpdateCouponInfo(long id, CouponInfoEntity couponInfo)
        {
            try
            {
                var rlt = await UnitOfWorkService.Execute(async () => await _repo.UpdateAsync(couponInfo));
                return rlt
                    ? new Result { Data = rlt.ToString(), Message = "更新优惠券成功！" }
                    : new Result { Code = (int)ResultCode.Fail, Data = rlt.ToString(), Message = "更新优惠券失败！" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("更新优惠券异常", ex);
                return new Result { Code = (int)ResultCode.Fail, Message = "更新优惠券异常！" };
            }
        }
        public async Task<Result<MyCouponList>> GetMyCoupons(long userId, int status = -2, int businessType = -1, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var list = await UnitOfWorkService.Execute(async () => await _userCouponRepository.GetListAsync());
                var queryList = list.AsQueryable();
                queryList = queryList.Where(t => t.UserId.Equals(userId));
                if (status != -2)
                    queryList = queryList.Where(t => t.Status == status);
                if (businessType != -1)
                    queryList = queryList.Where(t => t.BusinessType == businessType);
                var totalCount = queryList.Count();
                var ts = queryList.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                var myCouponList = ts.MapTo<List<MyCoupon>>();
                if (myCouponList == null || myCouponList.Count <= 0)
                    return new Result<MyCouponList>
                    {
                        Data = new MyCouponList()
                        {
                            CouponList = myCouponList,
                            PageIndex = pageIndex,
                            PageSize = pageSize,
                            TotalCount = totalCount
                        }
                    };

                myCouponList.ForEach(async t => t.CouponInfo = await UnitOfWorkService.Execute(() => _repo.GetAsync(t.CouponId)));
                return new Result<MyCouponList>
                {
                    Data = new MyCouponList()
                    { CouponList = myCouponList, PageIndex = pageIndex, PageSize = pageSize, TotalCount = totalCount }
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取当前用户所有优惠券异常", ex);
                return new Result<MyCouponList>() { Code = (int)ResultCode.Exception, Message = "获取当前用户所有优惠券异常" };
            }

        }
        public async Task<Result<MyCanUseCouponList>> GetMyCanUseCoupons(long userId, int businessType = -1, int vipType = 0, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var list = await UnitOfWorkService.Execute(async () => await _userCouponRepository.GetListAsync());
                var queryList = list.AsQueryable();
                queryList = queryList.Where(t => t.UserId.Equals(userId) &&
                                                 t.Status == (int)CouponStatus.NoUse &&
                                                 t.UseLimit != (int)CouponUseLimit.Share);
                if (vipType != 0)
                    queryList = queryList.Where(t => t.VipType == vipType);
                if (businessType != -1)
                    queryList = queryList.Where(t => t.BusinessType == businessType);
                var totalCount = queryList.Count();
                var ts = queryList.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                var myCouponList = ts.MapTo<List<MyCoupon>>();
                if (myCouponList == null || myCouponList.Count <= 0)
                    return new Result<MyCanUseCouponList>
                    {
                        Data = new MyCanUseCouponList()
                        {
                            CouponList = myCouponList,
                            PageIndex = pageIndex,
                            PageSize = pageSize,
                            TotalCount = totalCount
                        }
                    };

                myCouponList.ForEach(async t => t.CouponInfo = await UnitOfWorkService.Execute(() => _repo.GetAsync(t.CouponId)));
                return new Result<MyCanUseCouponList>
                {
                    Data = new MyCanUseCouponList()
                    { CouponList = myCouponList, PageIndex = pageIndex, PageSize = pageSize, TotalCount = totalCount }
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取当前用户所有优惠券异常", ex);
                return new Result<MyCanUseCouponList>() { Code = (int)ResultCode.Exception, Message = "获取当前用户所有优惠券异常" };
            }

        }
        public async Task<Result<List<MyCoupon>>> GetMyCouponAll(long userId, int status = -2, int businessType = -1)
        {
            try
            {
                var list = await UnitOfWorkService.Execute(async () => await _userCouponRepository.GetListAsync());
                var queryList = list.AsQueryable();
                queryList = queryList.Where(t => t.UserId.Equals(userId));
                if (status != -2)
                    queryList = queryList.Where(t => t.Status == status);
                if (businessType != -1)
                    queryList = queryList.Where(t => t.BusinessType == businessType);
                var ts = queryList.ToList();
                var myCouponList = ts.MapTo<List<MyCoupon>>();
                if (myCouponList == null || myCouponList.Count <= 0)
                    return new Result<List<MyCoupon>>() { Data = myCouponList };
                myCouponList.ForEach(async t => t.CouponInfo = await UnitOfWorkService.Execute(() => _repo.GetAsync(t.CouponId)));
                return new Result<List<MyCoupon>> { Data = myCouponList };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取当前用户所有优惠券异常", ex);
                return new Result<List<MyCoupon>>() { Code = (int)ResultCode.Exception, Message = "获取当前用户所有优惠券异常" };
            }

        }

        /// <summary>
        /// 获取当前用户指定优惠券信息
        /// </summary>
        /// <param name="couponId"></param>
        /// <returns></returns>
        public async Task<Result<MyCoupon>> GetMyCoupon(long couponId)
        {
            try
            {
                var entity = await UnitOfWorkService.Execute(async () => await _userCouponRepository.GetAsync(couponId));
                if (entity != null)
                {
                    var myCoupon = entity.MapTo<MyCoupon>();
                    myCoupon.CouponInfo = await UnitOfWorkService.Execute(async () => await _repo.GetAsync(entity.CouponId));
                    return new Result<MyCoupon>() { Data = myCoupon };
                }
                else
                    return new Result<MyCoupon>() { Code = (int)ResultCode.Fail, Message = "获取当前用户指定优惠券信息失败" };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取当前用户所有优惠券异常", ex);
                return new Result<MyCoupon>() { Code = (int)ResultCode.Exception, Message = "获取当前用户指定优惠券信息异常" };
            }
        }

        public async Task<Result> GiveToFriend(long userId, long userCouponId, string phone)
        {
            try
            {
                var userInfo = UnitOfWorkService.Execute(() => _userRepository.FirstOrDefault(u => u.Phone.Equals(phone)));
                if (userInfo == null)
                    return new Result() { Code = (int)ResultCode.Fail, Message = "请先让好友注册马小哈" };
                if (userInfo.Id == userId)
                    return new Result() { Code = (int)ResultCode.Fail, Message = "不能赠送给自己" };
                var isFlag = await UnitOfWorkService.Execute(async () =>
                 {
                     var entity = _userCouponRepository.Get(userCouponId);
                     if (entity == null)
                         return -2;
                     if (entity.Status != 0)
                         return entity.Status;
                     entity.Status = (int)CouponStatus.HasUsed;
                     var isUpdate = await _userCouponRepository.UpdateAsync(entity);
                     if (!isUpdate)
                         return -3;
                     var tempEntity = entity.MapTo<UserCouponEntity>();
                     tempEntity.Status = (int)CouponStatus.NoUse;
                     tempEntity.UserId = userInfo.Id;
                     tempEntity.UseLimit = (int)CouponUseLimit.Own;
                     tempEntity.UpdateTime = DateTime.Now;
                     tempEntity.GiveTime = DateTime.Now;
                     tempEntity.Giver = userId;
                     var result = await _userCouponRepository.InsertAndGetIdAsync(tempEntity);
                     return result > 1 ? 200 : result;
                 });
                if (isFlag == 200) return new Result() { Message = "赠送成功", Data = isFlag.ToString() };
                if (isFlag == -2)
                    return new Result() { Code = (int)ResultCode.Fail, Message = "优惠券信息不存在" };
                if (isFlag == -3)
                    return new Result() { Code = (int)ResultCode.Fail, Message = "赠送优惠券失败" };
                var error = EnumHelper.GetDescription(
                    EnumHelper.GetEnumFromStr<CouponStatus>(isFlag.ToString()));
                return new Result() { Code = (int)ResultCode.Fail, Data = isFlag.ToString(), Message = $"赠送优惠券失败,优惠券{error}" };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("赠送优惠券给指定好友异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "赠送优惠券给指定好友异常" };
            }
        }

        /// <summary>
        /// 获取用户可用的优惠券
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <param name="businessType"></param>
        /// <returns></returns>
        public async Task<Result> GetUsableCount(long userId, int status = -2, int businessType = -1)
        {
            try
            {
                var list = await UnitOfWorkService.Execute(async () => await _userCouponRepository.GetListAsync());
                var queryList = list.AsQueryable();
                queryList = queryList.Where(t => t.UserId.Equals(userId));
                if (status != -2)
                    queryList = queryList.Where(t => t.Status == status);
                if (businessType != -1)
                    queryList = queryList.Where(t => t.BusinessType == businessType);
                var ts = queryList.ToList();
                return new Result { Code = (int)ResultCode.Success, Message = "获取当前用户可用优惠券数量", Data = ts.Count.ToString() };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取当前用户可用优惠券异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "获取当前用户可用优惠券异常" };
            }

        }
        public async Task<Result> userCouponHasUsed(long userId, long userCouponId)
        {
            try
            {
                var userInfo = UnitOfWorkService.Execute(() => _userRepository.FirstOrDefault(u => u.Id == userId));

                var isFlag = await UnitOfWorkService.Execute(async () =>
                {
                    var entity = _userCouponRepository.Get(userCouponId);
                    if (entity == null)
                        return -2;
                    if (entity.Status != 0)
                        return entity.Status;
                    entity.Status = (int)CouponStatus.HasUsed;
                    var isUpdate = await _userCouponRepository.UpdateAsync(entity);
                    return isUpdate == true ? 200 : 100;
                });
                if (isFlag == 200)
                {
                    return new Result() { Message = "修改券已使用成功", Data = isFlag.ToString() };
                }
                else
                {
                    return new Result() { Code = (int)ResultCode.Fail, Data = isFlag.ToString(), Message = $"修改券失败" };
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("修改券失败异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "修改券失败异常" };
            }
        }

        public async Task<Result> GetIsCanUse(long userCouponId, string conflictId)
        {
            try
            {
                var isFlag = await UnitOfWorkService.Execute(async () =>
                {
                    var entity = await _userCouponRepository.GetAsync(userCouponId);
                    if (entity == null)
                        return -2;
                    if (entity.Status != 0)
                        return entity.Status;
                    entity.Status = (int)CouponStatus.HasUsed;
                    var couponInfo = _repo.Get(entity.CouponId);
                    if (couponInfo == null)
                        return -2;
                    if (string.IsNullOrEmpty(couponInfo.ConflictIds))
                        return 200;

                    var temps = couponInfo.ConflictIds.Split(',', StringSplitOptions.RemoveEmptyEntries);
                    if (temps == null)
                        return 200;
                    var list = temps.ToList();
                    if (list.Exists(t => t == conflictId))
                        return 100;
                    else
                        return 200;
                });
                if (isFlag == 200)
                {
                    return new Result() { Message = "此兑换券可以使用", Data = isFlag.ToString() };
                }
                else
                {
                    return new Result() { Code = (int)ResultCode.Fail, Data = isFlag.ToString(), Message = $"此兑换券不能使用" };
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("不能使用此兑换券，异常信息", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "不能使用此兑换券，异常信息" };
            }
        }
    }









}
