﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Order;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Order;
using Senparc.NeuChar.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Order
{
    public class IOSPriceService : ServiceBase, IIOSPriceService
    {
        IIOSPriceRepository _repo;

        public IOSPriceService(IIOSPriceRepository repo)
        {
            _repo = repo;
        }

        public IQueryable<IOSPriceEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<IOSPriceEntity>>(() =>
               {
                   return _repo.GetList().AsQueryable();
               });
            return body;
        }

        public async Task<Result<float>> GetIosPrice(string productId)
        {
            try
            {
                var rlt = await UnitOfWorkService.Execute(async () =>
                 {
                     var query = await _repo.GetListAsync();
                     return query = query.Where(t => t.ProductId == productId).ToList();
                 });
                var iosPriceEntities = rlt.ToList();
                return iosPriceEntities.Any()
                    ? new Result<float>() { Data = iosPriceEntities[0].Price }
                    : new Result<float>() { Code = (int)ResultCode.Fail, Message = "获取苹果价格失败，不存在当前productId" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取苹果价格异常", ex);
                return new Result<float>() { Code = (int)ResultCode.Exception, Message = "获取苹果价格异常" };
            }
        }

        public async Task<Result<Dictionary<string, float>>> Gets()
        {
            try
            {
                var rlt = await UnitOfWorkService.Execute(async () =>
                {
                    var query = await _repo.GetListAsync();
                    return query = query.ToList();
                });
                var iosPriceEntities = rlt.ToList();
                var dic = iosPriceEntities.ToDictionary(iosPrice => iosPrice.ProductId, iosPrice => iosPrice.Price);
                return new Result<Dictionary<string, float>>() { Data = dic };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("获取苹果价格列表异常", ex);
                return new Result<Dictionary<string, float>>() { Code = (int)ResultCode.Exception, Message = "获取苹果价格列表异常" };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        public async Task<Result> AddIOSPrice(IOSPriceEntity addEntity)
        {
            try
            {
                var isFlag = await UnitOfWorkService.Execute(async () =>
                    {
                        var isHas = _repo.FirstOrDefault(t => t.ProductId.Equals(addEntity.ProductId));
                        if (isHas != null)
                            return -1;
                        return await _repo.InsertAndGetIdAsync(addEntity);
                    });
                if (isFlag == -1)
                {
                    return new Result() { Code = (int)ResultCode.Fail, Message = "已存在当前产品Id" };
                }
                return isFlag > 0 ? new Result() { Data = isFlag.ToString(), Message = "添加成功" } : new Result() { Code = (int)ResultCode.Fail, Message = "添加苹果价格失败" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("添加苹果价格异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "添加苹果价格异常" };
            }
        }
    }









}
