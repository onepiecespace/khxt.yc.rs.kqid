﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.Models.Sys;
using KhXt.YcRs.Domain.Pagination;
using KhXt.YcRs.Domain.Repositories.Sys;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using Hx.Extensions;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services
{
    public class ContentService : ServiceBase, IContentService
    {
        private readonly IContentRepository _repo;
        IDirectMemoryCache _contCache;
        private readonly string _adsCacheKey = "ads";
        public ContentService(IContentRepository repo, IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _contCache = cacheManager.GetCache("__contcache");
        }
        #region  增删改方法 暂时没用到
        public long Create(ContentModel inputModel)
        {
            var entity = inputModel.MapTo<ContentEntity>();
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            if (body > 0)
                RebuildCache();

            return body;

        }
        #endregion

        #region 全部内存缓存数据
        public async Task<Result<List<AdModel>>> GetAll()
        {

            try
            {
                var imgSvr = GetConfig("imgserver");
                #region  同步缓存
                var ads = _contCache.Get(_adsCacheKey, () =>
                {
                    var models = UnitOfWorkService.Execute(() => _repo.GetList(p => p.IsDelete == 0).MapTo<List<AdModel>>());
                    models.ForEach(ad =>
                    {
                        ad.Image = $"{imgSvr}{ad.Image}";
                        ad.SmallImage = $"{imgSvr}{ad.SmallImage}";
                    });
                    return models;
                }, TimeSpan.FromHours(8));
                #endregion
                return new Result<List<AdModel>> { Data = ads };
            }
            catch (Exception ex)
            {
                Logger.Error("GetAll", ex);
                var rlt = new Result<List<AdModel>>(ResultCode.Exception, ex.Message);
                return await Task.FromResult(rlt);
            }
        }
        #endregion

        #region  消息同步
        protected override void DoSync(IDomainEventData eventData)
        {
            //if (!(eventData is ContentEventData data)) return;
            //var method = (EventDataMethod)data.Method;
            //if (method == EventDataMethod.Clear)
            //{
            ClearCache();
            //  }
        }
        public void RebuildCache()
        {
            ClearCache();
            //Trigger(new ContentEventData() { Method = (int)EventDataMethod.Clear });
        }
        public override void ClearCache()
        {
            _contCache.Clear();
            //GetAll().GetAwaiter();
        }
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool addClearCache()
        {
            RebuildCache();
            return true;
        }

        #endregion

        #region 自定义扩展方法
        public async Task<Result<List<AdModel>>> GetAllAd(ContentType type)
        {

            try
            {
                var alllist = await GetAll();
                var adslist = alllist.Data.Where(p => p.Category == (int)type).OrderBy(t => t.Action).ToList();
                return new Result<List<AdModel>> { Data = adslist };
            }
            catch (Exception ex)
            {
                Logger.Error("GetAllAd", ex);
                var rlt = new Result<List<AdModel>>(ResultCode.Exception, ex.Message);
                return await Task.FromResult(rlt);
            }
        }
        public async Task<Result<List<AdModel>>> GetAdsByTag(ContentType type, string tag)
        {
            var adsRlt = await GetAllAd(type);
            var all = adsRlt.Data;
            var ads = all.Where(p => p.Tag.Equals(tag, StringComparison.OrdinalIgnoreCase)).OrderBy(t => t.Sort).ToList();
            return new Result<List<AdModel>> { Data = ads };
        }
        public async Task<Result<List<AdModel>>> GetAdsByAction(ContentType type, int action)
        {
            var adsRlt = await GetAllAd(type);
            var all = adsRlt.Data;
            var ads = all.Where(p => p.Action == action).ToList();
            return new Result<List<AdModel>> { Data = ads };
        }
        public async Task<Result<AdModel>> GetAdByTag(ContentType type, string tag)
        {
            var adsRlt = await GetAllAd(type);
            var all = adsRlt.Data;
            var ad = all.FirstOrDefault(p => p.Tag.Equals(tag, StringComparison.OrdinalIgnoreCase));
            return new Result<AdModel> { Data = ad };
        }


        #endregion

    }
}
