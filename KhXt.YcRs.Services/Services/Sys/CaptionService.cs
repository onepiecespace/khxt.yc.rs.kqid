﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.Models.Sys;
using KhXt.YcRs.Domain.Repositories.Sys;
using KhXt.YcRs.Domain.Services;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services
{
    public class CaptionService : ServiceBase, ICaptionService
    {
        private readonly ICaptionRepository _repo;
        private IDirectMemoryCache _cache;
        private readonly string _captionsCacheKey = "captions";
        private readonly string _levelCaptionsCacheKey = "levelcaptions";
        private const string IMG_EXT = ".png";

        public CaptionService(ICaptionRepository repo, IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _cache = cacheManager.GetCache("__captionCache");
        }
        /// <summary>
        /// 获取所有
        /// </summary>
        /// <returns></returns>
        public List<CaptionModel> GetCaptions()
        {
            var captions = _cache.Get<string, List<CaptionModel>>(_captionsCacheKey, () =>
            {
                var es = UnitOfWorkService.Execute(() => _repo.GetList(p => p.CaptionType == (int)CaptionType.Ranking).ToList());

                var list = es.MapTo<List<CaptionModel>>();
                list.ForEach(m =>
                {
                    m.Image = $"{DomainSetting.ResourceUrl}{m.Image}{m.Ranking}{IMG_EXT}";
                });
                return list;
            }, TimeSpan.FromHours(24));
            return captions;
        }
        public Result<List<CaptionModel>> GetCaptions(BusinessType businessType)
        {
            var captions = GetCaptions();
            var items = captions.Where(e => e.Category == (int)businessType).ToList();
            //Expression<Func<CaptionEntity, bool>> exp = e => e.Category == (int)businessType;
            //var items = UnitOfWorkService.Execute(() => _repo.GetList(exp).ToList());
            return new Result<List<CaptionModel>> { Data = items.MapTo<List<CaptionModel>>() };
        }

        public List<LevelCaptionModel> GetLevelCaptions()
        {
            var captions = _cache.Get(_levelCaptionsCacheKey, () =>
            {
                var es = UnitOfWorkService.Execute(() => _repo.GetList(p => p.CaptionType == (int)CaptionType.Level).OrderByDescending(p => p.Threshold).ToList());
                var list = es.MapTo<List<LevelCaptionModel>>();
                list.ForEach(m =>
                {
                    m.Image = $"{DomainSetting.ResourceUrl}{m.Image}{m.Ranking}{IMG_EXT}";
                });
                return list;
            }, TimeSpan.FromHours(24));
            return captions;
        }

        public Result<List<LevelCaptionModel>> GetLevelCaptions(BusinessType businessType)
        {
            var captions = GetLevelCaptions();
            var items = captions.Where(e => e.Category == (int)businessType).ToList();
            //Expression<Func<CaptionEntity, bool>> exp = e => e.Category == (int)businessType;
            //var items = UnitOfWorkService.Execute(() => _repo.GetList(exp).ToList());
            return new Result<List<LevelCaptionModel>> { Data = items.MapTo<List<LevelCaptionModel>>() };
        }
        public async Task<int> GetLevel(int businessType, long exp)
        {
            var captions = _cache.Get("levelcaptions", () =>
            {
                var es = UnitOfWorkService.Execute(() => _repo.GetList(p => p.CaptionType == (int)CaptionType.Level).OrderByDescending(p => p.Threshold).ToList());
                var list = es.MapTo<List<LevelCaptionModel>>();
                list.ForEach(m =>
                {
                    m.Image = $"{DomainSetting.ResourceUrl}{m.Image}{m.Ranking}{IMG_EXT}";
                });
                return list;
            }, TimeSpan.FromHours(24));

            var items = captions.Where(e => e.Category == businessType && e.Threshold <= exp).ToList();
            return await Task.Run(() =>
            {
                var result = items.Count > 0
                    ? items[0].Ranking
                    : 1;
                return result;
            });
        }
        protected override void DoSync(IDomainEventData eventData)
        {
            // if (!(eventData is CaptionEventData data)) return;
            //var method = (EventDataMethod)data.Method;
            //if (method == EventDataMethod.Clear)
            //{
            _cache.Clear();
            GetCaptions();
            GetLevelCaptions();
            // }
        }
        public override void ClearCache()
        {
            _cache.Clear();
            //Trigger(new CaptionEventData() { Method = (int)EventDataMethod.Clear });
            GetCaptions();
            GetLevelCaptions();
        }

        public CaptionModel GetCaptionsByRankingId(BusinessType businessType, int rankingId)
        {
            var caps = GetCaptions(businessType).Data;
            return caps.FirstOrDefault(g => (g.Category == (int)businessType) && g.Ranking == rankingId);
        }

        public LevelCaptionModel GetLevelCaptionsByLevel(BusinessType businessType, int level)
        {
            var caps = GetLevelCaptions(businessType).Data;
            return caps.FirstOrDefault(g => (g.Category == (int)businessType) && g.Ranking == level);
        }


    }


}
