﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Repositories.Sys;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Sys;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Sys
{
    public class VersionService : ServiceBase, IVersionService
    {
        IVersionRepository _repo;
        private IKVStoreService _keyValue;

        private readonly IDirectMemoryCache _cache;
        private readonly string _versionCacheKey = "Version";
        public VersionService(IVersionRepository repo, IKVStoreService keyValue, IDirectMemoryCacheManager cacheManager)
        {
            _keyValue = keyValue;
            _repo = repo;
            _cache = cacheManager.GetCache("__VersionCache");
        }
        #region 复用缓存查询方法
        public async Task<List<VersionLog>> GetAll()
        {
            try
            {
                var rlt = _cache.Get(_versionCacheKey, async () =>
                {
                    var body = await UnitOfWorkService.Execute(async () =>
                    {
                        var list = await _repo.GetListAsync();
                        var taskList = list.MapTo<List<VersionLog>>();
                        return taskList;
                    });
                    return body;
                }, TimeSpan.FromHours(8));

                return await rlt;
            }
            catch (Exception ex)
            {
                Logger.Error("版本列表缓存异常", ex);
                return null;
            }

        }

        #endregion
        #region  DoSync增删修改缓存

        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is VersionEventData data)) return;
            var method = (EventDataMethod)data.Method;
            if (method == EventDataMethod.Clear)
            {
                ClearCache();
            }
        }
        public void RebuildCache()
        {
            ClearCache();
            //Trigger(new VersionEventData() { Method = (int)EventDataMethod.Clear });
        }
        public override void ClearCache()
        {
            _cache.Clear();
            GetAll().GetAwaiter().GetResult();
        }

        public long Add(VersionLog Info)
        {
            var entity = Info.MapTo<VersionEntity>();
            if (entity.OsSign == 1)
            {
                entity.ApkUrl = "";
                entity.OS = 1;
                entity.Build = int.Parse(Info.VersionCode);
            }
            else if (entity.OsSign == 0)
            {
                entity.OS = 2;
                entity.Build = Info.ParentId;
            }
            entity.CreateTime = DateTime.Now;
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            if (body > 0) RebuildCache();
            return body;
        }

        public bool Update(VersionLog Info)
        {
            var entity = Info.MapTo<VersionEntity>();
            entity.CreateTime = DateTime.Now;
            if (entity.OsSign == 1)
            {
                entity.ApkUrl = "";
                entity.OS = 1;
                entity.Build = int.Parse(Info.VersionCode);
            }
            else if (entity.OsSign == 0)
            {
                entity.OS = 2;
                entity.Build = Info.ParentId;
            }
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var tempEntity = _repo.Get(Info.Id);
                if (tempEntity != null)
                {
                    return _repo.Update(entity);
                }
                else
                {
                    return false;
                }

            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(VersionLog Info)
        {
            var entity = Info.MapTo<VersionEntity>();
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var tempEntity = _repo.Get(Info.Id);
                if (tempEntity != null)
                {
                    return _repo.Delete(entity);
                }
                else
                {
                    return false;
                }
            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool VersionClearCache()
        {
            RebuildCache();
            return true;
        }
        #endregion
        #region 自定义查询方法
        public async Task<Result<VersionLog>> Get(long id)
        {
            try
            {
                var list = await GetAll();
                var info = list.Find(t => t.Id == id);
                return await Task.FromResult(new Result<VersionLog>() { Data = info });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取版本{id}异常！", exception);
                return new Result<VersionLog> { Code = (int)ResultCode.Exception, Message = $"获取版本异常" };
            }
        }
        public async Task<Result<VersionLog>> GetVersionCode(string Version, string Aid)
        {
            try
            {
                var list = await GetAll();
                var info = list.Find(t => t.VersionCode == Version && t.Aid == Aid);
                return await Task.FromResult(new Result<VersionLog>() { Data = info });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取版本{Version}异常！", exception);
                return new Result<VersionLog> { Code = (int)ResultCode.Exception, Message = $"获取版本异常" };
            }
        }

        /// <summary>
        /// 获取版本信息
        /// </summary>
        /// <param name="osSign"></param>
        /// <returns></returns>
        public async Task<Result<VersionInfo>> GetVersionInfo(int osSign, string aid = "college", int currentVersion = 1)
        {
            try
            {
                var code = "0";
                int version = 0;
                string downData = "";
                var list = await GetAll();
                List<VersionLog> newlist = new List<VersionLog>();

                VersionLog infoCode = new VersionLog();
                switch (osSign)
                {
                    case 0://andriod
                        newlist = list.Where(n => n.OsSign == 0 && n.Aid == aid).OrderByDescending(n => n.ParentId).ToList();
                        break;
                    case 1://ios
                        newlist = list.Where(n => n.OsSign == 1).OrderByDescending(n => n.Build).ToList();
                        break;
                    default:
                        break;
                }
                infoCode = newlist.FirstOrDefault();
                if (infoCode != null)
                {
                    code = infoCode.VersionCode;
                    if (osSign == 0)
                    {
                        version = infoCode.ParentId;
                        downData = DomainSetting.ResourceUrl + infoCode.ApkUrl;
                    }
                    else
                    {
                        version = int.Parse(code);
                    }
                    #region  暂时没加判断
                    //如currentVersion == XXX,则forceUpdate = true;
                    //如果currentVersion < newVersion,则isUpdate = true；
                    //如果currentVersion < minVersion,则forceUpdate = true；
                    //如果currentVersion >= minVersion,则forceUpdate = false;
                    //如果currentVersion == newVersion，则isUpdate = false.
                    #endregion

                    var logInfo = newlist.Where(n => n.VersionCode == code).OrderByDescending(n => n.Id).ToList();
                    List<string> strList = new List<string>();
                    strList.Add(infoCode.Content);
                    var info = new VersionInfo()
                    {
                        Id = infoCode == null ? 100 : infoCode.Id,
                        NewVersionCode = code,
                        ParentId = infoCode.ParentId,
                        NewVersionDownLoadUrl = downData,
                        Aid = infoCode.Aid,
                        MinVersion = infoCode.MinVersion,
                        IsUpdate = infoCode.IsUpdate,
                        ForceUpdate = infoCode.ForceUpdate,
                        VersionLogListToStr = string.Join(@"\n", strList),
                        VersionLogList = logInfo.MapTo<List<VersionLog>>(),
                    };
                    return await Task.FromResult(new Result<VersionInfo>() { Data = info });
                }
                // downData = osSign == 1 ? "" : _keyValue.Table().FirstOrDefault(n => n.TenantId == Consts.DOWNLOADURL_TENANTID)?.ValueData ?? "";
                else
                {
                    return await Task.FromResult(new Result<VersionInfo>() { Code = (int)ResultCode.warning, Message = $"为获取到获取版本" });
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取版本{osSign}异常！", exception);
                return new Result<VersionInfo> { Code = (int)ResultCode.Exception, Message = $"获取版本异常" };
            }
        }

        #endregion
    }
}
