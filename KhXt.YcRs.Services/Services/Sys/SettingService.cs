﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Home;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Sys;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Sys
{
    public class SettingService : ServiceBase, ISettingService
    {
        ICourseCateoryService _courseCateoryService;
        public SettingService(ICourseCateoryService courseCateoryService)
        {
            _courseCateoryService = courseCateoryService;
        }

        public async Task<string> SetHomeCourse(long id, int mode, int sort, int picDirect, long[] values)
        {
            var key = RedisKeyHelper.SETTING_HOME_COURSE;
            string caption;
            switch (id)
            {
                case -1:
                    caption = "热门课程";
                    sort = 1;
                    break;
                case -2:
                    caption = "直播课";
                    sort = 2;
                    break;
                case -3:
                    caption = "推荐课程";
                    sort = 3;
                    break;
                default:
                    var ct = await _courseCateoryService.Get(id);
                    if (ct == null) return $"course cateory :{id } not exists";
                    caption = ct.Name;
                    break;
            }

            var model = new HomeCourseSettingModel { Id = id, Caption = caption, Mode = mode, Sort = sort, PictureDirect = picDirect, Values = values };
            var rlt = await Redis.HashSetWithTypeAsync(key, id.ToString(), model);
            return rlt ? "Ok" : "Fail";
        }

        public async Task<List<HomeCourseSettingModel>> GetHomeCourseSetting()
        {
            var key = RedisKeyHelper.SETTING_HOME_COURSE;
            var models = await Redis.HashGetDictionaryWithTypeAsync<HomeCourseSettingModel>(key);
            if (models == null) return null;
            return models.Values.OrderBy(v => v.Sort).ToList();
        }
        public async Task<List<LevelCourseSettingModel>> GetLevelCourseSetting()
        {
            var key = RedisKeyHelper.SETTING_LEVEL_COURSE;
            var models = await Redis.HashGetDictionaryWithTypeAsync<LevelCourseSettingModel>(key);
            if (models == null) return null;
            return models.Values.OrderBy(v => v.Sort).ToList();
        }
        public async Task<List<LevelCourseListSettingModel>> GetLevelCourselistSetting()
        {
            var key = RedisKeyHelper.SETTING_LEVEL_COURSE;
            var models = await Redis.HashGetDictionaryWithTypeAsync<LevelCourseListSettingModel>(key);
            if (models == null) return null;
            return models.Values.OrderBy(v => v.Sort).ToList();
        }

        public async Task RemoveHomeCourseSetting(long id)
        {
            var key = RedisKeyHelper.SETTING_HOME_COURSE;
            Redis.HashRemove(key, id.ToString());
            await Task.CompletedTask;
        }
    }
}