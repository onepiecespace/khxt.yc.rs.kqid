﻿using AutoMapper.Configuration;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Contracts;
using KhXt.YcRs.Domain.Models.Sys;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.SmsOptions;
using KhXt.YcRs.Domain.Util;
using KhXt.YcRs.Infrastructure;
using KhXt.YcRs.Infrastructure.Config;
using KhXt.YcRs.Infrastructure.Sms;
using KhXt.YcRs.Infrastructure.Sms.Config;
using Hx.Configurations.Application;
using Hx.Extensions;
using Hx.MQ;
using Microsoft.Extensions.Options;
using NPOI.OpenXmlFormats.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services
{
    public class SmsService : ServiceBase, ISmsService
    {
        HttpClient _httpClient;
        SmsSetting _smsSetting;
        string _preParas;
        string _validTestCodeFormat = "您好！您的验证码是：{0}【马小哈测试】";
        string _validCodeFormat = "您好！您的验证码是：{0}【{1}】";
        IUserCacheService _userCacheService;
        Microsoft.Extensions.Configuration.IConfiguration _configuration;
        private readonly SmsSettings _smsSettings;
        public SmsService(HxAppSettings appSettings, IUserCacheService userCacheService,
            Microsoft.Extensions.Configuration.IConfiguration configuration, IOptions<SmsSettings> smsSettings)
        {
            _userCacheService = userCacheService;
            Init(appSettings);
            _configuration = configuration;
            _smsSettings = smsSettings.Value;
        }

        private void Init(HxAppSettings appSetting)
        {
            if (!appSetting.Parameters.ContainsKey("sms")) return;
            var setting = appSetting.Parameters["sms"].Split(",");
            _smsSetting = new SmsSetting
            {
                Url = setting[0].StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? setting[0] : "http://" + setting[0],
                UserName = setting[1],
                Password = Hx.Security.Md5Getter.Get(setting[2]),
                ApiKey = setting[3]
            };
            if (setting.Length > 4)
            {
                _smsSetting.SpeedThreshold = Convert.ToInt32(setting[4]);
            }
            if (setting.Length > 5)
            {
                _smsSetting.SpeedDuring = Convert.ToInt32(setting[5]);
            }
            _preParas = $"username={_smsSetting.UserName}&password_md5={_smsSetting.Password}&apikey={_smsSetting.ApiKey}&encode={_smsSetting.Encoding}";

            var handler = new HttpClientHandler();
            _httpClient = new HttpClient(handler) { BaseAddress = new Uri(_smsSetting.Url) };
        }

        bool SafeCheck()
        {
            var cv = Redis.StringIncrementEx(RedisKeyHelper.SMS_SPEED_LIMIT, _smsSetting.SpeedDuring);

            return cv < _smsSetting.SpeedThreshold;
        }

        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="phone">手机号</param>
        /// <param name="aid">APP终端标识</param>
        /// <param name="bizType">业务类型，1，2：普通发送验证码类型，可扩展3...N</param>
        /// <param name="content">发送内容（json）</param>
        /// <returns></returns>
        public async Task<Result> SendValidCode(string phone, string aid, string bizType = "1", string content = "")
        {
            var result = new Result();
            try
            {
                return await SendValidCodeV2(phone, aid, bizType, "");
            }
            catch (Exception ex)
            {
                Logger.Error("发送短信异常", ex);
                return new Result { Code = (int)ResultCode.Exception, Message = "发送失败" };
            }
        }

        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="phone">手机号码</param>
        /// <param name="aid">平台标识</param>
        /// <param name="biztype">短信业务类型：1:获取验证码，2:找回密码 等</param>
        /// <param name="content">短信内容</param>
        /// <returns></returns>
        public async Task<Result> SendValidCodeV2(string phone, string aid, string bizType, string content)
        {
            var result = new Result();
            try
            {

                //是否是非验证电话号
                var isCrossValidPhone = _smsSettings.Limit.MobileWhiteList.Contains(phone);
                Logger.Info($"----{phone},发送短信开始----");
                //短信过期时间：秒
                var smsConfigExpireSecondValue = ConfigurationHelper.Config[SmsConfigKeys.SMS_Ali_ExpireSeconds];
                var smsExpireSeconds = Convert.ToInt32(string.IsNullOrWhiteSpace(smsConfigExpireSecondValue) ? "60" : smsConfigExpireSecondValue);
                var smsCodeExpireTime = TimeSpan.FromSeconds(smsExpireSeconds);


                if (!Hx.Utilities.RegexHelper.IsMobile(phone) && !phone.StartsWith("168"))
                {
                    Logger.Info("发送短信失败：错误的手机号码");
                    return new Result(ResultCode.Fail, "错误的手机号码");
                }

                //手机号日发送频率限制判断规则
                var isValidMolileSendRequest = await IsCheckMobileSendValid(phone);
                if (!isValidMolileSendRequest)
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = _smsSettings.Limit.OverDaySendMessage;
                    Logger.Info($"----{phone},发送短信结束----当前手机号到达日发送上限");
                    return result;
                }

                var vc = Redis.StringGet(RedisKeyHelper.CreateSmsKey(phone));
                if (vc.IsNotNullOrEmpty())
                {
                    result.Message = "发送成功";
                    result.Data = vc;
                    Logger.Info($"----{phone},发送短信结束----短信有效期还未失效");
                    return result;
                }
                var smsCode = RandomHelper.GenerateRandomCode(6);

                if (isCrossValidPhone || phone.StartsWith("168")) //内部测试用户，不用实际发，直接固定123456
                {
                    smsCode = "123456";
                    Redis.StringSet(RedisKeyHelper.CreateSmsKey(phone), smsCode, smsCodeExpireTime);
                    result.Message = "发送成功";
                    result.Data = smsCode;
                    Logger.Info($"----{phone},发送短信结束----");
                    return result;
                }
                //发送短信
                var sendResultData = BeginSendSms(phone, content, aid, bizType, smsCode);
                if (sendResultData.Code != (int)ResultCode.Success)
                {
                    Logger.Info($"----{phone},发送短信结束----{Newtonsoft.Json.JsonConvert.SerializeObject(sendResultData)}");
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "发送失败";
                    return result;
                }

                //记录手机号监控日志
                MarkMobileSendMonitor(phone);
                result.Code = (int)ResultCode.Success;
                result.Message = "发送成功";
                result.Data = smsCode;
                Redis.StringSet(RedisKeyHelper.CreateSmsKey(phone), smsCode, smsCodeExpireTime);
                Logger.Info($"----{phone},发送短信结束----");
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("发送短信异常:", ex);
                return new Result { Code = (int)ResultCode.Exception, Message = "发送失败" };
            }
        }


        public async Task<Result> SendValidCodeTest(string phone)
        {
            var result = new Result();

            if (!Hx.Utilities.RegexHelper.IsMobile(phone) && !phone.StartsWith("168")) //DJD:168开头为临时压测用，之后删除
            {
                return new Result(ResultCode.Fail, "错误的手机号码");
            }
            var num = RandomHelper.GenerateRandomCode(6); //Hx.Util.GetRandomNumberString(6);

            if (phone == "17710681482") //DJD:压测、内部测试用户，不用实际发，直接固定123456 可以设置白名单控制具体的开发部验证码
            {
                num = "123456";
                Redis.StringSet(RedisKeyHelper.CreateSmsKey(phone), num, TimeSpan.FromSeconds(60 * 5));
                result.Message = "发送成功";
                result.Data = num;
                return result;
            }
            var content = string.Format(_validCodeFormat, num);

            var sms = new SmsContract
            {
                Sn = phone,
                Content = content,
                SubmitTime = DateTime.Now,
            };
            try
            {
                sms.ReceiptTime = DateTime.Now;
                result.Message = "发送成功";
                result.Data = num;
                sms.Status = 1;

                Redis.StringSet(RedisKeyHelper.CreateSmsKey(phone), num, TimeSpan.FromSeconds(60 * 5));

                await sms.Publish();

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("Sms", ex);
                return new Result { Code = (int)ResultCode.Exception, Message = "发送失败" };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public async Task<Result> SendContentTest(string phone, string contentstr)
        {
            var result = new Result();

            if (!Hx.Utilities.RegexHelper.IsMobile(phone) && !phone.StartsWith("168")) //DJD:168开头为临时压测用，之后删除
            {
                return new Result(ResultCode.Fail, "错误的手机号码");
            }
            var num = RandomHelper.GenerateRandomCode(6); //Hx.Util.GetRandomNumberString(6);
            if (phone == "18210413749")
            {
                num = contentstr;
            }
            var content = string.Format(_validCodeFormat, num);

            var sms = new SmsContract
            {
                Sn = phone,
                Content = content,
                SubmitTime = DateTime.Now,
            };
            try
            {
                var rlt = await SendSms(phone, content);
                sms.ReceiptTime = DateTime.Now;
                result.Message = "发送成功";
                result.Data = num;
                sms.Status = 1;

                Redis.StringSet(RedisKeyHelper.CreateSmsKey(phone), num, TimeSpan.FromSeconds(60 * 5));

                await sms.Publish();

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("Sms", ex);
                return new Result { Code = (int)ResultCode.Exception, Message = "发送失败" };
            }
        }
        /// <summary>
        /// 获取短信验证码
        /// </summary>
        /// <param name="sn"></param>
        /// <param name="content"></param>
        /// <param name="bizType">短信业务类型：1:普通获取，客户端获取短信业务类型</param>
        /// <returns></returns>
        private async Task<Tuple<bool, string>> SendSms(string sn, string content)
        {

            //todo:短信轰炸SMS：临时过渡限定判断逻辑
            var isValidMolileSendRequest = await IsCheckMobileSendValid(sn);
            if (!isValidMolileSendRequest)
            {
                Logger.Info("手机请求被拦截");
                var unValidMessage = "您当日的短信发送次数已达上限，请稍后再试";
                return new Tuple<bool, string>(true, unValidMessage);
            }

            var nameValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username",_smsSetting.UserName),
                new KeyValuePair<string, string>("password_md5",_smsSetting.Password),
                new KeyValuePair<string, string>("apikey",_smsSetting.ApiKey),
                new KeyValuePair<string, string>("encode",_smsSetting.Encoding),
                new KeyValuePair<string, string>("mobile",sn),
                new KeyValuePair<string, string>("content",content)
            };

            var req = new HttpRequestMessage(HttpMethod.Post, "");
            req.Content = new FormUrlEncodedContent(nameValues);
            var rep = await _httpClient.SendAsync(req);
            var reqSucc = rep.StatusCode == HttpStatusCode.OK;
            var rlt = reqSucc ? await rep.Content.ReadAsStringAsync() : $"短信服务访问出错,StatusCode:{rep.StatusCode}";
            //临时记录手机号监控日志
            MarkMobileSendMonitor(sn);

            return new Tuple<bool, string>(reqSucc, rlt);
        }


        /// <summary>
        /// 短信业务类型：1:普通获取，客户端获取短信业务类型
        /// </summary>
        /// <param name="sn"></param>
        /// <param name="content"></param>
        /// <param name="aid"></param>
        /// <param name="biztype"></param>
        /// <param name="smsCode"></param>
        /// <returns></returns>
        private Result BeginSendSms(string sn, string content, string aid, string biztype, string smsCode)
        {
            var dataResult = new Result();
            dataResult.Code = (int)ResultCode.Success;
            dataResult.Message = "短信发送成功";
            try
            {
                //aid = "huangli";//college也用黄鹂
                #region 发送短信入口
                //todo:新版阿里云发送短信入口（阿里大于短信,后期扩展策略切换按需切换备用通道）
                var resultData = new AliSmsSender().sendSms(new AliSmsData()
                {
                    phoneNo = sn,
                    signName = EnumHelper.GetDescription<SmsPlatform>(aid),//aid
                    bizType = biztype,
                    templateParams = content,
                    code = smsCode
                });
                #endregion

                if (resultData == null || resultData.Code != (int)ResultCode.Success)
                {
                    dataResult.Code = (int)ResultCode.Fail;
                    dataResult.Message = "短信发送失败";
                    Logger.Error($"发送短信异常或失败,原因:{resultData.Message}");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("发送短信异常:", ex);
                dataResult.Code = (int)ResultCode.Fail;
                dataResult.Message = "短信发送失败";
            }
            return dataResult;
        }

        private async Task<bool> IsCheckMobileSendValid(string mobile)
        {
            int limitSendCount = 2;
            //已注册用户增大条数
            var info = await _userCacheService.GetUserByPhone(mobile, true);
            if (info != null && info.Userid > 0)
            {
                if (_smsSettings.Limit.MobileWhiteList.Contains(info.Phone))
                {
                    limitSendCount = 50;
                }
                else
                {
                    limitSendCount = _smsSettings.Limit.AlreadyRegisterUserSendLimitCount;
                }
            }
            var mobileCacheKey = RedisKeyHelper.CreateMobileMonitorLogKey(mobile);
            var mobileCacheKeyValue = Redis.StringGet(mobileCacheKey);
            if (string.IsNullOrWhiteSpace(mobileCacheKeyValue))
            {
                MarkMobileSendMonitor(mobile);
                return true;
            }
            else
            {
                if (Convert.ToInt32(mobileCacheKeyValue) >= limitSendCount)
                {
                    return false;
                }
            }
            return true;
        }

        private void MarkMobileSendMonitor(string mobile)
        {
            var mobileCacheKey = RedisKeyHelper.CreateMobileMonitorLogKey(mobile);
            var mobileCurrentSendCount = String.IsNullOrWhiteSpace(Redis.StringGet(mobileCacheKey)) ? 0 : Convert.ToInt32(Redis.StringGet(mobileCacheKey));

            //if (mobileCurrentSendCount == 0)
            //{
            Redis.StringSet(mobileCacheKey, (++mobileCurrentSendCount).ToString(), TimeSpan.FromDays(1));
            //}
            //else
            //{
            //    Redis.StringSet(mobileCacheKey, (++mobileCurrentSendCount).ToString());
            //}
        }
    }
}