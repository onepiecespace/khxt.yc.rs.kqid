﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Repositories.Sys;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Sys;
using KhXt.YcRs.Domain.Services.User;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Services.Sys
{
    public class User_ChannelService : ServiceBase, IUser_ChannelService
    {
        IUser_ChannelRepository _repo;

        public User_ChannelService(IUser_ChannelRepository repo)
        {
            _repo = repo;
        }

        #region IComBaseService
        public IQueryable<User_ChannelEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<User_ChannelEntity>>(() =>
               {
                   return _repo.GetList().AsQueryable();
               });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(User_ChannelEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }



        public void Delete(User_ChannelEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(entity);
            });
        }

        public void Delete(Expression<Func<User_ChannelEntity, bool>> predicate)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(predicate);
            });
        }



        public IQueryable<User_ChannelEntity> Fetch(Expression<Func<User_ChannelEntity, bool>> predicate, Action<Orderable<User_ChannelEntity>> order)
        {
            var body = UnitOfWorkService.Execute<IQueryable<User_ChannelEntity>>(() =>
           {
               if (order == null)
               {
                   return Table().Where(predicate);
               }
               var deferrable = new Orderable<User_ChannelEntity>(Table().Where(predicate));
               order(deferrable);
               return deferrable.Queryable;

           });
            return body;

        }

        public User_ChannelEntity Get(object id)
        {
            var body = UnitOfWorkService.Execute<User_ChannelEntity>(() =>
           {
               long.TryParse(id.ToString(), out long nid);
               return _repo.Get(nid);

           });
            return body;

        }


        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList<User_ChannelEntity> Gets(Expression<Func<User_ChannelEntity, bool>> predicate, Action<Orderable<User_ChannelEntity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList<User_ChannelEntity>>(() =>
           {
               if (order == null)
               {
                   return new PageList<User_ChannelEntity>(Table().Where(predicate), pageIndex, pageSize);
               }
               var deferrable = new Orderable<User_ChannelEntity>(Table().Where(predicate));
               order(deferrable);
               var ts = deferrable.Queryable;
               var totalCount = ts.Count();
               var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

               return new PageList<User_ChannelEntity>(pagingTs, pageIndex, pageSize, totalCount);

           });
            return body;
        }

        public void Update(User_ChannelEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }
        #endregion

        #region 自定义方法

        #endregion
    }









}
