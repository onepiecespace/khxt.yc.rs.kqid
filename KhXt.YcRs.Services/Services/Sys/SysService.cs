﻿using Autofac.Core;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.EventData.Sys;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.Pay;
using KhXt.YcRs.Domain.Models.Sys;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Repositories.Order;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.Files;
using KhXt.YcRs.Domain.Services.Sys;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx.Components;
using Hx.Components.Autofac;
using Hx.Extensions;
using Hx.ObjectMapping;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Sys
{
    public partial class SysService : ServiceBase, ISysService
    {
        private readonly IUserCacheService _userCacheService;
        private readonly IOrderPayRepository _orderPayRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly ICourseService _courseService;
        private readonly ICourseOrderService _courseOrderService;
        private readonly IFilesService _filesService;
        private readonly string _aid;
        public SysService(IUserCacheService userCacheService,
            IOrderPayRepository orderPayRepository,
            IOrderItemRepository orderItemRepository,
            ICourseService courseService,
            ICourseOrderService courseOrderService,
            IFilesService filesService)
        {
            _userCacheService = userCacheService;
            _orderPayRepository = orderPayRepository;
            _orderItemRepository = orderItemRepository;
            _courseService = courseService;
            _courseOrderService = courseOrderService;
            _filesService = filesService;
        }
        public async Task<Result<UserHourChars>> GetHourChar(string today, long userId)
        {
            var lrt = await _userCacheService.GetAid(userId);
            var dt = today.IsNullOrEmpty() ? DateTime.Now : DateTime.Parse(today);
            var alllist = await _userCacheService.GetUserList();
            //alllist = alllist.FindAll(t => t.Aid == lrt.Data);
            alllist = alllist.FindAll(t => t.AccountType <= 1);
            var allTotalCount = alllist.Count();
            var users = alllist.Where(t => t.Creatime.ToString("yyyy-MM-dd") == dt.ToString("yyyy-MM-dd")).ToList();
            var totalCount = 0;
            var chars = users.GroupBy(x => new { x.Creatime.Hour }).Select(group => new UserHourChar()
            {
                Hour = group.Key.Hour,
                Count = group.Count()
            }).ToList();
            for (var i = 0; i < 24; i++)
            {
                if (!chars.Exists(t => t.Hour.Equals(i)))
                    chars.Add(new UserHourChar() { Count = 0, Hour = i });
            }
            chars.ForEach(t =>
            {
                totalCount += t.Count;
            });

            chars = chars.OrderBy(t => t.Hour).ToList();
            return await Task.Run(() => new Result<UserHourChars>() { Data = new UserHourChars() { Chars = chars, TotalCount = totalCount, AllTotalCount = allTotalCount } });
        }

        public async Task<Result<UserWeekChars>> GetWeekChar(long userId)
        {
            var dt = DateTime.Now;
            var lrt = await _userCacheService.GetAid(userId);
            var alllist = await _userCacheService.GetUserList();
            //alllist = alllist.FindAll(t => t.Aid == lrt.Data);
            alllist = alllist.FindAll(t => t.AccountType <= 1);
            var allTotalCount = alllist.Count();
            var users = alllist.Where(t => t.Creatime <= dt && t.Creatime >= dt.AddDays(-6)).ToList();

            var totalCount = 0;
            var chars = users.GroupBy(x => new { x.CreateTimeStr }).Select(group => new UserWeekChar()
            {
                Day = group.Key.CreateTimeStr,
                Count = group.Count()
            }).ToList();
            for (var i = 6; i > -1; i--)
            {
                var time = dt.AddDays(-i).ToString("yyyy-MM-dd");
                if (!chars.Exists(t => t.Day.Equals(time)))
                    chars.Add(new UserWeekChar() { Count = 0, Day = time });
                else
                    chars.Find(t => t.Day.Equals(time)).Day = time;
            }
            chars.ForEach(t =>
            {
                totalCount += t.Count;
            });
            chars = chars.OrderBy(t => t.Day).ToList();
            return await Task.Run(() => new Result<UserWeekChars>() { Data = new UserWeekChars() { Chars = chars, TotalCount = totalCount } });
        }

        public async Task<Result<UserYearChars>> GetYearChar(long userId)
        {
            var lrt = await _userCacheService.GetAid(userId);
            var alllist = await _userCacheService.GetUserList();
            //alllist = alllist.FindAll(t => t.Aid == lrt.Data);
            alllist = alllist.FindAll(t => t.AccountType <= 1);
            var allTotalCount = alllist.Count();
            var dt = DateTime.Now;
            var list = alllist.Where(t => t.Creatime <= dt && t.Creatime >= dt.AddYears(-1)).ToList();
            var users = list.MapTo<List<UserCharInfo>>();
            var totalCount = 0;
            var chars = users.GroupBy(x => new { x.Creatime.Month }).Select(group => new UserYearChar()
            {
                Mouth = group.Key.Month,
                Count = group.Count()
            }).ToList();
            for (var i = 11; i > -1; i--)
            {
                var time = dt.AddMonths(-i);
                if (!chars.Exists(t => t.Mouth.Equals(time.Month)))
                    chars.Add(new UserYearChar() { Count = 0, Mouth = time.Month, Time = time.ToString("yyyy-MM") });
                else
                    chars.Find(t => t.Mouth.Equals(time.Month)).Time = time.ToString("yyyy-MM");
            }
            chars.ForEach(t =>
            {
                totalCount += t.Count;
            });
            chars = chars.OrderBy(t => t.Time).ToList();
            return await Task.Run(() => new Result<UserYearChars>() { Data = new UserYearChars() { Chars = chars, TotalCount = totalCount } });
        }

        public async Task<Result<PayChars>> GetPayChars(long userId, int category, string startTime = "", string endTime = "")
        {
            try
            {
                var lrt = await _userCacheService.GetAid(userId);
                var result = new PayChars();
                var payList = await UnitOfWorkService.Execute(async () =>
                 {
                     var orders = await _orderPayRepository.GetListAsync(t => t.IsTest == false && t.IsSuccess == true && t.Aid == lrt.Data);
                     return orders.ToList();
                 });
                double allTotal = 0;
                foreach (var pay in payList)
                {
                    allTotal += double.Parse(pay.Price);
                }
                //Todo:获取课程统计

                double total = 0;
                var payDayChars = new List<PayDayChars>();
                var payWeekChars = new List<PayWeekChars>();
                var payYearChars = new List<PayYearChars>();
                var now = DateTime.Now;
                var dayList = payList.Where(t => t.CreateTime.ToString("yyyy-MM-dd") == now.ToString("yyyy-MM-dd")).ToList();
                foreach (var dayPayEntity in dayList)
                {
                    total += double.Parse(dayPayEntity.Price);
                }
                //今日
                if (category == 0)
                {
                    var chars = dayList.GroupBy(x => new { x.CreateTime.Hour }).Select(group => new PayDayChars()
                    {
                        Hour = group.Key.Hour,
                        Price = Math.Round(group.Sum(t => double.Parse(t.Price)), 2)
                    }).ToList();
                    for (var i = 0; i < 24; i++)
                    {
                        if (payDayChars.Exists(t => t.Hour == i))
                        {
                            var info = payDayChars.Find(t => t.Hour == i);
                            payDayChars.Add(info);
                        }
                        else
                        {
                            payDayChars.Add(new PayDayChars { Hour = i, Price = 0 });
                        }
                    }
                }
                else if (category == 1)//最近一周
                {
                    var payWeeks = payList.Where(t => t.CreateTime <= now && t.CreateTime >= now.AddDays(-6));
                    var tempList = payWeeks.MapTo<List<PayChar>>();
                    payWeekChars = tempList.GroupBy(x => new { x.Time }).Select(group => new PayWeekChars()
                    {
                        Time = group.Key.Time,
                        Price = Math.Round(group.Sum(t => t.Price), 2)
                    }).ToList();
                    for (var i = 6; i > -1; i--)
                    {
                        var time = now.AddDays(-i).ToString("yyyy-MM-dd");
                        if (!payWeekChars.Exists(t => t.Time.Equals(time)))
                            payWeekChars.Add(new PayWeekChars() { Price = 0, Time = time });
                    }
                    payWeekChars = payWeekChars.OrderBy(t => t.Time).ToList();
                }
                else
                {
                    var payYears = payList.Where(t => t.CreateTime <= now && t.CreateTime >= now.AddYears(-1));
                    var tempList = payYears.MapTo<List<PayChar>>();
                    payYearChars = tempList.GroupBy(x => new { x.CreateTime.Month }).Select(group => new PayYearChars()
                    {
                        Month = group.Key.Month,
                        Price = Math.Round(group.Sum(t => t.Price), 2)
                    }).ToList();
                    for (var i = 11; i > -1; i--)
                    {
                        var time = now.AddMonths(-i);
                        if (!payYearChars.Exists(t => t.Month.Equals(time.Month)))
                            payYearChars.Add(new PayYearChars() { Price = 0, Month = time.Month, Time = time.ToString("yyyy-MM") });
                        else
                            payYearChars.Find(t => t.Month.Equals(time.Month)).Time = time.ToString("yyyy-MM");
                    }
                    payYearChars = payYearChars.OrderBy(t => t.Time).ToList();

                }
                result.TotalCount = Math.Round(total, 2);
                result.AllTotalCount = Math.Round(allTotal, 2);
                result.PayDayChars = payDayChars;
                result.PayWeekChars = payWeekChars;
                result.PayYearChars = payYearChars;
                return new Result<PayChars> { Data = result };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("支付统计异常", ex);
                return new Result<PayChars>() { Code = (int)ResultCode.Exception, Message = "支付统计异常" };
            }
        }

        public async Task<List<Type>> GetAllServices()
        {
            var container = ((AutofacObjectContainerEx)ObjectContainer.Current).Container;
            var comps = container.ComponentRegistry.Registrations;
            var services = comps.Where(c => typeof(IService).IsAssignableFrom(c.Activator.LimitType));
            return await Task.FromResult(services.Select(c => ((TypedService)(c.Services.First())).ServiceType).ToList());
        }

        public async Task ClearAllServiceCache()
        {
            var services = await GetAllServices();
            services.ForEach(s =>
            {
                var service = ObjectContainer.Resolve(s) as IService;
                if (service != null)
                {
                    service.ClearCache();
                    Logger.Debug($"清理缓存：{s.Name}");
                }
                else
                {
                    Logger.Debug($"未找到服务实现：{s.Name},跳过清理缓存");
                }
            });
        }

        public async Task ClearServiceCache(Type serviceImplType)
        {
            if (serviceImplType == null)
            {
                return;
            }
            var services = await GetAllServices();
            var serviceType = services.FirstOrDefault(s => s == serviceImplType);


            if (serviceType != null)
            {
                var service = ObjectContainer.Resolve(serviceType) as IService;
                if (service != null)
                {
                    service.ClearCache();
                    Logger.Debug($"清理缓存：{serviceType.Name}");
                }
                else
                {
                    Logger.Debug($"未找到服务实现：{serviceType.Name},跳过清理缓存");

                }
            }
        }

        public async Task<Dictionary<string, long>> GetAllNodes()
        {
            var key = RedisKeyHelper.NODE_STATUS;
            var nodes = await Redis.HashGetDictionaryWithTypeAsync<long>(key);
            return nodes;
        }

        public async Task<Dictionary<string, long>> GetActiveNodes()
        {
            var nodes = await GetAllNodes();
            var ts = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var activeNodes = nodes.Where(n => ts - n.Value <= 120).ToDictionary(n => n.Key, n => n.Value);
            return activeNodes;
        }

        public async Task<Dictionary<string, long>> GetInActiveNodes()
        {
            var nodes = await GetAllNodes();
            var ts = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var activeNodes = nodes.Where(n => ts - n.Value > 120).ToDictionary(n => n.Key, n => n.Value);
            return activeNodes;
        }


        public async Task NoticeClearCache(string nodeName = "", Type ServiceType = null)
        {
            var eventData = new ClearCacheEventData { NodeName = nodeName, ServiceType = ServiceType };
            await TriggerAsync(eventData);

        }
        /// <summary>
        /// 清空马小哈接口api
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="ServiceType"></param>
        /// <returns></returns>
        public async Task NoticeMxhClearCache(string nodeName = "", Type ServiceType = null)
        {
            //var eventData = new ClearCacheEventData { NodeName = nodeName, ServiceType = ServiceType };
            //await TriggerAsync(eventData);
            var path = string.Format("/api/NoticeClearCache");
            if (!string.IsNullOrEmpty(nodeName))
            {
                path = string.Format("/api/NoticeClearCache?nodeName={0}", nodeName);
            }
            var rlt = await HttpPost<string>(path, null);
        }

        public async Task NoticeClearCache(string nodeName = "", int code = 0)
        {
            if (Consts.ServiceDictionary.TryGetValue(code, out Type serviceType))
            {
                await NoticeClearCache(nodeName, serviceType);
            }
        }
        /// <summary>
        /// 清空黄鹂接口api
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task NoticeMxhClearCache(string nodeName = "", int code = 0)
        {
            var path = string.Format("/api/NoticeClearCache?nodeName={0}&code={1}", nodeName, code);
            var rlt = await HttpPost<string>(path, null);
            //  await NoticeHLClearCache(nodeName, code);
            if (rlt == "false")
            {
                Logger.Info("同步课程缓存失败");
            }
        }

        public async Task<string> GetServiceDebugInfo(int serviceCode)
        {
            if (Consts.ServiceDictionary.TryGetValue(serviceCode, out var svrType))
            {
                return await GetServiceDebugInfo(svrType);
            }
            return "";
        }

        public async Task<string> GetServiceDebugInfo(Type serviceType)
        {
            var svr = ObjectContainer.Resolve(serviceType) as IService;
            if (svr == null) return "";
            return await svr.DebugAsync();
        }

        public async Task SaveVisit(List<KeyValuePair<string, long>> data)
        {
            var aggData = data.GroupBy(d => d.Key).Select(g => new GroupModel { Key = g.Key, Count = g.Count(), Sum = g.Sum(t => t.Value) }).ToList();
            var key = RedisKeyHelper.CreateVisitStatKey();
            foreach (var d in aggData)
            {
                var v = Calc(d.Count, d.Sum);
                await Redis.HashIncrementAsync(key, d.Key, v);
            }
        }


        public async Task<VisitStat> GetVisitStat(string path, DateTime? date = null)
        {
            var key = RedisKeyHelper.CreateVisitStatKey(date);
            var value = await Redis.HashGetAsync(key, path.ToLower());
            if (value.IsNullOrEmpty()) return new VisitStat();
            var v = Parse(Convert.ToInt64(value));
            return new VisitStat { Path = path, Visits = v.Item1, TotalLength = (ulong)v.Item2 };
        }

        public async Task<List<VisitStat>> GetAllVisitStat(DateTime? date = null)
        {
            var key = RedisKeyHelper.CreateVisitStatKey(date);
            var values = await Redis.HashGetDictionaryWithTypeAsync<long>(key);
            var rlt = new List<VisitStat>();
            foreach (var item in values)
            {
                var t = Parse(Convert.ToInt64(item.Value));
                rlt.Add(new VisitStat { Path = item.Key, Visits = t.Item1, TotalLength = (ulong)t.Item2 });
            }
            return rlt;
        }

        private long Calc(int n, long sum)
        {
            var n1 = (long)n;
            var v = n1 << 40 | sum;
            return v;
        }


        private Tuple<uint, long> Parse(long n)
        {
            var n1 = n >> 40;
            var n2 = n & 0xFFFFF;

            return new Tuple<uint, long>((uint)n1, n2);
        }

        public async Task<Result<SysOrderPayInfoResult>> GetSysOrderPayList(OrderQueryModel orderQueryModel)
        {
            try
            {
                DateTime dtStart;
                DateTime dtEnd;
                var users = await _userCacheService.GetUserList();
                var now = DateTime.Now;
                if (orderQueryModel.StartTime.IsNullOrEmpty())
                    orderQueryModel.StartTime = $"{now.ToString("yyyy-MM-dd")} 00:00:00";
                if (orderQueryModel.EndTime.IsNullOrEmpty())
                    orderQueryModel.EndTime = now.ToString("yyyy-MM-dd HH:mm:ss");
                if (!DateTime.TryParse(orderQueryModel.StartTime, out dtStart))
                    return new Result<SysOrderPayInfoResult> { Code = (int)ResultCode.Fail, Message = "开始时间格式不正确！" };
                if (!DateTime.TryParse(orderQueryModel.EndTime, out dtEnd))
                    return new Result<SysOrderPayInfoResult> { Code = (int)ResultCode.Fail, Message = "结束时间格式不正确！" };

                if (dtStart == DateTime.MinValue || dtStart > now)
                    dtStart = DateTime.Parse($"{now.ToString("yyyy-MM-dd")} 00:00:00");
                if (dtEnd == DateTime.MinValue || dtEnd > now)
                    dtEnd = now;
                if (dtEnd < dtStart)
                    return new Result<SysOrderPayInfoResult> { Code = (int)ResultCode.Fail, Message = "开始时间大于结束时间！" };

                var list = UnitOfWorkService.Execute(() => { return _orderPayRepository.GetList(t => t.CreateTime >= dtStart && t.CreateTime <= dtEnd && t.IsSuccess == true && t.IsTest == false); });
                if (orderQueryModel.BusinessType > 0)
                    list = list.Where(t => t.BusinessType == orderQueryModel.BusinessType);
                if (orderQueryModel.PayType > -1)
                    list = list.Where(t => t.PaymentType == orderQueryModel.PayType);
                if (orderQueryModel.Aid.IsNotNullOrEmpty())
                    list = list.Where(t => t.Aid == orderQueryModel.Aid);
                if (orderQueryModel.IsRefund != null)
                    list = list.Where(t => t.IsRefund == orderQueryModel.IsRefund.Value);
                //if (orderQueryModel.UserName.IsNotNullOrEmpty())
                //{
                //    var userInfo = users.Find(t => t.Username.Contains(orderQueryModel.UserName.Trim()));
                //    if (userInfo != null)
                //        list = list.Where(t => t.UserId == userInfo.Userid);

                //}
                //if (orderQueryModel.Phone.IsNotNullOrEmpty())
                //{
                //    var userInfo = users.Find(t => t.Phone.Contains(orderQueryModel.Phone.Trim()));
                //    if (userInfo != null)
                //        list = list.Where(t => t.UserId == userInfo.Userid);
                //}
                if (orderQueryModel.UserName.IsNotNullOrEmpty())
                {
                    var tmpUsers = users.FindAll(t => t.Username.IsNotNullOrEmpty());
                    var userInfo = tmpUsers.Where(t => t.Username.Contains(orderQueryModel.UserName.Trim()));
                    var ids = userInfo.Select(t => t.Userid).ToList();
                    if (tmpUsers != null)
                        list = list.Where(t => ids.Contains(t.UserId));
                }
                if (orderQueryModel.Phone.IsNotNullOrEmpty())
                {
                    var tmpUsers = users.FindAll(t => t.Phone.IsNotNullOrEmpty());
                    var userInfo = tmpUsers.Where(t => t.Phone.Contains(orderQueryModel.Phone.Trim()));
                    var ids = userInfo.Select(t => t.Userid).ToList();
                    if (tmpUsers != null)
                        list = list.Where(t => ids.Contains(t.UserId));
                }
                if (orderQueryModel.TransactionNo.IsNotNullOrEmpty())
                    list = list.Where(t => t.TransactionNo.Contains(orderQueryModel.TransactionNo));
                if (orderQueryModel.OrderId.IsNotNullOrEmpty())
                    list = list.Where(t => t.OrderId.Contains(orderQueryModel.OrderId));

                list = list.OrderByDescending(t => t.CreateTime).ToList();
                var totalCount = list.Count();
                var models = list.Skip((orderQueryModel.PageIndex - 1) * orderQueryModel.PageSize).Take(orderQueryModel.PageSize).ToList();
                var rows = models.MapTo<List<SysOrderPayInfo>>();
                if (rows != null)
                {
                    var orderItems = UnitOfWorkService.Execute(() => _orderItemRepository.Query<Domain.Entities.Order.OrderItemEntity>("SELECT `order_item`.`ItemId`, `order_item`.`OrderId`,`order_item`.`Title`,  `order_item`.`Id` FROM `order_item`"));
                    //GetList().Select(t => new
                    //{
                    //    t.ItemId,
                    //    t.OrderId,
                    //    t.Title,
                    //    t.Id
                    //})
                    foreach (var item in rows)
                    {
                        var user = users.Find(t => t.Userid == item.UserId);
                        if (user != null)
                        {
                            item.UserName = user.Username;
                            item.Phone = user.Phone;
                        }
                        //if (item.BusinessType == (int)BusinessType.Vip)
                        //{
                        var orderItem = orderItems.FirstOrDefault(t => t.OrderId == item.OrderId);
                        if (orderItem != null)
                            item.ItemName = orderItem.Title;
                        //}
                        //else
                        //{
                        //    var courseOrder = _courseOrderService.Table().FirstOrDefault(t => t.OrderNo == item.OrderId);
                        //    if (courseOrder == null) continue;

                        //    var courseInfo = await _courseService.GetAsync(courseOrder.CourseId);
                        //    if (courseInfo.Code != (int)ResultCode.Success) continue;
                        //    item.ItemName = courseInfo.Data.CourseName;
                        //}
                        if (item.Aid != "college")
                        {
                            item.Phone = item.Phone.Substring(0, item.Phone.Length - 3) + "***";
                            item.TransactionNo = "";
                        }
                    }
                }
                var data = new SysOrderPayInfoResult
                {
                    PageIndex = orderQueryModel.PageIndex,
                    PageSize = orderQueryModel.PageSize,
                    Rows = rows,
                    Total = totalCount
                };
                return new Result<SysOrderPayInfoResult> { Code = (int)ResultCode.Success, Data = data };

            }
            catch (Exception ex)
            {
                Logger.Error("获取支付台账异常", ex);
                return new Result<SysOrderPayInfoResult> { Code = (int)ResultCode.Fail, Message = "获取支付台账异常" };
            }
        }

        public async Task<Result> ExportOrderPay(OrderQueryModel orderQueryModel)
        {
            try
            {
                DateTime dtStart;
                DateTime dtEnd;
                var users = await _userCacheService.GetUserList();
                var now = DateTime.Now;
                if (orderQueryModel.StartTime.IsNullOrEmpty())
                    orderQueryModel.StartTime = $"{now.ToString("yyyy-MM-dd")} 00:00:00";
                if (orderQueryModel.EndTime.IsNullOrEmpty())
                    orderQueryModel.EndTime = now.ToString("yyyy-MM-dd HH:mm:ss");
                if (!DateTime.TryParse(orderQueryModel.StartTime, out dtStart))
                    return new Result { Code = (int)ResultCode.Fail, Message = "开始时间格式不正确！" };
                if (!DateTime.TryParse(orderQueryModel.EndTime, out dtEnd))
                    return new Result { Code = (int)ResultCode.Fail, Message = "结束时间格式不正确！" };

                if (dtStart == DateTime.MinValue || dtStart > now)
                    dtStart = DateTime.Parse($"{now.ToString("yyyy-MM-dd")} 00:00:00");
                if (dtEnd == DateTime.MinValue || dtEnd > now)
                    dtEnd = now;
                if (dtEnd < dtStart)
                    return new Result { Code = (int)ResultCode.Fail, Message = "开始时间大于结束时间！" };

                var list = UnitOfWorkService.Execute(() => { return _orderPayRepository.GetList(t => t.CreateTime >= dtStart && t.CreateTime <= dtEnd && t.IsSuccess == true && t.IsTest == false); });
                if (orderQueryModel.BusinessType > 0)
                    list = list.Where(t => t.BusinessType == orderQueryModel.BusinessType);
                if (orderQueryModel.PayType > -1)
                    list = list.Where(t => t.PaymentType == orderQueryModel.PayType);
                if (orderQueryModel.Aid.IsNotNullOrEmpty())
                    list = list.Where(t => t.Aid == orderQueryModel.Aid);
                if (orderQueryModel.IsRefund != null)
                    list = list.Where(t => t.IsRefund == orderQueryModel.IsRefund.Value);
                list = list.OrderByDescending(t => t.CreateTime).ToList();
                var rows = list.MapTo<List<SysOrderPayInfoExport>>();
                if (rows != null)
                {
                    var orderItems = UnitOfWorkService.Execute(() => _orderItemRepository.Query<Domain.Entities.Order.OrderItemEntity>("SELECT `order_item`.`ItemId`, `order_item`.`OrderId`,`order_item`.`Title`,  `order_item`.`Id` FROM `order_item`"));
                    //GetList().Select(t => new
                    //{
                    //    t.ItemId,
                    //    t.OrderId,
                    //    t.Title,
                    //    t.Id
                    //})
                    foreach (var item in rows)
                    {
                        var user = users.Find(t => t.Userid == item.UserId);
                        if (user != null)
                        {
                            item.UserName = user.Username;
                            item.Phone = user.Phone;
                        }
                        //if (item.BusinessType == (int)BusinessType.Vip)
                        //{
                        var orderItem = orderItems.FirstOrDefault(t => t.OrderId == item.OrderId);
                        if (orderItem != null)
                            item.ItemName = orderItem.Title;
                        //}
                        //else
                        //{
                        //    var courseOrder = _courseOrderService.Table().FirstOrDefault(t => t.OrderNo == item.OrderId);
                        //    if (courseOrder == null) continue;

                        //    var courseInfo = await _courseService.GetAsync(courseOrder.CourseId);
                        //    if (courseInfo.Code != (int)ResultCode.Success) continue;
                        //    item.ItemName = courseInfo.Data.CourseName;
                        //}
                    }
                }
                string fileName = $"{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                string[] nameStrs = new string[12] { "id", "用户名", "手机号码", "名称", "实付金额", "购买时间", "类别", "代理商", "支付方式", "交易号", "订单号", "是否退款" };//按照模型先后顺序，赋值需要的名称
                string savePath = "wwwroot/Excel";//相对路径
                string msg = "Excel/" + fileName;//文件返回地址，出错就返回错误信息。
                var rlt = OfficeHelper.ModelExportEPPlusExcel(rows, savePath, fileName, nameStrs, ref msg);
                if (!rlt)
                    return new Result { Code = (int)ResultCode.Fail, Message = msg };
                return new Result { Code = (int)ResultCode.Success, Data = msg };

            }
            catch (Exception ex)
            {
                throw new Exception("导出订单台账异常", ex);
            }
        }

        public bool Valid(string key)
        {
            var pkey = $"ly{DateTime.Now.ToString("HHdd")}";
            return key.Contains(pkey, StringComparison.OrdinalIgnoreCase);
        }

        private class GroupModel
        {
            public string Key { get; set; }
            public int Count { get; set; }
            public long Sum { get; set; }
        }


        public async Task<int> GetGuestStatus()
        {
            var key = RedisKeyHelper.GUEST_STATUS;
            return await Redis.StringGetWithTypeAsync<int>(key);
        }

        public async Task<bool> SetGuestStatus(int flag)
        {
            var key = RedisKeyHelper.GUEST_STATUS;
            return await Redis.StringSetWithTypeAsync(key, flag);
        }
    }
}