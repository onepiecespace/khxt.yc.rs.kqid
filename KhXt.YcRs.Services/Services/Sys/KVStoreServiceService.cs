﻿
using AutoMapper;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Repositories.Sys;
using KhXt.YcRs.Domain.Services;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Services
{
    //WHY 规范接口，加缓存
    public class KVStoreService : ServiceBase, IKVStoreService
    {
        IKVStoreRepository _repo;
        private readonly IMapper _mapper;

        public KVStoreService(IKVStoreRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }


        public List<KeyValueStore> GetClassList()
        {
            var infocStores = Table().Where(n => n.TenantId == Consts.USER_CLASS_TENANTID).ToList();

            return _mapper.Map<List<KeyValueStore>>(infocStores);
            //return infocStores.MapTo<List<KeyValueStore>>();
        }

        #region IComBaseService
        public IQueryable<KVStoreEntity> Table()
        {
            var body = UnitOfWorkService.Execute(() =>
            {
                return _repo.GetList().AsQueryable();
            });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(KVStoreEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }


        public void Delete(KVStoreEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(entity);
            });
        }

        public void Delete(Expression<Func<KVStoreEntity, bool>> predicate)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(predicate);
            });
        }



        public IQueryable<KVStoreEntity> Fetch(Expression<Func<KVStoreEntity, bool>> predicate, Action<Orderable<KVStoreEntity>> order)
        {
            var body = UnitOfWorkService.Execute<IQueryable<KVStoreEntity>>(() =>
            {
                if (order == null)
                {
                    return Table().Where(predicate);
                }
                var deferrable = new Orderable<KVStoreEntity>(Table().Where(predicate));
                order(deferrable);
                return deferrable.Queryable;

            });
            return body;

        }

        public KVStoreEntity Get(object id)
        {
            var body = UnitOfWorkService.Execute<KVStoreEntity>(() =>
            {
                long.TryParse(id.ToString(), out long nid);
                return _repo.Get(nid);

            });
            return body;

        }


        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList<KVStoreEntity> Gets(Expression<Func<KVStoreEntity, bool>> predicate, Action<Orderable<KVStoreEntity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList<KVStoreEntity>>(() =>
            {
                if (order == null)
                {
                    return new PageList<KVStoreEntity>(Table().Where(predicate), pageIndex, pageSize);
                }
                var deferrable = new Orderable<KVStoreEntity>(Table().Where(predicate));
                order(deferrable);
                var ts = deferrable.Queryable;
                var totalCount = ts.Count();
                var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

                return new PageList<KVStoreEntity>(pagingTs, pageIndex, pageSize, totalCount);

            });
            return body;
        }

        public void Update(KVStoreEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }




        #endregion
    }


}
