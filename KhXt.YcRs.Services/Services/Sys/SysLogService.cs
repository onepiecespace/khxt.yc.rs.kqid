﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Models.Sys;
using KhXt.YcRs.Domain.Repositories.Sys;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Sys;
using Hx.Extensions;
using Hx.MongoDb;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Sys
{
    /// <summary>
    /// 
    /// </summary>
    public class SysLogService : ServiceBase, ISysLogService
    {
        private readonly IMongoUnitOfWorkService _mongoUow;
        private readonly ISysLogRepository _sysLogRepository;
        private readonly Dictionary<string, int> _apiBusinessType;
        public SysLogService(IMongoUnitOfWorkService mongoUow, ISysLogRepository sysLogRepository)
        {
            _mongoUow = mongoUow;
            _sysLogRepository = sysLogRepository;
            _apiBusinessType = new Dictionary<string, int>();
            InitBusinessType();
        }
        private void InitBusinessType()
        {
            //登录认证
            _apiBusinessType.Add("/api/Organization", 1);
            _apiBusinessType.Add("/api/Tenants", 1);
            _apiBusinessType.Add("/api/User", 1);
            _apiBusinessType.Add("/api/Version", 1);

            _apiBusinessType.Add("/api/Auth", 2);
            _apiBusinessType.Add("/api/Dictation", 2);

            _apiBusinessType.Add("/api/Ad", 3);

            _apiBusinessType.Add("/api/Course", 4);

            _apiBusinessType.Add("/api/MyTasks", 8);
            _apiBusinessType.Add("/api/Task", 8);

            _apiBusinessType.Add("/api/Pay", 16);
            _apiBusinessType.Add("/api/pay", 16);
            _apiBusinessType.Add("/Pay", 16);
            _apiBusinessType.Add("/pay", 16);

            _apiBusinessType.Add("/api/HxUser", 32);

            _apiBusinessType.Add("/api/ Read", 64);
            _apiBusinessType.Add("/api/Synthesis", 64);

            _apiBusinessType.Add("/api/Lesson", 65);

            _apiBusinessType.Add("/api/Pk", 128);

            _apiBusinessType.Add("/api/game", 256);

            _apiBusinessType.Add("/api/Vip", 512);
            _apiBusinessType.Add("/api/VipCard", 512);

            _apiBusinessType.Add("/api/Coupon", 1024);

            _apiBusinessType.Add("/api/Comments", 2048);

            _apiBusinessType.Add("/api/Test", 4096);

            _apiBusinessType.Add("/api/recite", 8192);

            _apiBusinessType.Add("/api/msgnotice", 16384);

            _apiBusinessType.Add("/api/order", 32768);
            _apiBusinessType.Add("/api/Order", 32768);

            _apiBusinessType.Add("/api/marketing", 65536);

            _apiBusinessType.Add("/api/idiomgame", 131072);

            _apiBusinessType.Add("/api/sys", 262144);
        }
        public async Task<Result<SysLogStatistics>> LogsStatistics(int category, string startTime = "", string endTime = "")
        {
            try
            {
                var result = new SysLogStatistics();
                var allTotal = _mongoUow.Execute(() =>
                {
                    var tempCount = 0;
                    var tempList = _sysLogRepository.GetList().Where(t => t.Category == 0);
                    tempList.ForEach(t =>
                    {
                        tempCount += (int)t.Count;
                    });
                    return tempCount;
                });
                result.AllTotalCount = allTotal;
                var total = 0;
                var hourLogs = new List<HourCount>();
                var apiLogs = new List<SysLogEntity>();
                var now = DateTime.Now;
                //按照小时统计接口访问频率
                var keyHh = RedisKeyHelper.CreateSysHhLog(now.ToString("yyyy-MM-dd"));
                var logHhList = await Redis.HashGetAllAsync(keyHh);
                var todayLogs = DealSysLogs(logHhList, now, 0);
                hourLogs.AddRange(todayLogs.MapTo<List<HourCount>>());
                //按照小时统计接口访问频率
                var key = RedisKeyHelper.CreateSysLog(now.ToString("yyyy-MM-dd"));
                var logApiList = await Redis.HashGetAllAsync(key);
                var todayApiLogs = DealSysLogs(logApiList, now, 1);
                foreach (var item in todayApiLogs)
                {
                    item.BusinessType = DealApiToBusinessType(item.ApiUrl);
                }
                hourLogs.ForEach(t => { total += (int)t.Count; });
                apiLogs.AddRange(todayApiLogs);
                //今日
                if (category == 0)
                {
                    var hourChars = new List<HourCount>();
                    for (var i = 0; i < 24; i++)
                    {
                        if (hourLogs.Exists(t => t.Hour == i))
                        {
                            var info = hourLogs.Find(t => t.Hour == i);
                            hourChars.Add(info);
                        }
                        else
                        {
                            hourChars.Add(new HourCount { Hour = i, Count = 0 });
                        }
                    }
                    result.TotalCount = total;
                    result.AllTotalCount += result.TotalCount;
                    result.CountsHourChar = hourChars;
                    var apiChars = apiLogs.GroupBy(x => new { x.BusinessType }).Select(group => new
                    ApiCount
                    {
                        BusinessType = group.Key.BusinessType,
                        Count = group.Sum(p => p.Count)
                    }).ToList();
                    result.ApiHourChar = apiChars;
                }
                else if (category == 1)//最近一周
                {
                    var list = await _mongoUow.Execute(async () => await _sysLogRepository.GetListAsync());
                    var tempHourChars = list.Where(t => t.CreateTime <= now && t.CreateTime >= now.AddDays(-6) && t.Category == 0);
                    var hourChars = tempHourChars.MapTo<List<HourCount>>();
                    hourChars.AddRange(hourLogs);
                    var chars = hourChars.GroupBy(x => new { x.Time }).Select(group => new HourCount()
                    {
                        Time = group.Key.Time,
                        Count = group.Sum(t => t.Count)
                    }).ToList();
                    for (var i = 6; i > -1; i--)
                    {
                        var time = now.AddDays(-i).ToString("yyyy-MM-dd");
                        if (!chars.Exists(t => t.Time.Equals(time)))
                            chars.Add(new HourCount() { Count = 0, Time = time });
                    }
                    chars = chars.OrderBy(t => t.Time).ToList();
                    result.TotalCount = total;
                    result.AllTotalCount += total;
                    result.CountsHourChar = chars;
                    var apiChars = apiLogs.GroupBy(x => new { x.BusinessType }).Select(group => new
                    ApiCount
                    {
                        BusinessType = group.Key.BusinessType,
                        Count = group.Sum(p => p.Count)
                    }).ToList();
                    result.ApiHourChar = apiChars;
                }
                else
                {
                    var list = await _mongoUow.Execute(async () => await _sysLogRepository.GetListAsync());
                    var tempHourChars = list.Where(t => t.CreateTime <= now && t.CreateTime >= now.AddYears(-1) && t.Category == 0);
                    var hourChars = tempHourChars.MapTo<List<HourCount>>();
                    hourChars.AddRange(hourLogs);
                    var chars = hourChars.GroupBy(x => new { x.CreateTime.Month }).Select(group => new HourCount()
                    {
                        Month = group.Key.Month,
                        Count = group.Sum(p => (int)p.Count)
                    }).ToList();
                    for (var i = 11; i > -1; i--)
                    {
                        var time = now.AddMonths(-i);
                        if (!chars.Exists(t => t.Month.Equals(time.Month)))
                            chars.Add(new HourCount() { Count = 0, Hour = time.Month, Time = time.ToString("yyyy-MM") });
                        else
                            chars.Find(t => t.Month.Equals(time.Month)).Time = time.ToString("yyyy-MM");
                    }
                    chars = chars.OrderBy(t => t.Time).ToList();
                    result.TotalCount = total;
                    result.AllTotalCount += total;
                    result.CountsHourChar = chars;
                    var apiChars = apiLogs.GroupBy(x => new { x.BusinessType }).Select(group => new
                        ApiCount
                    {
                        BusinessType = group.Key.BusinessType,
                        Count = group.Sum(p => p.Count)
                    }).ToList();
                    result.ApiHourChar = apiChars;
                }
                return new Result<SysLogStatistics> { Data = result };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("访问记录统计异常", ex);
                return new Result<SysLogStatistics>() { Code = (int)ResultCode.Exception, Message = "访问记录统计异常" };
            }
        }

        public async Task<Result> SyncRedis(string startTime, bool isClean = false)
        {
            try
            {
                var today = DateTime.Now;
                var startDay = DateTime.Parse(startTime);
                var days = today.Day - startDay.Day;
                var logs = new List<SysLogEntity>();
                var redisKeys = new List<string>();
                for (var i = 0; i < days; i++)
                {
                    startDay = startDay.AddDays(i);
                    //按照小时统计接口访问频率
                    var keyHh = RedisKeyHelper.CreateSysHhLog(startDay.ToString("yyyy-MM-dd"));
                    var logHhList = await Redis.HashGetAllAsync(keyHh);
                    logs.AddRange(DealSysLogs(logHhList, startDay, 0));
                    redisKeys.Add(keyHh);
                    //按照接口统计接口访问频率
                    var key = RedisKeyHelper.CreateSysLog(startDay.ToString("yyyy-MM-dd"));
                    var logApiList = await Redis.HashGetAllAsync(key);
                    logs.AddRange(DealSysLogs(logApiList, startDay, 1));
                    foreach (var item in logs)
                    {
                        item.BusinessType = DealApiToBusinessType(item.ApiUrl);
                    }
                    redisKeys.Add(key);
                }
                if (logs.Count <= 0)
                    return new Result() { Message = "同步服务器Redis数据存储成功" };
                var flag = _mongoUow.Execute(() => _sysLogRepository.BatchInsert(logs));
                if (flag <= 0)
                    return new Result() { Message = "同步服务器Redis数据存储错误", Code = (int)ResultCode.Fail };
                if (isClean != true || redisKeys.Count <= 0) return new Result() { };
                foreach (var redisKey in redisKeys)
                {
                    if (!Redis.KeyExists(redisKey))
                        continue;
                    Redis.KeyDelete(redisKey);
                }
                return new Result() { Message = "同步服务器Redis数据存储成功" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("同步服务器Redis记录异常", ex);
                return new Result() { Code = (int)ResultCode.Exception, Message = "同步服务器Redis记录异常" };
            }
        }

        private List<SysLogEntity> DealSysLogs(string[] logs, DateTime createTime, int category)
        {
            var sysLogs = new List<SysLogEntity>();
            if (logs == null || logs.Length <= 0)
                return sysLogs;
            foreach (var log in logs)
            {
                string[] str = log.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (str.Length < 2)
                    continue;
                if (category == 0)
                    sysLogs.Add(new SysLogEntity
                    {
                        Id = Guid.NewGuid().ToString(),
                        Category = category,
                        ApiUrl = "",
                        Count = long.Parse(str[1]),
                        Hour = int.Parse(str[0]),
                        CreateTime = DateTime.Parse(createTime.ToString10())
                    });
                else
                    sysLogs.Add(new SysLogEntity
                    {
                        Id = Guid.NewGuid().ToString(),
                        Category = category,
                        ApiUrl = str[0],
                        Count = long.Parse(str[1]),
                        Hour = 0,
                        CreateTime = DateTime.Parse(createTime.ToString10())
                    });
            }
            return sysLogs;
        }

        private int DealApiToBusinessType(string apiUrl)
        {
            if (apiUrl.IsNullOrEmpty())
                return 0;
            var v = from d in _apiBusinessType where apiUrl.Contains(d.Key) select d;
            if (v != null && v.Any())
                return _apiBusinessType[v.First().Key];
            else
                return 0;
        }
    }
}
