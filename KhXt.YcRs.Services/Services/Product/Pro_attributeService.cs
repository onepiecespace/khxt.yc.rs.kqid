﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models.Product;
using KhXt.YcRs.Domain.Repositories.Product;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Product;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Product
{
    public class Pro_attributeService : ServiceBase, IPro_attributeService
    {
        IPro_attributeRepository _repo;

        public Pro_attributeService(IPro_attributeRepository repo)
        {
            _repo = repo;
        }

        #region IComBaseService

        public async Task<Result<List<ProAttributeInfo>>> GetAll()
        {
            try
            {


                var body = await UnitOfWorkService.Execute(async () =>
                {
                    var list = await _repo.GetListAsync();
                    var taskList = list.ToList().MapTo<List<ProAttributeInfo>>();

                    return taskList;
                });

                return new Result<List<ProAttributeInfo>> { Data = body };
            }
            catch (Exception ex)
            {
                Logger.Error("商品属性配置列表缓存异常", ex);
                return null;
            }

        }

        public async Task<IQueryable<ProAttributeInfo>> Table()
        {
            var taskListRlt = await GetAll();
            var taskList = new List<ProAttributeInfo>();
            var List = taskListRlt.Data;
            return List.AsQueryable();

        }

        /// <summary>
        /// List异步的分页
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<Result<List<ProAttributeInfo>>> GetListAsync(Expression<Func<ProAttributeInfo, bool>> predicate, Action<Orderable<ProAttributeInfo>> order)
        {
            var body = await Table();
            if (order == null)
            {
                var orderlist = body.Where(predicate).ToList();
                return await Task.FromResult(new Result<List<ProAttributeInfo>>() { Data = orderlist });
            }
            var deferrable = new Orderable<ProAttributeInfo>(body.Where(predicate));
            order(deferrable);
            var ts = deferrable.Queryable;
            var list = ts.ToList();
            var totalCount = ts.Count();
            return await Task.FromResult(new Result<List<ProAttributeInfo>>() { Data = list });

        }

        #endregion

        #region 自定义方法

        public async Task<Result<List<ProAttributeInfo>>> GetList(int proid)
        {
            try
            {
                #region  条件判断
                Expression<Func<ProAttributeInfo, bool>> predicate = null;
                predicate = n => n.recycle == 0 && n.product_id == proid;
                #endregion
                var ProList = await GetListAsync(predicate, order => order.Asc(n => n.attrId));
                //var result = new List<ProAttributeInfo>()
                //{
                //    ProAttributeList = ProList.Data
                //};
                return await Task.FromResult(new Result<List<ProAttributeInfo>>() { Data = ProList.Data });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询商品属性配置异常", ex);
                return await Task.FromResult(new Result<List<ProAttributeInfo>>() { Code = (int)ResultCode.Exception, Message = "查询商品属性配置异常！" });
            }
        }
        public async Task<Result<ProAttributeInfo>> Get(int attrId)
        {
            try
            {
                var body = await Table();
                var list = body.ToList();
                var info = list.Find(t => t.attrId == attrId);
                return await Task.FromResult(new Result<ProAttributeInfo>() { Data = info });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取商品属性配置{attrId}异常！", exception);
                return new Result<ProAttributeInfo> { Code = (int)ResultCode.Exception, Message = $"获取属性配置异常" };
            }

        }
        #endregion
    }

}
