﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Models.Product;
using KhXt.YcRs.Domain.Repositories.Product;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Product;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Product
{
    public class Pro_classService : ServiceBase, IPro_classService
    {
        IPro_classRepository _repo;

        public Pro_classService(IPro_classRepository repo)
        {
            _repo = repo;
        }

        #region IComBaseService

        public async Task<Result<List<ProCategoryInfo>>> GetAll()
        {
            try
            {


                var body = await UnitOfWorkService.Execute(async () =>
                {
                    var list = await _repo.GetListAsync();
                    var taskList = list.ToList().MapTo<List<ProCategoryInfo>>();

                    return taskList;
                });

                return new Result<List<ProCategoryInfo>> { Data = body };
            }
            catch (Exception ex)
            {
                Logger.Error("商品分类列表缓存异常", ex);
                return null;
            }

        }

        public async Task<IQueryable<ProCategoryInfo>> Table()
        {
            var taskListRlt = await GetAll();
            var taskList = new List<ProCategoryInfo>();
            var List = taskListRlt.Data;
            return List.AsQueryable();

        }

        /// <summary>
        /// List异步的分页
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<Result<List<ProCategoryInfo>>> GetListAsync(Expression<Func<ProCategoryInfo, bool>> predicate, Action<Orderable<ProCategoryInfo>> order)
        {
            var body = await Table();
            if (order == null)
            {
                var orderlist = body.Where(predicate).ToList();
                return await Task.FromResult(new Result<List<ProCategoryInfo>>() { Data = orderlist });
            }
            var deferrable = new Orderable<ProCategoryInfo>(body.Where(predicate));
            order(deferrable);
            var ts = deferrable.Queryable;
            var list = ts.ToList();
            var totalCount = ts.Count();
            return await Task.FromResult(new Result<List<ProCategoryInfo>>() { Data = list });

        }

        #endregion
        #region 自定义方法

        public async Task<Result<ProCategoryListInfo>> GetList(int sid)
        {
            try
            {
                #region  条件判断
                Expression<Func<ProCategoryInfo, bool>> predicate = null;
                predicate = n => n.recycle == 0 && n.sid == sid;
                #endregion
                var ProList = await GetListAsync(predicate, order => order.Asc(n => n.sort));
                var result = new ProCategoryListInfo()
                {
                    ProCategorypList = ProList.Data
                };
                return await Task.FromResult(new Result<ProCategoryListInfo>() { Data = result });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询商品分类异常", ex);
                return await Task.FromResult(new Result<ProCategoryListInfo>() { Code = (int)ResultCode.Exception, Message = "查询商品分类异常！" });
            }
        }
        public async Task<Result<ProCategoryInfo>> Get(int cid)
        {
            try
            {
                var body = await Table();
                var list = body.ToList();
                var bankinfo = list.Find(t => t.Id == cid);
                return await Task.FromResult(new Result<ProCategoryInfo>() { Data = bankinfo });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取商品分类{cid}异常！", exception);
                return new Result<ProCategoryInfo> { Code = (int)ResultCode.Exception, Message = $"获取分类异常" };
            }

        }
        #endregion
    }









}
