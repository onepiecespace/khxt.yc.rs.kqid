﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Product;
using KhXt.YcRs.Domain.Models.Product;
using KhXt.YcRs.Domain.Repositories.Product;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Product;
using Hx.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Product
{
    public class Pro_listService : ServiceBase, IPro_listService
    {
        IPro_listRepository _repo;

        private readonly IPro_attributeService _proattributeService;

        public Pro_listService(IPro_listRepository repo, IPro_attributeService proattributeService)
        {
            _repo = repo;
            _proattributeService = proattributeService;
        }

        #region IComBaseService

        public async Task<Result<List<ProInfo>>> GetAll()
        {
            try
            {

                var imgSvr = GetConfig("imgserver");
                var body = await UnitOfWorkService.Execute(async () =>
                {
                    var list = await _repo.GetListAsync();
                    var taskList = list.ToList().MapTo<List<ProInfo>>();
                    if (taskList.Count > 0)
                    {
                        taskList.ForEach(item =>
                       {
                           if (!string.IsNullOrEmpty(item.imgurl))
                           {
                               item.imgurl = ResUrl(item.imgurl);
                           }
                       });
                        //   await Redis.StringSetWithTypeAsync(key, taskList, TimeSpan.FromHours(1));
                    }
                    return taskList;
                });

                return new Result<List<ProInfo>> { Data = body };
            }
            catch (Exception ex)
            {
                Logger.Error("商品列表缓存异常", ex);
                return null;
            }

        }

        public async Task<IQueryable<ProInfo>> Table()
        {
            var taskListRlt = await GetAll();
            var taskList = new List<ProInfo>();
            var List = taskListRlt.Data;
            if (List.Count > 0)
            {
                List.ForEach(async item =>
                {
                    if (item.volume == item.total_num)
                    {
                        item.IsSoldout = true;
                    }
                    else
                    {
                        item.IsSoldout = false;
                    }
                    var rlt = await _proattributeService.GetList(item.proId);
                    if (rlt.Data.Count > 0)
                    {
                        var ProAttributeInfo = rlt.Data.FirstOrDefault();
                        item.attribute = ProAttributeInfo.attribute;
                        item.iscoins = ProAttributeInfo.iscoins;
                        item.coins_price = ProAttributeInfo.coins_price;
                        item.costprice = ProAttributeInfo.costprice;

                    }
                });
                //   await Redis.StringSetWithTypeAsync(key, taskList, TimeSpan.FromHours(1));
            }

            return List.AsQueryable();

        }

        /// <summary>
        /// List异步的分页
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<Result<List<ProInfo>>> GetListAsync(Expression<Func<ProInfo, bool>> predicate, Action<Orderable<ProInfo>> order)
        {
            var body = await Table();
            if (order == null)
            {
                var orderlist = body.Where(predicate).ToList();
                return await Task.FromResult(new Result<List<ProInfo>>() { Data = orderlist });
            }
            var deferrable = new Orderable<ProInfo>(body.Where(predicate));
            order(deferrable);
            var ts = deferrable.Queryable;
            var list = ts.ToList();
            var totalCount = ts.Count();
            return await Task.FromResult(new Result<List<ProInfo>>() { Data = list });

        }
        /// <summary>
        /// 条件排序有分页查询
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<PageList<ProInfo>> GetPageListAsync(Expression<Func<ProInfo, bool>> predicate, Action<Orderable<ProInfo>> order, int pageSize, int pageIndex)
        {
            var body = await Table();
            if (order == null)
            {
                return await Task.FromResult(new PageList<ProInfo>(body.Where(predicate), pageIndex, pageSize));
            }
            var deferrable = new Orderable<ProInfo>(body.Where(predicate));
            order(deferrable);
            var ts = deferrable.Queryable;
            var totalCount = ts.Count();
            var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return await Task.FromResult(new PageList<ProInfo>(pagingTs, pageIndex, pageSize, totalCount));

        }
        #endregion
        #region 自定义方法

        public async Task<Result<ProListInfo>> GetList(int cid)
        {
            try
            {
                #region  条件判断
                #region  条件判断
                Expression<Func<ProInfo, bool>> predicate = null;
                Action<Orderable<ProInfo>> preorder = null;
                preorder = order => order.Asc(n => n.sort);
                switch (cid)
                {
                    case 0:
                        predicate = n => n.recycle == 0;
                        break;
                    default:
                        predicate = n => n.recycle == 0 && n.pro_classid == cid;
                        break;
                }
                #endregion
                #endregion
                var ProList = await GetListAsync(predicate, order => order.Asc(n => n.proId));
                var List = ProList.Data;
                var taskList = List.MapTo<List<ProInfoDto>>();
                //taskList.ForEach(async item =>
                //{
                //    var rlt = await _proattributeService.GetList(item.proId);
                //    var ProAttributeList = rlt.Data.ProAttributeList;
                //    var ProAttributeInfo = ProAttributeList.FirstOrDefault();
                //    item.iscoins = ProAttributeInfo.iscoins;
                //    item.coins_price = ProAttributeInfo.coins_price;
                //});

                var result = new ProListInfo()
                {
                    ProList = taskList
                };
                return await Task.FromResult(new Result<ProListInfo>() { Data = result });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询商品异常", ex);
                return await Task.FromResult(new Result<ProListInfo>() { Code = (int)ResultCode.Exception, Message = "查询商品异常！" });
            }
        }
        /// <summary>
        /// 分页商品
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="s_type">产品值属性 1：新品,2：热销，3：推荐</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<Result<ProListPageList>> ProPageList(int cid = 0, int s_type = 0, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                #region  条件判断
                Expression<Func<ProInfo, bool>> predicate = null;
                Action<Orderable<ProInfo>> preorder = null;
                preorder = order => order.Asc(n => n.sort);
                switch (cid)
                {
                    case 0:
                        if (s_type > 0)
                        {
                            predicate = n => n.recycle == 0 && n.s_type == s_type;
                        }
                        else
                        {
                            predicate = n => n.recycle == 0;
                        }
                        break;
                    default:
                        if (s_type == 0)
                        {
                            predicate = n => n.recycle == 0 && n.pro_classid == cid;
                        }
                        else
                        {
                            predicate = n => n.recycle == 0 && n.pro_classid == cid && n.s_type == s_type;
                        }
                        break;
                }
                #endregion
                var ProList = await GetPageListAsync(predicate, preorder, pageSize, pageIndex);

                var taskList = ProList.MapTo<List<ProInfoDto>>();

                var result = new ProListPageList()
                {
                    TotalCount = ProList.TotalCount,
                    PageIndex = ProList.PageIndex,
                    PageSize = ProList.PageSize,
                    ProList = taskList
                };
                return await Task.FromResult(new Result<ProListPageList>() { Data = result });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询商品列表异常", ex);
                return await Task.FromResult(new Result<ProListPageList>() { Code = (int)ResultCode.Exception, Message = "查询商品列表异常！" });
            }

        }

        public async Task<Result<ProDetailInfoDto>> Get(int proId)
        {
            try
            {
                var body = await Table();
                var list = body.ToList();
                var info = list.Find(t => t.proId == proId);

                var datilinfo = info.MapTo<ProDetailInfoDto>();

                var rlt = await _proattributeService.GetList(proId);
                if (rlt.Data.Count > 0)
                {
                    datilinfo.ProAttributeList = rlt.Data;
                }

                datilinfo.baseurl = DomainSetting.ResourceUrl;

                if (!string.IsNullOrWhiteSpace(info.bannerurl))
                {
                    var picStrings = info.bannerurl.Split('|', StringSplitOptions.None);
                    datilinfo.bannerurl = picStrings.ToList();
                }
                if (!string.IsNullOrWhiteSpace(info.contenturl))
                {
                    var picStrings = info.contenturl.Split('|', StringSplitOptions.None);
                    datilinfo.contenturl = picStrings.ToList();
                }

                return await Task.FromResult(new Result<ProDetailInfoDto>() { Data = datilinfo });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取商品{proId}异常！", exception);
                return new Result<ProDetailInfoDto> { Code = (int)ResultCode.Exception, Message = $"获取异常" };
            }

        }
        #endregion
    }

}



