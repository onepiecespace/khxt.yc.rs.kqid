﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx.Components;
using Hx.Extensions;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.User
{
    public class UserCacheService : ServiceBase, IUserCacheService
    {
        private readonly IUserRepository _userRepository;
        private readonly IDirectMemoryCache _cache;
        //private readonly IUserIntegratedService _integratedService;
        private const string USER_CACHE_KEY = "users";
        private readonly string _defaultHeaderUrl = "/image/pidan.png";
        //private const string USER_CACHE_KEY = "__userCache";
        public UserCacheService(IDirectMemoryCacheManager cacheManager, IUserRepository userRepository)
        {
            _userRepository = userRepository;
            //_integratedService = userIntegratedService;
            _cache = cacheManager.GetCache("__userCache");
        }

        #region 同步缓存节点

        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is UserEventData data)) return;
            if (data.Data == null || data.Data.Userid == 0) return;

            var method = (EventDataMethod)data.Method;
            var users = GetAll().Result;
            switch (method)
            {
                case EventDataMethod.Clear:
                    ClearCache();
                    break;
                case EventDataMethod.Add:
                    if (users != null)
                    {
                        users[data.Data.Userid] = data.Data;
                    }
                    break;
                case EventDataMethod.Del:
                    if (users != null)
                    {
                        users.Remove(data.Data.Userid);
                    }
                    break;
                case EventDataMethod.Update:
                    if (users != null)
                    {
                        users[data.Data.Userid] = data.Data;
                    }
                    break;
            }
        }

        public void RebuildCache()
        {
            ClearCache();
            GetUserList().GetAwaiter();
            //Trigger(new UserEventData() { Method = (int)EventDataMethod.Clear });
        }
        public override void ClearCache()
        {
            _cache.Clear();
        }
        #endregion
        #region 缓存数据处理

        private async Task<Dictionary<long, UserBaseInfo>> GetAll()
        {
            try
            {
                var imgSvr = GetConfig("imgserver");
                var cacheList = await _cache.Get(USER_CACHE_KEY, async () =>
                {
                    var body = await UnitOfWorkService.Execute(async () =>
                    {
                        var alllist = await _userRepository.GetListAsync();
                        var list = alllist.MapTo<List<UserBaseInfo>>().ToDictionary(u => u.Userid, u => u);
                        if (list.Count > 0)
                        {
                            //替换头像
                            list.ForEach(item =>
                            {
                                if (item.Value.Headurl.IsNullOrEmpty())
                                    item.Value.Headurl = _defaultHeaderUrl;
                                if (!item.Value.Headurl.StartsWith("http"))
                                    item.Value.Headurl = ResUrl(item.Value.Headurl) + "/head132";
                            });
                        }
                        return list;
                    });
                    return body;
                }, TimeSpan.FromHours(8));
                return cacheList;
            }
            catch (Exception ex)
            {
                Logger.Error("用户列表缓存异常", ex);
                return null;
            }
        }

        public async Task<List<UserBaseInfo>> GetUserList()
        {
            var all = await GetAll();
            return all.Values.ToList();
        }

        public async Task<UserBaseInfo> GetUser(long userId, bool tryUpdate = true)
        {
            var users = await GetAll();
            users.TryGetValue(userId, out var user);
            //var user = users.Values.FirstOrDefault(u => u.Userid == userId);
            //if (users.TryGetValue(userId, out var user))
            //{
            //    return user;
            //}
            if (tryUpdate)
            {
                user = UnitOfWorkService.Execute(() =>
                  {
                      return _userRepository.Get(userId).MapTo<UserBaseInfo>();
                  });
                if (user != null)
                {
                    if (user.Headurl.IsNullOrEmpty())
                        user.Headurl = _defaultHeaderUrl;
                    if (!user.Headurl.StartsWith("http"))
                        user.Headurl = ResUrl(user.Headurl) + "/head132";

                    users[user.Userid] = user;
                }
            }
            return user;

        }

        public async Task<UserBaseInfo> GetUserByPhone(string phone, bool tryUpdate = true)
        {
            if (phone.IsNullOrEmpty()) return null;
            var users = await GetAll();
            var user = users.Values.FirstOrDefault(u => u.Phone.IsNotNullOrEmpty() && u.Phone.Equals(phone));
            //if (user != null)
            //{
            //    return user;
            //}
            if (tryUpdate)
            {
                user = UnitOfWorkService.Execute(() =>
                {
                    return _userRepository.FirstOrDefault(u => u.Phone.Equals(phone)).MapTo<UserBaseInfo>();
                });
                if (user != null)
                {
                    if (user.Headurl.IsNullOrEmpty())
                        user.Headurl = _defaultHeaderUrl;
                    if (!user.Headurl.StartsWith("http"))
                        user.Headurl = ResUrl(user.Headurl) + "/head132";
                    users[user.Userid] = user;
                }
            }
            return user;
        }

        public async Task<UserBaseInfo> GetUserByUnionId(string id, LoginType loginType, bool tryUpdate = true)
        {
            if (id.IsNullOrEmpty()) return null;
            var users = await GetAll();
            var user = users.Values.FirstOrDefault(u => u.UnionId.IsNotNullOrEmpty() && u.UnionId.Equals(id) && u.LoginType == (int)loginType);
            //if (user != null)
            //{
            //    return user;
            //}
            if (tryUpdate)
            {
                user = UnitOfWorkService.Execute(() =>
                {
                    return _userRepository.FirstOrDefault(u => u.UnionId.Equals(id)).MapTo<UserBaseInfo>();
                });
                if (user != null)
                {
                    if (user.Headurl.IsNullOrEmpty())
                        user.Headurl = _defaultHeaderUrl;
                    if (!user.Headurl.StartsWith("http"))
                        user.Headurl = ResUrl(user.Headurl) + "/head132";
                    users[user.Userid] = user;
                }
            }
            return user;
        }


        public async Task<UserBaseInfo> GetUserByOpenId(string id, LoginType loginType, bool tryUpdate = true)
        {
            if (id.IsNullOrEmpty()) return null;
            var users = await GetAll();
            var user = users.Values.FirstOrDefault(u => u.OpenId.IsNotNullOrEmpty() && u.OpenId.Equals(id) && u.LoginType == (int)loginType);
            //if (user != null)
            //{
            //    return user;
            //}
            if (tryUpdate)
            {
                user = UnitOfWorkService.Execute(() =>
                {
                    return _userRepository.FirstOrDefault(u => u.OpenId.Equals(id)).MapTo<UserBaseInfo>();
                });
                if (user != null)
                {
                    if (user.Headurl.IsNullOrEmpty())
                        user.Headurl = _defaultHeaderUrl;
                    if (!user.Headurl.StartsWith("http"))
                        user.Headurl = ResUrl(user.Headurl) + "/head132";
                    users[user.Userid] = user;
                }
            }
            return user;
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool userCacheClearCache()
        {
            RebuildCache();
            return true;
        }
        #endregion
        #region 增删改

        public async Task<Result<long>> Add(UserBaseInfo model, bool ifRefreshCache = true)
        {
            if (model == null)
                return new Result<long>() { Code = (int)ResultCode.Fail, Message = "用户为空！" };
            Logger.Debug($"准备新建用户:{model.Phone},node={NodeName}");

            if (model.Phone.IsNotNullOrEmpty() && model.Phone.StartsWith("168")) //DJD:压测暂用，事后删除
            {
                model.AccountType = (int)AccountType.Tmp;
            }
            DealUserName(model);
            model.Creatime = DateTime.Now;

            var entity = model.MapTo<UserEntity>();
            var uid = await UnitOfWorkService.Execute(async () =>
            {
                if (model.Phone.IsNotNullOrEmpty())
                {
                    var eu = _userRepository.Count(u => u.Phone.Equals(model.Phone));
                    if (eu > 0)
                    {
                        return -1;//已存在用户
                    }
                }
                var userId = _userRepository.InsertAndGetId(entity);
                entity.Id = userId;
                //注册到环信
                //await _integratedService.AddUser(entity);
                return userId;
            });

            if (uid == -1)
            {
                return new Result<long>(ResultCode.ExistPhoneUser) { Data = -1 };
            }

            var users = await GetAll();
            if (model.Headurl.IsNullOrEmpty())
                model.Headurl = _defaultHeaderUrl;
            if (!model.Headurl.StartsWith("http"))
                model.Headurl = ResUrl(model.Headurl) + "/head132";
            model.Userid = uid;
            users.Add(uid, model);
            //users[model.Userid]=model;
            //  Logger.Error($"UserCacheServiceAddjson:{ObjectContainer.Resolve<Hx.Serializing.ISerializer>().Serialize(users)}");
            //  if (ifRefreshCache) //Trigger(new UserEventData() { Data = model, Method = (int)EventDataMethod.Add });
            Logger.Debug($"用户创建成功:uid={uid},phone={model.Phone},node={NodeName}");
            return new Result<long>() { Code = (int)ResultCode.Success, Message = "新增用户缓存成功！", Data = uid };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Result> Update(UserBaseInfo model)
        {
            if (model == null)
                return await Task.FromResult(new Result() { Code = (int)ResultCode.Fail, Message = "用户为空！" });
            var all = await GetAll();
            if (!all.TryGetValue(model.Userid, out var user))
            {
                return await Task.FromResult(new Result() { Code = (int)ResultCode.Fail, Message = "用户缓存为空！" });
            }
            if (model.Headurl.IsNullOrEmpty())
                model.Headurl = _defaultHeaderUrl;
            if (!model.Headurl.StartsWith("http"))
                model.Headurl = ResUrl(model.Headurl) + "/head132";
            //users.Add(model);
            all[model.Userid] = model;

            ////Trigger(new UserEventData() { Data = model, Method = (int)EventDataMethod.Update });
            return await Task.FromResult(new Result() { Code = (int)ResultCode.Success, Message = "修改用户缓存成功！" });
        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Result<string>> GetAid(long userId)
        {
            var userinfo = await GetUser(userId);
            var aidstr = userinfo.Aid.IsNullOrEmpty() ? "college" : userinfo.Aid;
            return await Task.FromResult(new Result() { Code = (int)ResultCode.Success, Message = "获取Aid成功！", Data = aidstr });

        }

        private void DealUserName(UserBaseInfo Info)
        {
            var tmpUserCode = Info.Phone;
            if (tmpUserCode.IsNullOrEmpty())
                tmpUserCode = RandomHelper.GenerateRandom(11);

            if (string.IsNullOrWhiteSpace(Info.Username))
            {
                Info.Username = $"college_{tmpUserCode}";
                //entity.Username = "马小哈";
            }
            else
            {
                var userName = Info.Username;
                try
                {
                    var pattern = @"[\U00010000-\U0010ffff]";
                    var reg = new Regex(pattern);
                    userName = reg.Replace(Info.Username, "");
                    pattern = @"[\uD800-\uDBFF][\uDC00-\uDFFF]";
                    reg = new Regex(pattern);
                    userName = reg.Replace(Info.Username, "");
                }
                catch
                {
                    string pattern = @"[\uD800-\uDBFF][\uDC00-\uDFFF]";
                    var reg = new Regex(pattern);
                    userName = reg.Replace(Info.Username, "");
                }
                finally
                {
                    if (string.IsNullOrEmpty(userName))
                        userName = $"college_{tmpUserCode}";
                }
                Info.Username = userName;
            }
        }


        public async Task<Result<long>> GetCoin(long userId)
        {
            var key = RedisKeyHelper.HASH_USER_COIN;
            var rlt = await Redis.HashGetWithTypeAsync<long>(key, userId.ToString());
            return new Result<long> { Data = rlt };
        }

        public async Task<Result<Tuple<long, long>>> GetExp(long userId, BusinessType businessType)
        {
            var key = RedisKeyHelper.CreateExpKey(businessType);
            var rs = await Redis.SortedSetRankScoreAsync(key, userId.ToString(), false);
            return new Result<Tuple<long, long>> { Data = new Tuple<long, long>(rs.Item1, (long)rs.Item2) };
        }

    }
}
