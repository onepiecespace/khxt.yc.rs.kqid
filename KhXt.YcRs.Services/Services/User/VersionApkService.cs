﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services
{
    //DJD 暂时没用到，计划代替versionservice用
    public class VersionApkService : ServiceBase, IVersionApkService
    {
        IVersionApkRepository _repo;
        private IKVStoreService _keyValue;
        public VersionApkService(IVersionApkRepository repo, IKVStoreService keyValue)
        {
            _keyValue = keyValue;
            _repo = repo;
        }


        public IQueryable<VersionApkEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<VersionApkEntity>>(() =>
               {
                   return _repo.GetList().AsQueryable();
               });
            return body;
        }
        #region 自定义方法

        /// <summary>
        /// 
        /// </summary>
        /// <param name="osSign"></param>
        /// <returns></returns>
        public async Task<Result<VersionApkInfo>> GetVersionInfo(int osSign, string currentVersion)
        {
            try
            {
                VersionApkInfo info = new VersionApkInfo();
                var newVersion = Table().Where(n => n.OsSign.Equals(osSign)).OrderByDescending(n => n.newVersion).FirstOrDefault();
                if (newVersion != null)
                {
                    info.apkUrl = newVersion.apkUrl;
                    info.updateDescription = newVersion.updateDescription;
                    info.isUpdate = newVersion.isUpdate;
                    info.forceUpdate = newVersion.forceUpdate;
                    //根据是否有特殊需求可指定某个版本必须强制更新，
                    //如currentVersion == XXX,则forceUpdate = true;
                    //如果currentVersion < newVersion,则isUpdate = true；
                    //如果currentVersion < minVersion,则forceUpdate = true；
                    //如果currentVersion >= minVersion,则forceUpdate = false;
                    //如果currentVersion == newVersion，则isUpdate = false.
                    info.md5 = newVersion.md5;
                    info.apkSize = newVersion.apkSize;
                }

                return await Task.FromResult(new Result<VersionApkInfo>() { Data = info });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询版本升级异常", ex);
                return await Task.FromResult(new Result<VersionApkInfo>() { Code = (int)ResultCode.Exception, Message = "查询版本升级异常！" });
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="osSign"></param>
        /// <returns></returns>
        public async Task<Result> GetVersionInfo(int osSign)
        {
            try
            {
                string fsname = "";
                if (osSign == 0)
                {
                    fsname = _keyValue.Table().FirstOrDefault(n => n.TenantId == Consts.DOWNLOADURL_AndrodID)?.ValueData ?? "";
                }
                else if (osSign == 1)
                {
                    fsname = _keyValue.Table().FirstOrDefault(n => n.TenantId == Consts.DOWNLOADURL_IOSID)?.ValueData ?? "";
                }

                return await Task.FromResult(new Result() { Data = fsname, Message = "获取成功" });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("查询版本升级异常", ex);
                return await Task.FromResult(new Result() { Code = (int)ResultCode.Exception, Message = "查询版本升级异常！" });
            }
        }

        #endregion
    }









}
