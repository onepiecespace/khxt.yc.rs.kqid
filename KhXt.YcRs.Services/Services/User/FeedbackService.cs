﻿
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Services
{
    /// <summary>
    ///  Feedback  FeedbackEntity FeedbackRepository FeedbackService
    /// </summary>
    public class FeedbackService : ServiceBase, IFeedbackService
    {
        IFeedbackRepository _repo;

        public FeedbackService(IFeedbackRepository repo)
        {
            _repo = repo;
        }

        public IQueryable<FeedbackEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<FeedbackEntity>>(() =>
           {
               return _repo.GetList().AsQueryable();
           });
            return body;
        }

    }
}
