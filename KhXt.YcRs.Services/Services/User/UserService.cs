﻿using Castle.Components.DictionaryAdapter;
using Castle.Core.Internal;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.Course;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.JWT;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Order;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Models.Vip;
using KhXt.YcRs.Domain.Pagination;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Course;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Services.Vip;
using KhXt.YcRs.Domain.Services.VipCard;
using KhXt.YcRs.Domain.Util;
using Hx;
using Hx.Extensions;
using Hx.ObjectMapping;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.User
{
    public class UserService : ServiceBase, IUserService
    {
        #region 构造方法
        private readonly IKVStoreService _kvStoreService;
        private readonly IUserRepository _repo;
        private readonly ICourseBuyCollectService _courseBuyCollectService;
        private readonly IUserValueLogRepository _uvlRepo;
        private readonly IAddressService _addressService;
        private readonly IUserCollegeService _usercollegeService;
        private readonly ICaptionService _captionService;
        private readonly IVipService _vipService;
        private readonly IUserCacheService _userCacheService;
        private readonly IUserThreePartyRepository _utpRepo;
        private readonly IUserLevelCacheService _levelCacheService;
        private readonly int _tokenTime = 60 * 24 * 900;
        private readonly string _appcode;
        private string _defaultHeaderUrl = "/image/pidan.png";
        public UserService(IUserRepository repo,
            IUserValueLogRepository uvlRepo,
            IUserThreePartyRepository utpRepo,
            IKVStoreService kvStoreService,
            ICourseBuyCollectService courseBuyCollectService,
            IAddressService addressService,
            IUserCollegeService usercollegeService,
            IVipService vipService, ICaptionService captionService, IUserCacheService userCacheService, IUserLevelCacheService levelCacheService)
        {
            _repo = repo;
            _uvlRepo = uvlRepo;
            _kvStoreService = kvStoreService;
            _courseBuyCollectService = courseBuyCollectService;
            _addressService = addressService;
            _usercollegeService = usercollegeService;
            _vipService = vipService;
            _captionService = captionService;
            _userCacheService = userCacheService;
            _levelCacheService = levelCacheService;
            //_hxService = hxService;
            _utpRepo = utpRepo;
            _appcode = DomainSetting.appcode;
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tryUpdate">是否从数据库加载数据</param>
        /// <returns></returns>
        public async Task<UserBaseInfo> GetUser(long userId, bool tryUpdate = true)
        {
            return await Get(userId, tryUpdate);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public async Task<UserBaseInfo> GetUserByPhone(string phone, bool tryUpdate = true)
        {
            return await _userCacheService.GetUserByPhone(phone, tryUpdate);
        }


        #region IComBaseService
        public async Task<List<UserBaseInfo>> GetAll()
        {
            return await _userCacheService.GetUserList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<Result<long>> Create(UserBaseInfo Info, bool ifRefreshCache = true)
        {
            var rlt = await _userCacheService.Add(Info, ifRefreshCache);
            return rlt;
        }


        public async Task<Result> Update(UserBaseInfo Info)
        {
            DealUserName(Info);
            var entity = Info.MapTo<UserEntity>();

            var rlt = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

            if (rlt)
            {
                var result = await _userCacheService.Update(Info);
                return await Task.FromResult(new Result() { Code = (int)ResultCode.Success, Message = "修改成功" + result.Message });
            }
            else
            {
                return await Task.FromResult(new Result() { Code = (int)ResultCode.Success, Message = "更新失败" });
            }
        }

        public async Task<UserBaseInfo> Get(long id, bool tryUpdate = true)
        {
            return await _userCacheService.GetUser(id, tryUpdate);
        }
        /// <summary>
        /// 获取token
        /// </summary>
        /// <param name="phone">id或手机号</param>
        /// <returns></returns>
        public async Task<Result> GetToken(string phone)
        {
            var token = "";
            var openid = "";
            long userid = 0;
            Result result = new Result();
            if (phone == "0")
            {
                result.Code = (int)ResultCode.Fail;
                result.Data = "";
                result.Message = "参数不能为0";
                return await Task.FromResult(result);
            }

            if (phone.Length > 0 && phone.Length < 11)
            {
                try
                {
                    long.TryParse(phone, out long idResult);
                    var info = await Get(idResult);
                    if (info != null)
                    {
                        userid = info.Userid;
                        phone = string.IsNullOrWhiteSpace(info.Phone) ? info.OpenId : info.Phone;
                        openid = string.IsNullOrWhiteSpace(info.OpenId) ? info.OpenId : info.OpenId;
                    }

                }
                catch (Exception ex)
                {
                    result.Code = (int)ResultCode.Fail;
                    Logger.Error(ex);
                }
            }
            if (!string.IsNullOrEmpty(phone))
            {

                //存在该用户，取redis里的token
                token = await ReadToken(phone);
                //var infoAUser = _userService.GetUserByPhone(phone.Trim());
                //如果第一次登陆或者Token 过期 则重新生成Token

                if (!string.IsNullOrEmpty(token))
                {
                    result.Code = (int)ResultCode.Success;
                    result.Data = $"{token}";
                    result.Message = "获取Token凭证成功";
                }
                else
                {
                    var info = await GetUserByPhone(phone.Trim());
                    if (info != null)
                    {
                        token = CreateToken(info.Userid, phone);
                        result.Code = (int)ResultCode.Success;
                        result.Data = $"{token}";
                        result.Message = "获取Token凭证成功";
                    }

                }
            }
            else
            {
                result.Code = (int)ResultCode.Fail;
                result.Data = "";
                result.Message = "获取Token凭证失败";
            }
            return await Task.FromResult(result);
        }

        private void DealUserName(UserBaseInfo Info)
        {
            var tmpUserCode = Info.Phone;
            if (tmpUserCode.IsNullOrEmpty())
                tmpUserCode = RandomHelper.GenerateRandom(11);

            if (string.IsNullOrWhiteSpace(Info.Username))
            {
                Info.Username = $"college_{tmpUserCode}";
                //entity.Username = "马小哈";
            }
            else
            {
                var userName = $"college_{tmpUserCode}";
                try
                {
                    var pattern = @"[\U00010000-\U0010ffff]";
                    var reg = new Regex(pattern);
                    userName = reg.Replace(Info.Username, "");
                    pattern = @"[\uD800-\uDBFF][\uDC00-\uDFFF]";
                    reg = new Regex(pattern);
                    userName = reg.Replace(Info.Username, "");
                }
                catch
                {
                    string pattern = @"[\uD800-\uDBFF][\uDC00-\uDFFF]";
                    var reg = new Regex(pattern);
                    userName = reg.Replace(Info.Username, "");
                }
                finally
                {
                    if (string.IsNullOrEmpty(userName))
                        userName = $"college_{tmpUserCode}";
                }
                Info.Username = userName;
            }
        }
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool userClearCache()
        {
            return _userCacheService.userCacheClearCache();
        }
        #endregion

        /// <summary>
        ///  // 1已购买 2收藏
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PageList<CourseBuyCollectEntity> GetMyCourseOrderCollectList(int type, int userId, int pageIndex, int pageSize)
        {
            var list = new PageList<CourseBuyCollectEntity>();

            if (type == 1)
            {
                list = _courseBuyCollectService.Gets(n => n.UserId == userId && n.IsBuy == 1 && n.IsHide == 0, order => order.Desc(n => n.CreateTime), pageSize, pageIndex);
            }
            else if (type == 2)
            {

                list = _courseBuyCollectService.Gets(n => n.UserId == userId && n.IsCollect == 1 && n.IsHide == 0, order => order.Desc(n => n.CreateTime), pageSize, pageIndex);
            }

            return list;
        }

        /// <summary>
        ///  已购买的课程
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<CourseBuyCollectEntity> GetMyCourseOrderCollect(int courseid)
        {
            var list = new List<CourseBuyCollectEntity>();

            list = _courseBuyCollectService.Table().Where(n => n.CourseId == courseid && n.IsBuy == 1 && n.IsHide == 0).ToList();

            return list;
        }
        public PageList<MyCourseCollectInfo> GetCourseCollectList(string phone, int pageIndex, int pageSize)
        {

            return new PageList<MyCourseCollectInfo>();
        }




        public async Task<Domain.Paged<ValueLogModel>> GetValueLogs(long userId, ValueCategory valueCategory, BusinessType businessType, int pageIndex, int pageSize)
        {
            var count = 0;
            var rlt = UnitOfWorkService.Execute(() =>
              {
                  var exp = PredicateBuilder.Default<UserValueLogEntity>().And(v => v.UserId == userId && v.Category == (int)valueCategory);
                  if (businessType != BusinessType.All)
                  {
                      exp = exp.And(v => v.BusinessType == (int)businessType);
                  }
                  var logs = _uvlRepo.GetPaged(exp, pageIndex, pageSize, out count, false, e => e.CreateTime);
                  return logs.MapTo<List<ValueLogModel>>();
              });
            var paged = new Domain.Paged<ValueLogModel>(count, rlt, pageIndex, pageSize);
            return await Task.FromResult(paged);
        }


        public async Task<Result> LoginOrRegister(string phone, string aid, string tid, string regid)
        {
            Result result = new Result();
            string token;
            if (phone.IsNullOrEmpty())
            {
                return new Result(ResultCode.Fail, "手机号不能为空");
            }
            //存在该手机号，查询数据
            var user = await GetUserByPhone(phone.Trim(), true);
            if (user == null)
            {
                //var crlt = await Create(new UserBaseInfo
                //{
                //    TenantId = long.Parse(tid),
                //    Phone = phone,
                //    LoginType = 1,
                //    AccountType = (int)AccountType.Normal,
                //    Grade = 1,
                //    Creatime = DateTime.Now,
                //    Username = "",
                //    Sex = 2,
                //    Provincial = "",
                //    City = "",
                //    Address = "",
                //    Areas = "",
                //    Headurl = _defaultHeaderUrl,
                //    Wx = "",
                //    Aid = aid,
                //    Qq = ""
                //});

                //if (crlt.Code == (int)ResultCode.Success && crlt.Data > 0)
                //{
                #region 商学院标识的用户必须是会员才可以登陆
                if (aid == "college")
                {
                    result.Code = (int)ResultCode.warning;
                    result.Message = "此手机号暂没有开通账号";
                    Logger.Error($"手机号{phone}登陆失败!,您的账号待审核,暂不可以登陆！");
                    return result;
                }
                #endregion
                //token = CreateToken(crlt.Data, phone);
                //result.Data = token;
                //return result;
                //}
                //result.Code = (int)ResultCode.Fail;
                //result.Message = "注册失败";
                //Logger.Error($"手机号{phone}注册失败!,可能原因添加到数据库失败！");
                // return result;
            }
            else
            {
                if (user.TenantId == 0 && long.Parse(tid) > 0)
                    user.TenantId = long.Parse(tid);
                // var trlt = await Update(user);
                Logger.Info($"更新用户TenantId绑定({user.Phone}) TenantId:{user.TenantId},{DateTime.Now.ToString23()}");//ResultCode:{trlt.Code},
                #region 商学院标识的用户必须市会员才可以登陆
                if (aid == "college")
                {
                    if (user.AccountType <= 1)//状态不是内部审核
                    {
                        result.Code = (int)ResultCode.warning;
                        result.Message = "此手机号暂无登录权限";
                        Logger.Error($"手机号{phone}登陆失败!,您的账户待审核,不可以登陆！");
                        return result;
                    }
                    else
                    {
                        if (user.UseDueDate < System.DateTime.Now)
                        {
                            //查看Redis是否存在匹配项 存在移除token的值
                            string tokenstr = await ReadToken(phone);
                            if (!string.IsNullOrEmpty(tokenstr))
                            {
                                string tokenkey = "token:" + phone;
                                await Redis.KeyDeleteAsync(tokenkey); //删除token缓存
                            }
                            result.Code = (int)ResultCode.warning;
                            result.Message = "此手机号授权使用已过期,暂无登录权限";
                            Logger.Error($"手机号{phone}登陆失败!,您的账户授权使用年限已过期,不可以登陆！");
                            return result;
                        }

                    }
                }
                #endregion
            }
            //存在该用户，取redis里的token
            token = await ReadToken(phone);
            //设备绑定token
            if (!string.IsNullOrEmpty(regid))
            {
                var regtoken = await ReadTokenByRegid(regid);
                if (token != regtoken)
                {
                    token = CreateToken(user.Userid, phone);
                    //设备绑定token
                    if (!string.IsNullOrEmpty(regid))
                    {
                        CreateRegId(regid, token);
                    }
                    //result.Code = (int)ResultCode.warning;
                    //result.Message = "当前账号已在其他设备登录";
                    //Logger.Error($"手机号{phone}登陆失败!,当前账号已在其他设备登录！");
                    //return result;
                }

            }
            //var infoAUser = _userService.GetUserByPhone(phone.Trim());
            //如果第一次登陆或者Token 过期 则重新生成Token
            if (string.IsNullOrEmpty(token))
            {
                token = CreateToken(user.Userid, phone);
                //设备绑定token
                if (!string.IsNullOrEmpty(regid))
                {
                    CreateRegId(regid, token);
                }
            }
            Logger.Error($"手机号{phone}登陆失成功!,aid:{aid},tid:{tid},regid:{regid}");
            result.Message = "登录成功";
            result.Data = token;

            return result;
        }
        private string CreateToken(long userId, string phone)
        {
            var token = JwtHelper.IssueJWT(new TokenModel() { Phone = phone, UserId = userId });
            Redis.StringSet("token:" + phone, token, TimeSpan.FromSeconds(_tokenTime));//TimeSpan.FromDays(90)
            Logger.Info($"token created:uid:{userId},phone:{phone},{DateTime.Now}");
            return token;
        }
        private void CreateRegId(string regid, string token)
        {
            Redis.StringSet("regid:" + regid, token, TimeSpan.FromSeconds(_tokenTime));//TimeSpan.FromDays(90)
            Logger.Info($"regid created:uid:{regid},phone:{token},{DateTime.Now}");
        }
        public async Task<Result> SSOLoginOrRegister(string phone, string aid)
        {

            if (phone.IsNullOrEmpty())
            {
                return new Result(ResultCode.Fail, "手机号不能为空");
            }
            Result result = new Result();

            var token = string.Empty;
            //存在该手机号，查询数据
            var output = await GetUserByPhone(phone.Trim());
            if (output == null)
            {
                UserBaseInfo baseinfo = new UserBaseInfo();
                var path = string.Format("api/SSO/AuthValidateOrRegister?account={0}&password={1}&appCode={2}", phone.Trim(), "123456", _appcode);
                var resultCheck = await HttpGet<LoginResult>(path);
                if (resultCheck.Success == true)
                {
                    var AccessToken = resultCheck.AccessToken;
                    var tokenpath = string.Format("api/Token/CheckAppToken?token={0}", AccessToken);
                    var resultuser = await HttpGet<CommonResult<UserSSOOutPutDto>>(tokenpath);
                    UserSSOOutPutDto OutPutDto = new UserSSOOutPutDto();
                    if (resultuser.Success == true)
                    {
                        OutPutDto = resultuser.Result;
                        if (OutPutDto != null && OutPutDto.MobilePhone == phone)
                        {
                            baseinfo.Phone = phone;
                            baseinfo.LoginType = 1;
                            baseinfo.Grade = 1;
                            baseinfo.Creatime = DateTime.Now;
                            baseinfo.Username = OutPutDto.RealName;
                            baseinfo.Sex = int.Parse(OutPutDto.Gender.ToString());
                            baseinfo.Provincial = OutPutDto.Province;
                            baseinfo.City = OutPutDto.City;
                            baseinfo.Areas = OutPutDto.District;
                            if (!string.IsNullOrEmpty(OutPutDto.HeadIcon))
                            {
                                _defaultHeaderUrl = OutPutDto.HeadIcon;
                            }
                            baseinfo.Headurl = _defaultHeaderUrl;
                        }
                    }
                }
                else
                {
                    baseinfo.Phone = phone;
                    baseinfo.LoginType = 1;
                    baseinfo.Grade = 1;
                    baseinfo.Creatime = DateTime.Now;
                    baseinfo.Address = "";
                    baseinfo.Wx = "";
                    baseinfo.Aid = aid;
                    baseinfo.Qq = "";
                    baseinfo.Username = "";
                    baseinfo.Sex = 0;
                    baseinfo.Provincial = "";
                    baseinfo.City = "";
                    baseinfo.Areas = "";
                    baseinfo.Headurl = _defaultHeaderUrl;
                }
                var resolute = await Create(baseinfo);
                if (resolute.Code == (int)ResultCode.Success && resolute.Data > 0)
                {
                    token = CreateToken(output.Userid, phone);
                }
                else
                {
                    result.Code = (int)ResultCode.Fail;
                    result.Message = "注册失败";
                    Logger.Error($"手机号{phone}注册失败!,可能原因添加到数据库失败！");
                }
                result.Data = token;
                result.Message = "登录SSO成功";
                return await Task.FromResult(result);
            }
            else
            {
                //存在该用户，取redis里的token
                token = Redis.StringGet("token:" + phone);
            }
            //存在该用户，取redis里的token
            token = await ReadToken(phone);
            //var infoAUser = _userService.GetUserByPhone(phone.Trim());
            //如果第一次登陆或者Token 过期 则重新生成Token
            if (string.IsNullOrEmpty(token))
            {
                token = CreateToken(output.Userid, phone);
            }
            else
            {
                result.Message = "登录SSO成功";
            }
            result.Data = token;
            return await Task.FromResult(result);
        }

        public async Task<Result> ThreeLogin(ThreeLoginModel model)
        {
            if (model.OpenId.IsNullOrEmpty())
            {
                return new Result(ResultCode.Fail, "openId值不能为空");
            }
            var user = await GetByOpenId(model.OpenId, model.LogingType);
            if (user == null)
            {
                if (model.LogingType == LoginType.Guest)
                {
                    var um = new UserBaseInfo
                    {
                        //Username = $"xh_{RandomHelper.GenerateRandomCode(8)}",
                        //Headurl = $"{DomainSetting.ResourceUrl}{_defaultHeaderUrl}",
                        Username = model.NickName,
                        Headurl = model.Avatar,
                        Aid = model.Aid,
                        Sex = (int)model.Sex,
                        AccountType = (int)AccountType.Guest,
                        Creatime = DateTime.Now,
                        OpenId = model.OpenId,
                        UnionId = model.UnionId,
                        Grade = 1
                    };
                    var rlt = await Create(um);
                    var token = CreateToken(rlt.Data, model.OpenId);
                    return new Result { Data = token };
                }

                //Redis.StringSet($"tophone_{user.Phone}", "");
                //新用户信息暂存，下一步进行绑定threeloginbind
                Redis.StringSetWithType($"topenid_{model.OpenId}", model, TimeSpan.FromSeconds(60 * 5 * 24));
                return new Result { Code = (int)ResultCode.NoBind, Message = ResultCode.Success.GetDescription(), Data = model.OpenId };

            }

            if (user.AccountType == 512)
            {
                var tokenGuest = JwtHelper.IssueJWT(new TokenModel() { Phone = model.OpenId, UserId = user.Userid });
                Redis.StringSet("token:" + model.OpenId, tokenGuest, TimeSpan.FromSeconds(60 * 60 * 24));
                return await Task.FromResult(new Result { Data = tokenGuest });
            }
            if (!string.IsNullOrEmpty(user.Phone))
            {
                var token = await ReadToken(user.Phone);
                if (token.IsNullOrEmpty()) token = CreateToken(user.Userid, user.Phone);
                return await Task.FromResult(new Result { Data = token });
            }

            Redis.StringSetWithType($"topenid_{model.OpenId}", model, TimeSpan.FromSeconds(60 * 5 * 24));
            return new Result { Code = (int)ResultCode.NoBind, Message = ResultCode.Success.GetDescription(), Data = model.OpenId };
        }

        private async Task<string> ReadToken(string phone)
        {
            return await Redis.StringGetAsync("token:" + phone);
        }
        private async Task<string> ReadTokenByRegid(string regid)
        {
            return await Redis.StringGetAsync("regid:" + regid);
        }
        [Obsolete("旧版本停止支持后可删除，改用unionid版本")]
        public async Task<string> GetTokenByOpenId(string openId, LoginType loginType)
        {
            try
            {
                var users = await _userCacheService.GetUserList();
                var user = users.Find(u => u.OpenId.IsNotNullOrEmpty() && u.OpenId.Equals(openId, StringComparison.OrdinalIgnoreCase) && u.LoginType == (int)loginType);
                if (user == null) return "";
                if (user.Phone.IsNullOrEmpty()) return "";
                var token = await ReadToken(user.Phone);
                if (token.IsNotNullOrEmpty()) return token;
                token = CreateToken(user.Userid, user.Phone);
                return token;
            }
            catch (Exception ex)
            {
                Logger.Error($"GetTokenByOpenId fail:{openId},{loginType}", ex);
                throw ex;
            }
        }

        public async Task<UserBaseInfo> GetByUnionId(string unionId, LoginType loginType)
        {
            return await _userCacheService.GetUserByUnionId(unionId, loginType, true);
        }

        public async Task<UserBaseInfo> GetByOpenId(string openId, LoginType loginType)
        {
            return await _userCacheService.GetUserByOpenId(openId, loginType, true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unionId"></param>
        /// <param name="loginType"></param>
        /// <returns></returns>
        /// <remarks>改为unionid，逐渐淘汰openid,便于多系统之间登录</remarks>
        public async Task<string> GetTokenByUnionId(string unionId, LoginType loginType)
        {
            var user = await GetByUnionId(unionId, loginType);

            if (user == null) return "";
            if (user.Phone.IsNullOrEmpty()) return "";
            var token = await ReadToken(user.Phone);
            if (token.IsNotNullOrEmpty()) return token;
            token = CreateToken(user.Userid, user.Phone);
            return token;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks>unionid版本</remarks>
        public async Task<Result> LoginWith2fa(LoginWith2faModel model)
        {
            if (model.UnionId.IsNullOrEmpty())
            {
                return new Result(ResultCode.Fail, "UnionId值不能为空");
            }

            var user = await GetByUnionId(model.UnionId, model.LogingType);

            //兼容旧版本使用openid
            if (user == null)
            {
                Logger.Debug($"GetByUnionId未获取到用户，尝试用openId,unionid={model.UnionId},openId:{model.OpenId},loginType={model.LogingType},{model.Platform},{model.AppCode}");
                user = await _userCacheService.GetUserByOpenId(model.OpenId, model.LogingType);
                if (user != null)
                {
                    user.UnionId = model.UnionId;
                    var trlt = await Update(user);
                    Logger.Info($"更新用户({user.Phone}) unionid:{user.UnionId},openId={model.OpenId},ResultCode:{trlt.Code},{DateTime.Now.ToString23()}");
                }
            }


            if (user == null)
            {
                if (model.LogingType == LoginType.Guest)
                {
                    var um = new UserBaseInfo
                    {
                        //创建
                        //Username = $"xh_{RandomHelper.GenerateRandomCode(8)}",
                        //Headurl = $"{DomainSetting.ResourceUrl}{_defaultHeaderUrl}",
                        Username = model.NickName,
                        Headurl = model.Avatar,
                        Aid = model.AgentId,
                        Sex = (int)model.Sex,
                        AccountType = (int)AccountType.Guest,
                        Creatime = DateTime.Now,
                        OpenId = model.OpenId,
                        UnionId = model.UnionId,
                        LoginType = (int)model.LogingType,
                        Grade = 1
                    };
                    var rlt = await Create(um);
                    var token = CreateToken(rlt.Data, model.UnionId);
                    Logger.Debug($"新游客登录：unionid={model.UnionId},openId={model.OpenId},loginType={model.LogingType},{model.Platform},{model.AppCode}");
                    return new Result { Data = token };

                }

                //新用户信息暂存，下一步进行绑定threebind
                Redis.StringSetWithType($"topenid_{model.UnionId}", model, TimeSpan.FromSeconds(60 * 5 * 24));
                Logger.Debug($"新用户登录并准备第三方绑定：unionid={model.UnionId},openId={model.OpenId},loginType={model.LogingType},{model.Platform},{model.AppCode}");
                return new Result(ResultCode.NoBind) { Data = model.UnionId };
            }

            if (user.AccountType == 512)
            {
                var token = CreateToken(user.Userid, model.UnionId);
                Logger.Debug($"游客登录：unionid={model.UnionId},openId={model.OpenId},loginType={model.LogingType},{model.Platform},{model.AppCode}");
                return new Result { Data = token };
            }

            if (!string.IsNullOrEmpty(user.Phone))
            {
                var token = await ReadToken(user.Phone);
                if (token.IsNullOrEmpty()) token = CreateToken(user.Userid, user.Phone);
                Logger.Debug($"用户登录：unionid={model.UnionId},openId={model.OpenId},loginType={model.LogingType},{model.Platform},{model.AppCode}");
                return new Result { Data = token };
            }

            Redis.StringSetWithType($"topenid_{model.UnionId}", model, TimeSpan.FromSeconds(60 * 5 * 24));
            Logger.Debug($"用户准备进行第三方登录绑定：unionid={model.UnionId},openId={model.OpenId},loginType={model.LogingType},{model.Platform},{model.AppCode}");
            return new Result(ResultCode.NoBind) { Data = model.UnionId };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks>union版本绑定，最终替换openid版本</remarks>
        public async Task<Result> LoginWith2faBind(LoginWith2faBindModel para)
        {
            Logger.Debug($"LoginWith2faBind:UnionId={para.UnionId},Phone={para.Phone}");
            if (para.Phone.IsNullOrEmpty())
            {
                return new Result(ResultCode.InvalidPara, "缺失手机号参数");
            }

            if (para.UnionId.IsNullOrEmpty())
            {
                return new Result(ResultCode.InvalidPara, "缺失UnionId参数");
            }

            var userRedis = Redis.StringGetWithType<LoginWith2faModel>($"topenid_{para.UnionId}");

            if (userRedis == null)
            {
                return new Result(ResultCode.Fail, "操作绑定缓存超时");
            }

            string validCode = Redis.StringGet($"SMS:{para.Phone}");

            if (string.IsNullOrEmpty(validCode))
            {
                return new Result(ResultCode.ValidCodeExpired);
            }

            if (para.ValidCode.Trim() != validCode.Trim())
            {
                return new Result(ResultCode.ValidCodeError);
            }

            try
            {
                var user = await GetUserByPhone(para.Phone);
                if (user == null) user = await GetByUnionId(para.UnionId, userRedis.LogingType);

                //兼容旧版本使用openid
                if (user == null)
                {
                    user = await _userCacheService.GetUserByOpenId(userRedis.OpenId, userRedis.LogingType);
                }
                string token;
                if (user == null)
                {
                    var accountType = userRedis.LogingType == LoginType.Guest ? AccountType.Guest : AccountType.Normal;

                    var model = new UserBaseInfo
                    {
                        Username = FormatNickName(userRedis.NickName),
                        Headurl = userRedis.Avatar,
                        Aid = userRedis.AgentId,
                        AccountType = (int)accountType,
                        Phone = para.Phone,
                        Creatime = DateTime.Now,
                        UnionId = para.UnionId,
                        OpenId = userRedis.OpenId,
                        LoginType = (int)userRedis.LogingType,
                        Grade = 1,
                        Sex = (int)userRedis.Sex,
                        Provincial = "",
                        City = "",
                        Address = "",
                        Areas = ""
                    };
                    var rlt = await Create(model);
                    token = CreateToken(rlt.Data, para.Phone);

                    return new Result { Data = token };
                }

                if (user.UnionId.IsNotNullOrEmpty() || user.OpenId.IsNotNullOrEmpty())
                {
                    return new Result(ResultCode.AccountErr);
                }

                user.Headurl = userRedis.Avatar;
                user.LoginType = (int)userRedis.LogingType;
                user.OpenId = userRedis.OpenId;
                user.UnionId = para.UnionId;
                user.Phone = para.Phone;

                await Update(user);
                Logger.Info($"threebind successed:uid:{user.Userid},phone:{para.Phone},unionid:{para.UnionId},{DateTime.Now}");

                token = CreateToken(user.Userid, para.Phone);

                return new Result { Data = token };
            }
            catch (Exception ex)
            {
                Logger.Error("LoginWith2faBind", ex);
                return new Result(ResultCode.Exception, ex.Message);
            }
        }


        [Obsolete("改用unionid版本(LoginWith2faBind),app等旧版本停止支持后可删除")]
        public async Task<Result> ThreeLoginBind(string aid, string openId, string phone, string code)
        {
            if (phone.IsNullOrEmpty())
            {
                return new Result { Code = (int)ResultCode.InvalidPara, Message = "缺失手机号参数" };
            }

            if (openId.IsNullOrEmpty())
            {
                return new Result { Code = (int)ResultCode.InvalidPara, Message = "缺失OpenId参数" };
            }

            var userRedis = Redis.StringGetWithType<ThreeLoginModel>($"topenid_{openId}");

            if (userRedis == null)
            {
                return new Result { Code = (int)ResultCode.Fail, Message = "操作超时" };
            }

            string validCode = Redis.StringGet($"SMS:{phone}");

            if (string.IsNullOrEmpty(validCode))
            {
                return new Result { Code = (int)ResultCode.ValidCodeExpired, Message = ResultCode.ValidCodeExpired.GetDescription() };
            }

            if (code.Trim() != validCode.Trim())
            {
                return new Result { Code = (int)ResultCode.ValidCodeError, Message = ResultCode.ValidCodeError.GetDescription() };
            }

            //DJD:此处有逻辑问题，可能已有第三方游客，手机用户进行第三方绑定的时候会造成第三方游客账户成为垃圾数据，与phone用户拥有同一openid
            var user = await GetUserByPhone(phone, true);
            if (user == null) user = await GetByOpenId(openId, userRedis.LogingType);
            Logger.Debug($"ThreeLoginBind:{aid},{openId},{phone},{code}");
            string token;

            if (user == null)
            {
                var accountType = userRedis.LogingType == LoginType.Guest ? AccountType.Guest : AccountType.Normal;

                var model = new UserBaseInfo
                {
                    Username = FormatNickName(userRedis.NickName),
                    Headurl = userRedis.Avatar,
                    Aid = userRedis.Aid,
                    AccountType = (int)accountType,
                    Phone = phone,
                    Creatime = DateTime.Now,
                    OpenId = userRedis.OpenId,
                    LoginType = (int)userRedis.LogingType,
                    Grade = 1,
                    Sex = (int)userRedis.Sex,
                    Provincial = "",
                    City = "",
                    Address = "",
                    Areas = ""
                };
                var rlt = await Create(model);

                token = CreateToken(rlt.Data, phone);

                return new Result { Data = token };
            }

            if (user.OpenId.IsNotNullOrEmpty() && !user.OpenId.Equals(openId))
            {
                return await Task.FromResult(new Result(ResultCode.AccountErr));
            }

            user.Headurl = userRedis.Avatar;
            user.LoginType = (int)userRedis.LogingType;
            user.OpenId = userRedis.OpenId;
            user.Phone = phone;
            await Update(user);

            Logger.Info($"threebind successed:uid:{user.Userid},phone:{phone},openid:{openId},{DateTime.Now}");

            token = CreateToken(user.Userid, phone);
            return new Result { Data = token };
        }


        public async Task<Result> PhoneChangeAsync(string phone, string code, string oldphone)
        {
            //根据phone取用户信息
            var user = await GetUserByPhone(oldphone);
            if (user == null)
            {
                return new Result(ResultCode.Fail);
            }
            user.Phone = phone;
            var rlt = await Update(user);

            if (rlt.Code == (int)ResultCode.Success)
            {
                var token = JwtHelper.IssueJWT(new TokenModel() { Phone = phone, UserId = user.Userid });
                Redis.StringSet($"token:" + phone, token, TimeSpan.FromSeconds(DomainSetting.TokenExprie));
                Redis.KeyDelete($"token:" + oldphone);
                return await Task.FromResult(new Result { Data = token });
            }
            return new Result(ResultCode.Fail);
        }

        private string FormatNickName(string nickName)
        {
            string pattern = @"[\uD800-\uDBFF][\uDC00-\uDFFF]";
            var reg = new Regex(pattern);
            return reg.Replace(nickName, "");
        }

        public async Task<Result<AddressInfo>> GetAddress(string userId)
        {
            try
            {
                var info = await GetUser(long.Parse(userId));
                var query = await _addressService.Get(info.Userid);
                //var query = await UnitOfWorkService.Execute(() => _addressRepository.GetListAsync());
                // var infoAddressEntity = query.FirstOrDefault(n => n.Userid == info.Userid);
                var rlt = await _addressService.Get(long.Parse(userId));
                if (info == null)
                    return new Result<AddressInfo>()
                    { Code = (int)ResultCode.Fail, Data = new AddressInfo(), Message = "用户地址信息不存在" };
                //var addressInfo = infoAddressEntity.MapTo<AddressInfo>();
                return new Result<AddressInfo>() { Data = rlt.Data };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取用户地址异常,用户Id：{userId}", ex);
                return new Result<AddressInfo>() { Code = (int)ResultCode.Exception, Message = "获取用户地址异常" };
            }

        }

        public async Task<Result<List<AddressInfo>>> GetAddressInfos(long userId)
        {
            try
            {
                var info = await GetUser(userId);
                var query = await _addressService.GetAll();
                //  var query = await UnitOfWorkService.Execute(() => _addressRepository.GetListAsync());
                query = query.Where(t => t.Userid == userId).OrderByDescending(t => t.Iscommonly).ToList();
                if (query == null)
                    return new Result<List<AddressInfo>>()
                    { Code = (int)ResultCode.Fail, Message = "用户地址信息不存在" };
                //var addressInfos = query.MapTo<List<AddressInfo>>();
                return new Result<List<AddressInfo>>() { Data = query };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取用户地址异常,用户Id：{userId}", ex);
                return new Result<List<AddressInfo>>() { Code = (int)ResultCode.Exception, Message = "获取用户地址异常" };
            }

        }
        public async Task<Dictionary<int, long>> GetAllExp(long userId)
        {
            var dic = new Dictionary<int, long>();
            var businessList = EnumHelper.GetEnumValues(typeof(BusinessType));
            foreach (var item in businessList)
            {
                var exp = await _userCacheService.GetExp(userId, Enum.Parse<BusinessType>(item.ToString()));
                dic.Add(item, exp.Code == (int)ResultCode.Success ? exp.Data.Item2 : 0);
            }
            return dic;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Result<UserBaseInfo>> GetUserInfo(long userId)
        {
            try
            {
                var userInfo = await Get(userId, true);
                if (userInfo == null)
                {
                    return new Result<UserBaseInfo>() { Code = (int)ResultCode.Fail, Message = "获取用户信息失败！" };
                }
                else
                {
                    if (userInfo.UseDueDate < System.DateTime.Now)
                    {
                        //查看Redis是否存在匹配项 存在移除token的值
                        string tokenstr = await ReadToken(userInfo.Phone);
                        if (!string.IsNullOrEmpty(tokenstr))
                        {
                            string tokenkey = "token:" + userInfo.Phone;
                            await Redis.KeyDeleteAsync(tokenkey); //删除token缓存
                        }
                        return new Result<UserBaseInfo>() { Code = (int)ResultCode.warning, Message = "此手机号授权使用已过期,暂无使用权限！" };
                    }
                    else
                    {
                        #region 用户地址
                        var detailAddress = userInfo.Provincial + userInfo.City + userInfo.Areas + userInfo.Address;
                        AddressInfo userAddress = null;
                        var userAddressRlt = await GetAddress(userId.ToString());
                        if (userAddressRlt.Code != (int)ResultCode.Success)
                            Logger.Error(userAddressRlt.Message);
                        else
                            userAddress = userAddressRlt.Data;
                        if (userAddress != null)
                        {
                            detailAddress = (string.IsNullOrWhiteSpace(userAddress.Provincial)
                                                ? userInfo.Provincial : userAddress.Provincial) +
                                            (string.IsNullOrWhiteSpace(userAddress.City)
                                                ? userInfo.City : userAddress.City) +
                                            (string.IsNullOrWhiteSpace(userAddress.Areas)
                                                ? userInfo.Areas : userAddress.Areas) +
                                            (string.IsNullOrWhiteSpace(userAddress.Address)
                                                ? userInfo.Address : userAddress.Address);
                        }
                        var userDetailInfo = userInfo.MapTo<UserBaseInfo>();
                        userDetailInfo.DetailAddress = detailAddress;
                        userDetailInfo.IsFirstInputAddress = 1;
                        userDetailInfo.IsFirstCollege = userInfo.IsFirstCollege;
                        //使用时间与课程分类授权
                        userDetailInfo.AuthCateorys = userInfo.AuthCateorys;
                        userDetailInfo.UseDueDate = userInfo.UseDueDate;
                        #endregion
                        #region 会员信息
                        var vipInfoRlt = await _vipService.GetUserVip(userId);
                        userDetailInfo.VipInfo = vipInfoRlt.Code == (int)ResultCode.Success ? vipInfoRlt.Data : null;
                        if (userInfo.IsMember > 0)
                        {
                            userDetailInfo.IsMember = 1;
                            //用户会员等级
                            if (userDetailInfo.VipInfo != null)
                            {
                                if (userDetailInfo.VipInfo.VipType > 0)
                                {
                                    userDetailInfo.Category = (int)userDetailInfo.VipInfo.VipType;
                                }
                                else
                                {
                                    userDetailInfo.Category = 0;
                                }
                            }
                        }
                        #endregion
                        #region 经验
                        userDetailInfo.Experiences = await GetAllExp(userId);
                        long exp = 0;
                        if (userDetailInfo.Experiences != null && userDetailInfo.Experiences.Count > 0)
                            userDetailInfo.Experiences.ForEach(t => exp = exp + t.Value);
                        userDetailInfo.Experience = exp;
                        #endregion
                        #region 等级信息
                        //userDetailInfo.Level = await _captionService.GetLevel(-1, exp);
                        var levels = _levelCacheService.GetUserLevel(userId);
                        userDetailInfo.Levels = levels;
                        #endregion
                        #region 金币
                        var goldRlt = await _userCacheService.GetCoin(userId);
                        if (goldRlt.Code == (int)ResultCode.Success)
                            userDetailInfo.Gold = goldRlt.Data;
                        #endregion
                        #region 用户校区信息
                        var collegeRlt = await _usercollegeService.Get(userId);
                        if (collegeRlt.Code == (int)ResultCode.Success)
                        {
                            if (collegeRlt.Data != null)
                            {
                                userDetailInfo.CollegeInfo = collegeRlt.Data;
                                userDetailInfo.IsFirstCollege = 1;
                            }
                            else
                            {
                                var data = new UserCollegeInfo()
                                {
                                    Id = 0,
                                    UserId = userId,
                                    RealName = "",
                                    SchoolName = "",
                                    PostType = 0,
                                    Provincial = "",
                                    City = "",
                                    Areas = "",
                                    ModeType = 0,
                                    RenextTime = DateTime.Now
                                };
                                userDetailInfo.CollegeInfo = data;
                                userDetailInfo.IsFirstCollege = 0;
                            }
                        }
                        else
                        {

                            var data = new UserCollegeInfo()
                            {
                                Id = 0,
                                UserId = userId,
                                RealName = "",
                                SchoolName = "",
                                PostType = 0,
                                Provincial = "",
                                City = "",
                                Areas = "",
                                ModeType = 0,
                                RenextTime = DateTime.Now
                            };
                            userDetailInfo.CollegeInfo = data;
                            userDetailInfo.IsFirstCollege = 0;
                        }
                        userDetailInfo.PostType = GetPostType();
                        userDetailInfo.ModeType = GetModeType();

                        #endregion
                        return new Result<UserBaseInfo> { Data = userDetailInfo, Message = "获取用户信息成功" };
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取用户{userId}信息异常", ex);
                return new Result<UserBaseInfo> { Code = (int)ResultCode.Exception, Message = "获取用户信息异常" };
            }
        }
        /// <summary>
        /// 岗位
        /// </summary>
        /// <returns></returns>
        private List<CollegeKeyValue> GetPostType()
        {
            var dic = new List<CollegeKeyValue>();
            var PostList = EnumHelper.GetEnumValues(typeof(PostType));
            foreach (var key in PostList)
            {
                var value = EnumHelper.GetDescription((PostType)key);
                var item = new CollegeKeyValue { Key = key, Value = value };
                dic.Add(item);
            }
            return dic;
        }
        /// <summary>
        /// 模式
        /// </summary>
        /// <returns></returns>
        private List<CollegeKeyValue> GetModeType()
        {
            var dic = new List<CollegeKeyValue>();
            var PostList = EnumHelper.GetEnumValues(typeof(ModeType));
            foreach (var key in PostList)
            {
                var value = EnumHelper.GetDescription((ModeType)key);
                var item = new CollegeKeyValue { Key = key, Value = value };
                dic.Add(item);
            }
            return dic;
        }
        /// <summary>
        /// 我的经验
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<Result<MyExpInfo>> GetMyExp(long userId, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var page = await GetValueLogs(userId, ValueCategory.Exp, BusinessType.All, pageIndex, pageSize);
                long exp = 0;
                var allExp = await GetAllExp(userId);
                if (allExp != null && allExp.Count > 0)
                    allExp.ForEach(t => exp = exp + t.Value);
                var maxValue = await _userCacheService.GetExp(userId, BusinessType.All);
                var data = new MyExpInfo()
                {
                    ExpList = page.Data,
                    ExpRule = "",
                    TotalExp = exp,
                    TotalCount = page.Total,
                    PageSize = pageSize,
                    PageIndex = pageIndex
                };
                return new Result<MyExpInfo>() { Data = data };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取我的{userId}经验异常", ex);
                return new Result<MyExpInfo>() { Code = (int)ResultCode.Exception, Message = "获取我的经验异常" };
            }
        }

        public async Task<Result> Delete(long id)
        {
            var entity = await _userCacheService.GetUser(id);
            if (entity != null)
            {
                entity.Isdelete = 1;
                var lrt = await Update(entity);
                return new Result() { Code = lrt.Code, Message = lrt.Message };
            }
            else
            {
                return new Result() { Code = (int)ResultCode.Exception, Message = "未获取到用户信息删除异常" };
            }

        }

        public async Task<Result> Zap(long id)
        {
            var info = await _userCacheService.GetUser(id);

            var rlt = UnitOfWorkService.Execute(() =>
            {
                return _repo.Delete(id);
            });
            if (!rlt)
            {
                Logger.Error($"物理删除用户：{id}失败");
                return new Result() { Code = (int)ResultCode.Exception, Message = "未获取到用户信息删除异常" };
            }
            //Trigger(new UserEventData() { Data = info, Method = (int)EventDataMethod.Del });
            return new Result() { Code = (int)ResultCode.Success, Message = "用户信息删除成功" };
        }


        public List<UserInfoOfMsg> GetListOfMsg(string datetime)
        {
            var rlt = UnitOfWorkService.Execute(() =>
            {
                return _repo.GetListOfMsg(datetime);
            });
            return rlt; ;
        }

        /// <summary>
        /// tob同步商学院账户
        /// </summary>
        /// <param name="aUserTob"></param>
        /// <returns></returns>
        public async Task<Result<long>> ChargeCollegeAsync(AUserTobInput aUserTob)
        {
            try
            {
                string message = "";
                long Uid = 0;
                var rltuserlist = GetAll().Result;
                var rltuser = rltuserlist.Where(p => p.Phone == aUserTob.Phone);
                if (rltuser.Count() <= 0)
                {
                    //创建一个用户
                    #region 注册
                    var resolute = await Create(new UserBaseInfo()
                    {
                        Phone = aUserTob.Phone,
                        LoginType = 1,
                        AccountType = (int)AccountType.Verify,
                        Grade = 1,
                        Creatime = DateTime.Now,
                        Username = aUserTob.UserName,
                        Sex = aUserTob.Sex,
                        Provincial = "",
                        City = "",
                        Address = "",
                        Areas = "",
                        Headurl = _defaultHeaderUrl,
                        Wx = "",
                        Aid = "college",
                        Qq = "",
                        Level = 1,
                        AuthCateorys = "24,25",
                        UseDueDate = DateTime.Now.AddYears(1),
                        Remark = $"盟校导入用户{aUserTob.UserId},同属盟校：{aUserTob.UserId},{aUserTob.UserName}"
                    }); ;
                    #endregion

                    if (resolute.Code != (int)ResultCode.Success && resolute.Data < 0)
                    {
                        Domain.Result result = new Domain.Result();
                        result.Code = (int)ResultCode.Fail;
                        result.Message = "注册失败";
                        Logger.Error($"手机号{aUserTob.Phone}注册失败!,可能原因添加到数据库失败！");
                    }
                    else
                    {
                        Uid = resolute.Data;
                    }
                    message = "用户开通成功";
                }
                else
                {
                    var userinfo = rltuser.FirstOrDefault();
                    Uid = userinfo.Userid;
                    if (aUserTob.isfrozen)
                    {
                        userinfo.AccountType = 1;
                    }
                    else
                    {
                        userinfo.AccountType = 2;
                    }
                    var rlt = await Update(userinfo);
                    message = "用户修改状态成功";
                }
                return new Result<long>() { Code = (int)ResultCode.Success, Message = message, Data = Uid };
            }
            catch (Exception ex)
            {
                Logger.Error($"开通用户：{ex}失败");
                return new Result<long>() { Code = (int)ResultCode.Exception, Message = aUserTob.Phone + "开通异常" };
            }
        }
        /// <summary>
        /// 线下开课账号开通
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public long SeOrCeUid(string phone)
        {
            try
            {
                long Uid = 0;
                var rltuserlist = GetAll().Result;
                var rltuser = rltuserlist.Where(p => p.Phone == phone);
                if (rltuser.Count() <= 0)
                {
                    //创建一个用户
                    #region 注册
                    var resolute = Create(new UserBaseInfo()
                    {
                        Phone = phone,
                        LoginType = 1,
                        AccountType = (int)AccountType.Verify,
                        Grade = 1,
                        Creatime = DateTime.Now,
                        Username = "",
                        Sex = 2,
                        Provincial = "",
                        City = "",
                        Address = "",
                        Areas = "",
                        Headurl = _defaultHeaderUrl,
                        Wx = "",
                        Aid = "college",
                        Qq = "",
                        Level = 1,
                        AuthCateorys = "24,25",
                        UseDueDate = DateTime.Now.AddYears(1),
                        Remark = "CMS:Create New User"
                    }); ;
                    #endregion

                    if (resolute.Result.Code != (int)ResultCode.Success && resolute.Result.Data < 0)
                    {
                        Domain.Result result = new Domain.Result();
                        result.Code = (int)ResultCode.Fail;
                        result.Message = "注册失败";
                        Logger.Error($"手机号{phone}注册失败!,可能原因添加到数据库失败！");
                    }
                    else
                    {
                        Uid = resolute.Result.Data;
                    }
                }
                else
                {
                    var userinfo = rltuser.FirstOrDefault();
                    if (userinfo.AccountType == 1)
                    {
                        userinfo.AccountType = 2;
                        var rlt = Update(userinfo);
                    }
                    Uid = rltuser.FirstOrDefault().Userid;
                }
                return Uid;
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex);
            }
        }
        /// <summary>
        /// 线下开课账号开通
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public long CreateUserOpen(UserOpenReq req)
        {
            try
            {
                long Uid = 0;
                var rltuserlist = GetAll().Result;
                var rltuser = rltuserlist.Where(p => p.Phone == req.phone);
                string authCateorys = "24,25";
                if (rltuser.Count() <= 0)
                {
                    //创建一个用户
                    switch (req.level)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        default:
                            break;
                    }
                    #region 注册
                    var resolute = Create(new UserBaseInfo()
                    {
                        Phone = req.phone,
                        LoginType = 1,
                        AccountType = (int)AccountType.Verify,
                        Grade = 1,
                        Creatime = DateTime.Now,
                        Username = "",
                        Sex = 2,
                        Provincial = "",
                        City = "",
                        Address = "",
                        Areas = "",
                        Headurl = _defaultHeaderUrl,
                        Wx = "",
                        Aid = "college",
                        Level = req.level,
                        AuthCateorys = authCateorys,
                        UseDueDate = req.useDueDate,
                        Qq = "",
                        Remark = "CMS:Create New User"
                    }); ;
                    #endregion

                    if (resolute.Result.Code != (int)ResultCode.Success && resolute.Result.Data < 0)
                    {
                        Domain.Result result = new Domain.Result();
                        result.Code = (int)ResultCode.Fail;
                        result.Message = "注册失败";
                        Logger.Error($"手机号{req.phone}注册失败!,可能原因添加到数据库失败！");
                    }
                    else
                    {
                        Uid = resolute.Result.Data;
                    }
                }
                else
                {
                    var userinfo = rltuser.FirstOrDefault();
                    if (userinfo.AccountType == 1)
                    {
                        userinfo.AccountType = 2;
                    }
                    userinfo.Level = req.level;
                    userinfo.AuthCateorys = authCateorys;
                    userinfo.UseDueDate = req.useDueDate;
                    var rlt = Update(userinfo);
                    Uid = rltuser.FirstOrDefault().Userid;
                }
                return Uid;
            }
            catch (Exception ex)
            {
                return Convert.ToInt32(ex);
            }
        }
        public async Task<Result<UserQueryResultPageModel>> GetUsesPage(UserQueryModel userQueryModel)
        {
            try
            {
                UserQueryResultPageModel data = null;
                DateTime dtStart;
                DateTime dtEnd;
                var users = await _userCacheService.GetUserList();
                var now = DateTime.Now;
                if (userQueryModel.StartTime.IsNullOrEmpty())
                    userQueryModel.StartTime = $"{now.ToString("yyyy-MM-dd")} 00:00:00";
                if (userQueryModel.EndTime.IsNullOrEmpty())
                    userQueryModel.EndTime = now.ToString("yyyy-MM-dd HH:mm:ss");
                if (!DateTime.TryParse(userQueryModel.StartTime, out dtStart))
                    return new Result<UserQueryResultPageModel> { Code = (int)ResultCode.Fail, Message = "开始时间格式不正确！" };
                if (!DateTime.TryParse(userQueryModel.EndTime, out dtEnd))
                    return new Result<UserQueryResultPageModel> { Code = (int)ResultCode.Fail, Message = "结束时间格式不正确！" };

                if (dtStart == DateTime.MinValue || dtStart > now)
                    dtStart = DateTime.Parse($"{now.ToString("yyyy-MM-dd")} 00:00:00");
                if (dtEnd == DateTime.MinValue || dtEnd > now)
                    dtEnd = now;
                if (dtEnd < dtStart)
                    return new Result<UserQueryResultPageModel> { Code = (int)ResultCode.Fail, Message = "开始时间大于结束时间！" };

                var list = UnitOfWorkService.Execute(() => { return _repo.GetList(t => t.Creatime >= dtStart && t.Creatime <= dtEnd); });

                if (userQueryModel.AccountType > -1)
                {
                    if (userQueryModel.AccountType > 1)
                    {
                        list = list.Where(t => t.AccountType == userQueryModel.AccountType);
                    }
                    else
                    {
                        list = list.Where(t => t.AccountType == 0 || t.AccountType == 1);
                    }
                }
                if (userQueryModel.IsMember > -1)
                    list = list.Where(t => t.IsMember == userQueryModel.IsMember);
                if (userQueryModel.UserName.IsNotNullOrEmpty())
                {
                    var tmpUsers = users.FindAll(t => t.Username.IsNotNullOrEmpty());
                    var userInfo = tmpUsers.Where(t => t.Username.Contains(userQueryModel.UserName.Trim()));
                    var ids = userInfo.Select(t => t.Userid).ToList();
                    if (tmpUsers != null)
                        list = list.Where(t => ids.Contains(t.Id));
                }
                if (userQueryModel.Phone.IsNotNullOrEmpty())
                {
                    var tmpUsers = users.FindAll(t => t.Phone.IsNotNullOrEmpty());
                    var userInfo = tmpUsers.Where(t => t.Phone.Contains(userQueryModel.Phone.Trim()));
                    var ids = userInfo.Select(t => t.Userid).ToList();
                    if (tmpUsers != null)
                        list = list.Where(t => ids.Contains(t.Id));
                }
                list = list.OrderByDescending(t => t.Creatime).ToList();
                var entitys = list.Page(userQueryModel.PageSize, userQueryModel.PageIndex);
                if (entitys != null && entitys.Items != null && entitys.Items.Length > 0)
                {
                    data = new UserQueryResultPageModel();
                    data.Rows = entitys.Items.ToList().MapTo<List<UserQueryResultModel>>();
                    data.PageSize = userQueryModel.PageSize;
                    data.PageIndex = userQueryModel.PageIndex;
                    data.TotalItemCount = list.Count();
                }
                //if (data != null && data.Rows != null && data.Rows.Count > 0)
                //{
                //    for (int i = 0; i < data.Rows.Count; i++)
                //    {
                //        var userInfo = users.Find(t => t.Userid == data.Rows[i].Id);
                //        if (userInfo != null)
                //        {
                //            data.Rows[i].UserName = userInfo.Username;
                //            data.Rows[i].Phone = userInfo.Phone;
                //        }
                //    }
                //}
                return new Result<UserQueryResultPageModel> { Data = data };
            }
            catch (Exception ex)
            {
                return new Result<UserQueryResultPageModel>
                { Code = (int)ResultCode.Exception, Message = ex.Message };
            }
        }


    }
}
