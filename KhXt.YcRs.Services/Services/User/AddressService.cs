﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using Hx.Extensions;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using Senparc.NeuChar.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services
{
    public class AddressService : ServiceBase, IAddressService
    {
        IAddressRepository _repo;
        private readonly IDirectMemoryCache _cache;
        private readonly string _addressCacheKey = "Address";
        public AddressService(IAddressRepository repo, IDirectMemoryCacheManager cacheManager)
        {
            _repo = repo;
            _cache = cacheManager.GetCache("__AddressCache");
        }

        #region 复用缓存查询方法
        public async Task<List<AddressInfo>> GetAll()
        {
            try
            {
                var rlt = _cache.Get(_addressCacheKey, async () =>
                {
                    var body = await UnitOfWorkService.Execute(async () =>
                    {
                        var list = await _repo.GetListAsync();
                        var taskList = list.MapTo<List<AddressInfo>>();
                        return taskList;
                    });
                    return body;
                }, TimeSpan.FromHours(8));

                return await rlt;
            }
            catch (Exception ex)
            {
                Logger.Error("收回地址列表缓存异常", ex);
                return null;
            }

        }

        #endregion
        #region  DoSync增删修改缓存

        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is AddressEventData data)) return;
            var method = (EventDataMethod)data.Method;
            if (method == EventDataMethod.Clear)
            {
                ClearCache();
            }
        }
        public void RebuildCache()
        {
            ClearCache();
            //Trigger(new AddressEventData() { Method = (int)EventDataMethod.Clear });
        }
        public override void ClearCache()
        {
            _cache.Clear();
            GetAll().GetAwaiter().GetResult();
        }

        public long Add(AddressInfo Info)
        {
            var entity = Info.MapTo<AddressEntity>();
            entity.Createtime = DateTime.Now;
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            if (body > 0) RebuildCache();
            return body;
        }
        public bool Update(AddressInfo Info)
        {
            var entity = Info.MapTo<AddressEntity>();
            entity.Updatetime = DateTime.Now;
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var tempEntity = _repo.Get(Info.Id);
                if (tempEntity != null)
                {
                    return _repo.Update(entity);
                }
                else
                {
                    return false;
                }

            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(AddressInfo Info)
        {
            var entity = Info.MapTo<AddressEntity>();
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                var tempEntity = _repo.Get(Info.Id);
                if (tempEntity != null)
                {
                    return _repo.Delete(entity);
                }
                else
                {
                    return false;
                }
            });
            if (body)
            {
                RebuildCache();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool AddressClearCache()
        {
            RebuildCache();
            return true;
        }
        #endregion
        #region 自定义查询方法

        public async Task<IQueryable<AddressInfo>> Table()
        {
            var List = await GetAll();
            return List.AsQueryable();

        }
        public async Task<Result<AddressInfo>> Get(long Userid)
        {
            try
            {
                var list = await GetAll();
                var info = list.Find(t => t.Userid == Userid);
                return await Task.FromResult(new Result<AddressInfo>() { Data = info });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Logger.Error($"获取地址{Userid}异常！", exception);
                return new Result<AddressInfo> { Code = (int)ResultCode.Exception, Message = $"获取地址异常" };
            }
        }


        #endregion


    }

}
