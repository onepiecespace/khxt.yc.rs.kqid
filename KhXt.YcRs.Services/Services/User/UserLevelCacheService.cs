﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.EventData;
using KhXt.YcRs.Domain.EventData.User;
using KhXt.YcRs.Domain.Models.User;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using KhXt.YcRs.Domain.Util;
using Hx.ObjectMapping;
using Hx.Runtime.Caching;
using Hx.Runtime.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.User
{
    public class UserLevelCacheService : ServiceBase, IUserLevelCacheService
    {
        private readonly IDirectMemoryCache _cache;
        private readonly IUserLevelRepository _repo;
        private readonly string _usersLevelCacheKey = "usersLevel";
        public UserLevelCacheService()
        {

        }
        public UserLevelCacheService(IDirectMemoryCacheManager cacheManager, IUserLevelRepository userLevelRepository)
        {
            _repo = userLevelRepository;
            _cache = cacheManager.GetCache("__userLevelCache");
        }

        public List<UserLeveModel> GetList()
        {
            try
            {
                var levels = _cache.Get(_usersLevelCacheKey, () =>
                {
                    var list = UnitOfWorkService.Execute(() => _repo.GetList()).ToList();
                    return list.MapTo<List<UserLeveModel>>();

                }, TimeSpan.FromHours(24));
                return levels;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取用户等级异常");
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Dictionary<int, int> GetUserLevel(long userId)
        {
            try
            {
                var dic = new Dictionary<int, int>();
                var list = GetList();
                if (list == null || list.Count <= 0) return dic;
                var businessList = EnumHelper.GetEnumValues(typeof(BusinessType));
                foreach (var item in businessList)
                {
                    var entity = list.FirstOrDefault(t => t.BusinessType.Equals(item) && t.UserId == userId);
                    dic.Add(item, entity == null ? 1 : entity.Level);
                }
                return dic;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取用户{userId}等级异常");
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="businessType"></param>
        /// <returns></returns>
        public int GetUserLevel(long userId, int businessType)
        {
            try
            {
                var dic = GetUserLevel(userId);
                if (dic != null && dic.Count > 0 && dic.ContainsKey(businessType))
                    return dic[businessType];
                else
                    return 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error($"获取用户{userId}指定模块{businessType}等级异常");
                throw;
            }
        }

        protected override void DoSync(IDomainEventData eventData)
        {
            if (!(eventData is UserLevelEventData data)) return;
            if (data.Data == null) return;

            var method = (EventDataMethod)data.Method;
            var list = GetList();
            switch (method)
            {
                case EventDataMethod.Clear:
                    ClearCache();
                    break;
                case EventDataMethod.Add:
                    if (list != null)
                    {
                        list.Add(data.Data);
                    }
                    break;
                case EventDataMethod.Del:
                    if (list != null)
                    {
                        var model = list.Find(t => t.Id == data.Data.Id);
                        if (model != null)
                        {
                            list.Remove(model);
                        }
                    }
                    break;
                case EventDataMethod.Update:
                    if (list != null)
                    {
                        var model = list.Find(t => t.Id == data.Data.Id);
                        if (model != null)
                        {
                            list.Remove(model);
                        }
                        list.Add(data.Data);
                    }
                    break;
            }

        }
        public bool Update(UserLeveModel model)
        {
            if (model == null)
                return false;
            var entity = model.MapTo<UserLeveEntity>();
            if (entity == null)
                return false;
            var rlt = UnitOfWorkService.Execute<bool>(() => _repo.Update(entity));
            if (!rlt)
                return false;
            var list = GetList();
            var info = list.Find(t => t.Id == model.Id);
            list.Remove(info);
            list.Add(model);
            //Trigger(new UserLevelEventData() { Data = model, Method = (int)EventDataMethod.Update });
            return true;
        }

        public long Add(UserLeveModel model)
        {
            if (model == null)
                return 0;
            var entity = model.MapTo<UserLeveEntity>();
            if (entity == null)
                return 0;
            var rlt = UnitOfWorkService.Execute<long>(() => _repo.InsertAndGetId(entity));
            if (rlt < 1)
                return rlt;
            model.Id = rlt;
            var list = GetList();
            list.Add(model);
            //Trigger(new UserLevelEventData() { Data = model, Method = (int)EventDataMethod.Add });
            return rlt;
        }

        public bool Delete(UserLeveModel model)
        {
            if (model == null)
                return false;
            var entity = model.MapTo<UserLeveEntity>();
            if (entity == null)
                return false;
            var rlt = UnitOfWorkService.Execute<bool>(() => _repo.Delete(entity));
            if (!rlt)
                return false;
            var list = GetList();
            var info = list.Find(t => t.Id == entity.Id);
            if (info != null)
                list.Remove(info);
            //Trigger(new UserLevelEventData() { Data = model, Method = (int)EventDataMethod.Del });
            return true;
        }
        public override void ClearCache()
        {
            _cache.Clear();
            GetList();
        }

    }
}
