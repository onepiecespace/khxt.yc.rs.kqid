﻿
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Services.User
{
    public class UserThreePartyService : ServiceBase, IUserThreePartyService
    {
        IUserThreePartyRepository _repo;

        public UserThreePartyService(IUserThreePartyRepository repo)
        {
            _repo = repo;
        }


        public IQueryable<UserThreePartyEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<UserThreePartyEntity>>(() =>
               {
                   return _repo.GetList().AsQueryable();
               });
            return body;
        }

    }









}
