﻿using HuangLiCollege.Common;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using Hx.ObjectMapping;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Services.User
{
    public class UserFeedbackService : ServiceBase, IUserFeedbackService
    {
        IUserFeedbackRepository _repo;

        public UserFeedbackService(IUserFeedbackRepository repo)
        {
            _repo = repo;
        }

        #region IComBaseService
        public IQueryable<UserFeedbackEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<UserFeedbackEntity>>(() =>
               {
                   return _repo.GetList().AsQueryable();
               });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(UserFeedbackInfo info)
        {
            var entity = info.MapTo<UserFeedbackEntity>();
            entity.CreateTime = DateTime.Now;
            entity.PicPath = RemoveResUrl(info.PicPath);
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }



        public void Delete(UserFeedbackEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(entity);
            });
        }

        public void Delete(Expression<Func<UserFeedbackEntity, bool>> predicate)
        {

            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Delete(predicate);
            });
        }



        public IQueryable<UserFeedbackEntity> Fetch(Expression<Func<UserFeedbackEntity, bool>> predicate, Action<Orderable<UserFeedbackEntity>> order)
        {
            var body = UnitOfWorkService.Execute<IQueryable<UserFeedbackEntity>>(() =>
           {
               if (order == null)
               {
                   return Table().Where(predicate);
               }
               var deferrable = new Orderable<UserFeedbackEntity>(Table().Where(predicate));
               order(deferrable);
               return deferrable.Queryable;

           });
            return body;

        }

        public UserFeedbackEntity Get(object id)
        {
            var body = UnitOfWorkService.Execute<UserFeedbackEntity>(() =>
           {
               long.TryParse(id.ToString(), out long nid);
               return _repo.Get(nid);

           });
            return body;

        }


        /// <summary>
        /// 需要优化，这里要想执行条件在分页，目前没有什么好的方式继承
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageList<UserFeedbackEntity> Gets(Expression<Func<UserFeedbackEntity, bool>> predicate, Action<Orderable<UserFeedbackEntity>> order, int pageSize, int pageIndex)
        {
            var body = UnitOfWorkService.Execute<PageList<UserFeedbackEntity>>(() =>
           {
               if (order == null)
               {
                   return new PageList<UserFeedbackEntity>(Table().Where(predicate), pageIndex, pageSize);
               }
               var deferrable = new Orderable<UserFeedbackEntity>(Table().Where(predicate));
               order(deferrable);
               var ts = deferrable.Queryable;
               var totalCount = ts.Count();
               var pagingTs = ts.Skip((pageIndex - 1) * pageSize).Take(pageSize);

               return new PageList<UserFeedbackEntity>(pagingTs, pageIndex, pageSize, totalCount);

           });
            return body;
        }

        public void Update(UserFeedbackEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }
        #endregion
    }









}
