﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Entities.User;
using KhXt.YcRs.Domain.Repositories.User;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.User;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace KhXt.YcRs.Services
{
    public class UserBankService : ServiceBase, IUserBankService
    {
        IUserBankRepository _repo;

        public UserBankService(IUserBankRepository repo)
        {
            _repo = repo;
        }

        #region IComBaseService
        public IQueryable<UserBankEntity> Table()
        {
            var body = UnitOfWorkService.Execute<IQueryable<UserBankEntity>>(() =>
                  {
                      return _repo.GetList().AsQueryable();
                  });
            return body;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public object Create(UserBankEntity entity)
        {
            var body = UnitOfWorkService.Execute<long>(() =>
            {
                return _repo.InsertAndGetId(entity);
            });
            return body;

        }

        public void Update(UserBankEntity entity)
        {
            var body = UnitOfWorkService.Execute<bool>(() =>
            {
                return _repo.Update(entity);
            });

        }
        #endregion
    }
}
