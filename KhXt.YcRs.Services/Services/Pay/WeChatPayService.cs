﻿using Essensoft.AspNetCore.Payment.WeChatPay;
using Essensoft.AspNetCore.Payment.WeChatPay.Request;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Models;
using KhXt.YcRs.Domain.Models.Pay;
using KhXt.YcRs.Domain.Services;
using KhXt.YcRs.Domain.Services.Pay;
using Hx.ObjectMapping;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.Services.Pay
{
    public class WeChatPayService : ServiceBase, IWeChatPayService
    {
        private readonly IWeChatPayClient _client;
        private readonly IOptions<WeChatPayOptions> _optionsAccessor;
        public WeChatPayService(IWeChatPayClient client, IOptions<WeChatPayOptions> optionsAccessor)
        {
            _client = client;
            _optionsAccessor = optionsAccessor;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="weChatPay"></param>
        /// <returns></returns>
        public async Task<Result<WeChatPayResult>> AppPay(WeChatPayUnifiedOrderRequest weChatPay)
        {
            try
            {
                var response = await _client.ExecuteAsync(weChatPay, _optionsAccessor.Value);
                var data = new PayWeChatPayResult();
                if (response.ReturnCode == WeChatPayCode.Success && response.ResultCode == WeChatPayCode.Success)
                {
                    var req = new WeChatPayAppSdkRequest
                    {
                        PrepayId = response.PrepayId
                    };
                    var parameter = await _client.ExecuteAsync(req, _optionsAccessor.Value);
                    // 将参数(parameter)给 ios/android端 让他调起微信APP(https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=8_5)
                    data.Parameter = JsonConvert.SerializeObject(parameter);
                    var result = JsonConvert.DeserializeObject<WeChatPayResult>(data.Parameter);
                    result.ResultCode = response.ReturnCode;
                    result.CodeUrl = response.CodeUrl;
                    result.MwebUrl = response.MwebUrl;
                    return new Result<WeChatPayResult>() { Data = result };
                }
                data.Response = response.ResponseBody;
                return new Result<WeChatPayResult>() { Code = (int)ResultCode.Fail, Message = data.Response };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("调用微信支付异常", ex);
                return new Result<WeChatPayResult>() { Code = (int)ResultCode.Exception, Message = "调用微信支付异常" };
            }
        }

        /// <summary>
        /// 微信h5内部拉起
        /// </summary>
        /// <param name="weChatPay"></param>
        /// <returns></returns>
        public async Task<Result<WeChatPayResult>> InnerPay(WeChatPayUnifiedOrderRequest weChatPay)
        {
            try
            {
                _optionsAccessor.Value.AppId = _optionsAccessor.Value.GHAppId;
                _optionsAccessor.Value.Secret = _optionsAccessor.Value.GHSecret;
                var response = await _client.ExecuteAsync(weChatPay, _optionsAccessor.Value);
                var data = new PayWeChatPayResult();
                if (response.ReturnCode == WeChatPayCode.Success && response.ResultCode == WeChatPayCode.Success)
                {
                    var req = new WeChatPayJsApiSdkRequest
                    {
                        Package = "prepay_id=" + response.PrepayId
                    };
                    var parameter = await _client.ExecuteAsync(req, _optionsAccessor.Value);
                    // 将参数(parameter)给 ios/android端 让他调起微信APP(https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=8_5)
                    data.Parameter = JsonConvert.SerializeObject(parameter);
                    var result = JsonConvert.DeserializeObject<WeChatPayResult>(data.Parameter);
                    result.ResultCode = response.ReturnCode;
                    result.CodeUrl = response.CodeUrl;
                    result.MwebUrl = response.MwebUrl;
                    return new Result<WeChatPayResult>() { Data = result };
                }
                data.Response = response.ResponseBody;
                return new Result<WeChatPayResult>() { Code = (int)ResultCode.Fail, Message = data.Response };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Error("调用微信支付异常", ex);
                return new Result<WeChatPayResult>() { Code = (int)ResultCode.Exception, Message = "调用微信支付异常" };
            }
        }
    }
}
