﻿using ds;
using KhXt.YcRs.Domain.Consumers;
using KhXt.YcRs.Domain.Contracts;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KhXt.YcRs.DomainService.Consumers
{
    class VisitStatConsumer : ConsumerBase<VisitStatContract>
    {

        public override async Task Consume(ConsumeContext<VisitStatContract> context)
        {
#if DEBUG
            await Console.Out.WriteLineAsync($"{context.SentTime},{context.Message.Path},{context.Message.Lentgh}");
#endif
            await BufferHelper.ResponseStatBuffer.EnqueueMessage(new KeyValuePair<string, long>(context.Message.Path.ToLower(),context.Message.Lentgh));
        }
    }
}
