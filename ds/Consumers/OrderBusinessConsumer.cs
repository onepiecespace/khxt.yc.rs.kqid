﻿using System;
using System.Threading.Tasks;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Consumers;
using KhXt.YcRs.Domain.Contracts;
using KhXt.YcRs.Domain.Services.Order;
using Hx.Components;
using MassTransit;

namespace KhXt.YcRs.DomainService.Consumers
{
    [PropertyWired]
    class OrderBusinessConsumer : ConsumerBase<OrderBusinessContract>
    {
        /// <summary>
        /// 
        /// </summary>
        public IOrderBusinessService OrderBusinessService { get; set; }

        public override async Task Consume(ConsumeContext<OrderBusinessContract> context)
        {
#if DEBUG
            Console.WriteLine($"{context.Message.OrderId}");
#endif
            try

            {
                var model = context.Message;
                var rlt = await OrderBusinessService.DealWithOrder(model.UserId, model.OrderId, model.BusinessType,
                    model.IsAutoRenew);
                var key = RedisKeyHelper.CreateOrderStatus();
                await Redis.HashSetAsync(key, model.OrderId, rlt.Data);
            }
            catch (Exception ex)
            {
                Logger.Error($"处理订单业务失败,{context.Message}", ex);
            }

        }
    }


}
