﻿using KhXt.YcRs.Domain.Consumers;
using KhXt.YcRs.Domain.Contracts;
using KhXt.YcRs.Domain.Entities.Sys;
using KhXt.YcRs.Domain.Repositories.Sys;
using Hx.Components;
using Hx.Domain.Services;
using Hx.Logging;
using Hx.MQ;
using Hx.ObjectMapping;
using Hx.Redis;
using Hx.Serializing;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.DomainService.Consumers
{
    [PropertyWired]
    class SmsWriteConsumer : ConsumerBase<SmsContract>
    {

        public ISmsRepository Repo { get; set; }
        public override async Task Consume(ConsumeContext<SmsContract> context)
        {
            var entity = context.Message.MapTo<SmsEntity>();
            entity.CreateTime = DateTime.Now;
            var nid=await UnitOfWorkService.Execute(async () =>
            {
                var rlt = await Repo.InsertAndGetIdAsync(entity);
                return rlt;
            });
            if (nid <= 0)
            {
                Logger.Error($"SmsWriteConsumer 写入失败 {Serializer.Serialize(context.Message)}");
            }
        }
    }
}
