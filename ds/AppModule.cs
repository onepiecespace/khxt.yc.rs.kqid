﻿using GreenPipes;
using KhXt.YcRs.Domain;
using KhXt.YcRs.DomainService.Consumers;
using KhXt.YcRs.DomainService.Jobs;
using Hx.Modules;
using Hx.MQ;
using Hx.QuartzCore;
using Quartz;
using System;
using Hx.Components;
using Microsoft.Extensions.Configuration;
using KhXt.YcRs.Domain.Services.Files;

namespace ds
{
    [DependsOn(
        typeof(DomainModule)
        , typeof(MqModule)
        , typeof(QuartzModule)
    )]
    class AppModule : ModuleBase
    {
        public override void PreInitialize()
        {
            Register<SmsWriteConsumer>();
            //Register<AudioSynthesisConsumer>();
            //Register<AudioSynVCRConsumer>();
            //Register<ReciteCompareConsumer>();
            Register<OrderBusinessConsumer>();
            Register<VisitStatConsumer>();
            //Register<ITextCompare, TextCompare>(null, LifeStyle.Transient);
            RegisterJobs();
        }

        public override void Initialize()
        {
            var cfg = Resolve<IConfiguration>();
            //var hxsetting = cfg.GetSection("HuanXinSettings").Get<HuanXinSetting>();
            //ObjectContainer.RegisterInstance(hxsetting);
            var aliOssConfig = cfg.GetSection("AliOssConfig").Get<OssOptions>();
            ObjectContainer.RegisterInstance(aliOssConfig);

            RegisterConsumers();
        }

        public override void BeforePostInitialize()
        {
            BufferHelper.Init();
            CreateJobs();
        }

        public override void Shutdown()
        {
            BufferHelper.ResponseStatBuffer.ClearAndStop();
        }

        private void RegisterConsumers()
        {
            var config = Configuration.Modules.StartupConfiguration.Modules.Mq().ConsumerConfigurator;
            config.AddConsumer<SmsWriteConsumer>(cfg =>
            {
                cfg.UseConcurrencyLimit(Environment.ProcessorCount);
                cfg.PrefetchCount = (ushort)(Environment.ProcessorCount * 2);
            });
            //config.AddConsumer<AudioSynthesisConsumer>(cfg =>
            //{
            //    cfg.UseConcurrencyLimit(Environment.ProcessorCount);
            //    cfg.PrefetchCount = (ushort)(Environment.ProcessorCount * 2);
            //});
            //config.AddConsumer<AudioSynVCRConsumer>(cfg =>
            //{
            //    cfg.UseConcurrencyLimit(Environment.ProcessorCount);
            //    cfg.PrefetchCount = (ushort)(Environment.ProcessorCount * 2);
            //});
            //config.AddConsumer<ReciteCompareConsumer>(cfg =>
            //{
            //    cfg.UseConcurrencyLimit(Environment.ProcessorCount);
            //    cfg.PrefetchCount = (ushort)(Environment.ProcessorCount * 1);
            //});
            config.AddConsumer<OrderBusinessConsumer>(cfg =>
            {
                cfg.UseConcurrencyLimit(Environment.ProcessorCount);
                cfg.PrefetchCount = (ushort)(Environment.ProcessorCount * 2);
            });

            config.AddConsumer<VisitStatConsumer>(cfg =>
            {
                cfg.UseConcurrencyLimit(Environment.ProcessorCount);
                cfg.PrefetchCount = (ushort)(Environment.ProcessorCount * 2);
            });
        }

        private void RegisterJobs()
        {
            Register<RankingBoardJob>();
            Register<ReciteRankingBoardJob>();
            //Register<MarketingBoardJob>();
            Register<CleanupJob>();
        }

        private void CreateJobs()
        {
            var schMgr = Resolve<IQuartzScheduleJobManager>();
            schMgr.ScheduleAsync<RankingBoardJob>(job =>
            {
                job.WithIdentity("job_rb_redis", "grp_rb_redis");
            }, tr =>
            {
#if DEBUG
                tr.
                    StartAt(DateTimeOffset.Now.AddMinutes(1))
                    .WithSimpleSchedule(x =>
                    {
                        x.
                            WithIntervalInMinutes(1)
                            .RepeatForever();
                    });
#else
                tr
                    .WithIdentity("tr_rb_redis", "tr_rb_redis")
                    .WithDailyTimeIntervalSchedule(x =>
                    {
                        x
                            .OnEveryDay()
                            .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 5))
                            .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 20))
                            .WithIntervalInHours(1);
                    });
#endif
            });

            schMgr.ScheduleAsync<ReciteRankingBoardJob>(job =>
            {
                job.WithIdentity("job_recite_rb_redis", "grp_recite_rb_redis");
            }, tr =>
            {
#if DEBUG
                tr.
                    StartAt(DateTimeOffset.Now.AddMinutes(1))
                    .WithSimpleSchedule(x =>
                    {
                        x.
                            WithIntervalInMinutes(1)
                            .RepeatForever();
                    });
#else
                tr
                    .WithIdentity("tr_recite_rb_redis", "tr_recite_rb_redis")
                    .WithDailyTimeIntervalSchedule(x =>
                    {
                        x
                            .OnEveryDay()
                            .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 5))
                            .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 20))
                            .WithIntervalInHours(1);
                    });
#endif
            });

//            schMgr.ScheduleAsync<MarketingBoardJob>(job =>
//            {
//                job.WithIdentity("job_mk_redis", "grp_mk_redis");
//            }, tr =>
//            {
//#if DEBUG
//                tr.StartAt(DateTimeOffset.Now.AddMinutes(1)).WithSimpleSchedule(x => x.WithIntervalInMinutes(1).RepeatForever());
//#else
//                tr.WithIdentity("tr_mk_redis", "tr_mk_redis").WithDailyTimeIntervalSchedule(x =>
//                    {
//                        x.OnEveryDay().
//                            StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 5))
//                            .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 20)).WithIntervalInHours(1);
//                    });
//#endif
//            });

            schMgr.ScheduleAsync<CleanupJob>(job =>
            {
                job.WithIdentity("job_cl_redis", "grp_cl_redis");
            }, tr =>
            {
#if DEBUG
                tr.StartAt(DateTimeOffset.Now.AddMinutes(1)).WithSimpleSchedule(x => x.WithIntervalInMinutes(1).RepeatForever());
#else
                tr.WithIdentity("tr_cl_redis", "tr_cl_redis").WithDailyTimeIntervalSchedule(x =>
                    {
                        x.OnEveryDay().
                            StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 5))
                            .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 20)).WithIntervalInHours(1);
                    });
#endif
            });
        }
    }
}
