﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Domain.Services.Sys;
using Hx.Components;
using Hx.Redis;
using Hx.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace ds
{
    internal static class BufferHelper
    {
        internal static BufferQueue<KeyValuePair<string,long>> ResponseStatBuffer;

        internal static void Init()
        {
            CreateResponseStatBuffer();
        }

        private static void CreateResponseStatBuffer()
        {
            ResponseStatBuffer = new BufferQueue<KeyValuePair<string, long>>("", 50, null, async data => {
                var svr = ObjectContainer.Resolve<ISysService>();
                await svr.SaveVisit(data.ToList());
            }, 3000);
        }


    }

}
