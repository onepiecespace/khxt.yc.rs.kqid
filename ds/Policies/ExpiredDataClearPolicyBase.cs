﻿using KhXt.YcRs.Domain.Policies;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.DomainService.Policies
{
    interface IExpiredDataClearPolicy : IPolicy
    {

    }

    abstract class ExpiredDataClearPolicyBase : IPolicy
    {
        public string Name { get; set; }

    }
}
