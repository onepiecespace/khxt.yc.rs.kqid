﻿using KhXt.YcRs.Domain;
using Hx.Extensions;
using Hx.QuartzCore;
using Hx.Redis;
using Quartz;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.DomainService.Jobs
{
    class RankingBoardJob : JobBase
    {
        private RedisHelper _redis;
        public RankingBoardJob(RedisHelper redisHelper)
        {
            _redis = redisHelper;
        }
        public override async Task Execute(IJobExecutionContext context)
        {
            var key = RedisKeyHelper.CreateExpKey(BusinessType.Dictation);
            var lastKey = RedisKeyHelper.CreateLastExpKey(BusinessType.Dictation);

            var rlt=await _redis.Copy(key, lastKey);
            if (!rlt)
            {
                Logger.Error($"RankingBoardJob fail:redis copy 未成功");
            }

            Logger.Debug($"RankingBoardJob run at :{DateTime.Now.ToString23()} with result :{rlt}");
        }
    }
}
