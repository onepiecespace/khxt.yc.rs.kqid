﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using KhXt.YcRs.Domain;
using Hx.Extensions;
using Hx.QuartzCore;
using Hx.Redis;
using Quartz;

namespace KhXt.YcRs.DomainService.Jobs
{
    /// <summary>
    /// 
    /// </summary>
    class ReciteRankingBoardJob : JobBase
    {
        private readonly RedisHelper _redis;

        public ReciteRankingBoardJob(RedisHelper redis)
        {
            _redis = redis;
        }
        public override async Task Execute(IJobExecutionContext context)
        {
            //copy 总排行
            const string key = RedisKeyHelper.SSET_RANKRECITE;
            const string lastKey = RedisKeyHelper.SSET_RANKRECITELAST;

            var rlt = await _redis.Copy(key, lastKey);
            if (!rlt)
            {
                Logger.Error($"背诵总排行 fail:redis copy 未成功");
            }
            Logger.Debug($"背诵总排行 run at :{DateTime.Now.ToString23()} with result :{rlt}");
            for (var i = 1; i < 7; i++)
            {
                try
                {
                    var keyGrade = RedisKeyHelper.CreateReciteRankKey(i.ToString());
                    var keyGradeLast = RedisKeyHelper.CreateReciteRankLastKey(i.ToString());
                    if (!_redis.KeyExists(keyGrade)) continue;
                    var rltGrade = await _redis.Copy(keyGrade, keyGradeLast);
                    if (!rltGrade)
                    {
                        Logger.Error($"背诵{i.ToString()}年级排行 fail:redis copy 未成功");
                    }
                    Logger.Debug($"背诵{i.ToString()}年级排行 run at :{DateTime.Now.ToString23()} with result :{rltGrade}");
                }
                catch (Exception ex)
                {
                    Logger.Debug($"背诵{i.ToString()}年级排行 run at :{DateTime.Now.ToString23()} with result :{ex.Message}");
                }

            }

        }
    }
}
