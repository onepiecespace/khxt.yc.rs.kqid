﻿using KhXt.YcRs.Domain;
using Hx.QuartzCore;
using Hx.Redis;
using Quartz;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhXt.YcRs.DomainService.Jobs
{
    class CleanupJob : JobBase
    {
        private readonly RedisHelper _redis;
        public CleanupJob(RedisHelper redis)
        {
            _redis = redis;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var isSmsLimitIpHasKey = await _redis.KeyExistsAsync(RedisKeyHelper.SMS_SPEED_LIMIT_IP);
                if (isSmsLimitIpHasKey)
                    await _redis.KeyDeleteAsync(RedisKeyHelper.SMS_SPEED_LIMIT_IP);
            }
            catch (Exception ex)
            {
                Logger.Debug($"清理Redis出错,错误:{ex.Message}");
            }
        }
    }
}
