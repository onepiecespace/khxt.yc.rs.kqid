﻿using Hx;
using System;
using Microsoft.Extensions.Configuration;

namespace ds
{
    class Program
    {
        static void Main(string[] args)
        {
            var parser = Hx.Utilities.CommandLineArgumentParser.Parse(args);
            var env = "";
            try
            {
                env=Environment.GetEnvironmentVariable("DOMAIN_ENVIRONMENT");
            }
            catch { }
            Console.WriteLine($"DOMAIN_ENVIRONMENT={env}");
            var settingFile = env.Equals("Development", StringComparison.OrdinalIgnoreCase) ? "appsettings.Development.json" : "appsettings.json";

            var isBakcground = parser.Has("-d");
            BootStrapHelper.Boot<AppModule>("plugins",settingFile,exitDelay:3);
            BootStrapHelper.RunWithSignal(isBakcground);
        }
    }
}
