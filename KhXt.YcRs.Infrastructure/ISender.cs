﻿using KhXt.YcRs.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Infrastructure.Sms
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISender<T>
    {
        Result sendSms(T data);
    }
}
