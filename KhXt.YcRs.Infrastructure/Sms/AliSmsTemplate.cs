﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Infrastructure.Sms
{
    /// <summary>
    /// 阿里短信模板内容代号
    /// </summary>
    public class AliSmsTemplate
    {
        /// <summary>
        /// 普通获取验证码
        /// </summary>
        public static string ali_SmsCommon = "SMS_191151609";
    }
}
