﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Infrastructure.Sms
{
    public class AliSmsData : BaseSmsData
    {
        public string templateParams { set; get; }
        public SmsChannel channelType { set; get; }
        public string signName { set; get; }
        public string templateId { set; get; }
        public string action { set; get; }
    }
    /// <summary>
    /// 短信模板内容替代参数
    /// </summary>
    public class SmsSimpleCodeTemplateData
    {
        public string code { set; get; }
    }
}
