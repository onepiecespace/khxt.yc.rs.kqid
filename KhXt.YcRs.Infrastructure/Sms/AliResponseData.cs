﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Infrastructure.Sms
{
    public class AliResponseData
    {
        /// <summary>
        /// 返回文案描述
        /// </summary>
        public string Message { set; get; }
        public string BizId { set; get; }
        public string RequestId { set; get; }
        /// <summary>
        /// 状态码：OK （成功）
        /// </summary>
        public string Code { set; get; }
    }
}
