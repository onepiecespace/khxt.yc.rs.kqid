﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Infrastructure.Sms.Config
{
    /// <summary>
    /// 短信中心配置keys
    /// </summary>
    public class SmsConfigKeys
    {
        public static string SMS_Ali_AccessKeyId = "SmsSettings:Ali:Ali_Sms_AccessKeyId";
        public static string SMS_Ali_AccessKeySecret = "SmsSettings:Ali:Ali_Sms_AccessKeySecret";
        public static string SMS_Ali_MethodActonName = "SmsSettings:Ali:Ali_Sms_MethodActonName";
        public static string SMS_Ali_Version = "SmsSettings:Ali:Ali_Sms_Version";
        public static string SMS_Ali_Domain = "SmsSettings:Ali:Ali_Sms_Domain";
        public static string SMS_Ali_ExpireSeconds = "SmsSettings:Ali:Ali_Sms_ExpireSeconds";
    }

}
