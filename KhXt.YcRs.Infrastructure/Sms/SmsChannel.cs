﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace KhXt.YcRs.Infrastructure.Sms
{
    /// <summary>
    /// 短信通道后期增加扩展
    /// </summary>
    public enum SmsChannel
    {
        alisms = 1
    }
    /// <summary>
    /// 短信签名
    /// </summary>
    public enum SmsSignName
    {
        [Description("两个黄鹂")]
        lianggehuangli = 1,
        [Description("杜甫语文")]
        dufuyuwen = 2,
        [Description("马小哈")]
        mxh = 3,
        [Description("黄鹂商学院")]
        college = 4
    }
    /// <summary>
    /// 使用短信业务平台名称
    /// </summary>
    public enum SmsPlatform
    {
        [Description("两个黄鹂")]
        huangli = 1,
        [Description("杜甫语文")]
        dufu = 2,
        [Description("马小哈")]
        mxh = 3,
        [Description("两个黄鹂商学院")]
        college = 4
    }
}
