﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Http;
using Aliyun.Acs.Core.Profile;
using KhXt.YcRs.Domain;
using KhXt.YcRs.Infrastructure.Config;
using KhXt.YcRs.Infrastructure.Sms.Config;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Infrastructure.Sms
{
    /// <summary>
    /// 阿里发送短信
    /// </summary>
    public class AliSmsSender : ISender<AliSmsData>
    {
        public AliSmsSender()
        {

        }
        public Result sendSms(AliSmsData data)
        {
            var resultData = new Result();
            String accessKeyId = ConfigurationHelper.Config[SmsConfigKeys.SMS_Ali_AccessKeyId];
            String accessKeySecret = ConfigurationHelper.Config[SmsConfigKeys.SMS_Ali_AccessKeySecret];
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultAcsClient client = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            request.Method = MethodType.POST;
            request.Domain = ConfigurationHelper.Config[SmsConfigKeys.SMS_Ali_Domain];
            request.Action = ConfigurationHelper.Config[SmsConfigKeys.SMS_Ali_MethodActonName];
            request.Version = ConfigurationHelper.Config[SmsConfigKeys.SMS_Ali_Version];
            request.AddQueryParameters("PhoneNumbers", data.phoneNo);
            request.AddQueryParameters("SignName", data.signName);
            var templateId = string.Empty;
            //var templateParams = $"{\"code\":\"88888\"}";
            SmsSimpleCodeTemplateData smsContentParams = null;
            var jsonParams = string.Empty;
            switch (data.bizType)
            {
                case "1"://用户登录获取验证码
                    templateId = AliSmsTemplate.ali_SmsCommon;
                    smsContentParams = new SmsSimpleCodeTemplateData() { code = data.code };
                    jsonParams = JsonConvert.SerializeObject(smsContentParams);
                    request.AddQueryParameters("TemplateParam", jsonParams);
                    break;
                case "2"://绑定手机号，获取验证码
                    templateId = AliSmsTemplate.ali_SmsCommon;
                    break;
                case "3":
                    templateId = "";
                    break;
                default:
                    break;
            }

            request.AddQueryParameters("TemplateCode", templateId);

            try
            {
                CommonResponse response = client.GetCommonResponse(request);
                var responseMessageJsonData = Encoding.Default.GetString(response.HttpResponse.Content);
                if (string.IsNullOrWhiteSpace(responseMessageJsonData))
                {

                    resultData.Code = (int)ResultCode.Fail;
                    resultData.Message = "调用阿里云短信网关返回接口数据为空";
                    return resultData;

                }
                var responseModelData = JsonConvert.DeserializeObject<AliResponseData>(responseMessageJsonData);
                if (responseModelData == null)
                {
                    resultData.Code = (int)ResultCode.Fail;
                    resultData.Message = "调用阿里云短信网关返回接口数据为空";
                    return resultData;
                }
                if (responseModelData.Code != "OK")
                {
                    resultData.Code = (int)ResultCode.Fail;
                    resultData.Message = $"调用阿里云短信网关，返回结果：{responseMessageJsonData}";
                    return resultData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultData;
        }
    }
}
