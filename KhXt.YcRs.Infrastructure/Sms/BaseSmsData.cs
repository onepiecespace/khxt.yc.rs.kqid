﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Infrastructure.Sms
{
    public class BaseSmsData
    {
        public string phoneNo { set; get; }
        /// <summary>
        /// 业务短信类型：登录、注册、找回免密等
        /// </summary>
        public string bizType { set; get; }
        //验证码
        public string code { set; get; }
        //扩展文案
        public string text { set; get; }
        //扩展文案1
        public string ext_text1 { set; get; }
        //扩展文案2
        public string ext_text2 { set; get; }
    }
}
