﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Infrastructure.Config
{
    /// <summary>
    /// 全局配置管理类（适配多环境）
    /// </summary>
    public static class ConfigurationHelper
    {
        public static IConfiguration Config { get; set; }
        static ConfigurationHelper()
        {
            var env = CustomerServiceProvider.ServiceProvider.GetRequiredService<IHostingEnvironment>();
            Config = new ConfigurationBuilder()
              .SetBasePath(env.ContentRootPath)
              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
              .Build();
        }
    }

    public static class CustomerServiceProvider
    {
        public static IServiceProvider ServiceProvider { get; set; }
    }
}
