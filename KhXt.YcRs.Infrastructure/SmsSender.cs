﻿using KhXt.YcRs.Domain;
using KhXt.YcRs.Infrastructure.Sms;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhXt.YcRs.Infrastructure
{
    public class SmsSender<T>
    {
        public static Result send(ISender<T> sender, T data)
        {
            return sender.sendSms(data);
        }
    }
}
